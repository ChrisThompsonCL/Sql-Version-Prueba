﻿CREATE TABLE [temp].[LocalesFonoFarma] (
    [Store]       INT           NOT NULL,
    [Descripcion] NVARCHAR (50) NULL,
    [Region]      NVARCHAR (50) NULL,
    [Ciudad]      NVARCHAR (50) NULL,
    [Tipo]        NVARCHAR (50) NULL,
    CONSTRAINT [PK_LocalesFonoFarma] PRIMARY KEY CLUSTERED ([Store] ASC) ON [FG_200804Q]
);

