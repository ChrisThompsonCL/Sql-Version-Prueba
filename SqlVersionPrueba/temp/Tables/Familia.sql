﻿CREATE TABLE [temp].[Familia] (
    [SKU]              VARCHAR (50) NOT NULL,
    [Familia-Variedad] VARCHAR (50) NULL,
    CONSTRAINT [PK_Familia] PRIMARY KEY CLUSTERED ([SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Familia_Familia]
    ON [temp].[Familia]([Familia-Variedad] ASC);

