﻿CREATE TABLE [temp].[MonitoreoCambiosPrecios] (
    [FechaCorrida]     DATE         NULL,
    [FechaCambio]      DATE         NOT NULL,
    [Sku]              INT          NOT NULL,
    [Division_Desc]    VARCHAR (50) NULL,
    [Subdivision_Desc] VARCHAR (50) NULL,
    [Price]            FLOAT (53)   NULL,
    [Cambio]           INT          NOT NULL,
    [EstáEnSugerencia] INT          NOT NULL,
    [FechaSug1]        DATE         NULL,
    [TomaSug1]         INT          NOT NULL,
    [FechaSug2]        DATE         NULL,
    [TomaSug2]         INT          NOT NULL,
    [TomaSugerencia]   INT          NOT NULL,
    [Movimiento]       VARCHAR (9)  NOT NULL,
    [PrecioSug1]       FLOAT (53)   NULL,
    [PrecioSug2]       FLOAT (53)   NULL,
    [Movimiento1]      VARCHAR (8)  NULL,
    [Movimiento2]      VARCHAR (8)  NULL,
    [SigueTendencia]   INT          NOT NULL
);

