﻿CREATE TABLE [temp].[PriceandQuantity] (
    [Date]              DATETIME       NULL,
    [SKU]               FLOAT (53)     NULL,
    [División]          NVARCHAR (255) NULL,
    [PrecioListaActual] FLOAT (53)     NULL,
    [VolumenActual]     FLOAT (53)     NULL,
    [PrecioListaOptimo] FLOAT (53)     NULL,
    [VolumenOptimo]     FLOAT (53)     NULL
);

