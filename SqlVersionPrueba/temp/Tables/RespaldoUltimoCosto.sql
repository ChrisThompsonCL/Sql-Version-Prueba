﻿CREATE TABLE [temp].[RespaldoUltimoCosto] (
    [SKU]         INT           NOT NULL,
    [Date]        DATE          NOT NULL,
    [Price]       FLOAT (53)    NULL,
    [Cost]        FLOAT (53)    NULL,
    [CostWithIVA] BIT           NULL,
    [Motive]      NVARCHAR (10) NULL
);

