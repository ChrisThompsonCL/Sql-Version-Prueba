﻿CREATE TABLE [temp].[StoreBrick] (
    [DIRECCION_LOCAL]   NVARCHAR (255) NULL,
    [BRICK_ID]          FLOAT (53)     NULL,
    [REGION_DESC]       NVARCHAR (255) NULL,
    [COMUNA_BRICK_DESC] NVARCHAR (255) NULL,
    [BRICK_DESC]        NVARCHAR (255) NULL,
    [REGION_ID]         FLOAT (53)     NULL,
    [MES_ID]            FLOAT (53)     NULL,
    [CUP_ID]            FLOAT (53)     NULL,
    [CADENA]            NVARCHAR (255) NULL,
    [FILIAL_ID]         FLOAT (53)     NULL
);

