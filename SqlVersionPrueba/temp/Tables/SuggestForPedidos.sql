﻿CREATE TABLE [temp].[SuggestForPedidos] (
    [sku]          INT        NOT NULL,
    [store]        INT        NOT NULL,
    [date]         DATE       NOT NULL,
    [fitDay]       FLOAT (53) NULL,
    [fitWeek]      FLOAT (53) NULL,
    [shareday]     FLOAT (53) NULL,
    [dailySale]    FLOAT (53) NULL,
    [days]         INT        NULL,
    [rate]         FLOAT (53) NULL,
    [suggestMin]   INT        NULL,
    [suggestMax]   INT        NULL,
    [LeadTime]     INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [SafetyDays]   INT        NULL
);

