﻿CREATE TABLE [temp].[RespaldoUltimoCostoReferencia] (
    [IdCodigoCliente] NVARCHAR (200)   NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [Fecha]           DATE             NOT NULL,
    [CostoReferencia] FLOAT (53)       NULL
);

