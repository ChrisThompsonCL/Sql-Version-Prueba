﻿CREATE TABLE [temp].[StoreClassRank] (
    [Store]      INT          NOT NULL,
    [StoreClass] VARCHAR (50) NULL,
    CONSTRAINT [PK_StoreClassRank] PRIMARY KEY CLUSTERED ([Store] ASC) ON [FG_200804Q]
);

