﻿CREATE TABLE [temp].[Last3PriceSuggest] (
    [sku]            INT        NOT NULL,
    [FechaCalculo1]  DATE       NULL,
    [PrecioPricing1] FLOAT (53) NULL,
    [FechaCalculo2]  DATE       NULL,
    [PrecioPricing2] FLOAT (53) NULL,
    [FechaCalculo3]  DATE       NULL,
    [PrecioPricing3] FLOAT (53) NULL
);

