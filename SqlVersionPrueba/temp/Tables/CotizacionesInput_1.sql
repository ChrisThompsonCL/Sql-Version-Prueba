﻿CREATE TABLE [temp].[CotizacionesInput] (
    [sku]          INT        NULL,
    [PricingClass] INT        NULL,
    [DiffPrecio]   FLOAT (53) NULL,
    [Efectividad]  FLOAT (53) NULL,
    [Contribución] FLOAT (53) NOT NULL
);

