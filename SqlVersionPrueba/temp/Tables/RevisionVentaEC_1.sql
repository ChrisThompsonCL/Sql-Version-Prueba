﻿CREATE TABLE [temp].[RevisionVentaEC] (
    [Date]     DATETIME   NULL,
    [Store]    FLOAT (53) NULL,
    [SKU]      FLOAT (53) NULL,
    [Income]   FLOAT (53) NULL,
    [Quantity] FLOAT (53) NULL
);

