﻿CREATE TABLE [temp].[NextReplenishment] (
    [Date]           SMALLDATETIME NOT NULL,
    [SKU]            INT           NOT NULL,
    [Store]          INT           NOT NULL,
    [Max]            INT           NULL,
    [Min]            INT           NULL,
    [Rate]           FLOAT (53)    NULL,
    [Days]           INT           NULL,
    [inOperation]    BIT           NULL,
    [LS]             FLOAT (53)    NULL,
    [OS]             FLOAT (53)    NULL,
    [currentMax]     INT           NULL,
    [currentMin]     INT           NULL,
    [currentSuggest] INT           NULL
);

