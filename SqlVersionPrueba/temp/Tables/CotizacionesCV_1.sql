﻿CREATE TABLE [temp].[CotizacionesCV] (
    [Sku]              INT        NULL,
    [Date]             DATE       NULL,
    [PricingClass]     INT        NULL,
    [CV]               INT        NULL,
    [UpdatedCV]        INT        NULL,
    [SalcoPrice]       INT        NULL,
    [DiffSalcoCV]      FLOAT (53) NULL,
    [Unidades]         INT        NULL,
    [sPrice]           FLOAT (53) NULL,
    [CostPond]         FLOAT (53) NULL,
    [Num]              BIGINT     NULL,
    [DateBefore]       DATE       NULL,
    [CVBefore]         INT        NULL,
    [UpdateCVAnterior] INT        NULL,
    [NumCotizaciones]  BIGINT     NULL,
    [DiasdeDiff]       INT        NULL,
    [DiffCV]           FLOAT (53) NULL,
    [CambioCV?]        INT        NOT NULL
);

