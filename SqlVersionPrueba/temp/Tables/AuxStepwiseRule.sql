﻿CREATE TABLE [temp].[AuxStepwiseRule] (
    [StepId]  INT        NOT NULL,
    [StepMax] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_AuxStepwiseRule] PRIMARY KEY CLUSTERED ([StepId] ASC)
);

