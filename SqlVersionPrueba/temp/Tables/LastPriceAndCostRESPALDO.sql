﻿CREATE TABLE [temp].[LastPriceAndCostRESPALDO] (
    [SKU]         INT           NOT NULL,
    [Date]        SMALLDATETIME NOT NULL,
    [Price]       FLOAT (53)    NULL,
    [Cost]        FLOAT (53)    NULL,
    [CostWithIVA] BIT           NULL,
    [Motive]      NVARCHAR (10) NULL
);

