﻿CREATE TABLE [temp].[ManufacturerCost] (
    [SKU]  INT        NOT NULL,
    [Cost] FLOAT (53) NULL,
    CONSTRAINT [PK_ManufacturerCost] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

