﻿CREATE TABLE [temp].[pilotopowerbi] (
    [Fecha]     SMALLDATETIME  NOT NULL,
    [Tiendas]   SMALLINT       NOT NULL,
    [Dirección] NVARCHAR (150) NULL,
    [Región]    NVARCHAR (57)  NOT NULL,
    [Venta Un]  INT            NULL,
    [Venta $]   FLOAT (53)     NULL
);

