﻿CREATE TABLE [temp].[LocalesFarmaPrecio] (
    [Store] INT           NOT NULL,
    [Tipo]  NVARCHAR (50) NULL,
    CONSTRAINT [PK_LocalesFarmaPrecio] PRIMARY KEY CLUSTERED ([Store] ASC) ON [FG_200804Q]
);

