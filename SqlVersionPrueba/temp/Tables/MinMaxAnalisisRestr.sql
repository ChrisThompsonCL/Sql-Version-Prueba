﻿CREATE TABLE [temp].[MinMaxAnalisisRestr] (
    [SKU]                  INT          NOT NULL,
    [Store]                INT          NOT NULL,
    [EntryDate]            DATE         NULL,
    [Price]                FLOAT (53)   NULL,
    [Cost]                 FLOAT (53)   NULL,
    [SuggestMin]           INT          NULL,
    [SuggestMax]           INT          NULL,
    [Minimo]               INT          NULL,
    [Fijo]                 INT          NULL,
    [Planograma]           INT          NULL,
    [MedioDUN]             NUMERIC (20) NULL,
    [SuggestMin_Actual]    INT          NULL,
    [SuggestMax_Actual]    INT          NULL,
    [SuggestMin_SoloMax]   INT          NULL,
    [SuggestMax_SoloMax]   INT          NULL,
    [SuggestMin_MismaDist] INT          NULL,
    [SuggestMax_MismaDist] INT          NULL
);

