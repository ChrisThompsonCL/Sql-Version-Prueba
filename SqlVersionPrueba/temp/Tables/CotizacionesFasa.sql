﻿CREATE TABLE [temp].[CotizacionesFasa] (
    [Sku]                INT        NULL,
    [Date]               DATE       NULL,
    [PricingClass]       INT        NULL,
    [Fasa]               INT        NULL,
    [UpdatedFasa]        INT        NULL,
    [SalcoPrice]         INT        NULL,
    [DiffSalcoFasa]      FLOAT (53) NULL,
    [Unidades]           INT        NULL,
    [sPrice]             FLOAT (53) NULL,
    [CostPond]           FLOAT (53) NULL,
    [Num]                BIGINT     NULL,
    [DateBefore]         DATE       NULL,
    [FasaBefore]         INT        NULL,
    [UpdateFasaAnterior] INT        NULL,
    [NumCotizaciones]    BIGINT     NULL,
    [DiasdeDiff]         INT        NULL,
    [DiffFasa]           FLOAT (53) NULL,
    [CambioFasa?]        INT        NOT NULL
);

