﻿-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2009-09-14
-- Description:	Genera una intervalo de stock
-- =============================================
CREATE PROCEDURE [temp].[CreateCustomStock] 
	-- Add the parameters for the stored procedure here
	@begindate smalldatetime = NULL,
	@enddate smalldatetime = NULL
AS
BEGIN
	
	truncate table [temp].[CustomStock]

	--inserto todo
	INSERT INTO [temp].[CustomStock] ([SKU],[Store],[Date],[Stock])
	select s.sku,s.store,@begindate,s.stock from [series].[Stock] s 
	inner join (select sku,store,max(date) date from [series].[Stock] where date <=@begindate group by sku,store) l
	on l.sku = s.sku and s.store = l.store and s.date = l.date
	union all
	select sku,store,date,stock from [series].[Stock]
	where date between dateadd(day,1,@begindate) and @enddate--dateadd(day,-1,@enddate) --para agregar la foto al final

END
