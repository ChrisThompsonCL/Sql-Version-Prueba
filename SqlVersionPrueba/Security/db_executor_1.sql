﻿CREATE ROLE [db_executor]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [db_executor] ADD MEMBER [ADPRICING\PlanZ];


GO
ALTER ROLE [db_executor] ADD MEMBER [ADPRICING\Zapotecas];

