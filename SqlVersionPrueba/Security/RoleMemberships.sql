﻿ALTER ROLE [db_owner] ADD MEMBER [oprDBSalcobrand];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\serveradminproy];


GO
ALTER ROLE [db_owner] ADD MEMBER [oprDBIntra];


GO
ALTER ROLE [db_owner] ADD MEMBER [oprDBEstandarizacion];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\Fedex];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\PlanZ];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\xOPRDB_01Salcobrand_Admin];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\administrador];


GO
ALTER ROLE [db_owner] ADD MEMBER [ADPRICING\EspecialAS];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [ADPRICING\Zapotecas];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ADPRICING\PlanZ];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ADPRICING\Zapotecas];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ADPRICING\PlanZ];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ADPRICING\Zapotecas];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ADPRICING\Innovacion];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ADPRICING\Operaciones];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ADPRICING\PlanZ];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ADPRICING\Zapotecas];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ADPRICING\Operaciones];

