﻿CREATE TABLE [inclusion].[Combinaciones] (
    [SKU]                    INT        NOT NULL,
    [Store]                  INT        NOT NULL,
    [Division]               INT        NOT NULL,
    [suggest]                INT        NOT NULL,
    [probabilidad]           FLOAT (53) NULL,
    [Precio]                 FLOAT (53) NULL,
    [Costo]                  FLOAT (53) NULL,
    [probabilidad1]          FLOAT (53) NULL,
    [probabilidad2]          FLOAT (53) NULL,
    [probabilidad3]          FLOAT (53) NULL,
    [probabilidad4]          FLOAT (53) NULL,
    [probabilidad5]          FLOAT (53) NULL,
    [probabilidad6]          FLOAT (53) NULL,
    [MarketShareBrick]       FLOAT (53) NULL,
    [MarketShareOtrosBricks] FLOAT (53) NULL,
    [MarketShareTotal]       FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Combinaciones]
    ON [inclusion].[Combinaciones]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Combinaciones_1]
    ON [inclusion].[Combinaciones]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Combinaciones_Division]
    ON [inclusion].[Combinaciones]([Division] ASC)
    INCLUDE([SKU], [Store]);

