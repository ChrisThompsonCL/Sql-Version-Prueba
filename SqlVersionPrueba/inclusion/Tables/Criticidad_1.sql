﻿CREATE TABLE [inclusion].[Criticidad] (
    [SKU]          INT        NOT NULL,
    [Unidades]     FLOAT (53) NULL,
    [Ingreso]      FLOAT (53) NULL,
    [Contribución] FLOAT (53) NULL,
    [Boletas]      INT        NULL,
    CONSTRAINT [PK_Criticidad] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

