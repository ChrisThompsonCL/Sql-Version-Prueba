﻿CREATE TABLE [inclusion].[Propuesta] (
    [SKU]                 INT           NULL,
    [Store]               INT           NULL,
    [Tipo]                NVARCHAR (50) NULL,
    [ProbVenderEn4sem]    FLOAT (53)    NULL,
    [IngresoEsperado]     FLOAT (53)    NULL,
    [CostoTotal]          FLOAT (53)    NULL,
    [ProbNoVenderEn10sem] FLOAT (53)    NULL,
    [DivisionInicial]     INT           NULL,
    [Escenario]           NVARCHAR (50) NULL
);

