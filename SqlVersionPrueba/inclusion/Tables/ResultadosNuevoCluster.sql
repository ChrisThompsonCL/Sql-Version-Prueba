﻿CREATE TABLE [inclusion].[ResultadosNuevoCluster] (
    [SKU]                          INT        NOT NULL,
    [Store]                        INT        NOT NULL,
    [Sugerido]                     INT        NOT NULL,
    [Costo]                        FLOAT (53) NULL,
    [Precio]                       FLOAT (53) NULL,
    [UnidadesEsperadasOptimista]   FLOAT (53) NULL,
    [UnidadesEsperadasConservador] FLOAT (53) NULL,
    [Critico]                      INT        NOT NULL,
    [VentaUltimos6Meses]           INT        NULL,
    [Stock]                        INT        NULL,
    [SugeridoActual]               INT        NULL,
    [Exclusion]                    BIT        NULL,
    [probabilidadExito]            FLOAT (53) NULL,
    [sugeridoPromedio]             FLOAT (53) NULL
);

