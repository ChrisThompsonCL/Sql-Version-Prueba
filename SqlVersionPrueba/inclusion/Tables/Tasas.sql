﻿CREATE TABLE [inclusion].[Tasas] (
    [SKU]    INT           NOT NULL,
    [Store]  INT           NOT NULL,
    [Semana] DATE          NOT NULL,
    [Tasa]   FLOAT (53)    NULL,
    [Metodo] NVARCHAR (50) NULL
);

