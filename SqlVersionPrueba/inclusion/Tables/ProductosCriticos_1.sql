﻿CREATE TABLE [inclusion].[ProductosCriticos] (
    [sku]      BIGINT NULL,
    [Division] INT    NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductosCriticos]
    ON [inclusion].[ProductosCriticos]([sku] ASC);

