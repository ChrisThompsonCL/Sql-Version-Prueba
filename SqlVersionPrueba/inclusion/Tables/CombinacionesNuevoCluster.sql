﻿CREATE TABLE [inclusion].[CombinacionesNuevoCluster] (
    [SKU]           INT        NOT NULL,
    [Store]         INT        NOT NULL,
    [Division]      INT        NOT NULL,
    [suggest]       INT        NOT NULL,
    [probabilidad]  FLOAT (53) NULL,
    [Precio]        FLOAT (53) NULL,
    [Costo]         FLOAT (53) NULL,
    [probabilidad1] FLOAT (53) NULL,
    [probabilidad2] FLOAT (53) NULL,
    [probabilidad3] FLOAT (53) NULL,
    [probabilidad4] FLOAT (53) NULL,
    [probabilidad5] FLOAT (53) NULL,
    [probabilidad6] FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CombinacionesNuevoCluster_Division]
    ON [inclusion].[CombinacionesNuevoCluster]([Division] ASC)
    INCLUDE([SKU], [Store]);


GO
CREATE NONCLUSTERED INDEX [IX_CombinacionesNuevoCluster_1]
    ON [inclusion].[CombinacionesNuevoCluster]([SKU] ASC, [Store] ASC);

