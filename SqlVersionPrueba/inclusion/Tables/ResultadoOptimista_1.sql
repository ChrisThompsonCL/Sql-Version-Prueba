﻿CREATE TABLE [inclusion].[ResultadoOptimista] (
    [Sku]                    INT           NOT NULL,
    [Store]                  INT           NOT NULL,
    [MetodoUtilizado]        NVARCHAR (50) NULL,
    [Tipo]                   INT           NULL,
    [VEQ]                    FLOAT (53)    NULL,
    [TotalRanking]           VARCHAR (1)   NULL,
    [IngresoEspTotalMEnsual] FLOAT (53)    NULL,
    [CostoTotalMensual]      FLOAT (53)    NULL,
    [ProbVender0en10sem]     FLOAT (53)    NULL
);

