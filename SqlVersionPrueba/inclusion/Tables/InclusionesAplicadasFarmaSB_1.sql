﻿CREATE TABLE [inclusion].[InclusionesAplicadasFarmaSB] (
    [Store] SMALLINT NULL,
    [SKU]   INT      NULL,
    [Date]  DATE     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_InclusionesMensualFarmaSB]
    ON [inclusion].[InclusionesAplicadasFarmaSB]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InclusionesMensualFarmaSB_1]
    ON [inclusion].[InclusionesAplicadasFarmaSB]([SKU] ASC);

