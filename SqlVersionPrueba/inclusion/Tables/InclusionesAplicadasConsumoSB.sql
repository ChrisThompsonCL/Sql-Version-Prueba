﻿CREATE TABLE [inclusion].[InclusionesAplicadasConsumoSB] (
    [SKU]   INT  NULL,
    [Store] INT  NULL,
    [Date]  DATE CONSTRAINT [DF__Inclusione__Date__5D21AF45] DEFAULT ('2014-11-14') NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_InclusionesMensualConsumoSB]
    ON [inclusion].[InclusionesAplicadasConsumoSB]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InclusionesMensualConsumoSB_1]
    ON [inclusion].[InclusionesAplicadasConsumoSB]([SKU] ASC);

