﻿CREATE TABLE [inclusion].[DatosAMB] (
    [Division]       INT        NULL,
    [Store]          SMALLINT   NOT NULL,
    [GroupId]        BIGINT     NULL,
    [PrecioPromedio] FLOAT (53) NULL,
    [PrecioCadena]   FLOAT (53) NULL
);

