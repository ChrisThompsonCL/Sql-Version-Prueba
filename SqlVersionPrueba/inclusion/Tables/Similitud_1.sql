﻿CREATE TABLE [inclusion].[Similitud] (
    [Store1]   INT        NULL,
    [Store2]   INT        NULL,
    [Division] INT        NULL,
    [V]        FLOAT (53) NULL,
    [S1]       FLOAT (53) NULL,
    [S2]       FLOAT (53) NULL
);

