﻿CREATE TABLE [inclusion].[ResultadoAMB] (
    [Division] INT NOT NULL,
    [Store]    INT NOT NULL,
    [ClaseAMB] INT NOT NULL,
    CONSTRAINT [PK_ResultadoAMB] PRIMARY KEY CLUSTERED ([Division] ASC, [Store] ASC)
);

