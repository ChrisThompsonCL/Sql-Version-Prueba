﻿CREATE TABLE [inclusion].[Resultados] (
    [SKU]                          INT        NOT NULL,
    [Store]                        INT        NOT NULL,
    [Sugerido]                     INT        NOT NULL,
    [Costo]                        FLOAT (53) NULL,
    [Precio]                       FLOAT (53) NULL,
    [UnidadesEsperadasOptimista]   FLOAT (53) NULL,
    [UnidadesEsperadasConservador] FLOAT (53) NULL,
    [Critico]                      INT        NOT NULL,
    [VentaUltimos6Meses]           INT        NULL,
    [Stock]                        INT        NULL,
    [SugeridoActual]               INT        NULL,
    [Exclusion]                    BIT        NULL,
    [probabilidadExito]            FLOAT (53) NULL,
    [sugeridoPromedio]             FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Resultados]
    ON [inclusion].[Resultados]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Resultados_1]
    ON [inclusion].[Resultados]([Store] ASC);

