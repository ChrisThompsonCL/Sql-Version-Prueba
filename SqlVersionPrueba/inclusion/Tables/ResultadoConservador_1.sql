﻿CREATE TABLE [inclusion].[ResultadoConservador] (
    [Sku]                INT           NOT NULL,
    [Store]              INT           NOT NULL,
    [MetodoUtilizado]    NVARCHAR (50) NULL,
    [Tipo]               INT           NULL,
    [VEQ]                FLOAT (53)    NULL,
    [Pef]                FLOAT (53)    NULL,
    [Costo]              FLOAT (53)    NULL,
    [TotalRanking]       VARCHAR (1)   NULL,
    [IngresoEsperado]    FLOAT (53)    NULL,
    [CostoTotal]         FLOAT (53)    NULL,
    [ProbVender0en10sem] FLOAT (53)    NULL
);

