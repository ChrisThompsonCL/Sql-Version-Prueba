﻿CREATE TABLE [mix].[IncludedStores] (
    [Store]         INT           NOT NULL,
    [Group]         NVARCHAR (50) NULL,
    [InclusionDate] SMALLDATETIME NULL,
    [PricingUCDate] SMALLDATETIME NULL,
    CONSTRAINT [PK_IncludedStores] PRIMARY KEY CLUSTERED ([Store] ASC)
);

