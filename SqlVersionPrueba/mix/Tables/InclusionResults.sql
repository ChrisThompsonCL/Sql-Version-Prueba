﻿CREATE TABLE [mix].[InclusionResults] (
    [Date]                    DATETIME     NOT NULL,
    [Store]                   SMALLINT     NOT NULL,
    [SKU]                     INT          NOT NULL,
    [Description]             VARCHAR (50) NULL,
    [Generic]                 BIT          NULL,
    [Critical]                BIT          NULL,
    [Success]                 SMALLINT     NULL,
    [Universe]                SMALLINT     NULL,
    [Probability]             FLOAT (53)   NULL,
    [Quantity]                SMALLINT     NULL,
    [Income]                  FLOAT (53)   NULL,
    [Contribution]            FLOAT (53)   NULL,
    [Stock]                   FLOAT (53)   NULL,
    [Cost]                    FLOAT (53)   NULL,
    [Share]                   FLOAT (53)   NULL,
    [OtherGenerics]           SMALLINT     NULL,
    [ContributionSKU]         FLOAT (53)   NULL,
    [ContributionSubstitutes] FLOAT (53)   NULL,
    [HasStock]                BIT          NULL,
    [ExpectedUnits]           FLOAT (53)   NULL,
    [ExpectedIncome]          FLOAT (53)   NULL,
    [ExpectedContribution]    FLOAT (53)   NULL,
    [ExpectedStock]           FLOAT (53)   NULL,
    [ExpectedValue]           FLOAT (53)   NULL,
    [Suggest]                 INT          NULL,
    [StockLoad]               FLOAT (53)   NULL,
    [Accepted]                BIT          NULL,
    [CommentId]               INT          NULL,
    CONSTRAINT [PK_InclusionResults] PRIMARY KEY CLUSTERED ([Date] ASC, [Store] ASC, [SKU] ASC)
);

