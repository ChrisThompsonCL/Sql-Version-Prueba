﻿CREATE TABLE [pricing].[CategoryInfo] (
    [SKU]                  INT          NULL,
    [Name]                 VARCHAR (50) NULL,
    [Creacion]             DATE         NULL,
    [Division]             SMALLINT     NULL,
    [Division_Desc]        VARCHAR (50) NULL,
    [Positioning_ID]       SMALLINT     NULL,
    [Positioning_Desc]     VARCHAR (50) NULL,
    [Category_ID]          SMALLINT     NULL,
    [Category_Desc]        VARCHAR (50) NULL,
    [Class_ID]             SMALLINT     NULL,
    [Class_Desc]           VARCHAR (50) NULL,
    [CategoryManager_Desc] VARCHAR (50) NULL,
    [CategoryManager_ID]   SMALLINT     NULL,
    [Fabricante_ID]        SMALLINT     NULL,
    [Fabricante_DESC]      VARCHAR (50) NULL
);

