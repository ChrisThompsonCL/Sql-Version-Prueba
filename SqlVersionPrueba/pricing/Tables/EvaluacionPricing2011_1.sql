﻿CREATE TABLE [pricing].[EvaluacionPricing2011] (
    [Semana]            SMALLDATETIME NOT NULL,
    [SKU]               BIGINT        NOT NULL,
    [Category]          INT           NULL,
    [B]                 FLOAT (53)    NULL,
    [Bc]                FLOAT (53)    NULL,
    [Costo]             FLOAT (53)    NULL,
    [PrecioComp]        FLOAT (53)    NULL,
    [PrecioLista_Real]  FLOAT (53)    NULL,
    [PrecioEff_Real]    FLOAT (53)    NULL,
    [RatioComp_Real]    FLOAT (53)    NULL,
    [Volumen_Real]      FLOAT (53)    NULL,
    [Ingresos_Real]     FLOAT (53)    NULL,
    [Contribución_Real] FLOAT (53)    NULL,
    [K_Ajustado]        FLOAT (53)    NULL,
    [Fasa]              FLOAT (53)    NULL,
    [CV]                FLOAT (53)    NULL,
    [TipoPolitica]      VARCHAR (50)  NULL,
    [Mu]                FLOAT (53)    NULL,
    [EsFijo]            BIT           NULL,
    [PrecioLista_Sup]   FLOAT (53)    NULL,
    [PrecioEff_Sup]     FLOAT (53)    NULL,
    [RatioComp_Sup]     FLOAT (53)    NULL,
    [Volumen_Sup]       FLOAT (53)    NULL,
    [Ingresos_Sup]      FLOAT (53)    NULL,
    [Contribución_Sup]  FLOAT (53)    NULL,
    CONSTRAINT [PK_EvaluacionPricing2011] PRIMARY KEY CLUSTERED ([Semana] ASC, [SKU] ASC)
);

