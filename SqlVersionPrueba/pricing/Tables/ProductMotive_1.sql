﻿CREATE TABLE [pricing].[ProductMotive] (
    [MotiveId]    NVARCHAR (7)   NOT NULL,
    [MotiveDesc]  NVARCHAR (255) NULL,
    [IsOperative] BIT            NULL,
    CONSTRAINT [PK_ProductMotive] PRIMARY KEY CLUSTERED ([MotiveId] ASC)
);

