﻿CREATE TABLE [pricing].[LoadedScenario] (
    [SKU]        INT        NOT NULL,
    [Price]      FLOAT (53) NULL,
    [LoadedDate] DATE       NOT NULL,
    CONSTRAINT [PK_LoadedScenario] PRIMARY KEY CLUSTERED ([SKU] ASC, [LoadedDate] ASC)
);

