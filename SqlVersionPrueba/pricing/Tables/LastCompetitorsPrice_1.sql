﻿CREATE TABLE [pricing].[LastCompetitorsPrice] (
    [SKU]            INT           NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [Fasa]           FLOAT (53)    NULL,
    [CV]             FLOAT (53)    NULL,
    [Simi]           FLOAT (53)    NULL,
    [LastUpdateFasa] SMALLDATETIME NULL,
    [LastUpdateCV]   SMALLDATETIME NULL,
    [LastUpdateSimi] SMALLDATETIME NULL,
    CONSTRAINT [PK_LastCompetitorsPrice] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC)
);

