﻿CREATE TABLE [pricing].[FinalPricing2] (
    [DateKey]      DATE       NOT NULL,
    [SKU]          INT        NOT NULL,
    [ActualPrice]  FLOAT (53) NULL,
    [NewPrice]     FLOAT (53) NULL,
    [Forecast]     FLOAT (53) NULL,
    [Sale]         FLOAT (53) NULL,
    [Contribution] FLOAT (53) NULL,
    [Margin]       FLOAT (53) NULL,
    CONSTRAINT [PK_FinalPricing2] PRIMARY KEY CLUSTERED ([DateKey] ASC, [SKU] ASC)
);

