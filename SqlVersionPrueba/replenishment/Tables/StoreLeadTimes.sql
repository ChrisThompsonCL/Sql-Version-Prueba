﻿CREATE TABLE [replenishment].[StoreLeadTimes] (
    [Store]       INT           NOT NULL,
    [LeadTime]    INT           NOT NULL,
    [LeadTimeRef] INT           NULL,
    [Username]    NVARCHAR (50) NULL,
    [LastUpdate]  SMALLDATETIME NULL,
    CONSTRAINT [PK_StoreLeadTimes] PRIMARY KEY CLUSTERED ([Store] ASC)
);

