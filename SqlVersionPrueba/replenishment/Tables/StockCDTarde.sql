﻿CREATE TABLE [replenishment].[StockCDTarde] (
    [SKU]     INT  NOT NULL,
    [Date]    DATE NOT NULL,
    [StockCD] INT  NULL,
    [Transit] INT  NULL,
    [Order]   INT  NULL,
    CONSTRAINT [PK_StockCD] PRIMARY KEY NONCLUSTERED ([SKU] ASC)
);

