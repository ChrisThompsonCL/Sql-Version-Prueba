﻿CREATE TABLE [replenishment].[NextSuggest] (
    [SKU]          INT        NOT NULL,
    [Store]        INT        NOT NULL,
    [Max]          INT        NULL,
    [Min]          INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [Days]         INT        NULL,
    [inOperation]  BIT        NULL,
    [LS]           FLOAT (53) NULL,
    [OS]           FLOAT (53) NULL,
    [Date]         DATE       NOT NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [Minimo]       INT        NULL,
    [Fijo]         INT        NULL,
    [Planograma]   INT        NULL,
    [MedioDUN]     INT        NULL,
    [Maximo]       INT        NULL,
    [CajaTira]     INT        NULL,
    [FijoEstricto] INT        NULL,
    CONSTRAINT [PK_NextSuggest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NextSuggest]
    ON [replenishment].[NextSuggest]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NextSuggest_1]
    ON [replenishment].[NextSuggest]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NextSuggest_Date]
    ON [replenishment].[NextSuggest]([Date] ASC)
    INCLUDE([SKU], [Store], [Max]);

