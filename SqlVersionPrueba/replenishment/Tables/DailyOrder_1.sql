﻿CREATE TABLE [replenishment].[DailyOrder] (
    [DUN14]          BIGINT NULL,
    [DUN]            INT    NULL,
    [SKU]            INT    NOT NULL,
    [Store]          INT    NOT NULL,
    [Original_Order] INT    NULL,
    [Delta_Order]    INT    NULL,
    [Rounded_Order]  INT    NULL,
    [Cutted_Order]   INT    NULL,
    [Generic_Order]  INT    NULL,
    CONSTRAINT [PK_DailyOrder] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

