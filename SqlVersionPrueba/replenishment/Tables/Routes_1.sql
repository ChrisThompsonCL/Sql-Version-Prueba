﻿CREATE TABLE [replenishment].[Routes] (
    [RouteId]        INT           NOT NULL,
    [Store]          INT           NOT NULL,
    [DayOfWeek]      INT           NOT NULL,
    [LogisticAreaId] NVARCHAR (50) NOT NULL,
    [LT]             INT           NOT NULL,
    CONSTRAINT [PK_Routes] PRIMARY KEY CLUSTERED ([Store] ASC, [DayOfWeek] ASC, [LogisticAreaId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Routes]
    ON [replenishment].[Routes]([LogisticAreaId] ASC)
    INCLUDE([Store], [DayOfWeek], [LT]);

