﻿CREATE TABLE [replenishment].[SpecialRoutes] (
    [Store]             INT           NOT NULL,
    [DayOfWeek]         INT           NOT NULL,
    [LogisticAreaId]    NVARCHAR (50) NOT NULL,
    [LT]                INT           NOT NULL,
    [WeeklyFrecuency]   INT           NOT NULL,
    [LastReplenishment] DATE          NULL,
    CONSTRAINT [PK_SpecialRoutes] PRIMARY KEY CLUSTERED ([Store] ASC, [DayOfWeek] ASC, [LogisticAreaId] ASC)
);

