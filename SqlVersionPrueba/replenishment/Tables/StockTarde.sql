﻿CREATE TABLE [replenishment].[StockTarde] (
    [SKU]       INT        NOT NULL,
    [Store]     INT        NOT NULL,
    [Date]      DATE       NOT NULL,
    [Stock]     INT        NULL,
    [Suggest]   INT        NULL,
    [Transit]   INT        NULL,
    [Max]       INT        NULL,
    [Min]       INT        NULL,
    [IsBloqued] NCHAR (10) NULL,
    CONSTRAINT [PK_StockTarde] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

