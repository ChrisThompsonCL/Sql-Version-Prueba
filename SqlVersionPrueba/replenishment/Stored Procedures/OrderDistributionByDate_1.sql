﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-06-13
-- Description:	Se adelantan pedidos en días anteriores por sku
-- =============================================
CREATE PROCEDURE [replenishment].[OrderDistributionByDate] 

@sku int,
@dia_seleccionado date,
@cantidad_dias int,
@cantidad_repartir int

AS
BEGIN

declare @ped_efect int = (SELECT [PedidoEfectivo]+isnull(b.ExcesoCD,0)+isnull(b.SobrecargaEstacionalidad,0)+isnull(b.SobrecargaFeriado,0)
						  +isnull(b.SobrecargaPromocion,0)+isnull(b.SobrecargaRestricción,0)
						  +isnull(c.ExcesoCD,0)+isnull(c.SobrecargaEstacionalidad,0)+isnull(c.SobrecargaFeriado,0)
						  +isnull(c.SobrecargaPromocion,0)+isnull(c.SobrecargaRestricción,0)
						  FROM [minmax].[DistributionOrdersForecastCurrent] a
						  left join aux.[DistributionOrderDeltas] b on b.sku=a.sku and b.Date=a.date
						  left join minmax.[DistributionOrderDeltas] c on c.sku=a.sku and c.Date=a.date
						  where a.sku=@sku and a.[Date]=@dia_seleccionado)

set @cantidad_repartir=(select case when @ped_efect<@cantidad_repartir then @ped_efect else @cantidad_repartir end)

-- se el primer delta
update d set d.SobrecargaEstacionalidad=d.SobrecargaEstacionalidad-@cantidad_repartir
from aux.[DistributionOrderDeltas] d
where date=@dia_seleccionado and sku=@sku 

insert into aux.[DistributionOrderDeltas]
select @sku,@dia_seleccionado,null,null,-@cantidad_repartir,null,null
FROM [minmax].[DistributionOrdersForecastCurrent] a
left join aux.[DistributionOrderDeltas] b on b.sku=a.sku and b.Date=a.date
where a.sku=@sku and a.[Date]=@dia_seleccionado and b.sku is null


-- se distribuye
declare @rubro nvarchar(10)=(select Rubro from [temp].[RubrosDistribucion] where sku=@sku)

select x.*,isnull(b.LocalesTotales,1) LocalesTotales,@cantidad_repartir distr
into #distribucion
from (
	SELECT [Date]
		  ,[sku]
		  ,[PedidoEfectivo]
		  ,ROW_NUMBER () OVER (Partition By [sku] ORDER BY [Date] DESC) Orden 
	  FROM [minmax].[DistributionOrdersForecastCurrent]
	  where sku=@sku and date < @dia_seleccionado and [PedidoEfectivo]>0
  ) x
  left join (
		select DayOfWeek,count(store) LocalesTotales 
		from replenishment.StoreRoutesTarde where rubro =@rubro
		group by DayOfWeek
  ) b on b.DayOfWeek=DATEPART ( weekday ,dateadd(day,1,x.Date))
  where Orden<=@cantidad_dias

  declare @total int = (select sum([PedidoEfectivo]) total from #distribucion)
  declare @prom float = (select cast(avg(LocalesTotales*1.0) as float) total from #distribucion)
  
  select date,PedidoEfectivo,LocalesTotales
  ,cast([PedidoEfectivo]*1.0*LocalesTotales/@prom*1.0as float) correccion1
  ,cast((cast([PedidoEfectivo]*1.0*LocalesTotales/@prom*1.0as float))*1.0*distr*1.0/@total*1.0 as float) delta
  ,round(cast((cast([PedidoEfectivo]*1.0*LocalesTotales/@prom*1.0as float))*1.0*distr*1.0/@total*1.0 as float),0) deltaRed
  ,Orden
  into #distribucion2
  from #distribucion

  declare @total2 int = (select sum(deltaRed) total from #distribucion2)

  declare @dif int = @cantidad_repartir-@total2

  update d set d.deltaRed=d.deltaRed+@dif
  from #distribucion2 d
  where orden = 1

  --select *from #distribucion2

  -- update final
  update a set a.SobrecargaEstacionalidad=a.SobrecargaEstacionalidad+b.deltaRed
  from aux.[DistributionOrderDeltas] a
  inner join #distribucion2 b on b.Date=a.Date
  where sku=@sku
  
  insert into aux.[DistributionOrderDeltas]
  select a.sku,a.Date,null,null,c.deltaRed,null,null
  FROM [minmax].[DistributionOrdersForecastCurrent] a
  inner join #distribucion2 c on c.Date=a.Date
  left join aux.[DistributionOrderDeltas] b on b.sku=a.sku and b.Date=a.date
  where a.sku=@sku and b.sku is null

END