﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2013-06-04
-- Description:	Cálculo de reposición de SB
-- =============================================
CREATE PROCEDURE [replenishment].[CalculateReplenishment]
	-- Add the parameters for the stored procedure here
	@date smalldatetime,
	@roundDUN14 bit = 0, 
	@generateTasksDUN14 bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    	-- Locales y bodegas que reponen
	SELECT top 10 ls.SKU, ls.Store, r.LogisticAreaId, ls.Suggest, ls.Stock, ls.Transit, 0 as PendienteDespacho
	FROM [minmax].[Replenishment] r 
	INNER JOIN products.FullProducts p ON p.LogisticAreaId =r.LogisticAreaId
	INNER JOIN series.LastStock ls ON ls.SKU = p.SKU AND ls.Store = r.Store
	WHERE r.[Date] = DATEADD(day, -1, @date) and r.[Days] > 0
		AND ((IsGeneric = 0 AND MotiveId not in (1,3,92)) OR IsGeneric = 1)
		AND p.SKU IN (SELECT SKU FROM series.StockCD WHERE Date = (SELECT MAX(date) FROM series.StockCD) and StockCD > 0)
		AND p.SKU IN (SELECT distinct SKU FROM series.LastStock WHERE Suggest > 0)
		AND ls.Suggest > 0
		
	
END
