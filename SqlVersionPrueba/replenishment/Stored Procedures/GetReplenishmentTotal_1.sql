﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 08-01-2018
-- Description:	Insertar o actualizar Nivel de Servicio y días de sobreprotección
-- =============================================

CREATE PROCEDURE [replenishment].[GetReplenishmentTotal]
@SKU       INT,
@FECHA    DATE
AS
BEGIN
SET NOCOUNT ON;

DECLARE @Cantidad INT = (
SELECT PedidoEfectivo+ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)
  FROM [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] f
  LEFT JOIN [minmax].[DistributionOrderDeltas] d ON f.sku = d.sku AND f.date = d.Date
  WHERE (f.SKU = @SKU)
  AND (f.Date = @FECHA));

SELECT ISNULL(@Cantidad,0)

END
