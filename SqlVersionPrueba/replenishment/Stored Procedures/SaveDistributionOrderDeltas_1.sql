﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-06-13
-- Description:	Se adelantan pedidos en días anteriores por sku
-- =============================================
create PROCEDURE [replenishment].[SaveDistributionOrderDeltas] 

@sku int

AS
BEGIN
 
  update a set a.SobrecargaEstacionalidad=a.SobrecargaEstacionalidad+b.SobrecargaEstacionalidad
  ,a.SobrecargaFeriado=a.SobrecargaFeriado+b.SobrecargaFeriado
  ,a.SobrecargaPromocion=a.SobrecargaPromocion+b.SobrecargaPromocion
  ,a.SobrecargaRestricción=a.SobrecargaRestricción+b.SobrecargaRestricción
  ,a.ExcesoCD=a.ExcesoCD+b.ExcesoCD
  --select b.*
  from minmax.[DistributionOrderDeltas] a
  inner join aux.[DistributionOrderDeltas] b on b.Date=a.Date and b.SKU=a.SKU
  where a.sku=@sku
  
  insert into minmax.[DistributionOrderDeltas]
  select c.*
  FROM aux.[DistributionOrderDeltas] c
  left join minmax.[DistributionOrderDeltas] b on b.sku=c.sku and b.Date=c.date
  where c.sku=@sku and b.sku is null


  delete
  from aux.[DistributionOrderDeltas] where sku=@sku

end