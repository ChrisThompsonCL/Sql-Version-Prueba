﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-05-25
-- Description:	Obtiene los skus con deficit de stock en CD para los pedidos diarios
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_DeficitCD_SKU] 

AS
BEGIN

SELECT do.[SKU]
    , Sum(do.Delta_Order)-(case when isnull(ls.stockCD,0)>= 0 then isnull(ls.stockCD,0) else 0 end) Deficit
    ,case  when isnull(pp.PetitorioMinimo,0) > 0 then 1 else 0 end PetitorioMinimo
into #deficit1
FROM replenishment.[DailyOrder] do
left join replenishment.StockCDTarde ls on ls.sku =do.SKU
left join products.FullProducts pp on pp.SKU =do.sku
where not exists (select 1 from products.GenericFamilyDetail g where g.SKU=do.SKU)
group by do.SKU, pp.PetitorioMinimo,isnull(ls.stockCD, 0)
Having Sum(do.Delta_Order)-(case when isnull(ls.stockCD,0)>= 0 then isnull(ls.stockCD,0) else 0 end)> 0

insert into #deficit1
SELECT do.[SKU]
        , Sum(do.Delta_Order)-(case when isnull(ls.stockCD,0)>= 0 then isnull(ls.stockCD,0) else 0 end) Deficit
        ,case  when isnull(pp.PetitorioMinimo,0) > 0 then 1 else 0 end PetitorioMinimo
FROM replenishment.[DailyOrder] do
inner join products.GenericFamilyDetail g on g.SKU=do.SKU
left join products.FullProducts pp on pp.SKU =do.sku
left join (
        select g.GenericFamilyId, sum(b.StockCD) stockcd
        from replenishment.StockCDTarde b
        inner join products.GenericFamilyDetail g on g.SKU= b.SKU
        group by g.GenericFamilyId
) ls on ls.GenericFamilyId=g.GenericFamilyId
group by do.SKU, pp.PetitorioMinimo,isnull(ls.stockCD, 0)
Having Sum(do.Delta_Order)-(case when isnull(ls.stockCD,0)>= 0 then isnull(ls.stockCD,0) else 0 end)> 0

select* from #deficit1

END