﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 08-01-2018
-- Description:	Insertar o actualizar Nivel de Servicio y días de sobreprotección
-- =============================================

CREATE PROCEDURE [replenishment].[GetDistributionOrders]
@DIVISION     INT,
@CATEGORIA    INT,
@SUBCATEGORIA INT,
@TOTALRANKING NVARCHAR(200),
@FLAGS        NVARCHAR(200),
@SKU		  INT,
@FECHA_I      DATE,
@FECHA_T      DATE
AS
BEGIN
SET NOCOUNT ON;

DECLARE @FECHAI_N NVARCHAR(10)= CAST(@FECHA_I AS NVARCHAR);
DECLARE @FECHAT_N NVARCHAR(10)= CAST(@FECHA_T AS NVARCHAR);

DECLARE @query NVARCHAR(MAX) = N'
SELECT 
f.Date Fecha,
CASE WHEN SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)) < 0 THEN 
SUM(PedidoEfectivo)+ SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)) ELSE SUM(PedidoEfectivo) END PedidoEfectivo,
CASE WHEN SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)) > 0 THEN
SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)) ELSE 0 END PedidoAdicional,
CASE WHEN SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0)) < 0 THEN
ABS(SUM(ISNULL(d.ExcesoCD,0)+ISNULL(d.SobrecargaEstacionalidad,0)+ISNULL(d.SobrecargaFeriado,0)+ISNULL(d.SobrecargaPromocion,0)+ISNULL(d.SobrecargaRestricción,0))) ELSE 0 END PedidoReducido
FROM [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] f
INNER JOIN products.fullproducts fp ON f.sku =fp.SKU
LEFT JOIN minmax.DistributionOrderDeltas d ON f.sku = d.SKU AND f.Date = d.Date
WHERE (fp.[SKU] = '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' or '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' is null)
AND   (fp.[DivisionUnificadaId] = '+ISNULL(CAST(@DIVISION AS varchar), 'NULL')+' or '+ISNULL(CAST(@DIVISION AS varchar), 'NULL')+' is null)
AND   (fp.[CategoriaUnificadaId]          = '+ISNULL(CAST(@CATEGORIA AS varchar), 'NULL')+' or '+ISNULL(CAST(@CATEGORIA AS varchar), 'NULL')+' is null)          
AND   (fp.[SubcategoriaUnificadaId]          = '+ISNULL(CAST(@SUBCATEGORIA AS varchar), 'NULL')+' or '+ISNULL(CAST(@SUBCATEGORIA AS varchar), 'NULL')+' is null)   
AND (f.Date BETWEEN '''+@FECHAI_N+''' and '''+@FECHAT_N+''')';

IF @TOTALRANKING is not null BEGIN
SET @query += @TOTALRANKING;
END

IF @FLAGS is not null BEGIN
SET @query += @FLAGS;
END

SET @query += 'GROUP BY f.DATE ORDER BY 1 ASC';
EXEC sp_executesql @query;


END
