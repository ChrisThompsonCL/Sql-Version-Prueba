﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-04-27
-- Description:	Se aplica eliminación de pedido, por SKu, en los casos que se tenga deficit en el CD.
-- =============================================
create PROCEDURE replenishment.[SIIRA_GenerateCuttedOrder] 

AS
BEGIN

--Reiniciar
  update d set Cutted_Order=Delta_Order
  from replenishment.[DailyOrder] d


-- Actualizar órdenes cotadas

  update a set a.Cutted_Order=case when a.Delta_Order-b.Removed_Units<0 then 0 else a.Delta_Order-b.Removed_Units end
  FROM replenishment.[DailyOrder] a
  inner join ((SELECT * FROM [aux].[DeltaAndDeficitAssignation] where Removed_Units>0)) b on b.Sku=a.SKU and b.Store=a.Store


END