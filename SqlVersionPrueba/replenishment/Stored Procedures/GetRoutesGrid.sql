﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-08-06
-- Description:	Obtiene las rutas de un producto/local
-- =============================================
CREATE PROCEDURE [replenishment].[GetRoutesGrid]
	@skus NVARCHAR(MAX),
	@stores NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @tablestore TABLE (Store INT NOT NULL PRIMARY KEY);
	IF(@stores IS NOT NULL)
	BEGIN
		INSERT INTO @tablestore
		SELECT * FROM dbo.SplitString(@stores)
	END
	

	DECLARE @tablesku TABLE (Sku INT NOT NULL PRIMARY KEY);
	IF(@skus IS NOT NULL)
	BEGIN
		INSERT INTO @tablesku
		SELECT * FROM dbo.SplitString(@skus)
	END
	


	 SELECT p.Store, p.LogisticAreaId, 
	 CAST(p.Store AS NVARCHAR(50)) + ' - ' + s.[Description] NombreLocal, pr.SKU, pr.[Description] NombreProducto, 
	 CASE WHEN[2] is null THEN 0 ELSE LT END L,
	 CASE WHEN[3] is null THEN 0 ELSE LT END M,
	 CASE WHEN[4] is null THEN 0 ELSE LT END W,
	 CASE WHEN[5] is null THEN 0 ELSE LT END J,
	 CASE WHEN[6] is null THEN 0 ELSE LT END V,
	 CASE WHEN[7] is null THEN 0 ELSE LT END S,
	 CASE WHEN[8] is null THEN 0 ELSE LT END D
	 FROM(
		SELECT LogisticAreaId, [DayOfWeek], Store, LT 
		FROM replenishment.[Routes]
	) AS r 
	 PIVOT (
		AVG([DayOfWeek]) FOR[DayOfWeek] in  ([2], [3], [4], [5], [6], [7], [8])
	 ) as p 
	 INNER JOIN stores.FullStores s on s.Store = p.Store 
	 INNER JOIN products.FullProducts pr on pr.LogisticAreaId = p.LogisticAreaId 
	 WHERE(@skus IS NULL OR pr.SKU IN(SELECT SKU FROM @tablesku)) 
	 AND (@stores IS NULL OR p.Store IN (select Store from @tablestore)) 
	 ORDER BY pr.SKU 
END
