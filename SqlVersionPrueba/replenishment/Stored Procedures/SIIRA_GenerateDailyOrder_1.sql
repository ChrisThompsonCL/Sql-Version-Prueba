﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-04-17
-- Description:	Cálculo de pedido diario
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_GenerateDailyOrder] 

AS
BEGIN

truncate table  [replenishment].[DailyOrder]


-- pedido inicial
 declare @fecha date = CAST(GETDATE() AS date)
 

 select NULL DUN14,null DUN,ls.Sku SKU
	,ls.Store  Store
	,case when 
		((case when suggest>0 then suggest else 0 end)-(case when stock>0 then stock else 0 end)- (case when transit>0 then transit else 0 end))>0 then
		((case when suggest>0 then suggest else 0 end)-(case when stock>0 then stock else 0 end)- (case when transit>0 then transit else 0 end)) else 0
	end Original_Order
	,0 Rounded_Order
	,0 Cutted_Order
	,0 Generic_Order
 INTO #Ped1
 from replenishment.stocktarde ls
 inner join [temp].[RubrosDistribucion] pp on pp.SKU=ls.SKU
 inner join ( select distinct store,Rubro from replenishment.StoreRoutesTarde r where r.DayOfWeek=DATEPART ( weekday ,dateadd(day,1,@fecha))
 ) r on r.Store=ls.Store and r.Rubro=pp.Rubro 
 where ls.Suggest>0

 -- corrección genéricos
  update a set a.Original_Order=0
  from #Ped1 a
  inner join [products].[GenericFamilyDetail] b on b.SKU=a.SKU
  left join [products].[GenericFamily] c on c.AuxSKU=a.sku
  where c.AuxSKU is null

-- incluye caja - tira
insert into [replenishment].[DailyOrder]
select NULL,NULL,x.SKU,x.Store,sum(x.Original_Order) Original_Order,0,0,0,0
from(
	SELECT case when b.SKUTira is not null then b.SKUCaja else A.SKU end SKU
	,a.Store
	,case when b.SKUTira is not null then round(a.Original_Order/b.Conversion,0) else a.Original_Order end Original_Order
	from #Ped1 a
	left join products.[CajaTira] b on b.SKUTira=a.SKU
) x
group by x.SKU,x.Store
order by x.SKU,x.Store


END

