﻿
-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-04-23
-- Description:	Se aplica los pedidos extras o recorte en los pedidos
-- =============================================
create PROCEDURE [replenishment].[SIIRA_GenerateDeltaOrder] 

AS
BEGIN

 -- reiniciar
  update d set Delta_Order=Original_Order
  from replenishment.[DailyOrder] d

-- parte con probabilidad
  update a set a.Delta_Order=case when a.Original_Order+b.DeltaAcum<0 then 0 else a.Original_Order+b.DeltaAcum end
  FROM replenishment.[DailyOrder] a
  inner join (select * from [aux].[DeltaAndDeficitAssignation] b where DeltaAcum<>0) b on b.Sku=a.SKU and b.Store=a.Store

   update d set Delta_Order=Original_Order
  from [aux].[DeltaAndDeficitAssignation] d
  
  update b set b.Delta_Order=case when b.Original_Order+b.DeltaAcum<0 then 0 else b.Original_Order+b.DeltaAcum end
  from [aux].[DeltaAndDeficitAssignation] b



END