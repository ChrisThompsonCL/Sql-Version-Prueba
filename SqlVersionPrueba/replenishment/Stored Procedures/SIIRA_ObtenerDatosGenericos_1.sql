﻿-- =============================================
-- Author:		plan z <3
-- Create date: 2018-06-08
-- Description:	f
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_ObtenerDatosGenericos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT  gf.Id FamilyId
		, sum([Cutted_Order]) TotalFamilyOrder
		,ls.sku Sku
		,case when do.sku=ls.sku then 1 else 0 end Representativo
		,case when ls.StockCD>=0 then ls.stockCD else 0 end StockCD
		,0 AssignOrder
		, sum([Cutted_Order]) PendingFamilyOrder
	from Salcobrand.[replenishment].[DailyOrder] do
	inner join Salcobrand.products.GenericFamily gf on gf.AuxSKU = do.sku
	inner join Salcobrand.products.GenericFamilyDetail gd on gd.GenericFamilyId = gf.Id
	inner join Salcobrand.replenishment.StockCDTarde ls on ls.SKU= gd.SKU
	group by  gf.Id,do.sku, ls.sku, ls.StockCD
	order by gf.Id,case when do.sku= ls.sku then 1 else 0 end desc

END
