﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-05-25
-- Description:	Obtiene los skus con delta a repartir en los pedidos
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_DeltaSKU] 

AS
BEGIN

SELECT d.SKU
    ,isnull([SobrecargaFeriado],0)+isnull([SobrecargaPromocion],0)+isnull([SobrecargaEstacionalidad],0)+isnull([SobrecargaRestricción],0)+isnull([ExcesoCD],0) Delta
    ,case  when isnull(pp.PetitorioMinimo,0) > 0 then 1 else 0 end PetitorioMinimo
    into #deltas
    from [minmax].[DistributionOrderDeltas] d
inner join(select distinct sku from replenishment.[DailyOrder]) p on p.sku = d.sku
left join products.FullProducts pp on pp.SKU= p.sku
where d.date = cast(DATEADD(dy, 1, getdate()) as date)
and isnull([SobrecargaFeriado],0)+isnull([SobrecargaPromocion],0)+isnull([SobrecargaEstacionalidad],0)+isnull([SobrecargaRestricción],0)+isnull([ExcesoCD],0)<>0

select c.AuxSKU
    , SUM(d.Delta) Delta
    ,case  when isnull(pp.PetitorioMinimo,0) > 0 then 1 else 0 end PetitorioMinimo
into #genericos
from #deltas d
inner join [products].[GenericFamilyDetail] b on d.SKU=b.SKU
inner join [products].[GenericFamily] c on c.Id=b.GenericFamilyId
left join products.FullProducts pp on pp.SKU= c.AuxSKU
group by c.AuxSKU, pp.PetitorioMinimo


select* into #deltas2 from #deltas where sku not in (select sku from [products].[GenericFamilyDetail])

insert into #deltas2
select* from #genericos

select* from #deltas2

END