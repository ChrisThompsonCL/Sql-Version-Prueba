﻿-- =============================================
-- Author:		plan z <3
-- Create date: 2018-06-08
-- Description:	f
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_ObtenerFamilias]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT distinct gf.Id FamilyId
	from Salcobrand.[replenishment].[DailyOrder] do
	inner join Salcobrand.products.GenericFamily gf on gf.AuxSKU = do.sku
	inner join Salcobrand.products.GenericFamilyDetail gd on gd.GenericFamilyId = gf.Id
	inner join Salcobrand.replenishment.StockCDTarde ls on ls.SKU = gd.SKU
	order by gf.Id

END
