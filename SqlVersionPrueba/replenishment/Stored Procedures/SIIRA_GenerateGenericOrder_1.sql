﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2018-04-27
-- Description:	Se aplica eliminación de pedido, por SKu, en los casos que se tenga deficit en el CD.
-- =============================================
CREATE PROCEDURE [replenishment].[SIIRA_GenerateGenericOrder] 

AS
BEGIN

--Reiniciar
  update d set Generic_Order=Cutted_Order
  from replenishment.[DailyOrder] d

-- hacer update después del código
update d set d.Generic_Order=g.Generic_Order
from replenishment.DailyOrder d
inner join [aux].[GenericFamilyOrderAssignation] g on g.Sku=d.sku and g.Store=d.Store


insert into salcobrand.replenishment.DailyOrder
select null,null,d.Sku,d.Store,d.Original_Order,d.Delta_Order,d.Rounded_Order,d.Cutted_Order,d.Generic_Order
from [aux].[GenericFamilyOrderAssignation] d
where not exists (select 1 from replenishment.DailyOrder g where d.Sku=g.sku and d.store=g.store)

END