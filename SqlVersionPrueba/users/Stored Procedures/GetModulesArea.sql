﻿CREATE PROCEDURE [users].[GetModulesArea]
@IDAREA INT
AS
BEGIN

SET NOCOUNT ON;

	 SELECT m.Descripcion [Text],
	  CAST(m.Id as nvarchar(10)) [Value] 
	  FROM Salcobrand.users.Modulo m
  INNER JOIN Salcobrand.users.Areas a ON m.Modulo = a.Id
  WHERE a.Id=@IDAREA and m.Descripcion!='*' --and m.Details != '' ...prueba flyway gabo

END