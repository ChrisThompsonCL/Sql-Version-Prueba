﻿-- =============================================
-- Author:		<Carlos Astroza>
-- Create date: <06-02-2018>
-- Description:	<SubModulos por usuario>
-- =============================================
CREATE PROCEDURE [users].[ObtenerModulosUsuario]
@username nvarchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
	mo.Descripcion as Area,
	sm.Descripcion as Controller,
	
	up.PermiteEdicion as PermiteEdicion,
	up.PermiteEliminar as PermiteEliminar
	FROM users.UsersPermission up
	INNER JOIN users.Modulo sm on up.Modulo = sm.Id
	INNER JOIN users.Areas mo on sm.Modulo = mo.Id
	WHERE up.Username = @username
	AND up.Active = 1
	

END
