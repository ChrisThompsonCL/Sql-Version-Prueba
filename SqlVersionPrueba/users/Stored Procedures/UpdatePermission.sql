﻿-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	Modifica los compradores de la tabla UserRules. // 4
-- =============================================
--EXEC [users].[UpdatePermission] 'aotero',4
CREATE PROCEDURE [users].[UpdatePermission]
@username NVARCHAR(MAX),
@Modulo int,
@tipo int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PermisoEdicion bit = (SELECT PermiteEdicion FROM users.UsersPermission 
	WHERE Username=@username and Modulo=@Modulo),
			@PermisoEliminar bit = (SELECT PermiteEliminar FROM users.UsersPermission
	WHERE Username=@username and Modulo=@Modulo)
	
	-- Permiso Edicion
	IF @tipo=1  BEGIN
		If @PermisoEdicion = 0 BEGIN
		UPDATE users.UsersPermission
			SET PermiteEdicion=1
				WHERE Username=@username and Modulo=@Modulo
		END
		If @PermisoEdicion = 1 BEGIN
		UPDATE users.UsersPermission
			SET PermiteEdicion=0
				WHERE Username=@username and Modulo=@Modulo
		END
	END
	IF @tipo=2 BEGIN
		If @PermisoEliminar = 0 BEGIN
		UPDATE users.UsersPermission
			SET PermiteEliminar=1
				WHERE Username=@username and Modulo=@Modulo
		END
		If @PermisoEliminar = 1 BEGIN
		UPDATE users.UsersPermission
			SET PermiteEliminar=0
				WHERE Username=@username and Modulo=@Modulo
		END
	END
END