﻿CREATE PROCEDURE [users].[CreateModuleUsers]
@Username NVARCHAR(30),
@PermiteEdicion bit,
@PermiteEliminar bit,
@Modulo INT
AS
BEGIN

SET NOCOUNT ON;

		DECLARE @TABLA AS TABLE(
		Usuario NVARCHAR(50),
		PermiteEdicion bit,
		PermiteEliminar bit,
		Modulo int
		)
		DECLARE @area INT = (SELECT [Modulo] from users.Modulo where Id=@Modulo)

		INSERT INTO @TABLA VALUES(@Username,@PermiteEdicion,@PermiteEliminar,@Modulo)

		-- SI INGRESO UN MODULO QUE SEA DEL AREA COMPRA AGREGAR EL MODULO PRONOSTICO
		IF @area = 4 BEGIN
		
			IF @Modulo!=20 BEGIN
				INSERT INTO @TABLA VALUES(@Username,@PermiteEdicion,@PermiteEliminar,20)
			END
			IF @Modulo!=1 BEGIN
				INSERT INTO @TABLA VALUES(@Username,@PermiteEdicion,@PermiteEliminar,1)
			END
		END
		
		MERGE INTO users.UsersPermission as up
		USING @Tabla as s
		ON up.Username = s.Usuario AND up.Modulo = s.Modulo		
		WHEN MATCHED THEN 
		UPDATE SET up.PermiteEdicion = s.PermiteEdicion, up.PermiteEliminar = s.PermiteEliminar
		WHEN NOT MATCHED THEN
		INSERT VALUES(@Username,s.Modulo,@PermiteEdicion,@PermiteEliminar,1);
		SELECT 1;

END