﻿CREATE PROCEDURE [users].[GetUsers]
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	. Prueba para versionar en Flyway// 4
-- =============================================

AS
BEGIN

SET NOCOUNT ON;

	 SELECT DISTINCT Username [TEXT],
	  Username [Value] FROM Salcobrand.users.UsersPermission

END