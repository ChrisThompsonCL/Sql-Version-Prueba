﻿CREATE PROCEDURE [users].[CopyPermissionUsers]
@Username NVARCHAR(30),
@UsernameDes NVARCHAR(30)
AS
BEGIN

SET NOCOUNT ON;


	DECLARE @tableOrigen TABLE(Modulo INT, PermiteEdicion bit, PermiteEliminar bit,active bit)
	
	INSERT INTO @tableOrigen
	 SELECT Modulo, PermiteEdicion,PermiteEliminar,Active
	 FROM Salcobrand.users.UsersPermission 
	 WHERE Username=@username

	DELETE FROM users.UsersPermission where Username=@usernameDes

		MERGE INTO Salcobrand.users.UsersPermission as td
		USING @tableOrigen as o
		ON td.Modulo = o.Modulo and td.Username = @UsernameDes
		WHEN MATCHED THEN 
		UPDATE SET td.PermiteEdicion = o.PermiteEdicion
		WHEN NOT MATCHED THEN
		INSERT VALUES(@UsernameDes,o.Modulo,o.PermiteEdicion,o.PermiteEliminar,1);

END