﻿
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 09-07-2018
-- Description:	Obtiene la información de los módulos de usuarios. Este procedimiento es utilizado para una
-- grilla especifica en el mantenedor de usuarios. 
-- =============================================
--exec users.GetModulesUsers 'cthompson'
CREATE PROCEDURE [users].[GetModulesUsers]
@username nvarchar(50)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
	--CASE WHEN sm.Descripcion = '*' THEN mo.Descripcion+'.*' ELSE mo.Descripcion END as Area,
	
	mo.Descripcion as Area,
	sm.Descripcion as Controller,
	sm.Details as Details,
	up.PermiteEdicion as PermiteEdicion,
	up.PermiteEliminar as PermiteEliminar,
	sm.Id as Modulo,
	up.Username as Username
	FROM users.UsersPermission up
	INNER JOIN users.Modulo sm on up.Modulo = sm.Id
	INNER JOIN users.Areas mo on sm.Modulo = mo.Id
	WHERE up.Username = @username
	AND up.Active = 1 AND sm.Id!=12
	

END
