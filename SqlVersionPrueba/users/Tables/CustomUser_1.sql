﻿CREATE TABLE [users].[CustomUser] (
    [Id]            INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PricingUserId] INT NOT NULL,
    [CustomRoleId]  INT NOT NULL,
    CONSTRAINT [PK_CustomUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);

