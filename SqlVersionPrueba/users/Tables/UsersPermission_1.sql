﻿CREATE TABLE [users].[UsersPermission] (
    [Username]        NVARCHAR (50) NULL,
    [Modulo]          INT           NULL,
    [PermiteEdicion]  BIT           NULL,
    [PermiteEliminar] BIT           NULL,
    [Active]          BIT           NULL
);

