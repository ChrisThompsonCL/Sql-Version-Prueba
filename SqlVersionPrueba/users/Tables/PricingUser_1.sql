﻿CREATE TABLE [users].[PricingUser] (
    [Id]            BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Username]      NVARCHAR (255) NULL,
    [Password]      NVARCHAR (255) NULL,
    [Name]          NVARCHAR (255) NULL,
    [PricingRoleId] BIGINT         NULL,
    CONSTRAINT [PK_PricingUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);

