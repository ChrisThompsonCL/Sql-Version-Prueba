﻿CREATE TABLE [users].[LogException] (
    [Id]         BIGINT     IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Date]       DATETIME   NULL,
    [Message]    TEXT       NULL,
    [StackTrace] TEXT       NULL,
    [WebService] NCHAR (80) NULL,
    [UserName]   NCHAR (50) NULL,
    [UserId]     BIGINT     NULL
);

