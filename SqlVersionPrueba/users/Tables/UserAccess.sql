﻿CREATE TABLE [users].[UserAccess] (
    [UserId]        BIGINT NOT NULL,
    [HierarchyId]   INT    NOT NULL,
    [IsDemandGroup] BIT    NOT NULL,
    CONSTRAINT [PK_UserAccess] PRIMARY KEY CLUSTERED ([UserId] ASC, [HierarchyId] ASC, [IsDemandGroup] ASC),
    CONSTRAINT [FK_UserAccess_PricingUser] FOREIGN KEY ([UserId]) REFERENCES [users].[PricingUser] ([Id]) ON DELETE CASCADE
);

