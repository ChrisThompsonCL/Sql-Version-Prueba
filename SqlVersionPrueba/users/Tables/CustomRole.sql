﻿CREATE TABLE [users].[CustomRole] (
    [Id]          BIGINT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (50) NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_CustomRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);

