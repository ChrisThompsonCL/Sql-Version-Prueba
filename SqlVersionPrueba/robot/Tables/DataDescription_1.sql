﻿CREATE TABLE [robot].[DataDescription] (
    [Id_File]          INT           NOT NULL,
    [Name]             NVARCHAR (50) NOT NULL,
    [Filename]         NVARCHAR (50) NOT NULL,
    [FilenameNight]    NVARCHAR (50) NULL,
    [Wildcard]         NVARCHAR (50) NULL,
    [Schedule]         NVARCHAR (50) NOT NULL,
    [Enabled]          BIT           NOT NULL,
    [DayEnabled]       BIT           NOT NULL,
    [AfternoonEnabled] BIT           NOT NULL,
    [NightEnabled]     BIT           NOT NULL,
    [HalfHourEnabled]  BIT           NULL,
    [ProcessingOrder]  INT           NULL,
    [FileType]         NVARCHAR (50) NULL,
    [HasHeader]        BIT           NULL,
    CONSTRAINT [PK_DataDescription] PRIMARY KEY CLUSTERED ([Id_File] ASC)
);

