﻿CREATE TABLE [robot].[ProcLog] (
    [Date]           SMALLDATETIME NOT NULL,
    [Id_Proc]        INT           NOT NULL,
    [Successful]     BIT           NOT NULL,
    [LastDataUpdate] SMALLDATETIME NULL,
    CONSTRAINT [PK_Robot_ProcLog] PRIMARY KEY CLUSTERED ([Date] ASC, [Id_Proc] ASC),
    CONSTRAINT [FK_ProcLog_ProcDescription] FOREIGN KEY ([Id_Proc]) REFERENCES [robot].[ProcDescription] ([Id_Proc]) ON DELETE CASCADE
);

