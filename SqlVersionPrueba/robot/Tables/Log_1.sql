﻿CREATE TABLE [robot].[Log] (
    [Date]             SMALLDATETIME NOT NULL,
    [Id_File]          INT           NOT NULL,
    [Period_StartDate] SMALLDATETIME NOT NULL,
    [Period_EndDate]   SMALLDATETIME NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Log]
    ON [robot].[Log]([Date] ASC);

