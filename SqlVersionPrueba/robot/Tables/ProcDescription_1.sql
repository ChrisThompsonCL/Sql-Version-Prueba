﻿CREATE TABLE [robot].[ProcDescription] (
    [Id_Proc]     INT           NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Schedule]    VARCHAR (20)  NULL,
    [Last_Update] SMALLDATETIME NULL,
    CONSTRAINT [PK_Robot_ProcDescription] PRIMARY KEY CLUSTERED ([Id_Proc] ASC)
);

