﻿









CREATE VIEW [view].[SuggestRestrictionMatrix]

AS
SELECT        ls.SKU, ls.Store, fp.TotalRanking, ls.Suggest, ls.IsBloqued
	, minimoProd.Minimo MinimoProd_Restricción, minimoProd.Fecha MinimoProd_FechaCarga, minimoProd.Archivo MinimoProd_Archivo, minimoProd.Usuario MinimoProd_Usuario
	, minimoComb.Minimo MinimoComb_Restricción, minimoComb.Fecha MinimoComb_FechaCarga, minimoComb.BeginDate MinimoComb_Inicio, minimoComb.EndDate MinimoComb_Término, minimoComb.Archivo MinimoComb_Archivo, minimoComb.Usuario MinimoComb_Usuario
	, fijo.Fijo CargaManual_Restricción, fijo.Fecha CargaManual_FechaCarga, fijo.BeginDate CargaManual_Inicio, fijo.EndDate CargaManual_Término, fijo.Archivo CargaManual_Archivo, fijo.UserName CargaManual_Usuario
	, planograma.Planograma Planograma_Restricción, planograma.Fecha Planograma_FechaCarga, planograma.Archivo Planograma_Archivo, planograma.UserName Planograma_Usuario
	, dun.MedioDUN DUN_Restricción, dun.Fecha DUN_FechaCarga, dun.Archivo DUN_Archivo, dun.UserName DUN_Usuario
	, ups.SuggestMax Sugerido_SinRestricción, ups.[Date] Sugerido_SinRestricción_Fecha , ups.HolidayIncrease Sugerido_SinRestricción_PorAlzaFeriado
	, maximoProd.Maximo MaximoProd_Restricción, maximoProd.Fecha MaximoProd_FechaCarga, maximoProd.Archivo MaximoProd_Archivo, maximoProd.Usuario MaximoProd_Usuario
	, maximoComb.Maximo MaximoComb_Restricción, maximoComb.Fecha MaximoComb_FechaCarga, maximoComb.BeginDate MaximoComb_Inicio, maximoComb.EndDate MaximoComb_Término, maximoComb.Archivo MaximoComb_Archivo, maximoComb.Usuario MaximoComb_Usuario
	, cajatira.CajaTira CajaTira_Restricción, cajatira.Fecha CajaTira_FechaCarga, cajatira.Archivo CajaTira_Archivo, cajatira.UserName CajaTira_Usuario
	, FijoEstricto.FijoEstricto FijoEstricto_Restricción, FijoEstricto.Fecha FijoEstrictoFechaCarga, FijoEstricto.BeginDate FijoEstricto_Inicio, FijoEstricto.EndDate FijoEstricto_Término, FijoEstricto.Archivo FijoEstricto_Archivo, FijoEstricto.Usuario FijoEstricto_Usuario
FROM series.LastStock ls 
INNER JOIN products.FullProducts fp ON fp.sku = ls.sku 
--INNER JOIN aux.DailySuggestMix dsm ON dsm.SKU = ls.SKU AND dsm.store = ls.Store 
LEFT JOIN [minmax].UnrestrictedPricingSuggest ups ON ls.SKU = ups.SKU and ls.Store = ups.Store
LEFT JOIN
(
	/*MINIMO PRODUCTO*/ 
	SELECT SKU, Store, Minimo, Fecha
		, CASE WHEN SuggestRestrictionTypeID = 8 THEN 'Restricción de Sistema' ELSE srf.[FileName] END Archivo
		, BeginDate, EndDate
		, CASE WHEN SuggestRestrictionTypeID = 8 THEN 'Sistema' ELSE srf.UserName END Usuario
    FROM (
		SELECT ls.SKU, ls.Store, srd.SuggestRestrictionFileID, srd.SuggestRestrictionTypeID, srd.Detail Minimo
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, ls.Store ORDER BY Detail DESC, cast(InsertionDate AS smalldatetime) DESC) R
		FROM [view].[SuggestRestrictionDetail] srd 
		INNER JOIN series.LastStock ls ON ls.SKU = srd.ProductId
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (3, 8, 12)
	) minimo 
	LEFT JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = minimo.SuggestRestrictionFileID
	WHERE R = 1 
) minimoProd ON minimoProd.SKU = ls.SKU AND minimoProd.Store = ls.Store 
LEFT JOIN
(
	/*MINIMO COMB*/ 
	SELECT SKU, Store, Minimo, Fecha
		, CASE WHEN SuggestRestrictionTypeID = 12 THEN 'Restricción de Sistema' ELSE srf.[FileName] END Archivo
		, BeginDate, EndDate
		, CASE WHEN SuggestRestrictionTypeID = 12 THEN 'Sistema' ELSE srf.UserName END Usuario
    FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store, srd.SuggestRestrictionFileID, srd.SuggestRestrictionTypeID, srd.Detail Minimo
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, srd.StoreID ORDER BY Detail DESC, cast(InsertionDate AS smalldatetime) DESC) R
		FROM [view].[SuggestRestrictionDetail] srd
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (4)
	) minimo 
	LEFT JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = minimo.SuggestRestrictionFileID
	WHERE R = 1
) minimoComb ON minimoComb.SKU = ls.SKU AND minimoComb.Store = ls.Store 
LEFT JOIN
(
	/*FIJO*/ 
	SELECT SKU, Store, Fijo, Fecha, srf.[FileName] Archivo, srf.UserName, BeginDate, EndDate
    FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store, SuggestRestrictionFileID, srd.Detail Fijo
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, srd.StoreID ORDER BY cast(InsertionDate AS smalldatetime) DESC, Detail DESC) R
		FROM [view].[SuggestRestrictionDetail] srd
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (2)
	) fijo 
	INNER JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = fijo.SuggestRestrictionFileID
	WHERE R = 1
) fijo ON fijo.SKU = ls.SKU AND fijo.Store = ls.Store 
LEFT JOIN
(
	/*PLANOGRAMA*/ 
	SELECT SKU, Store, Planograma, Fecha, srf.[FileName] Archivo, srf.UserName, BeginDate, EndDate
	FROM (
		SELECT        srd.ProductID SKU, srd.StoreID Store, SuggestRestrictionFileID, srd.Detail Planograma
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, srd.StoreID ORDER BY Detail DESC, cast(InsertionDate AS smalldatetime) DESC) R
		FROM [view].[SuggestRestrictionDetail] srd
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (9, 10, 11)
	) planograma 
	INNER JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = planograma.SuggestRestrictionFileID
	WHERE  R = 1
) planograma ON planograma.SKU = ls.SKU AND planograma.Store = ls.Store 
LEFT JOIN
(
	/*DUN*/ 
	SELECT SKU, Store, MedioDUN, Fecha, srf.[FileName] Archivo, srf.UserName, BeginDate, EndDate
	FROM (
		SELECT ls.SKU, ls.Store, SuggestRestrictionFileID, CEILING(Detail * 1.0 / 2.0) + 1 MedioDUN
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY ls.SKU, ls.Store ORDER BY cast(InsertionDate AS smalldatetime) DESC, Detail DESC) R
		FROM  [view].[SuggestRestrictionDetail] srd 
		INNER JOIN series.LastStock ls ON ls.SKU = srd.ProductId AND ((srd.StoreType = 0 AND srd.StoreID = ls.Store) OR (srd.StoreType = 3))
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (5, 6, 7)
	) dun 
	INNER JOIN 	br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = dun.SuggestRestrictionFileID
	WHERE R = 1
) dun ON dun.SKU = ls.SKU AND dun.Store = ls.Store
LEFT JOIN (
	/*MAXIMO PRODUCTO*/ 
	SELECT SKU, Store, Maximo, Fecha , srf.[FileName] Archivo, BeginDate, EndDate, srf.UserName Usuario
    FROM (
		SELECT ls.SKU, ls.Store, srd.SuggestRestrictionFileID, srd.SuggestRestrictionTypeID, srd.Detail Maximo
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, ls.Store ORDER BY Detail ASC, cast(InsertionDate AS smalldatetime) DESC) R
		FROM [view].[SuggestRestrictionDetail] srd 
		INNER JOIN series.LastStock ls ON ls.SKU = srd.ProductId
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (13)
	) maximoProd 
	LEFT JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = maximoProd.SuggestRestrictionFileID
	WHERE R = 1 
) maximoProd ON maximoProd.SKU = ls.SKU AND maximoProd.Store = ls.Store 
LEFT JOIN
(
	/*MAXIMO COMB*/ 
	SELECT SKU, Store, Maximo, Fecha
		, srf.[FileName] Archivo
		, BeginDate, EndDate
		, srf.UserName Usuario
    FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store, srd.SuggestRestrictionFileID, srd.SuggestRestrictionTypeID, srd.Detail Maximo
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, srd.StoreID ORDER BY Detail ASC, cast(InsertionDate AS smalldatetime) DESC) R
		FROM [view].[SuggestRestrictionDetail] srd
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (14)
	) maximoComb 
	LEFT JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = maximoComb.SuggestRestrictionFileID
	WHERE R = 1
) maximoComb ON maximoComb.SKU = ls.SKU AND maximoComb.Store = ls.Store 
LEFT JOIN
(
	/*CAJA TIRA*/ 
	SELECT SKU, Store, CajaTira, Fecha, srf.[FileName] Archivo, srf.UserName, BeginDate, EndDate
	FROM (
		SELECT ls.SKU, ls.Store, SuggestRestrictionFileID, Detail CajaTira
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY ls.SKU, ls.Store ORDER BY cast(InsertionDate AS smalldatetime) DESC, Detail DESC) R
		FROM  [view].[SuggestRestrictionDetail] srd 
		INNER JOIN series.LastStock ls ON ls.SKU = srd.ProductId
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (15)
	) cajatira 
	INNER JOIN 	br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = cajatira.SuggestRestrictionFileID
	WHERE R = 1
) cajatira ON cajatira.SKU = ls.SKU AND cajatira.Store = ls.Store
LEFT JOIN
(
	/*FIJO COMB ESTRICTO*/ 
	SELECT SKU, Store, FijoEstricto, Fecha, srf.[FileName] Archivo, BeginDate, EndDate, srf.UserName Usuario
    FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store, srd.SuggestRestrictionFileID, srd.SuggestRestrictionTypeID, srd.Detail FijoEstricto
			, cast(InsertionDate AS smalldatetime) Fecha, srd.BeginDate, srd.EndDate
			, ROW_NUMBER() OVER (partition BY srd.ProductID, srd.StoreID ORDER BY cast(InsertionDate AS smalldatetime) DESC, Detail DESC) R
		FROM [view].[SuggestRestrictionDetail] srd
		WHERE cast(GETDATE() AS date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') AND srd.SuggestRestrictionTypeID IN (16)
	) fijoestricto 
	LEFT JOIN br.SuggestRestrictionFile srf ON srf.SuggestRestrictionFileID = fijoestricto.SuggestRestrictionFileID
	WHERE R = 1
) fijoestricto ON fijoestricto.SKU = ls.SKU AND fijoestricto.Store = ls.Store 
WHERE minimoProd.Minimo IS NOT NULL OR
    minimoComb.Minimo IS NOT NULL OR
    fijo.Fijo IS NOT NULL OR
    planograma.Planograma IS NOT NULL OR
    dun.MedioDUN IS NOT NULL OR
	maximoProd.Maximo IS NOT NULL OR
	maximoComb.Maximo IS NOT NULL OR
	cajatira.CajaTira IS NOT NULL OR
	fijoestricto.FijoEstricto IS NOT NULL







GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'view', @level1type = N'VIEW', @level1name = N'SuggestRestrictionMatrix';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'view', @level1type = N'VIEW', @level1name = N'SuggestRestrictionMatrix';

