﻿






CREATE VIEW [view].[SuggestRestrictionToday]
AS
SELECT ls.SKU, ls.Store, max(ls.Suggest) Suggest, max(minimo.Minimo) Minimo, max(fijo.Fijo) Fijo, max(planograma.Planograma) Planograma, Max(dun.MedioDUN) MedioDUN, min(Maximo)Maximo, max(Cajatira)CajaTira, max(FijoEstricto)FijoEstricto
FROM series.LastStock ls
LEFT JOIN (
	/*MINIMO*/
	SELECT ls.SKU, ls.Store
		, MAX(case when srd.SuggestRestrictionTypeID in (3,4,8,12) then srd.Detail else null end) Minimo
	FROM [view].[SuggestRestrictionDetail] srd
	INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
	WHERE cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
		and srd.SuggestRestrictionTypeID in (3,4,8,12)
	GROUP BY ls.SKU, ls.Store
) minimo on minimo.SKU=ls.SKU and minimo.Store=ls.Store
LEFT JOIN (
	/*FIJO*/
	SELECt SKU, Store, Fijo
	FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store
			, case when srd.SuggestRestrictionTypeID=2 then srd.Detail else null end Fijo
			, ROW_NUMBER() over (partition by srd.ProductID , srd.StoreID order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
		FROM [view].[SuggestRestrictionDetail] srd
		where cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (2)
	) fijo WHERE R=1
) fijo ON fijo.SKU=ls.SKU and fijo.Store=ls.Store
LEFT JOIN (
	/*PLANOGRAMA*/
	SELECT srd.ProductID SKU, srd.StoreID Store
		, MAX(case when srd.SuggestRestrictionTypeID in (9,10,11) then srd.Detail else null end) Planograma
	FROM [view].[SuggestRestrictionDetail] srd
	where cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
		and srd.SuggestRestrictionTypeID in (9,10,11)
	group by srd.ProductID , srd.StoreID
) planograma on planograma.SKU=ls.SKU and planograma.Store=ls.Store
LEFT JOIN (
	/*DUN*/
	SELECT SKU, Store, MedioDUN
	FROM (
		SELECT ls.SKU, ls.Store
			, case when srd.SuggestRestrictionTypeID in (5,6,7) then CEILING(Detail*1.0/2.0)+1 else null end MedioDUN
			, ROW_NUMBER() over (partition by ls.SKU, ls.Store order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
		FROM [view].[SuggestRestrictionDetail] srd
		INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
		WHERE cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (5,6,7)
	) dun WHERE R=1
) dun on dun.SKU=ls.SKU and dun.Store=ls.Store
LEFT JOIN (
	/*MAXIMO*/
	SELECT ls.SKU, ls.Store
		, MIN(case when srd.SuggestRestrictionTypeID in (13,14) then srd.Detail else null end) Maximo
	FROM [view].[SuggestRestrictionDetail] srd
	INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
	WHERE cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
		and srd.SuggestRestrictionTypeID in (13,14)
	GROUP BY ls.SKU, ls.Store
) maximo on maximo.SKU=ls.SKU and maximo.Store=ls.Store
LEFT JOIN (
	/*CajaTira*/
		SELECT ls.SKU, ls.Store
			, Detail CajaTira
		FROM [view].[SuggestRestrictionDetail] srd
		INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId
		WHERE cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (15)
) cajatira on cajatira.SKU=ls.SKU and cajatira.Store=ls.Store
LEFT JOIN (
	/*FIJO ESTRICTO*/
	SELECt SKU, Store, FijoEstricto
	FROM (
		SELECT srd.ProductID SKU, srd.StoreID Store
			, case when srd.SuggestRestrictionTypeID=16 then srd.Detail else null end FijoEstricto
			, ROW_NUMBER() over (partition by srd.ProductID , srd.StoreID order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
		FROM [view].[SuggestRestrictionDetail] srd
		where cast(GETDATE() as date) BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (16)
	) FijoEstricto WHERE R=1
) FijoEstricto on FijoEstricto.SKU=ls.SKU and FijoEstricto.Store=ls.Store
WHERE minimo.Minimo is not null OR fijo.Fijo is not null OR planograma.Planograma is not null or dun.MedioDUN is not null or Maximo is not null or CajaTira is not null or FijoEstricto is not null
group by ls.SKU, ls.Store






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[30] 4[16] 2[36] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'view', @level1type = N'VIEW', @level1name = N'SuggestRestrictionToday';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'view', @level1type = N'VIEW', @level1name = N'SuggestRestrictionToday';

