﻿CREATE TABLE [suggest].[SuggestDownload] (
    [SKU]          INT        NOT NULL,
    [Store]        INT        NOT NULL,
    [Max]          INT        NULL,
    [Min]          INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [Days]         INT        NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [Minimo]       INT        NULL,
    [Fijo]         INT        NULL,
    [Planograma]   INT        NULL,
    [MedioDUN]     INT        NULL,
    [Maximo]       INT        NULL,
    [CajaTira]     INT        NULL,
    [FijoEstricto] INT        NULL,
    CONSTRAINT [PK_SuggestDownload] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

