﻿CREATE TABLE [suggest].[SuggestDownloadHistory] (
    [SKU]          INT           NOT NULL,
    [Store]        INT           NOT NULL,
    [Max]          INT           NULL,
    [Min]          INT           NULL,
    [Rate]         FLOAT (53)    NULL,
    [Days]         INT           NULL,
    [Date]         SMALLDATETIME NOT NULL,
    [LeadTime]     INT           NULL,
    [SafetyDays]   INT           NULL,
    [ServiceLevel] FLOAT (53)    NULL,
    [Minimo]       INT           NULL,
    [Fijo]         INT           NULL,
    [Planograma]   INT           NULL,
    [MedioDUN]     INT           NULL,
    [Maximo]       INT           NULL,
    [CajaTira]     INT           NULL,
    [FijoEstricto] INT           NULL,
    CONSTRAINT [PK_SuggestDownloadHistory] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestDownloadHistory]
    ON [suggest].[SuggestDownloadHistory]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestDownloadHistory_1]
    ON [suggest].[SuggestDownloadHistory]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestDownloadHistory_2]
    ON [suggest].[SuggestDownloadHistory]([Date] ASC)
    INCLUDE([SKU], [Store], [Max], [Min]);

