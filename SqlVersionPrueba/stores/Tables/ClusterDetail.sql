﻿CREATE TABLE [stores].[ClusterDetail] (
    [Id]      BIGINT NOT NULL,
    [StoreId] INT    NOT NULL,
    CONSTRAINT [PK_ClusterDetail] PRIMARY KEY CLUSTERED ([Id] ASC, [StoreId] ASC),
    CONSTRAINT [FK_ClusterDetail_Cluster] FOREIGN KEY ([Id]) REFERENCES [stores].[Cluster] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ClusterDetail]
    ON [stores].[ClusterDetail]([Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClusterDetail_1]
    ON [stores].[ClusterDetail]([StoreId] ASC);

