﻿CREATE TABLE [stores].[Cluster] (
    [Id]       BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]     VARCHAR (255) NULL,
    [ClientId] VARCHAR (50)  NULL,
    [Level]    INT           NULL,
    [ParentId] INT           NULL,
    CONSTRAINT [PK_Cluster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

