﻿CREATE TABLE [stores].[StoreFilterDetail] (
    [ColumnName]   VARCHAR (50) NOT NULL,
    [DisplayType]  INT          NULL,
    [SpecialValue] VARCHAR (50) NULL,
    CONSTRAINT [PK_StoreFilterDetail] PRIMARY KEY CLUSTERED ([ColumnName] ASC)
);

