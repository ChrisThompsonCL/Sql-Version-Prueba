﻿CREATE TABLE [stores].[NewStores2] (
    [NewStore]           INT            NOT NULL,
    [MirrorStoreFarma]   INT            NOT NULL,
    [ShareFarma]         FLOAT (53)     NOT NULL,
    [MirrorStoreConsumo] NCHAR (10)     NOT NULL,
    [ShareConsumo]       FLOAT (53)     NOT NULL,
    [MirrorStoreDermo]   NCHAR (10)     NOT NULL,
    [ShareDermo]         FLOAT (53)     NOT NULL,
    [BeginDate]          DATE           NOT NULL,
    [UpdateDate]         DATETIME       NULL,
    [Usuario]            NVARCHAR (100) NULL,
    CONSTRAINT [PK_NewStores2] PRIMARY KEY CLUSTERED ([NewStore] ASC)
);

