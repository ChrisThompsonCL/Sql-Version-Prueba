﻿CREATE TABLE [class].[ProductClassificationBest] (
    [SKU]                  INT        NOT NULL,
    [Cluster]              INT        NOT NULL,
    [ClassificationTypeId] INT        NOT NULL,
    [Measure]              FLOAT (53) NULL,
    [Metric]               FLOAT (53) NULL,
    [Acum]                 FLOAT (53) NULL,
    [Elasticity]           FLOAT (53) NULL,
    CONSTRAINT [PK_ProductClassificationBest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Cluster] ASC, [ClassificationTypeId] ASC),
    CONSTRAINT [FK_ProductClassificationBest_ClassificationType] FOREIGN KEY ([ClassificationTypeId]) REFERENCES [class].[ClassificationType] ([Id])
);

