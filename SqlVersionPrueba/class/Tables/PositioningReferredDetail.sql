﻿CREATE TABLE [class].[PositioningReferredDetail] (
    [PosReferredId]  INT        NOT NULL,
    [PosReferenceId] INT        NOT NULL,
    [Weight]         FLOAT (53) NOT NULL,
    CONSTRAINT [PK_PositioningReferredDetail_1] PRIMARY KEY CLUSTERED ([PosReferredId] ASC, [PosReferenceId] ASC),
    CONSTRAINT [FK_PositioningReferredDetail_PositioningReference1] FOREIGN KEY ([PosReferenceId]) REFERENCES [class].[PositioningReference] ([Id])
);

