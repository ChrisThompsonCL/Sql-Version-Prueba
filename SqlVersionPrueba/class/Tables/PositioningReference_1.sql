﻿CREATE TABLE [class].[PositioningReference] (
    [Id]              INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [ReferenceTypeId] INT           NOT NULL,
    CONSTRAINT [PK_PositioningReference] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PositioningReference_PositioningReferenceType] FOREIGN KEY ([ReferenceTypeId]) REFERENCES [class].[PositioningReferenceType] ([Id])
);

