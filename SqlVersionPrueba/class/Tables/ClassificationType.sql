﻿CREATE TABLE [class].[ClassificationType] (
    [Id]   INT          NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ClassificationType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

