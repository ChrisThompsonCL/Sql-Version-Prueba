﻿CREATE TABLE [class].[PositioningReferenceType] (
    [Id]          INT          NOT NULL,
    [Name]        VARCHAR (50) NOT NULL,
    [GroupTypeId] INT          NOT NULL,
    CONSTRAINT [PK_PositioningReferenceType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PositioningReferenceType_PositioningReferenceGroupType] FOREIGN KEY ([GroupTypeId]) REFERENCES [class].[PositioningReferenceGroupType] ([Id]),
    CONSTRAINT [IX_PositioningReferenceType] UNIQUE NONCLUSTERED ([Id] ASC)
);

