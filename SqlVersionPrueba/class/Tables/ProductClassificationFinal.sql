﻿CREATE TABLE [class].[ProductClassificationFinal] (
    [SKU]                  INT NOT NULL,
    [Cluster]              INT NOT NULL,
    [ClassificationTypeId] INT NOT NULL,
    CONSTRAINT [PK_ProductClassificationFinal] PRIMARY KEY CLUSTERED ([SKU] ASC, [Cluster] ASC, [ClassificationTypeId] ASC),
    CONSTRAINT [FK_ProductClassificationFinal_ClassificationType] FOREIGN KEY ([ClassificationTypeId]) REFERENCES [class].[ClassificationType] ([Id])
);

