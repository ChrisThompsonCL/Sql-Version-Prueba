﻿CREATE TABLE [class].[Positioning] (
    [Id]                    INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ClassificationId]      INT        NOT NULL,
    [PositioningTypeId]     INT        NULL,
    [MinReferredId]         INT        NULL,
    [MinBound]              FLOAT (53) NULL,
    [MaxReferredId]         INT        NULL,
    [MaxBound]              FLOAT (53) NULL,
    [CurrentAvgPositioning] FLOAT (53) NULL,
    CONSTRAINT [PK_Positioning] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Positioning_Classification] FOREIGN KEY ([ClassificationId]) REFERENCES [trash].[Classification] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UKMax_Positioning] UNIQUE NONCLUSTERED ([MaxReferredId] ASC),
    CONSTRAINT [UKMin_Positioning] UNIQUE NONCLUSTERED ([MinReferredId] ASC)
);

