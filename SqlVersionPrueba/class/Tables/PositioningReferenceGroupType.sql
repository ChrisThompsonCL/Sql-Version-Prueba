﻿CREATE TABLE [class].[PositioningReferenceGroupType] (
    [Id]   INT          NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PositioningReferenceGroupType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

