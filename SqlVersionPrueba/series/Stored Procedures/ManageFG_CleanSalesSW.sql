﻿-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2014/10/30
-- Description:	Revisa las particiones del tipo FG_SalesSW que ya no están mapeadas al esquema PS_Sales y las elimina del disco.
-- =============================================
CREATE PROCEDURE [series].[ManageFG_CleanSalesSW] 
AS
BEGIN
	SET NOCOUNT ON;
		
	declare @msg varchar(255)			
	declare @year int	
	declare @month int 	
	declare @rDate date = NULL		
	
	Declare @rows CURSOR 
	SET @rows = CURSOR FAST_FORWARD
	FOR
		select substring(fgs.name,len('PF_SalesSW')+1,4) [year], substring(fgs.name,len('PF_SalesSW')+1+4,2) [month] 
		from  
		(  
			SELECT sh.*, prv.value,ROW_NUMBER() OVER (ORDER BY prv.value) rowId
			FROM sys.partition_schemes sh 
			inner join sys.partition_functions AS pf on sh.function_id =pf.function_id 
			left JOIN sys.partition_range_values AS prv ON prv.function_id = pf.function_id
			WHERE pf.name = 'PF_SalesSW'
		) b
		inner join sys.data_spaces sp on sp.data_space_id = b.data_space_id
		inner join sys.destination_data_spaces mix on sp.data_space_id = mix.partition_scheme_id and rowId=mix.destination_id
		inner join sys.filegroups fg on fg.data_space_id = mix.data_space_id
		right outer join (
		select * from sys.filegroups fg where fg.name like  'FG_SalesSW%') fgs on fg.filegroup_guid = fgs.filegroup_guid
		where b.value is null
		order by rowId
	OPEN @rows

	FETCH NEXT FROM @rows 
	INTO @year,@month
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		set @rDate = convert(date,CAST(@year as varchar(4))+'-'+CAST(@month as varchar(2))+'-01')		
		
		EXECUTE [series].[ManageFG_SalesSW] @rDate,2
		
		FETCH NEXT FROM @rows
		INTO @year,@month
			
	END

	CLOSE @rows
	DEALLOCATE @rows


END

