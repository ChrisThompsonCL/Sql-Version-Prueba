﻿-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2014-10-14
-- Description:	Agrega o elimina un FileGroup y un File del tipo FG_RerportStores, para el mes correspondiente a la fecha indicada.
-- =============================================
CREATE PROCEDURE [series].[ManageFG_SalesSW]
	-- Add the parameters for the stored procedure here
	@date date=NULL,
	@action int=-1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @year int,@month int
	set @year = DATEPART(year,@date)
	set @month = DATEPART(month,@date)
	
	DECLARE @sqlFileGroup varchar(3000)
	DECLARE @sqlFile varchar(3000)
		
		
	IF(@action=1)
	BEGIN
		SELECT @sqlFileGroup = 'ALTER DATABASE Salcobrand add FILEGROUP FG_SalesSW'+cast((@year*100+@month) as varchar)
		EXEC(@sqlFileGroup)

		SELECT @sqlFile = 'ALTER DATABASE Salcobrand  ADD FILE (NAME = SalesSW'+cast((@year*100+@month) as varchar)+
						',FILENAME="D:\MSSQLSERVER\MSSQL\Salcobrand\FG_SalesSW\SalesSW'+cast((@year*100+@month) as varchar)+'.ndf",'+
						'SIZE = 5MB,MAXSIZE = UNLIMITED,FILEGROWTH = 5MB)   TO FILEGROUP FG_SalesSW'+cast((@year*100+@month) as varchar)
		EXEC(@sqlFile)
	END

	ELSE IF (@action=2)
	BEGIN 
		
		SELECT @sqlFile = 'ALTER DATABASE Salcobrand  remove FILE SalesSW'+cast((@year*100+@month) as varchar)
		EXEC(@sqlFile)
				
		SELECT @sqlFileGroup = 'ALTER DATABASE Salcobrand remove FILEGROUP FG_SalesSW'+cast((@year*100+@month) as varchar)
		EXEC(@sqlFileGroup)

	END
	
END
