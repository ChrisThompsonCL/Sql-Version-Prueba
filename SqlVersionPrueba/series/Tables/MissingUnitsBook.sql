﻿CREATE TABLE [series].[MissingUnitsBook] (
    [Store]    INT            NOT NULL,
    [SKU]      INT            NOT NULL,
    [Date]     DATE           NOT NULL,
    [RUT]      INT            NOT NULL,
    [Type]     INT            NOT NULL,
    [TypeDesc] NVARCHAR (150) NOT NULL,
    [Times]    INT            NULL
);

