﻿CREATE TABLE [series].[CompetitorsDailyPrice] (
    [SKU]         INT           NOT NULL,
    [Date]        SMALLDATETIME NOT NULL,
    [Fasa]        FLOAT (53)    NULL,
    [CV]          FLOAT (53)    NULL,
    [Simi]        FLOAT (53)    NULL,
    [UpdatedFasa] BIT           NULL,
    [UpdatedCV]   BIT           NULL,
    [UpdatedSimi] BIT           NULL,
    CONSTRAINT [PK_CompetitorsDailyPrice] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC) ON [SchPartitionQ] ([Date])
);


GO
CREATE NONCLUSTERED INDEX [IX_CompetitorsDailyPrice]
    ON [series].[CompetitorsDailyPrice]([SKU] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_CompetitorsDailyPrice_1]
    ON [series].[CompetitorsDailyPrice]([Date] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_CompetitorsDailyPrice_Date]
    ON [series].[CompetitorsDailyPrice]([Date] ASC)
    INCLUDE([SKU], [Fasa], [CV], [Simi])
    ON [SchPartitionQ] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_CompetitorsDailyPrice_Comps]
    ON [series].[CompetitorsDailyPrice]([Fasa] ASC, [CV] ASC, [Simi] ASC)
    INCLUDE([SKU], [Date])
    ON [SchPartitionQ] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_CompetitorsDailyPrice_Date2]
    ON [series].[CompetitorsDailyPrice]([Date] ASC)
    INCLUDE([SKU], [Fasa], [CV], [Simi])
    ON [SchPartitionQ] ([Date]);

