﻿CREATE TABLE [series].[NielsenCat] (
    [Mes]           DATE           NOT NULL,
    [CategoriaId]   INT            NOT NULL,
    [ZonaId]        INT            NOT NULL,
    [ValoresSB]     FLOAT (53)     NOT NULL,
    [ValoresPU]     FLOAT (53)     NOT NULL,
    [ValoresCadena] FLOAT (53)     NOT NULL,
    [CategoriaDesc] NVARCHAR (255) NOT NULL,
    [ZonaDesc]      NVARCHAR (255) NOT NULL
);

