﻿CREATE TABLE [series].[StockCD] (
    [SKU]     INT           NOT NULL,
    [Date]    SMALLDATETIME NOT NULL,
    [StockCD] INT           CONSTRAINT [DF_StockCD_StockCD] DEFAULT ((0)) NULL,
    [Transit] INT           CONSTRAINT [DF_StockCD_Transit] DEFAULT ((0)) NULL,
    [Order]   INT           CONSTRAINT [DF_StockCD_Order] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_StockCD] PRIMARY KEY NONCLUSTERED ([SKU] ASC, [Date] ASC) ON [PRIMARY]
) ON [SchPartitionQ] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_StockCD_Date]
    ON [series].[StockCD]([Date] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_StockCD_SKU]
    ON [series].[StockCD]([SKU] ASC)
    ON [PRIMARY];

