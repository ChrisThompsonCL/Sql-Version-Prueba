﻿CREATE TABLE [series].[Sales] (
    [SKU]      INT           NOT NULL,
    [Store]    SMALLINT      NOT NULL,
    [Date]     SMALLDATETIME NOT NULL,
    [Quantity] INT           NULL,
    [Income]   FLOAT (53)    NULL,
    CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC) ON [SchPartitionQ] ([Date])
);


GO
CREATE NONCLUSTERED INDEX [IX_SKU]
    ON [series].[Sales]([SKU] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_Store]
    ON [series].[Sales]([Store] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_Date]
    ON [series].[Sales]([Date] ASC)
    INCLUDE([SKU], [Store])
    ON [SchPartitionQ] ([Date]);

