﻿CREATE TABLE [series].[TicketSales] (
    [Date]        DATE           NOT NULL,
    [TicketId]    BIGINT         NOT NULL,
    [SKU]         INT            NOT NULL,
    [Quantity]    FLOAT (53)     NULL,
    [ListPrice]   FLOAT (53)     NULL,
    [Cost]        FLOAT (53)     NULL,
    [Discount]    FLOAT (53)     NULL,
    [FinalIncome] FLOAT (53)     NULL,
    [NetIncome]   FLOAT (53)     NULL,
    [ClientId]    NVARCHAR (20)  NULL,
    [Gender]      NVARCHAR (1)   NULL,
    [AgeRange]    NVARCHAR (50)  NULL,
    [Intensity]   NVARCHAR (50)  NULL,
    [Status]      NVARCHAR (50)  NULL,
    [FamilyType]  NVARCHAR (150) NULL,
    [SES]         NVARCHAR (50)  NULL,
    [IsChronic]   NVARCHAR (1)   NULL,
    [Store]       INT            NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_TicketSales]
    ON [series].[TicketSales]([Date] ASC, [SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TicketSale_SKU]
    ON [series].[TicketSales]([SKU] ASC)
    INCLUDE([TicketId]);

