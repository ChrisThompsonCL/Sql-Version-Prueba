﻿CREATE TABLE [series].[DailyPurchaseOrders] (
    [NumeroOC]              INT            NOT NULL,
    [SKU]                   INT            NOT NULL,
    [FechaCreacion]         DATE           NOT NULL,
    [ManufacturerId]        INT            NOT NULL,
    [RUT]                   VARCHAR (50)   NOT NULL,
    [Glosa]                 NVARCHAR (500) NULL,
    [Comprador]             VARCHAR (50)   NULL,
    [Cantidad]              INT            NOT NULL,
    [Despachado]            INT            NOT NULL,
    [Rechazado]             INT            NOT NULL,
    [Tolerancia]            INT            NULL,
    [FechaExpiracionActual] DATE           NULL,
    [FechaEntregaReal]      DATE           NULL,
    [PrecioFinal]           FLOAT (53)     NOT NULL,
    [Estado]                TINYINT        NOT NULL,
    [EstadoDesc]            VARCHAR (50)   NOT NULL,
    PRIMARY KEY CLUSTERED ([NumeroOC] ASC, [SKU] ASC)
);

