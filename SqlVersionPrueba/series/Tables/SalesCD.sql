﻿CREATE TABLE [series].[SalesCD] (
    [SKU]      INT           NOT NULL,
    [Date]     SMALLDATETIME NOT NULL,
    [Order]    INT           NULL,
    [Dispatch] INT           NULL,
    [BrokenCD] FLOAT (53)    NULL,
    CONSTRAINT [PK_SalesCD1] PRIMARY KEY NONCLUSTERED ([SKU] ASC, [Date] ASC) ON [PRIMARY]
) ON [SchPartitionQ] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_SalesCD_Date]
    ON [series].[SalesCD]([Date] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_SalesCD_SKU]
    ON [series].[SalesCD]([SKU] ASC)
    ON [PRIMARY];

