﻿CREATE TABLE [series].[IMS] (
    [Date]              DATE       NOT NULL,
    [Codigo]            BIGINT     NULL,
    [UnidadesSB]        FLOAT (53) NULL,
    [UnidadesCadenas]   FLOAT (53) NULL,
    [VentasSB]          FLOAT (53) NULL,
    [VentasCadenas]     FLOAT (53) NULL,
    [SKU]               BIGINT     NULL,
    [PositioningID]     INT        NULL,
    [MarketShareSKUCat] FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_IMS_1]
    ON [series].[IMS]([Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IMS_2]
    ON [series].[IMS]([SKU] ASC)
    INCLUDE([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_IMS_4]
    ON [series].[IMS]([PositioningID] ASC);

