﻿CREATE TABLE [series].[PriceAndCost] (
    [SKU]         INT           NOT NULL,
    [Date]        DATE          NOT NULL,
    [Price]       FLOAT (53)    NULL,
    [Cost]        FLOAT (53)    NULL,
    [CostWithIVA] BIT           CONSTRAINT [DF_PriceAndCost_CostWithIVA2] DEFAULT ((0)) NULL,
    [Motive]      NVARCHAR (10) NULL,
    CONSTRAINT [PK_PriceAndCost2] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC)
);

