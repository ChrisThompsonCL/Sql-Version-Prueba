﻿CREATE TABLE [series].[LastStock] (
    [SKU]       INT           NOT NULL,
    [Store]     SMALLINT      NOT NULL,
    [Date]      SMALLDATETIME NOT NULL,
    [Stock]     INT           NULL,
    [Suggest]   INT           NULL,
    [Transit]   INT           NULL,
    [Max]       INT           NULL,
    [Min]       INT           NULL,
    [IsBloqued] NCHAR (10)    NULL,
    CONSTRAINT [PK_LastStock] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_LastStock_Date]
    ON [series].[LastStock]([Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LastStock_SKU]
    ON [series].[LastStock]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LastStock_Store]
    ON [series].[LastStock]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LastStock_IsBloqued]
    ON [series].[LastStock]([IsBloqued] ASC)
    INCLUDE([SKU], [Store], [Max]);

