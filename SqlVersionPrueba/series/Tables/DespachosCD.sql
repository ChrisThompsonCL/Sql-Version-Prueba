﻿CREATE TABLE [series].[DespachosCD] (
    [Date]               DATE          NOT NULL,
    [Store]              INT           NOT NULL,
    [SKU]                INT           NOT NULL,
    [Bodega]             NVARCHAR (50) NOT NULL,
    [Sugerido]           INT           NULL,
    [Solicitado]         INT           NULL,
    [Corte]              INT           NULL,
    [Picking]            INT           NULL,
    [Disponible]         INT           NULL,
    [Pedido]             INT           NULL,
    [DisponibleSugerido] INT           NULL,
    PRIMARY KEY CLUSTERED ([Date] ASC, [Store] ASC, [SKU] ASC, [Bodega] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_DateStoreSku]
    ON [series].[DespachosCD]([Picking] ASC)
    INCLUDE([Date], [Store], [SKU]);


GO
CREATE NONCLUSTERED INDEX [IX_DespachosCD_1]
    ON [series].[DespachosCD]([Store] ASC, [Date] ASC, [Picking] ASC)
    INCLUDE([SKU]);

