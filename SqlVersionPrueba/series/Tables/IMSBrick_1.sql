﻿CREATE TABLE [series].[IMSBrick] (
    [Semana]         DATE           NOT NULL,
    [SemanaNum]      INT            NOT NULL,
    [BrickId]        INT            NOT NULL,
    [Brick]          NVARCHAR (255) NOT NULL,
    [SKU]            INT            NOT NULL,
    [SKUDesc]        NVARCHAR (255) NOT NULL,
    [UnidadesSB]     INT            NOT NULL,
    [UnidadesCadena] INT            NOT NULL,
    [ValoresSB]      INT            NOT NULL,
    [ValoresCadena]  INT            NOT NULL,
    CONSTRAINT [PK_IMSBrick] PRIMARY KEY CLUSTERED ([Semana] ASC, [BrickId] ASC, [SKU] ASC)
);

