﻿CREATE TABLE [series].[StockCDDia] (
    [Date]    DATE       NOT NULL,
    [SKU]     INT        NOT NULL,
    [Stock]   INT        NOT NULL,
    [Transit] INT        NOT NULL,
    [UPrice]  FLOAT (53) NULL,
    [UCost]   FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);

