﻿CREATE TABLE [series].[DailySales] (
    [SKU]      INT        NOT NULL,
    [Date]     DATE       NOT NULL,
    [Quantity] INT        NULL,
    [Income]   FLOAT (53) NULL,
    CONSTRAINT [PK_DailySalesseries2] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DailySales_Date]
    ON [series].[DailySales]([Date] ASC);

