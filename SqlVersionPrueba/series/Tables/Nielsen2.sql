﻿CREATE TABLE [series].[Nielsen2] (
    [Date]              SMALLDATETIME NOT NULL,
    [SKU]               INT           NOT NULL,
    [UnidadesSB]        FLOAT (53)    NULL,
    [UnidadesCadenas]   FLOAT (53)    NULL,
    [VentasSB]          FLOAT (53)    NULL,
    [VentasCadenas]     FLOAT (53)    NULL,
    [ClassID]           INT           NULL,
    [MarketShareSKUCat] FLOAT (53)    NULL,
    CONSTRAINT [PK_Nielsen_1] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Nielsen_SKU]
    ON [series].[Nielsen2]([SKU] ASC)
    INCLUDE([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_Nielsen_Date]
    ON [series].[Nielsen2]([Date] ASC)
    INCLUDE([SKU]);


GO
CREATE NONCLUSTERED INDEX [IX_Nielsen_11]
    ON [series].[Nielsen2]([Date] ASC, [ClassID] ASC)
    INCLUDE([UnidadesCadenas]);

