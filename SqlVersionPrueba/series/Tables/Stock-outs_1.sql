﻿CREATE TABLE [series].[Stock-outs] (
    [sku]   INT  NOT NULL,
    [store] INT  NOT NULL,
    [date]  DATE NOT NULL,
    [stock] INT  NULL,
    CONSTRAINT [PK_stock] PRIMARY KEY CLUSTERED ([sku] ASC, [store] ASC, [date] ASC)
);

