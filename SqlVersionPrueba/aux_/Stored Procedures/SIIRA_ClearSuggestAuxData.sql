﻿-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-03-08
-- Description:	Vacía las tablas auxiliares del cálculo de pedidos
		--		
-- =============================================
create PROCEDURE [aux].[SIIRA_ClearSuggestAuxData]

AS
BEGIN

	truncate table aux.RatesForSuggest
	truncate table [aux].[SimulatedSales]

END
