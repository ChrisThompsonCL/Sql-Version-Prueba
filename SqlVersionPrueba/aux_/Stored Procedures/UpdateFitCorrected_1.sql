﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 24-05-2018
-- Description:	Actualizar Fit Corregido mediante carga excel.
-- =============================================

CREATE PROCEDURE [aux].[UpdateFitCorrected]
@id_proceso     uniqueidentifier,
@usuario        NVARCHAR(MAX),
@fecha          DATE
AS
BEGIN

SET NOCOUNT ON;

UPDATE a SET FitDailyForecast = FitCargaUsuario, ModifiedBy = @usuario, ModifiedOn = @fecha
FROM(
SELECT lo.FitCorrected FitCargaUsuario, df.FitCorregido FitDailyForecast, df.ModifiedBy, df.ModifiedOn
FROM aux.LoadFitCorrected lo
INNER JOIN forecast.DailyForecast df ON lo.SKU = df.SKU AND lo.Date = df.Fecha
WHERE ProcessId = @id_proceso
AND ISNULL(lo.FitCorrected,0) <> ISNULL(df.FitCorregido,0)
) a

TRUNCATE TABLE aux.LoadFitCorrected

END


