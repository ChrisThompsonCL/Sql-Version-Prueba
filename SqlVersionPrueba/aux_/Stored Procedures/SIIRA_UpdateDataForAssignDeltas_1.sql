﻿
-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-04-17
-- Description:	Actualiza la tabla "[aux].[UpdateStatisticalData]" que contiene información relevante para la distribución de deltas
-- =============================================
CREATE PROCEDURE [aux].[SIIRA_UpdateDataForAssignDeltas] 

AS
BEGIN
	

	truncate table [aux].[DeltaAndDeficitAssignation]

	insert into [aux].[DeltaAndDeficitAssignation]

	SELECT o.sku Sku 
	       ,o.Store
	       ,o.Original_Order
		   ,0 Delta_Order 
	       ,isnull(ps.rate,0) Rate
		   ,isnull( case when ls.Stock<0 then 0 else ls.Stock end  +ls.Transit,0) StockAndTransit
		   ,0 DeltaAcum
		   ,0 Removed_Units
	 FROM replenishment.[DailyOrder] o
	left join [minmax].[PricingSuggest] ps on ps.store=o.Store and ps.sku=o.SKU
	left join replenishment.StockTarde ls on ls.SKU=o.sku and ls.Store=o.Store
	left join products.GenericFamilyDetail g on g.SKU=o.SKU
	where not exists (select 1 from  products.GenericFamilyDetail g where g.SKU=o.SKU)

	-- para genéricos
	insert into [aux].[DeltaAndDeficitAssignation]

	 SELECT o.sku Sku 
	       ,o.Store
	       ,o.Original_Order 
	       ,0 Delta_Order 
	       ,isnull(ps.rate,0) Rate
		   ,isnull( case when ls.Stock<0 then 0 else ls.Stock end  +ls.Transit,0) StockAndTransit
		   ,0 DeltaAcum
		   ,0 Removed_Units
	 FROM replenishment.[DailyOrder] o
	 inner join products.GenericFamilyDetail g on g.SKU=o.SKU
	left join [minmax].[PricingSuggest] ps on ps.store=o.Store and ps.sku=o.SKU
	left join (
			select g.GenericFamilyId,b.Store,sum(b.Stock) stock,sum(b.Transit) Transit
			from replenishment.StockTarde b 
			inner join products.GenericFamilyDetail g on g.SKU=b.SKU
			group by g.GenericFamilyId,b.Store
	) ls on ls.GenericFamilyId=g.GenericFamilyId and ls.Store=o.Store


END