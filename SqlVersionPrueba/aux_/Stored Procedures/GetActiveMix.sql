﻿
-- =============================================
-- Author:	
-- Create date:
-- 
-- =============================================
CREATE PROCEDURE [aux].[GetActiveMix]
	@SKU NVARCHAR(200) = NULL,
	@Division NVARCHAR(200) = NULL,
	@Categoria NVARCHAR(200) = NULL,
	@Subcategoria NVARCHAR(200) = NULL,
	@Ranking NVARCHAR(200) = NULL,
	@Local NVARCHAR(200) = NULL,
	@mixState NVARCHAR(3) = NULL
AS
BEGIN

SET NOCOUNT ON;



	--DECLARE @SKU int
	--DECLARE @Division NVARCHAR(200)
	--DECLARE @Categoria NVARCHAR(200)
	--DECLARE @Subcategoria NVARCHAR(200)
	--DECLARE @Ranking NVARCHAR(200)
	--DECLARE @Local NVARCHAR(200)
	--SET @Division = 1
	--SET @Ranking = 'A'

	DECLARE @skus_enviados TABLE (NAME INT NOT NULL PRIMARY KEY);
	INSERT INTO @skus_enviados 
	SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@SKU)

	DECLARE @Division_enviados TABLE (NAME INT NOT NULL PRIMARY KEY);
	INSERT INTO @Division_enviados 
	SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@Division)

	DECLARE @Categoria_enviados TABLE (NAME INT NOT NULL PRIMARY KEY);
	INSERT INTO @Categoria_enviados 
	SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@Categoria)

	DECLARE @Subcategoria_enviados TABLE (NAME INT NOT NULL PRIMARY KEY);
	INSERT INTO @Subcategoria_enviados 
	SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@Subcategoria)

	DECLARE @Ranking_enviados TABLE (NAME CHAR NOT NULL PRIMARY KEY);
	INSERT INTO @Ranking_enviados 
	SELECT [Name] FROM dbo.SplitString(@Ranking) WHERE Name IS NOT NULL

	DECLARE @Local_enviados TABLE (NAME INT NOT NULL PRIMARY KEY);
	INSERT INTO @Local_enviados 
	SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@Local)

	SELECT	pro.[SKU],
			pro.[Name] as Descripcion,
			stores.[Store] as Local,
			stores.[Address] as Direccion,
			pro.[DivisionUnificadaDesc] as Division,
			pro.[CategoriaUnificadaDesc] as Categoria,
			pro.[SubcategoriaUnificadaDesc] as SubCategoria,
			pro.[TotalRanking] as TotalRanking,
			CAST (case when d.SKU IS NOT NULL then 1 ELSE 0 END as bit) as MixPricing
	FROM [series].[LastStock] ls
	INNER JOIN [Products].Products pro ON ls.SKU = pro.SKU
	INNER JOIN [Stores].Stores stores ON ls.Store = stores.Store
	LEFT JOIN(SELECT	sku,
						store
				FROM [aux].DailySuggestMix dsm 
				WHERE	(dsm.SKU = sku) AND
						(dsm.Store = store)) d ON ls.SKU = d.SKU AND ls.Store = d.Store
	WHERE ( pro.SKU =  @SKU  OR  @SKU  IS NULL)
	AND   (@Division IS NULL OR pro.DivisionUnificadaId IN (SELECT NAME FROM @Division_enviados))
	AND   (@Categoria IS NULL OR pro.CategoriaUnificadaId IN (SELECT NAME FROM @Categoria_enviados))
	AND   (@Subcategoria IS NULL OR pro.SubcategoriaUnificadaId IN (SELECT NAME FROM @Subcategoria_enviados))
	AND   (@Ranking IS NULL OR pro.TotalRanking IN (SELECT NAME FROM @Ranking_enviados))
	AND   (@Local IS NULL OR ls.Store IN (SELECT NAME FROM @Local_enviados))
	AND   (@mixState IS NULL OR (d.SKU IS NOT NULL AND @mixState = 'mp') OR ((d.SKU IS NULL AND @mixState = 'fmp')))
	ORDER BY pro.SKU ASC
END