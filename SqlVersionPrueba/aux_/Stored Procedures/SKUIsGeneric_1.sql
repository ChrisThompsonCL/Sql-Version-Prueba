﻿-- =============================================
-- Author:		Jorge Ibacache
-- Create date: 2018-07-03
-- Description:	Indica si un SKU específico es parte de una familia (genérico)
-- =============================================
CREATE PROCEDURE [aux].[SKUIsGeneric]
	@SKU int
AS
BEGIN
	--DECLARE @SKU int = 920008
	SELECT CASE WHEN COUNT(gfd.SKU) > 0 THEN 1 ELSE 0 END 
	FROM [products].[GenericFamilyDetail] gfd
	WHERE gfd.SKU = @SKU
	GROUP BY gfd.SKU
END