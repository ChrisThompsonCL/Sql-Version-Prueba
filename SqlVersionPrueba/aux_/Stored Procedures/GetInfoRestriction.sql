﻿-- =============================================
-- Author:		Juan M Hernandez
-- Create date: 27/02/2018
-- Description:	Obtener los GES con sugerido de compra
-- =============================================

CREATE PROCEDURE [aux].[GetInfoRestriction]
@sku int,
@store int
AS
BEGIN

SET NOCOUNT ON;

declare @S_SKU int = @sku;
declare @S_Store int = @Store;


DECLARE  @MinimoProd int,@MinimoComb int, @tipo nvarchar(50) ; 
SET @MinimoProd = (select MinimoProd_Restricción FROM minmax.LastSuggestRestriction WHERE SKU = @S_SKU AND Store = @S_Store)
SET @MinimoComb = (select MinimoComb_Restricción FROM minmax.LastSuggestRestriction WHERE SKU = @S_SKU AND Store = @S_Store )


IF @MinimoProd is not null and @MinimoComb is not null BEGIN 
   IF @MinimoProd>@MinimoComb BEGIN
		SET @tipo = 'Mínimo por combinación'
   END
   ELSE BEGIN
		SET @tipo = 'Mínimo por producto'
   END
END



  ----------------------------------------------------------------
SELECT * FROM (
	SELECT sku, 
	 cast (store as int ) store
	,CASE 
		WHEN r= 'MinimoProd_Restricción' THEN 'Mínimo por producto' 
		 WHEN r = 'MinimoComb_Restricción' THEN 'Mínimo por combinación' 
		 WHEN r= 'CargaManual_Restricción' THEN 'Carga Manual' 
		 WHEN r= 'Planograma_Restricción' THEN 'Planograma' 
		 WHEN r= 'DUN_Restricción' THEN 'DUN' 
		 WHEN r= 'MaximoProd_Restricción' THEN 'Máximo por producto'
		 WHEN r = 'MaximoComb_Restricción' THEN 'Máximo por combinación' 
		 WHEN r= 'CajaTira_Restricción' THEN 'Caja Tira' 
		 WHEN r= 'FijoEstricto_Restricción' THEN 'Fijo Estricto' 
	 END TipoRestriccion
	 ,case	
		wHEN r= 'MinimoProd_Restricción' THEN MinimoProd_Usuario
		 WHEN r = 'MinimoComb_Restricción' THEN MinimoComb_Usuario
		 WHEN r= 'CargaManual_Restricción' THEN CargaManual_Usuario
		 WHEN r= 'Planograma_Restricción' THEN Planograma_Usuario
		 WHEN r= 'DUN_Restricción' THEN DUN_Usuario
		 WHEN r= 'MaximoProd_Restricción' THEN MaximoProd_Usuario
		 WHEN r = 'MaximoComb_Restricción' THEN MaximoComb_Usuario
		 WHEN r= 'CajaTira_Restricción' THEN CajaTira_Usuario
		 WHEN r= 'FijoEstricto_Restricción' THEN FijoEstricto_Usuario
		 end [User]
	 ,CASE 
		WHEN r = 'MinimoComb_Restricción' THEN  MinimoComb_Término 
		WHEN r= 'CargaManual_Restricción' THEN CargaManual_Término 
		WHEN r ='MaximoComb_Restricción' THEN MaximoComb_Término
		WHEN r ='FijoEstricto_Restricción' THEN FijoEstricto_Término
	  END Fechatermino
	 ,case	
		wHEN r= 'MinimoProd_Restricción' THEN MinimoProd_Archivo
		 WHEN r = 'MinimoComb_Restricción' THEN MinimoComb_Archivo
		 WHEN r= 'CargaManual_Restricción' THEN CargaManual_Archivo
		 WHEN r= 'Planograma_Restricción' THEN Planograma_Archivo
		 WHEN r= 'DUN_Restricción' THEN DUN_Archivo
		 WHEN r= 'MaximoProd_Restricción' THEN MaximoProd_Archivo
		 WHEN r = 'MaximoComb_Restricción' THEN MaximoComb_Archivo
		 WHEN r= 'CajaTira_Restricción' THEN CajaTira_Archivo
		 WHEN r= 'FijoEstricto_Restricción' THEN FijoEstricto_Archivo
		 end Archivo    
	 ,convert(int, restriccion ) Restriccion
	FROM minmax.LastSuggestRestriction t
	UNPIVOT
	(
		restriccion
		for [r] in ([MinimoProd_Restricción],
				[MinimoComb_Restricción],
				[CargaManual_Restricción],
				[Planograma_Restricción], 
				[DUN_Restricción] ,
				[MaximoProd_Restricción],
				[MaximoComb_Restricción],
				[CajaTira_Restricción],
				[FijoEstricto_Restricción]
				)

	) as p
)A
WHERE SKU = @sku AND Store = @Store AND (TipoRestriccion <> @tipo or  @tipo is null)



END




