﻿CREATE TABLE [aux].[TempSales] (
    [SKU]      INT           NOT NULL,
    [Store]    SMALLINT      NOT NULL,
    [Date]     SMALLDATETIME NOT NULL,
    [Quantity] INT           NULL,
    [Income]   FLOAT (53)    NULL,
    CONSTRAINT [PK_TempSales] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

