﻿CREATE TABLE [aux].[TempStockTarde] (
    [SKU]       INT          NOT NULL,
    [Store]     INT          NOT NULL,
    [Date]      DATE         NOT NULL,
    [Stock]     INT          NOT NULL,
    [Transit]   INT          NOT NULL,
    [Min]       INT          NOT NULL,
    [Max]       INT          NOT NULL,
    [Suggest]   INT          NOT NULL,
    [IsBloqued] NVARCHAR (1) NOT NULL
);

