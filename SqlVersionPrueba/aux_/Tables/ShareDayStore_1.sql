﻿CREATE TABLE [aux].[ShareDayStore] (
    [SKU]        BIGINT     NOT NULL,
    [Week]       DATE       NOT NULL,
    [Id_region0] INT        NOT NULL,
    [sundayopen] BIT        NOT NULL,
    [L]          FLOAT (53) NULL,
    [M]          FLOAT (53) NULL,
    [W]          FLOAT (53) NULL,
    [J]          FLOAT (53) NULL,
    [V]          FLOAT (53) NULL,
    [S]          FLOAT (53) NULL,
    [D]          FLOAT (53) NULL
);

