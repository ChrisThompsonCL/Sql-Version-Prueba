﻿CREATE TABLE [aux].[OptimalPolicyPrice] (
    [Date]     SMALLDATETIME NOT NULL,
    [SKU]      BIGINT        NOT NULL,
    [Store]    BIGINT        NOT NULL,
    [Price]    FLOAT (53)    NOT NULL,
    [MinPrice] FLOAT (53)    NULL,
    [MaxPrice] FLOAT (53)    NULL
);

