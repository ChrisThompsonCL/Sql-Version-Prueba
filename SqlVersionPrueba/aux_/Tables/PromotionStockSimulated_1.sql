﻿CREATE TABLE [aux].[PromotionStockSimulated] (
    [ProductId]           INT           NOT NULL,
    [BeginDate]           SMALLDATETIME NOT NULL,
    [EndDate]             SMALLDATETIME NOT NULL,
    [NivelPromocion]      FLOAT (53)    NULL,
    [PromotionFileId]     INT           NOT NULL,
    [DemandaSinPromocion] FLOAT (53)    NULL,
    [DemandaConPromocion] FLOAT (53)    NULL
);

