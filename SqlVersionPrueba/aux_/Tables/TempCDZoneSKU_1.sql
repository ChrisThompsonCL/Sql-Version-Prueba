﻿CREATE TABLE [aux].[TempCDZoneSKU] (
    [SKU]                   INT            NOT NULL,
    [HandlingUnit]          NVARCHAR (5)   NOT NULL,
    [LocationNumber]        NVARCHAR (50)  NOT NULL,
    [PickLocationNumber]    NVARCHAR (50)  NOT NULL,
    [Area]                  NVARCHAR (50)  NOT NULL,
    [AutReplenishment]      NVARCHAR (1)   NOT NULL,
    [Min]                   INT            NULL,
    [MinPerDay]             INT            NULL,
    [Max]                   INT            NULL,
    [Active]                NVARCHAR (1)   NULL,
    [MaxSO]                 INT            NOT NULL,
    [Buffer]                INT            NULL,
    [SDAMax]                INT            NULL,
    [SDAMin]                INT            NULL,
    [EjectionSpeed]         INT            NULL,
    [ActivationCriteria]    NVARCHAR (100) NULL,
    [ReplenishmentCriteria] NVARCHAR (100) NULL,
    [Date]                  SMALLDATETIME  NOT NULL
);

