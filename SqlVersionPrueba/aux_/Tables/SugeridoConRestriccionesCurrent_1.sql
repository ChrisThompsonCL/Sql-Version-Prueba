﻿CREATE TABLE [aux].[SugeridoConRestriccionesCurrent] (
    [sku]          INT           NULL,
    [StoreId]      INT           NOT NULL,
    [date]         SMALLDATETIME NOT NULL,
    [Minimo]       INT           NULL,
    [Fijo]         INT           NULL,
    [Planograma]   INT           NULL,
    [MedioDUN]     NUMERIC (20)  NULL,
    [Maximo]       INT           NULL,
    [CajaTira]     INT           NULL,
    [FijoEstricto] INT           NULL
);

