﻿CREATE TABLE [aux].[RatesForSuggest] (
    [sku]          INT        NOT NULL,
    [store]        INT        NOT NULL,
    [date]         DATE       NOT NULL,
    [fitDay]       FLOAT (53) NULL,
    [fitWeek]      FLOAT (53) NULL,
    [shareday]     FLOAT (53) NULL,
    [dailySale]    FLOAT (53) NULL,
    [days]         INT        NULL,
    [rate]         FLOAT (53) NULL,
    [suggestMin]   INT        NULL,
    [suggestMax]   INT        NULL,
    [LeadTime]     INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [SafetyDays]   INT        NULL,
    CONSTRAINT [PK_RatesForSuggest] PRIMARY KEY CLUSTERED ([sku] ASC, [store] ASC, [date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDXStoreDateStoreSuggest]
    ON [aux].[RatesForSuggest]([date] ASC)
    INCLUDE([store], [suggestMin], [suggestMax]);


GO
CREATE NONCLUSTERED INDEX [IDXSkuDate]
    ON [aux].[RatesForSuggest]([sku] ASC, [date] ASC)
    INCLUDE([store], [suggestMin], [suggestMax]);


GO
CREATE NONCLUSTERED INDEX [idx_sk_st_dt]
    ON [aux].[RatesForSuggest]([sku] ASC, [store] ASC, [date] ASC)
    INCLUDE([fitDay]);


GO
CREATE NONCLUSTERED INDEX [idx_dt_sug]
    ON [aux].[RatesForSuggest]([date] ASC, [suggestMin] ASC)
    INCLUDE([sku], [store], [rate]);

