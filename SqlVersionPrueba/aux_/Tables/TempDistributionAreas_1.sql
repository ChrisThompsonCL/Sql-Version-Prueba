﻿CREATE TABLE [aux].[TempDistributionAreas] (
    [SKU]              INT            NOT NULL,
    [Description]      NVARCHAR (100) NOT NULL,
    [StockId]          INT            NOT NULL,
    [DistributionArea] NVARCHAR (50)  NOT NULL
);

