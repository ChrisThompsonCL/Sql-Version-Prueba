﻿CREATE TABLE [aux].[ShareStoreCurrent] (
    [SKU]   BIGINT        NOT NULL,
    [Store] INT           NOT NULL,
    [Week]  SMALLDATETIME NOT NULL,
    [Share] FLOAT (53)    NULL
);

