﻿CREATE TABLE [aux].[DeltaAndDeficitAssignation] (
    [SKU]             INT        NOT NULL,
    [Store]           INT        NOT NULL,
    [Original_Order]  INT        NULL,
    [Delta_Order]     INT        NULL,
    [Rate]            FLOAT (53) NULL,
    [StockAndTransit] INT        NULL,
    [DeltaAcum]       INT        NULL,
    [Removed_Units]   INT        NULL,
    CONSTRAINT [PK_DeltaAndDeficitAssignation] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

