﻿CREATE TABLE [aux].[TempProducts] (
    [SKU]                       INT            NOT NULL,
    [Barcode]                   BIGINT         NULL,
    [Description]               NVARCHAR (70)  NULL,
    [IsGeneric]                 VARCHAR (5)    NULL,
    [GenericCategory]           VARCHAR (50)   NULL,
    [GenericCategoryDesc]       VARCHAR (50)   NULL,
    [UseModeId]                 TINYINT        NULL,
    [UseMode_Desc]              VARCHAR (20)   NULL,
    [PositioningId]             TINYINT        NULL,
    [Positioning_Desc]          VARCHAR (50)   NULL,
    [SubstituteId]              SMALLINT       NULL,
    [Substitute_Desc]           VARCHAR (50)   NULL,
    [Division]                  SMALLINT       NULL,
    [Division_Desc]             VARCHAR (50)   NULL,
    [Subdivision]               SMALLINT       NULL,
    [Subdivision_Desc]          VARCHAR (50)   NULL,
    [Departament]               SMALLINT       NULL,
    [Departament_Desc]          VARCHAR (50)   NULL,
    [Class]                     SMALLINT       NULL,
    [Class_Desc]                VARCHAR (50)   NULL,
    [Group]                     SMALLINT       NULL,
    [Group_Desc]                VARCHAR (50)   NULL,
    [Subgroup]                  SMALLINT       NULL,
    [Subgroup_Desc]             VARCHAR (50)   NULL,
    [Motive]                    VARCHAR (50)   NULL,
    [TotalRanking]              VARCHAR (1)    NULL,
    [IsActive]                  BIT            NULL,
    [FirstSale]                 SMALLDATETIME  NULL,
    [LastSale]                  SMALLDATETIME  NULL,
    [Critical]                  BIT            NULL,
    [CategoryId]                INT            NULL,
    [CategoryDesc]              NVARCHAR (50)  NULL,
    [ManufacturerId]            INT            NULL,
    [ManufacturerDesc]          VARCHAR (50)   NULL,
    [Units]                     FLOAT (53)     NULL,
    [ConcentrationId]           NVARCHAR (50)  NULL,
    [Concentration]             NVARCHAR (50)  NULL,
    [Season]                    VARCHAR (50)   NULL,
    [Expression]                BIGINT         NULL,
    [LogisticAreaId]            NVARCHAR (50)  NULL,
    [LogisticAreaDesc]          NVARCHAR (50)  NULL,
    [MotiveId]                  INT            NULL,
    [CorporationId]             INT            NULL,
    [CorporationDesc]           NVARCHAR (50)  NULL,
    [UnitsString]               VARCHAR (50)   NULL,
    [Notable]                   NVARCHAR (50)  NULL,
    [ManufacturerFilter]        NVARCHAR (50)  NULL,
    [Consignment]               NVARCHAR (50)  NULL,
    [Infaltable]                NVARCHAR (1)   NULL,
    [PetitorioMinimo]           INT            NULL,
    [PetitorioMinimo_Desc]      VARCHAR (50)   NULL,
    [ProCuidado]                VARCHAR (50)   NULL,
    [DivisionUnificadaId]       INT            NULL,
    [DivisionUnificadaDesc]     VARCHAR (MAX)  NULL,
    [CategoriaUnificadaId]      INT            NULL,
    [CategoriaUnificadaDesc]    VARCHAR (MAX)  NULL,
    [SubcategoriaUnificadaId]   INT            NULL,
    [SubcategoriaUnificadaDesc] VARCHAR (MAX)  NULL,
    [ClaseUnificadaId]          INT            NULL,
    [ClaseUnificadaDesc]        NVARCHAR (100) NULL,
    [Ges]                       VARCHAR (50)   NULL,
    [SustitutoComercialId]      INT            NULL,
    [VigenteId]                 INT            NULL,
    [VigenteDesc]               NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_TempPtoructs__TotalRanking]
    ON [aux].[TempProducts]([TotalRanking] ASC)
    INCLUDE([SKU]);


GO
CREATE NONCLUSTERED INDEX [IX_TempProducts_LogisticAreaDesc]
    ON [aux].[TempProducts]([LogisticAreaDesc] ASC)
    INCLUDE([SKU]);


GO
CREATE NONCLUSTERED INDEX [IX_TempProducts]
    ON [aux].[TempProducts]([SKU] ASC);

