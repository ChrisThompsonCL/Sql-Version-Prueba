﻿CREATE TABLE [aux].[PromotionLoad] (
    [Date]          SMALLDATETIME  NOT NULL,
    [ProductId]     INT            NOT NULL,
    [PromotionId]   BIGINT         NOT NULL,
    [HasFixedPrice] BIT            NULL,
    [PromotionName] NVARCHAR (255) NULL,
    [BeginDate]     SMALLDATETIME  NOT NULL,
    [EndDate]       SMALLDATETIME  NOT NULL,
    [PromotionType] INT            NULL,
    CONSTRAINT [PK_PromotionLoad_1] PRIMARY KEY CLUSTERED ([Date] ASC, [ProductId] ASC, [PromotionId] ASC)
);

