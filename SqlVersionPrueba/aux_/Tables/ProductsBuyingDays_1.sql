﻿CREATE TABLE [aux].[ProductsBuyingDays] (
    [Date]          SMALLDATETIME NOT NULL,
    [SKU]           INT           NOT NULL,
    [BuyingDays]    SMALLINT      NULL,
    [LeadtimeAvg]   FLOAT (53)    NULL,
    [LeadtimeStd]   FLOAT (53)    NULL,
    [ProtectionMin] FLOAT (53)    NULL,
    [ProtectionMax] FLOAT (53)    NULL,
    [MaxDays]       FLOAT (53)    NULL,
    [MinDays]       FLOAT (53)    NULL,
    CONSTRAINT [PK_ProductBuyingDays] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductBuyingDays]
    ON [aux].[ProductsBuyingDays]([Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductBuyingDays_1]
    ON [aux].[ProductsBuyingDays]([SKU] ASC);

