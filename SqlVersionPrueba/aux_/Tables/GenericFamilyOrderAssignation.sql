﻿CREATE TABLE [aux].[GenericFamilyOrderAssignation] (
    [Sku]            INT NULL,
    [Store]          INT NULL,
    [Original_Order] INT NULL,
    [Delta_Order]    INT NULL,
    [Rounded_Order]  INT NULL,
    [Cutted_Order]   INT NULL,
    [Generic_Order]  INT NULL
);

