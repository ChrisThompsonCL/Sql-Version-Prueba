﻿CREATE TABLE [aux].[TempNielsen] (
    [SemanaId]       INT            NOT NULL,
    [Semana]         DATE           NOT NULL,
    [SKU]            INT            NOT NULL,
    [SKUDesc]        NVARCHAR (255) NOT NULL,
    [UnidadesSB]     FLOAT (53)     NOT NULL,
    [ValoresSB]      FLOAT (53)     NOT NULL,
    [UnidadesCadena] FLOAT (53)     NOT NULL,
    [ValoresCadena]  FLOAT (53)     NOT NULL,
    [UnidadesPU]     FLOAT (53)     NOT NULL,
    [ValoresPU]      FLOAT (53)     NOT NULL
);

