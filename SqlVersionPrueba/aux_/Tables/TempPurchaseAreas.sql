﻿CREATE TABLE [aux].[TempPurchaseAreas] (
    [SKU]         INT            NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    [StockId]     INT            NOT NULL,
    [WMSZone]     NVARCHAR (50)  NOT NULL
);

