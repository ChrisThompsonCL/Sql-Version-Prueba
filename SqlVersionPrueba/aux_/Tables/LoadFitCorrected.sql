﻿CREATE TABLE [aux].[LoadFitCorrected] (
    [SKU]          INT              NULL,
    [Date]         DATE             NULL,
    [FitCorrected] FLOAT (53)       NULL,
    [ProcessId]    UNIQUEIDENTIFIER NULL
);

