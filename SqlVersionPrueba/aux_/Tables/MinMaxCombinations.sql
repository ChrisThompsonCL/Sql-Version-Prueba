﻿CREATE TABLE [aux].[MinMaxCombinations] (
    [SKU]           INT  NOT NULL,
    [Store]         INT  NOT NULL,
    [LastStockDate] DATE NOT NULL,
    [Picking]       INT  NOT NULL,
    [NewComb]       BIT  NOT NULL,
    CONSTRAINT [PK_AuxMinMaxComb] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

