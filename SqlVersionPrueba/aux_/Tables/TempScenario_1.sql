﻿CREATE TABLE [aux].[TempScenario] (
    [DateKey]      SMALLDATETIME NOT NULL,
    [SKU]          INT           NOT NULL,
    [Cluster]      INT           NOT NULL,
    [Forecast]     FLOAT (53)    NULL,
    [Sale]         FLOAT (53)    NULL,
    [Contribution] FLOAT (53)    NULL,
    [Margin]       FLOAT (53)    NULL,
    [Price]        FLOAT (53)    NULL
);

