﻿CREATE TABLE [aux].[PromotionStockLoad] (
    [ProductId]            INT            NOT NULL,
    [BeginDate]            SMALLDATETIME  NOT NULL,
    [EndDate]              SMALLDATETIME  NOT NULL,
    [PromotionName]        NVARCHAR (255) NOT NULL,
    [Goal]                 INT            NULL,
    [PromotionPrice]       FLOAT (53)     NULL,
    [DebeCalcularSugerido] BIT            NOT NULL,
    [PromotionFileId]      INT            NOT NULL
);

