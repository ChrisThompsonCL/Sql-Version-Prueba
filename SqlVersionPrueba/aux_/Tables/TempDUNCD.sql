﻿CREATE TABLE [aux].[TempDUNCD] (
    [SKU]              INT            NOT NULL,
    [Name]             NVARCHAR (100) NULL,
    [UnidadesxCamada]  INT            NULL,
    [UnidadesxPallet]  INT            NULL,
    [BuyingMultiple]   INT            NULL,
    [ConveniencePack]  INT            NULL,
    [ConveniencePackP] INT            NULL
);

