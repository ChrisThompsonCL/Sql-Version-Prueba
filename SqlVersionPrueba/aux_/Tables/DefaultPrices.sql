﻿CREATE TABLE [aux].[DefaultPrices] (
    [SKU]       INT           NULL,
    [Price]     FLOAT (53)    NULL,
    [StartDate] SMALLDATETIME NULL,
    [EndDate]   SMALLDATETIME NULL,
    [Motive]    VARCHAR (50)  NULL
);

