﻿CREATE TABLE [aux].[TempDUNStore] (
    [SKU]        INT    NOT NULL,
    [DUN14]      BIGINT NOT NULL,
    [Units]      INT    NULL,
    [CreateDate] DATE   NULL,
    [UpdateDate] DATE   NULL
);

