﻿CREATE TABLE [aux].[CalculoSugerido] (
    [sku]        INT        NULL,
    [store]      INT        NULL,
    [date]       DATE       NULL,
    [fitDay]     FLOAT (53) NULL,
    [fitWeek]    FLOAT (53) NULL,
    [shareday]   FLOAT (53) NULL,
    [dailySale]  FLOAT (53) NULL,
    [days]       INT        NULL,
    [rate]       FLOAT (53) NULL,
    [suggestMin] INT        NULL,
    [suggestMax] INT        NULL,
    [LT]         INT        NULL
);

