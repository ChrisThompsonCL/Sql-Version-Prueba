﻿CREATE TABLE [aux].[TempRecetas] (
    [Date]          DATE       NOT NULL,
    [Folio]         INT        NOT NULL,
    [Store]         INT        NULL,
    [RUTMedico]     INT        NOT NULL,
    [SKU]           INT        NOT NULL,
    [VentaUnidades] INT        NULL,
    [VentaNeta]     FLOAT (53) NULL
);

