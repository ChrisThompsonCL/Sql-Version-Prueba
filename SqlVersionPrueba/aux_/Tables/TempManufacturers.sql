﻿CREATE TABLE [aux].[TempManufacturers] (
    [Id]   INT            NOT NULL,
    [Name] NVARCHAR (500) NULL,
    [RUT]  NVARCHAR (50)  NULL
);

