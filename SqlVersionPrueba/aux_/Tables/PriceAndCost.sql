﻿CREATE TABLE [aux].[PriceAndCost] (
    [Date]   NVARCHAR (20) NOT NULL,
    [SKU]    INT           NOT NULL,
    [Motive] NVARCHAR (10) NULL,
    [Cost]   NVARCHAR (50) NULL,
    [Price]  NVARCHAR (50) NULL
);


GO
CREATE CLUSTERED INDEX [IX_PriceAndCost]
    ON [aux].[PriceAndCost]([SKU] ASC);

