﻿CREATE TABLE [aux].[ManualFitCorrections] (
    [DateKey]       DATE       NOT NULL,
    [Date]          DATE       NOT NULL,
    [SKU]           INT        NOT NULL,
    [ModelacionFit] FLOAT (53) NULL,
    CONSTRAINT [PK_ManualFitCorrections] PRIMARY KEY CLUSTERED ([DateKey] ASC, [Date] ASC, [SKU] ASC)
);

