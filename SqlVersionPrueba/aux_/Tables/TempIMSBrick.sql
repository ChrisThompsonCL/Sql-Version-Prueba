﻿CREATE TABLE [aux].[TempIMSBrick] (
    [Semana]         DATE           NOT NULL,
    [SemanaNum]      INT            NOT NULL,
    [BrickId]        INT            NOT NULL,
    [Brick]          NVARCHAR (255) NOT NULL,
    [SKU]            INT            NOT NULL,
    [SKUDesc]        NVARCHAR (255) NOT NULL,
    [UnidadesSB]     NVARCHAR (50)  NULL,
    [UnidadesCadena] NVARCHAR (50)  NULL,
    [ValoresSB]      NVARCHAR (50)  NULL,
    [ValoresCadena]  NVARCHAR (50)  NULL
);

