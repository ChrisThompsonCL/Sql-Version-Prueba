﻿CREATE TABLE [aux].[TempCortesPicking] (
    [CodigoEmpresa]          INT           NOT NULL,
    [LineasMaxLaboral]       INT           NOT NULL,
    [LineasMaxFestivo]       INT           NOT NULL,
    [AplicaExclusion]        NVARCHAR (1)  NOT NULL,
    [AplicaExclusiónLocales] NVARCHAR (1)  NOT NULL,
    [EstacionWMS]            NVARCHAR (50) NOT NULL,
    [CapacidadMaxLaboral]    INT           NOT NULL,
    [CapacidadMaxFestivo]    INT           NOT NULL,
    [FechaActualizacion]     DATE          NOT NULL
);

