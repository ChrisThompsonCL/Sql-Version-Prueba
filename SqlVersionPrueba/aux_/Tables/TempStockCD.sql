﻿CREATE TABLE [aux].[TempStockCD] (
    [Store]              INT          NOT NULL,
    [SKU]                INT          NOT NULL,
    [Date]               DATE         NOT NULL,
    [Stock]              INT          NULL,
    [UCost]              FLOAT (53)   NULL,
    [UPrice]             FLOAT (53)   NULL,
    [Transit]            INT          NULL,
    [ranking_quiebre_id] VARCHAR (50) NULL
);

