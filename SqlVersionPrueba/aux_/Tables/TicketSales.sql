﻿CREATE TABLE [aux].[TicketSales] (
    [Date]        DATETIME       NULL,
    [TicketId]    BIGINT         NULL,
    [SKU]         INT            NULL,
    [Quantity]    FLOAT (53)     NULL,
    [ListPrice]   FLOAT (53)     NULL,
    [Cost]        NVARCHAR (50)  NULL,
    [Discount]    FLOAT (53)     NULL,
    [FinalIncome] FLOAT (53)     NULL,
    [NetIncome]   NVARCHAR (50)  NULL,
    [ClientId]    NVARCHAR (20)  NULL,
    [Gender]      NVARCHAR (1)   NULL,
    [AgeRange]    NVARCHAR (50)  NULL,
    [Intensity]   NVARCHAR (50)  NULL,
    [Status]      NVARCHAR (50)  NULL,
    [FamilyType]  NVARCHAR (150) NULL,
    [SES]         NVARCHAR (50)  NULL,
    [IsChronic]   NVARCHAR (1)   NULL,
    [Store]       INT            NULL
);

