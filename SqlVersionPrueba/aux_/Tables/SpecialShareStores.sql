﻿CREATE TABLE [aux].[SpecialShareStores] (
    [Store]         INT           NOT NULL,
    [VentaStore]    FLOAT (53)    NOT NULL,
    [ShareStore]    FLOAT (53)    NOT NULL,
    [FechaStore]    DATE          NOT NULL,
    [City]          NVARCHAR (50) NULL,
    [VentaCity]     FLOAT (53)    NOT NULL,
    [NumStoresCity] INT           NOT NULL,
    [ShareCity]     FLOAT (53)    NOT NULL,
    [FechaCity]     DATE          NOT NULL
);

