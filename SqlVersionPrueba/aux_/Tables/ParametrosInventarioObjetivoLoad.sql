﻿CREATE TABLE [aux].[ParametrosInventarioObjetivoLoad] (
    [SKU]           INT              NULL,
    [NivelServicio] FLOAT (53)       NULL,
    [DiasSP]        INT              NULL,
    [id_proceso]    UNIQUEIDENTIFIER NULL,
    [InsertionDate] DATETIME         NULL
);

