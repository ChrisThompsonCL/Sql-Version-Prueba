﻿CREATE TABLE [aux].[SuggestRestrictionDetail] (
    [SuggestRestrictionDetailID] INT              NULL,
    [ProductID]                  INT              NULL,
    [ProductType]                INT              NULL,
    [StoreID]                    INT              NULL,
    [StoreType]                  INT              NULL,
    [Detail]                     INT              NULL,
    [BeginDate]                  DATE             NULL,
    [EndDate]                    DATE             NULL,
    [InsertionDate]              DATETIME         NULL,
    [SuggestRestrictionFileID]   INT              NULL,
    [SuggestRestrictionTypeID]   INT              NULL,
    [AnalysisID]                 UNIQUEIDENTIFIER NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail]
    ON [aux].[SuggestRestrictionDetail]([ProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_1]
    ON [aux].[SuggestRestrictionDetail]([StoreID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_2]
    ON [aux].[SuggestRestrictionDetail]([BeginDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_3]
    ON [aux].[SuggestRestrictionDetail]([EndDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_4]
    ON [aux].[SuggestRestrictionDetail]([SuggestRestrictionTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_5]
    ON [aux].[SuggestRestrictionDetail]([AnalysisID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_AnalysisID]
    ON [aux].[SuggestRestrictionDetail]([AnalysisID] ASC)
    INCLUDE([StoreType]);

