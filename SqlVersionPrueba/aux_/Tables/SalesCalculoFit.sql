﻿CREATE TABLE [aux].[SalesCalculoFit] (
    [sku]       INT           NOT NULL,
    [store_id]  SMALLINT      NOT NULL,
    [sale_date] SMALLDATETIME NOT NULL,
    [Quantity]  INT           NULL,
    CONSTRAINT [PK_SalesCalculoFit] PRIMARY KEY CLUSTERED ([sku] ASC, [store_id] ASC, [sale_date] ASC)
);

