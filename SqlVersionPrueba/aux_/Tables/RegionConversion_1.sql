﻿CREATE TABLE [aux].[RegionConversion] (
    [Region0_OLD] NVARCHAR (50) NOT NULL,
    [Region0_NEW] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_RegionConversion] PRIMARY KEY CLUSTERED ([Region0_OLD] ASC, [Region0_NEW] ASC)
);

