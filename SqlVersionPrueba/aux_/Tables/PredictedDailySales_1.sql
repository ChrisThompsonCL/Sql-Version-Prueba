﻿CREATE TABLE [aux].[PredictedDailySales] (
    [DateKey]      SMALLDATETIME NOT NULL,
    [ProductId]    INT           NOT NULL,
    [Date]         SMALLDATETIME NOT NULL,
    [Fit]          FLOAT (53)    NULL,
    [MaxValue]     FLOAT (53)    NULL,
    [MaxMean]      FLOAT (53)    NULL,
    [Corrected]    BIT           NULL,
    [CorrectedFit] FLOAT (53)    NULL
);

