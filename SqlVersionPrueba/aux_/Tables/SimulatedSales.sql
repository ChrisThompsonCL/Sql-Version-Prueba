﻿CREATE TABLE [aux].[SimulatedSales] (
    [Date]       DATE       NOT NULL,
    [SKU]        INT        NOT NULL,
    [Store]      INT        NOT NULL,
    [Uniforme01] FLOAT (53) NULL,
    [Rate]       FLOAT (53) NULL,
    [Venta]      INT        NULL,
    CONSTRAINT [PK_SimulatedSales_1] PRIMARY KEY CLUSTERED ([Date] ASC, [Store] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_Simulated]
    ON [aux].[SimulatedSales]([SKU] ASC);

