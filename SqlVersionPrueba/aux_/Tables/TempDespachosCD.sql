﻿CREATE TABLE [aux].[TempDespachosCD] (
    [Date]               DATE          NOT NULL,
    [Store]              INT           NOT NULL,
    [Bodega]             NVARCHAR (50) NOT NULL,
    [SKU]                INT           NOT NULL,
    [Sugerido]           INT           NULL,
    [Solicitado]         INT           NULL,
    [Corte]              INT           NULL,
    [Picking]            INT           NULL,
    [Disponible]         INT           NULL,
    [Pedido]             INT           NULL,
    [DisponibleSugerido] INT           NULL
);

