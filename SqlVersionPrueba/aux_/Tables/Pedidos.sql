﻿CREATE TABLE [aux].[Pedidos] (
    [Date]             DATE       NOT NULL,
    [SKU]              INT        NOT NULL,
    [Store]            INT        NOT NULL,
    [MinSug]           INT        NULL,
    [MaxSug]           INT        NULL,
    [Venta]            FLOAT (53) NULL,
    [Stock]            FLOAT (53) NULL,
    [Pedido]           FLOAT (53) NULL,
    [PedidoEfectivo]   FLOAT (53) NULL,
    [PedidoDisponible] FLOAT (53) NULL,
    [PedidoEnTransito] FLOAT (53) NULL,
    [VentaPerdida]     INT        NULL,
    CONSTRAINT [PK_Pedidos] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC, [Store] ASC)
);

