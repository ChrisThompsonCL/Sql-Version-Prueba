﻿CREATE TABLE [aux].[LocalesEspejoBorrar] (
    [NewStore]           FLOAT (53) NULL,
    [MirrorStoreFarma]   FLOAT (53) NULL,
    [ShareFarma]         FLOAT (53) NULL,
    [MirrorStoreConsumo] FLOAT (53) NULL,
    [ShareConsumo]       FLOAT (53) NULL,
    [MirrorStoreDermo]   FLOAT (53) NULL,
    [ShareDermo]         FLOAT (53) NULL,
    [BeginDate]          FLOAT (53) NULL
);

