﻿CREATE TABLE [aux].[DailySuggestMix] (
    [Date]     DATE   NOT NULL,
    [SKU]      BIGINT NOT NULL,
    [IdLevel1] INT    NULL,
    [IdLevel2] INT    NULL,
    [Store]    BIGINT NOT NULL,
    CONSTRAINT [PK_DailySuggestMix] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DailySuggestMix]
    ON [aux].[DailySuggestMix]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DailySuggestMix_1]
    ON [aux].[DailySuggestMix]([Store] ASC);

