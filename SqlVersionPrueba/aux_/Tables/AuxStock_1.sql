﻿CREATE TABLE [aux].[AuxStock] (
    [SKU]       INT           NOT NULL,
    [Store]     SMALLINT      NOT NULL,
    [StartDate] SMALLDATETIME NOT NULL,
    [EndDate]   SMALLDATETIME NOT NULL,
    [Duration]  INT           NULL
);

