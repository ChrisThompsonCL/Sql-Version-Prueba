﻿CREATE TABLE [aux].[PromotionFilesSimulated] (
    [PromotionFileID] INT                        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileID]          UNIQUEIDENTIFIER           DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [date]            SMALLDATETIME              NULL,
    [FileName]        NVARCHAR (50)              NOT NULL,
    [FileExtension]   NVARCHAR (10)              NOT NULL,
    [FullFileName]    AS                         ([FileName]+[FileExtension]),
    [FileData]        VARBINARY (MAX) FILESTREAM NULL,
    [UserName]        NVARCHAR (255)             NULL,
    PRIMARY KEY CLUSTERED ([PromotionFileID] ASC),
    UNIQUE NONCLUSTERED ([FileID] ASC)
) FILESTREAM_ON [FS_Attachments];

