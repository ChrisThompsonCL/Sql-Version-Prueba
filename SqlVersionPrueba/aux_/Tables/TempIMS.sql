﻿CREATE TABLE [aux].[TempIMS] (
    [Date]            NVARCHAR (50) NULL,
    [SKU]             INT           NULL,
    [UnidadesSB]      INT           NULL,
    [UnidadesCadenas] INT           NULL,
    [VentasSB]        INT           NULL,
    [VentasCadenas]   INT           NULL
);

