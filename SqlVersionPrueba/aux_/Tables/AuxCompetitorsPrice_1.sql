﻿CREATE TABLE [aux].[AuxCompetitorsPrice] (
    [SKU]  INT           NOT NULL,
    [Date] DATE          NOT NULL,
    [Fasa] NVARCHAR (50) NULL,
    [CV]   NVARCHAR (50) NULL,
    [Simi] NVARCHAR (50) NULL
);

