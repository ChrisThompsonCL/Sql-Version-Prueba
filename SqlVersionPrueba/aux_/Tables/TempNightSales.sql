﻿CREATE TABLE [aux].[TempNightSales] (
    [SKU]      INT  NOT NULL,
    [Store]    INT  NOT NULL,
    [Date]     DATE NOT NULL,
    [Quantity] INT  NOT NULL
);

