﻿CREATE TABLE [aux].[TempRutasTarde] (
    [Id]                INT           NULL,
    [EtCodigo]          NVARCHAR (10) NULL,
    [DiaSem]            NVARCHAR (20) NULL,
    [NumeroOla]         INT           NULL,
    [Store]             INT           NULL,
    [Prioridad]         INT           NULL,
    [PrioridadDespacho] INT           NULL,
    [MiSalud]           NVARCHAR (1)  NULL
);

