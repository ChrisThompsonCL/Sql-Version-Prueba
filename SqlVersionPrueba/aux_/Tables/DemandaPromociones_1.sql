﻿CREATE TABLE [aux].[DemandaPromociones] (
    [IdCombinacion]       UNIQUEIDENTIFIER NOT NULL,
    [DemandaSinPromocion] FLOAT (53)       NOT NULL,
    [DemandaConPromocion] FLOAT (53)       NOT NULL,
    [IsSimulated]         BIT              NOT NULL
);

