﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2017-12-12
-- Description:	Obtiene los últimos 50 archivos de restricciones cargados para el tipo de restricción especificado
-- Update: 2018-06-18, Se actualiza join a BD StdUsuarios
-- =============================================
CREATE FUNCTION [br].[GetSuggestRestrictionFiles]
(	
	@suggestRestrictionTypeID INT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT  
		f.SuggestRestrictionFileID
		, f.[FileName], f.[Date], ISNULL(u.Nombre, f.UserName) UserName
		, d.FechaInicio FechaInicio
		, d.FechaTermino FechaTermino 
        , f.Deleted, ISNULL(uDeleted.Nombre, f.DeletedBy) DeletedBy
	FROM (
		SELECT 
			MIN(BeginDate) FechaInicio, MAX(EndDate) FechaTermino, 
			d.SuggestRestrictionTypeID, d.SuggestRestrictionFileID
		FROM br.SuggestRestrictionDetail d 
		GROUP BY SuggestRestrictionTypeID, SuggestRestrictionFileID
	) d  
	INNER JOIN br.SuggestRestrictionFile f ON d.SuggestRestrictionFileID = f.SuggestRestrictionFileID 
	--LEFT JOIN users.PricingUser u ON f.UserName = u.Username  
	--LEFT JOIN users.PricingUser uDeleted ON f.DeletedBy = uDeleted.Username  
	LEFT JOIN StdUsuarios.dbo.usuario u ON f.Username = u.Id
	LEFT JOIN StdUsuarios.dbo.usuario uDeleted ON f.DeletedBy = uDeleted.Id

	WHERE d.SuggestRestrictionTypeID = @suggestRestrictionTypeID --AND f.Deleted = 0
	--order by [Date] desc
)
