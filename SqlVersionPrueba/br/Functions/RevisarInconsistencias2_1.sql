﻿-- =============================================
-- Author:		Gabriel Espinoza
-- Create date: 2017-12-18
-- Description:	Revisa las inconsistencias de las restricciones de sugerido
--				sobre la tabla [aux].[SuggestRestrictionDetail]
-- =============================================
CREATE FUNCTION [br].[RevisarInconsistencias2]
(	
	@analysisId UniqueIdentifier
)
RETURNS TABLE 
AS
RETURN 
(
	select DISTINCT 
	CASE 
		WHEN EndDate < BeginDate THEN 'Fecha `Desde` es posterior a fecha `Hasta`.'
		WHEN r.StoreType = 0 AND s.Store IS NULL THEN 'Local no existe.'
		WHEN r.ProductType = 0 AND p.SKU IS NULL THEN 'Producto no existe.'
		WHEN r.EndDate > DATEADD(YEAR, 1, GETDATE()) THEN 'Fecha `Hasta` es posterior a 1 año.'
		WHEN r.BeginDate < CAST(GETDATE() AS DATE) THEN 'Fecha `Desde` es anterior a hoy.'
		WHEN r.Detail = 0 THEN 'Sugerido es 0.'
		WHEN r.EndDate > DATEADD(MONTH, 2, GETDATE()) THEN 'Fecha `Hasta` es posterior a 2 meses.'
	END Alerta,
	CAST(
		CASE 
			WHEN EndDate < BeginDate THEN 1
			WHEN r.StoreType = 0 AND s.Store IS NULL THEN 0
			WHEN r.ProductType = 0 AND p.SKU IS NULL THEN 0
			WHEN r.EndDate > DATEADD(YEAR, 1, GETDATE()) THEN 1
			WHEN r.BeginDate < CAST(GETDATE() AS DATE) THEN 1
			WHEN r.Detail = 0 THEN 0
			WHEN r.EndDate > DATEADD(MONTH, 2, GETDATE()) THEN 0
		END AS BIT
	) EsAlertaBloqueante
	from [aux].[SuggestRestrictionDetail] r
	LEFT JOIN stores.FullStores s ON r.StoreID = s.Store
	LEFT JOIN products.FullProducts p ON r.ProductID = p.SKU
	WHERE r.AnalysisID = @analysisId
)
