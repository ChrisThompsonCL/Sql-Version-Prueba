﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 08-01-2018
-- Description:	Insertar o actualizar Nivel de Servicio y días de sobreprotección mediante carga de excel.
-- =============================================

CREATE PROCEDURE [br].[UpdateParametrosInventarioObjetivo]
@id_proceso     uniqueidentifier,
@username       nvarchar(50),
@date           datetime
AS
BEGIN

SET NOCOUNT ON;

DECLARE @ResultadoTemporal TABLE (
SKU INT,
NivelServicio FLOAT,
DiasSP INT
)

INSERT INTO @ResultadoTemporal (SKU, NivelServicio, DiasSP)
SELECT p.SKU, p.NivelServicio, p.DiasSP
	FROM [Salcobrand].[aux].[ParametrosInventarioObjetivoLoad] p
	where (p.id_proceso = @id_proceso)

	   MERGE INTO [minmax].[SuggestParameters] as psf
	   USING @ResultadoTemporal AS p
	   on p.sku = psf.sku
	   WHEN MATCHED THEN
			UPDATE SET psf.safetydays = p.DiasSP, psf.LastUpdate = @date, psf.UserResp = @username
	   WHEN NOT MATCHED THEN
		    INSERT (sku, safetydays, LastUpdate, UserResp)
		    VALUES (p.sku,p.DiasSP, @date, @username);
		
	   MERGE INTO [minmax].[SuggestParameters] as sl
	   USING @ResultadoTemporal AS p
	   on p.sku = sl.sku
	   WHEN MATCHED THEN
			UPDATE SET sl.[ServiceLevelDesc] = p.NivelServicio,  sl.LastUpdate = @date, sl.UserResp = @username
	   WHEN NOT MATCHED THEN
		    INSERT (sku, [ServiceLevelDesc], LastUpdate, UserResp)
		    VALUES (p.sku, p.NivelServicio, @date, @username);

	   TRUNCATE TABLE [Salcobrand].[aux].[ParametrosInventarioObjetivoLoad]
END
