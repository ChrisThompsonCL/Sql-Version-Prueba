﻿
-- =============================================
-- Author:		Carlos Astroza
-- Create date: 08-01-2018
-- Description:	Obtener Restricciones por SKU - Local
-- =============================================

CREATE PROCEDURE [br].[GetSuggestRestrictionFiles_SKUStore]
@SKU   INT,
@STORE INT,
@RESTR INT
AS
BEGIN

SET NOCOUNT ON;


SELECT
s.SuggestRestrictionDetailID ID, 
ProductID SKU,
CASE WHEN s.StoreType = 3 THEN '-' ELSE CAST(StoreID AS nvarchar) END Local,
s.Detail Restriccion,
cast (s.InsertionDate as date ) Date,
s.BeginDate FechaInicio,
s.EndDate FechaTermino,
s.SuggestRestrictionFileID,
sr.Name NombreRestriccion,
u.Nombre Usuario,
f.FileName
FROM [view].[SuggestRestrictionDetail] s
LEFT JOIN br.SuggestRestrictionFile f on s.SuggestRestrictionFileID = f.SuggestRestrictionFileID
LEFT JOIN StdUsuarios.dbo.Usuario u on f.UserName = u.id COLLATE Modern_Spanish_CI_AS
--LEFT JOIN users.PricingUser u on f.UserName = u.Username
INNER JOIN br.SuggestRestrictionType sr on s.SuggestRestrictionTypeID = sr.SuggestRestrictionTypeID
WHERE --ISNULL(EndDate, '9999-12-31') >= GETDATE() AND
 (@SKU IS NULL OR @SKU = s.ProductID)
--AND (@STORE IS NULL OR @STORE = s.StoreID)
and (s.StoreType=3 OR (s.StoreType<>3 and (s.StoreID= @STORE or @STORE is null)))
AND (@RESTR IS NULL OR @RESTR = s.SuggestRestrictionTypeID)
order by s.BeginDate desc
END
