﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2017-04-04
-- Description:	combinaciones a las que no aplica la restricción dado que están fuera del mix activo
-- =============================================
CREATE PROCEDURE [br].[OutsideMixSuggestRestriction]
	-- Add the parameters for the stored procedure here
	@suggestFileid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @hasStores bit = (select top 1 case when StoreType =0 then 1 else 0 end from br.SuggestRestrictionDetail where SuggestRestrictionFileID =@suggestFileid)
	
	if(@hasStores=1)
	begin
		select srd.ProductId SKU, srd.StoreID [Local], ls.Suggest SugeridoActual, srd.Detail Restricción
		from br.SuggestRestrictionDetail srd
		left join series.LastStock ls on ls.sku=srd.ProductID and srd.StoreID=ls.Store
		where NOT EXISTS (select 1 from aux.DailySuggestMix dsm where dsm.SKU=srd.ProductID and srd.StoreID=dsm.Store)
			and SuggestRestrictionFileID =@suggestFileid
	end
	else
	begin
		select srd.ProductId SKU, ls.Store [Local], ls.Suggest SugeridoActual, srd.Detail Restricción
		from br.SuggestRestrictionDetail srd
		inner join series.LastStock ls on ls.sku=srd.ProductID
		where NOT EXISTS (select 1 from aux.DailySuggestMix dsm where dsm.SKU=srd.ProductID and dsm.Store=ls.Store)
			and SuggestRestrictionFileID =@suggestFileid
	end

END
