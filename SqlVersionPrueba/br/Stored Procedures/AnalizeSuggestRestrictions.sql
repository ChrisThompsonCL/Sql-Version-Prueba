﻿-- =============================================
-- Author:		Gabriel Espinoza Erices y Dani!! <3
-- Create date: 2012-05-31
-- Description:	Obtiene las alzas y bajas totales de las restricciones de sugeridos.
--		Utiliza la tabla aux.SuggestRestrictionDetail y series.LastStock
-- =============================================
CREATE PROCEDURE [br].[AnalizeSuggestRestrictions]
	@AnalysisID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @date date = GETDATE()

	SELECT 
		LogisticAreaID 
		, SUM([Monday_alza]) AS 'Lunes_Alzas', SUM([Monday_baja])  AS 'Lunes_Bajas'
		, SUM([Tuesday_alza])  AS 'Martes_Alzas', SUM([Tuesday_baja])  AS 'Martes_Bajas'
		, SUM([Wednesday_alza]) AS 'Miercoles_Alzas', SUM([Wednesday_baja]) AS 'Miercoles_Bajas'
		, SUM([Thursday_alza]) AS 'Jueves_Alzas', SUM([Thursday_baja]) AS 'Jueves_Bajas'
		, SUM([Friday_alza]) AS 'Viernes_Alzas', SUM([Friday_baja]) AS 'Viernes_Bajas'
		, SUM([Saturday_alza]) AS 'Sabado_Alzas', SUM([Saturday_baja]) AS 'Sabado_Bajas'
		, SUM([Sunday_alza]) AS 'Domingo_Alzas', SUM([Sunday_baja]) AS 'Domingo_Bajas'
	FROM
	(
		SELECT
			LogisticAreaID
			, DATENAME(DW, ProximaReposicion) + '_alza' as DayWeekAlza
			, DATENAME(DW, ProximaReposicion) + '_baja' as DayWeekBaja
			, AlzasTotales
			, BajasTotales
		FROM (
			select LogisticAreaId, ProximaReposicion
				, sum(case when NewSuggest>Suggest then NewSuggest-Suggest else 0 end) AlzasTotales
				, sum(case when NewSuggest<Suggest then Suggest-NewSuggest else 0 end) BajasTotales
			from (
				SELECT h.LogisticAreaId, h.ProximaReposicion, h.Suggest,  NewSuggest = a1.RestrictedSuggest
				FROM (
					select a.LogisticAreaId, a.SKU, a.Store, a.ProximaReposicion, a.Detail, a.Suggest
						, case when a.SuggestRestrictionTypeID in (3,4,8,12) then a.Detail else null end minimo
						, case when a.SuggestRestrictionTypeID in (2) then a.Detail else null end fijo
						, case when a.SuggestRestrictionTypeID in (9,10,11) then a.Detail else null end planograma
						, case when a.SuggestRestrictionTypeID in (5,6,7) then CEILING(a.Detail*1.0/2.0)+1 else null end mediodun
						, case when a.SuggestRestrictionTypeID in (13,14) then a.Detail else null end maximo
						, case when a.SuggestRestrictionTypeID in (15) then a.Detail else null end cajatira
						, case when a.SuggestRestrictionTypeID in (16) then a.Detail else null end minimoestricto 
					from (
						select ls.SKU, ls.Store, p.LogisticAreaId, Suggest, min(dateadd(day,1,r.[Date])) as ProximaReposicion  
							, srd.SuggestRestrictionTypeID, srd.Detail
						from series.LastStock ls
						inner join products.Products p on p.SKU = ls.SKU
						inner join (select * from minmax.Replenishment r where r.[Days] > 0) r on r.Store=ls.Store and r.LogisticAreaId=p.LogisticAreaId
						inner join aux.SuggestRestrictionDetail srd on srd.ProductID=ls.SKU and ((srd.StoreType=0 and srd.StoreID=ls.Store) OR  srd.StoreType=3) --and a.ProximaReposicion BETWEEN srd.BeginDate AND ISNULL(srd.EndDate, '21000101')
						where  AnalysisID = @AnalysisID and r.[Date] BETWEEN @date AND DATEADD(WEEK, 1, @date) 
						group by ls.SKU, ls.Store, p.LogisticAreaId, Suggest, srd.BeginDate, srd.EndDate, srd.SuggestRestrictionTypeID, srd.Detail
						having min(dateadd(day,1,r.[Date])) BETWEEN srd.BeginDate AND ISNULL(srd.EndDate, '21000101')
					) a 
					inner join br.SuggestRestrictionType t ON a.SuggestRestrictionTypeID = t.SuggestRestrictionTypeID
				) h
				cross apply [function].ApplyRestriction(suggest, minimo, fijo, planograma, medioDUN, maximo, cajatira, minimoestricto) a1
			) f
			GROUP BY LogisticAreaId, ProximaReposicion
		) a
	) tbl
	PIVOT 
	(
		SUM(AlzasTotales) FOR DayWeekAlza IN ([Monday_alza], [Tuesday_alza], [Wednesday_alza], [Thursday_alza], [Friday_alza], [Saturday_alza], [Sunday_alza])
	) pvt
	PIVOT 
	(
		SUM(BajasTotales) FOR DayWeekBaja IN ([Monday_baja], [Tuesday_baja], [Wednesday_baja], [Thursday_baja], [Friday_baja], [Saturday_baja], [Sunday_baja])
	) pvt2
	GROUP BY LogisticAreaID


END
--EXEC [br].[AnalizeSuggestRestrictions] '25D146C1-5A63-4699-AE1F-8113A2A06CE2'



