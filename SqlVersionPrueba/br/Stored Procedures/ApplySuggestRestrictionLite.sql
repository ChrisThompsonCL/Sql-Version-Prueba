﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-12-28
-- Description:	Aplica restricciones recién cargadas (o eliminadas). Solo se aplica en tabla final del DailySuggest
-- =============================================
CREATE PROCEDURE [br].[ApplySuggestRestrictionLite] 
	-- Add the parameters for the stored procedure here

AS
BEGIN

	--update s set [Min]=a1.RestrictedSuggest
	--	, [Max]=a2.RestrictedSuggest
	--from replenishment.NextSuggest s
	--inner join [view].SuggestRestrictionToday srt on srt.SKU=s.SKU and srt.Store=s.Store
	--cross apply [function].ApplySuggestRestriction([Min], Minimo, Fijo, Planograma, MedioDUN) a1
	--cross apply [function].ApplySuggestRestriction([Max], Minimo, Fijo, Planograma, MedioDUN) a2

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	update s set [Max]=a1.RestrictedSuggest + ([Max]-[Min])
		, [Min]=a1.RestrictedSuggest
		, Minimo=srt.Minimo, Fijo=srt.Fijo, Planograma=srt.Planograma, MedioDUN=srt.MedioDUN , Maximo=srt.Maximo, CajaTira=srt.CajaTira, FijoEstricto=srt.FijoEstricto
		--select *
	from replenishment.NextSuggest s
	--inner join [view].SuggestRestrictionToday srt on srt.SKU=s.SKU and srt.Store=s.Store
	inner join #SuggestRestrictionToday srt on srt.SKU=s.SKU and srt.Store=s.Store
	cross apply [function].ApplyRestriction([Min], srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a1

	update ps set [Max] = [Min]
	from replenishment.NextSuggest ps
	where ps.[Min] > ps.[Max]

END


