﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 18-06-2018
-- Description:	Obtiene restricciones que vencen hoy, o en 7 días más.
-- =============================================

CREATE PROCEDURE [br].[GetRestrictionsToReminder]
AS
BEGIN

SET NOCOUNT ON;

	SELECT  
		TOP 3
		--f.SuggestRestrictionFileID,
		  f.[FileName]
		, f.[Date]
		, ISNULL(u.Nombre, f.UserName) UserName
		, CASE WHEN F.UserName = 'rortega' THEN 'dvalenzuela@sb.cl' ELSE u.Email END Email
		, d.FechaInicio FechaInicio
		, d.FechaTermino FechaTermino
		, CAST(CASE WHEN d.FechaTermino = CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT) VenceHoy 
		, TY.Name TipoRestriccion
	FROM (
		SELECT 
			MIN(BeginDate) FechaInicio, MAX(EndDate) FechaTermino, 
			d.SuggestRestrictionTypeID, d.SuggestRestrictionFileID
		FROM br.SuggestRestrictionDetail d 
		GROUP BY SuggestRestrictionTypeID, SuggestRestrictionFileID
	) d  
	INNER JOIN br.SuggestRestrictionFile f ON d.SuggestRestrictionFileID = f.SuggestRestrictionFileID 
	LEFT JOIN StdUsuarios.dbo.usuario u ON f.Username = u.Id  COLLATE Modern_Spanish_CI_AS
	INNER JOIN br.SuggestRestrictionType ty ON d.SuggestRestrictionTypeID = ty.SuggestRestrictionTypeID
	WHERE f.Deleted = 0
	AND (d.FechaTermino = CAST(GETDATE() AS DATE)) OR (d.FechaTermino = CAST(DATEADD(DAY,+7,GETDATE()) AS DATE))
	ORDER BY FechaTermino ASC

END
