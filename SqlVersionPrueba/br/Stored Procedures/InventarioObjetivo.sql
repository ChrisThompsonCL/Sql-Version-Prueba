﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 08-01-2018
-- Description:	Insertar o actualizar Nivel de Servicio y días de sobreprotección
-- =============================================

CREATE PROCEDURE [br].[InventarioObjetivo]
@SKU       INT,
@DIV       INT,
@CAT       INT,
@SUB_CAT   INT,
@RAN       VARCHAR(MAX),
@DIAS_SP   INT,
@NS		   FLOAT,
@FLAGS	   VARCHAR(MAX),
@USERNAME  NVARCHAR(50),
@DATE      DATETIME

AS
BEGIN

SET NOCOUNT ON;

DECLARE @ResultadoTemporal TABLE (
SKU INT
)

declare @query NVARCHAR(MAX) = 

	'SELECT sg.[SKU]
	FROM [minmax].[SuggestParameters] sg
	INNER JOIN [products].[FullProducts] p ON sg.[SKU] = p.[SKU]
	
	WHERE (p.[SKU] = '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' or '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' is null)
	AND   (p.[DivisionUnificadaId] = '+ISNULL(CAST(@DIV AS varchar), 'NULL')+' or '+ISNULL(CAST(@DIV AS varchar), 'NULL')+' is null)
	AND   (p.[CategoriaUnificadaId]          = '+ISNULL(CAST(@CAT AS varchar), 'NULL')+' or '+ISNULL(CAST(@CAT AS varchar), 'NULL')+' is null)          
	AND   (p.[SubcategoriaUnificadaId]          = '+ISNULL(CAST(@SUB_CAT AS varchar), 'NULL')+' or '+ISNULL(CAST(@SUB_CAT AS varchar), 'NULL')+' is null)                                                                               
	';

	IF @RAN is not null BEGIN
	SET @query += @RAN;
    END
	
	IF @FLAGS is not null BEGIN
	SET @query += @FLAGS;
    END

	--AND   (p.[TotalRanking]        = '+ISNULL(CAST(@RAN AS varchar), 'NULL')+' or '+ISNULL(CAST(@RAN AS varchar), 'NULL')+' is null)

INSERT INTO @ResultadoTemporal (SKU)
EXEC sp_executesql @query;

SELECT * FROM @ResultadoTemporal

  IF @DIAS_SP IS NOT NULL  BEGIN
  MERGE INTO [minmax].[SuggestParameters] as psf
  USING @ResultadoTemporal AS p
  on p.sku = psf.sku
  WHEN MATCHED THEN
  UPDATE SET psf.safetydays = @DIAS_SP, psf.LastUpdate = @date, psf.UserResp = @username
  WHEN NOT MATCHED THEN
  INSERT (sku, safetydays, LastUpdate, UserResp)
  VALUES (p.sku, @DIAS_SP,@date,@username);
  END
  
  IF @NS IS NOT NULL AND @NS != 0 BEGIN		
  MERGE INTO [minmax].[SuggestParameters] as sl
  USING @ResultadoTemporal AS p
  ON p.sku = sl.sku
  WHEN MATCHED THEN
  UPDATE SET sl.[ServiceLevelDesc] = @NS, sl.LastUpdate = @date, sl.UserResp = @username
  WHEN NOT MATCHED THEN
  INSERT (sku, [ServiceLevelDesc], LastUpdate, UserResp)
  VALUES (p.sku, @NS,@date,@username);
  END

END
