﻿

-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2012-06-26
-- Description:	Confirma la carga de restricciones de sugeridos
-- =============================================
CREATE PROCEDURE [br].[ConfirmSuggestRestrictionUpload]
	@AnalysisID Uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @MustOverrideDate BIT; 
DECLARE @date date = GETDATE()

SET @MustOverrideDate = (
	select distinct t.UpdateDateOnNewFile from 
	aux.SuggestRestrictionDetail d
	INNER JOIN br.SuggestRestrictionType t ON d.SuggestRestrictionTypeID = t.SuggestRestrictionTypeID
	WHERE d.AnalysisID = @AnalysisID);

IF @MustOverrideDate = 1 BEGIN
	UPDATE br.SuggestRestrictionDetail SET
		EndDate = DATEADD(DAY, -1, @date)
	WHERE
		EndDate is null AND
		SuggestRestrictionTypeID = (SELECT distinct SuggestRestrictionTypeID FROM aux.SuggestRestrictionDetail WHERE AnalysisID = @AnalysisID)
END


INSERT INTO br.SuggestRestrictionDetail 
	(ProductID, ProductType, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate, SuggestRestrictionFileID, SuggestRestrictionTypeID)
SELECT  
    ProductID, ProductType, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate, SuggestRestrictionFileID, SuggestRestrictionTypeID  
FROM aux.SuggestRestrictionDetail 
WHERE AnalysisID = @AnalysisID

INSERT INTO minmax.SkuNewRestrictionsCurrent
SELECT distinct ProductID
FROM aux.SuggestRestrictionDetail 
WHERE AnalysisID = @AnalysisID
	AND ProductID NOT IN (select SKU from minmax.SkuNewRestrictionsCurrent)

INSERT INTO minmax.SkuNewRestrictionsNew
SELECT distinct ProductID
FROM aux.SuggestRestrictionDetail 
WHERE AnalysisID = @AnalysisID
	AND ProductID NOT IN (select SKU from minmax.SkuNewRestrictionsNew)

DELETE FROM aux.SuggestRestrictionDetail WHERE AnalysisID = @AnalysisID          

-- DANI A.: comento la aplicación de restricciones en cada momento que se carga, ahora irá solo al final cuando se carga el sugerido.
--EXEC [br].[ApplySuggestRestrictionLite]    

END


