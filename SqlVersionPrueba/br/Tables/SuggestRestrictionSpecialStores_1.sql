﻿CREATE TABLE [br].[SuggestRestrictionSpecialStores] (
    [Store]     INT  NOT NULL,
    [BeginDate] DATE NOT NULL,
    CONSTRAINT [PK_SuggestRestrictionSpecialStores] PRIMARY KEY CLUSTERED ([Store] ASC)
);

