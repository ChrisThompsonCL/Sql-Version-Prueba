﻿CREATE TABLE [br].[SuggestRestrictionType] (
    [SuggestRestrictionTypeID] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                     NVARCHAR (50)  NOT NULL,
    [Description]              NVARCHAR (200) NOT NULL,
    [StoreType]                INT            NULL,
    [ProductType]              INT            NULL,
    [DefaultStore]             INT            NULL,
    [ActionID]                 INT            CONSTRAINT [DF_SuggestRestrictionType_ActionID] DEFAULT ((1)) NOT NULL,
    [UpdateDateOnNewFile]      BIT            CONSTRAINT [DF_SuggestRestrictionType_UpdateDateOnNewFile] DEFAULT ((0)) NOT NULL,
    [Show]                     BIT            CONSTRAINT [DF_SuggestRestrictionType_Show] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__SuggestR__05BDE5F91DBB5747] PRIMARY KEY CLUSTERED ([SuggestRestrictionTypeID] ASC),
    CONSTRAINT [FK_SuggestRestrictionType_SuggestRestrictionAction] FOREIGN KEY ([ActionID]) REFERENCES [br].[SuggestRestrictionAction] ([ActionID])
);

