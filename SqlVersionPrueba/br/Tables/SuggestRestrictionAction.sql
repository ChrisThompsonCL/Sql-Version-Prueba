﻿CREATE TABLE [br].[SuggestRestrictionAction] (
    [ActionID] INT           NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SuggestRestrictionAction] PRIMARY KEY CLUSTERED ([ActionID] ASC)
);

