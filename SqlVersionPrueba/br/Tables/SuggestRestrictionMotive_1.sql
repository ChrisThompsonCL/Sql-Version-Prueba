﻿CREATE TABLE [br].[SuggestRestrictionMotive] (
    [SuggestRestrictionMotiveID] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                       NVARCHAR (50)  NOT NULL,
    [Description]                NVARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([SuggestRestrictionMotiveID] ASC)
);

