﻿CREATE TABLE [br].[SuggestRestrictionFile] (
    [SuggestRestrictionFileID]   INT                        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SuggestRestrictionMotiveID] INT                        NOT NULL,
    [FileID]                     UNIQUEIDENTIFIER           ROWGUIDCOL NOT NULL,
    [FileName]                   NVARCHAR (50)              NOT NULL,
    [FileExtension]              NVARCHAR (10)              NOT NULL,
    [FullFileName]               AS                         ([FileName]+[FileExtension]),
    [FileData]                   VARBINARY (MAX) FILESTREAM NULL,
    [Date]                       DATETIME                   CONSTRAINT [DF_SuggestRestrictionFile_Date] DEFAULT (getdate()) NULL,
    [UserName]                   NVARCHAR (255)             NULL,
    [Deleted]                    BIT                        CONSTRAINT [DF_SuggestRestrictionFile_Deleted] DEFAULT ((0)) NOT NULL,
    [DeletedBy]                  NVARCHAR (255)             NULL,
    PRIMARY KEY CLUSTERED ([SuggestRestrictionFileID] ASC),
    UNIQUE NONCLUSTERED ([FileID] ASC),
    UNIQUE NONCLUSTERED ([FileID] ASC)
) FILESTREAM_ON [FS_Attachments];

