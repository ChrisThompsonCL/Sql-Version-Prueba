﻿CREATE TABLE [br].[SuggestRestrictionDetail] (
    [SuggestRestrictionDetailID] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductID]                  INT      NOT NULL,
    [ProductType]                INT      NOT NULL,
    [StoreID]                    INT      NOT NULL,
    [StoreType]                  INT      NOT NULL,
    [Detail]                     INT      NOT NULL,
    [BeginDate]                  DATE     NOT NULL,
    [EndDate]                    DATE     NULL,
    [InsertionDate]              DATETIME DEFAULT (getdate()) NOT NULL,
    [SuggestRestrictionFileID]   INT      NOT NULL,
    [SuggestRestrictionTypeID]   INT      NOT NULL,
    PRIMARY KEY CLUSTERED ([SuggestRestrictionDetailID] ASC),
    CONSTRAINT [FK_SuggestRestrictionDetail_SuggestRestrictionFile] FOREIGN KEY ([SuggestRestrictionFileID]) REFERENCES [br].[SuggestRestrictionFile] ([SuggestRestrictionFileID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_SuggestRestrictionDetail_SuggestRestrictionType] FOREIGN KEY ([SuggestRestrictionTypeID]) REFERENCES [br].[SuggestRestrictionType] ([SuggestRestrictionTypeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_6]
    ON [br].[SuggestRestrictionDetail]([SuggestRestrictionFileID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_7]
    ON [br].[SuggestRestrictionDetail]([SuggestRestrictionTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_8]
    ON [br].[SuggestRestrictionDetail]([ProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_9]
    ON [br].[SuggestRestrictionDetail]([StoreID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_BeginDate]
    ON [br].[SuggestRestrictionDetail]([BeginDate] ASC)
    INCLUDE([ProductID], [ProductType], [StoreID], [StoreType], [Detail], [EndDate], [InsertionDate], [SuggestRestrictionTypeID]);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_1]
    ON [br].[SuggestRestrictionDetail]([SuggestRestrictionTypeID] ASC, [BeginDate] ASC)
    INCLUDE([ProductID], [ProductType], [StoreID], [StoreType], [Detail], [EndDate], [InsertionDate]);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestRestrictionDetail_BLA]
    ON [br].[SuggestRestrictionDetail]([ProductID] ASC, [BeginDate] ASC, [SuggestRestrictionTypeID] ASC)
    INCLUDE([StoreID], [StoreType], [Detail], [EndDate]);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [br].[SuggestRestrictionDetail]([ProductID] ASC, [InsertionDate] ASC, [SuggestRestrictionTypeID] ASC)
    INCLUDE([StoreID], [StoreType], [Detail], [BeginDate], [EndDate]);

