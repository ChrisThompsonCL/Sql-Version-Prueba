﻿CREATE TABLE [br].[SuggestRestrictionColumns] (
    [SuggestRestrictionColumnsID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RowName]                     NVARCHAR (20) NOT NULL,
    [RowDataType]                 NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK__SuggestR__2B9738D419EAC663] PRIMARY KEY CLUSTERED ([SuggestRestrictionColumnsID] ASC)
);

