﻿CREATE TABLE [br].[SuggestRestrictionColumnsByType] (
    [SuggestRestrictionColumnsID] INT NOT NULL,
    [SuggestRestrictionTypeID]    INT NOT NULL,
    CONSTRAINT [PK_SuggestRestrictionColumnsByType] PRIMARY KEY CLUSTERED ([SuggestRestrictionColumnsID] ASC, [SuggestRestrictionTypeID] ASC),
    CONSTRAINT [FK_SuggestRestrictionColumns_ByType] FOREIGN KEY ([SuggestRestrictionColumnsID]) REFERENCES [br].[SuggestRestrictionColumns] ([SuggestRestrictionColumnsID]),
    CONSTRAINT [FK_SuggestRestrictionType_ByColumns] FOREIGN KEY ([SuggestRestrictionTypeID]) REFERENCES [br].[SuggestRestrictionType] ([SuggestRestrictionTypeID])
);

