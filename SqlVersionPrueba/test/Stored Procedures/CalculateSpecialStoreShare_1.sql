﻿-- =============================================
-- Author:		Carlos Quappe
-- Create date: 2017-10-02
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [test].[CalculateSpecialStoreShare] 
	-- Add the parameters for the stored procedure here
	@specialDate date, @mirrorDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    /*cáluclo de proporciones de Store por fecha mirror*/
	SELECT City, [Date], sum([Quantity]) as Venta, count(distinct stores.store) as NumStores
	into #temp1
	FROM [series].[Sales] sales 
	inner join [stores].[FullStores] stores on sales.store = stores.store 
	where sales.[Date]=@mirrorDate
	group by city , [Date]

	SELECT City, [Date], Venta, NumStores, cast(Venta as float)/(select SUM(venta) from #temp1) ShareCity
	into #ciudad_dia
	FROM #temp1
	where Venta>=0

	select City, sales.Store, sum(Quantity) as Venta
	into #temp2
	from [series].[Sales] sales 
	inner join [stores].[FullStores] stores on sales.Store = stores.Store
	where date between DATEADD(d, -30, @specialDate) and @specialDate 
	group by City, sales.Store
	order by city

	truncate table aux.SpecialShareStores

	insert into aux.SpecialShareStores
	select Store, isnull(a.Venta,0)  VentaStore , cast(isnull(a.Venta,0) as float)/(select SUM(venta) from #temp2) ShareStore, @specialDate FechaStore
		, a.City, isnull(b.Venta,0) VentaCity, isnull(b.NumStores,0) NumStoresCity, isnull(b.ShareCity,0) ShareCity, @mirrorDate FechaCity
	from #temp2 a 
	inner join #ciudad_dia b on isnull(b.City,'sin_cuidad')=isnull(a.City,'sin_cuidad')

	drop table #temp1 
	drop table #temp2 
	drop table #ciudad_dia

END
