﻿
CREATE PROCEDURE [test].[TruncateLowSale] 
	@currentdate smalldatetime = NULL
AS
BEGIN

--drop table #weekSale
CREATE TABLE #weekSale (sku int,store int, quantity float)
CREATE INDEX AW_Index ON #weekSale (sku) 
CREATE INDEX AW2_Index ON #weekSale (store) 

-- insertar todas las combinaciones que enviaremos 
insert into #weekSale (sku,store)
select distinct sku,store from [test].PricingSuggest

update #weekSale
-- máximo entre la venta semanal promedio de los últimos 3 meses y de los siguientes 3 meses del año anterior
set quantity = convert(float,T.quantity)/(4*3) from #weekSale w inner join
(
	select sku,Store,MAX(quantity) quantity from
	(
		-- venta de los últimos 3 meses
		select s.sku,s.store,sum(quantity) quantity from [series].[Sales] s
		inner join 
		( select sku,store from #weekSale ) sug on sug.sku = s.sku and sug.store = s.store 
		where s.date between dateadd(day,-28*3,@currentdate) and @currentdate group by s.sku,s.store
		union all
		-- venta de los siguientes 3 meses del año anterior
		select s.sku,s.store,sum(quantity) quantity from [series].[Sales] s
		inner join 
		( select sku,store from #weekSale ) sug on sug.sku = s.sku and sug.store = s.store 
		where s.date between dateadd(day,-28*13,@currentdate) and dateadd(day,-28*10,@currentdate)  group by s.sku,s.store
	) h 
	group by sku,store
) T
on w.sku = t.sku and w.store = t.store

update #weekSale
set quantity = 0 where quantity is null

-- reglas C,D y NULL no más de 4 semanas de venta
update p set suggestMin = case when p.SuggestMin>w.cotaMin then w.cotaMin else p.SuggestMin end,
p.SuggestMax = case when p.SuggestMax>w.cotaMax then w.cotaMax else p.SuggestMax end from [test].PricingSuggest p
inner join (select sku,store,
case when CEILING(4*quantity) <= 0 then 1 else CEILING(4*quantity) end cotaMin,
case when CEILING(5*quantity) <= 0 then 2 else CEILING(5*quantity) end  cotaMax
from #weekSale 
where sku in (select sku from products.Products where (TotalRanking in ('C','D') OR TotalRanking is null)) ) w
on w.sku = p.sku and w.store = p.store

-- reglas para A y B
--update p set p.suggestMin = case when p.SuggestMin>w.cotaMin then w.cotaMin else p.SuggestMin end,
--p.SuggestMax = case when p.SuggestMax>w.cotaMax then w.cotaMax else p.SuggestMin end from minmax.PricingSuggest p
--inner join (select sku,store,
--case when CEILING(2*quantity) <= 0 then 3 else CEILING(2*quantity) end  cotaMin,
--case when CEILING(3*quantity) <= 0 then 4 else CEILING(3*quantity) end  cotaMax from #weekSale 
--where sku in (select sku from products.FullProducts where TotalRanking in ('A','B')) ) w
--on w.sku = p.sku and w.store = p.store

END
