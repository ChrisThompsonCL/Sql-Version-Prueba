﻿

-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2012-04-18
-- Description:	Ejecuta todos los procesos para dejar el sugerido con min/max del dia calculado
-- =============================================
CREATE PROCEDURE [test].[DailySuggest] 
	@date smalldatetime = NULL
AS
BEGIN
	DECLARE @msg VARCHAR(100) 
	SET NOCOUNT ON;
	
	Declare @monday datetime,@sunday datetime
	select @monday=dbo.GetMonday(@date)
	select @sunday=dateadd(day,-1,@monday)

	if @date = @monday
	begin
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando Carga Lunes...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
		truncate table [test].[SuggestAllCurrent]
		insert into [test].[SuggestAllCurrent]
		select * from [test].[SuggestAllNew]

		truncate table [test].[PilotoSugeridoSemanalCurrent]
		insert into [test].[PilotoSugeridoSemanalCurrent]
		select * from [test].[PilotoSugeridoSemanalNew]

		declare @minDate smalldatetime=(select min(date) from [test].[RatesNew])
		insert into [test].[RatesNew]
		select * from [test].[RatesCurrent]
		where [Date] between dateadd(day,-7,@minDate) and dateadd(day,-1,@minDate)

		truncate table [test].[RatesCurrent]
		insert into [test].[RatesCurrent]
		select * from [test].[RatesNew]

		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga Lunes ejecutada Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	-- cargar el pricingsuggest
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando Carga en PricingSuggest...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	truncate table [test].PricingSuggest
	INSERT INTO [test].[PricingSuggest]([SKU],[Store],[Date],[SuggestMin],[SuggestMax], [Days], [Rate])
	select sac.[SKU],sac.[Store],sac.[Date],sac.[SuggestMin],sac.[SuggestMax], sac.[Days], sac.Rate
	from [test].[SuggestAllCurrent] sac 
	inner join aux.DailySuggestMix ds on ds.SKU=sac.SKU and ds.Store=sac.Store
	--where sac.[Date] = @date
	where sac.[Date] between @date and dateadd(day, 2, @date)

	-- Incluir sugerido semanal
	UPDATE ps SET SuggestMin=p.SuggestMin, SuggestMax=p.SuggestMax, [Days]=p.[Days], Rate=p.Rate
	FROM [test].[PricingSuggest] ps
	INNER JOIN [test].[PilotoSugeridoSemanalCurrent] p ON p.SKU=ps.SKU AND p.[Date]=ps.[Date]


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga en PricingSuggest Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- Aplicar los ajuster por venta
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando minmax.TruncateLowSale...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	Execute [test].TruncateLowSale @date
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Minmax.TruncateLowSale Listo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- ajustar min=max para todos los productos menos los Marcas Propias y productos en prueba de punto de reorden
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes (Min = Max)...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	update p set suggestMin = suggestMin+1,suggestMax = suggestMin+1 
	from [test].PricingSuggest p
	where p.SKU not in (select SKU from products.Products where ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers))-- union all select SKU from temp.PruebaMinMaxDistintoNS)
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ajuste Listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
	--arreglo para marcas propias
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes para Marcas Propias...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	update p set suggestMin = suggestMax+1,suggestMax = suggestMax+1 
	from [test].PricingSuggest p where p.SKU in (select SKU from products.Products where ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers))
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ajuste Listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	----arreglo para productos en prueba sobre el punto de reorden
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes punto de reorden (Min distinto Max)...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--update p set suggestMin = suggestMin,suggestMax = suggestMax 
	--from minmax.PricingSuggest p where SKU in (select SKU from temp.PruebaMinMaxDistintoNS)
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ajuste Listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- muevo los minmax.suggestForecast
	truncate table [test].SuggestForecastCorrected
	insert into [test].SuggestForecastCorrected
	select * from [test].SuggestForecast
	
	-- Aplicar restricciones
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando minmax.SuggestRestriction...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	Execute [test].SuggestRestriction @date
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'minmax.SuggestRestriction Listo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	---- eliminar los que no provoquen cambios
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando los sugeridos que no provoquen cambios...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	--delete s from [minmax].[PricingSuggest] s
	--inner join series.LastStock l on s.SKU = l.SKU and s.Store = l.Store
	--where s.SuggestMax=l.[Max] and s.SuggestMin=l.[Min]
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos en promoción...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	---- eliminar promociones y próximas promociones (que partan hasta en un mes más)
	--DELETE ps FROM [minmax].[PricingSuggest] ps
	--INNER JOIN promotion.PromotionStock pr ON pr.ProductId=ps.SKU and ps.[Date] between DATEADD(month, -1, BeginDate) and EndDate
	--DELETE ps FROM [minmax].[PricingSuggest] ps
	--INNER JOIN promotion.PromotionDetail pr ON pr.ProductId=ps.SKU and ps.[Date] between DATEADD(month, -1, BeginDate) and EndDate
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminación Lista...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 


	--calcular alzas de feriados cuando ya están aprobadas
	--declare @holidays cursor, @holiday date
	--set @holidays = cursor fast_forward for select distinct HolidayDate from minmax.HolidayApprovedSuggests order by HolidayDate
	--open @holidays 	fetch next from @holidays into @holiday

	--while @@FETCH_STATUS = 0
	--begin
	--	exec minmax.HolidayCalculateDailyIncrease @holiday

	--	fetch next from @holidays into @holiday
	--end

	--close @holidays deallocate @holidays

	-- Cargar replenishment.nextSuggest
	EXECUTE [test].[LoadDailySuggestProposal]  @date

	-- calcular proximas reposiciones -- pendiente

END

