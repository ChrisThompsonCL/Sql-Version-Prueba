﻿-- =============================================
-- Author:		Elizabeth Peña
-- Create date: 19-07-2018
-- Description:	Procesa y prepara los datos de recetas para ser usados en calibración/pronóstico
-- =============================================
CREATE PROCEDURE [test].[RecetasPronostico]
	-- Add the parameters for the stored procedure here
		@sku int, 
		@inicio date, 
		@fin date
AS
BEGIN
	SET NOCOUNT ON;



--declare @sku int = 160170
--declare @inicio date = '2016-01-01'
--declare @fin date = '2018-05-31'


-- CONSIDERAR SKU O, SI ES GENERICO, LA FAMILIA DE SKU
DECLARE @skusAux table(SKU int)
	INSERT INTO @skusAux
	SELECT SKU 
	FROM products.Products
	WHERE SKU=@sku and SKU not in (select SKU from products.GenericFamilyDetail)
	UNION
	SELECT SKU 
	FROM products.GenericFamilyDetail 
	WHERE GenericFamilyId=(select GenericFamilyId from products.GenericFamilyDetail where SKU=@sku)



DECLARE @finReal date = (select top 1 [date] from series.lastsale)
DECLARE @inicioReal date = (select min(case when FirstSale > @inicio then FirstSale else @inicio end) from products.FullProducts where sku in (select SKU from @skusAux))                   

-- Recetas del @sku
SELECT date fecha
		,count(distinct folio) Nrecetas	
INTO #RecetasPorSKU
FROM  Salcobrand.series.Recetas r
WHERE R.SKU in (select sku from @skusAux) --AND r.VentaUnidades > 0
		AND r.date <= @fin --- SOLO PARA EL CASO CUANDO NO SE QUIERA @finReal
GROUP BY date
--ORDER BY date 



/*#############################
		 RECETAS PASADO
###############################*/
SELECT G.FECHA
	,CASE WHEN G.fecha IN (SELECT * FROM Salcobrand.aux.FechasSinRecetas) 
		THEN ISNULL((SELECT ROUND(AVG(Nrecetas),0)
				FROM #RecetasPorSKU RR 
				WHERE DATEPART(DW, RR.FECHA) = DATEPART(DW, G.FECHA) AND RR.FECHA < G.FECHA 
					AND RR.FECHA >= DATEADD(DAY, -28, G.FECHA) AND G.FeriadoIrrenunciable =0),0)
		ELSE ISNULL(rs.Nrecetas,0) 
		END Recetas
INTO #RecetasPasado
FROM  Salcobrand.forecast.GenericVariables g
LEFT JOIN #RecetasPorSKU rs ON rs.fecha = g.fecha
WHERE g.Fecha >= @inicio and g.fecha <= @fin --@finreal




/*#############################
		 RECETAS SEMANAL
###############################*/

SELECT  dbo.GetMonday(rp.Fecha) Semana, SUM(Recetas) Recetas
INTO #RecetasSemanal
FROM #RecetasPasado rp
GROUP BY dbo.GetMonday(rp.Fecha)



-- INFORMACIÓN PARA EXTREMAR RECETAS (HACER EL CAMBIO DE LA CANTIDAD DE RECETAS MAS NOTORIO)
DECLARE @RecMin INT = (select min(Recetas) FROM #RecetasSemanal)
DECLARE @RecMax INT = (select max(Recetas) FROM #RecetasSemanal)
DECLARE @Divisor3 FLOAT = (@RecMax - @RecMin)/ 3
--DECLARE @Divisor5 FLOAT = (@RecMax - @RecMin)/ 5
--DECLARE @Divisor10 FLOAT = (@RecMax - @RecMin)/ 10



SELECT Semana
		,CASE WHEN  @Divisor3 > 1 THEN  ROUND((Recetas/@Divisor3), 0) ELSE Recetas END Recetas
	--,CASE WHEN  @Divisor3 > 1 THEN  ROUND((Recetas/@Divisor3), 0) 
	--	ELSE (CASE WHEN @Divisor5 > 1 THEN ROUND((Recetas/@Divisor5), 0) 
	--		ELSE (CASE WHEN @Divisor10 > 1 THEN ROUND((Recetas/@Divisor10), 0) ELSE Recetas END) END) END Recetas
INTO #RecSemanalCambioNotorio
FROM #RecetasSemanal




/*#############################
		 RECETAS FUTURO
###############################*/
SELECT A.Semana, ISNULL(s.Recetas ,0) Recetas
INTO #RecetasFuturo
FROM (SELECT dbo.GetMonday(g.fecha) Semana FROM [Salcobrand].[forecast].[GenericVariables] g WHERE G.Fecha > @fin AND g.Fecha < dateadd(MM, 4, @fin) GROUP BY dbo.GetMonday(g.fecha)) A
	CROSS JOIN (SELECT Recetas FROM #RecSemanalCambioNotorio WHERE Semana = (select max(Semana) FROM #RecSemanalCambioNotorio)) S


 DELETE FROM Salcobrand.test.Recetas_AUX_Semanal
 WHERE [Sku] = @sku


INSERT INTO Salcobrand.test.Recetas_AUX_Semanal
	SELECT @sku, rp.Semana, rp.Recetas, p.semana DosSemanasAtras, p.Recetas RecetasDosSemanasAtras
	FROM #RecSemanalCambioNotorio rp
		LEFT JOIN #RecSemanalCambioNotorio p on p.Semana = dateadd(WEEK,-2,rp.Semana)
	UNION
	SELECT @sku, R.Semana ,R.Recetas
			, CASE WHEN rr.Semana is null THEN rp.Semana ELSE RR.Semana END DosSemanasAtras
			, CASE WHEN rr.Recetas is null THEN rp.Recetas ELSE RR.Recetas END RecetasDosSemanasAtras
	from #RecetasFuturo R
		LEFT JOIN #RecetasFuturo RR ON RR.Semana = dateadd(WEEK, -2,R.Semana)
		LEFT JOIN #RecSemanalCambioNotorio RP ON RP.Semana =  dateadd(WEEK, -2,R.Semana)


DROP TABLE #RecetasPorSKU
DROP TABLE #RecetasPasado
DROP TABLE #RecetasFuturo
DROP TABLE #RecetasSemanal
DROP TABLE #RecSemanalCambioNotorio



END
