﻿
-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2013-08-20
-- Description:	Actualiza los leadtimes y fillrate de las ordenes recibidas
-- =============================================
CREATE PROCEDURE [test].[UpdateDeliveryInformation]
	@date smalldatetime -- fecha que toma como referencia para el calculo
AS
BEGIN
	SET NOCOUNT ON;

    
    TRUNCATE TABLE [test].[DeliveryInformationByManufacturer]
    
    INSERT INTO [test].[DeliveryInformationByManufacturer]
           ([ManufacturerId],[Date],[FillrateAvg],[FillrateStd],[LeadtimeAvg],[LeadtimeStd],[Norder])
	SELECT ManufacturerId,@date,AVG(fillrate) fillrateAvg,STDEV(fillrate) fillrateST,AVG(leadtime) leadtimeAvg,STDEV(leadtime) leadtimeST,COUNT(*) norder 
	FROM (
		SELECT h.SKU,p.ManufacturerId, OCID, cast(Delivered as float)/Ordered fillrate
			, cast(DATEDIFF(day,OrderDate,DeliveryDate) as float) leadtime 
		FROM (
			SELECT t.SKU, t.OCID, t.Ordered, t.Delivered, t.OrderDate, t.ExpirationDate, t.DeliveryDate, t.CreateDate 
			FROM ( 
				SELECT CreateDate, SKU, OCID 
					, avg(Ordered) Ordered, sum(Delivered) Delivered, max(OrderDate) OrderDate 
					, max(ExpirationDate) ExpirationDate, max(DeliveryDate) DeliveryDate
					, RANK() over (PARTITION by sku,ocid order by createDate desc) r 
				FROM [cdbuy].[ProductsManufacturerInformation]  -- es necesario cambiar esto cuando la tabla cambie
				WHERE [State] = 2 
				and OrderDate between DATEADD(day,-98,@date) and DATEADD(day,-8,@date)
				--and SKU in (select ProductId from [function].GetOperativeCombination(4,0,0,3,0))
				and Ordered > 0
				GROUP BY CreateDate, SKU, OCID 
			) t 
			WHERE t.r = 1
		) h
		inner join products.Products p on p.SKU = h.SKU
	) j 
	WHERE leadtime>0 
	GROUP BY ManufacturerId
	
	TRUNCATE TABLE [test].[DeliveryInformationByProducts]
	
	INSERT INTO [test].[DeliveryInformationByProducts](SKU,[Date])
	SELECT productId,@date from [function].GetOperativeCombination(4,0,3,3,0)
	
	update p set p.[FillrateAvg]=t.[FillrateAvg],
				p.[FillrateStd]=t.[FillrateStd],
				p.[LeadtimeAvg]=t.[LeadtimeAvg],
				p.[LeadtimeStd]=t.[LeadtimeStd],
				p.[Norder]= t.[Norder],
				p.[UsingManufacturerInformation]=t.[UsingManufacturerInformation] from [test].[DeliveryInformationByProducts] p
	inner join (
	SELECT byProd.SKU, @date Date
		, case when byProd.norder >= 4 then byProd.fillrateAvg else byMid.FillrateAvg end FillrateAvg
		, case when byProd.norder >= 4 then byProd.fillrateST else byMid.FillrateStd end FillrateStd
		, case when byProd.norder >= 4 then byProd.leadtimeAvg else byMid.LeadtimeAvg end LeadtimeAvg
		, case when byProd.norder >= 4 then byProd.leadtimeST else byMid.LeadtimeStd end LeadtimeStd
		, byProd.norder
		, case when byProd.norder >= 4 then 0 else 1 end UsingManufacturerInformation
	FROM (
		SELECT sku, ManufacturerId,AVG(fillrate) fillrateAvg,STDEV(fillrate) fillrateST,AVG(leadtime) leadtimeAvg,STDEV(leadtime) leadtimeST,COUNT(*) norder 
		FROM (
			SELECT h.SKU, p.ManufacturerId, OCID, Delivered*1.0/Ordered fillrate, DATEDIFF(day,OrderDate,DeliveryDate)*1.0 leadtime 
			FROM (
				SELECT t.SKU, t.OCID, t.Ordered, t.Delivered, t.OrderDate, t.ExpirationDate, t.DeliveryDate, t.CreateDate 
				FROM ( 
					SELECT CreateDate, SKU, OCID 
						, avg(Ordered) Ordered, sum(Delivered) Delivered, max(OrderDate) OrderDate 
						, max(ExpirationDate) ExpirationDate, max(DeliveryDate) DeliveryDate
						, RANK() over (PARTITION by sku,ocid order by createDate desc) r 
					FROM [cdbuy].[ProductsManufacturerInformation]  -- es necesario cambiar esto cuando la tabla cambie
					WHERE [State] = 2 
					and OrderDate between DATEADD(day,-98,@date) and DATEADD(day,-8,@date)
					--and SKU in (select ProductId from [function].GetOperativeCombination(4,0,0,3,0))
					and Ordered > 0
					GROUP BY CreateDate, SKU, OCID 
				) t 
				where t.r = 1
			) h
			inner join products.Products p on p.SKU = h.SKU
		) j 
		where leadtime>0 
		group by sku, ManufacturerId
	) byProd
	inner join [test].DeliveryInformationByManufacturer byMid on byMid.ManufacturerId=byProd.ManufacturerId and byMid.[Date] = @date
	) t on t.SKU = p.SKU
	
	update p set p.[FillrateAvg]=byMid.[FillrateAvg],
				p.[FillrateStd]=byMid.[FillrateStd],
				p.[LeadtimeAvg]=byMid.[LeadtimeAvg],
				p.[LeadtimeStd]=byMid.[LeadtimeStd],
				p.[Norder]= byMid.[Norder],
				p.[UsingManufacturerInformation]=1 from [test].[DeliveryInformationByProducts] p
	inner join products.Products h on h.SKU = p.SKU
	inner join [test].DeliveryInformationByManufacturer byMid on byMid.ManufacturerId=h.ManufacturerId and byMid.[Date] = @date
	where p.LeadtimeAvg is null
	
END

