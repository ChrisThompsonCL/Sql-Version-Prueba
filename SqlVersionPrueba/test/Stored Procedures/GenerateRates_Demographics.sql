﻿-- =================================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-21
-- Description:	Sugerido para fines de semana
-- largos que conllevan a un movimiento demografico
-- Revisar los "IMPORTANTE"
-- =================================================
CREATE PROCEDURE [test].[GenerateRates_Demographics]
	-- Add the parameters for the stored procedure here
	@sku int,
	@forecast_tag nvarchar(150),
	@date datetime
AS
BEGIN

	create table #original_forecast (date date, fit float)
	create table #share_ciudad(city nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL, share_ciudad float)
	create table #resultado_proc(fechacreacion datetime, sku int, date datetime, store_id int, rate float, forecast_tag nvarchar(150))
	create table #multiplicacion_shares(fechacreacion datetime, sku int, city nvarchar(50), store int, rate float, share_ciudad float, MultiplicacionShares float)


	SET NOCOUNT ON;

	insert into #original_forecast
	select Fecha, Fit 
	from [Temporal].[STD_Pronosticos].[Salcobrand] pron inner join [view].[RelacionIdCombinacion] ri on pron.IdCombinacion = ri.idcombinacion
	where tagcreacion = @forecast_tag and Fecha = @date and ri.idcodigocliente = @sku

	-- Si no se entrega forecast tag, entrega un share porcentual
	IF	(@forecast_tag is null)
	BEGIN 
			insert into #original_forecast
			select @date, 1
	END


	-- Es importante mantener actualizada la tabla [ResultadosShareLocal_ShareMovDemos] cada vez que se produce 
	-- un movimiento demografico nuevo (i.e. feriado renunciable lunes o martes)
	insert into #share_ciudad
	select city, avg(share) as shareciudad  -- Si bien estos avg() no suman 1, lo importante es la magnitud relativa de una ciudad vs otra
	from [Temporal].[dbo].[ResultadosShareLocal_ShareMovDemos]
	where DiaSemana = datename(dw, @date) and Estacion = dbo.Estacion(@date)  -- IMPORTANTE: Estos datename deben ser cambiados a datepart
	group by city
	-- Para un día de la semana y su estación correspondiente, entrega una participación de cada ciudad


	insert into #resultado_proc
	select fechacreacion, sku, date, store, rate, tagcreacion
	from [Temporal].[dbo].[RateRespaldo20171109]  -- IMPORTANTE: Aqui va a ir [minmax].[Rates]
	where sku = @sku and date = @date and tagcreacion = @forecast_tag
	-- Aqui guarda en #resultado_proc el rate entregado por el procedimiento almacenado base


	insert into #multiplicacion_shares
	select fechacreacion, SKU, sc.City, store_id as Store, rate, share_ciudad, rate*share_ciudad as MultiplicacionShares
	from	#resultado_proc rp inner join 
			[stores].[Stores] stores on rp.store_id = stores.Store  inner join
			#share_ciudad sc on sc.city = stores.City
	-- Se obtiene la multipliacion del rate de la ciudad con el rate del local


	declare	@suma_multiplicacion_shares float
	set		@suma_multiplicacion_shares = (select sum(MultiplicacionShares) from #multiplicacion_shares)


	delete from [Temporal].[dbo].[RateRespaldo20171109]  -- IMPORTANTE: Aqui va a ir [minmax].[Rates]
	where sku = @sku and date = @date and tagcreacion = @forecast_tag
	insert into [Temporal].[dbo].[RateRespaldo20171109]  -- IMPORTANTE: Aqui va a ir [minmax].[Rates]
	select @forecast_tag, fechacreacion, sku, Store, date, case when @suma_multiplicacion_shares > 0 then MultiplicacionShares/@suma_multiplicacion_shares*fit else 0 end Rate
	from #multiplicacion_shares ms inner join #original_forecast orf on @date =orf.date
	order by Store
	-- Normaliza la multplicacion de los rates


	drop table #share_ciudad
	drop table #resultado_proc
	drop table #original_forecast
	drop table #multiplicacion_shares


END
