﻿-- ==========================================================================================
-- Author:		Carlos Quappe
-- Create date: 2018-04-20
-- ==========================================================================================
CREATE PROCEDURE [test].[GenerateFits_Rancagua] 
	-- Add the parameters for the stored procedure here
	@beginning_date datetime,
	@finishing_date datetime
AS
BEGIN
	SET NOCOUNT ON;
		
	declare @auxSku int

	declare cursor_skus cursor fast_forward 
	for
		select distinct idcodigocliente as sku
		from [view].[RelacionIdCombinacion] ri inner join [Temporal].[STD_Pronosticos].[Salcobrand] pr on ri.idcombinacion = pr.idcombinacion
		where tagcreacion ='SB.25062018' --'Anibal-creativo-parte-2' --and idcodigocliente not in (select sku from Temporal.[dbo].[1Borrar_1CronometroPronosticos12Meses2017])

	open cursor_skus
	fetch next from cursor_skus into @auxSku

	while @@fetch_status = 0
	begin
		
		exec [test].[GenerateFits_Base_Rancagua]  @auxSku, @beginning_date, @finishing_date

		insert into [Temporal].[dbo].[1Borrar_1CronometroPronosticos12Meses2017]
		--insert into [Temporal].[dbo].[1Borrar_1CronometroPronosticos12Meses2017]
		select @auxSku Sku, getdate() Date
	
		fetch next from cursor_skus into @auxSku

	end

	close cursor_skus
	deallocate cursor_skus


END