﻿-- ==========================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-28
-- ==========================================================================================
CREATE PROCEDURE [test].[GenerateRates] 
	-- Add the parameters for the stored procedure here
	@forecast_tag nvarchar(150),
	@beginning_date datetime,
	@finishing_date datetime
AS
BEGIN
	SET NOCOUNT ON;
		
	declare @fecha_creacion smalldatetime = getdate()
	declare @auxSku int
	declare @auxDate date, @auxMirrorDate date


	/***********************/
	/*cálculo de tasas base*/
	/***********************/

	declare cursor_skus cursor fast_forward 
	for
		select distinct idcodigocliente as sku
		from [view].RelacionIdCombinacion ri 
		inner join Temporal.STD_Pronosticos.Salcobrand pr on ri.idcombinacion = pr.IdCombinacion
		where pr.TagCreacion = @forecast_tag and idcodigocliente%100 between 8 and 20

	open cursor_skus
	fetch next from cursor_skus into @auxSku

	while @@fetch_status = 0
	begin
		-- Borrar la tabla temporal 
		truncate table [dbo].[Rates_Temp]  --CAMBIO!!

		exec [test].[GenerateRates_Base]  @auxSku, @forecast_tag, @beginning_date, @finishing_date
		--exec [test].[GenerateRates_Base_DESARROLLO]  @auxSku, @forecast_tag, @beginning_date, @finishing_date

		--delete from minmax.Rates where sku=@auxSku and [date]=@auxDate and tagCreacion=@forecast_tag  -- Eliminar tagCreacion=@forecast_tag (deseable), date between fechas (necesario)
		delete from [Temporal].[dbo].[Rates_sinMax] where sku=@auxSku and tagCreacion=@forecast_tag and [date] between @beginning_date and @finishing_date  -- No quiero que el test guarde en minmax

		insert into [Temporal].[dbo].[Rates_sinMax] (tagcreacion, fechacreacion, SKU, Store ,[date], Rate)  -- No quiero que el test guarde en minmax
		select @forecast_tag, @fecha_creacion, sku, store_id as store, [date], suggest as rate  -- store por store_id, rate por suggest
		from [dbo].[Rates_Temp]  --CAMBIO!!

		fetch next from cursor_skus into @auxSku

	end

	close cursor_skus
	deallocate cursor_skus



	/*******************/
	/*Fechas con espejo*/
	/*******************/

	--declare cursor_mirrorDates cursor fast_forward
	--for 
	--	select [Date], [MirrorDate]
	--	from [dbo].[Rates_SpecialDates] a
	--	where [Date] between @beginning_date and @finishing_date
	--		and Kind = 'espejo'

	--open cursor_mirrorDates
	--fetch next from cursor_mirrorDates into @auxDate, @auxMirrorDate

	--while @@FETCH_STATUS=0
	--begin

	--	truncate table [aux].[SpecialShareStores]

	--	--cálculo de shares local y ciudad para las fechas indicadas, todos los SKUs
	--	exec test.CalculateSpecialStoreShare @auxDate, @auxMirrorDate  -- ¿Aqui [SpecialShareStores]?

	--	declare cursor_skus cursor fast_forward 
	--	for
	--		select distinct idcodigocliente as sku
	--		from [view].RelacionIdCombinacion ri 
	--		inner join Temporal.STD_Pronosticos.Salcobrand pr on ri.idcombinacion = pr.IdCombinacion
	--		where pr.TagCreacion = @forecast_tag

	--	open cursor_skus
	--	fetch next from cursor_skus into @auxSku

	--	while @@fetch_status = 0
	--	begin
		
	--		-- Borrar la tabla temporal 
	--		truncate table [aux].[Rates_Temp]

	--		Exec [test].[GenerateRates_Mirror]  @auxSku, @forecast_tag, @auxDate

	--		delete from minmax.Rates where sku=@auxSku and [date]=@auxDate and tagCreacion=@forecast_tag

	--		insert into [minmax].[Rates] (tagcreacion, fechacreacion, SKU, Store ,[date], Rate)
	--		select forecast_tag, @fecha_creacion, sku, store, [date], rate
	--		from [aux].[Rates_Temp]

	--		fetch next from cursor_skus into @auxSku

	--	end

	--	close cursor_skus
	--	deallocate cursor_skus

	--	fetch next from cursor_mirrorDates into @auxDate, @auxMirrorDate

	--end

	--close cursor_mirrorDates
	--deallocate cursor_mirrorDates



	/***********************************/
	/*Fechas con movimiento demográfico*/
	/***********************************/

END
