﻿-- ================================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-07-11
-- Description:	Primera version del nuevo sugerido a partir de pronosticos diarios
-- Pending: 
-- 1. Al momento de crear el sugerido, revisar que el local correspondiente este activo 
-- para ese producto. En caso de no estarlo, debe dejar en 0 el sugerido para ese local, 
-- sin repartir ese sugerido entre otros locales, ya que es demanda perdida.
-- 2. Corregir la venta de los ultimos 3 meses (#last_3months_sales) por activacion de locales.
-- Corrección por quiebre completa, pero falta corregir cuando quiebre es mayor al 50% del tiempo.
-- La correccion por activacion de local no se esta haciendo (¿de donde obtener informacion de 
-- activacion?)
-- 3. Corregir por Promocion. Al momento de repartir el share, un local con promocion tendra 
-- un share mayor al normal. Hay muchas formas de atacar esto. 
--	3.1 Calcular un share sin promocion y un share con promocion. Usar el que corresponda.
--	3.2 Tener un factor que multiplique el share para estos locales (asi el share sumaria mas de 
--	uno, asumiendo que se vendera mas de lo esperado por estar ese local en promocion). Podria 
--	no ser correcto, ya que el pronostico considera cuando hay promocion.
-- ¿Importa la promocion en el local (cambia el nivel de ventas)?
-- Si al haber promocion en un local y se cree que esto aumenta las ventas del local. ¿Se debe
-- aumentar el sugerido local, o se debe repartir quitandole a los locales sin promocion?
-- ¿Se debe corregir por promocion para obtener la venta de los ultimos 3 meses? Si no se corrige 
-- podriamos estar dandole mucha importancia a un local que solo importa cuando hay promocion.
-- ¿Como obtener promocion: directo de la informacion que cargan ellos, o verificar que haya un
-- efecto?
-- Limitante: No tenemos informacion de promocion por local
-- Nota: Se modifico el procedimiento para que toda la informacion se tome con fecha anterior a 
-- beginning_date. Como la tabla que entrega el quiebre solo tiene informacion de los ultimos 3 
-- meses, cuando se usa este procedimiento para obtener share del pasado, no se va a poder corregir 
-- por quiebre (dependiendo de las fechas, se corrige distinta cantidad).
-- ================================================================================================
CREATE PROCEDURE [test].[GetNewSuggestNaive]
	-- Add the parameters for the stored procedure here
	@sku int,
	@forecast_tag nvarchar(150),
	@date datetime

AS
BEGIN


	--create table #sales (store_id int, sale_date date, quantity float)
	create table #initial_date (store_id int, initial_date date)
	-- let n denote the number of different stores
	-- for i in range(n):
	--     create table #matrix_i (Month varchar(15), Monday float, Tuesday float, Wednesday float, Thursday float, Friday float, Saturday float, Sunday float)
	create table #first_matrix2 (store_id int, Month varchar(15), Day varchar(15), quantity float)
	create table #total_sales (store_id int, Quantity float)
	create table #daily_importance2 (store_id int, Day varchar(15), quantity float)
	create table #monthly_importance(store_id int, Month varchar(15), quantity float)
	create table #second_matrix2 (store_id int, Month varchar(15), Day varchar(15), quantity float)
	create table #sum_second_matrix2 (Month varchar(15), Day varchar(15), quantity float)
	create table #last_3months_sales (store_id int, Quantity float)
	create table #last_1month_sales (store_id int, Quantity float)
	create table #last_1week_sales (store_id int, Quantity float)
	create table #first_instance_forecast (date date, store_id int, first_forecast float)
	create table #sum_first_instance_forecast (date date, total_forecast float)
	create table #final_forecast (sku int, date date, store_id int, suggest float, forecast_tag nvarchar(150))
	create table #original_forecast (date date, fit float)
	create table #stores_to_replace (store_id int)
	--create table #sales_by_store(store_id int, sales int)
	create table #days_out_of_stock_3months (store_id int, days int)
	create table #days_out_of_stock_1month  (store_id int, days int)
	create table #days_out_of_stock_1week	(store_id int, days int)
	create table #store_importance (store_id int, Quantity float)


	SET NOCOUNT ON;


	declare @categoryID int
	set @categoryID = (select CategoryId from [Salcobrand].[products].[Products] where sku = @sku)


	insert into #original_forecast
	select Fecha, Fit 
	from [Temporal].[STD_Pronosticos].[Salcobrand] pron inner join [Salcobrand].[view].[RelacionIdCombinacion] ri on pron.IdCombinacion = ri.idcombinacion
	where tagcreacion = @forecast_tag and Fecha = @date and ri.idcodigocliente = @sku


	-- Si no se entrega forecast tag, entrega un share porcentual
	IF	(@forecast_tag is null)
	BEGIN 
			EXECUTE('
			insert into #original_forecast
			select '''+@date+''', 1')
			END


	--insert into #sales
	--select [Store], date ,sum([Quantity]) as Quantity
	--from [Salcobrand].[series].[Sales]
	--where sku = @sku and date < @date  -- Cambio de beginning date
	--group by [Store], date


	Declare @Date_Aux Date
	Set @Date_Aux=GetDate()
	insert into #initial_date
	select	store_id, 
			case	when count(distinct DATEADD(MONTH, DATEDIFF(MONTH, 0, sale_date), 0)) > 12
					then DateAdd(MONTH,-(count(distinct DATEADD(MONTH, DATEDIFF(MONTH, 0, sale_date), 0)) - (count(distinct DATEADD(MONTH, DATEDIFF(MONTH, 0, sale_date), 0)) % 12)),@Date_Aux)
					else DateAdd(MONTH,-(count(distinct DATEADD(MONTH, DATEDIFF(MONTH, 0, sale_date), 0))),@Date_Aux) end initial
	from Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP]
	group by store_id
	-- Se guarda en #initial_date la fecha que se usará de inicio, despues de truncar por 12 meses al pasado 
	-- de la fecha actual para no darle importancia extra a meses que se repiten más. Solo si tiene mas de 
	-- 12 meses de historia.


	insert into #first_matrix2
	select	s.store_id,
			DATENAME(m,sale_date) as NombreMes,
			DATENAME(dw,sale_date) as NombreDia,
			sum(quantity) as Quantity
	from Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP] s inner join #initial_date id on (s.store_id = id.store_id)
	where sale_date >= id.initial_date
	group by s.store_id,DATENAME(m,sale_date),DATENAME(dw,sale_date)
	-- Crea una tabla que entrega la cantidad total vendida para: local-diasemana-mes


	insert into #total_sales
	select	store_id, 
			sum(Quantity) as Quantity
	from Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP]
	group by store_id
	-- Entrega la venta total de cada local


	insert into #daily_importance2
	select	f.store_id,
			day,
			case	when t.Quantity > 0
					then sum(f.Quantity)/t.Quantity 
					else 0 end Quantity
	from #first_matrix2 f inner join #total_sales t on f.store_id = t.store_id
	group by f.store_id,day,t.Quantity
	-- Participación día (diasemana)


	insert into #monthly_importance
	select	Store,
			DATENAME(m,date) as NombreMes,
			case	when t.Quantity > 0
					then sum(s.Quantity)/t.Quantity
					else 0 end Quantity
	from [Salcobrand].[series].[Sales] s inner join #total_sales t on s.store = t.store_id 
	where sku = @sku and date < @date
	group by Store, DATENAME(m,date), t.Quantity
	order by store, nombremes
	-- Participación mes

	
	--insert into #sales_by_store
	--select	distinct	[Store], count(*)
	--from				[Salcobrand].[series].[Sales]
	--where				sku = @sku and Quantity > 0 and date < @date  -- Cambio de beginning date
	--group by [Store]
	--order by [Store]


	insert into	#stores_to_replace
	select distinct store_id
	from Temporal.[dbo].[ResultadosShareLocal_#SALESBYSTORE_TEMP]
	where sales < 300
	-- Si hay menos de 300 instancias de ventas para un sku-local, su matriz diasemana-mes 
	-- va a ser reemplazada por la matriz diasemana-mes de su categoría

	
	insert into #second_matrix2
	select	d.store_id,
			m.Month,
			d.Day,
			case when	sr.store_id is null 
						then d.Quantity * m.Quantity 
						else mic.Quantity 
						end Quantity
	from	#daily_importance2 d inner join 
			#monthly_importance m on d.store_id = m.store_id left join
			[Temporal].[dbo].[MatrizImportanciaCategoria] mic on	(mic.category_ID = @categoryID 
																	and m.Month COLLATE DATABASE_DEFAULT = mic.month COLLATE DATABASE_DEFAULT
																	and d.Day COLLATE DATABASE_DEFAULT = mic.Day COLLATE DATABASE_DEFAULT
																	and mic.store_ID = d.store_id) left join
			#stores_to_replace sr on (sr.store_id = d.store_id)
	-- Importancia de diasemana-mes de acuerdo a la multiplicacion de importancia dia con importancia mes.
	-- Si el local está dentro de los locales a reemplazas, usa el dato de mic (matriz importancia categoria)
	
	
	IF	(select count(distinct Month) from #monthly_importance) < 12
	BEGIN 
			EXECUTE('delete from #second_matrix2')
			EXECUTE('
			insert into #second_matrix2
			select store_ID, month, day, quantity 
			from [Temporal].[dbo].[MatrizImportanciaCategoria] 
			where category_ID = '+@categoryID)
			END
	-- Si hay por lo menos un mes sin información, usamos la información díasemana-mes de la categoría (para todos los locales)
	
	
	insert into #sum_second_matrix2
	select	Month,
			Day,
			sum(quantity) as Quantity
	from #second_matrix2
	group by Month, Day
	-- Suma de celdas de matrix2 para todos los locales


	/* Importancia a locales*/

	-- Ultimos 3 meses
	insert into #days_out_of_stock_3months
	select	store, count(*) as days
	from	[Salcobrand].[reports].[ReportStores]
	where	sku = @sku and date >= DATEADD(day, -90, @date) and (stock <= 0) and date < @date  -- Cambio de beginning date 
	--or suggest = 0 esto ultimo es para corregir por sugerido
	group by store	
	declare @last_3months_days int
	set		@last_3months_days = datediff(day, DATEADD(day, -90, @date), @date)
	insert into #last_3months_sales
	select	s.store_id, 
			case when dos.days is not null 
				then	(case when dos.days/(1.0*@last_3months_days) > 0.95 -- 0.5
							then sum(Quantity)*20  -- Aqui va la venta diaria promedio * @last_3months_days (entregado por Gonzalo)
							else sum(Quantity)/(1-(dos.days/(1.0*@last_3months_days)))  -- Corrige linealmente los dias quebrados
							end)
				else sum(Quantity)
				end Quantity
	from	Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP] s left join 
			#days_out_of_stock_3months dos on s.store_id = dos.store_id
	where	sale_date >= DATEADD(day, -90, @date)
	group by s.store_id, dos.days
	-- Venta de los últimos 3 meses corregido por quiebre

	-- Ultimo mes
	insert into #days_out_of_stock_1month
	select	store, count(*) as days
	from	[Salcobrand].[reports].[ReportStores]
	where	sku = @sku and date >= DATEADD(day, -30, @date) and (stock <= 0) and date < @date  -- Cambio de beginning date 
	--or suggest = 0 esto ultimo es para corregir por sugerido
	group by store	
	declare @last_month_days int
	set		@last_month_days = datediff(day, DATEADD(day, -30, @date), @date)
	insert into #last_1month_sales
	select	s.store_id, 
			case when dos.days is not null 
				then	(case when dos.days/(1.0*@last_month_days) > 0.95 -- 0.5
							then sum(Quantity)*20  -- Aqui va la venta diaria promedio * @last_month_days (entregado por Gonzalo)
							else sum(Quantity)/(1-(dos.days/(1.0*@last_month_days)))  -- Corrige linealmente los dias quebrados
							end)
				else sum(Quantity)
				end Quantity
	from	Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP] s left join 
			#days_out_of_stock_1month dos on s.store_id = dos.store_id
	where	sale_date >= DATEADD(day, -30, @date)
	group by s.store_id, dos.days
	-- Venta del último mes corregido por quiebre

	-- Ultima semana
	insert into #days_out_of_stock_1week
	select	store, count(*) as days
	from	[Salcobrand].[reports].[ReportStores]
	where	sku = @sku and date >= DATEADD(day, -7, @date) and (stock <= 0) and date < @date  -- Cambio de beginning date 
	--or suggest = 0 esto ultimo es para corregir por sugerido
	group by store	
	declare @last_week_days int
	set		@last_week_days = datediff(day, DATEADD(day, -7, @date), @date)
	insert into #last_1week_sales
	select	s.store_id, 
			case when dos.days is not null 
				then	(case when dos.days/(1.0*@last_week_days) > 0.95 -- 0.5
							then sum(Quantity)*20  -- Aqui va la venta diaria promedio * @last_week_days (entregado por Gonzalo)
							else sum(Quantity)/(1-(dos.days/(1.0*@last_week_days)))  -- Corrige linealmente los dias quebrados
							end)
				else sum(Quantity)
				end Quantity
	from	Temporal.[dbo].[ResultadosShareLocal_#SALES_TEMP] s left join 
			#days_out_of_stock_1week dos on s.store_id = dos.store_id
	where	sale_date >= DATEADD(day, -7, @date)
	group by s.store_id, dos.days
	-- Venta de la ultima semana corregida por quiebre
	
	insert into #store_importance
	select	l3.store_id, case when	isnull(l3.Quantity, 0)/@last_3months_days > isnull(l2.Quantity, 0)/@last_month_days and isnull(l3.Quantity, 0)/@last_3months_days > isnull(l1.Quantity, 0)/@last_week_days
									then	isnull(l3.Quantity, 0)/@last_3months_days
									else	(case when	isnull(l2.Quantity, 0)/@last_month_days > isnull(l1.Quantity, 0)/@last_week_days
														then	isnull(l2.Quantity, 0)/@last_month_days
														else	isnull(l1.Quantity, 0)/@last_week_days end) 
									end Quantity
	from	#last_3months_sales l3									left join
			#last_1month_sales	l2 on (l3.store_id = l2.store_id)	left join 
			#last_1week_sales	l1 on (l2.store_id = l1.store_id)
	-- Guarda el máximo de venta diaria promedio (corregida) entre los últimos 3 meses, último mes y última semana, por local

	--select store_id, Quantity, Quantity/@last_3months_days as Promedio, '3m' from #last_3months_sales 
	--select store_id, Quantity, Quantity/@last_month_days as Promedio, '1m'	from #last_1month_sales 
	--select store_id, Quantity, Quantity/@last_week_days as Promedio, '1w'	from #last_1week_sales 
	--select *, 'tot'	from #store_importance
	/* Fin Importancia a locales*/


	DECLARE @forecast float
	SET @forecast =	(select fit
					 from #original_forecast
					 where date = @date)

	insert into #first_instance_forecast
	select	@date,
			s.store_id,
			s.quantity * @forecast * l.quantity / m.quantity
	from	#second_matrix2 s inner join
			#store_importance l on s.store_id = l.store_id cross join
			#sum_second_matrix2 m
	where	s.day = DATENAME(dw, @date) and s.Month = DATENAME(m, @date) and
			m.Day  = DATENAME(dw, @date) and m.Month = DATENAME(m, @date)
	-- Multiplica la importancia diasemana-mes por pronóstico del día por importancia del local (lo último que sacamos)
	-- dividido en la suma de matrices diasemana-mes (suma por locales)




	insert into #sum_first_instance_forecast
	select	date,
			sum(first_forecast)
	from #first_instance_forecast
	group by date
	-- Suma la tabla anterior para poder normalizarla y que la suma total de rates sea igual al pronóstico


	insert into #final_forecast
	select	@sku,
			s.date,
			store_id,
			case when	s.total_forecast > 0
						then (case when	(f.first_forecast * forecast.fit / s.total_forecast) is null 
										then 0 
										else f.first_forecast * forecast.fit / s.total_forecast end)
						else 0 end,
			@forecast_tag
	from	#first_instance_forecast f inner join #sum_first_instance_forecast s on f.date = s.date inner join #original_forecast forecast on s.date = forecast.date
	-- Cada valor lo multiplica por el pronóstico y lo divide por el total, para que la suma de rates sea igual al pronóstico


	SELECT *
	into Temporal.[dbo].[ResultadosShareLocal_TEMP]
	from #final_forecast


	--drop table #sales
	drop table #initial_date
	drop table #first_matrix2
	drop table #total_sales
	drop table #daily_importance2
	drop table #monthly_importance
	drop table #second_matrix2
	drop table #sum_second_matrix2
	drop table #last_3months_sales
	drop table #last_1month_sales
	drop table #last_1week_sales
	drop table #first_instance_forecast
	drop table #sum_first_instance_forecast
	drop table #final_forecast
	drop table #original_forecast
	drop table #stores_to_replace
	--drop table #sales_by_store
	drop table #days_out_of_stock_3months
	drop table #days_out_of_stock_1month
	drop table #days_out_of_stock_1week
	drop table #store_importance


END


-- Aqui al final se encuentra el codigo anterior que usaba pivot tables (no era necesario). Se guarda por si fuese necesaria la estructura


	--create table #first_matrix (store_id int, Month varchar(15), Monday float, Tuesday float, Wednesday float, Thursday float, Friday float, Saturday float, Sunday float)
	--create table #daily_importance (store_id int, Monday float, Tuesday float, Wednesday float, Thursday float, Friday float, Saturday float, Sunday float)
	--create table #second_matrix (store_id int, Month varchar(15), Monday float, Tuesday float, Wednesday float, Thursday float, Friday float, Saturday float, Sunday float)
	--create table #sum_second_matrix (Month varchar(15), Monday float, Tuesday float, Wednesday float, Thursday float, Friday float, Saturday float, Sunday float)


	/*insert into #first_matrix			
	SELECT	Store,
			NombreMes,
			ISNULL(Monday,0) as Monday,
			ISNULL(Tuesday,0) as Tuesday,
			ISNULL(Wednesday,0) as Wednesday,
			ISNULL(Thursday,0) as Thursday,
			ISNULL(Friday,0) as Friday,
			ISNULL(Saturday,0) as Saturday,
			ISNULL(Sunday,0) as Sunday
	FROM ( 
			select	[Store], 
					DATENAME(m,date) as NombreMes,
					DATENAME(dw,date) as NombreDia,
					Quantity
			from [Salcobrand].[series].[Sales]
			where sku = 1999956
			) as P
			PIVOT 
			(
				SUM(Quantity)
				FOR NombreDia IN ([Monday],[Tuesday],[Wednesday],[Friday],[Saturday],[Thursday],[Sunday])
			) as pivote
		ORDER BY Store,NombreMes*/

	/*


	insert into #daily_importance
	select	f.store_id, 
			sum(Monday)/Quantity as Monday, 
			sum(Tuesday)/Quantity as Tuesday, 
			sum(Wednesday)/Quantity as Wednesday, 
			sum(Thursday)/Quantity as Thursday, 
			sum(Friday)/Quantity as Friday, 
			sum(Saturday)/Quantity as Saturday, 
			sum(Sunday)/Quantity as Sunday
	from #first_matrix f inner join #total_sales t on f.store_id = t.store_id 
	group by f.store_id, Quantity*/


	/*insert into #second_matrix 
	select	d.store_id, 
			m.Month as Month,
			d.Monday * m.Quantity as Monday,
			d.Tuesday * m.Quantity as Tuesday,
			d.Wednesday * m.Quantity as Wednesday,
			d.Thursday * m.Quantity as Thursday,
			d.Friday * m.Quantity as Friday,
			d.Saturday * m.Quantity as Saturday,
			d.Sunday * m.Quantity as Sunday
	from #daily_importance d inner join #monthly_importance m on d.store_id = m.store_id*/
	/*


	insert into #sum_second_matrix
	select	Month,
			sum(Monday) Monday,
			sum(Tuesday) Tuesday,
			sum(Wednesday) Wednesday,
			sum(Thursday) Thursday,
			sum(Friday) Friday,
			sum(Saturday) Saturday,
			sum(Sunday) Sunday
	from #second_matrix
	group by Month
	*/
	
	--drop table #first_matrix
	--drop table #daily_importance
	--drop table #second_matrix
	--drop table #sum_second_matrix