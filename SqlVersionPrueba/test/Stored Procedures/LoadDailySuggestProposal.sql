﻿
-- =============================================
-- Author:		Mauricio Sepúlvea
-- Create date: Carga la propuesta diaria de sugeridos
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [test].[LoadDailySuggestProposal]
	-- Add the parameters for the stored procedure here
	@date smalldatetime = NULL
AS
BEGIN
	DECLARE @msg VARCHAR(100) 
	SET NOCOUNT ON;

    -- Cargar replenishment.nextSuggest
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Cargando [replenishment].[NextSuggest]...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	truncate table [test].[NextSuggest]
	INSERT INTO [test].[NextSuggest]([SKU],[Store],[Max],[Min],[Rate],[Days],[LS],[OS],[Date])
	select p.[SKU],p.[Store],p.SuggestMax,p.SuggestMin,[Rate],[Days], 0, 0 ,p.[Date]
	from [test].PricingSuggest p 

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando los sugeridos que no provoquen cambios...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- eliminar los que no provoquen cambios
	delete s from [test].[NextSuggest] s
	inner join series.LastStock l on s.SKU = l.SKU and s.Store = l.Store
	where s.[Max]=l.[Max] and s.[Min]=l.[Min]

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando sugeridos a la baja que suban en el corto plazo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- borro las bajas que subiran en el corto plazo (4 semanas)
	delete n from [test].[NextSuggest] n
	inner join (select X.SKU, X.Store from (select A.*, case when f.W1>=f.W2 then case when f.W1>=f.W3 then case when f.W1>=f.W4 then f.W1
	else f.W4 end  else case when f.W3>=f.W4 then f.W3 else f.W4 end end else case when f.W2>=f.W3 then case when f.W2>=f.W4 then 
	f.W2 else f.W4 end else case when f.W3>=f.W4 then f.W3 else f.W4 end end end Sugerido_Maximo from
	(Select NS.SKU, NS.Store, NS.Max MaximoPropuesto, NS.Min MinimoPropuesto, 
	LS.Max MaximoActual, LS.Min MinimoActual, LS.Stock StockActual,
	FP.LogisticAreaDesc Bodega, FP.TotalRanking Ranking 
	from [test].NextSuggest NS
	inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	inner join products.Products FP on NS.SKU=FP.SKU
	where NS.Max<LS.Max) A
	inner join [test].[SuggestForecast] f 
	on A.SKU=f.SKU and A.Store=f.Store) X
	where X.Sugerido_Maximo>X.MaximoPropuesto) p on n.SKU = p.SKU and n.Store = p.Store
	
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos en alza por feriado...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- borro alza por feriado
	delete n from [test].[NextSuggest] n
	inner join (select X.SKU, X.Store, X.EndDate
		from (
			select A.*,f.SugHolidays , f.EndDate
			from(
				Select NS.SKU, NS.Store, NS.Max MaximoPropuesto, NS.Min MinimoPropuesto, 
				LS.Max MaximoActual, LS.Min MinimoActual, LS.Stock StockActual,
				FP.LogisticAreaDesc Bodega, FP.TotalRanking Ranking 
				from [test].NextSuggest NS
				inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
				inner join products.Products FP on NS.SKU=FP.SKU
			) A
			inner join minmax.HolidaySuggestNew f on A.SKU=f.SKU and A.Store=f.Store
			where dateadd(day,2,@date) >= f.BeginDate and @date <= f.EndDate
		) X where X.SugHolidays>=X.MaximoPropuesto
	) p on n.SKU = p.SKU and n.Store = p.Store and n.[Date] <= EndDate

	---- borro lo que esta en alza por feriado
	--delete n from [replenishment].[NextSuggest] n
	--inner join (select X.SKU, X.Store 
	--	from (
	--		select A.*,f.SugHolidays 
	--		from(
	--			Select NS.SKU, NS.Store, NS.Max MaximoPropuesto, NS.Min MinimoPropuesto, 
	--			LS.Max MaximoActual, LS.Min MinimoActual, LS.Stock StockActual,
	--			FP.LogisticAreaDesc Bodega, FP.TotalRanking Ranking 
	--			from replenishment.NextSuggest NS
	--			inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	--			inner join products.Products FP on NS.SKU=FP.SKU
	--		) A
	--		inner join minmax.HolidaySuggestNew f on A.SKU=f.SKU and A.Store=f.Store
	--		where @date between f.BeginDate and f.EndDate
	--	) X where X.SugHolidays>=X.MaximoPropuesto
	--) p on n.SKU = p.SKU and n.Store = p.Store

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos en promoción...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- eliminar promociones y próximas promociones (que partan hasta en un mes más)
	DELETE ps FROM [test].[NextSuggest] ps
	INNER JOIN promotion.PromotionStock pr ON pr.ProductId=ps.SKU and ps.[Date] between DATEADD(month, -1, BeginDate) and EndDate
	DELETE ps FROM [test].[NextSuggest] ps
	INNER JOIN promotion.PromotionDetail pr ON pr.ProductId=ps.SKU and ps.[Date] between DATEADD(month, -1, BeginDate) and EndDate

	
	----Borro temporalmente marca propia
	--delete from replenishment.NextSuggest where SKU in (
	--	select SKU from temp.MarcasPropias union select SKU from temp.Genericos
	--	)

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos con sugerencia cero o negativa...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--borro sugerido cero
	delete from [test].NextSuggest where [Max]<=0


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminación Lista...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga [replenishment].[NextSuggest] Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- Generar movimiento de sugerido 
	
	-- actualizar venta perdida esperada
	update n set n.LS = h.ls from [test].NextSuggest n inner join (
	select n.SKU,n.Store,	case when l.[Min]<n.[Min] then [function].GetLostSaleForSuggest(rate,l.[Min])-[function].GetLostSaleForSuggest(rate,n.[Min]) 	else 0 end ls
	from [test].NextSuggest n inner join series.LastStock l on n.SKU = l.SKU and n.Store = l.Store
	where l.[Min]<n.[Min] or l.[Max]<n.[Max]) h on h.SKU = n.SKU and h.Store = n.Store WHERE n.[Date]=@date
	
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de Venta Perdida Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- actualizar costo de inventario
	update n set n.OS = h.OS from [test].NextSuggest n inner join (select n.SKU,n.Store,case when l.[Max]>n.[Max] then l.[Max]-n.[Max] 
	else case when l.[Min]>n.[Min] then l.[Min]-n.[Min] else 0 end end OS from [test].NextSuggest n inner join series.LastStock l on n.SKU = l.SKU and n.Store = l.Store
	where l.[Min]>n.[Min] or l.[Max]>n.[Max]) h on h.SKU = n.SKU and h.Store = n.Store WHERE n.[Date]=@date

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de Costos de Inventario Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

END

