﻿
-- =============================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-28
-- =============================================
CREATE PROCEDURE [test].[GenerateRates_MirrorQ]
	-- Add the parameters for the stored procedure here
	@sku int,
	@forecast_tag nvarchar(150),
	@date datetime
AS
BEGIN

	SET NOCOUNT ON;

	create table #original_forecast (date date, fit float)
	insert into #original_forecast
	select Fecha, Fit 
	from [Temporal].[STD_Pronosticos].[Salcobrand] pron inner join [Salcobrand].[view].[RelacionIdCombinacion] ri on pron.IdCombinacion = ri.idcombinacion
	where tagcreacion = @forecast_tag and Fecha = @date and ri.idcodigocliente = @sku
	-- Si no se entrega forecast tag, entrega un share porcentual
	IF	(@forecast_tag is null)
	BEGIN 
			insert into #original_forecast
			select @date, 1
	END

	select datename(dw,fecha) as DiaSemana, *
	into #temp
	from [forecast].GenericVariables gv inner join [dbo].[Rates_SpecialDates] fe on gv.Fecha = fe.MirrorDate


	select DiaSemana, Fecha
	into #temp3
	from #temp
	-- Obtenemos el díasemana y la fecha de las fechas que reemplazan


	SELECT #temp3.fecha as Dia
		  ,sum([Quantity]) as VentaTotal
	into #total_temporada
	FROM	[series].[Sales] sales inner join 
			[stores].[Stores] stores on sales.store = stores.store inner join
			#temp3 on #temp3.Fecha = date
	group by #temp3.fecha  -- select * from #total_temporada
	-- Obtiene la venta total para la fecha que reemplaza


	SELECT city
		  ,[Date]
		  ,sum([Quantity]) as Venta, VentaTotal
		  ,case when VentaTotal > 0 then sum([Quantity])/ CONVERT(DECIMAL(10,2), VentaTotal) else 0 end Share
		  ,count(distinct stores.store) as #Stores
	into #ciudad_dia
	FROM	[series].[Sales] sales inner join
			[stores].[Stores] stores on sales.store = stores.store inner join
			#total_temporada on (Dia = Date)
	group by city, date, VentaTotal
	order by city, date asc  -- select * from #ciudad_dia
	-- Aqui obtiene para cada ciudad-día la venta, su share (venta/ventatotal) y # locales que vendieron


	select city, fe.date as date, Share as ShareCiudad, #Stores
	into #ciudad_fecha_share
	from #ciudad_dia cd inner join [dbo].[Rates_SpecialDates] fe on cd.date = fe.MirrorDate
	-- Usa los resultados de #ciudad_dia con la fecha futura


	declare @total_sales int
	set		@total_sales =		(select sum(Quantity) as Quantity
								from [series].[Sales] sales inner join
									 [stores].[Stores] stores on sales.Store = stores.Store
								where date >= DATEADD(d, -30, @date) and date <= @date)

	select City, sales.Store, sum(Quantity) as Quantity, sum(Quantity)/CONVERT(DECIMAL(10,2), @total_sales) as ShareLocal
	into #local_share
	from [series].[Sales] sales inner join
		 [stores].[Stores] stores on sales.Store = stores.Store
	where date >= DATEADD(d, -30, @date) and date <= @date 
	group by City, sales.Store
	order by city
	-- Obtiene la importancia de cada local según los últimos 30 días de historia (todos los sku)


	create table #resultado_proc(sku int, date datetime, store_id int, suggest float, forecast_tag nvarchar(150))

	EXEC	[test].[GenerateRates_Base]	@sku, NULL, @date, @date

	insert into #resultado_proc 
	select * from [dbo].[Rates_Temp_Demo]
	drop table [dbo].[Rates_Temp_Demo]


	-- IMPORTANTE: Aqui en adelante limpiar todo lo que no se usa (hay mucha basura)
	declare @suma_shares_ciudad_corregidos float
	set		@suma_shares_ciudad_corregidos =	(select sum(shareciudad)
									from #local_share local inner join #ciudad_fecha_share ciudad on local.city = ciudad.City 
									where date = @date )
	declare @suma_shares_multiplicados float
	set		@suma_shares_multiplicados =	(select sum(sharelocal*(shareciudad/@suma_shares_ciudad_corregidos))
											from #local_share local inner join #ciudad_fecha_share ciudad on local.city = ciudad.City 
											where date = @date )

	select r.sku, ciudad.City, Store, ciudad.Date, #Stores, r.suggest as ShareLocal_ProcAlm, ShareCiudad, ShareLocal, ShareCiudad/@suma_shares_ciudad_corregidos as ShareCiudadCorregido, sharelocal*(ShareCiudad/@suma_shares_ciudad_corregidos) as MultiplicacionShares, sharelocal*(ShareCiudad/@suma_shares_ciudad_corregidos)/@suma_shares_multiplicados as ShareFinal
	into #share_final
	from	#local_share local inner join  -- Warning: Si hay ciudad nueva, va a "desaparecer"
			#ciudad_fecha_share ciudad on local.city = ciudad.City left join
			#resultado_proc r on r.date =  ciudad.date and r.store_id = local.Store
	where ciudad.date = @date 
	order by ciudad.city  -- select * from #share_final

	declare @suma_shares_multiplicados_alm float
	set		@suma_shares_multiplicados_alm =	(select sum(ShareLocal_ProcAlm*ShareCiudad)
												from #share_final
												where sku is not null)


	select city, date, count(distinct store) as #StoresActuales
	into #available_stores
	from #share_final
	-- where sku is not null
	group by city, date
	-- Esto se va a usar para ver el crecimiento del número de locales que vende 


	select	sku, sf.city, store, sf.date, #StoresActuales as #StoresActuales_Ciudad, 
			#Stores as #StoresPasado_Ciudad, (#StoresActuales-#Stores)/CONVERT(DECIMAL(10,2),#Stores) as AumentoPorcStores,
			ShareLocal_ProcAlm as ShareLocalProducto, 
			ShareCiudad as ShareCiudadGeneral, 
			ShareLocal_ProcAlm*ShareCiudad*(1+((#StoresActuales-#Stores)/CONVERT(DECIMAL(10,2),#Stores))) as MultiplicacionShares
	into	#share_final_previo
	from	#share_final sf left join 
			#available_stores avs on (sf.city = avs.city and sf.date = avs.date)
	where	sf.sku is not null
	-- Aqui multiplica el share ciudad (nivel agregado) con share local (nivel sku) con el aumento % del # de tiendas

	declare @suma_share_multiplicados_corregidos float
	set		@suma_share_multiplicados_corregidos = (select sum(MultiplicacionShares)
													from #share_final_previo)

	select	SKU, City, Store, Date, #StoresActuales_Ciudad, #StoresPasado_Ciudad, AumentoPorcStores, 
			ShareLocalProducto, ShareCiudadGeneral, MultiplicacionShares, MultiplicacionShares/@suma_share_multiplicados_corregidos as ShareFinal
	into #share_final_final
	from #share_final_previo
	-- De aqui se obtienen los resultados finales, que son sku-date-store y rate final. El rate final es el share anterior
	-- dividido por su suma (para que quede %) y sea después multiplicado por el fit (para que la suma de rate sea el fit)


	select sku, sff.date as date, store as store_id, sharefinal*fit as suggest, @forecast_tag as forecast_tag
	into [dbo].[Rates_Temp]
	from #share_final_final sff inner join #original_forecast orf on sff.date = orf.date
	order by Store

	drop table #temp
	drop table #temp3
	drop table #total_temporada
	drop table #ciudad_dia
	drop table #local_share
	drop table #ciudad_fecha_share
	drop table #share_final
	drop table #resultado_proc
	drop table #share_final_previo
	drop table #share_final_final
	drop table #available_stores
	drop table #original_forecast

END
