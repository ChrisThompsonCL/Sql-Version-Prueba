﻿-- ================================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-28
-- IMPORTANTE: NO SE TERMINÓ DE MODIFICAR, SE PASO A OTRO TEMA A MITAD DE CAMINO
-- ================================================================================================
CREATE PROCEDURE [test].[GenerateRates_Base_Mirror]
	-- Add the parameters for the stored procedure here
	@sku int,
	@forecast_tag nvarchar(150),
	@beginning_date datetime,
	@finishing_date datetime

AS
BEGIN



	create table #sales (store_id int, sale_date date, quantity float)
	create table #initial_date (store_id int, initial_date date)
	create table #dias_venta (store_id int,Month varchar(15), days int)
	create table #monthly_sales_corrected(store_id int, Month varchar(15), quantity float)
	create table #first_matrix2 (store_id int, Month varchar(15), Day varchar(15), quantity float)
	create table #total_sales (store_id int, Quantity float)
	create table #daily_importance2 (store_id int, Day varchar(15), quantity float)
	create table #monthly_importance(store_id int, Month varchar(15), quantity float)
	create table #second_matrix2 (store_id int, Month varchar(15), Day varchar(15), quantity float)
	create table #sum_second_matrix2 (Month varchar(15), Day varchar(15), quantity float)
	create table #last_3months_sales (store_id int, Quantity float)
	create table #last_1month_sales (store_id int, Quantity float)
	create table #last_1week_sales (store_id int, Quantity float)
	create table #first_instance_forecast (date date, store_id int, first_forecast float)
	create table #sum_first_instance_forecast (date date, total_forecast float)
	create table #final_forecast (sku int, date date, store_id int, suggest float, forecast_tag nvarchar(150))
	create table #original_forecast (date date, fit float)
	create table #stores_to_replace (store_id int)
	create table #sales_by_store(store_id int, sales int)
	create table #days_out_of_stock_historic(Month varchar(15), store_id int, days int)
	create table #days_out_of_stock_3months (store_id int, days int)
	create table #days_out_of_stock_1month  (store_id int, days int)
	create table #days_out_of_stock_1week	(store_id int, days int)
	create table #store_importance (store_id int, Quantity float)
	create table #to_delete_2(llave nvarchar(20), store int, date date)
	create table #sales_previo(store_id int, sale_date date, quantity float)
	create table #stores(store_id int)
	create table #fechas(date date, store_id int)
	create table #sales_inter(llave nvarchar(20), store_id int, sale_date date, quantity float)



	SET NOCOUNT ON;


	declare @categoryID int
	set @categoryID = (select CategoryId from [products].[Products] where sku = @sku)


	insert into #original_forecast
	select Fecha, Fit 
	from [Temporal].[STD_Pronosticos].[Salcobrand] pron inner join [view].[RelacionIdCombinacion] ri on pron.IdCombinacion = ri.idcombinacion
	where tagcreacion = @forecast_tag and Fecha >= @beginning_date and Fecha <= @finishing_date and ri.idcodigocliente = @sku  -- Aqui quiza cambiar @sku por @sku_espejo


	insert into #sales
	select [Store] as store_id, date as sale_date ,sum([Quantity]) as Quantity
	from [series].[Sales] s inner join [products].[Products] p on s.sku = p.sku
	where CategoryId = @categoryID and date < @beginning_date and date >= DATEADD(ww, -3, @beginning_date)
	group by [Store], date



	insert into #second_matrix2
	select store_ID, month, day, Importance 
	from [dbo].[Rates_CategoryImportance]
	where category_ID = @categoryID

	
	insert into #sum_second_matrix2
	select	Month,
			Day,
			sum(quantity) as Quantity
	from #second_matrix2
	group by Month, Day

	
	insert into #store_importance
	select	store_id, avg(Quantity)
	from	#sales
	group by store_id



	DECLARE @Dt datetime
	SET @dt = @beginning_date
	WHILE @dt <= @finishing_date
		BEGIN
		
					-- Si no se entrega forecast tag, entrega un share porcentual
			IF	(@forecast_tag is null)
				BEGIN 
						insert into #original_forecast
						select @dt, 1
				END


			DECLARE @forecast float
			SET @forecast =	(select fit
								from #original_forecast
								where date = @dt)

			insert into #first_instance_forecast
			select	@dt,
					s.store_id,
					s.quantity * @forecast * l.quantity / m.quantity
			from	#second_matrix2 s inner join
					#store_importance l on s.store_id = l.store_id cross join
					#sum_second_matrix2 m
			where	s.day = DATENAME(dw, @dt) and s.Month = DATENAME(m, @dt) and
					m.Day  = DATENAME(dw, @dt) and m.Month = DATENAME(m, @dt)


			SET @dt = DATEADD(dd,1,@Dt)

		END


	insert into #sum_first_instance_forecast
	select	date,
			sum(first_forecast)
	from #first_instance_forecast
	group by date


	insert into #final_forecast
	select	@sku,
			s.date,
			store_id,
			case when	s.total_forecast > 0
						then (case when	(f.first_forecast * forecast.fit / s.total_forecast) is null 
										then 0 
										else f.first_forecast * forecast.fit / s.total_forecast end)
						else 0 end,
			@forecast_tag
	from	#first_instance_forecast f 
	inner join #sum_first_instance_forecast s on f.date = s.date 
	inner join #original_forecast forecast on s.date = forecast.date
	


	IF	(@forecast_tag is not null)
		BEGIN 
			insert into aux.Rates_Temp  (sku, store, forecast_tag, [date], Rate)
			SELECT sku, store_id, @forecast_tag, [date], suggest--RateFinal
			--from #nuevo_final
			from #final_forecast
		END
	ELSE
		-- Significa que estan llamando este procedimiento almacenado desde una fecha especial (guardamos en otro temp, para que no se guarde nada oficialmente)
		BEGIN 
		insert into [dbo].[Rates_Temp_Demo]
			SELECT * 
			--from #nuevo_final
			from #final_forecast
		END


	drop table #sales
	drop table #initial_date
	drop table #dias_venta 
	drop table #monthly_sales_corrected
	drop table #first_matrix2
	drop table #total_sales
	drop table #daily_importance2
	drop table #monthly_importance
	drop table #second_matrix2
	drop table #sum_second_matrix2
	drop table #last_3months_sales
	drop table #last_1month_sales
	drop table #last_1week_sales
	drop table #first_instance_forecast
	drop table #sum_first_instance_forecast
	drop table #final_forecast
	drop table #original_forecast
	drop table #stores_to_replace
	drop table #sales_by_store
	drop table #days_out_of_stock_historic
	drop table #days_out_of_stock_3months
	drop table #days_out_of_stock_1month
	drop table #days_out_of_stock_1week
	drop table #store_importance
	drop table #to_delete_2
	drop table #sales_previo
	drop table #stores
	drop table #fechas
	drop table #sales_inter
	drop table #new_sales


END