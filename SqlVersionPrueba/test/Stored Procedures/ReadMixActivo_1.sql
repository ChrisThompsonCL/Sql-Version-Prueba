﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [test].[ReadMixActivo]

@SKU       INT,
@DIV       VARCHAR(MAX),
@CAT       VARCHAR(MAX),
@SUB       VARCHAR(MAX),
@RAN       VARCHAR(MAX),
@STORE     INT

AS
BEGIN

	SET NOCOUNT ON;

DECLARE @query NVARCHAR(MAX) = '

SELECT pro.[SKU] , pro.[Name] as Descripcion, 
stores.[Store] as Local, stores.[Address] as Direccion, 
CAST (case when d.SKU IS NOT NULL then 1 ELSE 0 END as bit) as MixPricing 
FROM[Salcobrand].[series].[LastStock] ls 
INNER JOIN[Salcobrand].[Products].Products pro ON ls.SKU = pro.SKU 
INNER JOIN[Salcobrand].[Stores].Stores stores ON ls.Store = stores.Store 
LEFT JOIN(SELECT sku, store FROM [Salcobrand].[aux].DailySuggestMix dsm where (dsm.SKU = sku) and(dsm.Store = store) 
) d ON ls.SKU = d.SKU and ls.Store = d.Store 
WHERE (pro.[SKU] = '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' or '+ISNULL(CAST(@SKU AS varchar), 'NULL')+' is null)'; 

    IF @DIV is not null BEGIN
	SET @query += @DIV;
    END

EXEC sp_executesql @query;

END
