﻿

-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2012-04-24
-- Description:	calcula la reposicion por area logistica
-- =============================================
CREATE PROCEDURE [test].[GenerateReplenishment] 
	@date date
AS
BEGIN
	
	DECLARE @msg VARCHAR(100) 
	SET NOCOUNT ON;
	
	-- creación de tabla e indices
	CREATE TABLE #replenishment (store int,logisticAreaId nvarchar(50)  collate Modern_Spanish_CI_AS, [Date] smalldatetime, LT int,Picking bit, DBP int, days int)
	CREATE INDEX Idx1 ON #replenishment(store)
	CREATE INDEX Idx2 ON #replenishment([Date])
	CREATE INDEX Idx3 ON #replenishment(logisticAreaId)

	-- llenado de tablas: cruce de fechas, locales y bodegas
	insert into #replenishment
	select store,p.LogisticAreaId,d.[Date],0,0,0,0 
	from (
		select Store from stores.Stores
	) m, (
		select fecha [Date] from forecast.genericvariable 
		where fecha between  '2018-05-01' and '2018-09-30'
	) d, (
		select distinct logisticAreaId from products.Products 
		where LogisticAreaId is not null and LogisticAreaId not in ('Blan.Lan','Bespe.Pro')
	) p
	
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'se cargó la tabla inicial' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- se obtiene cuándo se genera picking y su leadtime asociado
	update r set LT = t.LT, Picking=1 
	from #replenishment r
	inner join replenishment.[Routes] t on t.store = r.store and t.logisticAreaId = r.logisticAreaId and datepart(weekday,r.[Date]) = t.[DayOfWeek]

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'se actualizó el LT y días de reposición' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- se calculan los días entre picking
	update t set t.DBP = cast(h.DBP as int) 
	from #replenishment t inner join 
	(
		select r1.Store,r1.logisticAreaId, r1.[Date], datediff(day,r1.[Date], min(r2.[Date]))  DBP
		from #replenishment r1 
		inner join #replenishment r2 on r1.store = r2.store and r1.logisticAreaId = r2.logisticAreaId and r2.[date] > r1.[date] 
		where r1.picking> 0 and r2.picking=1
		group by r1.Store,r1.logisticAreaId,r1.[date]
	) h
	on h.[Date] = t.[Date] and t.store = h.Store and t.logisticAreaId = h.logisticAreaId
	where t.Picking=1

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'se actualizó los días entre reposiciones' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 	

	update t set t.[Days]=h.[Days] 
	from #replenishment t 
	inner join (
		select r1.Store,r1.logisticAreaId,r1.[Date],r1.DBP+r2.LT [Days] 
		from #replenishment r1
		inner join #replenishment r2 on r1.store = r2.store and r1.logisticAreaId = r2.logisticAreaId 
		where r2.[Date] in (
			select min([Date]) 
			from #replenishment 
			where [Date] > r1.[Date] and store = r1.store and logisticAreaId=r1.logisticAreaId and DBP>0
		)
	) h on h.[Date] = t.[Date] and t.store = h.Store and t.logisticAreaId = h.logisticAreaId
	where t.DBP > 0

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'se actualizó los días a cubrir' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 	
	
	delete from test.DailyReplenishment where [Date]>= @date
	
	INSERT INTO test.DailyReplenishment ([Store],[LogisticAreaId],[Date],[Days],[LT])
    select store,logisticAreaId,dateadd(day,-1,[Date]),[Days], LT from #replenishment 
    where [Date] > @date --and [Date]<=DATEADD(day,100,@date)
    
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'se cargó la tabla final en la BD' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 	
    
END




