﻿-- =============================================
-- Author:		Elizabeth Peña
-- Create date: 19-07-2018
-- Description:	Procesa y prepara los datos de recetas para ser usados en calibración/pronóstico
-- =============================================
CREATE PROCEDURE [test].[RecetasPronostico_Multiple]
	-- Add the parameters for the stored procedure here
		@inicio date, 
		@fin date
AS
BEGIN
	SET NOCOUNT ON;

		--declare @inicio date = '2016-01-01'
		--declare @fin date = '2018-05-31'--(select top 1 [date] from Salcobrand.series.lastsale)

SELECT Fecha FechaError
INTO Salcobrand.aux.FechasSinRecetas
FROM Salcobrand.series.Recetas R
RIGHT JOIN Salcobrand.forecast.GenericVariables G ON G.Fecha = R.Date
WHERE FECHA BETWEEN @inicio AND @fin
GROUP BY FECHA
HAVING COUNT(DISTINCT FOLIO) = 0

	DECLARE @sku int
	DECLARE cursor_skus cursor fast_forward 
	FOR
	

	--declare @finreal date = (select top 1 [date] from Salcobrand.series.lastsale)
	select a.* from (select distinct IdCodigoCliente from [Salcobrand].dbo.[RelacionIDCombinacion]) a --Troya.Salcobrand.view.RelacionIdCombinacion) a
		-- 1er Filtro: Mix Activo		
				INNER JOIN Salcobrand.products.Products P on p.SKU = a.IdCodigoCliente
		---- 2do Filtro: N° datos total sea >= 60
				INNER JOIN (SELECT SKU
							FROM [Salcobrand].[series].[Recetas] 
							GROUP BY SKU
							HAVING COUNT(distinct DATE) >= 60
							--order by sku
							) NdatTotal on NdatTotal.SKU = A.IdCodigoCliente
		----3er Filtro N° datos actuales (últimos 2 meses) >=20
		--		INNER JOIN (SELECT SKU
		--					FROM [Salcobrand].[series].[Recetas] 
		--					WHERE DATE >= DATEADD(DAY, -60, @fin) --@finreal
		--					GROUP BY SKU
		--					HAVING COUNT(distinct DATE) >= 20
		--					--order by sku
		--					) NdatActual on NdatActual.SKU = A.IdCodigoCliente
	


	open cursor_skus
	fetch next from cursor_skus into @sku


	while @@fetch_status = 0
	begin
		exec Salcobrand.[test].[RecetasPronostico] @sku, @inicio, @fin
		fetch next from cursor_skus into @sku
	end

	close cursor_skus
	deallocate cursor_skus


	drop table Salcobrand.aux.FechasSinRecetas

	END