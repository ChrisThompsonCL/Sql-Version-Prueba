﻿CREATE TABLE [test].[RatesCurrent] (
    [SKU]        INT           NOT NULL,
    [Store]      INT           NOT NULL,
    [Date]       SMALLDATETIME NOT NULL,
    [Rate]       FLOAT (53)    NULL,
    [Days]       INT           NULL,
    [SuggestMin] INT           NULL,
    [LT]         INT           NULL,
    CONSTRAINT [PK_RatesCurrenttest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

