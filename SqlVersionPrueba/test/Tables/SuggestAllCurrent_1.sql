﻿CREATE TABLE [test].[SuggestAllCurrent] (
    [SKU]        BIGINT     NOT NULL,
    [Store]      INT        NOT NULL,
    [Date]       DATETIME   NOT NULL,
    [Days]       INT        NULL,
    [Rate]       FLOAT (53) NULL,
    [SuggestMin] INT        NULL,
    [SuggestMax] INT        NULL,
    CONSTRAINT [PK_SuggestAllCurrenttest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

