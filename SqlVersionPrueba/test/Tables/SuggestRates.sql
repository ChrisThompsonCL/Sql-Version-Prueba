﻿CREATE TABLE [test].[SuggestRates] (
    [SKU]   BIGINT     NOT NULL,
    [Store] INT        NOT NULL,
    [Week]  DATETIME   NOT NULL,
    [Type]  INT        NULL,
    [L1]    FLOAT (53) NULL,
    [M1]    FLOAT (53) NULL,
    [W1]    FLOAT (53) NULL,
    [J1]    FLOAT (53) NULL,
    [V1]    FLOAT (53) NULL,
    [S1]    FLOAT (53) NULL,
    [D1]    FLOAT (53) NULL,
    [L2]    FLOAT (53) NULL,
    [M2]    FLOAT (53) NULL,
    [W2]    FLOAT (53) NULL,
    [J2]    FLOAT (53) NULL,
    [V2]    FLOAT (53) NULL,
    [S2]    FLOAT (53) NULL,
    [D2]    FLOAT (53) NULL,
    CONSTRAINT [PK_TestSuggestRates] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Week] ASC)
);

