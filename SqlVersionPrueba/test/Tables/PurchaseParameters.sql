﻿CREATE TABLE [test].[PurchaseParameters] (
    [ManufacturerId] INT NOT NULL,
    [Division]       INT NOT NULL,
    [DayofPurchase]  INT NULL,
    [Frequency]      INT NULL,
    CONSTRAINT [PK_ManufacturerInformation_test] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [Division] ASC)
);

