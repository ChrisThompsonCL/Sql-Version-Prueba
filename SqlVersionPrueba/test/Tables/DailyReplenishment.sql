﻿CREATE TABLE [test].[DailyReplenishment] (
    [Store]          INT           NOT NULL,
    [LogisticAreaId] NVARCHAR (50) NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [Days]           INT           NULL,
    [LT]             INT           NULL
);

