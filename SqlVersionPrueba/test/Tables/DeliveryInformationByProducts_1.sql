﻿CREATE TABLE [test].[DeliveryInformationByProducts] (
    [SKU]                          INT           NOT NULL,
    [Date]                         SMALLDATETIME NOT NULL,
    [FillrateAvg]                  FLOAT (53)    NULL,
    [FillrateStd]                  FLOAT (53)    NULL,
    [LeadtimeAvg]                  FLOAT (53)    NULL,
    [LeadtimeStd]                  FLOAT (53)    NULL,
    [Norder]                       INT           NULL,
    [UsingManufacturerInformation] BIT           NULL,
    CONSTRAINT [PK_DeliveryInformationByProducts_test] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC)
);

