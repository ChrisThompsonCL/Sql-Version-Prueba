﻿CREATE TABLE [test].[CumpleReglas] (
    [NombreProducto]                   NVARCHAR (MAX) NOT NULL,
    [MargenEscenarios]                 INT            NOT NULL,
    [MargenMinimo]                     INT            NOT NULL,
    [PosicionamientoCompetenciaMaximo] INT            NOT NULL,
    [PosicionamientoCompetenciaMinimo] INT            NOT NULL,
    [VariacionMaximaAlza]              INT            NOT NULL,
    [VariacionMaximaBaja]              INT            NOT NULL
);

