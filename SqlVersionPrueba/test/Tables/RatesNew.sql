﻿CREATE TABLE [test].[RatesNew] (
    [SKU]        INT           NOT NULL,
    [Store]      INT           NOT NULL,
    [Date]       SMALLDATETIME NOT NULL,
    [Rate]       FLOAT (53)    NULL,
    [Days]       INT           NULL,
    [SuggestMin] INT           NULL,
    [LT]         INT           NULL,
    CONSTRAINT [PK_RatesNewtest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

