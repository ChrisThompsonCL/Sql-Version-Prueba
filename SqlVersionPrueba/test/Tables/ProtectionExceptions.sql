﻿CREATE TABLE [test].[ProtectionExceptions] (
    [SKU]          INT           NOT NULL,
    [ServiceLevel] FLOAT (53)    NULL,
    [BeginDate]    SMALLDATETIME NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    CONSTRAINT [PK_ProtectionExceptions_test] PRIMARY KEY CLUSTERED ([SKU] ASC, [BeginDate] ASC)
);

