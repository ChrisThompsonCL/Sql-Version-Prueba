﻿CREATE TABLE [test].[DeliveryInformationByManufacturer] (
    [ManufacturerId] INT           NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [FillrateAvg]    FLOAT (53)    NULL,
    [FillrateStd]    FLOAT (53)    NULL,
    [LeadtimeAvg]    FLOAT (53)    NULL,
    [LeadtimeStd]    FLOAT (53)    NULL,
    [Norder]         INT           NULL,
    CONSTRAINT [PK_DeliveryInformationByManufacturer_test] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [Date] ASC)
);

