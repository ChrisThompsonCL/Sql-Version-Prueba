﻿CREATE TABLE [test].[Recetas_AUX] (
    [Sku]     INT  NOT NULL,
    [fecha]   DATE NOT NULL,
    [Recetas] INT  NULL,
    PRIMARY KEY CLUSTERED ([Sku] ASC, [fecha] ASC)
);

