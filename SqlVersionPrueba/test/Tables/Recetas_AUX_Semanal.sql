﻿CREATE TABLE [test].[Recetas_AUX_Semanal] (
    [sku]                    INT  NOT NULL,
    [Semana]                 DATE NOT NULL,
    [Recetas]                INT  NULL,
    [DosSemanasAtras]        DATE NULL,
    [RecetasDosSemanasAtras] INT  NULL,
    PRIMARY KEY CLUSTERED ([sku] ASC, [Semana] ASC)
);

