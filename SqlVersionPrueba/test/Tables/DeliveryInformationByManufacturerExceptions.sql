﻿CREATE TABLE [test].[DeliveryInformationByManufacturerExceptions] (
    [ManufacturerId] INT           NOT NULL,
    [BeginDate]      SMALLDATETIME NOT NULL,
    [EndDate]        SMALLDATETIME NULL,
    [Fillrate]       FLOAT (53)    NULL,
    [Leadtime]       FLOAT (53)    NULL,
    CONSTRAINT [PK_DeliveryInformationByManufacturerException_test] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [BeginDate] ASC)
);

