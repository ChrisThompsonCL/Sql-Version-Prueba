﻿CREATE TABLE [test].[PricingSuggest] (
    [SKU]        BIGINT     NOT NULL,
    [Store]      INT        NOT NULL,
    [Date]       DATETIME   NOT NULL,
    [SuggestMin] INT        NULL,
    [SuggestMax] INT        NULL,
    [Days]       INT        NULL,
    [Rate]       FLOAT (53) NULL,
    CONSTRAINT [PK_PricingSuggest_test] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

