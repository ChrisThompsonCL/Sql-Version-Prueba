﻿CREATE TABLE [test].[ProtectionParameters] (
    [Ranking]      NVARCHAR (50) NULL,
    [Division]     INT           NULL,
    [ServiceLevel] FLOAT (53)    NULL
);

