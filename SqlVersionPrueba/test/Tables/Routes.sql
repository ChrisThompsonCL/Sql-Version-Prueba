﻿CREATE TABLE [test].[Routes] (
    [RouteId]        INT           NULL,
    [Store]          INT           NULL,
    [DayOfWeek]      INT           NULL,
    [LogisticAreaId] NVARCHAR (50) NULL,
    [LT]             INT           NULL
);

