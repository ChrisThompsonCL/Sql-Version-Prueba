﻿CREATE TABLE [test].[FitsAllNewdani] (
    [SKU]   INT        NOT NULL,
    [Store] INT        NOT NULL,
    [Date]  DATE       NOT NULL,
    [Fit]   FLOAT (53) NULL,
    CONSTRAINT [PK_FitsAllNewdani_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

