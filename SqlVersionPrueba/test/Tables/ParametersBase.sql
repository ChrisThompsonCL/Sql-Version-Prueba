﻿CREATE TABLE [test].[ParametersBase] (
    [ProductId]       INT           NOT NULL,
    [Frequency]       INT           NULL,
    [DayOfPurchase]   INT           NULL,
    [PurchaseDay]     SMALLDATETIME NULL,
    [NextPurchaseDay] SMALLDATETIME NULL,
    [Leadtime]        INT           NULL,
    [Protection]      FLOAT (53)    NULL,
    [FillRate]        FLOAT (53)    NULL,
    CONSTRAINT [PK_ParametersBase_test] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

