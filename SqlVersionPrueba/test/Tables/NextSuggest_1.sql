﻿CREATE TABLE [test].[NextSuggest] (
    [SKU]         INT           NOT NULL,
    [Store]       INT           NOT NULL,
    [Max]         INT           NULL,
    [Min]         INT           NULL,
    [Rate]        FLOAT (53)    NULL,
    [Days]        INT           NULL,
    [inOperation] BIT           NULL,
    [LS]          FLOAT (53)    NULL,
    [OS]          FLOAT (53)    NULL,
    [Date]        SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_NextSuggest_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

