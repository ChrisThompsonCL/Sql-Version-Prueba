﻿CREATE TABLE [test].[Pallets] (
    [SKU]    INT NOT NULL,
    [Pallet] INT NOT NULL,
    CONSTRAINT [PK_Pallets_test] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

