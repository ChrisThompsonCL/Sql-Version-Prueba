﻿CREATE TABLE [test].[Replenishment] (
    [Store] INT           NOT NULL,
    [Type]  INT           NOT NULL,
    [Week]  SMALLDATETIME NOT NULL,
    [L1]    INT           NULL,
    [M1]    INT           NULL,
    [W1]    INT           NULL,
    [J1]    INT           NULL,
    [V1]    INT           NULL,
    [S1]    INT           NULL,
    [D1]    INT           NULL,
    [L2]    INT           NULL,
    [M2]    INT           NULL,
    [W2]    INT           NULL,
    [J2]    INT           NULL,
    [V2]    INT           NULL,
    [S2]    INT           NULL,
    [D2]    INT           NULL,
    CONSTRAINT [PK_TestReplenishment] PRIMARY KEY CLUSTERED ([Type] ASC, [Store] ASC, [Week] ASC)
);

