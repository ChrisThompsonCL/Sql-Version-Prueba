﻿CREATE TABLE [test].[ShareStoreCurrent] (
    [SKU]   BIGINT        NOT NULL,
    [Store] INT           NOT NULL,
    [Week]  SMALLDATETIME NOT NULL,
    [Share] FLOAT (53)    NULL,
    CONSTRAINT [PK_ShareStoreCurrent_temp] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Week] ASC)
);

