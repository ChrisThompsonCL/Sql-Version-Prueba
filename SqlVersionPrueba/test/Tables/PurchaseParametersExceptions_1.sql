﻿CREATE TABLE [test].[PurchaseParametersExceptions] (
    [ManufacturerId]  INT           NOT NULL,
    [EventDate]       SMALLDATETIME NOT NULL,
    [NextPurchaseDay] SMALLDATETIME NULL,
    CONSTRAINT [PK_PurchaseParametersExceptions_test] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [EventDate] ASC)
);

