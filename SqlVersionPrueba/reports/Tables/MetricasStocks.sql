﻿CREATE TABLE [reports].[MetricasStocks] (
    [SKU]                               INT           NULL,
    [Date]                              SMALLDATETIME NOT NULL,
    [Stock]                             INT           NULL,
    [Sugerido]                          INT           NULL,
    [SugeridoModelo]                    INT           NULL,
    [StockAjustadoSugerido]             INT           NULL,
    [Locales]                           INT           NULL,
    [LocalesQuebrados]                  INT           NULL,
    [LocalesModelo]                     INT           NULL,
    [LocalesQuebradosModelo]            INT           NULL,
    [LocalesQuebradosConStockCD]        INT           NULL,
    [LocalesQuebradosSinFallaProveedor] INT           NULL,
    [StockTransito]                     INT           NULL,
    [StockTransitoAjustado]             INT           NULL
) ON [FG_200804Q];


GO
CREATE NONCLUSTERED INDEX [IX_MetricasStocks]
    ON [reports].[MetricasStocks]([SKU] ASC)
    ON [FG_200804Q];


GO
CREATE NONCLUSTERED INDEX [IX_MetricasStocks_1]
    ON [reports].[MetricasStocks]([Date] ASC)
    ON [FG_200804Q];


GO
CREATE NONCLUSTERED INDEX [IX_MetricasStocks_SKUDate]
    ON [reports].[MetricasStocks]([SKU] ASC, [Date] ASC)
    INCLUDE([Locales])
    ON [FG_200804Q];


GO
CREATE NONCLUSTERED INDEX [IX_MetricasStocks_Date]
    ON [reports].[MetricasStocks]([Date] ASC)
    INCLUDE([SKU], [Locales], [LocalesQuebrados])
    ON [FG_200804Q];


GO
CREATE NONCLUSTERED INDEX [IX_MetricasStocks_SKU]
    ON [reports].[MetricasStocks]([SKU] ASC)
    INCLUDE([Date], [Sugerido], [StockAjustadoSugerido], [Locales], [LocalesQuebrados])
    ON [FG_200804Q];

