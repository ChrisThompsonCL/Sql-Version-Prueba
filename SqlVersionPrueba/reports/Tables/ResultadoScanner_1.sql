﻿CREATE TABLE [reports].[ResultadoScanner] (
    [SKU]            INT            NOT NULL,
    [Store]          INT            NOT NULL,
    [Unidades]       INT            NULL,
    [Modelo]         INT            NULL,
    [Sugerido]       INT            NULL,
    [Stock]          INT            NULL,
    [Cost]           FLOAT (53)     NULL,
    [Restriccion]    NVARCHAR (250) NULL,
    [SugeridoModelo] INT            NULL,
    [Division]       INT            NULL,
    [NivelSugerido]  INT            NULL,
    [Ranking]        NVARCHAR (10)  NULL,
    [Venta]          INT            NULL,
    [Fecha]          DATE           NULL,
    CONSTRAINT [PK_ResultadoScanner] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC) ON [FG_200804Q]
);

