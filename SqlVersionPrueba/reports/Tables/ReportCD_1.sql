﻿CREATE TABLE [reports].[ReportCD] (
    [Date]        SMALLDATETIME NOT NULL,
    [SKU]         INT           NOT NULL,
    [ZeroStockCD] INT           NULL,
    [StockCD]     INT           NULL,
    [Order]       INT           NULL,
    [Dispatch]    INT           NULL,
    [Cost]        FLOAT (53)    NULL,
    [MotiveId]    INT           NULL,
    [MixActive]   BIT           NULL,
    CONSTRAINT [PK_ReportCD] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ReportCD]
    ON [reports].[ReportCD]([Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ReportCD_1]
    ON [reports].[ReportCD]([SKU] ASC);

