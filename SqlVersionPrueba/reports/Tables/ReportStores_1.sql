﻿CREATE TABLE [reports].[ReportStores] (
    [Date]           DATE         NOT NULL,
    [SKU]            INT          NOT NULL,
    [Store]          INT          NOT NULL,
    [Stock]          INT          NULL,
    [Suggest]        INT          NULL,
    [Quantity]       INT          NULL,
    [Income]         FLOAT (53)   NULL,
    [SPrice]         FLOAT (53)   NULL,
    [Cost]           FLOAT (53)   NULL,
    [ZeroStockCD]    BIT          NULL,
    [VE]             FLOAT (53)   NULL,
    [LostSale]       FLOAT (53)   NULL,
    [MotiveId]       INT          NULL,
    [IsBloqued]      NVARCHAR (1) NULL,
    [MixActive]      BIT          NULL,
    [Transit]        INT          NULL,
    [Min]            INT          NULL,
    [Max]            INT          NULL,
    [HasToPickUp]    BIT          NULL,
    [PickedUnits]    INT          NULL,
    [LostSale1]      FLOAT (53)   NULL,
    [LostSale2]      FLOAT (53)   NULL,
    [Days]           INT          NULL,
    [LeadTime]       INT          NULL,
    [ServiceLevel]   FLOAT (53)   NULL,
    [Rate]           FLOAT (53)   NULL,
    [SafetyDays]     INT          NULL,
    [R_Minimo]       INT          NULL,
    [R_Fijo]         INT          NULL,
    [R_Planograma]   INT          NULL,
    [R_MedioDUN]     INT          NULL,
    [SentSuggest]    BIT          NULL,
    [PricingMin]     INT          NULL,
    [PricingMax]     INT          NULL,
    [R_Maximo]       INT          NULL,
    [R_CajaTira]     INT          NULL,
    [R_FijoEstricto] INT          NULL,
    CONSTRAINT [PK_ReportStoresSW2] PRIMARY KEY NONCLUSTERED ([Date] ASC, [SKU] ASC, [Store] ASC) ON [PS_ReportStoresSW] ([Date])
) ON [PS_ReportStoresSW] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_ReportStores_Date]
    ON [reports].[ReportStores]([Date] ASC)
    INCLUDE([SKU], [Store], [Stock], [Suggest], [Quantity], [VE], [LostSale], [MixActive])
    ON [PS_ReportStoresSW] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_ReportStoresSW_SKUSTORE]
    ON [reports].[ReportStores]([SKU] ASC, [Store] ASC)
    INCLUDE([Stock])
    ON [PS_ReportStoresSW] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_ReportStores_DateStore]
    ON [reports].[ReportStores]([Date] ASC, [Store] ASC)
    INCLUDE([SKU], [Stock], [Suggest], [ZeroStockCD], [Transit])
    ON [PS_ReportStoresSW] ([Date]);


GO
CREATE NONCLUSTERED INDEX [IX_ReportStores_Quantity]
    ON [reports].[ReportStores]([Quantity] ASC)
    INCLUDE([Date], [SKU], [Store], [Stock])
    ON [PS_ReportStoresSW] ([Date]);

