﻿CREATE TABLE [reports].[InclusionenPasadas] (
    [SKU]                          INT         NULL,
    [Store]                        SMALLINT    NULL,
    [Fecha Inicio]                 DATE        NULL,
    [Clustering 1]                 BIT         NULL,
    [Clustering 2]                 BIT         NULL,
    [Intersección]                 BIT         NULL,
    [Unión]                        BIT         NULL,
    [Inversión Inicial]            FLOAT (53)  NULL,
    [Pef]                          FLOAT (53)  NULL,
    [VEQ Optimista]                FLOAT (53)  NULL,
    [VEQ Conservador]              FLOAT (53)  NULL,
    [Critico]                      BIT         NULL,
    [probabilidadExito]            FLOAT (53)  NULL,
    [sugeridoPromedio]             FLOAT (53)  NULL,
    [IngresosEsperadosConservador] FLOAT (53)  NULL,
    [IngresosEsperadosOptimista]   FLOAT (53)  NULL,
    [TotalRanking]                 VARCHAR (1) NULL,
    [division]                     SMALLINT    NULL,
    [Recuperación]                 FLOAT (53)  NULL,
    [CostoEspOpt]                  FLOAT (53)  NULL,
    [CostoEspCons]                 FLOAT (53)  NULL
);

