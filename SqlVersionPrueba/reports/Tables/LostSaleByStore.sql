﻿CREATE TABLE [reports].[LostSaleByStore] (
    [Date]          DATE       NOT NULL,
    [Store]         INT        NOT NULL,
    [QuiebreSKUs]   INT        NULL,
    [SKUs]          INT        NULL,
    [VtaPerdida]    FLOAT (53) NULL,
    [VtaReal]       INT        NULL,
    [VtaPerdidaVal] FLOAT (53) NULL,
    [VtaRealVal]    FLOAT (53) NULL,
    CONSTRAINT [PK_LostSaleByStore] PRIMARY KEY CLUSTERED ([Date] ASC, [Store] ASC)
);

