﻿CREATE TABLE [reports].[MinMaxCombinationsOld] (
    [SKU]       INT  NOT NULL,
    [Store]     INT  NOT NULL,
    [EntryDate] DATE NOT NULL,
    [ExitDate]  DATE NULL,
    CONSTRAINT [PK_MinMaxCombinationsOld] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [EntryDate] ASC)
);

