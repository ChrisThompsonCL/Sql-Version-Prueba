﻿CREATE TABLE [reports].[PickingReport] (
    [Date]                               DATE          NOT NULL,
    [SKU]                                INT           NOT NULL,
    [Division]                           INT           NULL,
    [LogisticAreaId]                     NVARCHAR (50) NULL,
    [Motive]                             NVARCHAR (50) NULL,
    [TotalRanking]                       NVARCHAR (1)  NULL,
    [Combinaciones]                      INT           NULL,
    [CombinacionesInformadas]            INT           NULL,
    [CombinacionesDebenPickear]          INT           NULL,
    [CombinacionesPickeadasCorrecto]     INT           NULL,
    [CombinacionesPickeadasTotal]        INT           NULL,
    [UnidadesDebenPickear]               INT           NULL,
    [UnidadesPickeadasCorrecto]          INT           NULL,
    [UnidadesPickeadasCorrectoCorregido] FLOAT (53)    NULL,
    [UnidadesPickeadasTotal]             INT           NULL,
    [Quiebre]                            INT           NULL,
    [VtaUN]                              INT           NULL,
    [Vta$]                               FLOAT (53)    NULL,
    [VtaPerdidaUN]                       FLOAT (53)    NULL,
    [VtaPerdida$]                        FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_PickingReport_Date]
    ON [reports].[PickingReport]([Date] ASC);

