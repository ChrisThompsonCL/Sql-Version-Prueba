﻿-- =============================================
-- Author:		<Juan M Hernández>
-- Modificado: <Fairenes Zerpa>
-- Create date: <2017-12-05>; <2018-01-10>
-- Description:	<reporte de venta perdida>
-- =============================================
CREATE PROCEDURE [reports].[VentaPerdida] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; declare @date date = getdate()

    -- Insert statements for procedure here
	
select
	 fp.DivisionUnificadaDesc División
	,fp.CategoriaUnificadaDesc Categoría
	,fp.SubcategoriaUnificadaDesc SubCategoría
	,fp.Motive Motivo
	,fp.ManufacturerDesc Proveedor
	,fp.totalranking
	,CASE WHEN fp.DivisionUnificadaId=1 THEN fp.Positioning_Desc ELSE NULL END Posicionamiento
	,CASE WHEN fp.DivisionUnificadaId=2 THEN fp.Class_Desc ELSE NULL END Clase
	,fp.SKU SKU
	,fp.Description Descripción
	,rs.Date Date
    , case when fp.sku in (select sku from products.GenericFamilyDetail) then 1 else 0 end [Genérico?]
    , cast(sum(case when Stock<= 0 then 1 else 0 end) as float) [Nquiebre Locales]
    , cast(sum(case when rs.ZeroStockCD=1 then 1 else 0 end) as float) [Nquiebre CD]

--Venta perdida Total
    , SUM(rs.LostSale) VtaPerdida
    , SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaVal
    , SUM(isnull(rs.Quantity,0)) VtaReal
    , SUM(isnull(rs.Income,0)) VtaRealVal
    , count(*) Combinaciones
    , case when SUM(isnull(rs.Income,0))>SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) then 1 else 0 end [VtaReal>VtaPerdida]
    , case when SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end)=0 then 1 else 0 end SinVtaPerdida
	, case when SUM(isnull(rs.Income,0))<0 then 1 else 0 end VentaNegativa

--Venta Perdida Mix Pricing
	, sum(case when mixactive=1 then rs.lostsale else 0 end) VtaPerdidaMix
	, SUM(case when rs.LostSale*rs.SPrice >0 and mixactive=1 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaValMix
	, SUM(case when mixactive=1 then isnull(rs.Quantity,0) else 0 end) VtaRealMix
	, SUM(case when mixactive=1 then isnull(rs.Income,0) else 0 end) VtaRealValMix
	,sum(case when mixactive=1 then 1 else 0 end) CombMix
    , cast(sum(case when Stock<= 0 and mixactive= 1 then 1 else 0 end) as float) [Nquiebre Locales Mix]
    , cast(sum(case when rs.ZeroStockCD=1 and mixactive=1 then 1 else 0 end) as float) [Nquiebre CD Mix]

  from reports.ReportStores rs
  inner join products.fullProducts fp on fp.sku=rs.SKU
    where rs.Date between cast(getdate()-5 as date) and cast(getdate()-2 as date) 
    and isnull(fp.DivisionUnificadaId, fp.Division) in (1,2)
	and VE>0 
	and isnull(TotalRanking,'NULL') not in ('Z','E')
	and rs.Date not in (select Date from dcpurchase.Holidays )
	and rs.SKU not in (select sku from products.GenericFamilyDetail)
    and isnull(fp.Motive,'')<>'2-Descontinuado'
	and isnull(ManufacturerId,'')<>990
   group by fp.DivisionUnificadaId, fp.DivisionUnificadaDesc
   , fp.CategoriaUnificadaDesc,fp.SubcategoriaUnificadaDesc 
   ,fp.ManufacturerDesc, fp.Positioning_Desc, fp.Class_Desc
   , fp.SKU, fp.Description, rs.Date,fp.Motive
  , ManufacturerId, fp.totalranking,  fp.CategoryId
END

