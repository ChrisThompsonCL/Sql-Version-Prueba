﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-01-05
-- Description:	Análisis de venta perdida agregada x sku, dia, para todo el report stores
-- =============================================
CREATE PROCEDURE [reports].[ExecLostSaleBySKU] 
	-- Add the parameters for the stored procedure here
	@date date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--truncate table reports.LostSaleBySKU
	delete from reports.LostSaleBySKU where [Date]=@date

    -- Insert statements for procedure here
	 insert into reports.LostSaleBySKU
	 select rs.[Date]
		, Division, Division_desc División, Positioning_desc Posicionamiento, Class_desc Clase, Motive Motivo
		, rs.SKU , Name Descripción, CategoryId, CategoryDesc Categoría
		, ManufacturerId, ManufacturerDesc, TotalRanking
		, fp.procuidado
		, sum(rs.Suggest) [Sugerido]
		, sum(rs.Stock) [Stock]
		, sum(case when Suggest<Stock then suggest else stock end) [StockAjustadoSugerido]
		, sum(case when Stock<= 0 then 1 else 0 end) [QuiebreLocales]
		, sum(case when rs.ZeroStockCD=1 then 1 else 0 end) [QuiebreCD]
		, SUM(rs.LostSale) VtaPerdida
		, SUM(isnull(Quantity,0)) VtaReal
		, SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) [VtaPerdidaVal]
		, SUM(isnull(Income,0)) VtaRealVal
		, count(*) Combinaciones
	from reports.ReportStores rs
	left join products.Products fp on fp.sku=rs.SKU
	where rs.MixActive=1 AND rs.[Date] = @date
		--and rs.SKU not in (select sku from products.products where (manufacturerId in (18,92) or categoryId=1)	and sku not in (select sku from mix.PilotDetails where pilotID=1))
	group by rs.[Date], Division, Division_desc, Positioning_desc, Class_desc, Motive
		, rs.SKU , Name, CategoryDesc, ManufacturerId, ManufacturerDesc, fp.procuidado, totalranking, categoryid

END
