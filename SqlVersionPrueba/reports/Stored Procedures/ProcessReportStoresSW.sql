﻿

-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2012/01/25
-- Description: Procesa ReportStoresSW para eliminar todo lo más antiguo que 3 meses hasta el mes indicado.
-- =============================================
CREATE PROC [reports].[ProcessReportStoresSW]
      @runDate date 
 
AS
                                                                                                                                                                                                                                                                       
SET NOCOUNT, XACT_ABORT ON;
 
DECLARE
        @error int,
        @rowCount bigint,
        @errorLine int,
        @message varchar(255),
		@expirationDate date,
        @partitionBoundaryDate date,
        @retentionMonths int,
        @sqlFileGroup varchar(3000);
			  
SET @error = 0;

-- Comenzamos el proceso de ventana deslizante.
BEGIN TRY

	-- Fijamos en 3, los meses dentro de la ventana.
	SET @retentionMonths = 3;
	
	-- Nos aseguramos de que la fecha sea el primer día del mes.
	IF @runDate IS NULL
		BEGIN SET @runDate = getdate();	END	
	SET @runDate = convert(date,convert(varchar,DATEPART(year,@runDate),4)+'-'+convert(varchar,DATEPART(month,@runDate),4)+'-1');

	SET @Message = 'Ejecutando para el día = ' + CONVERT(varchar(23), @runDate, 121); RAISERROR (@Message, 0, 1) WITH NOWAIT;

	
	-- declare @runDate date = '2016-03-01', @PartitionBoundaryDate date, @Message varchar(255), @ExpirationDate date,@retentionMonths int = 3
 
	-- Comenzamos asegurándonos de que existan los FileGroups para las siguientes particiones.	
	SET @PartitionBoundaryDate = DATEADD(day, -1,DATEADD(month, 1,  @runDate)); select @PartitionBoundaryDate
	WHILE @PartitionBoundaryDate <= DATEADD(day, -1,DATEADD(month, 2, @runDate))
	BEGIN
		IF NOT EXISTS
		(
			SELECT prv.value,*
			FROM sys.partition_functions AS pf
			JOIN sys.partition_range_values AS prv ON prv.function_id = pf.function_id
			WHERE pf.name = 'PF_ReportStoresSW' AND CAST(prv.value AS date) = @PartitionBoundaryDate
		)
		BEGIN		
			EXECUTE reports.[ManageFG_ReportStoresSW] @PartitionBoundaryDate,1	
			SET @Message = 'FILEGROUP y FILE creados para ' + CONVERT(varchar(23), @PartitionBoundaryDate, 121); RAISERROR (@Message, 0, 1) WITH NOWAIT;
		END;
		ELSE 
		BEGIN 
			SET @Message = 'FILEGROUP y FILE NO creados para ' + CONVERT(varchar(23), @PartitionBoundaryDate, 121); RAISERROR (@Message, 0, 1) WITH NOWAIT;
		END;
		
		-- Fijamos el borde para la próxima iteración.
		SET @PartitionBoundaryDate = DATEADD(day, -1,DATEADD(month, 1, DATEADD(day, 1, @PartitionBoundaryDate)));
	END
	

	-- declare @runDate date = '2016-03-01', @PartitionBoundaryDate date, @Message varchar(255), @ExpirationDate date,@retentionMonths int = 3

	-- Fijamos la fecha al mayor posible valor del mes anterior.
	declare @lastBoundaryDate date = DATEADD(day, -1, cast(@runDate AS date));


	-- Calculamos la fecha de expiración considerando @runDate y @retentionMonths. 
	-- si @retentionMonths = 3 -> son tres meses atrás, menos un día. 01-ene -> 30-sep
	SET @ExpirationDate =  DATEADD(day, -1, DATEADD(month, @retentionMonths * -1,  DATEADD(day, 1, @lastBoundaryDate)));
	select @ExpirationDate

	-- Desplegamos los bordes de las particiones actuales.
	RAISERROR ('Partition boundaries before maintenance:', 0, 1) WITH NOWAIT;
	
	SELECT CAST(prv.value AS date) AS PartitionBoundary
		FROM sys.partition_functions AS pf
		JOIN sys.partition_range_values AS prv ON
			  prv.function_id = pf.function_id
		WHERE pf.name = 'PF_ReportStoresSW';


	-- Obtenemos el borde más antiguo.
	SET @PartitionBoundaryDate = NULL;
	SELECT
		@PartitionBoundaryDate = CAST( MIN(prv.value) AS date)
	FROM sys.partition_functions AS pf
	JOIN sys.partition_range_values AS prv ON
		prv.function_id = pf.function_id
	WHERE pf.name = 'PF_ReportStoresSW';
	  

	BEGIN TRAN;

	--Obtenemos el uso exclusivo de la tabla.
	SELECT TOP 1 @error = 0 FROM reports.ReportStores WITH (TABLOCKX, HOLDLOCK);
	
	-- Hacemos SWITCH y MERGE de las particiones más antiguas que @expirationDate
	WHILE @PartitionBoundaryDate <= @ExpirationDate
	BEGIN
		-- Nos aseguramos que la tabla de transición esté vacía.
		TRUNCATE TABLE reports.ReportStoresStaging;
		ALTER TABLE reports.ReportStores SWITCH PARTITION 1
			  TO reports.ReportStoresStaging PARTITION 1;
		
		-- Obtenemos la cantidad de filas a eliminar, para mostrarlas en el mensaje.
		SELECT @RowCount = rows FROM sys.partitions WHERE object_id = OBJECT_ID(N'ReportStoresSWStaging') AND partition_number = 1;
		
		-- Eliminamos la información permanentemente.
		TRUNCATE TABLE reports.ReportStoresStaging;
		SET @Message =
			  'Moved data older than ' +
			  CONVERT(varchar(23), @PartitionBoundaryDate, 121) +
			  ' (' + CAST(@RowCount as varchar(20)) + ' rows) to staging table';
		RAISERROR(@Message, 0, 1) WITH NOWAIT;

		-- Hacemos MERGE de la primera y segunda particiones, para así eliminar la más antigua.
		ALTER PARTITION FUNCTION PF_ReportStoresSW()
			  MERGE RANGE(@PartitionBoundaryDate);
			  			  
		SET @Message = 'Removed boundary ' + CONVERT(varchar(30), @PartitionBoundaryDate, 121); RAISERROR(@Message, 0, 1) WITH NOWAIT;

		-- Obtenemos el borde más antiguo para la siguiente iteración.
		SET @PartitionBoundaryDate = NULL;
		
		SELECT @PartitionBoundaryDate = CAST(MIN(prv.value) AS date)
		FROM sys.partition_functions AS pf
		JOIN sys.partition_range_values AS prv ON prv.function_id = pf.function_id
		WHERE pf.name = 'PF_ReportStoresSW';
	END;

	-- Nos aseguramos de tener particiones individualizadas para los siguientes meses.
	
	-- declare @runDate date = '2014-10-01', @PartitionBoundaryDate date, @Message varchar(255), @sqlFilegroup varchar(3000)

	SET @PartitionBoundaryDate = DATEADD(day, -1,DATEADD(month, 1, @runDate));
	WHILE @PartitionBoundaryDate <= DATEADD(day, -1,DATEADD(month, 2, @runDate))
	BEGIN
		IF NOT EXISTS
			  (
			  SELECT prv.value
			  FROM sys.partition_functions AS pf
			  JOIN sys.partition_range_values AS prv ON
					prv.function_id = pf.function_id
			  WHERE
					pf.name = 'PF_ReportStoresSW'
					AND CAST(prv.value AS date) = @PartitionBoundaryDate
			  )
		BEGIN		
			  SELECT @sqlFileGroup = 'ALTER PARTITION SCHEME PS_ReportStoresSW NEXT USED [FG_ReportStoresSW'
										+cast((DATEPART(year,@PartitionBoundaryDate)*100+DATEPART(month,@PartitionBoundaryDate)) as varchar)+']'
			  EXEC(@sqlFileGroup)			
			  ALTER PARTITION FUNCTION PF_ReportStoresSW()
					SPLIT RANGE(@PartitionBoundaryDate);
			  SET @Message =
					'Created boundary ' + CONVERT(varchar(30), @PartitionBoundaryDate, 121);

			  RAISERROR(@Message, 0, 1) WITH NOWAIT;
		END;
		ELSE
		BEGIN
			  SET @Message =
					'Partition already exists for boundary ' + CONVERT(varchar(30), @PartitionBoundaryDate, 121);
			  RAISERROR(@Message, 0, 1) WITH NOWAIT;
		END;

		-- Fijamos el borde para la próxima iteración.
		SET @PartitionBoundaryDate = DATEADD(day, -1,DATEADD(month, 1, DATEADD(day, 1, @PartitionBoundaryDate)));
	END


	COMMIT;
		
		-- Nos aseguramos de eliminar los Filegroups que ya no se utilizan
	EXECUTE [reports].[ManageFG_CleanReportStoresSW]
				
 
END TRY
	
BEGIN CATCH
 
	SELECT
		@error = ERROR_NUMBER(),
		@Message = ERROR_MESSAGE(),
		@errorLine = ERROR_LINE();

	SET @Message =
		'Partition maintenenace failed with error %d at line %d: ' +
		@Message;
	RAISERROR(@Message, 16, 1, @error, @errorLine) WITH NOWAIT;

	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK;
	END;
 
END CATCH;



RAISERROR ('Partition boundaries after maintenance', 0, 1) WITH NOWAIT;
SELECT CAST(prv.value AS date) AS PartitionBoundary
      FROM sys.partition_functions AS pf
      JOIN sys.partition_range_values AS prv ON
            prv.function_id = pf.function_id
      WHERE pf.name = 'PF_ReportStoresSW';
 
Done:

--USAR EN CASO DE ERROR -> The file cannot be removed because it is not empty
--DBCC SHRINKFILE('ReportStoresSW201511', EMPTYFILE)

--ALTER DATABASE Salcobrand
--REMOVE FILE ReportStoresSW201511;

 
RETURN @error;



