﻿-- =============================================
-- Author:		Juan M Hernández
-- Create date: 2017-12-06
-- Description:	Análisis de venta perdida agregada x sku, dia, para todo el report stores
-- =============================================
CREATE PROCEDURE [reports].[ExecLostSaleBySKUCambio] 
	-- Add the parameters for the stored procedure here
	@date date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--truncate table reports.LostSaleBySKUCambio
	delete from reports.LostSaleBySKUCambio where [Date]=@date

    -- Insert statements for procedure here
	 insert into reports.LostSaleBySKUCambio
	 	select fp.DivisionUnificadaId, fp.DivisionUnificadaDesc
		,fp.Motive, fp.CategoriaunificadaDesc, fp.ManufacturerDesc
		, CASE WHEN fp.DivisionUnificadaId=1 THEN fp.Positioning_Desc ELSE NULL END Positioning_Desc
		, CASE WHEN fp.DivisionUnificadaId=2 THEN fp.Class_Desc ELSE NULL END Class_Desc
		, fp.SKU, fp.Description, rs.Date
		, case when fp.sku in (select sku from products.GenericFamilyDetail) then 1 else 0 end [Genérico?]
		, cast(sum(case when Stock<= 0 then 1 else 0 end) as float) [NQuiebreLocales]
		, cast(sum(case when rs.ZeroStockCD=1 then 1 else 0 end) as float) [NQuiebreCD]

--Venta Perdida Total
		, SUM(rs.LostSale) VtaPerdida
		, SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaVal
		, SUM(isnull(rs.Quantity,0)) VtaReal
		, SUM(isnull(rs.Income,0)) VtaRealVal
		, count(*) Combinaciones
		, case when SUM(isnull(rs.Income,0))>SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) then 1 else 0 end MenorVtaPerdida
		, case when SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end)=0 then 1 else 0 end SinVentaPerdida

--Venta Perdida Mix Pricing
		, sum(case when mixactive=1 then rs.lostsale else 0 end) VtaPerdidaMix
		, SUM(case when rs.LostSale*rs.SPrice >0 and mixactive=1 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaValMix
		, SUM(case when mixactive=1 then isnull(rs.Quantity,0) else 0 end) VtaRealMix
		, SUM(case when mixactive=1 then isnull(rs.Income,0) else 0 end) VtaRealValMix
		,sum(case when mixactive=1 then 1 else 0 end) CombMix
		, cast(sum(case when Stock<= 0  and mixactive=1 then 1 else 0 end) as float) [NQuiebreLocalesMix]
		, cast(sum(case when rs.ZeroStockCD=1 and mixactive=1 then 1 else 0 end) as float) [NQuiebreCDMix]
	from reports.ReportStores rs
	left join products.fullProducts fp on fp.sku=rs.SKU
	where rs.Date =@date and VE>0 --@date and VE>0
	group by fp.DivisionUnificadaId,fp.DivisionUnificadaDesc, fp.CategoriaunificadaDesc, fp.ManufacturerDesc, fp.Positioning_Desc, fp.Class_Desc, fp.SKU, fp.Description, rs.Date,fp.Motive
	, ManufacturerId, CategoryId 

END


