﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [reports].[MonitoringChangePrice]

AS
BEGIN

---tabla 1
--------------------------------Reporte de Monitoreo Cambio de Precios-------------------------------------------------------
declare @Hasta as date=cast(getdate()-2 as date)
declare @Desde as date=cast(DATEADD(MONTH,-1,@Hasta) as date)

declare @LunesPrimero as date=DATEADD(DAY,-7,dbo.GetMonday(getdate()))
declare @LunesSegundo as date=DATEADD(DAY,-14,dbo.GetMonday(getdate()))
declare @LunesPrimero2 as date=DATEADD(DAY,-7-7,dbo.GetMonday(getdate()))
declare @LunesSegundo2 as date=DATEADD(DAY,-14-7,dbo.GetMonday(getdate()))

--SELECT 'LunesPrimero' Monday, @LunesPrimero Fecha
--UNION ALL
--SELECT 'LunesSegundo' Monday, @LunesSegundo Fecha
--UNION ALL
--SELECT 'LunesPrimero2' Monday, @LunesPrimero2 Fecha
--UNION ALL
--SELECT 'LunesSegundo2' Monday, @LunesSegundo2 Fecha

--CAMBIAN DE PRECIO
select pc.date,cast(dbo.getmonday(pc.date) as date) Semana,dgp.SKU
, pc.Price,pc.Cost PromCost,PriceEf
, b.Unidades, b.Ingresos
, pc.Price*Unidades PrecioListPond
, PriceEf*Unidades PrecioEfPond
,pc.Cost*Unidades CostoPond
, CASE WHEN c.dif<>0 then 'CambiosÚltimaSemana'  WHEN c1.dif<>0
	then 'CambiosAnteriorSemana' ELSE 'SinCambios' END Cambios
, CASE 
	when (C.DIF<0 or C1.DIF<0) then 'Baja' 
	when (C.DIF>0 or C1.DIF>0) then 'Sube' END DirecciónCambios
, Division_Desc División
, cast(pc.motive as varchar(3)) Motive, OperativeMotive.isoperative Operativo
, case when manufacturerId in (18,92) or categoryid=1 then 1 else 0 end Generico
, case when etiq.sku is not null then 1 else 0 end Etiquetado
FROM products.DemandGroupProduct dgp
INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
LEFT JOIN class.ProductClassificationFinal pcf on pcf.SKU = dgp.SKU 
INNER JOIN series.priceandcost pc ON pc.SKU = dgp.SKU
INNER JOIN products.Products P ON dgp.SKU=P.SKU
----MOTIVOS POR SKU OPERATIVOS			
--LEFT JOIN (SELECT [SKU], [Motive], [Cost], [Price], [Date] 
--			FROM (SELECT [SKU], [Motive], [Cost], [Price], [Date], ROW_NUMBER() over (partition by sku order by [Date] desc) rn
--			FROM [Salcobrand].[temp].[PreciosPiloto] p) a Where rn = 1) MOTIVE ON MOTIVE.SKU=dgp.SKU
LEFT JOIN (select * from pricing.ProductMotive) OperativeMotive on cast(OperativeMotive.MotiveId as varchar(50))=cast(pc.Motive as varchar(50))
---- SKUs que cambiaron PList de lunes a lunes
LEFT JOIN (SELECT * FROM (SELECT A.SKU, A.PRICE ListPriceAntes, B.PRICE ListPriceDespués, (B.PRICE-A.PRICE)/A.PRICE DIF
			FROM (select SKU, PRICE 
					from series.PriceAndCost
					where date=@LunesPrimero)b
			INNER JOIN (select SKU, PRICE 
						from series.priceandcost
						where date=@LunesSegundo)a ON A.SKU=B.SKU
			WHERE (B.PRICE-A.PRICE)>10 OR (B.PRICE-A.PRICE)<-10
			AND A.PRICE>10 AND B.PRICE>10)a ) C ON dgp.SKU=C.SKU
LEFT JOIN (SELECT * FROM (SELECT A.SKU, A.PRICE ListPriceAntes, B.PRICE ListPriceDespués, (B.PRICE-A.PRICE)/A.PRICE DIF
			FROM (select SKU, PRICE 
					from series.PriceAndCost
					where date=@LunesPrimero2)b
			INNER JOIN (select SKU, PRICE 
						from series.priceandcost
						where date=@LunesSegundo2)a ON A.SKU=B.SKU
			 WHERE (B.PRICE-A.PRICE)>10 OR (B.PRICE-A.PRICE)<-10
			 AND A.PRICE>10 AND B.PRICE>10)a) C1 ON dgp.SKU=C1.SKU
--Ventas para Ponderar
LEFT JOIN [pricing].[SKUEtiquetados] etiq on pc.sku=etiq.sku
LEFT JOIN (
			select date, s.sku, sum(Quantity) Unidades, sum(Income) Ingresos, sum(income)/sum(Quantity) PriceEf from series.DailySales s
			where date BETWEEN @Desde AND @Hasta
			group by s.sku, date
			having sum(Quantity)>0
			) B ON Pc.SKU=B.SKU and b.date=pc.date
where pc.date BETWEEN @Desde AND @Hasta and pc.price>1 and pc.cost>0 and dgn.ParentId in (2,3) --and pc.sku<>9909661
--AND PC.SKU NOT IN (2100002)

END
