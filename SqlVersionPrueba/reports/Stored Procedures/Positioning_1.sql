﻿-- =============================================
-- Author:		<Felipe Sepulveda>
-- Create date: <2017-10-25>
-- Description:	<Reporte de Posicionamiento>
-- Modificación: Fai <2018-05-04>
-- =============================================
CREATE PROCEDURE [reports].[Positioning]

AS
BEGIN
--------------------------------------------------------------HOJA LISTA SB--------------------------------------------------------------------------------------------
select *, 
case when VentaSB is null or VentaSB=0 or VentaCV is null or VentaCV=0 or VentaFASA is null or VentaFASA=0 then 0 else 1 end [Filtro Ventas]
, case when [Precio SB] = 0 or [Precio SB] is null or [Precio FASA] is null  then 1 else (case when ((ABS([Precio FASA]-[Precio SB]))/[Precio SB])>=0.2 and [Precio SB]>=5000 then 1 else 0 end) end [Filtro 20% FASA]
, case when [Precio SB] = 0 or [Precio SB] is null or [Precio FASA] is null  then 1 else (case when ((ABS([Precio FASA]-[Precio SB]))/[Precio SB])>=0.3 and [Precio SB]<5000 then 1 else 0 end) end [Filtro 30% FASA]
, case when [Precio SB] = 0 or [Precio SB] is null or [Precio CV] is null  then 1 else (case when ((ABS([Precio CV]-[Precio SB]))/[Precio SB])>=0.2 and [Precio SB]>=5000 then 1 else 0 end) end [Filtro 20% CV]
, case when [Precio SB] = 0 or [Precio SB] is null or [Precio CV] is null  then 1 else (case when ((ABS([Precio CV]-[Precio SB]))/[Precio SB])>=0.3 and [Precio SB]<5000 then 1 else 0 end) end [Filtro 30% CV]
from (SELECT *, [Precio SB]*[Unidades SB] [VentaSB], [Precio FASA]*[Unidades SB] [VentaFASA], [Precio CV]*[Unidades SB] [VentaCV],  Costo*[Unidades SB] CompraSB
,  ([Precio SB]*[Unidades SB])-(Costo*[Unidades SB]) Margen
FROM (SELECT [Date] Fecha
		, DivisionUnificadaDesc DivisiónUnif, CategoriaUnificadaDesc CategoríaUnif, SubcategoriaUnificadaDesc SubCategoríaUnif, ClaseUnificadaDesc ClaseUnif
		, a.Category [Id Categoría], a.CategoryName Categoría, p.CategoryDesc CategoryMaestro
		, case when p.division=1 then p.positioning_Desc 
				when p.division=2 then p.Class_Desc  end [Clase/Posicionamiento]
		, a.Clase [Id Clase]
		, a.SKU, p.name Nombre, case when p.division=1 then p.Positioning_Desc else p.Class_Desc END Category
		, ROUND(SUM(Cost*Quantity)/SUM(CASE WHEN Cost > 0 THEN Quantity ELSE NULL END),2) Costo
		, ROUND(SUM(PriceSB*Quantity)/SUM(CASE WHEN PriceSB > 0 THEN Quantity ELSE NULL END),2) [Precio SB]
		, ROUND(SUM(PriceFasa*Quantity)/SUM(CASE WHEN PriceFasa > 0 THEN Quantity ELSE NULL END),2) [Precio FASA]
		, ROUND(SUM(PriceCV*Quantity)/SUM(CASE WHEN PriceCV > 0 THEN Quantity ELSE NULL END),2) [Precio CV]
		, ROUND(SUM(CASE WHEN PriceSB > 0 AND PriceFasa > 0 THEN PriceSB/PriceFasa*Quantity ELSE NULL END)/SUM(CASE WHEN PriceSB > 0 AND PriceFasa > 0 THEN Quantity ELSE NULL END),2) [R FASA]
		, ROUND(SUM(CASE WHEN PriceSB > 0 AND PriceCV > 0 THEN PriceSB/PriceCV*Quantity ELSE NULL END)/SUM(CASE WHEN PriceSB > 0 AND PriceCV > 0 THEN Quantity ELSE NULL END),2) [R CV]
		, SUM(Quantity) [Unidades SB]
	FROM (
	SELECT pc.[Date], dgn.Id Category, dgn.Name CategoryName, dgp.SKU, pcf.clase
		, pc.Price PriceSB
		, case when comp.LastUpdateFasa>=dateadd(day,-180, getdate()) then COMP.Fasa else NULL end PriceFasa
		, case when comp.LastUpdateCV>=dateadd(day,-180, getdate()) then comp.CV else NULL end PriceCV
		, case when comp.LastUpdateSimi>=dateadd(day,-180, getdate()) then comp.simi else NULL end PriceSimi
		, pc.Cost
		, Quantity
	FROM products.DemandGroupProduct dgp
	INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	LEFT JOIN (SELECT s.*
               FROM [PricingDSS].[estrategia].[Clasificacion] c
               inner join [PricingDSS].setup.obtenerproductos (3040,1,0) s on s.IdCombinacion=c.IdCombinacion
			  ) pcf on pcf.idcodigocliente = dgp.SKU 
	INNER JOIN pricing.LastPriceAndCost pc ON pc.SKU = dgp.SKU 
	LEFT JOIN(select * from pricing.LastCompetitorsPrice 
	where ( LastUpdateCV >=dateadd(day,-180, getdate()) or LastUpdateSimi>=dateadd(day,-180, getdate()) 
		or LastUpdateFasa>=dateadd(day,-180, getdate()))) comp ON comp.SKU = dgp.SKU
	LEFT JOIN (SELECT [Date], SKU, ISNULL(SUM(Quantity),0) Quantity, SUM(Income) Income FROM series.Sales where [Date] BETWEEN DATEADD(day,-7,(select MAX([Date]) FROM pricing.LastPriceAndCost))
				 AND (select MAX([Date]) FROM pricing.LastPriceAndCost) GROUP BY [Date], SKU) v on v.SKU = dgp.SKU
	WHERE Quantity > 0 and dgn.ParentId in (2,3)) a
	INNER JOIN products.Products p on p.SKU = a.SKU
	GROUP BY [Date], DivisionUnificadaDesc, CategoriaUnificadaDesc, SubcategoriaUnificadaDesc, ClaseUnificadaDesc
	, a.Category, a.CategoryName, a.Clase, a.SKU, p.name
	, p.Division, p.Class_Desc, p.Positioning_Desc,p.CategoryDesc)a)b





--------------------------------------------------------------HOJA EFECTIVO SB--------------------------------------------------------------------------------------------
	DECLARE @weeks int = 4
	declare @maxDateC date,  @MinDate1wC date, @maxDateF date, @MinDate1wF date
	
	select @maxDateC = max(Semana) from series.Nielsen
	select @MinDate1wC = DATEADD(week, -@weeks+1, @maxDateC)

	select @maxDateF = max([Date]) from series.IMS
	select @MinDate1wF = DATEADD(week, -@weeks+1, @maxDateF)


(SELECT *, ([Unidades Cadena]-[Unidades SB]) [Un. Competidores], [PrecioEfect Competidores]*([Unidades Cadena] - [Unidades SB]) [Vtas Competidores], [PrecioEfect SB]*[Unidades SB] [Ventas SB], 
		[PrecioEfect Competidores]*([Unidades Cadena]-[Unidades SB]) [REAL Vtas Competidores1], [Unidades SB]*[Costo Unid SB] [Compra SB], [VentaEfect SB]-([Unidades SB]*[Costo Unid SB]) [MargenRealSB]
		, CASE WHEN ([PrecioEfect Competidores]*[Unidades Cadena])=0 OR ([PrecioEfect SB]*[Unidades Cadena])=0 THEN 0 ELSE 1 END [Filtro Ventas]
		, case when [PrecioEfect SB] = 0 then 1 else (case when ((ABS([PrecioEfect Competidores]-[PrecioEfect SB]))/[PrecioEfect SB])>=0.2 and [PrecioEfect SB]>=5000 then 1 else 0 end) end [Filtro 20% Competidores] 
		, case when [PrecioEfect SB] = 0 then 1 else (case when ((ABS([PrecioEfect Competidores]-[PrecioEfect SB]))/[PrecioEfect SB])>=0.3 and [PrecioEfect SB]<5000 then 1 else 0 end) end [Filtro 30% Competidores] 

		FROM (SELECT 
		cast(m.Date as date) Fecha, p.DivisionUnificadaDesc DivisionUnif, p.CategoriaUnificadaDesc CategoríaUnif, p.SubcategoriaUnificadaDesc SubcategoríaUnif, p.ClaseUnificadaDesc ClaseUnif
		, dgn.Id [Id Categoría], dgn.Name Categoría, pcf.Clase,p.SKU SKU , p.Name Nombre
		, SUM(UnidadesCadenas) [Unidades Cadena]
		, SUM(UnidadesSB) [Unidades SB]
		, SUM(VentasCadenas) [Ventas Cadena]
		, SUM(VentasSB) [VentaEfect SB]
		, CostSB [Costo Unid SB]
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end [PrecioEfect SB]
		, case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end [PrecioEfect Competidores]
		, case when p.division=1 then p.Positioning_Desc else p.Class_Desc END Category
		
		FROM (
		SELECT [Date], i.SKU, UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas
		FROM series.IMS i
		WHERE Date BETWEEN @MinDate1wF and @maxDateF
	) m
	INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN (select * from products.Products where division is not null) p on p.SKU=m.SKU
	LEFT JOIN (
		SELECT dbo.GetMonday([Date]) [Date], SKU, AVG(Cost) CostSB
		FROM series.PriceAndCost
		WHERE [Date] BETWEEN DATEADD(WK, -(4-1), (select max(Date) from series.IMS))  and (select max(Date) from series.IMS)
		GROUP BY dbo.GetMonday([Date]), SKU
	) c on c.SKU=m.SKU and c.Date=m.Date
	LEFT JOIN (SELECT s.*
               FROM [PricingDSS].[estrategia].[Clasificacion] c
               inner join [PricingDSS].setup.obtenerproductos (3040,1,0) s on s.IdCombinacion=c.IdCombinacion) pcf on pcf.idcodigocliente = m.SKU 
	WHERE p.Division=1
	GROUP BY m.date, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, p.ClaseUnificadaDesc, dgn.Id, dgn.Name, pcf.Clase, p.SKU, p.Name, CostSB, UnidadesCadenas-UnidadesSB
	, p.division, p.Positioning_Desc, p.Class_Desc)A)


UNION ALL


(SELECT *, [Unidades Cadena]-[Unidades SB] [Un. Competidores], [PrecioEfect Competidores]*([Unidades Cadena]- [Unidades SB]) [Vtas Competidores], [PrecioEfect SB]*[Unidades SB] [Ventas SB], 
	[PrecioEfect Competidores]*([Unidades Cadena]-[Unidades SB]) [REAL Vtas Competidores1], [Unidades SB]*[Costo Unid SB] [Compra SB], [VentaEfect SB]-([Unidades SB]*[Costo Unid SB]) [MargenRealSB]
	, CASE WHEN ([PrecioEfect Competidores]*[Unidades Cadena])=0 OR ([PrecioEfect SB]*[Unidades Cadena])=0 THEN 0 ELSE 1 END [Filtro Ventas]
	, case when [PrecioEfect SB] = 0 then 1 else (case when ((ABS([PrecioEfect Competidores]-[PrecioEfect SB]))/[PrecioEfect SB])>=0.2 and [PrecioEfect SB]>=5000 then 1 else 0 end) end [Filtro 20% Competidores] 
	, case when [PrecioEfect SB] = 0 then 1 else (case when ((ABS([PrecioEfect Competidores]-[PrecioEfect SB]))/[PrecioEfect SB])>=0.3 and [PrecioEfect SB]<5000 then 1 else 0 end) end [Filtro 30% Competidores] 
	FROM (SELECT 
		cast(Semana as date) Fecha, p.DivisionUnificadaDesc DivisionUnif, p.CategoriaUnificadaDesc CategoríaUnif, p.SubcategoriaUnificadaDesc SubcategoríaUnif, p.ClaseUnificadaDesc ClaseUnif
		, dgn.Id [Id Categoría], dgn.Name Categoría, pcf.clase,p.SKU SKU , p.Name Nombre
		, SUM(UnidadesCadena) [Unidades Cadena]
		, SUM(UnidadesSB) [Unidades SB]
		, SUM(ValoresCadena) [Ventas Cadena]
		, SUM(ValoresSB) [VentaEfect SB]
		, CostSB [Costo Unid SB]
		, case when SUM(UnidadesSB) >0 then SUM(ValoresSB)/SUM(UnidadesSB) else 0 end [PrecioEfect SB]
		, case when SUM(UnidadesCadena - UnidadesSB) > 0 then SUM(ValoresCadena - ValoresSB)/SUM(UnidadesCadena - UnidadesSB) else 0 end [PrecioEfect Competidores]
		, case when p.division=1 then p.Positioning_Desc else p.Class_Desc END Category
		FROM (
		SELECT Semana, i.SKU, UnidadesSB, UnidadesCadena, ValoresSB, ValoresCadena
		FROM series.Nielsen i
		WHERE Semana BETWEEN @MinDate1wC and @maxDateC
	) m
	INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN (select * from products.Products where division is not null) p on p.SKU=m.SKU
	LEFT JOIN (
		SELECT dbo.GetMonday([Date]) [Date], SKU, AVG(Cost) CostSB
		FROM series.PriceAndCost
		WHERE [Date] BETWEEN DATEADD(WK, -(4-1), (select max(Semana) from series.NIELSEN))  and (select max(Semana) from series.NIELSEN)
		GROUP BY dbo.GetMonday(Date), SKU
	) c on c.SKU=m.SKU and c.Date=m.Semana

	LEFT JOIN (SELECT s.*
               FROM [PricingDSS].[estrategia].[Clasificacion] c
               inner join [PricingDSS].setup.obtenerproductos (3040,1,0) s on s.IdCombinacion=c.IdCombinacion) pcf on pcf.idcodigocliente = m.SKU 
	WHERE p.Division=2
	and p.SKU not in (select sku from 
					(

					select p.Description, a.*
					 from (select * 
					, ABS([PrecioSB (data)]-[PrecioSB (nielsen)])/ cast([PrecioSB (data)] as float) [%DIF] 
					from (select s.SKU
					, SUM(ValoresSB) [VentasSB (nielsen)]
					, SUM(UnidadesSB) [UnidadesSB (nielsen)]
					, case when SUM(UnidadesSB) >0 then SUM(ValoresSB)/SUM(UnidadesSB) else 0 end [PrecioSB (nielsen)]
					, SUM(Income) [VentasSBData (data)]
					, SUM(Quantity) [UnidadesSB (data)]
					, case when SUM(Quantity) >0 then SUM(Income)/SUM(Quantity) else 0 end [PrecioSB (data)]
					from series.Nielsen s
					left join forecast.WeeklySales ws on s.sku=ws.sku and s.Semana=ws.Date
					where s.Semana >= @MinDate1wC and ws.SKU is not null and ws.Date is not null 
					group by s.sku)a
					where [VentasSBData (data)] is not null)a
					LEFT JOIN products.fullproducts p on a.sku=p.SKU
					where [%DIF] >cast(0.5 as float)
					and [UnidadesSB (nielsen)]<=500
					)a)
	GROUP BY m.Semana, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, p.ClaseUnificadaDesc, dgn.Id, dgn.Name, pcf.clase, p.SKU, p.Name, CostSB, UnidadesCadena-UnidadesSB
	, p.division, p.Positioning_Desc, p.Class_Desc) B)


--------------------------------------------------------------HOJA EFECTIVO SB--------------------------------------------------------------------------------------------

select 'Efectivo IMS' Precio, CAST(max(date) AS DATE) Fecha from series.ims
UNION ALL
select 'Efectivo NIELSEN' Precio, CAST(max(Semana) AS DATE) Fecha from  series.Nielsen
UNION ALL
select 'Sistema SB' Precio, CAST(max(date) AS DATE) Fecha from pricing.LastPriceAndCost

END
