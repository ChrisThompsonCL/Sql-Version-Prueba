﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-06-28
-- Description:	obtiene pronósticos del último periodo
-- =============================================
CREATE PROCEDURE [reports].[GetPredictedDailySales]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @datekey date = (select Max(DateKey) from forecast.predicteddailysales)

	select CONVERT(VARCHAR(10), [date], 126) Fecha, ProductId SKU, REPLACE(cast(cast(Fit as decimal(30,5)) as nvarchar(max)),',','-') Fit
	from forecast.predicteddailysales
	where [DateKey]=@datekey

END
