﻿-- =============================================
-- Author:		Diego Oyarzún
-- Create date: 2010-05-17
-- Description:	Calcula el reporte diario de locales. Adaptado por JPC para 
-- =============================================
CREATE PROCEDURE [reports].[ExecReportStores] 
	@today date
AS

BEGIN

---------------------------------------------------------
---- Actualizaciones con información cargada de HOY. ----
---------------------------------------------------------
DECLARE @msg VARCHAR(80) 

declare @monday date =  dbo.GetMonday(@today)
--declare @datekey date = (select max(datekey) from forecast.predictedSales where Datekey < @monday) --CAMBIAR
declare @datekey date = (select max(datekey) from forecast.predictedDailySales where Datekey < @monday) 
declare @auxDate date = (select max(date) from reports.MetricasStocks)

update r set r.VE=h.VE
from [reports].[ReportStores] r
inner join (
	select p.sku,p.Store, a.Minimo VE
	from (
		select s.sku,s.Store,s.Date,s.fit 
		from minmax.FitsAllCurrent s 
		where s.[Date] = @today
	) p 
	left join (
		select ls.SKU, ls.Store, ISNULL(q.Quantity,0) VtaDiaMax
		from (
			select SKU
			from reports.metricasStocks
			where date > dateadd(day, -30, @auxDate) 
			group by SKU
			having ROUND(SUM(cast(LocalesQuebrados*1.0/(Locales*1.0) as float)),0)/30.0 < 0.5
		) skussnoqebrados
		inner join series.LastStock ls on ls.SKU=skussnoqebrados.SKU
		left join (
			select sku, store , a.Maximo Quantity 
			from  [minmax].[DailySales] 
			cross apply [function].Maximo(AvgLargo, AvgMes) a
			where  a.Maximo>0 
		) q on q.SKU=ls.SKU and ls.Store=q.Store
	) ds on ds.SKU=p.sku and p.Store=ds.Store 
	cross apply [function].Minimo(p.Fit, VtaDiaMax*5.0) a
) h  on h.SKU = r.SKU and h.Store = r.Store 
where r.[Date] = @today

update l set VE = r.VE
from [reports].[LastReportStores] l
inner join (select * from [reports].[ReportStores] where [Date] = @today and VE>0) r on r.SKU=l.SKU and r.Store=l.Store

SET @msg = convert(varchar,getdate(),100)+' - '+ 'VE actualizado.' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 



---------------------------------------------------------
---- Actualizaciones con información cargada de AYER ----
---------------------------------------------------------

declare @yesterday date = dateadd(day,-1,@today)
SET @msg = convert(varchar,getdate(),100)+' - '+ 'Comenzando complemento para '+CONVERT(nvarchar(40),@yesterday)+' ...' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 

update r set Quantity = sale.Quantity, Income = sale.Income
from (select * from reports.ReportStores where date = @yesterday) r
inner join series.LastSale sale on sale.SKU=r.sku and sale.Store=r.store
SET @msg = convert(varchar,getdate(),100)+' - '+ 'Vnta actualizada.' RAISERROR (@msg, 0, 1) WITH NOWAIT 

-- Se actualiza el PSEUDO PRECIO diario para cada SKU de aquellos con venta y copiando la información desde el día anterior para aquellas combinaciones sin ventas.
update r set r.SPrice=isnull(s.SPrice,isnull(r2.SPrice,isnull(pc.Price,0))) 
from [reports].[ReportStores] r
left join (
	select sku,store,SPrice 
	from  [reports].[ReportStores] 
	where [Date] = DATEADD(day,-1,@yesterday)
) r2  on r2.SKU = r.SKU and r2.store = r.store
left join  (
	select SKU, CONVERT(float,(income)/(quantity)) SPrice 
	from series.DailySales  
	where Date = @yesterday and quantity>0 and Income>0
) s on s.SKU = r.SKU
left join  (
	select SKU, Price 
	from series.PriceAndCost 
	where Date = @yesterday
) pc on pc.SKU = r.SKU	
where Date = @yesterday 


SET @msg = convert(varchar,getdate(),100)+' - '+ 'PseudoPrecio actualizado.' RAISERROR (@msg, 0, 1) WITH NOWAIT 
-- actualiza unidades efectivamente pickeadas
update l set PickedUnits = case
		when (r.Stock+r.Transit+r.Quantity)>(l.Stock+l.Transit) then (r.Stock+r.Transit+r.Quantity)-(l.Stock+l.Transit)
	else 0 end
from (select * from reports.ReportStores where date = @yesterday)  r
inner join (select * from reports.ReportStores where Date = DATEADD(day,-1,@yesterday)) l on r.sku = l.sku and r.Store = l.Store 
where ((r.Stock+r.Transit+r.Quantity)>(l.Stock+l.Transit))


SET @msg = convert(varchar,getdate(),100)+' - '+ 'Picking actualizado' RAISERROR (@msg, 0, 1) WITH NOWAIT 

delete from reports.GenericReportStores where [date] = @yesterday

insert into  reports.GenericReportStores
select [date], GenericFamilyId, Store
	, SUM(Stock) Stock, MAX(Suggest)Suggest
	, SUM(Quantity)Quantity, SUM(Income)Income
	, AVG(SPrice)SPrice, AVG(Cost)Cost, Min(case when ZeroStockCD=1 then 1 else 0 end)ZeroStockCD
	, MAX(VE)VE, 0 LostSale, null MotiveId
	, cast(SUM(case when IsBloqued='S' then 1 else 0  end) as float)/count(*)  IsBloqued
	, cast(SUM(case when MixActive=1 then 1 else 0 end) as float)/count(*) MixActive
	, SUM(Transit)Transit, MAX([Min])[Min], Max([Max])[Max]
	, MIN(case when HasToPickUp=1 then 1 else 0 end)HasToPickUp
	, SUM(PickedUnits)PickedUnits
	, 0 LostSale1, 0 LostSale2
from reports.ReportStores rs
inner join products.GenericFamilyDetail gfd on gfd.SKU=rs.SKU
where rs.[date]=@yesterday 
group by [date], GenericFamilyId, Store

SET @msg = convert(varchar,getdate(),100)+' - '+ 'ReportStores para genéricos actualizado' RAISERROR (@msg, 0, 1) WITH NOWAIT 

	

	-- Calculo la VENTA PERDIDA
	update [reports].[ReportStores] 
	set LostSale = case 
			when suggest>0 and stock<=0 then [function].GetLostSale(VE,quantity) else 0 
		end
	where [Date] = @yesterday 

	update [reports].[ReportStores] 
	set LostSale1 = case 
			when suggest>1 and stock=1 then [function].GetLostSale(VE,quantity) else LostSale 
		end 
	where [Date] = @yesterday 

	update [reports].[ReportStores] 
	set LostSale2 = case 
			when suggest>2 and stock=2 then [function].GetLostSale(VE,quantity) else LostSale1
		end 
	where [Date] = @yesterday 


		-- Calculo la VENTA PERDIDA
	update [reports].GenericReportStores 
	set LostSale = case 
			when suggest>0 and stock<=0 then [function].GetLostSale(VE,quantity) else 0 
		end
	where [Date] = @yesterday 

	update [reports].GenericReportStores 
	set LostSale1 = case 
			when suggest>1 and stock=1 then [function].GetLostSale(VE,quantity) else LostSale 
		end 
	where [Date] = @yesterday 

	update [reports].GenericReportStores 
	set LostSale2 = case 
			when suggest>2 and stock=2 then [function].GetLostSale(VE,quantity) else LostSale1
		end 
	where [Date] = @yesterday 

SET @msg = convert(varchar,getdate(),100)+' - '+ 'LostSale actualizado.' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 

exec [reports].[ExecDailyReportStores]
SET @msg = convert(varchar,getdate(),100)+' - '+ 'Listo reporte diario.' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 

exec [reports].[ExecLostSaleBySKUCambio] @yesterday
exec [reports].[ExecLostSaleByStore] @yesterday
SET @msg = convert(varchar,getdate(),100)+' - '+ 'Listo venta perdida por producto y local.' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 

/*meter gap quiebre y gap vp*/

set @yesterday = dateadd(day,-1,@yesterday)
exec [reports].[ExecPickingReport] @yesterday
SET @msg = convert(varchar,getdate(),100)+' - '+ 'Listo reporte de picking.' RAISERROR (@msg, 0, 1 ) WITH NOWAIT 

END

