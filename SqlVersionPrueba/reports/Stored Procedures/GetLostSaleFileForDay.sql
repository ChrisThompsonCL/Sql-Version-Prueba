﻿-- =============================================
-- Author:		Dani Albornoz FEDEX
-- Create date: 2018-02-07
-- Description:	genera archivo de venta perdida que se carga a SB en el FTP
-- =============================================
CREATE PROCEDURE [reports].[GetLostSaleFileForDay] 
	-- Add the parameters for the stored procedure here
	@date date
AS
BEGIN

	SET NOCOUNT ON;
	declare @date1 date = @date

	SELECT SKU,Store AS Local,CONVERT(VARCHAR(10), Date, 126) AS Fecha
		,REPLACE(cast(cast(LostSale as decimal(18,5)) as nvarchar(max)),',','-') VtaPerdidaUN
		,REPLACE(cast(cast(LostSale*SPrice as decimal(18,5)) as nvarchar(max)),',','-') VtaPerdida$
		,REPLACE(cast(cast(Quantity as decimal(18,5)) as nvarchar(max)),',','-') VtaRealUN
		,REPLACE(cast(cast(Income as decimal(18,5)) as nvarchar(max)),',','-') VtaReal$
	FROM reports.ReportStores rs 
	where [Date]=@date1
		 and VE > 0
		 --and rs.MixActive=1
		 --and rs.SKU not in (select sku from products.products where (manufacturerId in (18,92) or categoryId=1) union all select sku from products.Products where TotalRanking='Z')
		 and (quantity > 0 or LostSale > 0)
		 and SKU not in (select SKU from products.GenericFamilyDetail)
END
