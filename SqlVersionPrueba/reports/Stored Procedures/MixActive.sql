﻿-- =============================================
-- Author:		<Fairenes Zerpa>
-- Create date: <29-09-2017>
-- Description:	<Reporte Mix Activo>
-- =============================================
CREATE PROCEDURE [reports].[MixActive]
	-- Add the parameters for the stored procedure here

AS
BEGIN


--Tabla 1
exec Salcobrand.[analysis].[DailyExcludedCombinations]


	--Tabla 2
	---------------Detalle por tipo de restricción y por nivel sugerido------------------------------------------
 SELECT Sca.sku
,case when [modelo] = 1 then 'Modelo' else 'SinModelo' end Modelo
, isnull([restriccion],'Sin Restricción') [Restricción]
, case when sca.[Division] = 1 then 'Farma' else 'Consumo' end [División]
, [Ranking]
, case when cast(sca.Sugerido as varchar)=1 then '1'
			when cast(sca.Sugerido as varchar)=2 then '2'
			when cast(sca.sugerido as varchar)= 3 then '3'
			when cast(sca.Sugerido as varchar) in (4,5) then '4 a 5'
			when cast(sca.sugerido as varchar) in (6,7,8,9,10) then '6 a 10'
			when cast(sca.sugerido as varchar)>10 then '11 o mas' end [Nivel Sugerido]
,p.Description,p.Class_Desc,p.Positioning_Desc,p.ManufacturerDesc, p.CategoryDesc,count(*) Combinaciones
, sum(stock*cost) StockValorado
, sum(case when unidades > 0 then unidades*cost else cost end) VentaValoradaCosto
, case when p.ManufacturerId=92 or p.ManufacturerId=18 or p.CategoryId=1 then 1 else 0 end [Lab 92?]
, case when m.ManufacturerId=p.ManufacturerId then 1 else 0 end [Marca Propia?]
, case when sum(stock*cost)<0 then 1 else 0 end [FiltroStockNegativo]
FROM [Salcobrand].[reports].[ResultadoScanner] Sca
left join products.FullProducts P on sca.sku=p.SKU
left join operation.PrivateLabelManufacturers M on p.ManufacturerId=m.ManufacturerId
where NivelSugerido>0
group by [modelo],[restriccion],sca.[Division],[Ranking],NivelSugerido,Sca.sku,p.Description,p.Class_Desc
 ,p.Positioning_Desc,p.ManufacturerDesc,m.ManufacturerId,p.ManufacturerId,p.CategoryId, p.CategoryDesc, sca.Sugerido


--Tabla 3
------------------Detalle Combinaciones Totales ---------------------
select 'Dun por combinación' Descripción , count(*) Combinaciones from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 6 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'Dun por producto' descripcion,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 5 ) p
inner join series.LastStock l on p.ProductID = l.sku
UNION ALL
select 'Mínimo por combinación' descripcion,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 4 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'Mínimo por producto' descripcion, count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 3 ) p
inner join series.LastStock l on p.ProductID = l.sku 
UNION ALL
select 'Carga Manual' descripcion,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 2 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'PlanogramaFarma' descripcion ,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 9 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'PlanogramaConsumo' descripcion ,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 10 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'PlanogramaDermo' descripcion ,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 11 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
UNION ALL
select 'RestricciónRankingA' descripcion ,count(*) from (
select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
where SuggestRestrictionTypeID = 8 ) p
inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store


--Tabla 4
--------------------------------SKUS's Fuera--------------------------------------

select Description [Descripción], sku [SKU] 
       from products.FullProducts 
	   where sku in (
					select distinct sku from series.LastStock
					where sku not in (
					select distinct ProductId from [function].[GetOperativeCombination](3,0,3,0,3)))
order by SKU


--Tabla 5
------------------------------- Locales Fuera-------------------------------------
select store [Local],Description [Descripción] 
		from stores.FullStores 
		where store in (
						select distinct store from series.LastStock
						where store not in (
						select distinct StoreId from [function].[GetOperativeCombination](3,0,3,0,3)))
order by store


--Tabla 6
--INACTIVOS
select sku, Description Descripción, ManufacturerDesc Laboratorio, CategoryDesc Categoría
from products.FULLproducts where (manufacturerId in (18,92) or categoryId=1)


--Tabla 7
------------------- Combinaciones Con Stock Negativo en Locales ---------------------
select l.sku, p.Description,l.Store,l.Date,l.Stock 
		from series.LastStock L
		left join products.FullProducts P on L.sku=p.sku
		where stock<0
order by stock


--Tabla 8
---------------------------Loboratorios Fuera -------------------------------------
Select distinct ManufacturerDesc 
		from products.FullProducts 
		where ManufacturerId not in (
select ManufacturerId 
		from operation.InOperationManufacturerAllStore) and IsActive=1
order by ManufacturerDesc







-- ------------------------------------Detalle combinaciones Activas-------------------------------------



--Select resumen.Restricción
--		,count(resumen.SKU) Combinaciones
--		,sum(resumen.[Sugerido Valorado])SugeridoValorado
--		,sum(resumen.[Sugerido Modelo Valorado]) SugeridoModeloValorado

--from (

--select l.sku SKU,l.Store Local,l.[Max] Sugerido, l.stock [Sugerido Modelo],m.suggestModelo,p.Cost Costo,'Dun por combinación' Restricción
--	, fp.name Descripción
--	, fp.Division_Desc División
--	, fp.Class_Desc Clase
--	, fp.Positioning_Desc Posicionamiento
--	, fp.TotalRanking Ranking
--	, fp.ManufacturerDesc Proveedor
--	,l.[Max]*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.[Max],l.[Min],l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 6 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
----where l.[Max]-l.[Min]>p.Detail/2
--) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store 
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.sku,l.Store,l.[Max] suggest, l.stock ,m.suggestModelo,p.Cost,'Dun por producto' descripcion 
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.[Max]*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.[Max],l.[Min],l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 5 ) p
--inner join series.LastStock l on p.ProductID = l.sku
----where l.[Max]-l.[Min]>p.Detail/2
-- ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store 
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'Mínimo por combinación' descripcion 
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 4 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store 
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'Mínimo por producto' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 3 ) p
--inner join series.LastStock l on p.ProductID = l.sku
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'Carga Manual' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 2 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store 
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'PlanogramaFarma' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 9 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'PlanogramaConsumo' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 10 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'Bloqueadas' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--SELECT sku,store,suggest,stock FROM [Salcobrand].[series].[LastStock] where IsBloqued='S') l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku  


--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'PlanogramaDermo' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 11 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--UNION ALL
--select l.*,m.suggestModelo,p.Cost,'RestriccionRankingA' descripcion  
--	, fp.name
--	, fp.Division_Desc
--	, fp.Class_Desc
--	, fp.Positioning_Desc
--	, fp.TotalRanking
--	, fp.ManufacturerDesc
--	,l.Suggest*cost [Sugerido Valorado]
--	,m.suggestModelo*cost [Sugerido Modelo Valorado]
--from (
--select l.sku,l.Store,l.Suggest,l.Stock from (
--select * from [function].[GetActiveSuggestRestriction] (getdate()-1)
--where SuggestRestrictionTypeID = 8 ) p
--inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
--where p.Detail = l.Suggest ) l
--inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
--group by sku,store) m
--on l.sku = m.sku and l.Store = m.Store
--LEFT JOIN products.products fp on m.sku=fp.sku
--inner join pricing.LastPriceAndCost p
--on p.sku = m.sku
--) resumen

--group by Restricción


			
END
