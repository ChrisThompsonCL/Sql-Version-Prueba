﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-07-23
-- Description:	Análisis de venta perdida agregada x store, dia, para todo el report stores
-- =============================================
CREATE PROCEDURE [reports].[ExecLostSaleByStore] 
	-- Add the parameters for the stored procedure here
	@date date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--truncate table reports.LostSaleBySKU
	delete from reports.LostSaleByStore where [Date]=@date

    -- Insert statements for procedure here
	insert into reports.LostSaleByStore
	select rs.[Date], rs.store
		, SUM(case when stock=0 then 1 else 0 end) QuiebreSKUs
		, count(*) SKUs
		, SUM(rs.LostSale) VtaPerdida
		, SUM(isnull(rs.Quantity,0)) VtaReal
		, SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) [VtaPerdidaVal]
		, SUM(isnull(rs.Income,0)) VtaRealVal
	from reports.ReportStores rs
	where MixActive=1 
		--and SKU not in (select sku from products.products where (manufacturerId in (18,92) or categoryId=1) and sku not in (select sku from mix.PilotDetails where pilotID=1))
		and [Date] = @date
	group by rs.[Date], rs.Store



END

