﻿-- =============================================
-- Author:		<Juan M Hernández>
-- Modificado: <Fairenes Zerpa>
-- Create date: <2017-12-05>; <2018-01-10>
-- Description:	<Historia de venta perdida>
-- =============================================
CREATE PROCEDURE [reports].[HistoriaVentaPerdida]
	
	
AS
BEGIN
	
	SET NOCOUNT ON;
	declare @date date = (select max(date) from reports.LostSaleBySKUcambio)
	
select rs.Date, p.DivisionUnificadaDesc División
--Venta Perdida Total
		, SUM([VtaPerdidaVal])/(case when cast( SUM(VtaRealVal+[VtaPerdidaVal]) as float)=0 then NULL else cast( SUM(VtaRealVal+[VtaPerdidaVal]) as float) end)  [Vta Perdida $]
		, SUM([VtaPerdida])/(case when cast( SUM(VtaReal+[VtaPerdida]) as float)=0 then NULL else cast( SUM(VtaReal+[VtaPerdida]) as float) end ) [Vta Perdida Un]
		, SUM([VtaPerdidaVal]) [VtaPerdida$]
		, SUM([VtaPerdida]) [VtaPerdidaUn]

--Venta Perdida Mix Pricing
		, SUM([VtaPerdidaValMix])/(case when cast( SUM(VtaRealValMix+[VtaPerdidaValMix]) as float)=0 then NULL else cast( SUM(VtaRealValMix+[VtaPerdidaValMix]) as float) end)  [Vta Perdida Mix $]
		, SUM([VtaPerdidaMix])/(case when cast( SUM(VtaRealMix+[VtaPerdidaMix]) as float)=0 then NULL else cast( SUM(VtaRealMix+[VtaPerdidaMix]) as float) end ) [Vta Perdida Mix Un]
		, SUM([VtaPerdidaValMix]) [VtaPerdidaMix$]
		, SUM([VtaPerdidaMix]) [VtaPerdidaMixUn]
	from  reports.LostSaleBySKUCambio rs
	inner JOIN products.fullproducts p on rs.sku=p.sku
	where rs.Date between DATEADD(MONTH, -1,@date) and cast(@date as date) 
		and isnull(p.DivisionUnificadaId, p.Division) in (1,2)
		and isnull(TotalRanking,'NULL') not in ('Z','E')
		and rs.SKU not in (select sku from products.GenericFamilyDetail)
		and rs.Date not in (select Date from dcpurchase.Holidays )
		and isnull(p.Motive,'')<>'2-Descontinuado'
		and isnull(ManufacturerId,'')<>990
	GROUP BY RS.DATE, p.DivisionUnificadaDesc
	order by p.DivisionUnificadaDesc, date desc
  
   
END
