﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [reports].[OLowMarketShare]

AS
BEGIN

--------------------------------------------------------------Reporte Oportundades Bajo MSH------------------------------------------------------------------------------
declare @weeks1 int = 4, @weeks2 int = 8, @weeks3 int = 12
	declare @maxDateF smalldatetime
	declare @MinDate1wF smalldatetime
	declare @MaxDate1wF smalldatetime
	declare @MinDate2wF smalldatetime
	declare @MaxDate2wF smalldatetime
	declare @MinDate3wF smalldatetime
	declare @MaxDate3wF smalldatetime
	declare @maxDateC smalldatetime
	declare @MinDate1wC smalldatetime
	declare @MaxDate1wC smalldatetime
	declare @MinDate2wC smalldatetime
	declare @MaxDate2wC smalldatetime
	declare @MinDate3wC smalldatetime
	declare @MaxDate3wC smalldatetime

	/*** FARMA ***/
	select @maxDateF = max(date) from series.IMS
	select @MinDate1wF = DATEADD(week, -@weeks1+1, @maxDateF)
	select @MinDate2wF = DATEADD(week, -@weeks2+1, @maxDateF)
	select @MinDate3wF = DATEADD(week, -@weeks3+1, @maxDateF)
	select @MaxDate1wF = DATEADD(day, 6, @maxDateF)
	select @MaxDate2wF = DATEADD(day, 6, @maxDateF)
	select @MaxDate3wF = DATEADD(day, 6, @maxDateF)

	select @maxDateC = max(date) from series.Nielsen
	select @MinDate1wC = DATEADD(week, -@weeks1+1, @maxDateC)
	select @MinDate2wC = DATEADD(week, -@weeks2+1, @maxDateC)
	select @MinDate3wC = DATEADD(week, -@weeks3+1, @maxDateC)
	select @MaxDate1wC = DATEADD(day, 6, @maxDateC)
	select @MaxDate2wC = DATEADD(day, 6, @maxDateC)
	select @MaxDate3wC = DATEADD(day, 6, @maxDateC)

	SELECT * 
	, case when [MS<20% 4SEM] is not NULL then 1 else 0 end [MS4]
	, case when [MS<20% 8SEM] is not NULL then 1 else 0 end [MS8]
	, case when [MS<20% 12SEM] is not NULL then 1 else 0 end [MS12]
	, case when [$SB>$Comp4SEM] is not NULL then 1 else 0 end [>Comp4SEM]
	, case when [$SB>$Comp8SEM] is not NULL then 1 else 0 end [>Comp8SEM]
	, case when [$SB>$Comp12SEM] is not NULL then 1 else 0 end [>Comp12SEM]
	, case when [Vta Perdida 4SEM]<cast(0.04 as float) then cast(1 as varchar) else cast(0 as varchar) end [Vta.Perd<4% 4SEM?]
	, case when [Vta Perdida 8SEM]<cast(0.04 as float) then cast(1 as varchar) else cast(0 as varchar) end [Vta.Perd<8% 4SEM?]
	, case when [Vta Perdida 12SEM]<cast(0.04 as float) then cast(1 as varchar) else cast(0 as varchar) end [Vta.Perd<12% 4SEM?]
	FROM (SELECT * 
	, CONCAT(SKU,'-',Descripción2) Descripción
	, case when PrecioSB4Sem>PrecioComp4Sem then PrecioSB4Sem-PrecioComp4Sem else NULL end [$SB>$Comp4SEM]
	, case when PrecioSB8Sem>PrecioComp8Sem then PrecioSB8Sem-PrecioComp8Sem else NULL end [$SB>$Comp8SEM]
	, case when PrecioSB12Sem>PrecioComp12Sem then PrecioSB12Sem-PrecioComp12Sem else NULL end [$SB>$Comp12SEM]
	, case when MshVentas4Sem<cast(0.2 as float) then MshVentas4Sem else NULL end [MS<20% 4SEM]
	, case when MshVentas8Sem<cast(0.2 as float) then MshVentas8Sem else NULL end [MS<20% 8SEM]
	, case when MshVentas12Sem<cast(0.2 as float) then MshVentas12Sem else NULL end [MS<20% 12SEM]
	, case when VentaPerdida4Sem+VentasSB4Sem =0 then NULL else VentaPerdida4Sem/cast(VentaPerdida4Sem+VentasSB4Sem as float) end [Vta Perdida 4SEM]
	, case when VentaPerdida8Sem+VentasSB8Sem =0 then NULL else VentaPerdida8Sem/cast(VentaPerdida8Sem+VentasSB8Sem as float) end [Vta Perdida 8SEM]
	, case when VentaPerdida12Sem+VentasSB12Sem =0 then NULL else VentaPerdida12Sem/cast(VentaPerdida12Sem+VentasSB12Sem as float) end [Vta Perdida 12SEM]
	FROM (SELECT p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc
		, p.Positioning_Desc [Posicionamiento/Clase], p.ManufacturerDesc Laboratorio, CategoryDesc Categoría
		, m1.SKU, p.Name Descripción2
		, MshVentas4Sem, UnidadesSB4Sem, UnidadesPerdidas4Sem, VentasSB4Sem, VentaPerdida4Sem, PrecioSB4Sem, PrecioComp4Sem,Stock4Sem
		, MshVentas8Sem, UnidadesSB8Sem, UnidadesPerdidas8Sem, VentasSB8Sem, VentaPerdida8Sem, PrecioSB8Sem, PrecioComp8Sem,Stock8Sem
		, MshVentas12Sem, UnidadesSB12Sem, UnidadesPerdidas12Sem, VentasSB12Sem, VentaPerdida12Sem, PrecioSB12Sem, PrecioComp12Sem,Stock12Sem
	FROM
	(
		select SKU, SUM(UnidadesSB)UnidadesSB4Sem, SUM(VentasSB)VentasSB4Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades4Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas4Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB4Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp4Sem
		from series.IMS
		where date >= @MinDate1wF
		group by SKU
		HAVING SUM(UnidadesCadenas) >0
	) m1
	INNER JOIN (
		select SKU, SUM(UnidadesSB)UnidadesSB8Sem, SUM(VentasSB)VentasSB8Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades8Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas8Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB8Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp8Sem
		from series.IMS
		where date >= @MinDate2wF
		group by SKU
	) m2 ON m2.SKU=m1.SKU
	INNER JOIN(
		select SKU, SUM(UnidadesSB)UnidadesSB12Sem, SUM(VentasSB)VentasSB12Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades12Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas12Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB12Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp12Sem
		from series.IMS
		where date >= @MinDate3wF
		group by SKU
	) m3 ON m3.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas4Sem, SUM(VtaPerdidaVal) VentaPerdida4Sem, sum(Stock) Stock4Sem
		from reports.LostSaleBySKU
		where date between @MinDate1wF and @MaxDate1wF
		group by SKU
	) v1 on v1.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas8Sem, SUM(VtaPerdidaVal) VentaPerdida8Sem, sum(Stock) Stock8Sem
		from reports.lostSalebySKU
		where date between @MinDate2wF and  @MaxDate2wF
		group by SKU
	) v2 on v2.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas12Sem, SUM(VtaPerdidaVal) VentaPerdida12Sem, sum(Stock) Stock12Sem
		from reports.LostSaleBySKU
		where date between @MinDate3wF and  @MaxDate3wF
		group by SKU
	) v3 on v3.SKU=m1.SKU
	INNER JOIN products.Products p on p.SKU=m1.SKU
	WHERE p.Division=1

	/*** CONSUMO MASIVO ***/
	UNION ALL

	SELECT p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc
		, p.Class_Desc, p.ManufacturerDesc Laboratorio, CategoryDesc Categoría, m1.SKU, p.Name Descripción
		, MshVentas4Sem, UnidadesSB4Sem, UnidadesPerdidas4Sem, VentasSB4Sem, VentaPerdida4Sem, PrecioSB4Sem, PrecioComp4Sem,Stock4Sem
		, MshVentas8Sem, UnidadesSB8Sem, UnidadesPerdidas8Sem, VentasSB8Sem, VentaPerdida8Sem, PrecioSB8Sem, PrecioComp8Sem,Stock8Sem
		, MshVentas12Sem, UnidadesSB12Sem, UnidadesPerdidas12Sem, VentasSB12Sem, VentaPerdida12Sem, PrecioSB12Sem, PrecioComp12Sem,Stock12Sem
	FROM
	(
		select SKU, SUM(UnidadesSB)UnidadesSB4Sem, SUM(VentasSB)VentasSB4Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades4Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas4Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB4Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp4Sem
		from series.Nielsen
		where date >= @MinDate1wC
		group by SKU
		HAVING SUM(UnidadesCadenas) >0
	) m1
	INNER JOIN (
		select SKU, SUM(UnidadesSB)UnidadesSB8Sem, SUM(VentasSB)VentasSB8Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades8Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas8Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB8Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp8Sem
		from series.Nielsen
		where date >= @MinDate2wC
		group by SKU
	) m2 ON m2.SKU=m1.SKU
	INNER JOIN(
		select SKU, SUM(UnidadesSB)UnidadesSB12Sem, SUM(VentasSB)VentasSB12Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades12Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas12Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB12Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp12Sem
		from series.Nielsen
		where date >= @MinDate3wC
		group by SKU
	) m3 ON m3.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas4Sem, SUM(VtaPerdidaVal) VentaPerdida4Sem, sum(Stock) Stock4Sem
		from reports.LostSaleBySKU
		where date between @MinDate1wC and @MaxDate1wC
		group by SKU
	) v1 on v1.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas8Sem, SUM(VtaPerdidaVal) VentaPerdida8Sem, sum(Stock) Stock8Sem
		from reports.lostSalebySKU
		where date between @MinDate2wC and  @MaxDate2wC
		group by SKU
	) v2 on v2.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(VtaPerdida) UnidadesPerdidas12Sem, SUM(VtaPerdidaVal) VentaPerdida12Sem, sum(Stock) Stock12Sem
		from reports.LostSaleBySKU
		where date between @MinDate3wC and  @MaxDate3wC
		group by SKU
	) v3 on v3.SKU=m1.SKU
	INNER JOIN products.Products p on p.SKU=m1.SKU
	WHERE p.Division=2
	and p.SKU not in (select sku from 
					(
					select p.Description, a.*
					 from (select * 
					, ABS([PrecioSB (data)]-[PrecioSB (nielsen)])/ cast([PrecioSB (data)] as float) [%DIF] 
					from (select --cast(Date as date) Fecha,
					s.SKU
					, SUM(VentasSB) [VentasSB (nielsen)]
					, SUM(UnidadesSB) [UnidadesSB (nielsen)]

					--, SUM(VentasCadenas)-SUM(VentasSB) [VentasCompetidores (nielsen)]
					--, SUM(UnidadesCadenas)-SUM(UnidadesSB) [UnidadesSB (nielsen)]

					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end [PrecioSB (nielsen)]
					, SUM(Income) [VentasSBData (data)]
					, SUM(Quantity) [UnidadesSB (data)]
					, case when SUM(Quantity) >0 then SUM(Income)/SUM(Quantity) else 0 end [PrecioSB (data)]
					from series.Nielsen s
					left join forecast.WeeklySales ws on s.sku=ws.sku and s.date=ws.Date
					where --s--.sku = 4737130 and 
					s.date >= @MinDate1wC and ws.SKU is not null and ws.Date is not null 
					group by s.sku)a
					where [VentasSBData (data)] is not null)a
					LEFT JOIN products.fullproducts p on a.sku=p.SKU
					where [%DIF] >cast(0.5 as float)
					and [UnidadesSB (nielsen)]<=500
					)a)

	)A)B

END

