﻿-- =============================================
-- Author:		Diego Oyarzún
-- Create date: 2010-03-29
-- Description:	Llena la tabla de reporte CD
-- =============================================
CREATE PROCEDURE [reports].[ExecReportCD] 
	@today smalldatetime = NULL
AS

BEGIN

---------------------------------------------------------
---- Actualizaciones con información cargada de HOY. ----
---------------------------------------------------------
	
	-- Borra las filas con la misma fecha en caso que exista
	delete from [reports].[ReportCD] where date = @today

	-- Llena la tabla
	INSERT INTO [reports].[ReportCD]
           ([Date]
           ,[SKU]
           ,[ZeroStockCD]
           ,[StockCD]
		   ,[MixActive])
	SELECT @today as Date
			,i.SKU
			,case when stockcd <= 0 then 1 else 0 end as ZeroStockCD
			,StockCD
			,0
		FROM [series].[StockCD] i 
		WHERE i.Date = @today -- Filtro de fecha
		AND i.SKU in -- Filtro para productos no hijos activos en la cadena
		(
			select distinct s.sku from [series].[LastStock] s 
			where s.suggest > 0	and s.sku not in (select sku_child from [products].[ProductsEquivalence])
		)
	
	UPDATE [reports].[ReportCD]
	SET MotiveId = p.MotiveId FROM [reports].[ReportCD] cd inner join [products].[FullProducts] p on p.SKU = cd.SKU 
	where [Date] = @today

	UPDATE cd SET MixActive=1
	FROM [reports].ReportCD cd INNER JOIN operation.ServicesProductsDetail s on s.SKU=cd.SKU
	WHERE cd.[Date]=@today and s.ServiceId=4
	
	
---------------------------------------------------------
---- Actualizaciones con información cargada de AYER ----
---------------------------------------------------------
	
declare @yesterday smalldatetime = dateadd(day,-1,@today)
		
	UPDATE rcd 
	SET [Order] = ISNULL(s.[Order],0), Dispatch = ISNULL(s.Dispatch,0)
	FROM (
		SELECT * 
		FROM reports.ReportCD 
		WHERE [Date] = (SELECT MAX(Date) FROM  [series].[SalesCD] WHERE [Date] <= @yesterday) 
	) rcd 
	LEFT OUTER JOIN [series].[SalesCD] s on s.SKU = rcd.SKU and s.[Date] = rcd.[Date]
	
	UPDATE cd 
	SET [Cost] = ISNULL(c.Cost,0)
	FROM [reports].[ReportCD] cd 
	left join (
			SELECT distinct pc.SKU, pc.Cost FROM [series].[PriceAndCost] pc 
			INNER JOIN (SELECT SKU, MAX(Date) as Date FROM [series].[PriceAndCost] WHERE Date <= @today GROUP BY SKU) d
			ON pc.SKU = d.SKU and pc.Date = d.Date
		) c ON cd.SKU = c.SKU
	where cd.Date =@yesterday
	
END
