﻿
-- =============================================
-- Author:		Constanza Calderón
-- Create date: 2014-09-11
-- Description:	<Description,,>
-- Modificación: 2016-08-04 Daniela Albornoz
-- Modificación: 2016-08-11 Daniela Albornoz
-- =============================================
CREATE PROCEDURE [reports].[ExecDailyReportStores]
	-- Add the parameters for the stored procedure here
	@date date = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @date is null
	BEGIN
		SET @date = (select max(date) from reports.ReportStores)
	END 
	
	
	delete from  [reports].[DailyReportStores] where Fecha <= dateadd(day, -7, @date)
	delete from  [reports].[DailyReportStores]  where Fecha = @date

	insert into [reports].[DailyReportStores]
	select dsm.SKU, dsm.Store, @date, ls.Stock, ls.Suggest, ls.Transit,dateadd(day,-50, @date),0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0, 0
	from  aux.DailySuggestMix dsm
	inner join series.LastStock ls on ls.SKU=dsm.SKU and ls.Store=dsm.Store
	where Stock<= 0

	update r set UltimaVenta = mdate
	from reports.DailyReportStores r
	inner join (	
		select sku, store, max(date) mdate
		from series.Sales
		where [date] >= dateadd(DAY, -50, @date)
		group by SKU, Store
	) s on s.sku=r.sku and s.Store=r.store
	where r.Fecha=@date

	update [reports].[DailyReportStores] set fallaProveedorAnt = 1 
	where Fecha = @date
			and sku in ( select distinct sku from reports.ReportStores
					where [Date] between dateadd(day,-4-2*7,@date) and dateadd(day,-4,@date) and MotiveId in (90,3))

	update [reports].[DailyReportStores] set fallaActual = 1 
	where Fecha = @date
			and  sku in (select sku from products.Products where MotiveId in (90,3))

	update [reports].[DailyReportStores] set promocion = 1
	where Fecha = @date
			and sku in (SELECT distinct ProductId FROM [Salcobrand].[promotion].[PromotionDetail] where @date between BeginDate and EndDate)

	update t set t.stockcd = s.stockCD,t.cobertura = s.cobertura,t.faltante = s.faltante,
			t.stockTotal = s.stock,t.sugeridoTotal = s.suggest,t.nquiebre = s.quiebre,t.locales = s.Locales,
			t.StockTotalAjustado = s.stockAjustado	
	from [reports].[DailyReportStores] t 
	inner join 	(
		select l.*,isnull(cd.stockCD,0) stockCD 
		from (
			select sku
				,sum(case when stock > 0 then stock else 0 end) stock
				,sum(case when stock > 0 then [function].inlinemin(stock,suggest) else 0 end) stockAjustado
				,sum(suggest) suggest
				,sum(case when stock>suggest then 0 else suggest-stock end) faltante
				,sum(case when suggest>0 then 1 else 0 end) cobertura
				,sum(case when suggest>0 and stock<=0 then 1 else 0 end) quiebre
				,sum(case when suggest>0 then 1 else 0 end) Locales 
			from series.LastStock
			group by sku) l 
		left outer join (
			select sku,avg(stockcd) stockCD from series.StockCD 
			where  date between dateadd(day,-4-2*7,@date) and dateadd(day,-4,@date)
			group by sku ) cd on l.sku = cd.SKU
	) s on s.SKU = t.sku
	where t.fecha = @date


	update t set Venta=a.Venta, Dias=a.Dias, DiasSinQuiebre=a.DiasSinQuiebre
	from [reports].[DailyReportStores] t 
	inner join (
		select drs.SKU, drs.Store
			, Venta=sum(s.quantity)
			, Dias=count(*)
			, DiasSinQuiebre=sum(case when s.stock>0 then 1 else 0 end)
		from [reports].[DailyReportStores] drs
		inner join reports.ReportStores s on s.SKU=drs.SKU and s.Store=drs.Store
		where Quantity is not null
			and s.[date]>= dateadd(DAY, -50, @date)
			and drs.Fecha = @date
		group by drs.SKU, drs.Store
		having sum(case when s.Stock>0 then 1 else 0 end) > 0
	) a on a.SKU=t.SKU and a.store=t.Store
	where t.Fecha = @date

	declare @auxDate date = dateadd(day, -1, @date)
	update t set VentaPerdida=a.LostSale, VentaPerdidaVal=a.LostSaleVal
	from [reports].[DailyReportStores] t 
	inner join (
		select SKU, Store, LostSale, LostSale*SPrice LostSaleVal
		from reports.ReportStores
		where [Date]=@auxDate
	) a on a.SKU=t.SKU and a.store=t.Store
	where t.Fecha = @auxDate


END

