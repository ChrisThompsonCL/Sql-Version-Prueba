﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-08-25
-- Description:	Actualiza información del picking del día
-- =============================================
CREATE PROCEDURE [reports].[ExecPickingReport]
	-- Add the parameters for the stored procedure here	
	@date date 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @auxdate date = @date

	--chequea no repetir datos para un día
	DELETE FROM [reports].[PickingReport] WHERE [Date]=@auxdate

	--inserta los datos del día indicado
	insert into [reports].[PickingReport]
	select rs.[Date], rs.SKU, fp.Division, fp.LogisticAreaId, fp.Motive, fp.TotalRanking
		, COUNT(*) Combinaciones
		, SUM(case when r.Store is not null then 1 else 0 end) CombinacionesInformadas
		, SUM(case when HasToPickUp=1 then 1 else 0 end) CombinacionesDebenPickear
		, SUM(case when HasToPickUp=1 and PickedUnits>0 then 1 else 0 end) CombinacionesPickeadasCorrecto
		, SUM(case when PickedUnits>0 then 1 else 0 end) CombinacionesPickeadasTotal
		, SUM(case when HasToPickUp=1 then [Max]-(rs.Stock+Transit) else 0 end) UnidadesDebenPickear
		, SUM(case when HasToPickUp=1 then PickedUnits else 0 end) UnidadesPickeadasCorrecto
		, SUM(case when HasToPickUp=1 then [function].InlineMin(PickedUnits, [Max]-(rs.Stock+Transit)) else 0 end) UnidadesPickeadasCorrectoCorregido
		, SUM(case when PickedUnits>0 then PickedUnits else 0 end) UnidadesPickeadasTotal
		, SUM(case when r.Store is not null and rs.Stock=0 then 1 else 0 end) Quiebre
		, SUM(case when r.Store is not null and rs.VE > 0 then Quantity else 0 end) VtaUN
		, SUM(case when r.Store is not null and rs.VE > 0 then Income else 0 end) Vta$
		, SUM(case when r.Store is not null then LostSale else 0 end) VtaPerdidaUN
		, SUM(case when r.Store is not null then LostSale*SPrice else 0 end) VtaPerdida$
	from reports.ReportStores rs
	inner join products.FullProducts fp on fp.SKU=rs.SKU
	left join (select distinct Store, LogisticAreaId from minmax.Replenishment where [days]>0) r on r.Store=rs.Store and fp.LogisticAreaId=r.LogisticAreaId
	where rs.[Date] = @auxdate
	group by rs.[Date], rs.SKU, fp.LogisticAreaId, fp.Motive, fp.TotalRanking, fp.division


END
