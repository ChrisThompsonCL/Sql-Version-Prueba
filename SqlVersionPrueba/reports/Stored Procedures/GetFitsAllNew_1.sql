﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-06-28
-- Description:	obtiene pronósticos del último periodo
-- =============================================
CREATE PROCEDURE [reports].[GetFitsAllNew]
	-- Add the parameters for the stored procedure here
	@date date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--declare @datekey date = (select Max(DateKey) from forecast.predicteddailysales)

	select CONVERT(VARCHAR(10), fac.[date], 126) Fecha, fac.SKU, fac.Store [Local], REPLACE(cast(cast(Fit as decimal(30,5)) as nvarchar(max)),',','-') Fit
	from minmax.FitsAllNew fac
	inner join aux.DailySuggestMix dsm on dsm.SKU=fac.SKU and dsm.Store=fac.Store
	where round(Fit,5)>0 
		and fac.[Date]=@date

END
