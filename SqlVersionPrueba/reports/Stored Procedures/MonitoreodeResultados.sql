﻿-- =============================================
-- Author:		<Fairenes Zerpa>
-- Create date: <03-10-2017>
-- Description:	<Reporte Monitoreo de Resultados>
-- =============================================
CREATE PROCEDURE [reports].[MonitoreodeResultados]

AS
BEGIN
	declare @FIN smalldatetime= CONVERT(date,dbo.GetMonday(getdate()))
declare @INICIO smalldatetime= DATEADD(week,-115,@FIN) 

--DROP TABLE #TablaBase 
CREATE TABLE #TablaBase (Semana date
							, SKU int)

INSERT INTO #TablaBase
SELECT *
FROM (	select distinct dbo.GetMonday(met.[Date]) Semana
		from salcobrand.reports.MetricasStocks met
		where [Date] > @INICIO
		) a
		CROSS JOIN (select SKU
					from Salcobrand.products.Fullproducts
					where IsActive = 1 and Class <> 196 and Subdivision <> 15 and FirstSale is not null and lastsale > DATEADD(Month,-6,@FIN) and division in (1,2)
					) pro 

---------------------------------------------------------------------------------------------------------------------------------------------------------------

--DROP TABLE #Inventario 
CREATE TABLE #Inventario (Semana date
							, SKU int
							, Sugerido float
							, SugeridoModelo float
							, StockAjustadoSugerido float
							, Locales float
							, LocalesQuebrados float
							, LocalesQuebradosConStockCD float
							, LocalesQuebradosSinFallaProveedor float
							, SugeridoProm float
							, StockAjustadoSugeridoProm float)
INSERT INTO #Inventario
SELECT	dbo.GetMonday(met.[Date]) Semana
		, met.SKU
		, AVG(Sugerido)Sugerido
		, AVG(SugeridoModelo) SugeridoModelo
		, AVG(StockAjustadoSugerido) StockAjustadoSugerido
		, AVG(Locales) Locales
		, AVG(LocalesQuebrados) LocalesQuebrados
		, AVG(LocalesQuebradosConStockCD)LocalesQuebradosConStockCD
		, AVG(LocalesQuebradosSinFallaProveedor) LocalesQuebradosSinFallaProveedor
		, CAST(AVG(Sugerido) as float)/AVG(Locales) SugeridoProm
		, CAST(AVG(StockAjustadoSugerido) as float)/AVG(Locales) StockAjustadoSugeridoProm
		from salcobrand.reports.MetricasStocks met
		where [Date] > @INICIO 
		group by dbo.GetMonday(met.[Date]), met.SKU


----------------------------------------------------------------------------------------------------------------------------------------
/* cambios de precios para Shampoo y Acondicionadores de Organix
Salcobrand lo ejecutará a partir del 1 de marzo la baja de Pvp*/

--DROP TABLE #Shampoo
CREATE TABLE #Shampoo (SKU int)
INSERT INTO #Shampoo

select SKU
from Salcobrand.products.FullProducts
where SKU in (4101684,4101671,4101602,4109101,4101432,4101752,4101422,4101442,4101694,4101652,4101612,4101692,4101751,4101683,4101670,4101601,4109100,4101431,4101421
		,4101441,4101693,4101651,4101611,4106914,4109011,4106900,4109031,4109001,4109002,4109030)

-------------------------------------------------------------- CM--------------------------------------------------------------------------


Select @INICIO,@FIN

select   v.[Date] Semana, p.Division_Desc División, p.CategoryDesc, ct.Name ClassName
		, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, p.ClaseUnificadaDesc, p.[Description] Name, p.SKU
		, case when pro.sku is not null then 'Servicio' else 'NoServicio' end Servicio
		, case when sugerenciagenericos1.SKU is not null then 1 else 0 end CambioPreciosGenericos1
		, case when sugerenciagenericos2.SKU is not null then 1 else 0 end CambioPreciosGenericos2
		, case when shampoo.SKU is not null then 1 else 0 end CambioPreciosShampoo
		, v.income Ingreso
		, v.Quantity Volumen
		, v.Income - (v.Quantity*pc.Cost) Contribucion$
		, pc.Cost Costo
		, pc.Price PrecioLista
		, pc.Cost * v.Quantity CostoPond
		, pc.Price * v.Quantity PrecioListaPond
		, case when p.Division = 1 then
				(case when IMS.UnidadesSB is not null then IMS.UnidadesSB else 0 end)
					else (case when Nielsen.UnidadesSB is not null then Nielsen.UnidadesSB else 0 end) end UnidadesSB
		, case when p.Division = 1 then
				(case when IMS.VentasSB is not null then IMS.VentasSB else 0 end)
					else (case when nielsen.VentasSB is not null then nielsen.VentasSB else 0 end) end VentasSB
		, case when p.Division = 1 then
				(case when IMS.UnidadesSB>0 then IMS.VentasSB/IMS.UnidadesSB else 0 end)
					else (case when nielsen.UnidadesSB>0 then nielsen.VentasSB/nielsen.UnidadesSB else 0 end) end PrecioEfectivoSBIMS

		, case when p.Division = 1 then
				(case when IMS.UnidadesCadenas is not null then IMS.UnidadesCadenas else 0 end)
					else (case when nielsen.UnidadesCadenas is not null then nielsen.UnidadesCadenas else 0 end) end UnidadesCadenas
		, case when p.Division = 1 then
				(case when IMS.VentasCadenas is not null then IMS.VentasCadenas else 0 end)
					else (case when nielsen.VentasCadenas is not null then nielsen.VentasCadenas else 0 end) end VentasCadenas
		, case when p.division = 1 then
				(case when IMS.UnidadesCadenas > 0 then IMS.VentasCadenas/IMS.UnidadesCadenas else 0 end)
					else (case when nielsen.UnidadesCadenas > 0 then nielsen.VentasCadenas/nielsen.UnidadesCadenas else 0 end) end PrecioEfectivoCadena
		
		, inv.Sugerido
		, inv.SugeridoModelo
		, inv.StockAjustadoSugeridoProm
		, inv.SugeridoProm
		, inv.Locales
		, inv.LocalesQuebrados
		, inv.LocalesQuebradosConStockCD
		, inv.LocalesQuebradosSinFallaProveedor

FROM #TablaBase tb

LEFT JOIN #Inventario inv on inv.SKU = tb.SKU and inv.Semana = tb.Semana
		
INNER JOIN (select dbo.GetMonday([date]) [Date], SKU, sum(Quantity) Quantity, sum(Income) Income
			from Salcobrand.series.DailySales
			where [date] > @INICIO
			group by dbo.GetMonday ([date]), SKU
			) v on v.sku = tb.sku and v.[Date] = tb.Semana

INNER JOIN [Salcobrand].[products].[FullProducts] p on p.sku=tb.SKU
LEFT JOIN  [Salcobrand].[products].[Products] pro on pro.sku=tb.SKU
LEFT JOIN class.ProductClassificationFinal c ON tb.SKU = c.SKU
LEFT JOIN class.ClassificationType ct on ct.Id=c.ClassificationTypeId
INNER JOIN (SELECT dbo.Getmonday([date]) Semana, SKU, AVG(Price) Price, AVG(Cost) Cost
			FROM series.priceandcost
			GROUP BY dbo.getmonday([date]), SKU
			) pc on pc.SKU=tb.SKU and pc.Semana=tb.Semana

LEFT JOIN Salcobrand.series.IMS IMS on IMS.[Date] = tb.Semana and IMS.sku = tb.sku
LEFT JOIN Salcobrand.series.Nielsen nielsen on nielsen.[Date] = tb.Semana and nielsen.sku = tb.sku
LEFT JOIN (select sku from pricing.LoadedScenario where LoadedDate = '2017-01-11') sugerenciagenericos1 on sugerenciagenericos1.SKU = tb.SKU
LEFT JOIN (select sku from pricing.LoadedScenario where LoadedDate = '2017-03-20') sugerenciagenericos2 on sugerenciagenericos2.SKU = tb.SKU
LEFT JOIN #Shampoo shampoo on shampoo.SKU = tb.SKU

where v.[Date] > @INICIO and v.Income <> 0 and v.Quantity <> 0 and p.DivisionUnificadaId = 2 --CAMBIAR ID DE DIVISIÓN :1 FARMA Y 2 CM
order by dbo.getmonday(v.[Date]), v.SKU


----------------------------------------------------------------- Farma---------------------------------------------------------------------

Select @INICIO,@FIN

select   v.[Date] Semana, p.Division_Desc División, p.CategoryDesc, ct.Name ClassName
		, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, p.ClaseUnificadaDesc, p.[Description] Name, p.SKU
		, case when pro.sku is not null then 'Servicio' else 'NoServicio' end Servicio
		, case when sugerenciagenericos1.SKU is not null then 1 else 0 end CambioPreciosGenericos1
		, case when sugerenciagenericos2.SKU is not null then 1 else 0 end CambioPreciosGenericos2
		, case when shampoo.SKU is not null then 1 else 0 end CambioPreciosShampoo
		, v.income Ingreso
		, v.Quantity Volumen
		, v.Income - (v.Quantity*pc.Cost) Contribucion$
		, pc.Cost Costo
		, pc.Price PrecioLista
		, pc.Cost * v.Quantity CostoPond
		, pc.Price * v.Quantity PrecioListaPond
		, case when p.Division = 1 then
				(case when IMS.UnidadesSB is not null then IMS.UnidadesSB else 0 end)
					else (case when Nielsen.UnidadesSB is not null then Nielsen.UnidadesSB else 0 end) end UnidadesSB
		, case when p.Division = 1 then
				(case when IMS.VentasSB is not null then IMS.VentasSB else 0 end)
					else (case when nielsen.VentasSB is not null then nielsen.VentasSB else 0 end) end VentasSB
		, case when p.Division = 1 then
				(case when IMS.UnidadesSB>0 then IMS.VentasSB/IMS.UnidadesSB else 0 end)
					else (case when nielsen.UnidadesSB>0 then nielsen.VentasSB/nielsen.UnidadesSB else 0 end) end PrecioEfectivoSBIMS

		, case when p.Division = 1 then
				(case when IMS.UnidadesCadenas is not null then IMS.UnidadesCadenas else 0 end)
					else (case when nielsen.UnidadesCadenas is not null then nielsen.UnidadesCadenas else 0 end) end UnidadesCadenas
		, case when p.Division = 1 then
				(case when IMS.VentasCadenas is not null then IMS.VentasCadenas else 0 end)
					else (case when nielsen.VentasCadenas is not null then nielsen.VentasCadenas else 0 end) end VentasCadenas
		, case when p.division = 1 then
				(case when IMS.UnidadesCadenas > 0 then IMS.VentasCadenas/IMS.UnidadesCadenas else 0 end)
					else (case when nielsen.UnidadesCadenas > 0 then nielsen.VentasCadenas/nielsen.UnidadesCadenas else 0 end) end PrecioEfectivoCadena
		
		, inv.Sugerido
		, inv.SugeridoModelo
		, inv.StockAjustadoSugeridoProm
		, inv.SugeridoProm
		, inv.Locales
		, inv.LocalesQuebrados
		, inv.LocalesQuebradosConStockCD
		, inv.LocalesQuebradosSinFallaProveedor

FROM #TablaBase tb

LEFT JOIN #Inventario inv on inv.SKU = tb.SKU and inv.Semana = tb.Semana
		
INNER JOIN (select dbo.GetMonday([date]) [Date], SKU, sum(Quantity) Quantity, sum(Income) Income
			from Salcobrand.series.DailySales
			where [date] > @INICIO
			group by dbo.GetMonday ([date]), SKU
			) v on v.sku = tb.sku and v.[Date] = tb.Semana

INNER JOIN [Salcobrand].[products].[FullProducts] p on p.sku=tb.SKU
LEFT JOIN  [Salcobrand].[products].[Products] pro on pro.sku=tb.SKU
LEFT JOIN class.ProductClassificationFinal c ON tb.SKU = c.SKU
LEFT JOIN class.ClassificationType ct on ct.Id=c.ClassificationTypeId
INNER JOIN (SELECT dbo.Getmonday([date]) Semana, SKU, AVG(Price) Price, AVG(Cost) Cost
			FROM series.priceandcost
			GROUP BY dbo.getmonday([date]), SKU
			) pc on pc.SKU=tb.SKU and pc.Semana=tb.Semana

LEFT JOIN Salcobrand.series.IMS IMS on IMS.[Date] = tb.Semana and IMS.sku = tb.sku
LEFT JOIN Salcobrand.series.Nielsen nielsen on nielsen.[Date] = tb.Semana and nielsen.sku = tb.sku
LEFT JOIN (select sku from pricing.LoadedScenario where LoadedDate = '2017-01-11') sugerenciagenericos1 on sugerenciagenericos1.SKU = tb.SKU
LEFT JOIN (select sku from pricing.LoadedScenario where LoadedDate = '2017-03-20') sugerenciagenericos2 on sugerenciagenericos2.SKU = tb.SKU
LEFT JOIN #Shampoo shampoo on shampoo.SKU = tb.SKU

where v.[Date] > @INICIO and v.Income <> 0 and v.Quantity <> 0 and p.DivisionUnificadaId = 1 --CAMBIAR ID DE DIVISIÓN :1 FARMA Y 2 CM
order by dbo.getmonday(v.[Date]), v.SKU

END
