﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [reports].[ODecreaseMarketShare]

AS
BEGIN

--------Reporte de Oportunidades Disminución MSH---------------------
DECLARE @RC int
	DECLARE @weeks int = 4
	DECLARE @totalweeks int = 24
	DECLARE @compareweeks int = 4
	DECLARE @x float = 0.9
	DECLARE @sens float = 0.01
	declare @maxDateF smalldatetime
	declare @maxDateC smalldatetime
	declare @MinDate1wC smalldatetime

	/*** FARMA ***/
	select @maxDateF = max(date) from series.IMS
	select @maxDateC = max(date) from series.Nielsen
	select @MinDate1wC = DATEADD(week, -@weeks+1, @maxDateC)

	-- busca evolución del MSh para los productos con alerta
	select p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc
		, p.Positioning_Desc, p.ManufacturerDesc Laboratorio, p.CategoryDesc Categoría, [Date],  p.SKU, p.Name
		, SUM(UnidadesSB)UnidadesSB, SUM(VentasSB)VentasSB
		, case when SUM(UnidadesCadenas)>0 then SUM(UnidadesSB)/case when SUM(UnidadesCadenas)=0 then NULL else SUM(UnidadesCadenas) end else 0 end MshUnidades
		, case when SUM(VentasCadenas)>0 then SUM(VentasSB)/case when SUM(VentasCadenas)=0 then NULL else SUM(VentasCadenas) end else 0 end MshVentas
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end else 0 end PrecioSB
		, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB)=0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end else 0 end PrecioComp
		, case when SUM(UnidadesSB) >0 and SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end
		/case when (SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB) =0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end )=0 then NULL else (SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB) =0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end ) end end Ratio
		, sum(i)/case when sum(q)=0 then NULL else sum(q) end PrecioSBEfectivoData
	from series.IMS i
	inner join products.Products p on p.SKU=i.SKU
	LEFT JOIN (select DATENAME(WEEK,DATE)Semana, sku, sum(INcome)i, Sum(Quantity)q 
					from SERIES.SALES where DATE >= DATEADD(week, -@totalweeks+1, @maxDateF)
					GROUP BY sku, DATENAME(WEEK,DATE)
					) v on i.sku=v.sku and DATENAME(WEEK,i.date)=v.Semana
	where [Date] >= DATEADD(week, -@totalweeks+1, @maxDateF)
	AND i.SKU IN (
		--Casos con MshUnidades mas bajo
		select n.SKU
		from
		(
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesViejo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasViejo
			from series.IMS
			where date BETWEEN DATEADD(week, -@totalweeks+1, @maxDateF) AND DATEADD(week, -@totalweeks+@compareweeks, @maxDateF)
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) v
		inner join (
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesNuevo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasNuevo
			from series.IMS
			where date >= DATEADD(week, -@compareweeks+1, @maxDateF) 
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) n 
		on v.SKU=n.SKU
		where v.MshUnidadesViejo > n.MshUnidadesNuevo + @sens and n.MshUnidadesNuevo > 0
		and v.SKU in (
			--reconocer productos en los que el @x% de las últimas @weeks semanas ha sido el más barato
			SELECT SKU FROM (
				select SKU, [Date] 
					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
					, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
				from series.IMS
				where [Date] >= DATEADD(week, -@weeks+1, @maxDateF)
				GROUP BY SKU, [Date]
				HAVING SUM(UnidadesCadenas) >0
			) A
			GROUP BY SKU
			HAVING SUM(CASE WHEN PrecioSB<PrecioComp and PrecioSB>0 and PrecioComp>0 THEN 1 ELSE 0 END) > floor(@weeks*@x)
		)
	)
		and division=1
	GROUP BY p.SKU, p.Name, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc
		, p.Positioning_Desc, [Date], p.ManufacturerDesc, p.CategoryDesc
	HAVING SUM(UnidadesCadenas) >0 and
	case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB)=0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end else 0 end>=500
	and
	case when SUM(UnidadesSB) >0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end else 0 end>=500

	UNION ALL
	/*** CM ***/
	

	-- busca evolución del MSh para los productos con alerta
	select  p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc 
		, p.Class_Desc, p.ManufacturerDesc Laboratorio, p.CategoryDesc Categoría, [Date],  p.SKU, p.Name
		, SUM(UnidadesSB)UnidadesSB, SUM(VentasSB)VentasSB
		, case when SUM(UnidadesCadenas)>0 then SUM(UnidadesSB)/case when SUM(UnidadesCadenas)=0 then NULL else SUM(UnidadesCadenas) end else 0 end MshUnidades
		, case when SUM(VentasCadenas)>0 then SUM(VentasSB)/case when SUM(VentasCadenas)=0 then NULL else SUM(VentasCadenas) end else 0 end MshVentas
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end else 0 end PrecioSB
		, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB)=0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end else 0 end PrecioComp
		, case when SUM(UnidadesSB) >0 and SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end
		/case when (SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB) =0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end )=0 then NULL else (SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB) =0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end ) end end Ratio
		, sum(i)/case when sum(q)=0 then NULL else sum(q) end PrecioSBEfectivoData
	from series.Nielsen n
	LEFT JOIN (select DATENAME(WEEK,DATE)Semana, sku, sum(INcome)i, Sum(Quantity)q 
					from SERIES.SALES where DATE >= DATEADD(week, -@totalweeks+1, @maxDateC)
					GROUP BY sku, DATENAME(WEEK,DATE)
					) v on n.sku=v.sku and DATENAME(WEEK,n.date)=v.Semana
	inner join products.Products p on p.SKU=n.SKU
	where [Date] >= DATEADD(week, -@totalweeks+1, @maxDateC)
	AND n.SKU IN (
		--Casos con MshUnidades mas bajo
		select n.SKU
		from
		(
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesViejo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasViejo
			from series.Nielsen
			where date BETWEEN DATEADD(week, -@totalweeks+1, @maxDateC) AND DATEADD(week, -@totalweeks+@compareweeks, @maxDateC)
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) v
		inner join (
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesNuevo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasNuevo
			from series.Nielsen
			where date >= DATEADD(week, -@compareweeks+1, @maxDateC) 
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) n 
		on v.SKU=n.SKU
		where v.MshUnidadesViejo > n.MshUnidadesNuevo + @sens and n.MshUnidadesNuevo > 0
		and v.SKU in (
			--reconocer productos en los que el @x% de las últimas @weeks semanas ha sido el más barato
			SELECT SKU FROM (
				select SKU, [Date] 
					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
					, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
				from series.Nielsen
				where [Date] >= DATEADD(week, -@weeks+1, @maxDateC)
				GROUP BY SKU, [Date]
				HAVING SUM(UnidadesCadenas) >0
			) A
			GROUP BY SKU
			HAVING SUM(CASE WHEN PrecioSB<PrecioComp and PrecioSB>0 and PrecioComp>0 THEN 1 ELSE 0 END) > floor(@weeks*@x)
		)
	)
		and division=2
		and p.SKU not in (select sku from 
					(
					select p.Description, a.*
					 from (select * 
					, ABS([PrecioSB (data)]-[PrecioSB (nielsen)])/ cast([PrecioSB (data)] as float) [%DIF] 
					from (select --cast(Date as date) Fecha,
					s.SKU
					, SUM(VentasSB) [VentasSB (nielsen)]
					, SUM(UnidadesSB) [UnidadesSB (nielsen)]

					--, SUM(VentasCadenas)-SUM(VentasSB) [VentasCompetidores (nielsen)]
					--, SUM(UnidadesCadenas)-SUM(UnidadesSB) [UnidadesSB (nielsen)]

					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end [PrecioSB (nielsen)]
					, SUM(Income) [VentasSBData (data)]
					, SUM(Quantity) [UnidadesSB (data)]
					, case when SUM(Quantity) >0 then SUM(Income)/SUM(Quantity) else 0 end [PrecioSB (data)]
					from series.Nielsen s
					left join forecast.WeeklySales ws on s.sku=ws.sku and s.date=ws.Date
					where --s--.sku = 4737130 and 
					s.date >= @MinDate1wC and ws.SKU is not null and ws.Date is not null 
					group by s.sku)a
					where [VentasSBData (data)] is not null)a
					LEFT JOIN products.fullproducts p on a.sku=p.SKU
					where [%DIF] >cast(0.5 as float)
					and [UnidadesSB (nielsen)]<=500
					)a)
	GROUP BY p.SKU, p.Name, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc
	, p.Class_Desc, [Date], ManufacturerDesc, CategoryDesc
	HAVING SUM(UnidadesCadenas) >0 and
	case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/case when SUM(UnidadesCadenas-UnidadesSB)=0 then NULL else SUM(UnidadesCadenas-UnidadesSB) end else 0 end>=500
	and
	case when SUM(UnidadesSB) >0 then SUM(VentasSB)/case when SUM(UnidadesSB)=0 then NULL else SUM(UnidadesSB) end else 0 end>=500

END
