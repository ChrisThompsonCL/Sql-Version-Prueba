﻿


-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 14/04/2009
-- Description:	Intentara calcular combinaciones a excluir por poca venta siendo que no registran problemas de quiebre
-- Modificación: Daniela Albornoz (07/08/2013)
-- Modificación: Daniela Albornoz (14/05/2014)
-- Modificación: Daniela Albornoz (23/07/2015)
-- Modificación: Daniela Albornoz (22/07/2016)
-- =============================================
CREATE PROCEDURE [reports].[ExecReportExclusion] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN

Declare @firstDateP date, @firstDateS date,@endDate date
	,@3months date,@6months date,@12months date, @2months date, @4months date , @5months date
	,@today date

select @endDate = MAX([Date]) from series.Sales

set @3months = DATEADD(month,-3,@endDate)
set @6months = DATEADD(month,-6,@endDate)
set @12months = DATEADD(month,-12,@endDate)
set @2months = DATEADD(month,-2,@endDate)
set @4months = DATEADD(month,-4,@endDate)
set @5months = DATEADD(month,-5,@endDate)

set @firstDateP = DATEADD(month,-6,@endDate)
set @firstDateS = DATEADD(month,-18,@endDate)
select @today = cast(floor(cast(GETDATE() as float)) as smalldatetime)

create table #table (sku int,store int,sale int,pro_stockout float,share float,stock int,suggest int
	,planogram int,dun int, child int, minimum int, fixed int, maximum int, strictfix int
	,lastpromotion date,ranking nvarchar(1),motive nvarchar(255),division int,isgeneric bit,cost float,stockVal float, [3M] bit, [4M] bit, [5M] bit, [6M] bit, [12M] bit, Modelo bit
	, Primary Key(SKU, Store))
--CREATE INDEX Idx1 ON #table(sku)
--CREATE INDEX Idx2 ON #table(store)

--combinaciones sin venta en los últimos 2 meses
insert into #table (sku, store, sale, pro_stockout, share, stock, suggest, planogram, dun, child, minimum, fixed, maximum, strictfix, lastpromotion, ranking, motive, division, isgeneric, cost, stockVal, Modelo)
select				m.SKU,m.Store,0 sale,0 stockout,0 share,0 stock,0 suggest,0 planogram,0 dun,0 child, 0 minimum, 0 fixed, 0 maximum, 0 strictfix, null promotion,null ranking,null motive,null division,0 isgeneric,0.0 cost,0.0 stockVal, 0 Modelo
from (
	select sku, store from series.LastStock
	where sku in (select sku from products.Products where FirstSale <=@firstDateP and MotiveId  not in (9,10) and not (CategoryId=1 OR ManufacturerId in (18,92)))
		and store in (select Store from stores.Stores where FirstSale <=@firstDateS) 
		and SKU not in (select sku from products.fullproducts where petitoriominimo<>0)

		and Suggest>0
) m 
left outer join (
	select distinct SKU,Store from series.Sales where date between @2months and @endDate and Quantity>0
) s on s.SKU = m.SKU and s.Store = m.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales WHERE SKU=m.SKU and Store=m.Store and date between @2months and @endDate and Quantity>0)

-- sin venta en los ultimos 3 mesas
update t set [3M]=1
from #table t 
left outer join  (
	select distinct SKU,Store from series.Sales where date between @3months and @endDate and Quantity>0
) s on s.SKU = t.SKU and s.Store = t.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales where SKU=t.SKU and Store=t.Store and date between @3months and @endDate and Quantity>0)



-- sin venta en los ultimos 4 mesas
update t set [4M]=1
from #table t 
left outer join  (
	select distinct SKU,Store from series.Sales where date between @4months and @endDate and Quantity>0
) s on s.SKU = t.SKU and s.Store = t.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales where SKU=t.SKU and Store=t.Store and date between @4months and @endDate and Quantity>0)



-- sin venta en los ultimos 5 mesas
update t set [5M]=1
from #table t 
left outer join  (
	select distinct SKU,Store from series.Sales where date between @5months and @endDate and Quantity>0
) s on s.SKU = t.SKU and s.Store = t.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales where SKU=t.SKU and Store=t.Store and date between @5months and @endDate and Quantity>0)



-- sin venta en los ultimos 6 mesas
update t set [6M]=1
from #table t 
left outer join  (
	select distinct SKU,Store from series.Sales where date between @6months and @endDate and Quantity>0
) s on s.SKU = t.SKU and s.Store = t.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales where SKU=t.SKU and Store=t.Store and date between @6months and @endDate and Quantity>0)


-- sin venta en los ultimos 12 mesas
update t set [12M]=1
from #table t 
left outer join  (
	select distinct SKU,Store from series.Sales where date between @12months and @endDate and Quantity>0
) s on s.SKU = t.SKU and s.Store = t.Store
where s.SKU is null
--where NOT EXISTS (SELECT 1 FROM series.Sales where SKU=t.SKU and Store=t.Store and date between @12months and @endDate and Quantity>0)


-- marcar las activas en el mix pricing
update t set Modelo=1
from #table t 
inner join aux.DailySuggestMix m on m.SKU=t.sku and M.Store=t.store

--info stock
update t set stock = ls.stock, suggest = ls.Suggest 
from #table t inner join series.LastStock ls on ls.SKU = t.SKU and ls.Store=t.Store

----info shares
--update t set share = sh.Share 
--from #table t 
--inner join forecast.ShareStoreCurrent sh on sh.SKU = t.sku and sh.Store = t.store

--info % quiebre dada toda la info de ReportStores
update t set t.pro_stockout=1.0-h.quiebre*1.0/h.n 
from #table t inner join (
	select r.sku,r.store,COUNT(*) n,SUM(case when r.stock=0 then 1 else 0 end) quiebre  from #table t
	inner join reports.ReportStores r on t.sku = r.SKU and t.store=r.Store
	group by r.SKU,r.store 
) h on h.SKU = t.sku and h.Store = t.store

--info restricciones
declare @sku int=null,
@fecha_creacion datetime=null,
@fecha_inicio datetime = cast(getdate() as date), 
@fecha_fin datetime = cast(getdate() as date),
@ajuste_new bit= 0
	
create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
insert into #SuggestRestrictionToday
exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

update r set planogram=isnull(srt.Planograma,0), dun=(isnull(srt.MedioDUN,0)-1)*2, minimum=isnull(srt.Minimo,0), maximum=isnull(srt.Maximo,0)
	, child=isnull(srt.CajaTira,0), fixed=isnull(srt.Fijo,0), strictfix=isnull(FijoEstricto,0)
from #table r 
--inner join [view].SuggestRestrictionToday srt on srt.SKU=r.sku and srt.Store=r.store
inner join #SuggestRestrictionToday srt on srt.SKU=r.sku and srt.Store=r.store

---- info planograma
--update r set planogram = p.Detail from #table r
--inner join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID in (9,10)
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store


---- info DUN 
--update r set Dun = case when p.Detail > isnull(q.Detail,0) and p.Detail is not null then p.Detail else q.Detail end from #table r
--left outer join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID = 6
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store
--left outer join (
--	select ProductID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID = 5
--	group by ProductID,StoreID
--) q on q.ProductID = r.sku
--where q.Detail is not null or p.Detail is not null

---- info caja tira
--update r set Child = p.Detail from #table r
--inner join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID = 7
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU

---- info minimo
--update r set Minimum = case when p.Detail > isnull(q.Detail,0) and p.Detail is not null then p.Detail else q.Detail end from #table r
--left outer join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID = 4
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store
--left outer join (
--	select ProductID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@endDate)
--	where SuggestRestrictionTypeID = 3
--	group by ProductID,StoreID
--) q on q.ProductID = r.sku
--where q.Detail is not null or p.Detail is not null

/**/
-- fecha última promocion
update r set LastPromotion = p.date from #table r inner join (
	select SKU, MAX(EndDate) Date from (

		select ProductId SKU,MAX(EndDate) EndDate from promotion.PromotionStock group by ProductId 
	) a group by SKU
) p on p.SKU = r.SKU 

-- info ranking, generic, motive, division
update r set Ranking = p.TotalRanking, isgeneric = case when CategoryId=1 or ManufacturerId in (18,92) then 1 else 0 end,motive = p.Motive,Division=p.Division 
from #table r inner join products.Products p on p.SKU = r.SKU 

-- cargo costo
update r set Cost = p.cost, stockVal=stock*p.cost from #table r inner join pricing.LastPriceAndCost p on p.SKU = r.SKU 

delete from reports.ExclusionResults where [date] = @today--dbo.getmonthfirstday(@endDate)

INSERT INTO [reports].[ExclusionResults] ([SKU],[Store],[date],[Stock],[Suggest],[StockVal],WithStock,[Planogram],[Dun],[Child],[Minimum], fixed, maximum, StrictFix,[LastPromotion],[Ranking],[IsGeneric],[motive],[Cost],[Division],Modelo, [3M] , [4M] , [5M] , [6M] , [12M] )
select sku,store,@today/*dbo.getmonthfirstday(@endDate)*/,stock,suggest,stockVal,pro_stockout,planogram,dun,child,minimum, fixed, maximum, strictfix,lastpromotion,ranking,isgeneric,motive,cost,division,Modelo, [3M] , [4M] , [5M] , [6M] , [12M]
from #table where pro_stockout >= 0.9

END


