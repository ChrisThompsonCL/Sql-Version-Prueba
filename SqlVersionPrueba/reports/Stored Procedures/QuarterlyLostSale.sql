﻿-- =============================================
-- Author:		Franco Adonis y Francisco Gonzalez <3
-- Create date: 2017-04-11
-- Description:	reporte venta perdida semanal
-- Modificado: Fairenes Zerpa 2018-02-21
-- =============================================
CREATE PROCEDURE [reports].[QuarterlyLostSale] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	declare @date date = (select max(date) from reports.LostSaleBySKUcambio)

    -- VENTA PERDIDA CONSUMO MASIVO
	select  DivisionUnificadaDesc División, ClaseUnificadaDesc Clase, rs.SKU SKU, pro.description Descripción, rs.[Date], pro.motive Motivo, pro.CategoriaUnificadaDesc Categoría, pro.ManufacturerId IDLaboratorio, pro.ManufacturerDesc Laboratorio
             , case when rs.SKU IN (select sku from products.GenericFamilyDetail) then 1 else 0 end [Es Genérico?]
       -- Venta Perdida Total
             , VtaPerdida
			 , VtaReal
			 , VtaPerdidaVal
			 , VtaRealVal
			 , Combinaciones
			 , NQuiebreLocales
             , NQuiebreCD  

       -- Venta Perdida Mix Pricing				
			 ,VtaPerdidaMix
			 ,VtaRealMix
			 ,VtaPerdidaValMix
			 ,VtaRealValMix
			 ,CombMix
             ,[NquiebreLocalesMix]
             ,[NquiebreCDMix]	
			 		           
       from reports.LostSaleBySKUCambio RS
	   inner JOIN Salcobrand.products.fullproducts pro on pro.sku = rs.sku
       where isnull(pro.DivisionUnificadaId, pro.Division)=2
			and rs.[Date] between DATEADD(MONTH,-3,@date) and @date
			and rs.[Date] not in (select Date from dcpurchase.Holidays)
			and isnull(pro.TotalRanking,'NULL') not in ('Z','E')
			and RS.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
			and isnull(pro.Motive,'')<>'2-Descontinuado'
			and isnull(ManufacturerId,'')<>990



	--VENTA PERDIDA FARMA
	select p.DivisionUnificadaDesc División, rs.SKU, p.description Descripción, rs.[Date]
		--, case when PositioningId in (110,15,1,27,31,32,33,40,41,45,47,7,8,90) then 1 else 0 end FiltroPosEst
		--, case when PositioningId in (110,15,1,27,31,32,33,40,41,45,47,7,8,90) then Posicionamiento else NULL end PosEstacional
		, p.motive Motivo, p.CategoriaUnificadaDesc Categoría, p.ManufacturerId IDLaboratorio, p.ManufacturerDesc Laboratorio
		, case when rs.sku IN (select sku from products.GenericFamilyDetail) then 1 else 0 end [Es Genérico?]
	-- Venta Perdida Total
		, VtaPerdida
		, VtaReal
		, [VtaPerdidaVal]
		, VtaRealVal
		, Combinaciones
		--, rs.Sugerido
		, rs.Positioning_Desc Posicionamiento
		, [NQuiebreLocales]
		, [NQuiebreCD]	
		
      -- Venta Perdida Mix Pricing				
		,VtaPerdidaMix
		,VtaRealMix
		,VtaPerdidaValMix
		,VtaRealValMix
		,CombMix
        ,[NquiebreLocalesMix]
        ,[NquiebreCDMix]		           
			
	from reports.LostSaleBySKUcambio rs
	inner JOIN products.FullProducts p on rs.sku=p.sku
	where isnull(p.DivisionUnificadaId, p.Division)=1
		and rs.[Date] between DATEADD(MONTH,-3,@date) and @date
		and rs.Date not in (select Date from dcpurchase.Holidays)
		and isnull(p.totalranking, 'Null') not in ('Z','E') 
		and rs.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
		and isnull(p.Motive,'')<> '2-Descontinuado'
		and isnull(ManufacturerId,'')<>990



	--VENTA PERDIDA PRO-CUIDADO
	select rs.SKU, pro.description Descripción, rs.[Date], pro.motive Motivo

	-- Venta Perdida Total
		, VtaPerdida
		, VtaReal
		, [VtaPerdidaVal]
		, VtaRealVal
		, Combinaciones
		, PRO.TotalRanking Ranking
		, PRO.ManufacturerDesc Proveedor
		, [NQuiebreLocales]
		, [NQuiebreCD]
	  --, case when Motivo='90-Falla Proveedor' then 1 else 0 END FallaProveedorCant	

      -- Venta Perdida Mix Pricing				
		,VtaPerdidaMix
		,VtaRealMix
		,VtaPerdidaValMix
		,VtaRealValMix
		,CombMix
        ,[NquiebreLocalesMix]
        ,[NquiebreCDMix]		           
			
	from reports.LostSaleBySKUcambio RS
	inner JOIN Salcobrand.products.FullProducts pro on pro.sku = rs.sku
	where isnull(pro.DivisionUnificadaId, pro.Division) in (1,2)
		and rs.[Date] between DATEADD(MONTH,-3,@date) and @date
		and rs.Date not in (select [Date] from dcpurchase.Holidays)
		and isnull(pro.totalranking,'null') not in ('Z','E') 
		and rs.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
		and isnull(pro.Motive,'')<> '2-Descontinuado'
		and pro.Procuidado=1 
		and isnull(ManufacturerId,'')<>990



	--VENTA PERDIDA MARCA PROPIA
	select p.DivisionUnificadaDesc
		, rs.SKU
		, p.description Descripción
		, rs.Date
		, p.motive Motivo
		, p.CategoriaUnificadaDesc Categoría

	-- Venta Perdida Total
		, VtaPerdida
		, VtaReal
		, VtaPerdidaVal
		, VtaRealVal 
		, Combinaciones		
		, rs.ManufacturerDesc ProveedorDetail		
		, case when p.ManufacturerId=210 then 'MEDIPHARM'
				when p.ManufacturerId=330 then 'RECETARIO' ELSE 'MEDCELL' end ProveedorMP --chequear
		, CASE WHEN rs.Division=1 THEN RS.Positioning_Desc ELSE NULL END Posicionamiento
		, CASE WHEN rs.Division=2 THEN rs.Class_Desc ELSE NULL END Clase
		, case when p.sku in (select SKU from [products].[GenericFamilyDetail]) then 1 else 0 end [Genérico?]
		, [NQuiebreLocales]
		, [NQuiebreCD]
		, p.motive ÚltimoMotivo
		, case when rs.Date between DATEADD(MONTH,-1,@date) and @date then 1 else 0 end ÚltimoMes
		--, VtaPerdidaVal OPVtaPerdidaValOP 
		
		
	-- Venta Perdida Mix Pricing
		,VtaPerdidaMix
		,VtaRealMix
		,VtaPerdidaValMix
		,VtaRealValMix
		,CombMix
		,NQuiebreLocalesMix
		,NQuiebreCDMix

	from reports.LostSaleBySKUCambio RS
	inner JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
	where isnull(p.DivisionUnificadaId, p.Division) in (1,2)
		AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
		and rs.Date not in (select Date from dcpurchase.Holidays)
		--and rs.ManufacturerDesc != 'NULL'
		and isnull(p.Motive,'')<> '2-Descontinuado'
		and isnull(TotalRanking,'null') not in ('Z','E') 
		and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos		
		and P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
		and isnull(ManufacturerId,'')<>990
		 
	
	--VENTA PERDIDA MARCA PROPIA TABLA 2
	select * from (
		select TOP 5 DivisionUnificadaDesc
			, RS.SKU, p.Description Descripción

	 -- Venta Perdida Total
			, SUM(VtaPerdidaVal) VtaPerdidaVal
			, SUM(VtaRealVal) VtaRealVal
			, rs.ManufacturerDesc ProveedorDetail
			, p.motive ÚltimoMotivo
			, case when P.ManufacturerId=210 then 'MEDIPHARM' 
			       when p.ManufacturerId=330 then 'RECETARIO' ELSE 'MEDCELL' end Proveedor
			, SUM([NQuiebreLocales]) [QuiebreLocales]
			, SUM(Combinaciones) Combinaciones
			--, SUM(VtaPerdidaVal) OPVtaPerdidaValOP

		-- Venta Perdida Mix Pricing
			, SUM(VtaPerdidaValMix) VtaPerdidaValMix
			, SUM(VtaRealValMix) VtaRealValMix
			, SUM([NQuiebreLocalesMix]) [QuiebreLocalesMix]
			,sum(combmix) CombMix


		from reports.LostSaleBySKUCambio RS
		inner JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
		where P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
				AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
				AND isnull(p.DivisionUnificadaId, p.Division)=1 --FARMA
				AND P.ManufacturerId not in (210,330) --MEDCELL
				and rs.Date not in (select Date from dcpurchase.Holidays) 
				and isnull(totalranking,'NULL') not in ('Z','E')
				and isnull(p.Motive,'')<> '2-Descontinuado'
				and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
				and isnull(ManufacturerId,'')<>990
		GROUP BY DivisionUnificadaDesc, RS.SKU, p.Description, p.motive, rs.ManufacturerDesc, p.ManufacturerId
		ORDER BY VtaPerdidaVal DESC
	)a

	UNION ALL

	select * from (
		select TOP 5 DivisionUnificadaDesc
			, RS.SKU, p.Description Descripción
		-- Venta Perdida Total
			, SUM(VtaPerdidaVal) VtaPerdidaVal
			, SUM(VtaRealVal) VtaRealVal 
			, rs.ManufacturerDesc ProveedorDetail
			, p.motive ÚltimoMotivo
			, case when P.ManufacturerId=210 then 'MEDIPHARM' 
			       when p.ManufacturerId=330 then 'RECETARIO'ELSE 'MEDCELL' end Proveedor
			, SUM([NQuiebreLocales]) [QuiebreLocales]
			, SUM(Combinaciones) Combinaciones                                 
			--, SUM(VtaPerdidaVal) OPVtaPerdidaValOP

		-- Venta Perdida Mix Pricing
			, SUM(VtaPerdidaValMix) VtaPerdidaValMix
			, SUM(VtaRealValMix) VtaRealValMix
			, SUM([NQuiebreLocalesMix]) [QuiebreLocalesMix]
			,sum(combmix) CombMix

		from reports.LostSaleBySKUCambio RS
		inner JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
		where P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
				AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
				AND RS.DIVISION=2 --CONSUMO MASIVO
				AND P.ManufacturerId not in (210,330) --MEDCELL
				and TotalRanking not in ('Z','E') and  p.Motive<> '2-Descontinuado'
				and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos)
				and isnull(ManufacturerId,'')<>990
			GROUP BY DivisionUnificadaDesc, RS.SKU, p.Description, p.motive, rs.ManufacturerDesc, p.ManufacturerId
		ORDER BY VtaPerdidaVal DESC
	) b

	UNION ALL 

	select * from (
		select TOP 5 DivisionUnificadaDesc
			, RS.SKU, p.Description Descripción
		-- Venta Perdida Total
			, SUM(VtaPerdidaVal) VtaPerdidaVal
			, SUM(VtaRealVal) VtaRealVal 
			, rs.ManufacturerDesc ProveedorDetail
			, p.motive ÚltimoMotivo
			, case when p.ManufacturerId=210 then 'MEDIPHARM' 
			       when p.ManufacturerId=330 then 'RECETARIO'ELSE 'MEDCELL' end Proveedor
			, SUM([NQuiebreLocales]) [QuiebreLocales]
			, SUM(Combinaciones) Combinaciones
			--, SUM(VtaPerdidaVal) OPVtaPerdidaValOP

			
		-- Venta Perdida MixPricing
			, SUM(VtaPerdidaValMix) VtaPerdidaValMix
			, SUM(VtaRealValMix) VtaRealValMix
			, SUM([NQuiebreLocalesMix]) [QuiebreLocalesMix]
			,sum(combmix) CombMix

		from reports.LostSaleBySKUCambio RS
		INNER JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
		where P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
				AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
				AND RS.DIVISION=1 --FARMA
				AND p.ManufacturerId=210 --MEDIPHARM
				and p.Motive<> '2-Descontinuado' and totalranking not in ('Z','E')
				and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
				and isnull(ManufacturerId,'')<>990
		GROUP BY DivisionUnificadaDesc, RS.SKU, p.Description, p.motive, rs.ManufacturerDesc, p.ManufacturerId
		ORDER BY VtaPerdidaVal DESC
	) C
	
		UNION ALL 

	select * from (
		select TOP 5 DivisionUnificadaDesc
			, RS.SKU, p.Description Descripción
		-- Venta Perdida Total
			, SUM(VtaPerdidaVal) VtaPerdidaVal
			, SUM(VtaRealVal) VtaRealVal 
			, rs.ManufacturerDesc ProveedorDetail
			, p.motive ÚltimoMotivo
			, case when p.ManufacturerId=210 then 'MEDIPHARM' 
			       when p.ManufacturerId=330 then 'RECETARIO'ELSE 'MEDCELL' end Proveedor
			, SUM([NQuiebreLocales]) [QuiebreLocales]
			, SUM(Combinaciones) Combinaciones
			--, SUM(VtaPerdidaVal) OPVtaPerdidaValOP

			
		-- Venta Perdida MixPricing
			, SUM(VtaPerdidaValMix) VtaPerdidaValMix
			, SUM(VtaRealValMix) VtaRealValMix
			, SUM([NQuiebreLocalesMix]) [QuiebreLocalesMix]
			,sum(combmix) CombMix

		from reports.LostSaleBySKUCambio RS
		INNER JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
		where P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
				AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
				--AND RS.DIVISION=1 --FARMA
				AND p.ManufacturerId=330 --RECETARIO
				and isnull(p.Motive,'')<> '2-Descontinuado' and totalranking not in ('Z','E')
				and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
				and isnull(ManufacturerId,'')<>990
		GROUP BY DivisionUnificadaDesc, RS.SKU, p.Description, p.motive, rs.ManufacturerDesc, p.ManufacturerId
		ORDER BY VtaPerdidaVal DESC
	) d


	--VENTA PERDIDA MARCA PROPIA TABLA 3
	select   DivisionUnificadaDesc
			, SUM(VtaPerdidaVal) VtaPerdidaVal
			, SUM(VtaRealVal) VtaRealVal
			, case when P.ManufacturerId=210 then 'MEDIPHARM' 
			  when p.ManufacturerId=330 then 'RECETARIO' ELSE 'MEDCELL' end Proveedor
			, count(DISTINCT RS.SKU) SKUs			
			, SUM(VtaPerdidaVal)/cast(SUM(VtaRealVal) as float) [%VtaPerdida]
			, SUM([NQuiebreLocales])/cast(SUM(Combinaciones) as float) [%Quiebre]	

			-- Venta perdida mix
			, SUM(VtaPerdidaValMix) VtaPerdidaValMix
			, SUM(VtaRealValMix) VtaRealValMix
				
			, SUM(VtaPerdidaValMix)/cast(SUM(VtaRealValMix) as float) [%VtaPerdidaMix]
			, SUM([NQuiebreLocalesMix])/cast(SUM(Combinaciones) as float) [%QuiebreMix]	

					
		from reports.LostSaleBySKUCambio RS
		inner JOIN Salcobrand.products.FullProducts P on P.sku = rs.sku
		where P.ManufacturerId IN (SELECT * FROM operation.PrivateLabelManufacturers)
				--(210,10,121,274,410,411,601,809,915,903,810,3227) --PREGUNTAR!
				AND rs.[Date] between DATEADD(MONTH,-1,@date) and @date
				AND (CASE WHEN p.ManufacturerId=210 THEN p.DIVISION else 1 end)=1 
				and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
				and isnull(p.Motive,'')<> '2-Descontinuado' 
				and isnull(totalranking,'Null') not in ('Z','E')
				and isnull(ManufacturerId,'')<>990
		GROUP BY DivisionUnificadaDesc
		, case when P.ManufacturerId=210 then 'MEDIPHARM' when p.ManufacturerId=330 then 'RECETARIO' ELSE 'MEDCELL' end


	--VENTA PERDIDA AUGE
	select p.SKU, p.description Descripción, p.TotalRanking Ranking
		  , Date
		  , p.motive Motivo
		  , p.ManufacturerDesc Proveedor
		  , case when p.SKU not in (select SKU from [products].[GenericFamilyDetail]) then 1 else 0 end [Genérico?]

	-- Venta Perdida Total
		  , NQuiebreLocales
		  , NQuiebreCD
		  , VtaPerdida
		  , VtaReal
		  , VtaPerdidaVal
		  , VtaRealVal
		  , Combinaciones

	-- Venta Perdida Mix Pricing
		
		,[NquiebreLocalesMix]
        ,[NquiebreCDMix]		
		,VtaPerdidaMix
		,VtaRealMix
		,VtaPerdidaValMix
		,VtaRealValMix
		,CombMix
		         			
	from reports.LostSaleBySKUcambio RS
	INNER JOIN Salcobrand.products.fullproducts P on P.sku = rs.sku
	WHERE p.Ges=1 --auge
		and rs.[Date] between DATEADD(MONTH,-3,@date) and @date
		and isnull(p.Motive,'')<> '2-Descontinuado' 
		and isnull(totalranking,'Null') not in ('Z','E') 
		and p.SKU not in (select SKU from [products].[GenericFamilyDetail]) --Se agrega el filtro de genéricos
		and isnull(ManufacturerId,'')<>990

END
