﻿-- =============================================
-- Author:		<Unknown>
-- Create date: <Unknown>
-- Description:	<Unknown>
--
-- Updated by:	Gabriel Espinoza
-- On:			2018-01-11
-- Description: Se actualiza para mantener consistencia de la carga y permitir discriminar entre productos
--				que saldrán del sugerido y productos que se mantendrán en el sugerido.
-- Updated by:	Juan Manuel
-- On:			2018-05-28
-- Description: Se agrega columna [PromotionLevel] .

-- =============================================
CREATE PROCEDURE [promotion].[LoadPromotion]
 @fileId INT ,
 @username nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insertamos en PromotionStock.
	INSERT INTO [promotion].[PromotionStock]
           ([ProductId]
           ,[ProductType]
           ,[StoreId]
           ,[StoreType]
           ,[DateAggregation]
           ,[BeginDate]
           ,[EndDate]
           ,[PromotionFileId]
           ,[PromotionName]
		   ,[DebeCalcularSugerido]
		   ,[Goal]
		   ,[PromotionPrice]
		   
		   )

	select [ProductId],0,1,3,1,[BeginDate],[EndDate],@fileId,[PromotionName], [DebeCalcularSugerido], [Goal], [PromotionPrice]
	from aux.PromotionStockLoad p
	where p.PromotionFileId = @fileId

	--borramos el archivo para no dejar basura
	
	 delete from [aux].[PromotionStockLoad] where PromotionFileId = @fileId 

	EXEC SalcobrandAbastecimiento.operation.LoadPromotionInfo
	
END
