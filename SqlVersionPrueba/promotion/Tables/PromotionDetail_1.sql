﻿CREATE TABLE [promotion].[PromotionDetail] (
    [PromotionId]         BIGINT        NOT NULL,
    [BeginDate]           SMALLDATETIME NOT NULL,
    [EndDate]             SMALLDATETIME NULL,
    [DateAggregation]     NCHAR (10)    NOT NULL,
    [ProductId]           INT           NOT NULL,
    [ProductType]         INT           NOT NULL,
    [StoreId]             INT           NOT NULL,
    [StoreType]           INT           NOT NULL,
    [PromotionVariableId] INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_PromotionDetail_ProductId]
    ON [promotion].[PromotionDetail]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PromotionDetail_Dates]
    ON [promotion].[PromotionDetail]([BeginDate] ASC, [EndDate] ASC)
    INCLUDE([ProductId]);

