﻿CREATE TABLE [promotion].[Promotion] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]            NVARCHAR (255) NULL,
    [BeginDate]       SMALLDATETIME  NOT NULL,
    [EndDate]         SMALLDATETIME  NULL,
    [DateAggregation] INT            NOT NULL,
    [VariableId]      INT            NOT NULL,
    [PromotionFileId] INT            NULL,
    CONSTRAINT [PK_Promotion] PRIMARY KEY CLUSTERED ([Id] ASC)
);

