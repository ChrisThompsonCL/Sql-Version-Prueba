﻿CREATE TABLE [promotion].[PromotionStock] (
    [ProductId]            INT            NOT NULL,
    [ProductType]          INT            NOT NULL,
    [StoreId]              INT            NOT NULL,
    [StoreType]            INT            NOT NULL,
    [DateAggregation]      NCHAR (10)     NOT NULL,
    [BeginDate]            SMALLDATETIME  NOT NULL,
    [EndDate]              SMALLDATETIME  NOT NULL,
    [PromotionFileId]      INT            NOT NULL,
    [PromotionName]        NVARCHAR (255) NULL,
    [DebeCalcularSugerido] BIT            NULL,
    [PromotionLevel]       NVARCHAR (50)  NULL,
    [Goal]                 INT            NULL,
    [PromotionPrice]       FLOAT (53)     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_PromotionStock_ProductID]
    ON [promotion].[PromotionStock]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PromotionStock_Dates]
    ON [promotion].[PromotionStock]([BeginDate] ASC, [EndDate] ASC)
    INCLUDE([ProductId], [ProductType], [StoreId], [StoreType], [PromotionFileId]);

