﻿CREATE TABLE [cdbuy].[ProductsPackingUnit] (
    [SKU]      INT NOT NULL,
    [DUN]      INT NULL,
    [StoreDUN] INT NULL,
    CONSTRAINT [PK_ProductsPackingUnitNew] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

