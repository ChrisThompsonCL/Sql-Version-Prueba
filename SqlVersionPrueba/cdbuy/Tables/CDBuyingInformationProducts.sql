﻿CREATE TABLE [cdbuy].[CDBuyingInformationProducts] (
    [Date]                SMALLDATETIME NOT NULL,
    [SKU]                 INT           NOT NULL,
    [PriorityInformation] INT           NULL,
    [BuyingDays]          SMALLINT      NULL,
    [ProtectionMin]       FLOAT (53)    NULL,
    [ProtectionMax]       FLOAT (53)    NULL,
    CONSTRAINT [PK_CDBuyingInformationProducts2] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);

