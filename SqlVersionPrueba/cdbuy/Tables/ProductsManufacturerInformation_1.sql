﻿CREATE TABLE [cdbuy].[ProductsManufacturerInformation] (
    [CreateDate]      SMALLDATETIME NULL,
    [ManufacturerID]  INT           NULL,
    [SKU]             BIGINT        NULL,
    [RUT]             NVARCHAR (50) NULL,
    [OCID]            INT           NULL,
    [State]           INT           NULL,
    [Ordered]         INT           NULL,
    [Delivered]       INT           NULL,
    [OrderDate]       SMALLDATETIME NULL,
    [ExpirationDate]  SMALLDATETIME NULL,
    [ExpirationDate2] SMALLDATETIME NULL,
    [DeliveryDate]    SMALLDATETIME NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductsManufacturerInformation2]
    ON [cdbuy].[ProductsManufacturerInformation]([CreateDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductsManufacturerInformation2_1]
    ON [cdbuy].[ProductsManufacturerInformation]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductsManufacturerInformation2_2]
    ON [cdbuy].[ProductsManufacturerInformation]([ManufacturerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductsManufacturerInformation2_3]
    ON [cdbuy].[ProductsManufacturerInformation]([OCID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductsManufacturerInformation2_4]
    ON [cdbuy].[ProductsManufacturerInformation]([OrderDate] ASC)
    INCLUDE([SKU], [OCID], [Ordered]);

