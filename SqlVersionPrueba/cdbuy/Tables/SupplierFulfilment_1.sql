﻿CREATE TABLE [cdbuy].[SupplierFulfilment] (
    [ManufacturerID] INT           NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [Order]          FLOAT (53)    NOT NULL,
    [Received]       FLOAT (53)    NOT NULL,
    [Compliance]     FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_SupplierFulfilment] PRIMARY KEY NONCLUSTERED ([ManufacturerID] ASC, [Date] ASC)
);

