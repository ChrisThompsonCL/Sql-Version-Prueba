﻿CREATE TABLE [cdbuy].[ManufacturerDeliveryInformation] (
    [ManufacturerId] INT           NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [FillrateAvg]    FLOAT (53)    NULL,
    [FillrateStd]    FLOAT (53)    NULL,
    [LeadtimeAvg]    FLOAT (53)    NULL,
    [LeadtimeStd]    FLOAT (53)    NULL,
    [Norder]         INT           NULL,
    CONSTRAINT [PK_ManufacturerDeliveryInformation] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ManufacturerDeliveryInformation]
    ON [cdbuy].[ManufacturerDeliveryInformation]([ManufacturerId] ASC);

