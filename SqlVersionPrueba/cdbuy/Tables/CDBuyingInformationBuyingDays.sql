﻿CREATE TABLE [cdbuy].[CDBuyingInformationBuyingDays] (
    [Priority]      INT           NULL,
    [Property]      NVARCHAR (50) NULL,
    [PropertyValue] NVARCHAR (50) NULL,
    [BuyingDays]    SMALLINT      NULL,
    [ProtectionMin] FLOAT (53)    NULL,
    [ProtectionMax] FLOAT (53)    NULL,
    [ReviewDays]    SMALLINT      NULL
);

