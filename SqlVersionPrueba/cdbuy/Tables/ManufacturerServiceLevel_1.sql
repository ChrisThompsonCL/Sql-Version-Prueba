﻿CREATE TABLE [cdbuy].[ManufacturerServiceLevel] (
    [ManufacturerID] INT        NOT NULL,
    [ClassRanking]   NCHAR (1)  CONSTRAINT [DF_ManufacturerServiceLevel_ClassRanking] DEFAULT (N'-') NOT NULL,
    [ServiceLevel]   FLOAT (53) CONSTRAINT [DF_ManufacturerServiceLevel_ServiceLevel] DEFAULT ((90)) NOT NULL,
    CONSTRAINT [PK_ManufacturerServiceLevel] PRIMARY KEY CLUSTERED ([ManufacturerID] ASC, [ClassRanking] ASC)
);

