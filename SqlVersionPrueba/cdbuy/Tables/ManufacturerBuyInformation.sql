﻿CREATE TABLE [cdbuy].[ManufacturerBuyInformation] (
    [ManufacturerId] INT NULL,
    [Division]       INT NULL,
    [Dayofbuy]       INT NULL,
    [Frequency]      INT NULL
);

