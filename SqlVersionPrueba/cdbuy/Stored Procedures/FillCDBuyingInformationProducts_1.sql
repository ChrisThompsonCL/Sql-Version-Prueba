﻿-- =============================================
-- Author:		Diego Oyarzun
-- Create date: <Create Date,,>
-- Description:	Determina prioridad y días de protección para cálculo de compras
-- =============================================
CREATE PROCEDURE [cdbuy].[FillCDBuyingInformationProducts]
	@dateKey smalldatetime = NULL --DateKey Lunes
AS
BEGIN
	SELECT @dateKey = DATEADD(WEEK,-1, @dateKey)
	-- Limpio la tabla
	truncate table [cdbuy].[CDBuyingInformationProducts]

	-- Declaracion de variables fechas
	declare @fromDate smalldatetime, @toDate smalldatetime
	set @fromDate = @dateKey
	set @toDate = dateadd(day, 7*6 -1, @fromDate)

	-- Llena la tabla
	INSERT INTO [cdbuy].[CDBuyingInformationProducts] ([Date],[SKU])
	SELECT o.BuyingDate, p.SKU FROM 
	(
		SELECT distinct ManufacturerId, BuyingDate
		FROM [cdbuy].[BuyingEvents] WHERE BuyingDate between @fromDate and @toDate
	) o
	inner join (select sku, manufacturerid from [products].[Products]) p
	ON o.manufacturerid = p.manufacturerid
	ORDER BY o.BuyingDate, p.SKU

	-- Llena los dias de compra y dias de protección
	DECLARE @buyingdays int, @priority int, @propertyvalue nvarchar(50), @pmin float, @pmax float, @prop nvarchar(50)

	DECLARE @tuplas cursor
	SET @tuplas = CURSOR FAST_FORWARD
	FOR
		select property,propertyvalue,priority,protectionmin,protectionmax,buyingdays 
		from [cdbuy].[CDBuyingInformationBuyingDays] order by priority desc

	OPEN @tuplas
	FETCH NEXT FROM @tuplas
	INTO @prop,@propertyvalue,@priority,@pmin,@pmax,@buyingdays

	WHILE @@FETCH_STATUS = 0
	BEGIN			 

		DECLARE @SQLStatement varchar(500)
		SELECT @SQLStatement = 'UPDATE [cdbuy].[CDBuyingInformationProducts]
								SET [PriorityInformation] = '+convert(nvarchar(50),@priority)+'
								   ,[BuyingDays] = '+convert(nvarchar(50),@buyingdays)+'
								   ,[ProtectionMin] = '+convert(nvarchar(50),@pmin)+'
								   ,[ProtectionMax] = '+convert(nvarchar(50),@pmax)+'
								WHERE SKU in (SELECT SKU FROM [products].[Products] WHERE [' + @prop + '] = '''+convert(nvarchar(50),@propertyvalue)+''')'
		
		EXEC(@SQLStatement)

		FETCH NEXT FROM @tuplas
		INTO @prop,@propertyvalue,@priority,@pmin,@pmax,@buyingdays
	END

	CLOSE @tuplas
	DEALLOCATE @tuplas	
	
	--se eliminan aquellos que quedan sin información
	DELETE FROM [cdbuy].[CDBuyingInformationProducts] WHERE PriorityInformation is null
	
RETURN 
END
