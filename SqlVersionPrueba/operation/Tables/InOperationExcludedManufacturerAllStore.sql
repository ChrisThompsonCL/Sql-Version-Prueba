﻿CREATE TABLE [operation].[InOperationExcludedManufacturerAllStore] (
    [ManufacturerId] INT NOT NULL,
    CONSTRAINT [PK_InOperationExcludedManufacturerAllStore] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

