﻿CREATE TABLE [operation].[UserLog] (
    [Id]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Date]     SMALLDATETIME NOT NULL,
    [UserId]   BIGINT        NULL,
    [Machine]  VARCHAR (50)  NULL,
    [Hostname] VARCHAR (50)  NULL,
    CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

