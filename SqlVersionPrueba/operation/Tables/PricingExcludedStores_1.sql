﻿CREATE TABLE [operation].[PricingExcludedStores] (
    [Store]   INT           NOT NULL,
    [Name]    NVARCHAR (50) NULL,
    [Region3] NVARCHAR (50) NULL,
    CONSTRAINT [PK_PricingExcludedStores] PRIMARY KEY CLUSTERED ([Store] ASC)
);

