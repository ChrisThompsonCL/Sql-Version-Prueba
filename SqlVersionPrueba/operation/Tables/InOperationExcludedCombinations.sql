﻿CREATE TABLE [operation].[InOperationExcludedCombinations] (
    [SKU]   INT      NOT NULL,
    [Store] SMALLINT NOT NULL,
    CONSTRAINT [PK_InOperationExcludedCombinations] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_InOperationExcludedCombinations]
    ON [operation].[InOperationExcludedCombinations]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InOperationExcludedCombinations_1]
    ON [operation].[InOperationExcludedCombinations]([Store] ASC);

