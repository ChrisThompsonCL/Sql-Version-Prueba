﻿CREATE TABLE [operation].[ExecutionLog] (
    [Id]         INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Date]       SMALLDATETIME NOT NULL,
    [Classname]  VARCHAR (50)  NULL,
    [Methodname] VARCHAR (250) NULL,
    [Machine]    VARCHAR (50)  NULL,
    [Hostname]   VARCHAR (50)  NULL,
    [Completed]  BIT           NULL,
    CONSTRAINT [PK_ExecutionLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

