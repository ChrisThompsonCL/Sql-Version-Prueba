﻿CREATE TABLE [operation].[ServicesProductsDetail] (
    [ServiceId] INT    NOT NULL,
    [SKU]       BIGINT NOT NULL,
    CONSTRAINT [PK_ServicesProductsDetail] PRIMARY KEY CLUSTERED ([ServiceId] ASC, [SKU] ASC),
    CONSTRAINT [FK_ServiceProductsDetail_Services] FOREIGN KEY ([ServiceId]) REFERENCES [operation].[Services] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ServicesProductsDetail]
    ON [operation].[ServicesProductsDetail]([ServiceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ServicesProductsDetail_1]
    ON [operation].[ServicesProductsDetail]([SKU] ASC);

