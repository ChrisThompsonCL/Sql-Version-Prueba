﻿CREATE TABLE [operation].[InOperationManufacturerAllStore] (
    [ManufacturerId] INT NOT NULL,
    [InOperation]    BIT NULL,
    CONSTRAINT [PK_InOperationManufacturerAllStore] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

