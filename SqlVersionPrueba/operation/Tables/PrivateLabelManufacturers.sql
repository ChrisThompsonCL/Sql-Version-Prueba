﻿CREATE TABLE [operation].[PrivateLabelManufacturers] (
    [ManufacturerId] INT NOT NULL,
    CONSTRAINT [PK_PrivateLabelManufacturers] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

