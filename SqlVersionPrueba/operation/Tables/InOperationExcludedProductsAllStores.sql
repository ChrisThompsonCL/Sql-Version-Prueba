﻿CREATE TABLE [operation].[InOperationExcludedProductsAllStores] (
    [SKU] INT NOT NULL,
    CONSTRAINT [PK_InOperationExcludedProductsAllStores] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

