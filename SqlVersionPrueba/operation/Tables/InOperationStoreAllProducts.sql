﻿CREATE TABLE [operation].[InOperationStoreAllProducts] (
    [Store]       SMALLINT NOT NULL,
    [InOperation] BIT      NULL,
    CONSTRAINT [PK_InOperationStoreAllProducts] PRIMARY KEY CLUSTERED ([Store] ASC)
);

