﻿CREATE TABLE [operation].[ServicesStoresDetail] (
    [ServiceId] INT    NOT NULL,
    [Store]     BIGINT NOT NULL,
    CONSTRAINT [PK_ServicesStoresDetail] PRIMARY KEY CLUSTERED ([ServiceId] ASC, [Store] ASC),
    CONSTRAINT [FK_ServiceStoresDetail_Services] FOREIGN KEY ([ServiceId]) REFERENCES [operation].[Services] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ServicesStoresDetail]
    ON [operation].[ServicesStoresDetail]([ServiceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ServicesStoresDetail_1]
    ON [operation].[ServicesStoresDetail]([Store] ASC);

