﻿CREATE TABLE [operation].[PricingExcludedProducts] (
    [SKU] BIGINT NOT NULL,
    CONSTRAINT [PK_PricingExcludedProducts] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

