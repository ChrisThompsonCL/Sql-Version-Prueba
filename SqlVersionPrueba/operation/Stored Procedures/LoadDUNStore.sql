﻿
-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-02-27
-- Description:	Carga las unidades de embalaje en base a los datos existentes en test.integracion_FABRICANTES
-- =============================================
CREATE PROCEDURE [operation].[LoadDUNStore]
	
AS
BEGIN

	DECLARE @date DATE = GETDATE()

	TRUNCATE TABLE [products].[DUNStore]

	INSERT INTO [products].[DUNStore] (SKU, DUN14, Units, RefDate, CreateDate, UpdateDate)
	select SKU, DUN14, Units, @date, CreateDate, UpdateDate
	from (
		select *, row_number() over(partition by SKU order by UpdateDate desc, CreateDate desc, Units) R
		from [aux].[TempDUNStore]
	) a where R = 1 


END
