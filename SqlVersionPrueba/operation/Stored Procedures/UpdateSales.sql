﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-09-02
-- Description:	Carga ventas temporales (lo usa el robot)
-- =============================================
CREATE PROCEDURE [operation].[UpdateSales]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*información a nivel de combinación*/
	--Procura no cargar dos veces un mismo día
	declare @date date = (select top 1 [date] from aux.TempSales)

	if exists(select @date)
	begin
		DELETE FROM series.Sales 
		WHERE [Date] = @date

		--Carga Datos
		INSERT INTO [series].[Sales]([SKU],[Store],[Date],[Quantity],[Income])
		SELECT [SKU],[Store],[Date],[Quantity],[Income]  
		FROM aux.TempSales
		WHERE Quantity > 0 

		/*información a nivel de SKU*/
		DELETE FROM [series].[DailySales] WHERE [Date] = @date

		INSERT INTO [series].[DailySales] ([SKU],[Date],[Quantity],[Income])
		SELECT SKU, [date], SUM(Quantity)Quantity, SUM(Income)Income
		FROM aux.TempSales
		WHERE Quantity>0 and Income>0
		GROUP BY SKU, [date]

	
		/*respaldo ultimo dia*/
		truncate table series.LastSale
		insert into series.LastSale
		select * from aux.TempSales
	end
	
	--[2018-05-25 Dani A.] Se comenta porque ahora el procedimiento lo ejecuta el robot. 
	--exec minmax.[GenerateDailySales]


	--/*propuesta de sugerido agil*/
	--exec [minmax].[CalcularSugeridoAgil] Se comenta por situación del CD. Esto fue informado a Rodrigo Muñoz
	--exec [minmax].[AplicarSugeridoAgil]

	--vacía la tabla 
	TRUNCATE TABLE aux.TempSales

END
