﻿-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2011-04-05
-- Description:	Actualiza las tablas de [products].[FullProducts] y [products].[Products] que no tengan First ni Last Sale.
-- =============================================
CREATE PROCEDURE [operation].[UpdateProductsFirstAndLastSale] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	CREATE TABLE #aux (SKU int, MinDate date, MaxDate date)
	CREATE INDEX #IDX1 ON #aux(SKU) include (MinDate, MaxDate)

	INSERT INTO #aux
	SELECT SKU, MIN(Date) ,MAX(Date)
	from [series].[Sales] 
	GROUP BY SKU

	--products.FullProducts
	UPDATE s SET s.FirstSale = case when s.FirstSale is null then Sales.MinDate else s.FirstSale end, s.LastSale=Sales.MaxDate 
	FROM products.FullProducts s 
	inner join #aux as Sales on sales.SKU = s.SKU


	--products.Products
	UPDATE s SET s.FirstSale = case when s.FirstSale is null then Sales.MinDate else s.FirstSale end, s.LastSale=Sales.MaxDate 
	FROM products.Products s 
	inner join #aux as Sales on sales.SKU = s.SKU

	drop table #aux
	
END
