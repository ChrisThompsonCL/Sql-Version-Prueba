﻿
-- =============================================
-- Author:		Gabriel Espinoza Erices (en verdad #LaDani)
-- Create date: 2018-02-27
-- Description:	Carga los manufacturers en base a los datos existentes en test.integracion_FABRICANTES
-- =============================================
CREATE PROCEDURE [operation].[LoadManufacturers]
	
AS
BEGIN
	--SET NOCOUNT ON;
  
	MERGE INTO products.Manufacturers as m
	USING ( select * from aux.TempManufacturers ) AS f
	on m.Id = CAST(f.id AS INT)
	WHEN MATCHED THEN
	UPDATE SET m.[Name] = f.[Name], m.RUT = f.RUT
	WHEN NOT MATCHED BY TARGET THEN
	INSERT (Id, [Name], RUT)
	VALUES (f.Id, f.[Name], f.RUT)
	WHEN NOT MATCHED BY SOURCE THEN
	DELETE
	;

	exec SalcobrandAbastecimiento.operation.LoadManufacturers

END
