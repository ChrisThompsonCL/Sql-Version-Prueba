﻿

-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-02-28
-- Description:	Carga el stock del cd desde tabla staging a tabla operativa. Se cargan sólo los productos activos.
-- =============================================
CREATE PROCEDURE operation.[UpdateStockCDDia]
	
AS
BEGIN

	--delete from series.StockDiarioCD
	--where [Date] IN (select dia_id from temp.integracion_btBodegaNueva)

	--insert into series.StockDiarioCD
	--select dia_id
	--	, producto_id
	--	, LEFT(stock_bodega, CHARINDEX('.', stock_bodega)-1)
	--	, LEFT(stock_transito, CHARINDEX('.', stock_transito)-1)
	--from temp.integracion_btBodegaNueva b
	--inner join products.Products p ON CAST(b.producto_id AS BIGINT) = CAST(p.SKU AS BIGINT)
	--where filial_id = 999 


	DELETE FROM series.StockCDDia
	WHERE [Date] IN (SELECT distinct [Date] FROM [aux].[TempStockCD])

	INSERT INTO series.StockCDDia
	select [Date], SKU, SUM(Stock), SUM(Transit), AVG(UPrice),avg( UCost)--, count(*)
	FROM [aux].[TempStockCD] a
	WHERE Store=999
	group by [Date], SKU
	having count(*)=1

	TRUNCATE TABLE [aux].[TempStockCD]

END
