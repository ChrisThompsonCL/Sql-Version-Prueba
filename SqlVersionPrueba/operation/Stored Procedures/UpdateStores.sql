﻿

-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2009-08-03
-- Description:	Actualiza la Tabla Store desde StoreTemp
-- =============================================
CREATE PROCEDURE [operation].[UpdateStores]
AS
BEGIN
	DECLARE @msg NVARCHAR(100) 
	SET NOCOUNT ON;
	
	-- Llenar campo IsOpen en TempStores
	UPDATE [aux].[TempStores]
	SET isOpen = 
		case 
			when status='ABIERTO' then 1 
			else 0 
		end

	/************************ACTUALIZACIÓN FULLSTORES CON TEMPSTORES**************************/		
	
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de FullStores...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	-- Actualizar los locales existentes
	UPDATE S
	SET    S.[Description] = T.[Description]
		  ,S.[Address] = T.[Address]
		  ,S.[IsOpen] = T.[IsOpen]
		  ,S.[City] = T.[City]
		  ,S.[Commune] = T.[Commune]
		  ,S.[Region0] = T.[Region0]
		  ,S.[Region2] = T.[Region2]
		  ,S.[Region3] = T.[Region3]
		  ,S.[Administrator] = T.[Administrator]
		  ,S.[FormatName] = T.[FormatName]
		  ,S.[Access] = T.[Access]
		  ,S.[StoreType] = T.[StoreType]
		  ,S.[Segment] = T.[Segment]
		  ,S.[TotalRanking] = T.[TotalRanking]
		  ,S.[FarmaRanking] = T.[FarmaRanking]
		  ,S.[ConsumoRanking] = T.[ConsumoRanking]
		  --,S.[FirstSale] = T.[FirstSale]
		  ,S.[AMB] = T.[AMB]
		  ,S.[AMB2] = T.[AMB2]
		  --,S.[LastSale] = T.[LastSale]
		  --,S.[isSundayOpen] = T.[IsSundayOpen]
		  --,S.[Shift] = T.[Shift]
		  ,S.[Status] = T.[Status]
	FROM [aux].[TempStores] T INNER JOIN [stores].[FullStores] S
		on T.Store = S.Store



	-- Copiar nuevos locales al maestro
	INSERT INTO [stores].[FullStores]
           ([Store]
           ,[Description]
           ,[Address]
           ,[IsOpen]
           ,[City]
           ,[Commune]
           ,[Region0]
           ,[Region2]
           ,[Region3]
           ,[Administrator]
           ,[FormatName]
           ,[Access]
           ,[StoreType]
           ,[Segment]
           ,[TotalRanking]
           ,[FarmaRanking]
           ,[ConsumoRanking]
           ,[FirstSale]
           ,[AMB]
           ,[AMB2]
           ,[LastSale]
           ,[SundayOpen]
           ,[Shift]
           ,[Status]
           ,[Operative])
		SELECT [Store]
			,[Description]
			,[Address]
			,[IsOpen]
			,[City]
			,[Commune]
			,[Region0]
			,[Region2]
			,[Region3]
			,[Administrator]
			,[FormatName]
			,[Access]
			,[StoreType]
			,[Segment]
			,[TotalRanking]
			,[FarmaRanking]
			,[ConsumoRanking]
			,[FirstSale]
			,[AMB]
			,[AMB2]
			,[LastSale]
			,[IsSundayOpen]
			,[Shift]
			,[Status]
			,null
		FROM [aux].[TempStores] 
		WHERE Store not in (SELECT Store FROM [stores].[FullStores])


	-- Llenar campo id_region0 en [FullStores]	
	UPDATE s
	SET id_region0=
		case 
			when rc.Region0_OLD like '%NORTE%' then 1
			when rc.Region0_OLD = 'METROPOLITANA' then 2
			when rc.Region0_OLD like '%SUR%' then 3 
			when rc.Region0_OLD = 'QUINTA' then 3
			when rc.Region0_OLD = 'OCTAVA' then 3
			else 0 
		end
	from [stores].[FullStores] s
	inner join aux.RegionConversion rc on rc.Region0_NEW=s.Region0

	update s set [SundayOpen] = case when [Shift] like '%DOMINGO' then 1 else 0 end 
	from stores.FullStores s

	--EXEC [operation].[UpdateStoresFirstAndLastSale] 

	/*************************ACTUALIZACIÓN STORES CON FULLSTORES***************************/		

	/*sacar locales espejos que tengan más de 6 meses de venta*/
	--DELETE --SELECT *
	--FROM stores.NewStores 
	--where begindate < DATEADD(MONTH,-3,GETDATE())

	DELETE --SELECT *
	FROM stores.NewStores2
	where begindate < DATEADD(MONTH,-3,GETDATE())

	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de Stores...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	-- Actualizar locales existentes
	UPDATE S
	SET    S.[Name] = T.[Description]
		  ,S.[City] = T.[City]
		  ,S.[Commune] = T.[Commune]
		  ,S.[Format] = T.[FormatName]
		  ,S.[Address] = T.[Address]
		  ,S.[IsOpen] = T.[IsOpen]
		  ,S.[Region0] = T.[Region0]
		  ,S.[Region2] = T.[Region2]
		  ,S.[Region3] = T.[Region3]
		  ,S.[Id_Region0] = T.[Id_Region0]
		  ,S.[Administrator] = T.[Administrator]
		  ,S.[Access] = T.[Access]
		  ,S.[StoreType] = T.[StoreType]
		  ,S.[Segment] = T.[Segment]
		  ,S.[TotalRanking] = T.[TotalRanking]
		  ,S.[FarmaRanking] = T.[FarmaRanking]
		  ,S.[ConsumoRanking] = T.[ConsumoRanking]
		  --,S.[FirstSale] = T.[FirstSale]
		  --,S.[LastSale] = T.[LastSale]
		  ,S.[SundayOpen] = T.[SundayOpen]
		  ,S.[Shift] = T.[Shift]
		  ,S.[Status] = T.[Status]
	FROM [stores].FullStores T INNER JOIN [stores].[Stores] S
		on T.Store = S.Store
		
	-- Insertar locales nuevos que cumplan criterios
	INSERT INTO [stores].[Stores]
		([Store]
		,[Name]
		,[Retail]
		,[City]
		,[Commune]
		,[Format]
		,[Address]
		,[IsOpen]
		,[Region0]
		,[Region2]
		,[Region3]
		,[Id_Region0]
		,[Administrator]
		,[Access]
		,[StoreType]
		,[Segment]
		,[TotalRanking]
		,[FarmaRanking]
		,[ConsumoRanking]
		,[FirstSale]
		,[LastSale]
		,[SundayOpen]
		,[Shift]
		,[Status]
		,[operative])
    SELECT [Store]
		,[Description]
		,'Salcobrand' --,[Retail]
		,[City]
		,[Commune]
		,[FormatName]
		,[Address]
		,[IsOpen]
		,[Region0]
		,[Region2]
		,[Region3]
		,[Id_Region0]
		,[Administrator]
		,[Access]
		,[StoreType]
		,[Segment]
		,[TotalRanking]
		,[FarmaRanking]
		,[ConsumoRanking]
		,[FirstSale]
		,[LastSale]
		,[SundayOpen]
		,[Shift]
		,[Status]
		,[Operative]
	FROM [stores].[FullStores]
	WHERE Store not in (SELECT Store FROM [stores].[Stores])
		and (
			(LastSale >= dateadd(week,-1,GETDATE()) --registra ventas en la última semana
				and FirstSale <= DATEADD(MONTH, -2, GETDATE()))  --vende desde al menos 2 meses atrás
			OR (Store in (select NewStore from stores.NewStores2))
			)


	-- Borrar locales que ya no cumplen criterios
	DELETE	--SELECT * 
	FROM [stores].[Stores]
	WHERE not (
			(LastSale >= dateadd(week,-1,GETDATE()) --registra ventas en la última semana
				and FirstSale <= DATEADD(MONTH, -2, GETDATE()))  --vende desde al menos 2 meses atrás
			OR (Store in (select NewStore from stores.NewStores2))
			)
		

	/*************************ACTUALIZACIÓN ÁRBOL EN BASE A STORES***************************/
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion del Arbol...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	-- Borrar último nivel del árbol
	TRUNCATE TABLE [stores].[ClusterDetail]
	
	--Agrega locales
	INSERT INTO [stores].[ClusterDetail] (Id, StoreId)
	SELECT 1, Store
	FROM [stores].[Stores]

	/*************************ACTUALIZACIÓN DE STORES EN SERVICIO***************************/
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de Servicios...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--Se borran de todos los servicios los stores que no estén en el maestro
	DELETE --SELECT *
	FROM operation.ServicesStoresDetail
	WHERE Store NOT IN (
		SELECT Store 
		FROM [stores].[Stores]
	)

	--se borran locales sin rutas del servicio de inventario en locales
	DELETE --select *
	FROM operation.ServicesStoresDetail
	where ServiceId=4 and Store not in (SELECT distinct Store from minmax.Replenishment where [days]>0)
	
	--se agregar a los servicios 1, 2 y 4 todos los stores del maestro
	INSERT INTO operation.ServicesStoresDetail (ServiceId, Store)
	SELECT 1, Store
	FROM [stores].[Stores]
	WHERE Store NOT IN (SELECT Store FROM operation.ServicesStoresDetail WHERE ServiceId = 1)
	UNION
	SELECT 2, Store
	FROM [stores].[Stores]
	WHERE Store NOT IN (SELECT Store FROM operation.ServicesStoresDetail WHERE ServiceId = 2)
	UNION
	SELECT 4, Store
	FROM [stores].[Stores]
	WHERE Store NOT IN (SELECT Store FROM operation.ServicesStoresDetail WHERE ServiceId = 4)
		and Store in (SELECT distinct Store from minmax.Replenishment where [days]>0)
	
	--Servicio de Sugeridos: a todos los stores que cumplan condiciones
	DELETE ssd --SELECT *
	FROM operation.ServicesStoresDetail ssd
	WHERE ssd.ServiceId = 3
	AND ssd.Store IN (SELECT Store FROM operation.InOperationStoreAllProducts WHERE InOperation = 0)
	
	INSERT INTO operation.ServicesStoresDetail (ServiceId, Store)
	SELECT 3, Store
	FROM [stores].[Stores]
	WHERE  Store NOT IN (SELECT Store FROM operation.ServicesStoresDetail WHERE ServiceId = 3)
	AND Store NOT IN (SELECT Store FROM operation.InOperationStoreAllProducts WHERE InOperation = 0)
END


