﻿
-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-02-26
-- Description:	
-- =============================================
CREATE PROCEDURE [operation].[LoadDespachosCD]
	
AS
BEGIN
	SET NOCOUNT ON;

	--eliminamos las fechas que se estan integrando de la tabla destino
	delete 
	from series.DespachosCD
	where [Date] IN (select distinct [Date] from aux.TempDespachosCD)
	
	-- insertamos en la tabla destino, en este caso, BaseCalculoReposicion
	INSERT INTO [series].[DespachosCD]
           ([Date],[Store],[SKU],[Bodega],[Sugerido],[Solicitado],[Corte],[Picking],[Disponible],[Pedido],[DisponibleSugerido])
	select [Date],[Store],[SKU],[Bodega],[Sugerido],[Solicitado],[Corte],[Picking],[Disponible],[Pedido],[DisponibleSugerido]
	from aux.TempDespachosCD

	-- limpiamos la tabla de staging
	TRUNCATE TABLE aux.TempDespachosCD

END
