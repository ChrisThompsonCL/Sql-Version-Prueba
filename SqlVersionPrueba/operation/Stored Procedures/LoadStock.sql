﻿-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 05-01-2011
-- Description:	Actualiza la última foto de Stock en ReportStores.
-- =============================================
CREATE PROCEDURE [operation].[LoadStock] 
	@today smalldatetime = NULL
AS
BEGIN
	truncate table [reports].[LastReportStores]

	-- Se insertan las nuevas combianciones del día @today, desde LASTSTOCK
	INSERT INTO [reports].[LastReportStores]
           ([Date]
           ,[SKU]
           ,[Store]
           ,[Stock]
           ,[Suggest]
           ,[Quantity]
           ,[Income]
           --,[SPrice]
           ,[Cost]
           ,[ZeroStockCD]
           ,[VE]
           ,[LostSale]
           ,[MotiveId]
           ,[IsBloqued]
           ,[MixActive]
           ,[Transit]
           ,[Min]
           ,[Max]
           ,[HasToPickUp]
           --,[PickedUnits]
           ,[LostSale1]
           ,[LostSale2]
		   , [Days]
		   , LeadTime
			, ServiceLevel
			, Rate
			, SafetyDays
			, R_Minimo
			, R_Fijo
			, R_Planograma
			, R_MedioDUN
			, R_Maximo
			, R_CajaTira
			, R_FijoEstricto
			, SentSuggest
			, PricingMin
			, PricingMax)
--declare 	@today date = '2018-02-04'
	select 
		ls.[Date]
		,ls.SKU
		,ls.Store
		,Stock=case when ls.Stock >= 0 then ls.Stock else 0 end
		,ls.Suggest
		,Quantity=0
        ,Income=0
		,Cost=pc.Cost
		,[ZeroStockCD]= case when cd.sku is null then 0 else 1 end
		,[VE]=0
		,LostSale=0
		,p.MotiveId
		,ls.IsBloqued
		,[MixActive]= case when d.sku is not null then 1 else 0 end         
		,ls.Transit
		,ls.[Min]
		,ls.[Max]
		,[HasToPickUp]= case when r.Store is not null and (ls.Stock+ls.Transit) < ls.[Min] then 1 else 0 end
		,[LostSale1]=0        
		,[LostSale2]=0
		
		--, lps.[Days]
		,isnull (lps.[Days], ors.[Days])
		--, lps.LeadTime
		,isnull (lps.LeadTime, ors.Leadtime)
		--, lps.ServiceLevel
		,isnull (lps.ServiceLevel, ors.ServiceLevel) ServiceLevel
		--, lps.Rate
		,isnull (lps.Rate, ors.Rate) 
		--, lps.SafetyDays
		,isnull (lps.SafetyDays, ors.SafetyDays) 
		
		, isnull(sd.Minimo, lsr.Minimo) R_Minimo
		, isnull(sd.Fijo, lsr.Fijo) R_Fijo
		, isnull(sd.Planograma, lsr.Planograma) R_Planograma
		, isnull(sd.MedioDUN, lsr.MedioDUN) R_MedioDUN
		, isnull(sd.Maximo, lsr.Maximo) R_Maximo
		, isnull(sd.CajaTira, lsr.CajaTira) R_CajaTira
		, isnull(sd.FijoEstricto, lsr.FijoEstricto) R_FijoEstricto
		, case when sd.[Max] is not null then 1 else 0 end SentSuggest
		, sd.[Min] PricingMin
		, sd.[Max] PricingMax
	from series.LastStock ls
	left join [aux].DailySuggestMix d on d.SKU=ls.SKU and d.Store=ls.Store
	left join products.products p on p.SKU=ls.SKU
	left join (select * from minmax.Replenishment where [Days]>0 and [date]=@today) r on r.Store=ls.Store and r.LogisticAreaId=p.LogisticAreaId
	left join pricing.LastPriceAndCost pc on pc.SKU=ls.SKU
	left join minmax.LastPricingSuggest lps on lps.SKU=ls.SKU and lps.Store=ls.Store and ls.[Date]=lps.[Date]
	left join minmax.LastSuggestRestriction lsr on lsr.SKU=ls.sku and lsr.Store=ls.store
	left join suggest.SuggestDownload sd on sd.SKU=ls.SKU and sd.Store=ls.Store
	left join (select * from reports.ReportStores where [Date]=DATEADD(day ,-1, @today)) ors on ors.SKU=ls.SKU and ls.Store=ors.Store
	left join (
		select distinct sku 
		from (
			select p.SKU,cd.Date,cd.StockCD 
			from (
				select p.SKU,isnull(e.SKU,p.sku) skuKey 
				from products.FullProducts p
				left outer join products.ProductsEquivalence e on p.SKU = e.SKU_Child
			) p 
			inner join series.StockCD cd on cd.SKU = p.skuKey 
			where cd.Date between DATEADD(day,-8,@today) and DATEADD(day,-1,@today)
		) h where h.StockCD<=0
	) cd on cd.SKU=ls.SKU
	where (ls.Stock >0 or ls.Suggest >0)


	insert into reports.ReportStores
	select * from reports.LastReportStores

	delete from [reports].[MetricasStocks] where [date] = @today

	insert into [reports].[MetricasStocks]
--declare 	@today date = '2017-06-21'
	select ls.SKU
		,ls.[Date]
		,Stock=SUM(case when ls.Stock >= 0 then ls.Stock else 0 end)
		,Suggest=sum(ls.Suggest)
		,SugeridoModelo=sum(case when d.sku is not null then ls.Suggest else 0 end)
		,StockAjustadoSugerido=sum(case when ls.stock> ls.suggest then ls.Suggest else ls.stock end)
		,Locales=sum(case when suggest>0 then 1 else 0 end)
		,localesQuebrados=sum(case when suggest>0 and stock <=0 then 1 else 0 end) 
		,LocalesModelo=sum(case when d.sku is not null and suggest>0 then 1 else 0 end)
		,localesQuebradosModelo=sum(case when d.sku is not null and suggest>0 and stock <=0 then 1 else 0 end) 
		,localesQuebradosConStockCD=sum(case when suggest>0 and stock <=0 and cd.sku is null then 1 else 0 end) 
		,LocalesQuebradosSinFallaProveedor=sum(case when suggest>0 and stock <=0 and MotiveId<>90  then 1 else 0 end) 
		,StockTransito = SUM(case when Transit>0 then Transit else 0 end)
		,StockTransitoAjustado = SUM(case when Transit>0 then (case when stock+transit < suggest then transit when Stock+Transit>Suggest and Stock < Suggest then Suggest-Stock else 0 end ) else 0 end)
	from series.LastStock ls
	left join [aux].DailySuggestMix d on d.SKU=ls.SKU and d.Store=ls.Store
	left join products.products p on p.SKU=ls.SKU
	--left join (select * from minmax.Replenishment where [Days]>0 and [date]=@today) r on r.Store=ls.Store and r.LogisticAreaId=p.LogisticAreaId
	left join pricing.LastPriceAndCost pc on pc.SKU=ls.SKU
	--left join series.LastSale sale on sale.SKU=ls.sku and sale.Store=ls.store
	left join (
		select distinct sku 
		from (
			select p.SKU,cd.Date,cd.StockCD 
			from (
				select p.SKU,isnull(e.SKU,p.sku) skuKey 
				from products.FullProducts p
				left outer join products.ProductsEquivalence e on p.SKU = e.SKU_Child
			) p 
			inner join series.StockCD cd on cd.SKU = p.skuKey 
			where cd.Date between DATEADD(day,-8,@today) and DATEADD(day,-1,@today)
		) h where h.StockCD<=0
	) cd on cd.SKU=ls.SKU
	where (ls.Stock >0 or ls.Suggest >0)
	group by ls.SKU, ls.[Date]


	delete from [series].[Stock-outs] where [date] = @today
	
	insert into [series].[Stock-outs]
	SELECT [SKU],[Store],[Date],[Stock]
	FROM series.LastStock
	where stock < 1
 

END



