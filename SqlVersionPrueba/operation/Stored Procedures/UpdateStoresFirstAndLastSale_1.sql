﻿-- =============================================
-- Author:	Juan Pablo Cornejo
-- Create date: 2011-04-05
-- Description:	Actualiza las tablas de [stores].[FullStores] y Stores que no tengan First ni Last Sale.
-- =============================================
CREATE PROCEDURE [operation].[UpdateStoresFirstAndLastSale] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	CREATE TABLE #aux (Store int, MinDate date, MaxDate date)
	CREATE INDEX #IDX1 ON #aux(Store) include (MinDate, MaxDate)

	INSERT INTO #aux
	SELECT Store, MIN(Date) ,MAX(Date)
	from [series].[Sales] 
	GROUP BY Store

	--[stores].[FullStores]
	UPDATE s SET s.FirstSale = case when s.FirstSale is null then Sales.MinDate else s.FirstSale end, s.LastSale=Sales.MaxDate 
	FROM [stores].[FullStores] s 
	inner join #aux as Sales on sales.Store = s.Store


	--[stores].[Stores]
	UPDATE s SET s.FirstSale = case when s.FirstSale is null then Sales.MinDate else s.FirstSale end, s.LastSale=Sales.MaxDate 
	FROM [stores].Stores s 
	inner join #aux as Sales on sales.Store = s.Store

	drop table #aux
END
