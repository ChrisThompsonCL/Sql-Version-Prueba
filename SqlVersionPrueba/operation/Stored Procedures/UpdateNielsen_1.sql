﻿



-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-02-13
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateNielsen] 
	-- Add the parameters for the stored procedure here	
AS
BEGIN


	delete from series.Nielsen where semana in (select distinct semana from [aux].[TempNielsen])

	insert into series.Nielsen ([Semana]
		  ,[SKU]
		  ,[SKUDesc]
		  ,[UnidadesSB]
		  ,[ValoresSB]
		  ,[UnidadesPU]
		  ,[ValoresPU]
		  ,[UnidadesCadena]
		  ,[ValoresCadena])
	SELECT [Semana]
		  ,[SKU]
		  ,[SKUDesc]
		  ,[UnidadesSB]
		  ,[ValoresSB]
		  ,[UnidadesPU]
		  ,[ValoresPU]
		  ,[UnidadesCadena]
		  ,[ValoresCadena]
	  FROM [aux].[TempNielsen]

	  truncate table [aux].[TempNielsen]

END


