﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-07-03
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateDistributionAreas] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	truncate table [products].[DistributionAreas]

	insert into [products].[DistributionAreas] (SKU, DistributionArea)
	select distinct SKU, DistributionArea
	from [aux].[TempDistributionAreas]

	truncate table [aux].[TempDistributionAreas]

END
