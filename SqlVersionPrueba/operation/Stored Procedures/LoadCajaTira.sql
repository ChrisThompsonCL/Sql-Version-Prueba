﻿
-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-03-26
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[LoadCajaTira]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE products.CajaTira

	INSERT INTO products.CajaTira
	SELECT SKU1 SKUCaja, SKU2 SKUTira, Conversion, [Date] RefDate
	FROM [aux].[TempCajaTira]
	WHERE [Type]='CAJA'

END
