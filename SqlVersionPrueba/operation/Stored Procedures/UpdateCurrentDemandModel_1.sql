﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateCurrentDemandModel]
AS
BEGIN	
	
	SET NOCOUNT ON;
	
-- Insertamos los productos que cumplan con las condiciones para realizar forecast.
INSERT INTO [OPRSalcobrand].[models].[CurrentDemandModel]
           ([ProductId]
           ,[ProductType]
           ,[StoreId]
           ,[StoreType]
           ,[TimeAggregation]
           ,[DemandModelId]
           ,[NeedUpdate]
           ,[EvaluationCriteriaId])
		select SKU,0,1,3,1,-1,1,0 from products.Products p
		left join [models].[CurrentDemandModel] cm on p.SKU = cm.ProductId
		where FirstSale<dateadd(week,-24,GETDATE()) and cm.ProductId is null
		

INSERT INTO [OPRSalcobrand].[models].[CurrentMethodology]
           ([ProductId]
           ,[ProductType]
           ,[StoreId]
           ,[StoreType]
           ,[TimeAggregation]
           ,[MethodologyId])
           
		select SKU,0,1,3,1,1 from products.Products p
		left join [models].[CurrentMethodology] cm on p.SKU = cm.ProductId
		where FirstSale<dateadd(week,-24,GETDATE()) and cm.ProductId is null


	
END
