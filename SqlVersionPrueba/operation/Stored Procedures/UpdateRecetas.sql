﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-03-12
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateRecetas] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE [aux].[TempRecetas]
	SET Store = NULL
	WHERE Store = 0 

	DELETE FROM series.Recetas WHERE [Date] IN (SELECT DISTINCT [Date] FROM [aux].[TempRecetas])

	INSERT INTO series.Recetas
	SELECT *
	FROM [aux].[TempRecetas]
	WHERE Store > 0

	TRUNCATE TABLE [aux].[TempRecetas]


END
