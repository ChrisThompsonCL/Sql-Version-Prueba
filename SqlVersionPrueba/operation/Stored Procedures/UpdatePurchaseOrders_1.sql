﻿
-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-03-16
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdatePurchaseOrders]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE SalcobrandAbastecimiento.series.PurchaseOrders

	INSERT INTO SalcobrandAbastecimiento.series.PurchaseOrders
	SELECT *
	FROM [aux].[TempPurchaseOrders]

END
