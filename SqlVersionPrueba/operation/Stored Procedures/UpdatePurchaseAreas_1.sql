﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-07-03
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdatePurchaseAreas] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table [products].[PurchaseAreas]

	insert into [products].[PurchaseAreas] (SKU, WMSZone)
	select distinct SKU, WMSZone
	from [aux].[TempPurchaseAreas]


	truncate table [aux].[TempPurchaseAreas]

END
