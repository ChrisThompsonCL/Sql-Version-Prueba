﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-06-11
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[LoadMatricesCosto] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--datos de cabecera
	truncate table SalcobrandAbastecimiento.setup.PriceMatrix

	insert into SalcobrandAbastecimiento.setup.PriceMatrix
	SELECT cast(col2 as int) as Organization, cast(col3 as int) as ManufacturerId, cast(Col4 as int) as MatrixId, Col7 as MatrixName
		, convert(date,Col5,103) as CreatedDate, Col6 as CreatedBy, convert(date,Col8,103) as UpdatedDate, Col9 as UpdatedBy 
		, Col10 as Currency, Col11 as ExchangeType, Col12 as [ComprasAMostrarExcepcion]
		, Col13 as Zona, convert(date,Col14,103) as StartingDate
		, number
	from (
		select * , row_number()  OVER (ORDER BY (SELECT NULL)) as number
		FROM [aux].[TempMatricesCosto]
	) a
	WHERE Col1='c'
		and cast(col2 as int)=1

	--datos de detalle
	truncate table SalcobrandAbastecimiento.setup.PriceMatrixDetail

	insert into SalcobrandAbastecimiento.setup.PriceMatrixDetail
		([Organization]
           ,[ManufacturerId]
           ,[MatrixId]
           ,[SKU]
           ,[IsConsigned]
           ,[ListPrice]
           ,[DiscountFact1]
           ,[DiscountFact2]
           ,[DiscountFact3]
           ,[DiscountFact4]
           ,[DiscountFact5]
           ,[DiscountFact6]
           ,[DiscountFact7]
           ,[DiscountFact8]
           ,[DiscountNC1]
           ,[DiscountNC2]
           ,[DiscountNC3]
           ,[DiscountNC4]
           ,[DiscountFactMarketing1]
           ,[DiscountFactMarketing2]
           ,[DiscountFactMarketing3]
           ,[DiscountFactMarketing4]
           ,[DiscountProntoPago]
		   ,[IdZona])
	select a.Organization, a.ManufacturerId, a.MatrixId, SKU, IsConsigned, ListPrice
			, DiscountFact1, DiscountFact2, DiscountFact3, DiscountFact4, DiscountFact5, DiscountFact6, DiscountFact7, DiscountFact8
			, DiscountNC1, DiscountNC2, DiscountNC3, DiscountNC4, DiscountFactMarketing1, DiscountFactMarketing2, DiscountFactMarketing3, DiscountFactMarketing4, DiscountProntoPago
			, pm.[IdZona]
	from (
		select a.Organization, a.ManufacturerId, a.MatrixId, SKU, IsConsigned, ListPrice
			, DiscountFact1, DiscountFact2, DiscountFact3, DiscountFact4, DiscountFact5, DiscountFact6, DiscountFact7, DiscountFact8
			, DiscountNC1, DiscountNC2, DiscountNC3, DiscountNC4, DiscountFactMarketing1, DiscountFactMarketing2, DiscountFactMarketing3, DiscountFactMarketing4, DiscountProntoPago, number
			, Max(PosIndex)PosIndex
		from (
			select cast(col2 as int) as Organization, cast(col3 as int) as ManufacturerId, cast(Col4 as int) as MatrixId
				,cast(col5 as int) SKU, col6 as IsConsigned, cast(Col7 as float) as ListPrice
				, cast(col8 as float) as DiscountFact1, cast(Col9 as float) as DiscountFact2, cast(Col10 as float) as DiscountFact3, Col11 as DiscountFact4
				, cast(Col12 as float) as DiscountFact5, cast(Col13 as float) as DiscountFact6, cast(Col14 as float) as DiscountFact7, Col15 as DiscountFact8
				, cast(col16 as float) as DiscountNC1, cast(col17 as float) as DiscountNC2, cast(col18 as float) as DiscountNC3, Col19 as DiscountNC4
				, cast(col20 as float) as DiscountFactMarketing1, cast(col21 as float) as DiscountFactMarketing2, cast(col22 as float) as DiscountFactMarketing3, cast(Col23 as float) as DiscountFactMarketing4
				, cast(col24 as float) as DiscountProntoPago
				, number --select*
			from (
				select * , row_number()  OVER (ORDER BY (SELECT NULL)) as number
				FROM [aux].[TempMatricesCosto]
			) a
			WHERE Col1='d' and cast(col2 as int)=1
		) a
		inner join (
			select Organization, ManufacturerId, MatrixId, [IdZona], PosIndex 
			from SalcobrandAbastecimiento.setup.PriceMatrix
		) j on j.Organization=a.Organization and j.ManufacturerId=a.ManufacturerId and j.MatrixId=a.MatrixId and j.PosIndex<a.number
		group by a.Organization, a.ManufacturerId, a.MatrixId, SKU, IsConsigned, ListPrice
			, DiscountFact1, DiscountFact2, DiscountFact3, DiscountFact4, DiscountFact5, DiscountFact6, DiscountFact7, DiscountFact8
			, DiscountNC1, DiscountNC2, DiscountNC3, DiscountNC4, DiscountFactMarketing1, DiscountFactMarketing2, DiscountFactMarketing3, DiscountFactMarketing4, DiscountProntoPago, number
	) a 
	inner join SalcobrandAbastecimiento.setup.PriceMatrix pm on pm.PosIndex=a.PosIndex

	--select ManufacturerId, SKU, count(*)
	--from SalcobrandAbastecimiento.setup.PriceMatrixDetail
	--group by ManufacturerId, SKU having count(*) > 1
END
