﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-05-28
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateStockTarde]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	truncate table replenishment.[StockTarde]

	insert into replenishment.[StockTarde] ([SKU],[Store],[Date],[Stock],[Transit],[Min],[Max],[Suggest],[IsBloqued] )
	select [SKU],[Store],[Date],[Stock],[Transit],[Min],[Max],[Suggest],[IsBloqued] 
	from [aux].[TempStockTarde]

END
