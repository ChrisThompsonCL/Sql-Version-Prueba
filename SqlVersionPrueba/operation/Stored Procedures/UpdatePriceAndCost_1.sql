﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-01-23
-- Description:	Carga archivo PrecioCosto que se encuentre en tabla aux
-- =============================================
CREATE PROCEDURE [operation].[UpdatePriceAndCost] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--corrige error de notación decimal
	update a set Price= REPLACE(Price, ',', '.'), Cost=REPLACE(Cost, ',', '.')
	from [aux].[PriceAndCost] a
	
	--borra datos ya cargados para las nuevas fechas
	delete from series.PriceAndCost
	where [Date] in (select distinct CONVERT(date, [date], 105) from aux.PriceAndCost)

	--carga datos nuevos a la tabla de series
	insert into series.PriceAndCost([date], SKU, Motive, Cost, Price)
	SELECT CONVERT(date, [date], 105),[SKU],MAX([Motive])Motive,[Cost],[Price]
	FROM [Salcobrand].[aux].[PriceAndCost]
	GROUP BY [Date], SKU, Cost, Price

	--incluye IVA en costo para que se pueda comparar con el precio
	update series.PriceAndCost
	set Cost= Cost*1.19, CostWithIVA=1 --select count(*) from series.PriceAndCost
	WHERE [CostWithIVA]=0

	-- Borramos la tabla [pricing].[LastPriceAndCost].
	truncate table [pricing].[LastPriceAndCost]

	-- Obtenemos la última fecha donde hayan datos en [series].[PriceAndCost].
	declare  @date date = (select MAX(date) from [series].[PriceAndCost])
	
	-- Copiamos la información del último día.
	INSERT INTO [pricing].[LastPriceAndCost] ([SKU],[Date], Motive,[Price],[Cost], [CostWithIVA] )
	SELECT [SKU],[Date], Motive,[Price],[Cost],[CostWithIVA] 
	FROM [series].[PriceAndCost] 
	where Date = @date


	TRUNCATE TABLE aux.PriceAndCost


END
