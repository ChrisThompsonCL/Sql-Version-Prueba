﻿

-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2011-04-05
-- Description:	Actualiza la tabla [series].[CompetitorsDailyPrice] con los datos cargados anteriormente en [aux].[AuxCompetitorsPrice]. También actualiza [pricing].[LastCompetitorsPrice]
-- =============================================
CREATE PROCEDURE [operation].[UpdateCompetitorsPrice]
	@date smalldatetime

	
AS
BEGIN
DECLARE @lastDate smalldatetime 


-- Primero, rellenamos todas las fechas desde la última fecha establecida hasta la indicada.
	select @date = max([date]) from aux.AuxCompetitorsPrice
	select @lastDate= MAX(date) from [series].[CompetitorsDailyPrice]

	if (@lastDate < @date)
	begin
		
		insert into [series].[CompetitorsDailyPrice] ([SKU],[Date],[Fasa],[CV], [Simi],[UpdatedFasa],[UpdatedCV], [UpdatedSimi])
		SELECT data.SKU ,dates.[Date],[Fasa] ,[CV],[Simi],0,0,0 
		FROM ( 
			select isnull(lcp.[SKU],b.SKU) SKU, Fasa, CV, Simi
			from (
				SELECT [SKU],[Fasa],[CV], Simi 
				FROM series.CompetitorsDailyPrice
				WHERE [Date] = @lastDate
			) lcp
			full join (
				select distinct SKU 
				from [aux].[AuxCompetitorsPrice] 
				where (Fasa is not null) or (CV is not null) or (Simi is not null)
			) b on lcp.SKU = b.SKU
		) data 
		cross join (
			SELECT fecha [Date]
			FROM forecast.GenericVariables 
			WHERE fecha between DATEADD(DAY,1,@lastDate) and @date		
		) dates 

	end

-- Actualizamos las cotizaciones de Fasa mayores a 0.
update cp
set Fasa = b.Fasa, [UpdatedFasa] = 1 -- DECLARE @date smalldatetime = '2011-03-30' select *, cp.Fasa-b.FAsa
from [series].[CompetitorsDailyPrice] cp
inner join (
select SKU, Fasa from [aux].[AuxCompetitorsPrice] a where DATE = @date and Fasa is not NULL and Fasa > 0
) b on cp.SKU = b.SKU 
where DATE = @date

-- Actualizamos las cotizaciones de CV mayores a 0.
update cp
set CV = b.CV, [UpdatedCV] = 1
from [series].[CompetitorsDailyPrice] cp
inner join (
select SKU, CV from [aux].[AuxCompetitorsPrice] where DATE = @date and CV is not NULL and CV > 0
) b on cp.SKU = b.SKU 
where DATE = @date

-- Actualizamos las cotizaciones de Simi mayores a 0.
update cp
set Simi = b.Simi, [UpdatedSimi] = 1
from [series].[CompetitorsDailyPrice] cp
inner join (
select SKU, Simi from [aux].[AuxCompetitorsPrice] where DATE = @date and Simi is not NULL and Simi > 0
) b on cp.SKU = b.SKU 
where DATE = @date

-- limpiar datos basura
delete from [series].[CompetitorsDailyPrice] where Fasa is null and CV is null and Simi is null

-- replicar cotizaciones x familia
update pc
	set Fasa= isnull(cotfamilia.Fasa, pc.Fasa)
	, CV = isnull(cotfamilia.CV, pc.CV)
	, Simi = isnull(cotfamilia.Simi, pc.Simi)
	, UpdatedFasa = cotfamilia.UpdatedFasa
	, UpdatedCV = cotfamilia.UpdatedCV
	, UpdatedSimi = cotfamilia.UpdatedSimi
--select *
from temp.Familia f
inner join series.CompetitorsDailyPrice pc on pc.SKU=f.SKU
inner join (
	select f.[Familia-Variedad], pc.[Date]
		, MAX(case when UpdatedFasa>0 then Fasa else null end)Fasa
		, MAX(case when UpdatedCV>0 then CV else null end)CV
		, MAX(case when UpdatedSimi>0 then Simi else null end)Simi
		, MAX(case when UpdatedFasa=1 then 1 else 0 end)UpdatedFasa
		, MAX(case when UpdatedCV=1 then 1 else 0 end)UpdatedCV
		, MAX(case when UpdatedSimi=1 then 1 else 0 end)UpdatedSimi
	from temp.Familia f
	inner join series.CompetitorsDailyPrice pc on pc.SKU=f.SKU
	where [Familia-Variedad] in (
		select [Familia-Variedad] R
		from [temp].[Familia] f
		group by [Familia-Variedad]
		having count(distinct SKU) > 1
	)
	and pc.[Date] = @date
	and (pc.UpdatedCV=1 OR pc.UpdatedFasa=1 OR pc.UpdatedSimi=1)
	group by f.[Familia-Variedad],[Date]
) cotfamilia on cotfamilia.[Familia-Variedad]=f.[Familia-Variedad] and cotfamilia.[Date]=pc.[Date]


insert into series.CompetitorsDailyPrice
select f.SKU, cotfamilia.[Date], cotfamilia.Fasa, cotfamilia.CV, cotfamilia.Simi, cotfamilia.UpdatedFasa, cotfamilia.UpdatedCV, cotfamilia.UpdatedSimi
from temp.Familia f
left join series.CompetitorsDailyPrice pc on pc.SKU=f.SKU
inner join (
	select f.[Familia-Variedad], pc.[Date]
		, MAX(case when UpdatedFasa>0 then Fasa else null end)Fasa
		, MAX(case when UpdatedCV>0 then CV else null end)CV
		, MAX(case when UpdatedSimi>0 then Simi else null end)Simi
		, MAX(case when UpdatedFasa=1 then 1 else 0 end)UpdatedFasa
		, MAX(case when UpdatedCV=1 then 1 else 0 end)UpdatedCV
		, MAX(case when UpdatedSimi=1 then 1 else 0 end)UpdatedSimi
	from temp.Familia f
	inner join series.CompetitorsDailyPrice pc on pc.SKU=f.SKU
	where [Familia-Variedad] in (
		select [Familia-Variedad]
		from [temp].[Familia] f
		group by [Familia-Variedad]
		having count(distinct SKU) > 1
	)
	and pc.[Date] = @date
	and (pc.UpdatedCV=1 OR pc.UpdatedFasa=1 OR pc.UpdatedSimi=1)
	group by f.[Familia-Variedad],[Date]
) cotfamilia on cotfamilia.[Familia-Variedad]=f.[Familia-Variedad] 
where cotfamilia.[Date] = @date and pc.SKU is null

-- Actualizamos [pricing].[LastCompetitorsPrice] 
declare @cotsObsoletas date = dateadd(MONTH,-6, @date)
 
truncate table [pricing].[LastCompetitorsPrice]

insert into [pricing].[LastCompetitorsPrice] (SKU, [Date], Fasa, CV, Simi, LastUpdateFasa, LastUpdateCV, LastUpdateSimi)
 SELECT COALESCE(cotsFasa.SKU, cotsCV.SKU, cotsSimi.SKU) SKU, @date [Date], Fasa, CV, Simi, LastFasa, LastCV, LastSimi
 FROM (
	 select  SKU, Fasa, [Date] LastFasa
	 from (
		 select SKU, Fasa, [Date], ROW_NUMBER() over (partition by SKU order by [Date] desc) R
		 from [series].[CompetitorsDailyPrice]
		 where UpdatedFasa=1 and [date]> @cotsObsoletas
	 ) cotsFasa where R=1
) cotsFasa 
full outer join (
	 select  SKU, CV, [Date] LastCV
	 from (
		 select SKU, CV, [Date], ROW_NUMBER() over (partition by SKU order by [Date] desc) R
		 from [series].[CompetitorsDailyPrice]
		 where UpdatedCV=1 and [date]> @cotsObsoletas
	 ) cotsCV where R=1
) cotsCV on cotsCV.SKU=cotsFasa.SKU
full outer join (
	 select  SKU, Simi, [Date] LastSimi
	 from (
		 select SKU, Simi, [Date], ROW_NUMBER() over (partition by SKU order by [Date] desc) R
		 from [series].[CompetitorsDailyPrice]
		 where UpdatedSimi=1 and [date]> @cotsObsoletas
	 ) cotsSimi where R=1
) cotsSimi on CotsSimi.SKU=isnull(cotsFasa.SKU, cotsCV.SKU)

update pricing.LastCompetitorsPrice set fasa=null where fasa is not null and LastUpdateFasa is null
update pricing.LastCompetitorsPrice set cv=null where cv is not null and LastUpdatecv is null
update pricing.LastCompetitorsPrice set Simi=null where Simi is not null and LastUpdateSimi is null

delete from pricing.LastCompetitorsPrice where Fasa is null and CV is null and Simi is null

END
