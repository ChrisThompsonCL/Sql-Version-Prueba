﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-06-11
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateDailyPurchaseOrders] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	TRUNCATE TABLE SalcobrandAbastecimiento.series.DailyPurchaseOrders

	INSERT INTO SalcobrandAbastecimiento.series.DailyPurchaseOrders
	SELECT *
	FROM [aux].[TempDailyPurchaseOrders]

END
