﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: <2018-02-08>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateMissingUnitsBook]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM [series].[MissingUnitsBook] WHERE [date] in (
		SELECT distinct [Date]
		FROM aux.TempMissingUnitsBook
	)

	INSERT INTO [series].[MissingUnitsBook] (Store, SKU, [Date], RUT, [Type], TypeDesc, Times)
	SELECT Store, SKU, [Date], RUT, [Type], TypeDesc, COUNT(*) Times
	FROM aux.TempMissingUnitsBook
	GROUP BY Store, SKU, [Date], RUT, [Type], TypeDesc

	TRUNCATE TABLE aux.TempMissingUnitsBook


END
