﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[CleanHistoryDB]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	DECLARE @today date = GETDATE()
	DECLARE @minDate date, @maxDate date
	DECLARE @msg NVARCHAR(100) 

	SET NOCOUNT ON;
	
	-- información previa de alzas de feriado guarda solo fechas futuras
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla HolidayAuxData y Rates de feriado guardan solo información futura' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    SET @minDate = DATEADD(day,1,@today)
    
    DELETE FROM [minmax].[HolidayAuxData] WHERE [HolidayDate] < @minDate
    DELETE FROM [minmax].HolidaysRates WHERE [Date] < @minDate
    
    ---- información para cálculo de compras guarda solo fechas futuras
    --SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla RatesCurrent guarda solo información futura' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    --SET @minDate = dbo.GetMonday(@today)
    
    --DELETE FROM dcpurchase.RatesCurrent WHERE [Date] < @minDate

	-- análisis restricciones no cargadas guarda una semana
    SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla [aux].[SuggestRestrictionDetail] guarda una semana de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    SET @minDate = dateadd(day, -7, @today)

	DELETE FROM [aux].[SuggestRestrictionDetail] WHERE [InsertionDate] < @mindate
	    
    -- historia de dailysuggest
    SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla TruncateLowSaleHistory y ShareStoreHistory guardan un mes de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    SET @minDate = DATEADD(MONTH, -1, @today)
    
    DELETE FROM minmax.TruncateLowSaleHistory WHERE [Date] < @minDate
	--DELETE FROM forecast.ShareStoreHistory WHERE [Week] < @minDate

    --alzas aprobadas guardan dos meses
    SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas HolidaysApprovedSuggests y HolidaySuggestNew guardan 2 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = @today
    SET @minDate = DATEADD(MONTH, -2, @maxDate)
    
    DELETE FROM [minmax].[HolidayApprovedSuggests] WHERE [HolidayDate] < @minDate
    DELETE FROM [minmax].[HolidaySuggestNew] WHERE [HolidayDate] < @minDate
	DELETE FROM [minmax].[HolidayIncreaseParams] WHERE [HolidayDate] < @minDate

	-- venta boleta guarda 6 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla TicketSales guarda 6 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = (select max([Date]) from series.TicketSales)
    SET @minDate = DATEADD(MONTH, -6, @maxDate)
    
    DELETE FROM [series].[TicketSales] WHERE [Date] < @minDate
    
    -- cálculo reposiciones guarda un mes
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas [minmax].[Replenishment] guarda 1 mes de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = @today
    SET @minDate = DATEADD(MONTH, -1, @maxDate)
    
    DELETE FROM [minmax].[Replenishment] WHERE [Date] < @minDate

    -- LOGS guardan tres meses de historia
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas Logs guardan 3 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    
    SET @maxDate = @today
    SET @minDate = DATEADD(MONTH, -3, @maxDate)
    
    DELETE FROM [operation].[ExecutionLog] WHERE [Date] < @minDate
    DELETE FROM [operation].[UserLog] WHERE [Date] < @minDate
    DELETE FROM [robot].[Log] WHERE [Date] < @minDate
    DELETE FROM [robot].[ProcLog] WHERE [Date] < @minDate
    
 --   -- Propuestas de Precios guardan 2 meses
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas pricing y LastPrice guardan 2 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
    
 --   SET @maxDate = DATEADD(day, -1, (SELECT MAX(DateKey) FROM pricing.FinalPricing))
 --   SET @minDate = DATEADD(MONTH, -2, @maxDate)
    
    -- Respaldo de propuestas en tabla histórica
 --   INSERT INTO [pricing].[ScenariosHistory] 
	--	([DateKey],[SKU],[Cluster]
	--	,[Actual_Price],[Actual_Forecast],[Actual_Sale],[Actual_Contribution],[Actual_Margin]
	--	,[Best_Price],[Best_Forecast],[Best_Sale],[Best_Contribution],[Best_Margin],[Best_Bound]
	--	,[Final_Price],[Final_Forecast],[Final_Sale],[Final_Contribution],[Final_Margin])
	--SELECT  actual.DateKey, actual.[SKU], actual.Cluster
	--	, actual.[Price], actual.[Forecast], actual.[Sale], actual.[Contribution] , actual.[Margin] 
	--	, best.[Price], best.[Forecast], best.[Sale], best.[Contribution], best.[Margin], best.[Bound] 
	--	, final.[Price], final.[Forecast], final.[Sale], final.[Contribution], final.[Margin]
	--FROM pricing.[ActualScenario] actual 
	--INNER JOIN pricing.[BestScenario] best ON actual.DateKey = best.DateKey and actual.SKU = best.SKU and actual.Cluster = best.Cluster 
	--INNER JOIN pricing.FinalPricing final ON actual.DateKey = final.DateKey and actual.SKU = final.SKU and actual.Cluster = final.Cluster 
	--WHERE actual.DateKey > (SELECT MAX(DateKey) FROM [pricing].[ScenariosHistory])
    
    --DELETE FROM [pricing].ActualScenario WHERE DateKey < @minDate
    --DELETE FROM [pricing].BestScenario WHERE DateKey < @minDate
    --DELETE FROM [pricing].FinalPricing WHERE DateKey < @minDate
    --DELETE FROM [pricing].DefaultPrices WHERE EndDate < @minDate OR CancelDate < @minDate
    --DELETE FROM [pricing].OutputOptimizationDetail WHERE Id in (SELECT Id FROM [pricing].OutputOptimization WHERE CreationDate < @minDate)
    --DELETE FROM [pricing].OutputOptimization WHERE CreationDate < @minDate
	--DELETE FROM [pricing].LastPrice WHERE [Date] < @minDate

	--Suggest Download guarda 6 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas Suggest Download guarda 6 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = @today
	SET @minDate = dbo.getmonday(DATEADD(MONTH, -6, @maxDate))

	DELETE FROM suggest.SuggestDownloadHistory WHERE [Date] < @minDate

	--Suggest Download guarda 6 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas GenericRerpot guarda 4 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = @today
	SET @minDate = dbo.getmonday(DATEADD(MONTH, -4, @maxDate))

	DELETE FROM reports.GenericReportStores WHERE [Date] < @minDate
	
	--restricciones sugeridos guardan 3 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Restricciones sugeridos guardan 3 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = @today
	SET @minDate = dbo.getmonday(DATEADD(MONTH, -3, @maxDate))
	
	delete from br.SuggestRestrictionFile where SuggestRestrictionFileID not in (
	select distinct SuggestRestrictionFileID from br.SuggestRestrictionDetail
	where EndDate >= @minDate or EndDate is null)

	delete from br.SuggestRestrictionDetail where SuggestRestrictionFileID not in (
	select distinct SuggestRestrictionFileID from br.SuggestRestrictionDetail
	where EndDate >= @minDate or EndDate is null)

	--Historias de Precio guardan 36 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla histórica de precios guarda 36 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = (SELECT MAX([Date]) FROM series.CompetitorsDailyPrice)
	SET @minDate = DATEADD(MONTH, -36, @maxDate)
    
	DELETE FROM series.CompetitorsDailyPrice WHERE [Date] < @minDate
	DELETE FROM series.PriceAndCost WHERE [Date] < @minDate
	--DELETE FROM pricing.ScenariosHistory WHERE [DateKey] < @minDate
	DELETE FROM pricing.LastCompetitorsPrice WHERE LastUpdateFasa is null and LastUpdateCV is null and LastUpdateSimi is null
	--DELETE FROM aux.PriceAndCost WHERE [Date] < @minDate
	
	-- Predicciones  guardan 6 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla predicciones de venta guarda 6 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	SET @maxDate = dbo.GetMonday((SELECT MAX(DateKey) FROM forecast.PredictedDailySales))
	SET @minDate = DATEADD(MONTH, -6, @maxDate)

	DELETE FROM forecast.PredictedDailySales WHERE DateKey < @minDate
	
	-- Para predicciones dejo la última semana de cálculo de cada día
	DELETE ps FROM forecast.PredictedDailySales ps 
	LEFT JOIN (
		SELECT [Date], MAX(DateKey) MaxDateKey
		FROM forecast.PredictedDailySales 
		WHERE DateKey BETWEEN @minDate AND DATEADD(WEEK,-5,@maxDate) 
		GROUP BY [Date]
	) ds ON ps.DateKey = ds.MaxDateKey AND ps.[Date] = ds.[Date]
	WHERE ps.DateKey BETWEEN @minDate AND DATEADD(WEEK,-5,@maxDate) AND ds.MaxDateKey IS NULL

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla predicciones estandarización guarda un mes' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = (select max(FechaCreacion) from [Temporal].[STD_Pronosticos].[Salcobrand])
	SET @minDate = dateadd(month, -1, @maxDate)

	DELETE FROM [Temporal].[STD_Pronosticos].[Salcobrand] WHERE FechaCreacion<@minDate
	
	-- Historia CD  (compras) guardan 6 meses
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas de Historia CD guardan 6 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
    SET @maxDate = (SELECT MAX(CreateDate) FROM cdbuy.ProductsManufacturerInformation)
	SET @minDate = DATEADD(MONTH, -6, @maxDate)
	
	DELETE FROM cdbuy.ProductsManufacturerInformation where CreateDate <@minDate
	DELETE FROM dcpurchase.ObjectiveStock where [Date] < @minDate
	DELETE FROM dcpurchase.Orders where [Date] < @minDate
	DELETE FROM reports.ReportCD where [Date] < @minDate
	DELETE FROM series.StockCD WHERE [Date] < @minDate
	
	-- series requeridas para calibración y variables guardan 3 años
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas de series requeridas para calibración y variables guardan 36 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	select @maxDate =  dateadd(week, -1, dbo.GetMonday((select top 1 [date] from series.lastsale)))
	SET @minDate = DATEADD(MONTH, -36, @maxDate)

	DELETE FROM series.IMS WHERE [Date] < @minDate
	DELETE FROM series.Nielsen WHERE [Semana] < @minDate
	DELETE FROM series.NielsenCat WHERE mes < @minDate
	DELETE FROM series.SalesCD WHERE [Date] < @minDate
	DELETE FROM promotion.Promotion WHERE EndDate < @minDate
	DELETE FROM promotion.PromotionDetail WHERE EndDate < @minDate
	DELETE FROM promotion.PromotionStock WHERE EndDate < @minDate
	DELETE FROM promotion.PromotionFiles WHERE PromotionFileID not in (SELECT DISTINCT PromotionFileId FROM promotion.PromotionStock)
	DELETE FROM reports.MetricasStocks WHERE [Date] < @minDate


	-- limpieza tabla de ventas
	SET @minDate = DATEADD(MONTH, -42, @maxDate)
	DELETE FROM series.dailySales WHERE [Date] < @minDate

	-- reportes mensuales guardan 2 años
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas de reportes guardan 6 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = (SELECT MAX([Date]) FROM reports.ExclusionResults)
	SET @minDate = DATEADD(MONTH, -6, @maxDate)
	
	DELETE FROM reports.ExclusionResults WHERE [date] < @minDate
	DELETE FROM minmax.OutsideMixSuggestResult WHERE [SaleDate] < @minDate
	DELETE FROM [reports].[LostSaleBySKUCambio] WHERE [Date] < @minDate
	DELETE FROM [reports].[LostSaleByStore] WHERE [Date] < @minDate


	-- Información del CD guarda un año de historia
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'tabla series.StockCDDia  guarda un año de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = (SELECT MAX([Date]) FROM series.StockCDDia)
	SET @minDate = DATEADD(YEAR, -1, @maxDate)
	
	DELETE FROM series.StockCDDia WHERE [Date] < @minDate


	-- Sugerido ágil mayor a 3 meses de historia
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'tabla minmax.SugeridoAgil  con mas de tres meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	SET @maxDate = (SELECT MAX(Fecha) FROM minmax.SugeridoAgil)
	SET @minDate = DATEADD(MONTH, -3, @maxDate)
	
	DELETE FROM minmax.SugeridoAgil WHERE Fecha < @minDate


	-- despachosCD solo un mes
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'tabla series.DespachosCD guarda 1 mes de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = (SELECT MAX([Date]) FROM series.DespachosCD)
	SET @minDate = DATEADD(MONTH, -1, @maxDate)
	
	DELETE FROM series.DespachosCD 	WHERE [Date] < @minDate

		-- Eliminar la información anterior a un año de tabla de quiebres (muy pesado y con un año es suficiente)
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'tabla [series].[Stock-outs]  guarda 15 meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxDate = (SELECT MAX([Date]) FROM [series].[Stock-outs])
	SET @minDate = DATEADD(MONTH, -15, @maxDate)
	
	DELETE FROM [series].[Stock-outs] where [Date] < @minDate 
	DELETE FROM series.IMSBrick where Semana < @minDate 

	/*limpieza restricciones*/
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'limpieza de restricciones redundantes' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	--restricciones de mínimo 1 son redundantes
	delete --select COUNT(*)
	from  [br].SuggestRestrictionDetail
	where SuggestRestrictionTypeID in (
		select SuggestRestrictionTypeID
		from br.SuggestRestrictionType
		where ActionID=1
	) and Detail = 1

	--restricciones con 0 es estúpido
	delete --select COUNT(*)
	from  [br].SuggestRestrictionDetail
	where Detail <= 0

	--restricciones con DUN 1 es estúpido
	delete --select COUNT(*)
	from  [br].SuggestRestrictionDetail
	where SuggestRestrictionTypeID in (
		select SuggestRestrictionTypeID
		from br.SuggestRestrictionType
		where ActionID=3
	) and Detail = 1

	--restricciones con CAJATIRA 1 es estúpido
	delete --select COUNT(*)
	from  [br].SuggestRestrictionDetail
	where SuggestRestrictionTypeID in (
		select SuggestRestrictionTypeID
		from br.SuggestRestrictionType
		where ActionID=5
	) and Detail = 1



END
