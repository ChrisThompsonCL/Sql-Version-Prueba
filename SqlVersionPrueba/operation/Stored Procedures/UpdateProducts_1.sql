﻿


-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2009-08-03
-- Description:	Actualiza la Tablas [products].[Products], [products].[FullProducts], [products].[DemandGroupProducts] 
--              con la informacion de TempProducts
-- =============================================
CREATE PROCEDURE [operation].[UpdateProducts] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	DECLARE @msg NVARCHAR(100) 
	SET NOCOUNT ON;
	
	-- Llenar campo units en TempProduct
	update [aux].[TempProducts] set units = convert(float,replace(units,',','.'))

		
		
	/************************ACTUALIZACIÓN FULLPRODUCTS CON TEMPPRODUCTS**************************/		
		
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de FullProducts...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	-- Actualizar productos existentes
	UPDATE P
	SET
			P.[Description] = T.[Description]
			,P.[IsGeneric] = CASE T.[IsGeneric] WHEN 'S' THEN 1 ELSE 0 END
			,P.[GenericCategory] = T.[GenericCategory]
			,P.[GenericCategoryDesc] = T.[GenericCategoryDesc]
			,P.[UseModeId] = T.[UseModeId]
			,P.[UseMode_Desc] = T.[UseMode_Desc]
			,P.[PositioningId] = T.[PositioningId] 
			,P.[Positioning_Desc] = T.[Positioning_Desc]
			,P.[SubstituteId] = T.[SubstituteId]
			,P.[Substitute_Desc] = T.[Substitute_Desc]
			,P.[Division] = T.[Division]
			,P.[Division_Desc] = T.[Division_Desc]
			,P.[Subdivision] = T.[Subdivision]
			,P.[Subdivision_Desc] = T.[Subdivision_Desc]
			,P.[Departament] = T.[Departament]
			,P.[Departament_Desc] = T.[Departament_Desc]
			,P.[Class] = T.[Class]
			,P.[Class_Desc] = T.[Class_Desc]
			,P.[Group] = T.[Group]
			,P.[Group_Desc] = T.[Group_Desc]
			,P.[Subgroup] = T.[Subgroup]
			,P.[Subgroup_Desc] = T.[Subgroup_Desc]
			,P.[Motive] = T.[Motive]
			,P.[TotalRanking] = T.[TotalRanking]
			,P.[IsActive] = T.[IsActive]
			,P.[CategoryId] = T.[CategoryId]
			,P.[CategoryDesc] = T.[CategoryDesc]
			,P.[ManufacturerId] = T.[ManufacturerId]
			,P.[ManufacturerDesc] = T.[ManufacturerDesc]
			,P.[Units] = T.[Units]
			,P.[ConcentrationId] = T.[ConcentrationId]
			,P.[Concentration] = T.[Concentration]
			,P.[Season] = T.[Season]
			,P.[Expression] = T.[Expression]
			,P.[LogisticAreaId] = T.[LogisticAreaId]
			,P.[LogisticAreaDesc] = T.[LogisticAreaDesc]
			,P.[MotiveId] = T.[MotiveId]
			,P.[CorporationId] = T.[CorporationId]
			,P.[CorporationDesc] = T.[CorporationDesc]
			,P.[Barcode] = T.[Barcode]
			,P.[IsInfaltable] = CASE WHEN T.[Infaltable] = 'S' THEN 1 ELSE 0 END
			,P.PetitorioMinimo = T.PetitorioMinimo
			,P.PetitorioMinimo_Desc = T.PetitorioMinimo_Desc
			,P.ProCuidado = CASE WHEN T.[ProCuidado] = 'S' THEN 1 ELSE 0 END
			,P.DivisionUnificadaId = T.DivisionUnificadaId
			,P.DivisionUnificadaDesc = T.DivisionUnificadaDesc
			,P.CategoriaUnificadaId = T.CategoriaUnificadaId
			,P.CategoriaUnificadaDesc = T.CategoriaUnificadaDesc
			,P.SubcategoriaUnificadaId = T.SubcategoriaUnificadaId
			,P.SubcategoriaUnificadaDesc = T.SubcategoriaUnificadaDesc
			,P.ClaseUnificadaId = T.ClaseUnificadaId
			,P.ClaseUnificadaDesc = T.ClaseUnificadaDesc
			,P.Ges = CASE WHEN T.Ges = 'S' THEN 1 ELSE 0 END
			,P.SustitutoComercialId = T.SustitutoComercialId
			,P.VigenteId = T.VigenteId
	FROM [aux].[TempProducts] T INNER JOIN [products].[FullProducts] P on T.SKU = P.SKU
	WHERE T.SKU NOT IN (select SKU from aux.TempProducts group by SKU having count(*) > 1)

	-- Insertar nuevos productos al maestro 
	INSERT INTO [products].[FullProducts] 
		([SKU]
        ,[Description]
        ,[IsGeneric]
        ,[GenericCategory]
		,[GenericCategoryDesc]
        ,[UseModeId]
        ,[UseMode_Desc]
        ,[PositioningId]
        ,[Positioning_Desc]
        ,[SubstituteId]
        ,[Substitute_Desc]
        ,[Division]
        ,[Division_Desc]
        ,[Subdivision]
        ,[Subdivision_Desc]
        ,[Departament]
        ,[Departament_Desc]
        ,[Class]
        ,[Class_Desc]
        ,[Group]
        ,[Group_Desc]
        ,[Subgroup]
        ,[Subgroup_Desc]
        ,[Motive]
        ,[TotalRanking]
        ,[IsActive]
        ,[FirstSale]
        ,[LastSale]
        ,[Critical]
        ,[CategoryId]
        ,[CategoryDesc]
        ,[ManufacturerId]
        ,[ManufacturerDesc]
        ,[Units]
        ,[ConcentrationId]
        ,[Concentration]
        ,[Season]
        ,[Expression]
        ,[LogisticAreaId]
        ,[LogisticAreaDesc]
        ,[MotiveId]
        ,[CorporationId]
        ,[CorporationDesc]
        ,[IsOperative]
        ,[Barcode]
		,[IsInfaltable]
		,[PetitorioMinimo]
		,[PetitorioMinimo_Desc]
		,[ProCuidado]
		,[DivisionUnificadaId]
		,[DivisionUnificadaDesc]
		,[CategoriaUnificadaId]
		,[CategoriaUnificadaDesc]
		,[SubcategoriaUnificadaId]
		,[SubcategoriaUnificadaDesc]
		,ClaseUnificadaId
		,ClaseUnificadaDesc
		,Ges
		,SustitutoComercialId
		,VigenteId)
	SELECT [SKU]
			,[Description]
			,[IsGeneric]= CASE [IsGeneric] WHEN 'S' THEN 1 ELSE 0 END
			,[GenericCategory]
			,[GenericCategoryDesc]
			,[UseModeId]
			,[UseMode_Desc]
			,[PositioningId]
			,[Positioning_Desc]
			,[SubstituteId]
			,[Substitute_Desc]
			,[Division]
			,[Division_Desc]
			,[Subdivision]
			,[Subdivision_Desc]
			,[Departament]
			,[Departament_Desc]
			,[Class]
			,[Class_Desc]
			,[Group]
			,[Group_Desc]
			,[Subgroup]
			,[Subgroup_Desc]
			,[Motive]
			,[TotalRanking]
			,[IsActive]
			,[FirstSale]
			,[LastSale]
			,0 --[Critical]
			,[CategoryId]
			,[CategoryDesc]
			,[ManufacturerId]
			,[ManufacturerDesc]
			,[Units]
			,[ConcentrationId]
			,[Concentration]
			,[Season]
			,[Expression]
			,[LogisticAreaId]
			,[LogisticAreaDesc]
			,[MotiveId]
			,[CorporationId]
			,[CorporationDesc]
			,0
			,[Barcode]
			,CASE WHEN [Infaltable] = 'S' THEN 1 ELSE 0 END
			,[PetitorioMinimo]
			,[PetitorioMinimo_Desc]
			,CASE WHEN [ProCuidado] = 'S' THEN 1 ELSE 0 END
			,[DivisionUnificadaId]
			,[DivisionUnificadaDesc]
			,[CategoriaUnificadaId]
			,[CategoriaUnificadaDesc]
			,[SubcategoriaUnificadaId]
			,[SubcategoriaUnificadaDesc]
			,ClaseUnificadaId
			,ClaseUnificadaDesc
			,CASE WHEN Ges = 'S' THEN 1 ELSE 0 END
			,SustitutoComercialId
			,VigenteId
		FROM [aux].[TempProducts] 
		WHERE DivisionUnificadaId > 0 AND SKU NOT IN (SELECT SKU FROM [products].[FullProducts])
		AND SKU NOT IN (select SKU from aux.TempProducts group by SKU having count(*) > 1)

	DELETE --select *
	from products.FullProducts 
	where SKU not in (select SKU from aux.TempProducts)
		OR SKU in (select SKU from aux.TempProducts group by SKU having count(*) > 1)

	-- Actualizar totalranking
	UPDATE [products].[FullProducts]
	SET totalRanking = NULL
	WHERE totalRanking = '0' or TotalRanking = 'N'

	--EXEC [operation].[UpdateProductsFirstAndLastSale] 

	/*************************CONSIDERACIÓN GENÉRICOS******************************************/
	
	--familias
	truncate table [products].[GenericFamily]
	insert into [products].[GenericFamily]
	select GenericCategory, GenericCategoryDesc, SKU
	from (
	SELECT GenericCategory, GenericCategoryDesc, SKU,lastsale
		, ROW_NUMBER() over (partition by GenericCategory, GenericCategoryDesc order by lastsale desc, SKU) R
	FROM [Salcobrand].[products].[FullProducts]
	where isnull(DivisionUnificadaId,Division)=1
		and GenericCategory<>9999
		--and IsGeneric=1
		--and IsActive=1
		--and VigenteId=999
	)a  where R=1

	truncate table [products].[GenericFamilyAll]
	insert into [products].[GenericFamilyAll]
	select GenericCategory, GenericCategoryDesc, SKU
	from (
	SELECT GenericCategory, GenericCategoryDesc, SKU,lastsale
		, ROW_NUMBER() over (partition by GenericCategory, GenericCategoryDesc order by lastsale desc, SKU) R
	FROM [Salcobrand].[products].[FullProducts]
	where isnull(DivisionUnificadaId,Division)=1
		and GenericCategory<>9999
		--and IsGeneric=1
		--and IsActive=1
		--and VigenteId=999
	)a  where R=1
	
	
	--productos que pertenecen a la familia
	truncate table [products].[GenericFamilyDetail]
	insert into [products].[GenericFamilyDetail]
	SELECT GenericCategory, SKU
	FROM [Salcobrand].[products].[FullProducts]
	where isnull(DivisionUnificadaId,Division)=1
		and GenericCategory<>9999
		--and IsGeneric=1
		--and IsActive=1
		--and VigenteId=999
	order by GenericCategory

	truncate table [products].[GenericFamilyDetailAll]
	insert into [products].[GenericFamilyDetailAll]
	SELECT GenericCategory, SKU
	FROM [Salcobrand].[products].[FullProducts]
	where isnull(DivisionUnificadaId,Division)=1
		and GenericCategory<>9999
		--and IsGeneric=1
		--and IsActive=1
		--and VigenteId=999
	order by GenericCategory


	declare @date date =getdate()
	--borrar familias sin venta en 6 meses
	select gfd.GenericFamilyId--, sum(isnull(Q,0)) Q
	into #borrar
	from  [products].[GenericFamilyDetail] gfd
	left join (
		select SKU, SUM(Quantity)Q 
		from series.DailySales 
		where [date]>= dateadd(month,-6,@date)
		group by SKU
	) ds on gfd.SKU=ds.SKU
	group by gfd.GenericFamilyId
	having sum(isnull(Q,0))=0

	delete from [products].[GenericFamily]
	where id in (select GenericFamilyId from #borrar)

	delete from [products].[GenericFamilyDetail]
	where GenericFamilyId in (select GenericFamilyId from #borrar)
	
	drop table #borrar

	/*************************ACTUALIZACIÓN PRODUCTS CON FULLPRODUCTS***************************/	
	

		
		
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de Products...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	-- Actualizar productos existentes
	UPDATE P
	SET	P.[Name] = T.[Description]
			,P.IsGeneric=T.IsGeneric
			,P.GenericCategory=T.GenericCategory
			,P.GenericCategoryDesc=T.GenericCategoryDesc
			,P.[UseModeId] = T.[UseModeId]
			,P.[UseMode_Desc] = T.[UseMode_Desc]
			,P.[PositioningId] = T.[PositioningId] 
			,P.[Positioning_Desc] = T.[Positioning_Desc]
			,P.[SubstituteId] = T.[SubstituteId]
			,P.[Substitute_Desc] = T.[Substitute_Desc]
			,P.[Division] = T.[Division]
			,P.[Division_Desc] = T.[Division_Desc]
			,P.[Subdivision] = T.[Subdivision]
			,P.[Subdivision_Desc] = T.[Subdivision_Desc]
			,P.[Class] = T.[Class]
			,P.[Class_Desc] = T.[Class_Desc]
			,P.[Group] = T.[Group]
			,P.[Group_Desc] = T.[Group_Desc]
			,P.[Subgroup] = T.[Subgroup]
			,P.[Subgroup_Desc] = T.[Subgroup_Desc]
			,P.[Motive] = T.[Motive]
			,P.[TotalRanking] = T.[TotalRanking]
			,P.[CategoryId] = T.[CategoryId]
			,P.[CategoryDesc] = T.[CategoryDesc]
			,P.[ManufacturerId] = T.[ManufacturerId]
			,P.[ManufacturerDesc] = T.[ManufacturerDesc]
			,P.[Units] = T.[Units]
			,P.Size = T.[Units]
			,P.[ConcentrationId] = T.[ConcentrationId]
			,P.[Concentration] = T.[Concentration]
			,P.[LogisticAreaId] = T.[LogisticAreaId]
			,P.[LogisticAreaDesc] = T.[LogisticAreaDesc]
			,P.[MotiveId] = T.[MotiveId]
			,P.[Barcode] = T.[Barcode]
			,P.[IsInfaltable] = T.[IsInfaltable]
			,P.[PetitorioMinimo] = T.[PetitorioMinimo]
			,P.[PetitorioMinimo_Desc] = T.[PetitorioMinimo_Desc]
			,P.[ProCuidado] = T.[ProCuidado]
			,P.IsActive = T.IsActive
			,P.DivisionUnificadaId = T.DivisionUnificadaId
			,P.DivisionUnificadaDesc = T.DivisionUnificadaDesc
			,P.CategoriaUnificadaId = T.CategoriaUnificadaId
			,P.CategoriaUnificadaDesc = T.CategoriaUnificadaDesc
			,P.SubcategoriaUnificadaId = T.SubcategoriaUnificadaId
			,P.SubcategoriaUnificadaDesc = T.SubcategoriaUnificadaDesc
			,P.ClaseUnificadaId = T.ClaseUnificadaId
			,P.ClaseUnificadaDesc = T.ClaseUnificadaDesc
			,P.Ges = T.Ges
			,P.SustitutoComercialId=T.SustitutoComercialId
			,P.VigenteId=T.VigenteId
	FROM [products].[FullProducts] T INNER JOIN [products].[Products] P
		on T.SKU = P.SKU
		
	-- Insertar productos nuevos que cumplan criterios
	INSERT INTO [products].[Products]
        ([SKU] ,[Name],IsGeneric,GenericCategory, GenericCategoryDesc ,[Retail] ,IsActive
        ,[UseModeId],[UseMode_Desc]
        ,[PositioningId],[Positioning_Desc]
        ,[SubstituteId],[Substitute_Desc]
        ,[Division],[Division_Desc]
        ,[Subdivision],[Subdivision_Desc]
        ,[Class],[Class_Desc]
        ,[Group],[Group_Desc]
        ,[Subgroup],[Subgroup_Desc]
        ,[Motive]
        ,[TotalRanking]
        ,[FirstSale],[LastSale]
        ,[CategoryId],[CategoryDesc]
        ,[ManufacturerId],[ManufacturerDesc]
        ,[Units],Size
        ,[ConcentrationId],[Concentration]
        ,[LogisticAreaId],[LogisticAreaDesc]
        ,[MotiveId]
        ,[IsOperative]
        ,[ManufacturerIdTotalRanking]
        ,[Barcode]
		,[IsInfaltable]
		,[PetitorioMinimo],[PetitorioMinimo_Desc]
		,[ProCuidado]
		,DivisionUnificadaId,DivisionUnificadaDesc
		,CategoriaUnificadaId,CategoriaUnificadaDesc
		,SubcategoriaUnificadaId,SubcategoriaUnificadaDesc
		,ClaseUnificadaId,ClaseUnificadaDesc
		,Ges
		,SustitutoComercialId
		,VigenteId)
	SELECT [SKU],[Description],IsGeneric,GenericCategory, GenericCategoryDesc,'Salcobrand',[IsActive]
		,[UseModeId],[UseMode_Desc]
		,[PositioningId],[Positioning_Desc]
		,[SubstituteId],[Substitute_Desc]
		,[Division],[Division_Desc]
		,[Subdivision],[Subdivision_Desc]
		,[Class],[Class_Desc]
		,[Group],[Group_Desc]
		,[Subgroup],[Subgroup_Desc]
		,[Motive]
		,[TotalRanking]
		,[FirstSale],[LastSale]
		,[CategoryId],[CategoryDesc]
		,[ManufacturerId],[ManufacturerDesc]
		,[Units],Units
		,[ConcentrationId],[Concentration]
		,[LogisticAreaId],[LogisticAreaDesc]
		,[MotiveId]
		,[IsOperative]
		,null --,[ManufacturerIdTotalRanking]
		,[Barcode]
		,[IsInfaltable]
		,[PetitorioMinimo],[PetitorioMinimo_Desc]
		,[ProCuidado]
		,DivisionUnificadaId,DivisionUnificadaDesc
		,CategoriaUnificadaId,CategoriaUnificadaDesc
		,SubcategoriaUnificadaId,SubcategoriaUnificadaDesc
		,ClaseUnificadaId,ClaseUnificadaDesc
		,Ges
		,SustitutoComercialId
		,VigenteId
	FROM [products].[FullProducts] p
	WHERE SKU not in (SELECT SKU FROM [products].[Products])
		and IsActive = 1 --activos
		and MotiveId <> 2 --que no estén por descontinuar
		and DivisionUnificadaId in (1,2) --farma o CM
		and subdivision<> 15 --que no sean servicios
		and ManufacturerId <> 990 --recetario sin inventario, es servicio
		and (
			LastSale >= dateadd(week,-24,@date) --registra ventas en los últimos 6 meses
			OR
			SKU in (select newProduct from products.NewProducts) --tiene producto espejo
		)
		and SKU not in (SELECT SKU FROM products.GenericFamilyDetail) --no insertar genéricos
		and VigenteId=999 --productos vigentes
		
	-- Borrar productos que ya no cumplen criterios
	DELETE 
	--SELECT *
	FROM [products].[Products]
	WHERE IsActive <> 1 --no activos
		OR MotiveId = 2 --por descontinuar
		OR DivisionUnificadaId not in (1,2) --farma o CM
		OR Subdivision = 15 --servicios
		OR ManufacturerId = 990 --recetario sin inventario, es servicio
		OR (FirstSale is null and SKU not in (select newProduct from products.NewProducts) )
		OR (LastSale is null and SKU not in (select newProduct from products.NewProducts) )
		OR not (
			LastSale >= dateadd(week,-24,@date) --registra ventas en los últimos 6 meses
			OR
			SKU in (select newProduct from products.NewProducts) --tiene producto espejo
		)
		OR SKU in (SELECT SKU FROM products.GenericFamilyDetail) --no insertar genéricos
		OR VigenteId<>999


	/*genéricos, solo insertar SKU auxiliar*/
	insert into [products].[Products]
        ([SKU] ,[Name],IsGeneric,GenericCategory, GenericCategoryDesc ,[Retail] ,IsActive
        ,[UseModeId],[UseMode_Desc]
        ,[PositioningId],[Positioning_Desc]
        ,[SubstituteId],[Substitute_Desc]
        ,[Division],[Division_Desc]
        ,[Subdivision],[Subdivision_Desc]
        ,[Class],[Class_Desc]
        ,[Group],[Group_Desc]
        ,[Subgroup],[Subgroup_Desc]
        ,[Motive]
        ,[TotalRanking]
        ,[FirstSale],[LastSale]
        ,[CategoryId],[CategoryDesc]
        ,[ManufacturerId],[ManufacturerDesc]
        ,[Units],Size
        ,[ConcentrationId],[Concentration]
        ,[LogisticAreaId],[LogisticAreaDesc]
        ,[MotiveId]
        ,[IsOperative]
        ,[ManufacturerIdTotalRanking]
        ,[Barcode]
		,[IsInfaltable]
		,[PetitorioMinimo],[PetitorioMinimo_Desc]
		,[ProCuidado]
		,DivisionUnificadaId,DivisionUnificadaDesc
		,CategoriaUnificadaId,CategoriaUnificadaDesc
		,SubcategoriaUnificadaId,SubcategoriaUnificadaDesc
		,ClaseUnificadaId,ClaseUnificadaDesc
		,Ges
		,SustitutoComercialId
		,VigenteId)
	SELECT [SKU],[Description],IsGeneric,GenericCategory, GenericCategoryDesc,'Salcobrand',[IsActive]
		,[UseModeId],[UseMode_Desc]
		,[PositioningId],[Positioning_Desc]
		,[SubstituteId],[Substitute_Desc]
		,[Division],[Division_Desc]
		,[Subdivision],[Subdivision_Desc]
		,[Class],[Class_Desc]
		,[Group],[Group_Desc]
		,[Subgroup],[Subgroup_Desc]
		,[Motive]
		,[TotalRanking]
		,[FirstSale],[LastSale]
		,[CategoryId],[CategoryDesc]
		,[ManufacturerId],[ManufacturerDesc]
		,[Units],Units
		,[ConcentrationId],[Concentration]
		,[LogisticAreaId],[LogisticAreaDesc]
		,[MotiveId]
		,[IsOperative]
		,null --,[ManufacturerIdTotalRanking]
		,[Barcode]
		,[IsInfaltable]
		,[PetitorioMinimo],[PetitorioMinimo_Desc]
		,[ProCuidado]
		,DivisionUnificadaId,DivisionUnificadaDesc
		,CategoriaUnificadaId,CategoriaUnificadaDesc
		,SubcategoriaUnificadaId,SubcategoriaUnificadaDesc
		,ClaseUnificadaId,ClaseUnificadaDesc
		,Ges
		,SustitutoComercialId 
		,VigenteId
	from products.FullProducts 
	where SKU in (SELECT AuxSKU FROM products.GenericFamily)

		
	-- Actualizar totalranking
	UPDATE [products].[Products]
	SET ManufacturerIdTotalRanking = convert(varchar(10),ManufacturerId)+''+convert(varchar(2),TotalRanking)
	WHERE TotalRanking is not null

	UPDATE [products].[Products]
	SET ManufacturerIdTotalRanking = null
	WHERE TotalRanking is null


	/*************************ACTUALIZACIÓN ÁRBOL EN BASE A PRODUCTS***************************/
		
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion del Arbol...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
	-- Agregar nuevas categorías (nivel 2) si es necesario
	--categorías de farma
	INSERT INTO [products].[DemandGroupNew]
        ([Name]
        ,[Level]
        ,[ClientId]
        ,[ParentId])
	SELECT distinct CategoryDesc
		, 2 --,[Level]
		, cast(CategoryId as int)
		, 2 --,[ParentId]
	FROM [products].[Products]
	WHERE DivisionUnificadaId = 1 and CategoryId is not null
		AND CategoryId NOT IN (SELECT Cast(ClientId as bigint) FROM [products].[DemandGroupNew] WHERE ParentId = 2 --categorías ya agregadas
					AND ClientId NOT IN (SELECT distinct AuxClientId FROM [products].[DemandGroupAuxiliarGroups] WHERE ParentId = 2)) 
		AND CategoryId NOT IN (SELECT Cast(ClientId as bigint) FROM [products].[DemandGroupAuxiliarGroups] WHERE ParentId=2) --categorías de grupos auxiliares
		AND SKU NOT IN (SELECT SKU FROM products.Products WHERE ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers)) --MP
		AND SKU NOT IN (SELECT SKU FROM products.DemandGroupCustomDetail) --grupos custom 
	ORDER BY  cast(CategoryId as int)
		
	--categorías de consumo masivo
	INSERT INTO [products].[DemandGroupNew]
        ([Name]
        ,[Level]
        ,[ClientId]
        ,[ParentId])
	SELECT distinct ClaseUnificadaDesc
		, 2 --,[Level]
		, cast(ClaseUnificadaId as int)
		, 3 --,[ParentId]
	FROM [products].[Products]
	WHERE DivisionUnificadaId = 2 AND ClaseUnificadaId is not null
		AND ClaseUnificadaId NOT IN (SELECT Cast(ClientId as bigint) FROM [products].[DemandGroupNew] WHERE ParentId = 3  --categorías ya agregadas
			AND ClientId NOT IN (SELECT distinct AuxClientId FROM [products].[DemandGroupAuxiliarGroups] WHERE ParentId = 3))
		AND ClaseUnificadaId NOT IN (SELECT Cast(ClientId as bigint) FROM [products].[DemandGroupAuxiliarGroups] WHERE ParentId=3) --categorías de grupos auxiliares
		AND SKU NOT IN (SELECT SKU FROM products.Products WHERE ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers)) --MP
		AND SKU NOT IN (SELECT SKU FROM products.DemandGroupCustomDetail) --grupos custom 
	ORDER BY cast(ClaseUnificadaId as int)
	
	-- categorías de Marca Propia
	INSERT INTO [products].[DemandGroupNew]
				([Name]
				,[Level]
				,[ClientId]
				,[ParentId])
	SELECT distinct ManufacturerDesc,2,Manufacturerid,800
	FROM products.Products p 
		where ManufacturerId in (SELECT ManufacturerId FROM operation.PrivateLabelManufacturers)
		--and SKU not in (SELECT SKU FROM operation.PricingExcludedProducts)
		and ManufacturerId not in (select ClientId from products.DemandGroupNew where ParentId=800)

	-- Borrar último nivel del árbol
	TRUNCATE TABLE products.DemandGroupProduct
	
	-- Agregar productos de categorías no agrupadas ni familia genéricos
	INSERT INTO products.DemandGroupProduct (Id, SKU)
	SELECT b.Id, a.SKU 
	FROM
	(
		SELECT SKU, Name, DivisionUnificadaId
			, CASE WHEN DivisionUnificadaId = 1 THEN CategoryDesc ELSE ClaseUnificadaDesc END CatName
			, CASE WHEN DivisionUnificadaId = 1 THEN CategoryId ELSE ClaseUnificadaId END ClientID
		FROM [products].[Products] p
	) a 
	INNER JOIN (
		SELECT * 
		FROM [products].[DemandGroupNew] 
		WHERE [Level] = 2
	) b ON cast(b.ClientId as int)= a.ClientID AND b.ParentId = (a.DivisionUnificadaId+1) AND b.Name = a.CatName
	LEFT JOIN [products].[DemandGroupAuxiliarGroups] dgag ON dgag.ClientId = b.ClientID AND dgag.ParentId = b.ParentId AND dgag.[Level] = b.[Level]
	WHERE dgag.ClientId IS NULL
		--AND SKU NOT IN (SELECT SKU FROM operation.PricingExcludedProducts)	
		AND SKU NOT IN (SELECT SKU FROM products.Products WHERE ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers))
		AND SKU NOT IN (SELECT SKU FROM products.DemandGroupCustomDetail) --grupos custom 
		AND SKU NOT IN (SELECT SKU FROM [products].[GenericFamilyDetail]) --productos de familias genéricos
		
	-- Agregar productos de categorías agrupadas
	INSERT INTO products.DemandGroupProduct (Id, SKU)
	SELECT dgn.Id, a.SKU
	FROM [products].[DemandGroupAuxiliarGroups] dgag
	INNER JOIN [products].[DemandGroupNew] dgn ON dgn.ClientId=dgag.AuxClientId AND dgn.ParentId = dgag.ParentId AND dgn.[Level] = dgag.[Level]
	INNER JOIN (
		SELECT SKU, Name, DivisionUnificadaId
			, CASE WHEN DivisionUnificadaId = 1 THEN CategoryDesc ELSE ClaseUnificadaDesc END CatName
			, CASE WHEN DivisionUnificadaId = 1 THEN CategoryId ELSE ClaseUnificadaId END ClientID
		FROM [products].[Products] p
	) a ON dgag.ClientId = a.ClientID and dgag.ParentId = (a.DivisionUnificadaId+1) --and dgag.Name = a.CatName
		--AND SKU NOT IN (SELECT SKU FROM operation.PricingExcludedProducts)
		AND SKU NOT IN (SELECT SKU FROM products.Products WHERE ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers))
		AND SKU NOT IN (SELECT SKU FROM products.DemandGroupCustomDetail) --grupos custom 
		AND SKU NOT IN (SELECT SKU FROM [products].[GenericFamilyDetail]) --productos de familias genéricos
		
	--agrega marcas propias
	INSERT INTO products.DemandGroupProduct (Id, SKU)
	SELECT dgn.Id, p.SKU 
	FROM products.Products p 
	INNER JOIN products.DemandGroupNew dgn ON dgn.ClientId = p.ManufacturerId
	WHERE dgn.ParentId = 800 
		and ManufacturerId in (SELECT ManufacturerId FROM operation.PrivateLabelManufacturers)
		AND SKU NOT IN (SELECT SKU FROM [products].[GenericFamilyDetail]) --productos de familias genéricos
	
	--familias genéricos
	INSERT INTO products.DemandGroupProduct (Id, SKU)
	SELECT  dgn.Id, p.auxSKU 
	FROM [products].[GenericFamily] p 
	CROSS JOIN (
		select dgn.*
		from products.DemandGroupNew dgn 
		inner join (
			SELECT distinct CategoryId, DivisionUnificadaId
			FROM [Salcobrand].[products].[FullProducts]
			where DivisionUnificadaId=1
				and GenericCategory<>9999
				and IsGeneric=1
				and IsActive=1
		) gen on gen.CategoryId=dgn.ClientId and dgn.ParentId=(DivisionUnificadaId+1)
	) dgn 
			
	--agrego  grupos custom
	INSERT INTO products.DemandGroupProduct (Id, SKU)
	SELECT dgn.Id, c.SKU
	FROM products.Products c 
	INNER JOIN products.DemandGroupCustomDetail d ON d.SKU=c.SKU
	INNER JOIN products.DemandGroupNew dgn on dgn.ClientId = d.AuxCliendId
	WHERE ManufacturerId not in (SELECT ManufacturerId FROM operation.PrivateLabelManufacturers)
		
			
	--Borrar categorías que quedan vacías del árbol (no agrupadas)
	DELETE FROM products.DemandGroupNew
	WHERE Id IN (
		SELECT dgn.Id--, dgn.Name , SUM(CASE WHEN SKU IS NULL THEN 0 ELSE 1 END) SKUs
		FROM products.DemandGroupNew dgn
		LEFT JOIN [products].[DemandGroupProduct] dgp ON dgp.Id = dgn.Id
		LEFT JOIN (select distinct AuxClientId, ParentId, [Level] from products.DemandGroupAuxiliarGroups) dgag 
		ON dgn.ClientId=dgag.AuxClientId AND dgn.ParentId = dgag.ParentId AND dgn.[Level] = dgag.[Level]
		WHERE dgn.[Level] = (SELECT MAX([Level]) FROM products.DemandGroupNew) AND dgag.AuxClientId IS NULL
			AND dgn.ParentId not in (800)
		GROUP BY dgn.Id, dgn.Name
		HAVING SUM(CASE WHEN SKU IS NULL THEN 0 ELSE 1 END) = 0
	)
		
	/*************************ACTUALIZACIÓN DE PRODUCTOS EN SERVICIO***************************/
		
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualizacion de Servicios...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--Se borran de todos los servicios los productos que no estén en el maestro
	DELETE  --SELECT *
	FROM operation.ServicesProductsDetail
	WHERE SKU NOT IN (
		SELECT SKU 
		FROM [products].[DemandGroupProduct]
	)
		
	--Servicio de Pricing: todos los productos del maestro que no sean MP
	DELETE --SELECT *
	FROM operation.ServicesProductsDetail
	WHERE ( SKU IN (SELECT SKU FROM products.Products WHERE ManufacturerId IN (select ManufacturerId from operation.PrivateLabelManufacturers) ) --MP
		OR SKU IN (SELECT SKU FROM pricing.LastPriceAndCost WHERE Motive NOT IN (select MotiveId from [pricing].[ProductMotive] where IsOperative=1)) --MOTIVOS OPERATIVOS
		) 
		AND ServiceId = 1
		
	INSERT INTO operation.ServicesProductsDetail (ServiceId, SKU)
	SELECT 1,SKU 
	FROM [products].[DemandGroupProduct] p 
	WHERE SKU NOT IN (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 1)
		AND SKU NOT IN (SELECT SKU FROM products.Products WHERE ManufacturerId IN (select ManufacturerId from operation.PrivateLabelManufacturers ) ) --MP
		AND SKU IN (SELECT SKU FROM pricing.LastPriceAndCost WHERE Motive IN (select MotiveId from [pricing].[ProductMotive] where IsOperative=1)) --MOTIVOS OPERATIVOS

	--Servicio de Modelos: todos los productos del árbol
	INSERT INTO operation.ServicesProductsDetail (ServiceId, SKU)
	SELECT 2,SKU 
	FROM [products].[DemandGroupProduct] p 
	WHERE SKU NOT IN (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 2) --que ya estén
		

	--Servicio de inventario: 
	DELETE spd --SELECT *
	FROM operation.ServicesProductsDetail spd
	INNER JOIN [products].[DemandGroupProduct] p ON p.SKU = spd.SKU
	INNER JOIN [products].[Products] pn	ON p.SKU = pn.SKU
	WHERE ServiceId = 3 
	AND (spd.SKU NOT IN (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 2)
		OR pn.ManufacturerId IN (SELECT ManufacturerId FROM operation.InOperationExcludedManufacturerAllStore)
		OR spd.SKU IN (SELECT SKU FROM operation.InOperationExcludedProductsAllStores)
		OR spd.SKU NOT IN (select sku from products.Products where LogisticAreaId in (select distinct LogisticAreaId from minmax.replenishment where [days]>0))
		OR isnull(pn.TotalRanking,'-')='E'
		OR spd.SKU IN (SELECT SKU FROM products.Products WHERE MotiveId in (6,10))
	)
		
	INSERT INTO operation.ServicesProductsDetail (ServiceId, SKU)
	SELECT 3, dp.SKU 
	FROM [products].[DemandGroupProduct] dp 
	INNER JOIN [products].[Products] p ON p.SKU = dp.SKU
	WHERE dp.SKU IN (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 2)
		AND ManufacturerId NOT IN (SELECT ManufacturerId FROM operation.InOperationExcludedManufacturerAllStore)
		AND dp.SKU IN (select sku from products.Products where LogisticAreaId in (select distinct LogisticAreaId from minmax.replenishment where [days]>0))
		AND dp.SKU NOT IN (SELECT SKU FROM operation.InOperationExcludedProductsAllStores)
		AND dp.SKU NOT IN (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 3)
		AND isnull(p.TotalRanking,'-')<>'E'
		AND dp.SKU NOT IN (SELECT SKU FROM products.Products WHERE MotiveId in (6,10))
		


	-- OBSOLETO CON LA MIGRACIÓN A PRICING DSS
	--/*Clasificación de productos: limpieza y clasificación por defecto*/
	--delete from class.ProductClassificationCurrent where SKU not in (select SKU from products.DemandGroupProduct)
	--delete from class.ProductClassificationBest where SKU not in (select SKU from products.DemandGroupProduct)
	--delete from class.ProductClassificationFinal where SKU not in (select SKU from products.DemandGroupProduct)

	--insert into class.ProductClassificationBest
	--select SKU, 1, 1, 0, 0, 1, 1 
	--from products.DemandGroupProduct where sku not in (select SKU from class.ProductClassificationBest)


	---- Productos nuevos entran como clase A al ranking de Pricing
	--insert into class.ProductClassificationFinal
	--select SKU, 1, 1
	--from products.DemandGroupProduct where sku not in (select SKU from class.ProductClassificationFinal)

	--/*************************ACTUALIZACIÓN DE PARÁMETROS DEL SERVICIO***************************/
	---- actualización de categorías en pricing
	--exec [pricing].[UpdateCategoryInformation]

	EXEC [minmax].[UpdateServiceLevel]
	EXEC [SalcobrandAbastecimiento].[operation].[LoadProducts]

END



