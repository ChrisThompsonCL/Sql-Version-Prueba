﻿

-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-02-27
-- Description:	
-- =============================================
CREATE PROCEDURE [operation].[LoadDUNCD]
	
AS
BEGIN

	DECLARE @date DATE = GETDATE()

	TRUNCATE TABLE [products].DUNCD

	INSERT INTO [products].DUNCD (SKU, [UnidadesxCamada],[UnidadesxPallet], BuyingMultiple, ConveniencePack, [ConveniencePack%], RefDate)
	select SKU, [UnidadesxCamada],[UnidadesxPallet],[BuyingMultiple],[ConveniencePack],cast([ConveniencePackP] as float)/100, @date 
	from [aux].[TempDUNCD]

	
END
