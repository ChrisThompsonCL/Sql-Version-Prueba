﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-05-28
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateStockCDTarde]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

		truncate table replenishment.[StockCDTarde]

		insert into replenishment.[StockCDTarde]
		select * from [aux].[TempStockCDTarde]

END
