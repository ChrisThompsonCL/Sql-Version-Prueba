﻿



-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-02-13
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE operation.[UpdateNielsenCat] 
	-- Add the parameters for the stored procedure here	
AS
BEGIN

	

		SELECT DATEFROMPARTS( 
			SUBSTRING(cast( [MesId] as nvarchar(6)), 1, 4) 
			, SUBSTRING( cast( [MesId] as nvarchar(6)), 5, 2) 
			, 1) Mes
		  ,[CategoriaId]
		  ,[ZonaId]
		  ,[ValoresSB]
		  ,[ValoresPU]
		  ,[ValoresCadena]
		  ,[CategoriaDesc]
		  ,[ZonaDesc]
		into #temptemp
	  FROM [aux].[TempNielsenCat]

	  DELETE FROM series.[NielsenCat] WHERE mes in (select distinct mes from #temptemp) 

	  insert into series.NielsenCat
	  select *
	  from #temptemp

	  drop table #temptemp

	  truncate table [aux].[TempNielsenCat]
	  
END


