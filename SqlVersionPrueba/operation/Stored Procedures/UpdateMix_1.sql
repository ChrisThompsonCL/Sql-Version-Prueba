﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2012-05-17
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateMix] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @date date = (select top 1 [date] from series.LastStock)


	TRUNCATE TABLE [aux].[DailySuggestMix]
	
	INSERT INTO [aux].[DailySuggestMix]	([Date], [SKU], IdLevel1, IdLevel2, [Store])
	SELECT CAST(FLOOR(CAST(GETDATE() as float)) as smalldatetime), spd.SKU SKU, null, null, ssd.Store
	FROM (
		SELECT * 
		FROM operation.ServicesProductsDetail 
		WHERE ServiceId = 3
			and SKU not in (SELECT SKU FROM products.GenericFamilyDetail)
		union 
		SELECT 3, SKU
		FROM products.GenericFamilyDetail
	) spd 
	CROSS JOIN (SELECT * FROM operation.ServicesStoresDetail WHERE ServiceId = 3) ssd
	INNER JOIN series.LastStock ls ON ls.SKU = spd.SKU AND ls.Store = ssd.Store
	inner join (
		--PRODUCTOS CON PRONÓSTICO PARA ESTA SEMANA, CALCULADO EN SEMANAS ANTERIORES
		select distinct ProductId SKU
		from forecast.PredictedDailySales
		where DateKey = (select max(datekey) from forecast.PredictedDailySales where datekey<dbo.GetMonday(@date))
			and dbo.Getmonday([Date]) = dbo.Getmonday(@date)
			and ProductId not in (SELECT NewProduct FROM products.NewProducts)
			and ProductId not in (SELECT SKU FROM products.GenericFamilyDetail)
		
		union
		--PRODUCTOS NUEVOS CON ESPEJOS
		SELECT NewProduct SKU
		FROM products.NewProducts np
		
		union
		--FAMILIAS COMPLETAS DE GENÉRICOS QUE TENGAN PRONÓSTICO
		SELECT SKU
		FROM products.GenericFamilyDetail
		where GenericFamilyId in (
			select distinct gfd.GenericFamilyId
			from forecast.PredictedDailySales pds
			inner join products.GenericFamilyDetail gfd on gfd.SKU=pds.ProductId
			where DateKey = (select max(datekey) from forecast.PredictedDailySales where datekey<dbo.GetMonday(@date))
				and dbo.Getmonday([Date]) = dbo.Getmonday(@date)
		)
	) dem on dem.SKU=spd.SKU
	LEFT JOIN operation.InOperationExcludedCombinations ioec ON ioec.SKU = spd.SKU AND ioec.Store = ssd.Store
	WHERE ls.Suggest > 0 
		AND ls.IsBloqued = 'N'
		AND ioec.SKU IS NULL AND ioec.Store IS NULL
		--AND spd.SKU not in (
		--			select sku 
		--			from products.products 
		--			where (manufacturerId in (18,92) or categoryId=1)
		--		--		and sku not in (select sku from mix.PilotDetails where pilotID=1)
		--			)



END

