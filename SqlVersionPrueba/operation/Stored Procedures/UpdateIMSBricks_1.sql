﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: <2018-02-08>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateIMSBricks]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM [series].[IMSBrick] WHERE Semana IN (
		SELECT distinct Semana
		FROM [aux].[TempIMSBrick]
	)

	INSERT INTO [series].[IMSBrick] ([Semana],[SemanaNum],[BrickId],[Brick],[SKU],[SKUDesc],[UnidadesSB],[UnidadesCadena],[ValoresSB],[ValoresCadena])
	SELECT [Semana],[SemanaNum],[BrickId],[Brick],[SKU],[SKUDesc]
		,[function].ScientificNotationToFloat([UnidadesSB])
		,[function].ScientificNotationToFloat([UnidadesCadena])
		,[function].ScientificNotationToFloat([ValoresSB])
		,[function].ScientificNotationToFloat([ValoresCadena])
	FROM [aux].[TempIMSBrick]


	DELETE FROM [series].IMS WHERE [Date] IN (
		SELECT distinct Semana
		FROM [aux].[TempIMSBrick]
	)

	INSERT INTO series.IMS ([Date], SKU,[UnidadesSB],[UnidadesCadenas],[VentasSB],[VentasCadenas], PositioningID)
	SELECT Semana, a.SKU
		, SUM([function].ScientificNotationToFloat([UnidadesSB]))
		, SUM([function].ScientificNotationToFloat([UnidadesCadena]))
		, SUM([function].ScientificNotationToFloat([ValoresSB]))
		, SUM([function].ScientificNotationToFloat([ValoresCadena]))
		, PositioningId
	FROM [aux].[TempIMSBrick] a
	LEFT JOIN products.FullProducts fp on fp.SKU=a.SKU
	GROUP BY Semana, a.SKU, PositioningId

	TRUNCATE TABLE [aux].[TempIMSBrick]


END
