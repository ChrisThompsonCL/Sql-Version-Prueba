﻿
-- =============================================
-- Author:		#LaDani
-- Create date: 2018-03-07
-- Description:	update mirror products
-- =============================================
CREATE PROCEDURE [operation].[UpdateMirrorProducts]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--borrar casos raros
	DELETE --SELECT *
	FROM aux.MirrorSKUs
	WHERE NewSKU=0 OR MirrorSKU=0

	--borrar casos no incluidos en el maestro
	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	LEFT JOIN products.FullProducts fp on fp.SKU=a.NewSKU
	WHERE fp.SKU is null

	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	LEFT JOIN products.Products fp on fp.SKU=a.MirrorSKU
	WHERE fp.SKU is null

	--borrar caso donde mirror es Ranking Z
	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	INNER JOIN products.Products fp on fp.SKU=a.MirrorSKU
	WHERE fp.TotalRanking='Z'

	--borra espejos con motivo descontinuado o inactivo
	DELETE a--SELECT a.NewSKU, fp.FirstSale, fp.Motive, fp.MotiveId
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.MirrorSKU
	WHERE MotiveId in (2,91)

	--borra casos sin información de stock
	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	LEFT JOIN (
			SELECT SKU, count(Store) Stores
			FROM series.LastStock
			GROUP BY SKU
			HAVING COUNT(Store)>0
	) fp on fp.SKU=a.NewSKU
	WHERE fp.SKU is null

	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	LEFT JOIN (
			SELECT SKU, count(Store) Stores
			FROM series.LastStock
			GROUP BY SKU
			HAVING COUNT(Store)>0
	) fp on fp.SKU=a.MirrorSKU
	WHERE fp.SKU is null

	--borrar casos producto nuevos "no nuevos"
	DELETE a--SELECT a.NewSKU, fp.FirstSale, c.[Date]
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.NewSKU
	, (select top 1 [date] from series.lastsale) c
	WHERE fp.FirstSale < DATEADD(MONTH, -3, c.[Date])
	
	--borrar casos productos espejos "no viejos"
	DELETE a--SELECT a.NewSKU, fp.FirstSale, c.[Date]
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.MirrorSKU
	, (select top 1 [date] from series.lastsale) c
	WHERE fp.FirstSale > DATEADD(MONTH, -3, c.[Date])

	--borrar casos Ranking E
	DELETE a--SELECT a.NewSKU, fp.FirstSale, fp.TotalRanking
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.NewSKU
	WHERE fp.TotalRanking='E'

	--borrar genéricos
	DELETE--SELECT *
	FROM aux.MirrorSKUs
	WHERE NewSKU in (select sku from products.GenericFamilyDetail)

	--borra compras por solicitud
	DELETE a--SELECT a.NewSKU, fp.FirstSale, fp.Motive, fp.MotiveId
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.MirrorSKU
	WHERE MotiveId in (6,10)




	--borrar casos ya configurados
	DELETE a--SELECT *
	FROM aux.MirrorSKUs a
	INNER JOIN products.NewProducts np on np.NewProduct=a.NewSKU



	--AGREGAR
	INSERT INTO products.NewProducts
	SELECT a.NewSKU, a.MirrorSKU, 1 Share, cast(getdate() as date) BeginDate, 'SYSTEM' as Username
	FROM aux.MirrorSKUs a
	INNER JOIN products.FullProducts fp on fp.SKU=a.NewSKU

	--borrar espejos de mucho tiempo
	DELETE a--SELECT a.*, fp.FirstSale, c.[Date]
	FROM products.NewProducts a
	INNER JOIN products.FullProducts fp on fp.SKU=a.NewProduct
	, (select top 1 [date] from series.lastsale) c
	WHERE fp.FirstSale < DATEADD(MONTH, -3, c.[Date])

	--borrar espejos que quedan sin modelo
	declare @date date = (select top 1 [date] from series.LastStock)

	DELETE a--SELECT a.*
	FROM products.NewProducts a
	WHERE a.MirrorProduct NOT IN (
		select distinct ProductId SKU
		from forecast.PredictedDailySales
		where DateKey = (select max(datekey) from forecast.PredictedDailySales where datekey<dbo.GetMonday(@date))
			and dbo.Getmonday([Date]) = dbo.Getmonday(@date)
			)
	
	--vacia tabla temporal
	TRUNCATE TABLE aux.MirrorSKUs

END
