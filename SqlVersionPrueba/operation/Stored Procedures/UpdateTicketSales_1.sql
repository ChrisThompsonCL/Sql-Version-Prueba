﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-08-10
-- Description:	Actualización de venta boleta
-- =============================================
CREATE PROCEDURE [operation].[UpdateTicketSales] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	update a set [Cost]= REPLACE([Cost], ',', '.'), [NetIncome]=REPLACE([NetIncome], ',', '.')
	from [aux].[TicketSales] a

    --Procura no cargar dos veces un mismo día
	DELETE FROM series.TicketSales WHERE [Date] in (SELECT distinct [Date] FROM aux.TicketSales)

	INSERT INTO [series].[TicketSales]--([Date],[TicketId],[SKU],[Quantity],[ListPrice],[Cost],[Discount],[FinalIncome],[NetIncome])
	SELECT *--[Date],[TicketId],[SKU],[Quantity],[ListPrice],[Cost],[Discount],[FinalIncome],[NetIncome]
	FROM [aux].[TicketSales]
	--WHERE Quantity <> 0 
 
	--vacía la tabla 
	TRUNCATE TABLE aux.[TicketSales]

    
END
