﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-03-09
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateRoutes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @diasSem int = (select count( distinct (diasem)) from [aux].TempRutas)


	if(@diasSem >= 5)
	BEGIN
		truncate table replenishment.StoreRoutes

		insert into  replenishment.StoreRoutes
		SELECT distinct
			 case a.DiaSem 
				WHEN 'LUNES' THEN 2
				WHEN 'MARTES' THEN 3
				WHEN 'MIERCOLES' THEN 4
				WHEN 'JUEVES' THEN 5
				WHEN 'VIERNES' THEN 6
				WHEN 'SABADO' THEN 7
				WHEN 'DOMINGO' THEN 1
			end [DayOfWeek]
			, a.Store
			, Bodega
			, case when Bodega like '%Ref%' then isnull(LeadTimeRef, 1) else LeadTime end LT
		--select *
		FROM [aux].TempRutas a
		inner join [aux].[TempRubrosRutas] b on b.Store=a.Store --and b.DiaSem=a.DiaSem
		inner join replenishment.StoreLeadTimes slt on slt.Store = a.Store

	END


END
