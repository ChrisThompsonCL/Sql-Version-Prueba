﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2018-05-28
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [operation].[UpdateRoutesTarde]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @diasSem int = (select count( distinct (diasem)) from [aux].TempRutasTarde)


	if(@diasSem >= 5)
	BEGIN
		truncate table replenishment.StoreRoutesTarde

		insert into  replenishment.StoreRoutesTarde
		SELECT distinct
			 case a.DiaSem 
				WHEN 'LUNES' THEN 2
				WHEN 'MARTES' THEN 3
				WHEN 'MIERCOLES' THEN 4
				WHEN 'JUEVES' THEN 5
				WHEN 'VIERNES' THEN 6
				WHEN 'SABADO' THEN 7
				WHEN 'DOMINGO' THEN 1
			end [DayOfWeek]
			, a.Store
			, Rubro
			, case when Rubro like '%Ref%' then isnull(LeadTimeRef, 1) else LeadTime end LT
		--select *
		FROM [aux].TempRutasTarde a
		inner join [aux].[TempRubrosRutasTarde] b on b.Store=a.Store --and b.DiaSem=a.DiaSem
		inner join replenishment.StoreLeadTimes slt on slt.Store = a.Store

	END


END
