﻿-- =============================================
-- Author:		Dani Albornoz
-- Create date: 2017-06-22
-- Description:	Obtiene el máximo valor entre dos números flotantes
-- =============================================
CREATE FUNCTION [function].Maximo
(	
	-- Add the parameters for the function here
	@val1 FLOAT, @val2 FLOAT
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT case when @val1>@val2 then @val1 else isnull(@val2,@val1) end as Maximo
)
