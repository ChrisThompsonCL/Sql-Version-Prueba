﻿
-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2011-11-02
-- Description:	Calcula la venta perdida para un producto
-- =============================================
CREATE FUNCTION [function].[GetLostSale] 
(
	@rate float,@sale float
)
RETURNS float
AS
BEGIN
	declare @result float
	set @result = 0
	
	-- parchamos las ventas negativas.
	if @sale <0
	begin 
	set @sale = 0
	end 

	if @sale = 0
	begin 
		set @result = @rate
	end
	else if @rate < 10 
	begin 
		declare @i int  
		set @i = @sale+1
		while(@i<20)
		begin
			set @result += [function].PoissonProbability(@rate,@i)*(@i-@sale)
			set @i=@i+1
		end
	end
	else
	begin
		declare @mu float,@sigma float,@Phi float,@pha float
		set @mu = @rate
		set @sigma = sqrt(@rate)
		declare @x float,@xCalc float
		set @x =cast(ROUND((@sale-@mu)/@sigma,3,0) as float)
		
		set @xCalc =cast(ROUND((@sale-@mu)/@sigma,3,0) as float) 
		
		if @xCalc > (select MAX(x) from reports.NormalDist)
			begin select @x = MAX(x) from reports.NormalDist end
		else if @xCalc < (select MIN(x) from reports.NormalDist)
			begin select @x = MIN(x) from reports.NormalDist end
		else 
			begin set @x = @xCalc end
		
		select @Phi =[FA(x)] from reports.NormalDist where x between @x-0.0001 and @x+0.0001
		select @pha =[f(x)] from reports.NormalDist where x between @x-0.0001 and @x+0.0001

        if @Phi = 1
			begin
            return 0
            end

        set @result = (1-@Phi) * (@mu + @sigma * @pha / (1 - @Phi)) - @sale * (1 - @Phi)
        
        if (@result < 0)
        begin
            set @result = 0
        end
        
	end
	RETURN @result
END

