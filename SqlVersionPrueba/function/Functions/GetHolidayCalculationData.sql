﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2013-06-19
-- Description: Obtiene datos para el cálculo del alza del feriado
-- =============================================
CREATE FUNCTION [function].[GetHolidayCalculationData]
(	
	-- Add the parameters for the function here
	@holidayDate date
	, @addDays int = 0
)
RETURNS TABLE 
AS
RETURN 
(

		select hr.SKU,hr.Store,@holidayDate HolidayDate,ls.[Date] LastStockDate
			,antes.RateBefore ,hr.Rate RateHoliday
			,SumRate = antes.RateBefore+ hr.Rate
			,isnull(mmc.RateNext,0) RateNext
			,ls.Suggest CurrentSuggest, ls.Stock
			,case when mmc.sku is null then 0 else 1 end IsMinMax
		from (
			SELECT SKU, Store, SUM(Rate) Rate
			FROM [minmax].[HolidaysRates]
			where [Date] BETWEEN DATEADD(day, -1, @holidayDate) AND DATEADD(day, -1+@addDays, @holidayDate) and Rate>0
			GROUP BY SKU, Store
		) hr inner join (
			SELECT SKU, Store, RateBefore
			FROM [minmax].[HolidaysRates]
			where [Date] = DATEADD(day, -1, @holidayDate) 
		)  antes on hr.SKU = antes.SKU and hr.Store = antes.Store
		inner join series.LastStock ls on ls.SKU = hr.SKU and ls.Store = hr.Store
		left join (
			select mmc.SKU, mmc.Store, rn.RateNext
			from minmax.MinMaxCombinations mmc
			inner join [minmax].[HolidaysRates] rn on rn.SKU=mmc.SKU and rn.Store=mmc.Store
			where [Date] = DATEADD(day, -1+@addDays, @holidayDate)
		) mmc on mmc.SKU=hr.SKU and mmc.Store=hr.Store
		where hr.SKU not in (
				select SKU
				from products.Products 
				where LogisticAreaId in ('Bcon.Con','Bref.Ref') 
					OR Class in (192,198)
					OR isnull(TotalRanking,'') in ('Z','E'))
			and hr.Store not in (56,273,546,600,601,499,585,862,565) -- locales excepción por capacidad de bodega
)

