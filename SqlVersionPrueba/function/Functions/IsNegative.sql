﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2012-06-18
-- Description:	Analiza el primer valor para saber si es negativo, 
	-- de serlo, retorna el segundo valor; 
	-- de no serlo retorna el primero
-- =============================================
CREATE FUNCTION [function].IsNegative
(
	@number REAL
	, @defaultValue REAL
)
RETURNS REAL
AS
BEGIN
	DECLARE @return REAL
	
	IF(@number < 0) BEGIN
		SET @return = @defaultValue;
	END 
	ELSE
		SET @return = @number;
	
	RETURN @return

END
