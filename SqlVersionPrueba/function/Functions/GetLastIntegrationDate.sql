﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-12-30
-- Description:	[ESTANDARIZACIÓN] Obtiene última fecha de integración
-- =============================================
CREATE FUNCTION [function].[GetLastIntegrationDate]
(
	-- Add the parameters for the function here
)
RETURNS date
AS
BEGIN

	-- Return the result of the function
		RETURN (select TOP 1 [date] FROM [series].[LastSale])


END
