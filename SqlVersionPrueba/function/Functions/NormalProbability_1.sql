﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [function].[NormalProbability]
(
	-- Add the parameters for the function here
	@mean float,@std float,@ServiceLevel float
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	declare @returnN01 float

	select @returnN01 = max(x) from reports.NormalDist where [FA(x)] between @ServiceLevel-0.0001 and @ServiceLevel+0.0001
	--select max(x) from reports.NormalDist where [FA(x)] between .95-0.0001 and .95+0.0001
	-- Add the T-SQL statements to compute the return value here
	-- Return the result of the function
	RETURN @returnN01*@std+@mean

END
