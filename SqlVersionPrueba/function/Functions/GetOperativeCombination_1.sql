﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 27-04-2012
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [function].[GetOperativeCombination]
(
	-- Add the parameters for the function here
	@serviceId int,
	@productType int,
	@productLevel int = 3,
	@storeType int,
	@storeLevel int = 0
)
RETURNS @result TABLE 
(
	-- Add the column definitions for the TABLE variable here
	ProductId bigint not null
	,ProductType int not null
	,StoreId bigint not null
	,StoreType int not null
	,primary key (ProductId, StoreId)
	--,CONSTRAINT [PK_result_1] PRIMARY KEY CLUSTERED 
	--(
	--	ProductId ASC,
	--	StoreId ASC,
	--	ProductType ASC,
	--	StoreType ASC
	--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

AS
BEGIN
	
	-- Fill the table variable with the rows for your result set

	/***************PRICING, DEMAND MODELS, CD BUY***************/
	IF @serviceId = 1 OR @serviceId = 2 OR @serviceId = 4
	BEGIN
		
		IF @productType = 0 AND @storeType = 0
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT SKU, @productType, Store, @storeType
		FROM (SELECT * FROM operation.Services WHERE Id = @serviceId) s
		INNER JOIN operation.ServicesProductsDetail spd ON spd.ServiceId = s.Id
		INNER JOIN operation.ServicesStoresDetail ssd ON ssd.ServiceId = s.Id
		ORDER BY SKU, Store
		END
		
		ELSE IF @productType = 0 AND @storeType = 3
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT SKU, @productType, 1, @storeType
		FROM (SELECT * FROM operation.Services WHERE Id = @serviceId) s
		INNER JOIN operation.ServicesProductsDetail spd ON spd.ServiceId = s.Id
		ORDER BY SKU
		END
		
		ELSE IF @productType = 3 AND @storeType = 0
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT 1, @productType, Store, @storeType
		FROM (SELECT * FROM operation.Services WHERE Id = @serviceId) s
		INNER JOIN operation.ServicesStoresDetail ssd ON ssd.ServiceId = s.Id
		ORDER BY Store
		END
		
		ELSE
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT distinct
			ProductId
			, @productType ProductType
			, CASE @storeType WHEN 3 THEN 1 WHEN 0 THEN ssd.Store END AS StoreId
			, @storeType AS StoreType
		FROM (SELECT * FROM operation.Services WHERE Id = @serviceId) s
		LEFT JOIN operation.ServicesProductsDetail spd ON spd.ServiceId = s.Id
		LEFT JOIN operation.ServicesStoresDetail ssd ON ssd.ServiceId = s.Id AND @storeType=0
		LEFT JOIN (
			SELECT CASE @productLevel WHEN 1 THEN d1.Id WHEN 2 THEN d2.Id END ProductId, dgp.SKU
			FROM products.DemandGroupProduct dgp 
			INNER JOIN products.DemandGroupNew d2 ON d2.Id = dgp.Id
			INNER JOIN products.DemandGroupNew d1 ON d1.Id = d2.ParentId
		) p ON p.SKU = spd.SKU
		LEFT JOIN (
			SELECT StoreId as Store
			FROM stores.ClusterDetail 
		) st ON st.Store = ssd.Store
		ORDER BY ProductId, StoreId
		END
		
	END
	
	
	/***************SUGERIDO***************/
	ELSE IF @serviceId = 3
	BEGIN
	
		IF @productType = 0 AND @storeType = 0
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT SKU, @productType, Store, @storeType
		FROM [aux].[DailySuggestMix] dsm
		ORDER BY SKU, Store
		END
		
		ELSE IF @productType = 0 AND @storeType = 3
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT distinct SKU, @productType, 1, @storeType
		FROM [aux].[DailySuggestMix] dsm
		ORDER BY SKU
		END
		
		ELSE IF @productType = 3 AND @storeType = 0
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT distinct 1, @productType, Store, @storeType
		FROM [aux].[DailySuggestMix] dsm
		ORDER BY Store
		END
		
		ELSE
		BEGIN
		INSERT INTO @result (ProductId, ProductType, StoreId, StoreType)
		SELECT distinct 
			CASE @productLevel WHEN 1 THEN IdLevel1 WHEN 2 THEN IdLevel2 END AS ProductId
			, @productType ProductType
			, CASE @storeType WHEN 3 THEN 1 WHEN 0 THEN Store END AS StoreId
			, @storeType AS StoreType
		FROM [aux].[DailySuggestMix] dsm
		ORDER BY ProductId, StoreId
		END
		
	END
	
	RETURN 
END
