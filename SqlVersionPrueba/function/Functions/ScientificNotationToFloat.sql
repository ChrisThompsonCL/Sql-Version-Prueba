﻿-- =============================================
-- Author:		Dani y Fai <3
-- Create date: 2018-04-03
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [function].ScientificNotationToFloat
(
	-- Add the parameters for the function here
	@sciennot nvarchar(50)
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result float 

	-- Add the T-SQL statements to compute the return value here
	SELECT @result=  replace(cast(cast(cast(@sciennot as real) as money) as nvarchar(255)),'.00','')

	-- Return the result of the function
	RETURN @result

END
