﻿
CREATE  FUNCTION [function].[Factorial](@numero numeric(5)) 
RETURNS numeric 
BEGIN 
   Declare @Fact numeric
   IF @numero = 0 OR @numero = 1
      SELECT @Fact = 1
   ELSE
      SELECT @Fact = [function].FACTORIAL(@numero - 1) * @numero

   RETURN @Fact 
END  
