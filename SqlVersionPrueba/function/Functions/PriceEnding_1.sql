﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2011-07-06
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [function].[PriceEnding]
(
	-- Add the parameters for the function here
	@sku int,
	@price decimal
)
RETURNS decimal
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result as decimal

	---- Add the T-SQL statements to compute the return value here
	--DECLARE @division int
	DECLARE @dgid int, @division int, @classId int 
	SELECT @dgid = Id from products.DemandGroupProduct WHERE SKU = @sku
	SELECT @division = Division FROM products.Products WHERE SKU = @sku
	SELECT  @classId = ClassificationTypeId FROM class.ProductClassificationFinal WHERE SKU = @sku


	SELECT @result = 
		CASE 
			WHEN CEILING(@price) > 0 THEN --solo precio positivo
				 CEILING(@price) + (99-RIGHT(CEILING(@price), 2)) --completa los 99				
			ELSE CEILING(@price)
		END


		--CASE 
		--	WHEN CEILING(@price) > 0 THEN --solo precio positivo
		--		CASE 
		--			WHEN RIGHT(CEILING(@price), 2) BETWEEN 00 AND 39 THEN CEILING(@price) - (RIGHT(CEILING(@price), 2) + 1) --baja al 99 anterior
		--			ELSE CEILING(@price) + (99-RIGHT(CEILING(@price), 2)) --completa los 99				
		--		END
		--	ELSE CEILING(@price)
		--END





			
	-- Return the result of the function
	RETURN @result

END

