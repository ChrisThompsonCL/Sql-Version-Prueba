﻿

CREATE FUNCTION [function].[PoissonProbability]
(
	-- Add the parameters for the function here
	@rate float,@x float
)
RETURNS float
AS
BEGIN
	
	declare @return float
	set @return = Power(@rate, @x) * Exp(-@rate) /[function].Factorial(@x)
	
	RETURN @return

END

