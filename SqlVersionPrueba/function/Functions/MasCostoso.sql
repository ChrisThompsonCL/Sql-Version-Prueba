﻿



-- =============================================
-- Author:	Daniela Albornoz
-- Create date: 2015-07-23
-- Description:	Obtiene el mínimo valor entre los 3 valores entregados
-- =============================================
CREATE FUNCTION [function].[MasCostoso](@val1 FLOAT, @val2 FLOAT, @val3 FLOAT)
RETURNS FLOAT
AS
BEGIN
	DECLARE @aux FLOAT = COALESCE(@val1,@val2,@val3)
	
	SET @aux = 
	(
		select top 1 P 
		from (
			select @val1 P
			union
			select @val2
			union 	
			select @val3
		) a 
		where P is not null 
		order by P desc
	)

	--SET @aux = case when @val3 < (case when @val1 < @val2 then @val2 else @val1 end) then (case when @val1 < @val2 then @val2 else @val1 end) else @val3 end

	RETURN @aux
END





