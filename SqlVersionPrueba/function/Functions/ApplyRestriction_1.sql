﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-08-10
-- Description:	Aplica restricciones sobre el sugerido entregado
-- =============================================
CREATE FUNCTION [function].[ApplyRestriction]
(	
	-- Add the parameters for the function here
	@sugerido int,
	@minimo int = null,
	@fijo int = null,
	@planograma int = null,
	@medioDUN int = null,
	@maximo int = null,
	@cajatira int = null,
	@fijoEstricto int = null
)
RETURNS TABLE 
AS
RETURN 
(

	select (
	FLOOR(	
			(case when @fijoEstricto is not null then @fijoEstricto else 
				( case 
					when @mediodun > (case 
						when @planograma > (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
							then @planograma 
						else (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
					end) 
					then @mediodun 
					else (case 
						when @planograma > (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
						then @planograma 
						else (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
					end) 
				end)
			end)
		/ isnull(@cajatira,1)
	) 
	+ case when 
			(case when @fijoEstricto is not null then @fijoEstricto else 
				( case 
					when @mediodun > (case 
						when @planograma > (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
							then @planograma 
						else (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
					end) 
					then @mediodun 
					else (case 
						when @planograma > (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
						then @planograma 
						else (case when @maximo < (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) then @maximo else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) end)
					end) 
				end)
			end)
		% isnull(@cajatira,1) > 0 then 1 else 0 end
	) * isnull(@cajatira,1)

	RestrictedSuggest
)
