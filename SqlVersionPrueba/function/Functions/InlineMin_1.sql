﻿

-- =============================================
-- Author:	Raimundo Cuadrado
-- Create date: 2015-04-22
-- Description:	Obtiene el mínimo valor entre dos números flotantes
-- =============================================
CREATE FUNCTION [function].[InlineMin](@val1 FLOAT, @val2 FLOAT)
RETURNS FLOAT
AS
BEGIN
 IF @val1 < @val2
   RETURN @val1
 RETURN ISNULL(@val2,@val1)
END



