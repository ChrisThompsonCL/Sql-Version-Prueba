﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2011-05-04
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [function].[GetCustomFixedPrices] 
(	
	-- Add the parameters for the function here
	--@cluster int, 
	--@category int,
	@beginDate smalldatetime, 
	@endDate smalldatetime
)
RETURNS @result TABLE 
(
	SKU int,
	Price float
)
AS
BEGIN 
	INSERT INTO @result
	----SERNAC
	--SELECT s.SKU, CASE WHEN pc.Price is not null THEN pc.Price ELSE rp.Price END Price FROM
	--	(SELECT SKU 
	--	FROM temp.SERNAC
	--	WHERE @beginDate <= ISNULL(EndDate, '2078-12-31')
	--	AND @endDate >= StartDate) s 
	--LEFT JOIN 
	--	(SELECT SKU, CEILING(AVG(Price)) Price 
	--	FROM series.PriceAndCost WHERE [Date] BETWEEN @beginDate AND @endDate 
	--	GROUP BY SKU) pc
	--ON s.SKU = pc.SKU
	--LEFT JOIN [view].LastRealPrice rp
	--ON s.SKU= rp.SKU
	--WHERE pc.Price is not null OR rp.RealPrice is not null
	select 0,0

RETURN 
END

