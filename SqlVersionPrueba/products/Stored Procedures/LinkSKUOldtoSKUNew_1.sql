﻿-- =============================================
-- Author:		Daniela Albornoz (re-hecho)
-- Create date: 2018-04-24
-- Description:	Traspaza historia de un SKU a otro, cuando hay cambios de SKU's 
-- =============================================
CREATE PROCEDURE [products].[LinkSKUOldtoSKUNew] 
	-- indica la fecha para cargar
	@sku_new int,@sku_old int
AS
BEGIN
	

	Print 'Inicio'

	declare @newsku int,@oldsku int/*,@firstdatenewsku date,@lastdataoldsku date*/, @aux bigint, @newGUID uniqueidentifier, @oldGUID uniqueidentifier

	set @newsku = @sku_new
	set @oldsku = @sku_old

	select @newGUID = IdCombinacion from [view].relacionIdCombinacion where IdCodigoCliente=@newsku
	select @oldGUID = IdCombinacion from [view].relacionIdCombinacion where IdCodigoCliente=@oldsku
	
	
	/**SERIES DE REFERENCIA**/
	print 'Serie precio de referencia'

	--historia agregada
	select @newGUID [GUID], Fecha,AVG(PrecioReferencia) PrecioReferencia
	into #temp
	from [dbo].[SeriePrecioReferencia] 
	where [GUID] in (@oldGUID,@newGUID) 
	group by Fecha

	--borro historia 2
	delete from [dbo].[SeriePrecioReferencia] where [GUID] = @newGUID
	
	--cargo historia completa
	insert into [dbo].[SeriePrecioReferencia]([GUID], Fecha, PrecioReferencia)
	select [GUID], Fecha, PrecioReferencia
	from #temp

	--drop tabla temporal
	drop table #temp
	
	print 'Serie costo de referencia'

	--historia común
	select @newGUID [GUID], Fecha,AVG(CostoReferencia) CostoReferencia
	into #temp1
	from [dbo].[SerieCostoReferencia] 
	where [GUID] in (@oldGUID,@newGUID) 
	group by Fecha

	--borro historia 2
	delete from [dbo].[SerieCostoReferencia] where [GUID] = @newGUID
	
	--cargo historia completa
	insert into [dbo].[SerieCostoReferencia]([GUID], Fecha, CostoReferencia)
	select [GUID], Fecha, CostoReferencia
	from #temp1

	--drop tabla temporal
	drop table #temp1

	print 'Serie factor de conversión'

	--historia común
	select @newGUID [GUID], Fecha,AVG(FactorDeConversion) FactorDeConversion
	into #temp2
	from [dbo].[SerieFactorDeConversion] 
	where [GUID] in (@oldGUID,@newGUID) 
	group by Fecha

	--borro historia 2
	delete from [dbo].[SerieFactorDeConversion] where [GUID] = @newGUID
	
	--cargo historia completa
	insert into [dbo].[SerieFactorDeConversion]([GUID], Fecha, FactorDeConversion)
	select [GUID], Fecha, FactorDeConversion
	from #temp2

	--drop tabla temporal
	drop table #temp2

	/**ESQUEMA FORECAST**/
	print '[forecast].[DailyForecast]'

	--historia común
	select @newSKU SKU, Fecha
		, sum(Fit)Fit, sum(FitCorregido)FitCorregido
		, max(FeriadoIrrenunciable)FeriadoIrrenunciable, max(Feriado)Feriado, max(Navidad)Navidad, max(FiestasPatrias)FiestasPatrias
		, max(DiaMadre)DiaMadre, max(DiaNino)DiaNino, max(DiaPadre)DiaPadre, max(SanValentin)SanValentin, max(InicioClases)InicioClases
		, max(Halloween)Halloween, max(SemanaSanta)SemanaSanta, max(AnoNuevo)AnoNuevo
		, sum(Venta)Venta, sum(Ingreso)Ingreso
		, avg(Quiebre)Quiebre, max(Cobertura)Cobertura, MAX(Promocion)Promocion
	into #temp3
	from [forecast].[DailyForecast] 
	where SKU in (@oldSKU,@newSKU) 
	group by Fecha


	--borro historia 2
	delete from [forecast].[DailyForecast] where SKU = @newSKU
	
	--cargo historia completa
	insert into [forecast].[DailyForecast](SKU, Fecha, Fit, FitCorregido, FeriadoIrrenunciable, Feriado, Navidad, FiestasPatrias, DiaMadre, DiaNino, DiaPadre, SanValentin, InicioClases, Halloween, SemanaSanta, AnoNuevo, Venta, Ingreso, Quiebre, Cobertura, Promocion )
	select SKU, Fecha, Fit, FitCorregido, FeriadoIrrenunciable, Feriado, Navidad, FiestasPatrias, DiaMadre, DiaNino, DiaPadre, SanValentin, InicioClases, Halloween, SemanaSanta, AnoNuevo, Venta, Ingreso, Quiebre, Cobertura, Promocion 
	from #temp3

	--drop tabla temporal
	drop table #temp3

	print '[forecast].[LastForecast]'

	--historia 1
	select @newSKU SKU, FechaCalculo, FechaPronosticada, sum(Fit)Fit, sum(CorrectedFit)CorrectedFit
	into #temp4
	from [forecast].[LastForecast] 
	where SKU in (@oldSKU,@newSKU) 
	group by FechaCalculo, FechaPronosticada

	--borro historia 2
	delete from [forecast].[DailyForecast] where SKU = @newSKU
	
	--cargo historia completa
	insert into [forecast].[LastForecast](SKU, FechaCalculo, FechaPronosticada, Fit, CorrectedFit )
	select SKU, FechaCalculo, FechaPronosticada, Fit, CorrectedFit
	from #temp4

	--drop tabla temporal
	drop table #temp4

	print '[forecast].[PredictedDailySales]'
	
	--historia
	select @newSKU ProductId, Datekey, [Date], sum(Fit)Fit, sum(MaxValue)MaxValue, sum(MaxMean)MaxMean, max(cast(Corrected as int))Corrected, sum(CorrectedFit)CorrectedFit
	into #temp5
	from [forecast].[PredictedDailySales] 
	where ProductId  in (@oldSKU,@newSKU) 
	group by Datekey, [Date]

	--borro historia 2
	delete from [forecast].[PredictedDailySales] where ProductId = @newSKU
	
	--cargo historia completa
	insert into [forecast].[PredictedDailySales](ProductId, Datekey, [Date], Fit, MaxValue, MaxMean, Corrected, CorrectedFit)
	select ProductId, Datekey, [Date], Fit, MaxValue, MaxMean, Corrected, CorrectedFit
	from #temp5

	--drop tabla temporal
	drop table #temp5

	print '[forecast].[StockHistory]'


	--historia común
	select @newSKU SKU, [Date], SUM(StockPromedio)StockPromedio, SUM(StockMinimo)StockMinimo, SUM(StockMaximo)StockMaximo
		, MAX(LocalesPromedio)LocalesPromedio, avg(LocalesQuebradosPromedio)LocalesQuebradosPromedio
	into #temp6
	from [forecast].[StockHistory] 
	where SKU in (@oldSKU,@newSKU) 
	group by [Date]
	
	--borro historia 2
	delete from [forecast].[StockHistory] where SKU = @newSKU
	
	--cargo historia completa
	insert into [forecast].[StockHistory](SKU, [Date], StockPromedio, StockMinimo, StockMaximo, LocalesPromedio, LocalesQuebradosPromedio)
	select SKU, [Date], StockPromedio, StockMinimo, StockMaximo, LocalesPromedio, LocalesQuebradosPromedio
	from #temp6

	--drop tabla temporal
	drop table #temp6

	print '[forecast].[WeeklySales]'

	--historia común
	select @newSKU SKU, [Date], SUM(Quantity)Quantity, SUM(Income)Income
	into #temp7
	from [forecast].[WeeklySales] 
	where SKU in (@oldSKU,@newSKU) 
	group by [Date]

	--borro historia 2
	delete from [forecast].[WeeklySales] where SKU = @newSKU
	
	--cargo historia completa
	insert into [forecast].[WeeklySales](SKU, [Date], Quantity, Income)
	select SKU, [Date], Quantity, Income
	from #temp7

	--drop tabla temporal
	drop table #temp7
	
	/**ESQUEMA MINMAX**/

	print '[minmax].[DistributionOrderDeltas]'

	--historia común
	select @newSKU SKU, [Date], SUM(SobrecargaFeriado)SobrecargaFeriado, SUM(SobrecargaPromocion)SobrecargaPromocion, SUM(SobrecargaEstacionalidad)SobrecargaEstacionalidad, SUM(SobrecargaRestricción)SobrecargaRestricción, SUM(ExcesoCD)ExcesoCD
	into #temp8
	from [minmax].[DistributionOrderDeltas] 
	where SKU in (@oldSKU,@newSKU) 
	group by [Date]

	--borro historia 2
	delete from [minmax].[DistributionOrderDeltas] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[DistributionOrderDeltas](SKU, [Date], SobrecargaFeriado, SobrecargaPromocion, SobrecargaEstacionalidad, SobrecargaRestricción, ExcesoCD)
	select SKU, [Date], SobrecargaFeriado, SobrecargaPromocion, SobrecargaEstacionalidad, SobrecargaRestricción, ExcesoCD
	from #temp8

	--drop tabla temporal
	drop table #temp8

	print '[minmax].[DistributionOrdersForecastCurrent]'

	--historia común
	select @newSKU SKU, [Date], SUM(Venta)Venta, SUM(Stock)Stock, SUM(Pedido)Pedido, SUM(PedidoEfectivo)PedidoEfectivo, SUM(PedidoDisponible)PedidoDisponible
		, SUM(PedidoEnTransito)PedidoEnTransito,SUM(VentaPerdida)VentaPerdida,SUM(Sugerido)Sugerido,SUM(Delta_Restricciones)Delta_Restricciones
	into #temp9
	from [minmax].[DistributionOrdersForecastCurrent] 
	where SKU in (@oldSKU,@newSKU) 
	group by [Date]

	--borro historia 2
	delete from [minmax].[DistributionOrdersForecastCurrent] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[DistributionOrdersForecastCurrent](SKU, [Date], Venta, Stock, Pedido, PedidoEfectivo, PedidoDisponible,PedidoEnTransito,VentaPerdida,Sugerido,Delta_Restricciones)
	select SKU, [Date], Venta, Stock, Pedido, PedidoEfectivo, PedidoDisponible,PedidoEnTransito,VentaPerdida,Sugerido,Delta_Restricciones
	from #temp9

	--drop tabla temporal
	drop table #temp9

	print '[minmax].[DistributionOrdersForecastnew]'

	--historia común
	select @newSKU SKU, [Date], SUM(Venta)Venta, SUM(Stock)Stock, SUM(Pedido)Pedido, SUM(PedidoEfectivo)PedidoEfectivo, SUM(PedidoDisponible)PedidoDisponible
		, SUM(PedidoEnTransito)PedidoEnTransito,SUM(VentaPerdida)VentaPerdida,SUM(Sugerido)Sugerido,SUM(Delta_Restricciones)Delta_Restricciones
	into #temp10
	from [minmax].[DistributionOrdersForecastnew] 
	where SKU in (@oldSKU,@newSKU) 
	group by [Date]

	--borro historia 2
	delete from [minmax].[DistributionOrdersForecastnew] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[DistributionOrdersForecastnew](SKU, [Date], Venta, Stock, Pedido, PedidoEfectivo, PedidoDisponible,PedidoEnTransito,VentaPerdida,Sugerido,Delta_Restricciones)
	select SKU, [Date], Venta, Stock, Pedido, PedidoEfectivo, PedidoDisponible,PedidoEnTransito,VentaPerdida,Sugerido,Delta_Restricciones
	from #temp10

	--drop tabla temporal
	drop table #temp10

	print '[minmax].[FitsAllCurrent]'

	--historia común
	select @newSKU SKU, Store, [date], fechacreacion, FechaCalculo, SUM(Fit)Fit
	into #temp11
	from [minmax].[FitsAllCurrent] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store, [date], fechacreacion, FechaCalculo

	--borro historia 2
	delete from [minmax].[FitsAllCurrent] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[FitsAllCurrent]( SKU, Store, [date], fechacreacion, FechaCalculo, Fit)
	select SKU, Store, [date], fechacreacion, FechaCalculo, Fit
	from #temp11

	--drop tabla temporal
	drop table #temp11

	print '[minmax].[FitsAllNew]'

	--historia común
	select @newSKU SKU, Store, [date], fechacreacion, FechaCalculo, SUM(Fit)Fit
	into #temp12
	from [minmax].[FitsAllNew] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store, [date], fechacreacion, FechaCalculo

	--borro historia 2
	delete from [minmax].[FitsAllNew] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[FitsAllNew]( SKU, Store, [date], fechacreacion, FechaCalculo, Fit)
	select SKU, Store, [date], fechacreacion, FechaCalculo, Fit
	from #temp12

	--drop tabla temporal
	drop table #temp12

	print '[minmax].[FitsAllWeekEnd]'

	--historia común
	select @newSKU SKU, Store, [date], fechacreacion, FechaCalculo, SUM(Fit)Fit
	into #temp13
	from [minmax].[FitsAllWeekEnd] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store, [date], fechacreacion, FechaCalculo

	--borro historia 2
	delete from [minmax].[FitsAllWeekEnd] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[FitsAllNew]( SKU, Store, [date], fechacreacion, FechaCalculo, Fit)
	select SKU, Store, [date], fechacreacion, FechaCalculo, Fit
	from #temp13

	--drop tabla temporal
	drop table #temp13

	print '[minmax].[HolidaysRates]'

	--historia común
	select @newSKU SKU, Store, [date], SUM(Rate)Rate, SUM(RateBefore)RateBefore, SUM(RateNext)RateNext
	into #temp14
	from [minmax].[HolidaysRates] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store, [date]

	--borro historia 2
	delete from [minmax].[HolidaysRates] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[HolidaysRates]( SKU, Store, [date], Rate, RateBefore, RateNext)
	select SKU, Store, [date], Rate, RateBefore, RateNext
	from #temp14

	--drop tabla temporal
	drop table #temp14

	print '[minmax].[SuggestForecast]'

	--historia común
	select @newSKU SKU, Store, SUM(W1)W1, SUM(W2)W2, SUM(W3)W3, SUM(W4)W4
	into #temp15
	from [minmax].[SuggestForecast] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store

	--borro historia 2
	delete from [minmax].[SuggestForecast] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[SuggestForecast]( SKU, Store, W1,W2,W3,W4)
	select SKU, Store, W1,W2,W3,W4
	from #temp15

	--drop tabla temporal
	drop table #temp15

	print '[minmax].[SuggestForecastCorrected]'

	--historia común
	select @newSKU SKU, Store, SUM(W1)W1, SUM(W2)W2, SUM(W3)W3, SUM(W4)W4
	into #temp16
	from [minmax].[SuggestForecastCorrected] 
	where SKU in (@oldSKU,@newSKU) 
	group by Store

	--borro historia 2
	delete from [minmax].[SuggestForecastCorrected] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[SuggestForecastCorrected]( SKU, Store, W1,W2,W3,W4)
	select SKU, Store, W1,W2,W3,W4
	from #temp16

	--drop tabla temporal
	drop table #temp16

	print '[minmax].[SuggestParameters]'

	--historia común
	select @newSKU SKU, safetyDays, ServiceLevel, serviceLevelDesc, LastUpdate, UserResp
	into #temp17
	from [minmax].[SuggestParameters] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [minmax].[SuggestParameters] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[SuggestParameters]( SKU, safetyDays, ServiceLevel, serviceLevelDesc, LastUpdate, UserResp)
	select SKU, safetyDays, ServiceLevel, serviceLevelDesc, LastUpdate, UserResp
	from #temp17

	--drop tabla temporal
	drop table #temp17

	print '[minmax].[MinMaxCombinations]'

	--historia común
	select @newSKU SKU, Store, GETDATE() EntryDate
	into #temp18
	from [minmax].[MinMaxCombinations] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [minmax].[MinMaxCombinations] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[MinMaxCombinations]( SKU, Store, EntryDate)
	select SKU, Store, EntryDate
	from #temp18

	--drop tabla temporal
	drop table #temp18

	print '[minmax].[MinMaxResult]'

	--historia común
	select @newSKU SKU, Store, [date], Rate, RateNext, SLMax, SLMin, Smax, Smin
	into #temp19
	from [minmax].[MinMaxResult] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [minmax].[MinMaxResult] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[MinMaxResult]( SKU, Store, [date], Rate, RateNext, SLMax, SLMin, Smax, Smin)
	select SKU, Store, [date], Rate, RateNext, SLMax, SLMin, Smax, Smin
	from #temp19

	--drop tabla temporal
	drop table #temp19

	print '[minmax].[SuggestAllCurrent]'

	--historia común
	select @newSKU SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel
	into #temp20
	from [minmax].[SuggestAllCurrent] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [minmax].[SuggestAllCurrent] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[SuggestAllCurrent]( SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel)
	select SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel
	from #temp20

	--drop tabla temporal
	drop table #temp20

	print '[minmax].[SuggestAllNew]'

	--historia común
	select @newSKU SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel
	into #temp21
	from [minmax].[SuggestAllNew] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [minmax].[SuggestAllNew] where SKU = @newSKU
	
	--cargo historia completa
	insert into [minmax].[SuggestAllNew]( SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel)
	select SKU, Store, [date], Rate, SuggestMin, SuggestMax, LeadTime, SafetyDays, ServiceLevel
	from #temp21

	--drop tabla temporal
	drop table #temp21

	print '[operation].[InOperationExcludedCombinations]'

	--historia común
	select @newSKU SKU, Store
	into #temp22
	from [operation].[InOperationExcludedCombinations] 
	where SKU in (@oldSKU,@newsku) 
	group by Store

	--borro historia 2
	delete from [operation].[InOperationExcludedCombinations] where SKU = @newSKU
	
	--cargo historia completa
	insert into [operation].[InOperationExcludedCombinations]( SKU, Store)
	select SKU, Store
	from #temp22

	--drop tabla temporal
	drop table #temp22

	print '[operation].[InOperationExcludedProductsAllStores]'

	--historia común
	select @newSKU SKU
	into #temp23
	from [operation].[PricingExcludedProducts] 
	where SKU in (@oldSKU) 

	--borro historia 2
	delete from [operation].[PricingExcludedProducts] where SKU = @newSKU
	
	--cargo historia completa
	insert into [operation].[PricingExcludedProducts]( SKU)
	select SKU
	from #temp23

	--drop tabla temporal
	drop table #temp23

	print '[pricing].[FinalPricing2]'
	
	--historia común
	select @newSKU SKU, Datekey, AVG(ActualPrice)ActualPrice, AVG(NewPrice)NewPrice, SUM(Forecast)Forecast, SUM(Sale)Sale, SUM(Contribution)Contribution, AVG(Margin)Margin
	into #temp24
	from [pricing].[FinalPricing2] 
	where SKU in (@oldSKU,@newsku) 
	group by Datekey

	--borro historia 2
	delete from [pricing].[FinalPricing2] where SKU = @newSKU
	
	--cargo historia completa
	insert into [pricing].[FinalPricing2]( SKU, datekey, actualprice, newprice, forecast, sale, contribution, margin)
	select SKU, datekey, actualprice, newprice, forecast, sale, contribution, margin
	from #temp24

	--drop tabla temporal
	drop table #temp24

	print '[promotion].[PromotionDetail]'
	
	--historia común
	select @newSKU ProductId, PromotionId, BeginDate, Enddate, DateAggregation, ProductType, StoreId, StoreType, PromotionVariableId 
	into #temp25
	from [promotion].[PromotionDetail] 
	where ProductId in (@oldSKU) 

	--borro historia 2
	delete p 
	from [promotion].[PromotionDetail] p
	inner join #temp25 t on t.ProductId=p.ProductId and t.PromotionId=p.PromotionId and t.BeginDate=p.BeginDate and t.EndDate=p.EndDate
	
	--cargo historia completa
	insert into [promotion].[PromotionDetail]( ProductId, PromotionId, BeginDate, Enddate, DateAggregation, ProductType, StoreId, StoreType, PromotionVariableId)
	select ProductId, PromotionId, BeginDate, Enddate, DateAggregation, ProductType, StoreId, StoreType, PromotionVariableId
	from #temp25

	--drop tabla temporal
	drop table #temp25

	print '[promotion].[PromotionStock]'
	
	--historia común
	select @newSKU ProductId, ProductType, StoreId, StoreType, DateAggregation, BeginDate, EndDate, PromotionFileId, PromotionName, DebeCalcularSugerido
	into #temp26
	from [promotion].[PromotionStock] 
	where ProductId in (@oldSKU) 

	--borro historia 2
	delete p 
	from [promotion].[PromotionStock] p
	inner join #temp26 t on t.ProductId=p.ProductId and t.PromotionFileId=p.PromotionFileId and t.BeginDate=p.BeginDate and t.EndDate=p.EndDate
	
	--cargo historia completa
	insert into [promotion].[PromotionStock](ProductId, ProductType, StoreId, StoreType, DateAggregation, BeginDate, EndDate, PromotionFileId, PromotionName, DebeCalcularSugerido)
	select ProductId, ProductType, StoreId, StoreType, DateAggregation, BeginDate, EndDate, PromotionFileId, PromotionName, DebeCalcularSugerido
	from #temp26

	--drop tabla temporal
	drop table #temp26

	/*ESQUEMA REPORTS*/

	print '[reports].[LostSaleBySKUCambio]'
	
	--historia común
	select @newSKU [SKU],DivisionUnificadaId,DivisionUnificadaDesc,[Motive],CategoriaunificadaDesc,j.[ManufacturerDesc],j.[Positioning_Desc],j.[Class_Desc],j.[Description]
		,[Date]
		,case when @newSKU in (select sku from products.GenericFamilyDetail) then 1 else 0 end  [Genérico?]
		,avg([NQuiebreLocales])[NQuiebreLocales],avg([NQuiebreCD])[NQuiebreCD],SUM([VtaPerdida])[VtaPerdida],SUM([VtaPerdidaVal])[VtaPerdidaVal],SUM([VtaReal])[VtaReal]
		,SUM([VtaRealVal])[VtaRealVal],AVG([Combinaciones])[Combinaciones],MIN([MenorVtaPerdida])[MenorVtaPerdida],MIN([SinVentaPerdida])[SinVentaPerdida]
		,SUM([VtaPerdidaMix])[VtaPerdidaMix],SUM([VtaPerdidaValMix])[VtaPerdidaValMix],SUM([VtaRealMix])[VtaRealMix],SUM([VtaRealValMix])[VtaRealValMix],AVG([CombMix])[CombMix]
		,AVG([NQuiebreLocalesMix])[NQuiebreLocalesMix],AVG([NQuiebreCDMix])[NQuiebreCDMix]
	into #temp27
	from [reports].[LostSaleBySKUCambio] l
	cross join (
		select DivisionUnificadaId, DivisionUnificadaDesc, CategoriaunificadaDesc, ManufacturerDesc
			, CASE WHEN DivisionUnificadaId=1 THEN Positioning_Desc ELSE NULL END Positioning_Desc
			, CASE WHEN DivisionUnificadaId=2 THEN Class_Desc ELSE NULL END Class_Desc
			, [Description] 
		from products.FullProducts 
		where SKU=@newsku
	) j
	where [SKU] in (@oldSKU,@newsku) 
	group by DivisionUnificadaId,DivisionUnificadaDesc,[Motive],CategoriaunificadaDesc,j.[ManufacturerDesc],j.[Positioning_Desc],j.[Class_Desc],j.[Description],[Date]

	--borro historia 2
	delete from [reports].[LostSaleBySKUCambio] where SKU = @newSKU
	
	--cargo historia completa
	insert into [reports].[LostSaleBySKUCambio]([SKU],[Division],[Division_Desc],[Motive],[CategoryDesc],[ManufacturerDesc],[Positioning_Desc],[Class_Desc],[Description],[Date],[Genérico?],[NQuiebreLocales],[NQuiebreCD],[VtaPerdida],[VtaPerdidaVal],[VtaReal],[VtaRealVal],[Combinaciones],[MenorVtaPerdida],[SinVentaPerdida],[VtaPerdidaMix],[VtaPerdidaValMix],[VtaRealMix],[VtaRealValMix],[CombMix],[NQuiebreLocalesMix],[NQuiebreCDMix])
	select [SKU],DivisionUnificadaId,DivisionUnificadaDesc,[Motive],CategoriaunificadaDesc,[ManufacturerDesc],[Positioning_Desc],[Class_Desc],[Description],[Date],[Genérico?],[NQuiebreLocales],[NQuiebreCD],[VtaPerdida],[VtaPerdidaVal],[VtaReal],[VtaRealVal],[Combinaciones],[MenorVtaPerdida],[SinVentaPerdida],[VtaPerdidaMix],[VtaPerdidaValMix],[VtaRealMix],[VtaRealValMix],[CombMix],[NQuiebreLocalesMix],[NQuiebreCDMix]
	from #temp27

	--drop tabla temporal
	drop table #temp27
	
	print '[reports].[MetricasStocks]'
	
	--historia común
	select @newSKU [SKU],[date], SUM(Stock)Stock, SUM(Sugerido)Sugerido,SUM(SugeridoModelo)SugeridoModelo, SUM(StockAjustadoSugerido)StockAjustadoSugerido
		, SUM(Locales)Locales, SUM(LocalesQuebrados)LocalesQuebrados, SUM(LocalesModelo)LocalesModelo, SUM(LocalesQuebradosModelo)LocalesQuebradosModelo
		, SUM(LocalesQuebradosConStockCD)LocalesQuebradosConStockCD, SUM(LocalesQuebradosSinFallaProveedor)LocalesQuebradosSinFallaProveedor
	into #temp39
	from [reports].[MetricasStocks] 
	where [SKU] in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [reports].[MetricasStocks] where SKU = @newSKU
	
	--cargo historia completa
	insert into [reports].[MetricasStocks]([SKU],[Date], Stock, Sugerido, SugeridoModelo, StockAjustadoSugerido, Locales, LocalesQuebrados, LocalesModelo, LocalesQuebradosModelo, LocalesQuebradosConStockCD, LocalesQuebradosSinFallaProveedor)
	select [SKU],[Date], Stock, Sugerido, SugeridoModelo, StockAjustadoSugerido, Locales, LocalesQuebrados, LocalesModelo, LocalesQuebradosModelo, LocalesQuebradosConStockCD, LocalesQuebradosSinFallaProveedor
	from #temp39

	--drop tabla temporal
	drop table #temp39
	

	/*ESQUEMA SERIES*/

	print '[series].[CompetitorsDailyPrice]'

	--historia común
	select @newSKU SKU, [DATE], AVG(Fasa)Fasa, AVG(CV)CV, AVG(Simi)Simi, MAX(cast(UpdatedFasa as int))UpdatedFasa, MAX(cast(UpdatedCV as int))UpdatedCV, MAX(cast(UpdatedSimi as int))UpdatedSimi
	into #temp28
	from [series].[CompetitorsDailyPrice] 
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[CompetitorsDailyPrice] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[CompetitorsDailyPrice](SKU,[Date], Fasa, CV, Simi, UpdatedFasa,UpdatedCV,UpdatedSimi)
	select SKU, [Date], Fasa, CV, Simi, UpdatedFasa,UpdatedCV,UpdatedSimi
	from #temp28

	--drop tabla temporal
	drop table #temp28

	print '[series].[DailySales]'

	--historia común
	select @newSKU SKU, [DATE], SUM(Quantity)Quantity, SUM(Income)Income
	into #temp29
	from [series].[DailySales] 
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[DailySales] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[DailySales]( SKU, [Date], Quantity, Income)
	select SKU, [Date], Quantity, Income
	from #temp29

	--drop tabla temporal
	drop table #temp29

	print '[series].[IMS]'

	--historia común
	select @newSKU SKU, [DATE], SUM(UnidadesSB)UnidadesSB, SUM(UnidadesCadenas)UnidadesCadenas, SUM(VentasSB)VentasSB,SUM(VentasCadenas)VentasCadenas, MAX(PositioningID)PositioningID
	into #temp30
	from [series].[IMS] 
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[IMS] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[IMS]( SKU, [Date], UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas, PositioningID)
	select SKU, [Date], UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas, PositioningID
	from #temp30

	--drop tabla temporal
	drop table #temp30

	print '[series].[IMSBrick]'

	--historia común
	select @newSKU SKU, Semana, SemanaNum, BrickId, Brick, p.[description]SKUDesc, SUM(UnidadesSB)UnidadesSB, SUM(UnidadesCadena)UnidadesCadena, SUM(ValoresSB)ValoresSB,SUM(ValoresCadena)ValoresCadena
	into #temp31
	from [series].[IMSBrick] i
	cross join (select [description] from products.FullProducts where SKU=@newsku) p
	where SKU in (@oldSKU,@newsku) 
	group by Semana, SemanaNum, BrickId, Brick, p.[description]

	--borro historia 2
	delete from [series].[IMSBrick] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[IMSBrick]( SKU, Semana, SemanaNum, BrickId, Brick, SKUDesc, UnidadesSB, UnidadesCadena, ValoresSB, ValoresCadena)
	select SKU, Semana, SemanaNum, BrickId, Brick, SKUDesc, UnidadesSB, UnidadesCadena, ValoresSB, ValoresCadena
	from #temp31

	--drop tabla temporal
	drop table #temp31

	print '[series].[MissingUnitsBook]'

	--historia común
	select @newSKU SKU, Store, [Date], RUT, TypeDesc, SUM(Times) Times
	into #temp32
	from [series].[MissingUnitsBook] i
	where SKU in (@oldSKU,@newsku) 
	group by Store, [Date], RUT, TypeDesc

	--borro historia 2
	delete from [series].[MissingUnitsBook] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[MissingUnitsBook]( SKU, Store, [Date], RUT, TypeDesc, Times)
	select SKU, Store, [Date], RUT, TypeDesc, Times
	from #temp32

	--drop tabla temporal
	drop table #temp32
	
	print '[series].[Sales]'

	--historia común
	select @newSKU SKU, Store, [Date],SUM(Quantity)Quantity, SUM(Income)Income
	into #temp33
	from [series].[Sales] i
	where SKU in (@oldSKU,@newsku) 
	group by Store, [Date]

	--borro historia 2
	delete from [series].[Sales] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[Sales](SKU, Store, [Date], Quantity, Income)
	select SKU, Store, [Date], Quantity, Income
	from #temp33

	--drop tabla temporal
	drop table #temp33

	print '[series].[SalesCD]'

	--historia común
	select @newSKU SKU, [Date],SUM([Order])[Order], SUM(Dispatch)Dispatch
	into #temp34
	from [series].[SalesCD] i
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[SalesCD] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[SalesCD](SKU, [Date], [Order], Dispatch)
	select SKU, [Date], [Order], Dispatch
	from #temp34

	--drop tabla temporal
	drop table #temp34

	print '[series].[StockCD]'

	--historia común
	select @newSKU SKU, [Date],SUM(StockCD)StockCD, SUM(Transit)Transit, SUM([Order])[Order]
	into #temp35
	from [series].[StockCD] i
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[StockCD] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[StockCD](SKU, [Date], StockCD, Transit, [Order] )
	select SKU, [Date], StockCD, Transit, [Order]
	from #temp35

	--drop tabla temporal
	drop table #temp35

	print '[series].[StockCDDia]'

	--historia común
	select @newSKU SKU, [Date],SUM(Stock)Stock, SUM(Transit)Transit, AVG(UPrice)UPrice, AVG(UCost)UCost
	into #temp36
	from [series].[StockCDDia] i
	where SKU in (@oldSKU,@newsku) 
	group by [Date]

	--borro historia 2
	delete from [series].[StockCDDia] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[StockCDDia](SKU, [Date], Stock, Transit, UPrice, UCost )
	select SKU, [Date], Stock, Transit, UPrice, UCost
	from #temp36

	--drop tabla temporal
	drop table #temp36

	print '[series].[Stock-outs]'

	--historia común
	select @newSKU SKU, [Date],Store,0 stock
	into #temp37
	from [series].[Stock-outs] i
	where SKU in (@oldSKU, @newsku) 
	group by [Date],Store

	--borro historia 2
	delete from [series].[Stock-outs] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[Stock-outs](SKU, [Date],Store, stock )
	select SKU, [Date],Store, stock
	from #temp37

	--drop tabla temporal
	drop table #temp37

	print '[series].[TicketSales]'

	--historia común
	select @newSKU SKU, [Date], TicketId, Store, ClientId, Gender, AgeRange, Intensity, [Status], FamilyType, SES, IsChronic
		, SUM(Quantity)Quantity, AVG(ListPrice)ListPrice, AVG(Cost)Cost, SUM(Discount)Discount
		, SUM(FinalIncome)FinalIncome, SUM(NetIncome)NetIncome
	into #temp38
	from [series].[TicketSales] i
	where SKU in (@oldSKU, @newsku) 
	group by [Date], TicketId, Store, ClientId, Gender, AgeRange, Intensity, [Status], FamilyType, SES, IsChronic

	--borro historia 2
	delete from [series].[TicketSales] where SKU = @newSKU
	
	--cargo historia completa
	insert into [series].[TicketSales](SKU, [Date], TicketId, Store, ClientId, Gender, AgeRange, Intensity, [Status], FamilyType, SES, IsChronic
		, Quantity, ListPrice, Cost, Discount, FinalIncome, NetIncome)
	select SKU, [Date], TicketId, Store, ClientId, Gender, AgeRange, Intensity, [Status], FamilyType, SES, IsChronic
		, Quantity, ListPrice, Cost, Discount, FinalIncome, NetIncome
	from #temp38

	--drop tabla temporal
	drop table #temp38

	print '[br].[SuggestRestrictionDetail]'

	--historia común
	select @newSKU ProductId, ProductType, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate
		, SuggestRestrictionFileID, SuggestRestrictionTypeID
	into #temp40
	from [br].[SuggestRestrictionDetail] i
	where ProductID in (@oldSKU) 
	
	--cargo historia completa
	insert into [br].[SuggestRestrictionDetail](ProductId, ProductType, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate
		, SuggestRestrictionFileID, SuggestRestrictionTypeID)
	select ProductId, ProductType, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate
		, SuggestRestrictionFileID, SuggestRestrictionTypeID
	from #temp40

	--drop tabla temporal
	drop table #temp40

	print 'Log'

	insert into [log].LinkSKUOldtoSKUNew (NewSKU, OldSKU, CreateDate)
	select @newsku, @oldsku, getdate()

	Print 'Fin'

END
