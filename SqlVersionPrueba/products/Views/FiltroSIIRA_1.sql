﻿--División, Categoría, Subcategoría, Proveedor, Bodega, Ranking, SKU eran los que sugerí la primera vez, además están los productos prioritarios y locales.
CREATE VIEW products.FiltroSIIRA AS
select
	DivisionUnificadaId DivisionId, DivisionUnificadaDesc DivisionDesc
	, CategoriaUnificadaId CategoriaId, CategoriaUnificadaDesc CategoriaDesc
	, SubcategoriaUnificadaId SubCategoriaId, SubcategoriaUnificadaDesc SubCategoriaDesc
	, ManufacturerId ProveedorId, ManufacturerDesc ProveedorDesc
	, LogisticAreaId BodegaId, LogisticAreaDesc BodegaDesc
	, ISNULL(TotalRanking, '-') Ranking
	, SKU, [Name] Producto
from products.Products
