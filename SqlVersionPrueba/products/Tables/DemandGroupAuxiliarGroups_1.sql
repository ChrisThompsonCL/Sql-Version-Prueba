﻿CREATE TABLE [products].[DemandGroupAuxiliarGroups] (
    [ClientId]    VARCHAR (50) NOT NULL,
    [Name]        VARCHAR (50) NOT NULL,
    [Level]       INT          NOT NULL,
    [ParentId]    INT          NOT NULL,
    [AuxClientId] VARCHAR (50) NOT NULL
);

