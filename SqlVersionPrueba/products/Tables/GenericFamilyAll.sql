﻿CREATE TABLE [products].[GenericFamilyAll] (
    [GenericCategory]     INT          NULL,
    [GenericCategoryDesc] VARCHAR (50) NULL,
    [SKU]                 INT          NOT NULL
);

