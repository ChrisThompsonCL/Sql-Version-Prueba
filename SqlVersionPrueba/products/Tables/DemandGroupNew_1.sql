﻿CREATE TABLE [products].[DemandGroupNew] (
    [Id]       INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    [Level]    INT            NOT NULL,
    [ClientId] NVARCHAR (100) NULL,
    [ParentId] INT            NULL,
    CONSTRAINT [PK_DemandGroupNew] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DemandGroupNew_DemandGroupNew] FOREIGN KEY ([Id]) REFERENCES [products].[DemandGroupNew] ([Id])
);

