﻿CREATE TABLE [products].[DemandGroupProduct] (
    [Id]  INT NOT NULL,
    [SKU] INT NOT NULL,
    CONSTRAINT [PK_DemandGroupProduct_1] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_DemandGroupProduct_DemandGroupNew] FOREIGN KEY ([Id]) REFERENCES [products].[DemandGroupNew] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_DemandGroupProduct]
    ON [products].[DemandGroupProduct]([Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DemandGroupProduct_1]
    ON [products].[DemandGroupProduct]([SKU] ASC);

