﻿CREATE TABLE [products].[ProductFilterDetail] (
    [ColumnName]   VARCHAR (50) NOT NULL,
    [DisplayType]  INT          NULL,
    [SpecialValue] VARCHAR (50) NULL,
    CONSTRAINT [PK_ProductFilterDetail] PRIMARY KEY CLUSTERED ([ColumnName] ASC)
);

