﻿CREATE TABLE [products].[PurchaseAreas] (
    [SKU]     INT           NOT NULL,
    [WMSZone] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PurchaseAreas] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

