﻿CREATE TABLE [products].[CajaTira] (
    [SKUCaja]    INT  NOT NULL,
    [SKUTira]    INT  NOT NULL,
    [Conversion] INT  NOT NULL,
    [RefDate]    DATE NOT NULL,
    CONSTRAINT [PK_CajaTira] PRIMARY KEY CLUSTERED ([SKUCaja] ASC)
);

