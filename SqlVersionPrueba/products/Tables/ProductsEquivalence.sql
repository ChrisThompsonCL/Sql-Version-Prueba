﻿CREATE TABLE [products].[ProductsEquivalence] (
    [SKU]              INT          NOT NULL,
    [SKU_Child]        INT          NOT NULL,
    [QuantityChildren] INT          NULL,
    [Equivalence]      VARCHAR (50) NULL
);

