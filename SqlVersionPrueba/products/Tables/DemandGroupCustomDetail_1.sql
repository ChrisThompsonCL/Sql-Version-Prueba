﻿CREATE TABLE [products].[DemandGroupCustomDetail] (
    [SKU]         INT NOT NULL,
    [AuxCliendId] INT NOT NULL,
    CONSTRAINT [PK_DemandGroupCustomDetail] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

