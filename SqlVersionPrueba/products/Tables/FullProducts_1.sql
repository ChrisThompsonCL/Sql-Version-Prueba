﻿CREATE TABLE [products].[FullProducts] (
    [SKU]                       INT            NOT NULL,
    [Description]               NVARCHAR (70)  NULL,
    [IsGeneric]                 BIT            NULL,
    [GenericCategory]           INT            NULL,
    [GenericCategoryDesc]       VARCHAR (50)   NULL,
    [UseModeId]                 TINYINT        NULL,
    [UseMode_Desc]              VARCHAR (20)   NULL,
    [PositioningId]             TINYINT        NULL,
    [Positioning_Desc]          VARCHAR (50)   NULL,
    [SubstituteId]              SMALLINT       NULL,
    [Substitute_Desc]           VARCHAR (50)   NULL,
    [Division]                  SMALLINT       NULL,
    [Division_Desc]             VARCHAR (50)   NULL,
    [Subdivision]               SMALLINT       NULL,
    [Subdivision_Desc]          VARCHAR (50)   NULL,
    [Departament]               SMALLINT       NULL,
    [Departament_Desc]          VARCHAR (50)   NULL,
    [Class]                     SMALLINT       NULL,
    [Class_Desc]                NVARCHAR (50)  NULL,
    [Group]                     SMALLINT       NULL,
    [Group_Desc]                NVARCHAR (50)  NULL,
    [Subgroup]                  SMALLINT       NULL,
    [Subgroup_Desc]             NVARCHAR (50)  NULL,
    [Motive]                    NVARCHAR (50)  NULL,
    [TotalRanking]              VARCHAR (1)    NULL,
    [IsActive]                  BIT            NULL,
    [FirstSale]                 SMALLDATETIME  NULL,
    [LastSale]                  SMALLDATETIME  NULL,
    [Critical]                  BIT            NULL,
    [CategoryId]                INT            NULL,
    [CategoryDesc]              NVARCHAR (50)  NULL,
    [ManufacturerId]            INT            NULL,
    [ManufacturerDesc]          VARCHAR (50)   NULL,
    [Units]                     NVARCHAR (50)  NULL,
    [ConcentrationId]           NVARCHAR (50)  NULL,
    [Concentration]             NVARCHAR (50)  NULL,
    [Season]                    NVARCHAR (50)  NULL,
    [Expression]                BIGINT         NULL,
    [LogisticAreaId]            NVARCHAR (50)  NULL,
    [LogisticAreaDesc]          NVARCHAR (50)  NULL,
    [MotiveId]                  INT            NULL,
    [CorporationId]             INT            NULL,
    [CorporationDesc]           NVARCHAR (50)  NULL,
    [IsOperative]               BIT            CONSTRAINT [DF_Products_IsOperative] DEFAULT ((0)) NULL,
    [Barcode]                   BIGINT         NULL,
    [IsInfaltable]              BIT            CONSTRAINT [DF_FullProducts_IsInfaltable] DEFAULT ((0)) NOT NULL,
    [PetitorioMinimo]           INT            NULL,
    [PetitorioMinimo_Desc]      NVARCHAR (50)  NULL,
    [ProCuidado]                BIT            NULL,
    [DivisionUnificadaId]       INT            NULL,
    [DivisionUnificadaDesc]     NVARCHAR (100) NULL,
    [CategoriaUnificadaId]      INT            NULL,
    [CategoriaUnificadaDesc]    NVARCHAR (100) NULL,
    [SubcategoriaUnificadaId]   INT            NULL,
    [SubcategoriaUnificadaDesc] NVARCHAR (100) NULL,
    [ClaseUnificadaId]          INT            NULL,
    [ClaseUnificadaDesc]        NVARCHAR (100) NULL,
    [Ges]                       BIT            NULL,
    [SustitutoComercialId]      INT            NULL,
    [VigenteId]                 INT            NULL,
    CONSTRAINT [PK_Maestro_Productos] PRIMARY KEY CLUSTERED ([SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FullProducts]
    ON [products].[FullProducts]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FullProducts_Barcode]
    ON [products].[FullProducts]([Barcode] ASC)
    INCLUDE([SKU], [Class]);


GO
CREATE NONCLUSTERED INDEX [IDXActiveDivision]
    ON [products].[FullProducts]([IsActive] ASC, [Division] ASC, [Subdivision] ASC, [Class] ASC, [FirstSale] ASC, [LastSale] ASC)
    INCLUDE([SKU]);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [products].[FullProducts]([CategoryId] ASC)
    INCLUDE([SKU]);

