﻿CREATE TABLE [products].[DUNCD] (
    [SKU]              INT        NOT NULL,
    [UnidadesxCamada]  INT        NULL,
    [UnidadesxPallet]  INT        NULL,
    [BuyingMultiple]   INT        NULL,
    [ConveniencePack]  INT        NULL,
    [ConveniencePack%] FLOAT (53) NULL,
    [RefDate]          DATE       NOT NULL,
    CONSTRAINT [PK_DUNCD] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

