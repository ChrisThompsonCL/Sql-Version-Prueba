﻿CREATE TABLE [products].[GenericFamilyDetail] (
    [GenericFamilyId] INT NOT NULL,
    [SKU]             INT NOT NULL,
    CONSTRAINT [PK_GenericFamilyDetail] PRIMARY KEY CLUSTERED ([SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_GenericFamilyDetail]
    ON [products].[GenericFamilyDetail]([GenericFamilyId] ASC);

