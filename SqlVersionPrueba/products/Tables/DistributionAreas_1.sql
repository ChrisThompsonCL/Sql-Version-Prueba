﻿CREATE TABLE [products].[DistributionAreas] (
    [SKU]              INT           NOT NULL,
    [DistributionArea] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_DistributionAreas] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

