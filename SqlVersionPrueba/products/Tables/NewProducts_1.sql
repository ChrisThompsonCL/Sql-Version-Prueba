﻿CREATE TABLE [products].[NewProducts] (
    [NewProduct]    INT            NOT NULL,
    [MirrorProduct] INT            NOT NULL,
    [Share]         FLOAT (53)     NOT NULL,
    [BeginDate]     DATE           NOT NULL,
    [Username]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_NewProducts] PRIMARY KEY CLUSTERED ([NewProduct] ASC)
);

