﻿CREATE TABLE [products].[Products] (
    [SKU]                        INT            NOT NULL,
    [Name]                       NVARCHAR (70)  NULL,
    [IsGeneric]                  BIT            NULL,
    [GenericCategory]            INT            NULL,
    [GenericCategoryDesc]        VARCHAR (50)   NULL,
    [Retail]                     VARCHAR (50)   NULL,
    [UseModeId]                  TINYINT        NULL,
    [UseMode_Desc]               VARCHAR (20)   NULL,
    [PositioningId]              TINYINT        NULL,
    [Positioning_Desc]           VARCHAR (50)   NULL,
    [SubstituteId]               SMALLINT       NULL,
    [Substitute_Desc]            VARCHAR (50)   NULL,
    [Division]                   SMALLINT       NULL,
    [Division_Desc]              VARCHAR (50)   NULL,
    [Subdivision]                SMALLINT       NULL,
    [Subdivision_Desc]           VARCHAR (50)   NULL,
    [Class]                      SMALLINT       NULL,
    [Class_Desc]                 VARCHAR (50)   NULL,
    [Group]                      SMALLINT       NULL,
    [Group_Desc]                 VARCHAR (50)   NULL,
    [Subgroup]                   SMALLINT       NULL,
    [Subgroup_Desc]              VARCHAR (50)   NULL,
    [Motive]                     VARCHAR (50)   NULL,
    [TotalRanking]               VARCHAR (1)    NULL,
    [FirstSale]                  SMALLDATETIME  NULL,
    [LastSale]                   SMALLDATETIME  NULL,
    [CategoryId]                 INT            NULL,
    [CategoryDesc]               NVARCHAR (50)  NULL,
    [ManufacturerId]             INT            NULL,
    [ManufacturerDesc]           VARCHAR (50)   NULL,
    [Units]                      FLOAT (53)     NULL,
    [ConcentrationId]            NVARCHAR (50)  NULL,
    [Concentration]              NVARCHAR (50)  NULL,
    [LogisticAreaId]             NVARCHAR (50)  NULL,
    [LogisticAreaDesc]           NVARCHAR (50)  NULL,
    [MotiveId]                   INT            NULL,
    [IsOperative]                BIT            NULL,
    [ManufacturerIdTotalRanking] NVARCHAR (50)  NULL,
    [Brand]                      VARCHAR (50)   NULL,
    [Barcode]                    BIGINT         NULL,
    [IsActive]                   BIT            NULL,
    [IsInfaltable]               BIT            CONSTRAINT [DF_Products_IsInfaltable] DEFAULT ((0)) NOT NULL,
    [PetitorioMinimo]            INT            NULL,
    [PetitorioMinimo_Desc]       NVARCHAR (50)  NULL,
    [ProCuidado]                 BIT            NULL,
    [DivisionUnificadaId]        INT            NULL,
    [DivisionUnificadaDesc]      NVARCHAR (100) NULL,
    [CategoriaUnificadaId]       INT            NULL,
    [CategoriaUnificadaDesc]     NVARCHAR (100) NULL,
    [SubcategoriaUnificadaId]    INT            NULL,
    [SubcategoriaUnificadaDesc]  NVARCHAR (100) NULL,
    [Size]                       FLOAT (53)     NULL,
    [ClaseUnificadaId]           INT            NULL,
    [ClaseUnificadaDesc]         NVARCHAR (100) NULL,
    [Ges]                        BIT            NULL,
    [SustitutoComercialId]       INT            NULL,
    [VigenteId]                  INT            NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Products]
    ON [products].[Products]([ManufacturerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Products_Division]
    ON [products].[Products]([Division] ASC)
    INCLUDE([SKU], [PositioningId], [Positioning_Desc]);


GO
CREATE NONCLUSTERED INDEX [IX_Products_Division2]
    ON [products].[Products]([Division] ASC)
    INCLUDE([SKU], [Class], [Class_Desc]);


GO
CREATE NONCLUSTERED INDEX [IX_Salcobrand_FirstSale]
    ON [products].[Products]([FirstSale] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Products_CategoryId]
    ON [products].[Products]([CategoryId] ASC)
    INCLUDE([SKU]);


GO
CREATE NONCLUSTERED INDEX [IX_Products_2]
    ON [products].[Products]([TotalRanking] ASC, [DivisionUnificadaId] ASC, [MotiveId] ASC);

