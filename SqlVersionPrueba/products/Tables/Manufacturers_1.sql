﻿CREATE TABLE [products].[Manufacturers] (
    [Id]   INT            NOT NULL,
    [Name] NVARCHAR (500) NULL,
    [RUT]  NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

