﻿CREATE TABLE [products].[DUNStore] (
    [SKU]        INT    NOT NULL,
    [DUN14]      BIGINT NOT NULL,
    [Units]      INT    NOT NULL,
    [RefDate]    DATE   NULL,
    [CreateDate] DATE   NULL,
    [UpdateDate] DATE   NULL,
    PRIMARY KEY CLUSTERED ([SKU] ASC)
);

