﻿CREATE TABLE [products].[GenericFamily] (
    [Id]     INT          NOT NULL,
    [Name]   VARCHAR (50) NOT NULL,
    [AuxSKU] INT          NOT NULL,
    CONSTRAINT [PK_GenericFamily] PRIMARY KEY CLUSTERED ([Id] ASC)
);

