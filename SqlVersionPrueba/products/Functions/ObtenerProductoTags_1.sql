﻿-- =============================================
-- Author:		Diego Aguilar
-- Create date: 28-06-2018
-- Description:	Obtiene todos los tags de los productos, para agregar un grupo tag, agregar un select con union all
-- =============================================
CREATE FUNCTION [products].[ObtenerProductoTags]
(
)
RETURNS 
@result TABLE
(
	IdCombinacion uniqueidentifier,
	GrupoTag nvarchar(200),
	IdTag int,
	Tag nvarchar(200),
	SKU nvarchar(100),
	Name nvarchar(200)
)
AS
BEGIN
	INSERT INTO @result
	SELECT rc.IdCombinacion, a.GrupoTag, a.IdTag, a.Tag, CAST(a.SKU as nvarchar(100)) as SKU, a.Name FROM (
		SELECT Distinct 'Division' as GrupoTag, DivisionUnificadaId IdTag, DivisionUnificadaDesc as Tag, SKU, Name FROM products.Products
		union all
		SELECT Distinct 'Categoria' as GrupoTag, CategoriaUnificadaId as IdTag, CategoriaUnificadaDesc as Tag, SKU, Name FROM products.Products
		union all
		SELECT Distinct 'Sub Categoria' as GrupoTag, SubcategoriaUnificadaId as IdTag, SubcategoriaUnificadaDesc as Tag, SKU, Name FROM products.Products
		union all
		SELECT Distinct 'Ranking' as GrupoTag, 1 as IdTag, TotalRanking as Tag, SKU, Name FROM products.Products
		union all
		SELECT Distinct 'Motivo' as GrupoTag, MotiveId as IdTag, Motive as Tag, SKU, Name FROM products.Products
	) a inner join [view].[RelacionIdCombinacion] rc ON a.SKU = rc.IdCodigoCliente
	right join operation.ServicesProductsDetail sp ON sp.SKU = a.SKU
	WHERE ServiceId = 2
	order by a.SKU
	RETURN 
END
