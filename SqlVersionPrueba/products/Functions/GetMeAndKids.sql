﻿-- =============================================
-- Author: Paulina Villaroel
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [products].[GetMeAndKids]
(	
	@SKU int
)
RETURNS @T TABLE
(
	SKU int,
	Quantity  float
)
AS
BEGIN
	insert into @T select SKU_Child,1/convert(float,QuantityChildren) from [products].[ProductsEquivalence] WHERE SKU=@SKU
	insert into @T values (@SKU,1)

RETURN 
END
