﻿-- =============================================
-- Author:		Shora Pau
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [products].[GetMeAndParent]
(	
	@SKU_CHILD int
)
RETURNS @T TABLE
(
	SKU_CHILD int,
	Quantity  float
)
AS
BEGIN
	
	DECLARE @Quantity float
	DECLARE @SKU int

	 select @SKU=SKU,@Quantity=convert(float,QuantityChildren) from [products].[ProductsEquivalence] WHERE SKU_CHILD=@SKU_CHILD

	insert into @T values (@SKU_CHILD,1)
	insert into @T values (@SKU,@Quantity)

RETURN 
END
