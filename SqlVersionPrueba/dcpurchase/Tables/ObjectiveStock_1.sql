﻿CREATE TABLE [dcpurchase].[ObjectiveStock] (
    [SKU]                      INT           NULL,
    [Date]                     SMALLDATETIME NULL,
    [ObjectiveStock]           INT           NULL,
    [EquivalentSuggest]        INT           NULL,
    [TotalRate]                FLOAT (53)    NULL,
    [TotalBeforeRate]          FLOAT (53)    NULL,
    [TotalProtectedBeforeRate] FLOAT (53)    NULL,
    [OverstockFactor]          FLOAT (53)    NULL
);

