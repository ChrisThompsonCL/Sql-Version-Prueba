﻿CREATE TABLE [dcpurchase].[Pallets] (
    [SKU]    INT NOT NULL,
    [Pallet] INT NOT NULL,
    CONSTRAINT [PK_Pallets] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

