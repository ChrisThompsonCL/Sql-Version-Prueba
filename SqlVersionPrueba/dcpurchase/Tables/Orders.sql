﻿CREATE TABLE [dcpurchase].[Orders] (
    [SKU]              BIGINT        NOT NULL,
    [Date]             SMALLDATETIME NOT NULL,
    [Min]              FLOAT (53)    NULL,
    [Max]              FLOAT (53)    NULL,
    [Shortage]         INT           NULL,
    [StockCD]          FLOAT (53)    NULL,
    [Order]            INT           NULL,
    [FillRate]         FLOAT (53)    NULL,
    [DUN]              INT           NULL,
    [Pallet]           INT           NULL,
    [FinalOrder]       FLOAT (53)    NULL,
    [StockStores]      FLOAT (53)    NULL,
    [SuggestStores]    FLOAT (53)    NULL,
    [StockAvailableCD] INT           NULL,
    [ReceptionDate]    DATETIME      NULL,
    [BuyingDays]       FLOAT (53)    NULL,
    [LTSKU]            INT           NULL,
    [OrderAux]         INT           NULL,
    [SugMax2Week]      INT           NULL,
    [SugMax4Week]      INT           NULL,
    [TotalRate]        FLOAT (53)    NULL,
    [TotalBeforeRate]  FLOAT (53)    NULL,
    [DailyFit]         FLOAT (53)    NULL,
    [OverStockFactor]  FLOAT (53)    NULL,
    CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([SKU] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Orders]
    ON [dcpurchase].[Orders]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_1]
    ON [dcpurchase].[Orders]([Date] ASC);

