﻿CREATE TABLE [dcpurchase].[PurchaseParameters] (
    [ManufacturerId] INT NOT NULL,
    [Division]       INT NOT NULL,
    [DayofPurchase]  INT NULL,
    [Frequency]      INT NULL,
    CONSTRAINT [PK_ManufacturerInformation] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [Division] ASC)
);

