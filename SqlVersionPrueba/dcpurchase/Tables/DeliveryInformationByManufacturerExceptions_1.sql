﻿CREATE TABLE [dcpurchase].[DeliveryInformationByManufacturerExceptions] (
    [ManufacturerId] INT           NOT NULL,
    [BeginDate]      SMALLDATETIME NOT NULL,
    [EndDate]        SMALLDATETIME NULL,
    [Fillrate]       FLOAT (53)    NULL,
    [Leadtime]       FLOAT (53)    NULL,
    CONSTRAINT [PK_DeliveryInformationByManufacturerException] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [BeginDate] ASC)
);

