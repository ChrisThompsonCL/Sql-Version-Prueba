﻿CREATE TABLE [dcpurchase].[PurchaseParametersExceptions] (
    [ManufacturerId]  INT           NOT NULL,
    [EventDate]       SMALLDATETIME NOT NULL,
    [NextPurchaseDay] SMALLDATETIME NULL,
    CONSTRAINT [PK_PurchaseParametersExceptions] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [EventDate] ASC)
);

