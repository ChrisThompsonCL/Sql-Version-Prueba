﻿-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2013-09-06
-- Description:	Calcula la compra del cd en base a los datos
-- =============================================
CREATE PROCEDURE [dcpurchase].[FillOrders] 
	-- indica la fecha para cargar
	@date smalldatetime = NULL
AS

BEGIN

	delete from [dcpurchase].[Orders] where date = @date
	
	-- se insertan todas los productos que tienen stock objetivo
	INSERT INTO [dcpurchase].[Orders] ([SKU],[Date],[Min],[Max],[TotalRate],[TotalBeforeRate])
    select m.sku,@date, m.ObjectiveStock-1,m.ObjectiveStock,m.TotalRate,m.TotalBeforeRate
	from [dcpurchase].[ObjectiveStock] as m 
	where m.date = @date
	and m.sku in (select distinct SKU from [products].[Products] where ManufacturerId in (select ManufacturerId from [operation].[InOperationCDManufacturerAllProducts]))

	-- saca productos no activos segun laststock
	delete from [dcpurchase].[Orders]
	where SKU not in (select distinct SKU from series.LastStock where suggest>0)
		and date =@date
	
	--Actualizo el fillrate
	UPDATE D SET FillRate = B.Fillrate,LTSKU = B.Leadtime, ReceptionDate = DATEADD(day,B.Leadtime,@date)
	FROM [dcpurchase].[Orders]  as D 
	inner join [dcpurchase].ParametersBase B on D.SKU = B.ProductId
	WHERE D.Date = @date
	
	--Actualizo el DUN
	UPDATE D SET DUN = T.Detail
	FROM [dcpurchase].[Orders]  as D inner join
	( 
		-- por ahora, mientras no se cambie esta tabla
		SELECT [SKU] ProductID,[DUN] Detail FROM [cdbuy].[ProductsPackingUnit]
	) T on D.SKU = T.ProductId
	WHERE D.Date = @date
	
	UPDATE D SET DUN = 1
	FROM [dcpurchase].[Orders] D
	WHERE D.Date = @date and DUN is null
	
	--Actualizo el Pallet
	UPDATE D SET Pallet= T.Detail
	FROM [dcpurchase].[Orders]  as D inner join
	( 
		-- por ahora, mientras no se cambie esta tabla
		SELECT [SKU] ProductID,[Pallet] Detail FROM [dcpurchase].Pallets
	) T on D.SKU = T.ProductId
	WHERE D.Date = @date
	
	--información stock
	UPDATE D SET StockStores = T.Stock
		, SuggestStores = T.Suggest
		, Shortage = T.Shortage
		, OverStockFactor=convert(float,(1.0-(T.localesConSobreStock*1.0/T.locales)))
	FROM [dcpurchase].[Orders] as D 
	left outer join (
		select SKU, SUM(Stock) Stock, SUM(Suggest) Suggest 
			, SUM(case when Suggest -(Stock + Transit)>0 then Suggest -(Stock + Transit) else 0 end) Shortage
			, SUM(case when Stock>Suggest then 1 else 0 end) localesConSobreStock
			, SUM(case when Suggest>0 then 1 else 0 end) locales
		from (
			SELECT SKU, Store, SUM(Stock) Stock, SUM(Suggest) Suggest, SUM(Transit) Transit
			FROM (
				-- productos normales
				select l.sku,l.store, l.Suggest, l.Stock, l.Transit 
				from [series].[LastStock] l
				union all
				-- productos caja-tira
				select e.sku,l.store, l.Suggest/convert(float,e.QuantityChildren) Suggest, l.Stock/convert(float,e.QuantityChildren) Stock, l.Transit/convert(float,e.QuantityChildren) Transit
				from [series].[LastStock] l 
				inner join [products].[ProductsEquivalence] e on e.sku_child = l.sku
			) a group by SKU, Store
		) h  group by sku 
	) T on T.SKU = D.SKU
	where date =@date
	
	--sugerido futuro
	UPDATE D SET D.SugMax2Week = T.suggestW2
	FROM [dcpurchase].[Orders] as D left outer join (
		select SKU,SUM(suggestW2) suggestW2 from (
			select l.sku,l.store,ISNULL(suggestW2,suggest) suggestW2 
			from series.LastStock l 
			left outer join 
			(
				SElECT SKU,store,case when W1> W2 then W1 else W2 end suggestW2 from minmax.SuggestForecastCorrected
			) m on m.SKU = l.SKU and m.Store = l.Store
		 ) h group by SKU
	) T on T.SKU = D.SKU
	where date =@date
	
	UPDATE D SET D.SugMax4Week = T.suggestW4
	FROM [dcpurchase].[Orders] as D left outer join (
		select SKU,SUM(suggestW4) suggestW4 from (
			select l.sku,l.store,ISNULL(suggestW4,suggest) suggestW4 
			from series.LastStock l 
			left outer join  
			(
				SElECT SKU,store,case when W3> W4 then W3 else W4 end suggestW4 from minmax.SuggestForecastCorrected
			) m on m.SKU = l.SKU and m.Store = l.Store
		 ) h group by SKU
	) T on T.SKU = D.SKU
	where date =@date
	
	-- ACTUALIZADO lleno el faltante para productos sin sugerido
	update [dcpurchase].[Orders] SET Shortage = 0
	where (Shortage is null or SuggestStores =0) and Date = @date

	-- ACTUALIZADO techo para no tener decimales
	update [dcpurchase].[Orders] 
	set [Shortage] = ceiling([Shortage]) where date = @date 

	-- inserto stock CD
	UPDATE D SET StockCD = isnull(T.stockCD ,0)
	FROM [dcpurchase].[Orders] as D 
	left outer join 
	(
		select s.sku,s.date,s.stockCD,s.Transit,s.[Order] 
		from [series].[StockCD] s 
		where s.Date = (select max(date) from series.StockCD)
	) as T on T.sku = D.SKU
	where D.date = @date

	-- inserto stock disponible
	UPDATE [dcpurchase].[Orders] 
	SET [StockAvailableCD] = [StockCD]- [Shortage] 
	where date = @date

	-- calculo las ordenes
	UPDATE [dcpurchase].[Orders] 
	SET [Order] = case when ([Max]-[StockCD]) <=0 then 0 else  ([Max] - [StockCD]) end
	where date = @date

	UPDATE [dcpurchase].[Orders] 
	SET [OrderAux] = round(convert(float,[Order])/[Fillrate]+1,0)-1
	where date = @date

	-- aplico los minimos por Manufacturer
	UPDATE D 
	SET OrderAux = Case when OrderAux>t.[Min] then OrderAux else t.[Min] end 
	from [dcpurchase].[Orders] D
	inner join 	(
		select p.sku,[MIN] 
		from [products].[Products] p 
		inner join [cdbuy].[MinOrderByManufacturer] m on m.ManufacturerId = p.ManufacturerId
	) t on t.sku = D.SKU 
	where D.Date = @date and D.OrderAux>0

	-- aplico los minimos por sku
	UPDATE D 
	SET OrderAux = Case when OrderAux>t.[Min] then OrderAux else t.[Min] end 
	from [dcpurchase].[Orders] D
	inner join (
		select SKU,[Min] from [cdbuy].[MinOrderByProduct] 
	) t on t.SKU = D.SKU 
	where D.Date = @date and D.OrderAux>0

	-- Ajuste por DUN
	UPDATE [dcpurchase].[Orders]
	   SET [FinalOrder]=
	case 
		when [OrderAux]<[DUN] Then 
			(
				case when (convert(float,[OrderAux])/convert(float,[DUN])) < 0.3 then [OrderAux]
				else DUN end
			)
		when [OrderAux]=[DUN] Then [OrderAux]
		when [OrderAux]>[DUN] Then 
				( case when convert(int,ceiling([OrderAux]))%dun<>0 
					   then (
								   case when (convert(float,convert(int,ceiling([OrderAux]))%dun)/ convert(float,dun)) > 0.15
										then convert(int,ceiling([OrderAux])) + dun - convert(int,ceiling([OrderAux]))%dun
										else convert(int,ceiling([OrderAux])) - convert(int,ceiling([OrderAux]))%dun end
							)
					   else convert(int,ceiling([OrderAux])) end ) 
		else [OrderAux] 
	end
	where Date = @date
	
	-- Ajuste por Pallet
	-- ranking A,B
	UPDATE [dcpurchase].[Orders]
	SET [OrderAux]=
		case 
			WHEN [OrderAux]%[Pallet]> 0.5*Pallet THEN [OrderAux]-[OrderAux]%[Pallet]+[Pallet]
			ELSE [OrderAux]-[OrderAux]%[Pallet]
		end
	where Date = @date and Pallet is not null and Pallet > 1
		and SKU in (SELECT SKU FROM products.Products WHERE TotalRanking in ('A','B'))
	
	-- ranking C,D, Null
	UPDATE [dcpurchase].[Orders]
	SET [OrderAux]=
		case 
			WHEN [OrderAux]%[Pallet]> 0.8*Pallet THEN [OrderAux]-[OrderAux]%[Pallet]+[Pallet]
			ELSE [OrderAux]-[OrderAux]%[Pallet]
		end
	where Date = @date and Pallet is not null and Pallet > 1
		and SKU in (SELECT SKU FROM products.Products WHERE TotalRanking not in ('A','B'))
	
	UPDATE [dcpurchase].[Orders] SET [FinalOrder]=[OrderAux] where Date = @date and Pallet is not null and Pallet > 1

	-- llenar con 0 las ordenes nulas
	UPDATE [dcpurchase].[Orders]
	   SET [FinalOrder] = 0
	where Date = @date and [FinalOrder] is null


	-- ordenes mayores que cero, se 
	UPDATE D 
	SET OrderAux = DOrder,[Order] = DOrder,FinalOrder = DOrder from [dcpurchase].[Orders] D
	inner join 
	(
		select SKU,DOrder from [cdbuy].[DefaultOrderByProduct] 
	) t on t.SKU = D.SKU 
	where D.Date = @date and D.OrderAux>0

	-- se calculan los dias que se estan comprando
	--UPDATE D SET D.BuyingDays = [MAX]/T.fitDiario , DailyFit = T.fitDiario
	--FROM [dcpurchase].[Orders] D INNER JOIN 
	--(
	--	select ProductId,avg(fit)/7.0 quantity
	--		, SUM(convert(float,case when fit<=0 then 1/7 else Fit end))/28 FitDiario
	--	from forecast.PredictedSales 
	--	where DateKey = (select MAX(datekey) from forecast.PredictedSales WHERE Date < @date) 
	--		and Date between dbo.GetMonday(@date) and dateadd(WEEK,3,dbo.GetMonday(@date))
	--	group by ProductId
	--) t on t.ProductId = D.SKU
	--WHERE D.Date = @date and t.quantity > 0

	--Cambio tabla de pronóstico
	UPDATE D SET D.BuyingDays = [MAX]/T.fitDiario , DailyFit = T.fitDiario
	FROM [dcpurchase].[Orders] D INNER JOIN 
	(
		select ProductId,avg(fit) quantity
			, SUM(convert(float,case when fit<=0 then 1/7 else Fit end))/28 FitDiario
		from forecast.PredictedDailySales 
		where DateKey = (select MAX(datekey) from forecast.PredictedDailySales WHERE Date < @date) 
			and Date between dbo.GetMonday(@date) and DATEADD(DAY,6,dateadd(WEEK,3,dbo.GetMonday(@date)))
		group by ProductId
	) t on t.ProductId = D.SKU
	WHERE D.Date = @date and t.quantity > 0
	
	

END
