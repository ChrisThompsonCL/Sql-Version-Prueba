﻿-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2013-08-20
-- Description:	Llena la tabla dcpurchase.ParametersBase con la información necesaria
-- se calcula para la semana siguiente
-- =============================================
CREATE PROCEDURE [dcpurchase].[FillParametersBase] 
	@date smalldatetime --debe ser un lunes
AS
BEGIN
	SET NOCOUNT ON;
	
	-- se borra la tabla
	truncate table dcpurchase.ParametersBase
	
	-- se cargan todos los productos a los cuales se dara sugerencia de compra 
	insert into dcpurchase.ParametersBase(ProductId)
	select productId from [function].GetOperativeCombination(4,0,3,3,0)

	update b set b.DayOfPurchase = params.DayofPurchase,b.Frequency = params.Frequency from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId
	inner join  dcpurchase.PurchaseParameters params on params.Division = p.Division and params.ManufacturerId = p.ManufacturerId
	
	update b set b.DayOfPurchase = params.DayofPurchase,b.Frequency = params.Frequency from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId
	inner join  dcpurchase.PurchaseParameters params on params.Division = p.Division 
	where params.ManufacturerId = 0
	
	-- se carga la fecha actual, como fecha de compra
	update b set b.PurchaseDay = dateadd(day,b.DayOfPurchase-2,dbo.GetMonday(@date)) from dcpurchase.ParametersBase b 
	update b set b.NextPurchaseDay = dateadd(day,b.Frequency,PurchaseDay) from dcpurchase.ParametersBase b 
	
	-- caso A
	update b set b.NextPurchaseDay = dateadd(day,b.Frequency,b.NextPurchaseDay) from dcpurchase.ParametersBase b 
	where @date>b.PurchaseDay and DATEDIFF(day,@date,b.NextPurchaseDay)<=3  
	
	-- caso b
	update b set b.NextPurchaseDay = b.PurchaseDay from dcpurchase.ParametersBase b 
	where @date<b.PurchaseDay and DATEDIFF(day,@date,b.PurchaseDay)>3
	
	update b set b.PurchaseDay = @date from dcpurchase.ParametersBase b 
	
	-- leadtime y fillrate
	update b set b.Leadtime = CEILING(p.LeadtimeAvg),b.FillRate = p.FillrateAvg from dcpurchase.ParametersBase b 
	inner join dcpurchase.DeliveryInformationByProducts p on b.ProductId = p.SKU
	
	-- se ajusta leadtime a la cota máxima
	update b set b.Leadtime = 10 from dcpurchase.ParametersBase b
	where b.Leadtime>10 
	
	-- se ajusta el fillrate a la cota máxima
	update b set b.FillRate = 0.75 from dcpurchase.ParametersBase b
	where b.FillRate<0.75 
	
	-- se ajustan los que no tienen datos
	update b set b.Leadtime = 3, b.FillRate=1 from dcpurchase.ParametersBase b
	where b.Leadtime is null
	
	-- se agregan las excepciones de laboratorios, con esto se cubren compras de dos semanas o tres, etc
	update b set b.NextPurchaseDay = t.nextpurchaseday from dcpurchase.ParametersBase b 
	inner join products.Products p on p.SKU = b.ProductId
	inner join dcpurchase.PurchaseParametersExceptions  t on t.ManufacturerId = p.ManufacturerId
	where t.EventDate = @date 
	
	-- se agrega la proteccion
	update b set b.Protection = params.ServiceLevel from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId
	inner join  dcpurchase.ProtectionParameters params on params.Division = p.Division and params.Ranking = p.TotalRanking
	
	update b set b.Protection = params.ServiceLevel from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId
	inner join  dcpurchase.ProtectionParameters params on params.Division = p.Division and ((params.Ranking = p.TotalRanking) or (params.Ranking is null and p.TotalRanking is null))
	
	-- excepcion de los motivos auges y auge por solicitud
	update b set b.Protection = 0.95 from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId where p.MotiveId in (10)
	
	update b set b.Protection = 0.98 from dcpurchase.ParametersBase b
	inner join products.Products p on p.SKU = b.ProductId where p.MotiveId in (9)
	
	-- se agregan las excepciones
	update b set b.Protection = p.ServiceLevel from dcpurchase.ParametersBase b
	inner join dcpurchase.protectionExceptions p on p.SKU = b.ProductId
	where p.BeginDate<=@date and (p.EndDate>=@date or p.EndDate is null)
	
	-- borra los que no tiene datos de compra por no pertener a la division u otro motivo
	delete p FROM [Salcobrand].[dcpurchase].[ParametersBase] p
	where DayOfPurchase is null 
	
END
