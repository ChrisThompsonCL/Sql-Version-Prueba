﻿CREATE PARTITION SCHEME [PS_ReportStoresSW]
    AS PARTITION [PF_ReportStoresSW]
    TO ([FG_ReportStoresSW201804], [FG_ReportStoresSW201805], [FG_ReportStoresSW201806], [FG_ReportStoresSW201807], [FG_ReportStoresSW201808], [FG_ReportStoresSWBase]);

