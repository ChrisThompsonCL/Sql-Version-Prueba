﻿
CREATE PROCEDURE [forecast].[GetSKUFitCorrected]
@sku int,
@inicio datetime,
@termino datetime

AS
BEGIN

DECLARE  @idfamilia  INT  = (SELECT g.Id FROM products.GenericFamily g inner join  products.GenericFamilyDetail gd on g.Id = gd.GenericFamilyId where gd.SKU = @SKU) 

SELECT 	  
	 @SKU SKU,
	 df.Fecha 
	, df.FitCorregido
FROM ( 
	SELECT * 
	FROM forecast.DailyForecast 
	WHERE sku IN ( 
		SELECT SKU 
		FROM products.GenericFamilyDetail 
		WHERE GenericFamilyId = @idfamilia
		union 
		SELECT @SKU
	) AND Fecha BETWEEN @inicio and @termino
) df 
ORDER BY df.Fecha

END
