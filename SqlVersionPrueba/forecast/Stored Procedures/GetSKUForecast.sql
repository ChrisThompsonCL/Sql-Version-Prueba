﻿
CREATE PROCEDURE [forecast].[GetSKUForecast]
	@IdDivision INT = null,
	@IdCategoria INT = null,
	@IdSubCategoria INT = null,
	@TotalRanking VARCHAR(1) = null,
	@SKU int = null,
	@termino datetime,
	@flags NVARCHAR(200)
AS
BEGIN

	--DECLARE @IdDivision INT
	--DECLARE @IdCategoria INT
	--DECLARE @IdSubCategoria INT
	--DECLARE @TotalRanking VARCHAR(1)
	--DECLARE @SKU int
	--DECLARE @inicio datetime
	--DECLARE @termino datetime
	--DECLARE @flags NVARCHAR(200)


	--SET @IdDivision = 1
	--SET @IdCategoria = NULL
	--SET @IdSubCategoria = NULL
	--SET @TotalRanking = 'B'
	--SET @SKU = NULL
	--SET @inicio = N'20170619'
	--SET @termino = N'20180911'
	--SET @flags = ''




	DECLARE @inicio_local datetime = GETDATE();
	DECLARE @termino_local datetime =  dateadd(month,-2, cast (@termino as date)) 
	DECLARE  @fechaVenta date = (select top 1 [Date] from series.LastSale) 

	DECLARE @flag NVARCHAR(3);
	DECLARE @filtroFlags NVARCHAR(300) = '';
	DECLARE @segmento NVARCHAR(300) = '';
	DECLARE cur01 CURSOR 
	FOR SELECT * FROM SplitString(@flags)
		OPEN cur01  

		FETCH NEXT FROM cur01   
		INTO @flag

		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			SELECT @segmento = CASE 
								WHEN @flag = '1' THEN 'OR p.IsGeneric = 1 '
								WHEN @flag = '2' THEN 'OR p.Ges = 1 '
								WHEN @flag = '3' THEN 'OR p.Procuidado = 1 '
								WHEN @flag = '4' THEN 'OR p.IsInfaltable = 1 '
								WHEN @flag = '0' THEN 'OR 1 = 1 '
							END
			SET @filtroFlags = @filtroFlags + @segmento
			FETCH NEXT FROM cur01   
			INTO @flag
		END   
		CLOSE cur01;  
	DEALLOCATE cur01; 
	IF(LEN(@filtroFlags)>0)
		SET @filtroFlags = CONCAT(' AND (',STUFF(@filtroFlags,1,3,''),')')
	DECLARE @QuerySKU NVARCHAR(4000);
	CREATE TABLE #TempSKU (SKU int primary key)
	SET @QuerySKU = CONCAT('INSERT INTO #TempSKU SELECT	p.SKU
					FROM [products].[Products] p
					WHERE 1 = 1',
						(SELECT CASE WHEN @IdDivision IS NOT NULL THEN CONCAT(' AND ','p.[DivisionUnificadaId] = ',CAST(@IdDivision as varchar)) ELSE '' END),
						(SELECT CASE WHEN @IdCategoria IS NOT NULL THEN CONCAT(' AND ','p.[CategoriaUnificadaId] = ',CAST(@IdCategoria as varchar)) ELSE '' END),
						(SELECT CASE WHEN @IdSubCategoria IS NOT NULL THEN CONCAT(' AND ','p.[SubcategoriaUnificadaId] = ',CAST(@IdSubCategoria as varchar)) ELSE '' END),
						(SELECT CASE WHEN @TotalRanking  IS NOT NULL THEN CONCAT(' AND ','p.[TotalRanking] = ''',CAST(@TotalRanking as varchar),'''') ELSE '' END),
						(SELECT CASE WHEN @SKU IS NOT NULL THEN CONCAT(' AND ','p.[SKU] = ',CAST(@SKU as varchar)) ELSE '' END),
						@filtroFlags)
	EXEC(@QuerySKU)
	
	CREATE TABLE #SKUS (SKU int primary key)

	INSERT INTO #SKUS 
	SELECT SKU
	FROM (SELECT SKU FROM #TempSKU
				UNION
			SELECT gd.SKU
			FROM products.GenericFamily g
			INNER JOIN products.GenericFamilyDetail gd on g.Id = gd.GenericFamilyId 
			WHERE gd.SKU IN (SELECT SKU FROM #TempSKU)
			) skus
	
	--SELECT	ms.[Date],
	--		MAX(ms.Sugerido) Sugerido,
	--		SUM(ms.StockAjustadoSugerido) StockAjustadoSugerido
	--INTO #MetricasStocks
	--FROM reports.MetricasStocks ms
	--INNER JOIN #SKUS sk ON ms.SKU = sk.SKU
	--WHERE [Date] BETWEEN @inicio_local AND @termino_local
	--GROUP BY [Date]

	SELECT	sk.SKU,
			df.Fecha,
			df.Fit,
			df.FitCorregido
			--,
			--CASE 
			--	WHEN Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN df.Venta
			--	ELSE NULL 
			--END Venta,
			--CASE 
			--	WHEN Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN df.Ingreso
			--	ELSE NULL 
			--END Ingreso,
			--CASE
			--	WHEN Venta = 0 THEN 0 
			--	ELSE CAST(df.Ingreso/df.Venta AS FLOAT) 
			--END Precio,
			--df.FeriadoIrrenunciable,
			--df.Feriado,
			--df.Navidad,
			--df.FiestasPatrias,
			--df.DiaMadre,
			--df.DiaNino,
			--df.DiaPadre,
			--df.SanValentin,
			--df.InicioClases,
			--df.Halloween,
			--df.SemanaSanta,
			--df.AnoNuevo,
			--CASE 
			--	WHEN df.Venta IS NOT NULL  and df.Fecha <= @fechaVenta THEN df.Quiebre
			--	ELSE NULL 
			--END Quiebre,
			--CASE 
			--	WHEN df.Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN df.Cobertura
			--	ELSE NULL 
			--END Cobertura,
			--df.Promocion Promocion,
			--CAST(ms.Sugerido AS FLOAT) as SugeridoPromedio,
			--CAST(ms.StockAjustadoSugerido AS FLOAT) as StockAjustadoPromedio
	FROM forecast.DailyForecast df
	INNER JOIN #SKUS sk ON df.SKU = sk.SKU
	--LEFT JOIN #MetricasStocks ms ON df.Fecha = ms.[Date] 
	WHERE df.Fecha BETWEEN @inicio_local AND @termino_local

	--DROP TABLE #MetricasStocks
	DROP TABLE #SKUS
	DROP TABLE #TempSKU
end