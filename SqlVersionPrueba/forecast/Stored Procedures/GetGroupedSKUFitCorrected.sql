﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [forecast].[GetGroupedSKUFitCorrected]
	@IdDivision INT = null,
	@IdCategoria INT = null,
	@IdSubCategoria INT = null,
	@TotalRanking VARCHAR(1) = null,
	@SKU int = null,
	@termino datetime,
	@flags NVARCHAR(200)
AS
BEGIN

	--DECLARE @IdDivision INT
	--DECLARE @IdCategoria INT
	--DECLARE @IdSubCategoria INT
	--DECLARE @TotalRanking VARCHAR(1)
	--DECLARE @SKU int
	--DECLARE @termino datetime
	--DECLARE @flags NVARCHAR(200)


	--SET @IdDivision = 1
	--SET @IdCategoria = NULL
	--SET @IdSubCategoria = NULL
	--SET @TotalRanking = 'B'
	--SET @SKU = NULL
	--SET @termino = '20180911'
	--SET @flags = NULL


	DECLARE @inicio_local datetime = GETDATE();
	DECLARE @termino_local datetime = @termino

	DECLARE  @fechaVenta date = (select top 1 [Date] from series.LastSale) 

	DECLARE @flag NVARCHAR(3);
	DECLARE @filtroFlags NVARCHAR(300) = '';
	DECLARE @segmento NVARCHAR(300) = '';
	DECLARE cur01 CURSOR 
	FOR SELECT * FROM SplitString(@flags)
		OPEN cur01  

		FETCH NEXT FROM cur01   
		INTO @flag

		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			SELECT @segmento = CASE 
								WHEN @flag = '1' THEN 'OR p.IsGeneric = 1 '
								WHEN @flag = '2' THEN 'OR p.Ges = 1 '
								WHEN @flag = '3' THEN 'OR p.Procuidado = 1 '
								WHEN @flag = '4' THEN 'OR p.IsInfaltable = 1 '
								WHEN @flag = '0' THEN 'OR 1 = 1 '
							END
			SET @filtroFlags = @filtroFlags + @segmento
			FETCH NEXT FROM cur01   
			INTO @flag
		END   
		CLOSE cur01;  
	DEALLOCATE cur01; 
	IF(LEN(@filtroFlags)>0)
		SET @filtroFlags = CONCAT(' AND (',STUFF(@filtroFlags,1,3,''),')')

	DECLARE @QuerySKU NVARCHAR(4000);
	CREATE TABLE #TempSKU (SKU int primary key)
	SET @QuerySKU = CONCAT('INSERT INTO #TempSKU SELECT	p.SKU
					FROM [products].[Products] p
					WHERE 1 = 1',
						(SELECT CASE WHEN @IdDivision IS NOT NULL THEN CONCAT(' AND ','p.[DivisionUnificadaId] = ',CAST(@IdDivision as varchar)) ELSE '' END),
						(SELECT CASE WHEN @IdCategoria IS NOT NULL THEN CONCAT(' AND ','p.[CategoriaUnificadaId] = ',CAST(@IdCategoria as varchar)) ELSE '' END),
						(SELECT CASE WHEN @IdSubCategoria IS NOT NULL THEN CONCAT(' AND ','p.[SubcategoriaUnificadaId] = ',CAST(@IdSubCategoria as varchar)) ELSE '' END),
						(SELECT CASE WHEN @TotalRanking  IS NOT NULL THEN CONCAT(' AND ','p.[TotalRanking] = ''',CAST(@TotalRanking as varchar),'''') ELSE '' END),
						(SELECT CASE WHEN @SKU IS NOT NULL THEN CONCAT(' AND ','p.[SKU] = ',CAST(@SKU as varchar)) ELSE '' END),
						@filtroFlags,
					' ORDER BY SKU')
	EXEC(@QuerySKU)

	SELECT	ts.SKU,
			df.Fecha,
			df.FitCorregido
	FROM forecast.DailyForecast df
	INNER JOIN #TempSKU ts ON df.SKU = ts.SKU
	WHERE df.Fecha BETWEEN @inicio_local and @termino_local
	ORDER BY df.Fecha,ts.SKU

	DROP TABLE #TempSKU

	end
