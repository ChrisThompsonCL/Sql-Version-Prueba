﻿-- =============================================
-- Author:		Jorge Ibacache Miquel
-- Create date: 2018-01-18
-- Description:	Procedimiento para la carga de pronósticos diarios por SKU

--Modificado Juan M Hernandez 2018-02-06 

-- =============================================
CREATE PROCEDURE [forecast].[CargaDailyForecast] 
	-- Add the parameters for the stored procedure here
	@fechaInicio date ,
	@fechaFin date,
	@skuObjetivo INT = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @fechaIniciAux date= DATEADD(MONTH, -1, @fechaInicio) 
	Declare @fechaVenta date = (select top 1 [Date] from series.LastSale)
	-- realizamos una copia de [view].[PromotionView] 

	SELECT  * 
	INTO #PromotionView
	FROM [view].[PromotionView]


	-- SE RESPALDA LA INFORMACIÓN DE DATOS CON FIT MODIFICADO EN TABLA TEMPORAL
	SELECT SKU, Fecha, FitCorregido 
	INTO #dataFitModificado
	FROM [forecast].DailyForecast df
	WHERE Fit != FitCorregido
		AND (@skuObjetivo IS NULL OR @skuObjetivo = df.SKU)


	DELETE [forecast].DailyForecast
	where Fecha >= @fechaIniciAux
		AND (@skuObjetivo IS NULL OR @skuObjetivo = SKU)

	CREATE TABLE #tempPredictedDailySales
	(
			DateKey date,
			ProductId int,
			[Date] date,
			Fit float,
			MaxValue float,
			MaxMean float,
			Corrected float,
			CorrectedFit float,
			R int 

	)
	create CLUSTERED Index  i on #tempPredictedDailySales (ProductId, [Date])

	INSERT INTO #tempPredictedDailySales
	SELECT  * 
		FROM  (
			  SELECT ps.*, ROW_NUMBER() OVER (partition by  [Date], ProductId  ORDER BY DateKey DESC) R
			  FROM forecast.PredictedDailySales ps 
			  INNER JOIN products.Products p on ps.ProductId = p.SKU
			  WHERE [Date] between @fechaIniciAux and  @fechaFin
				AND (@skuObjetivo IS NULL OR @skuObjetivo = p.SKU)
			) a
	WHERE R = 1 


	CREATE TABLE #tablatemp
	(
		Fecha date,
		Domingo int, Lunes int,	Martes int,	Miercoles int, Jueves int, Viernes int,	Sabado int,	
		Enero int, Febrero int,	Marzo int,	Abril int,	Mayo int, Junio int, Julio int, Agosto int, Septiembre int, Octubre int, Noviembre int, Diciembre int,
		FeriadoIrrenunciable int, AntesFeriadosI int, DespuesFeriadosI int,	Feriado int, AntesFeriados int,	DespuesFeriados int,		
		Navidad int,
		FiestasPatrias int,
		DiaMadre int, DiaNino int, DiaPadre int, SanValentin int,
		InicioClases int,
		Halloween int,
		SemanaSanta int,
		AnoNuevo int,
		FinMes int,
		Venta int, Ingreso int,
		Quiebre float, Cobertura int,
		Promocion int, Filtro int,
		PromocionInformada int--, Recetas int
	)

	DECLARE @total INT = (SELECT COUNT(*) FROM salcobrand.products.products p INNER JOIN [dbo].[RelacionIdCombinacion] rg ON p.sku = rg.idCodigoCliente);
	DECLARE @current INT = 0;
	DECLARE @msg VARCHAR(MAX);

	--listado de productos con guid
	DECLARE @sku int, @idCombinacion nvarchar(50);
	DECLARE cur01 CURSOR FOR (
		SELECT p.sku,rg.IdCombinacion
		FROM salcobrand.products.products p
		INNER JOIN [dbo].[RelacionIdCombinacion] rg ON p.sku = rg.idCodigoCliente
		WHERE (@skuObjetivo IS NULL OR @skuObjetivo = p.SKU)
	);
	OPEN cur01;
	FETCH NEXT FROM cur01 INTO @sku,@idCombinacion;

	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		SET @msg = 'Procesando SKU ' + CAST(@sku AS VARCHAR(50));
		RAISERROR (@msg, 10, 0) WITH NOWAIT 


		--exec forecast.GetVariableData 8, '2017-01-01', '2018-01-01'
		--Se recorre el cursor y por cada producto se hace la inserción en la tabla temporal, luego se cruza con el pronóstico para obtener otros datos y luego se almacena en test.DailyForecast
		TRUNCATE TABLE #tablatemp
		INSERT INTO #tablatemp
		(
			Fecha,
			Domingo, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado,
			Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre,
			FeriadoIrrenunciable, AntesFeriadosI, DespuesFeriadosI, Feriado, AntesFeriados, DespuesFeriados,
			Navidad,
			FiestasPatrias,
			DiaMadre, DiaNino, DiaPadre,
			SanValentin,
			InicioClases,
			Halloween,
			SemanaSanta,
			AnoNuevo,
			FinMes,
			Venta, Ingreso,
			Quiebre, Cobertura,
			Promocion, Filtro--,PromocionTemp--, Recetas
		)
		EXEC	[forecast].[GetVariableData]
				@producto = @sku,
				@inicio = @fechaIniciAux,
				@fin = @fechaFin
		
		INSERT INTO [forecast].[DailyForecast]
		SELECT	@sku         				,
				tt.Fecha					,
				pdf.Fit					,
				pdf.Fit as [FitCorregido]	,
				tt.[FeriadoIrrenunciable]	,
				tt.[Feriado]				,
				tt.[Navidad]				,
				tt.[FiestasPatrias]			,
				tt.[DiaMadre]				,
				tt.[DiaNino]				,
				tt.[DiaPadre]				,
				tt.[SanValentin]			,
				tt.[InicioClases]			,
				tt.[Halloween]				,
				tt.[SemanaSanta]			,
				tt.[AnoNuevo]				,
				CASE WHEN (tt.fecha > @fechaVenta ) THEN NULL ELSE tt.[Venta] END Venta ,
				tt.[Ingreso]				,
				tt.[Quiebre]				,
				tt.[Cobertura]				,
				tt.[Promocion]              ,-- detectada
				null                        ,
				null						,
				CASE WHEN (pv.PromocionesActivas>0) THEN 1 ELSE 0 END PromocionesActivas
		FROM #tablatemp tt
		left JOIN #tempPredictedDailySales pdf ON pdf.ProductId = @sku  and tt.Fecha = pdf.[Date]
		LEFT JOIN #PromotionView pv on pv.SKU = @sku and pv.Fecha = tt.Fecha

		SET @current = @current + 1;
		IF((@current % 50) = 0)
		BEGIN
			SET @msg = 'Completado ' + CAST(CAST(ROUND(((@current*1.0 / @total)*100), 2) AS NUMERIC(4,2)) AS VARCHAR(MAX)) + ' %%';
			RAISERROR (@msg, 10, 0) WITH NOWAIT 
		END	

		FETCH NEXT FROM cur01 INTO @sku,@idCombinacion;
	END   

	CLOSE cur01;  
	DEALLOCATE cur01;


	-- SE RESTAURA LA INFORMACIÓN DE DATOS CON FIT MODIFICADO DESDE LA TABLA TEMPORAL
	UPDATE df
	SET df.FitCorregido = tt.FitCorregido
	FROM [forecast].[DailyForecast] df
	INNER JOIN #dataFitModificado tt ON df.Fecha = tt.Fecha and df.SKU = tt.SKU

	-- EVALUAR SI SKU CON FIT MODIFICADO PERTENECE A UNA FAMILIA DE GENERICOS
	-- SACAR LOS SKU QUE PERTENECEN A GENERICOS UBICADOS EN LA TABLA DE RESPALDO
	SELECT df.sku, df.FitCorregido, df.Fecha, gf.GenericFamilyId  
	INTO #respaldogenerico
	FROM #dataFitModificado df
	LEFT JOIN products.GenericFamilyDetail gf on df.SKU = gf.SKU
	WHERE GenericFamilyId IS NOT NULL

	-- GENERICOS QUE ESTAN EN [forecast].DAILYFORECAST QUE NO ESTAN EN LA TABLA DE RESPALDO
	SELECT df.SKU, df.Fecha, df.FitCorregido, gf.GenericFamilyId
	INTO #genericosConsultarFamilia
	FROM #dataFitModificado df
	LEFT JOIN products.GenericFamilyDetail gf on df.SKU = gf.SKU
	RIGHT JOIN (	
			SELECT df.SKU
			FROM [forecast].DailyForecast df
			INNER JOIN products.GenericFamilyDetail gf on df.SKU = gf.SKU
			WHERE Fit IS NOT NULL AND FitCorregido IS NOT NULL
	) res ON res.SKU = df.sku
	WHERE GenericFamilyId IS NOT NULL

	-- COINCIDENCIAS POR FAMILIA DE PRODUCTOS 
	SELECT rg.* 
	INTO #coincidencias
	FROM #respaldogenerico rg
	INNER JOIN #genericosConsultarFamilia gf ON  gf.Fecha = rg.Fecha AND rg.GenericFamilyId = gf.GenericFamilyId

	--aCTUALIZO
	UPDATE df
	SET df.FitCorregido = tt.FitCorregido
	FROM [forecast].[DailyForecast] df
	INNER JOIN #coincidencias tt ON df.Fecha = tt.Fecha and df.SKU = tt.SKU

	
	DROP TABLE #dataFitModificado
	DROP TABLE #tablatemp

END
