﻿-- ==========================================================================================
-- Author:    Carlos Quappe
-- Create date: 2018-03-13
-- Description: Encontrar en que fechas existió una promoción en el pasado
-- Deseable: Lograr que dos promociones separadas por "pocos" días de no promoción, se tomen 
-- como una sola (ver comentario antes de rellenar #temp)
-- Falta documentar la parte de la media móvil.
-- No se está entrando al primer If a proposito (se comportaba bien antes, ahora no se ha 
-- probado)
-- ==========================================================================================
create PROCEDURE forecast.[DetectPromotions]
-- Add the parameters for the stored procedure here
@sku int,
@beginning_date date,
@finishing_date date
AS
BEGIN
  SET NOCOUNT ON;

  /************** Parámetros de Prueba **************/
  /*SET @sku = 7420062
  SET @beginning_date = '2015-01-01'
  SET @finishing_date = dateadd(dd, 1, getdate())*/
  /**************************************************/

  -- Tabla temporal #sales para no manejar en memoria datos innecesarios
  CREATE TABLE #sales (
    [Date] datetime,
    [Quantity] float,
    [Income] float
  )

  -- Tabla temporal #return, que guarda los resultados obtenidos por
  -- el procedimiento. Para cada fecha se entrega un int de retorno, 
  -- donde 0 significa que no hubo promoción, y un entero positivo 
  -- señala que sí hubo, y cuál fue su grado.
  CREATE TABLE #return (
    [Date] datetime,
    [Quantity] float,
    [Income] float,
    [Promotion] int
  )

  -- Se llena #sales con los datos correspondientes
  INSERT INTO #sales
    SELECT
      [Fecha],
      ISNULL([Quantity], 0),
      ISNULL([Income], NULL)
    FROM (SELECT
      [SKU],
      [Date],
      [Quantity],
      [Income]
    FROM [series].[DailySales]
    WHERE [SKU] = @sku) ds
    RIGHT JOIN [forecast].[GenericVariables] gv
      ON ds.[Date] = gv.[Fecha]
    WHERE [Fecha] >= @beginning_date
    AND [Fecha] <= @finishing_date

  -- Se llena #return con los datos de #sales
  INSERT INTO #return
    SELECT
      [Date],
      [Quantity],
      [Income],
      NULL
    FROM #sales

  -- Se obtienen los estadísticos promedio y desviación estándar, tanto para cantidad como para precio
  DECLARE @average float = (SELECT
    AVG([Quantity])
  FROM #sales)
  DECLARE @std_dev float = (SELECT
    STDEV([Quantity])
  FROM #sales)
  --DECLARE @average_p float = (SELECT avg(CASE WHEN [Quantity] > 0 THEN [Income] / [Quantity] ELSE NULL END) FROM #sales)
  --DECLARE @std_dev_p float = (SELECT stdev(CASE WHEN [Quantity] > 0 THEN [Income] / [Quantity] ELSE NULL END) FROM #sales)

  -- Este valor me permite decidir cuantas bandas habrán como máximo
  DECLARE @max_bandas int = 3

  -- Este es el factor por el que tengo que dividir para que nunca hayan más de @max_bandas niveles de prmoción
  DECLARE @max_qty float = (SELECT
    MAX([Quantity])
  FROM #return)
  DECLARE @max_v float = (SELECT
    CASE
      WHEN (@std_dev > 0) THEN (FLOOR((@max_qty - @average) / (@std_dev)) + 1)
      ELSE 1
    END)
  / @max_bandas

  -- Si la desviación estándar es grande comparada con la media, significa 
  -- (o podría siginificar) que hay muchos datos significativamente bajo 
  -- la media. Estos serán los datos que no están en promoción.
  IF (@average < 0.75 * @std_dev
    AND @std_dev > 0
    AND @std_dev < 0) -- Eliminar última condición (está hecha para que no entre nunca)
  BEGIN
    SELECT
      1
    UPDATE #return
    SET [Promotion] = 0

    -- Este valor regula que tan exigente es detectar una promoción (a mayor valor, menos promociones marca)
    DECLARE @exig int = 3

    -- Al dato, le resto el promedio*exigencia y lo divido por la desviación estándar. Si a esto le saco el piso y le sumo 1,
    -- tendría un número indefinido de bandas de promoción, que indican el nivel de promoción. Al dividir por 
    -- @max_v antes de sacar el piso, acoto a que los niveles de promoción estén entre 0 y @max_bandas
    UPDATE #return
    SET [Promotion] = FLOOR(([Quantity] - @exig * @average) / (@std_dev * @max_v)) + 1
    WHERE @std_dev * @max_v > 0

    -- Para cada período en promoción, quiero quedarme con la moda del período (más que tener distintos niveles 
    -- en una solo promoción). Recuperado de: https://stackoverflow.com/questions/49265761/sql-server-split-rows-in-a-specific-column
    -- Nos gustaría dejar en 1 los 0's que separan promociones (e.g. 5 días de promoción, 1 día marcado como 0, 7 días promoción 
    -- que queden como 13 días de promoción)
    SELECT
      #return.[Date],
      #return.[Quantity],
      #return.[Income],
      (CASE
        WHEN [Promotion] = 0 THEN 0
        ELSE FIRST_VALUE([Promotion]) OVER (PARTITION BY grp ORDER BY cnt DESC)
      END) AS [Promotion] INTO #temp
    FROM (SELECT
      #return.[Date],
      #return.[Quantity],
      #return.[Income],
      #return.[Promotion],
      #return.grp,
      (CASE
        WHEN [Promotion] = 0 THEN 0
        ELSE COUNT(*) OVER (PARTITION BY grp, [Promotion])
      END) AS cnt
    FROM (SELECT
      #return.*,
      SUM(CASE
        WHEN [Promotion] = 0 THEN 1
        ELSE 0
      END) OVER (ORDER BY [Date]) AS grp
    FROM #return) #return) #return

    TRUNCATE TABLE #return

    INSERT INTO #return
      SELECT
        [Date],
        [Quantity],
        [Income],
        [Promotion]
      FROM #temp

    DROP TABLE #temp

  END

  -- Si la desviación no es tan grande, vemos los casos en que aumentó un poco la demanda junto con una bajada de precio (respecto a una ventana 
  -- móvil de 20 días hacia atrás y 20 días hacia adelante)
  ELSE
  BEGIN

    -- Creamos tabla auxiliar para trabajar con las medias moviles
    CREATE TABLE #return_aux (
      [Date] datetime,
      [Quantity] float,
      [Income] float,
      [Promotion] int,
      [InformedPromotion] int,
      [Quantity_SMA_Past_30] float,
      [Quantity_SMA_Future_30] float,
      [Price_SMA_Past_30] float,
      [Price_SMA_Future_30] float,
      [Quantity_SMA_Past_60] float,
      [Quantity_SMA_Future_60] float,
      [Price_SMA_Past_60] float,
      [Price_SMA_Future_60] float,
      [Quantity_SMA_Past_120] float,
      [Quantity_SMA_Future_120] float,
      [Price_SMA_Past_120] float,
      [Price_SMA_Future_120] float
    )

    -- Llenamos la tabla auxiliar
    INSERT INTO #return_aux
      SELECT
        [Date],
        [Quantity],
        [Income],
        [Promotion],
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
      FROM #return

    -- Truncamos tabla de retorno
    TRUNCATE TABLE #return

    -- La siguiente sección está basada en http://www.sqlservercentral.com/articles/Moving+Average/69389/

    -- No estoy seguro para qué se hace esto, supongo que con objetivos de eficiencia
    CREATE CLUSTERED INDEX ix_goog ON #return_aux ([Date])

    -- Creamos tabla temporal que tiene el precio de un día, de algunos días en el pasado y de algunos días en el futuro (30, 60 y 120)
	CREATE TABLE #mod_data (n int, Date date, [Quantity] float, [Price] float, [Income] int, [sma_q_past_30] float, [sma_q_future_30] float, [sma_p_past_30] float, [sma_p_future_30] float, 
						   [sma_q_past_60] float, [sma_q_future_60] float, [sma_p_past_60] float, [sma_p_future_60] float, [sma_q_past_120] float, [sma_q_future_120] float, 
						   [sma_p_past_120] float, [sma_p_future_120] float, [30_day_prev_q] float, [30_day_forw_q] float, [30_day_prev_p] float, [30_day_forw_p] float, [60_day_prev_q] float, 
						   [60_day_forw_q] float, [60_day_prev_p] float, [60_day_forw_p] float, [120_day_prev_q] float, [120_day_forw_q] float, [120_day_prev_p] float, [120_day_forw_p] float)

    -- Creamos una CTE para poder obtener la enésima fila
    --;
    --WITH t1
    --AS (SELECT
    --  ROW_NUMBER() OVER (ORDER BY [Date]) [n],
    --  [Date],
    --  [Quantity],
    --  CASE
    --    WHEN [Quantity] > 0 THEN CAST([Income] / [Quantity] AS float)
    --    ELSE NULL
    --  END [Price],
    --  [Income]
    --FROM #return_aux)
	
	  --CREATE CLUSTERED INDEX ix_goog ON #return_aux ([Date])

	  CREATE TABLE #t1 (n int, [n-30] int, [n+30] int, [n-60] int, [n+60] int, [n-120] int, [n+120] int, date date, quantity float, price float, income int, primary key (n))
	  CREATE NONCLUSTERED INDEX [ix_n-30] ON #t1 ([n-30])
	  CREATE NONCLUSTERED INDEX [ix_n+30] ON #t1 ([n+30])
	  CREATE NONCLUSTERED INDEX [ix_n-60] ON #t1 ([n-60])
	  CREATE NONCLUSTERED INDEX [ix_n+60] ON #t1 ([n+60])
	  CREATE NONCLUSTERED INDEX [ix_n-120] ON #t1 ([n-120])
	  CREATE NONCLUSTERED INDEX [ix_n+120] ON #t1 ([n+120])

	  INSERT INTO #t1
	  SELECT
      ROW_NUMBER() OVER (ORDER BY [Date]) [n],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))-30 [n-30],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))+30 [n+30],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))-60 [n-60],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))+60 [n+60],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))-120 [n-120],
	  (ROW_NUMBER() OVER (ORDER BY [Date]))+120 [n+120],
      [Date],
      [Quantity],
      CASE
        WHEN [Quantity] > 0 THEN CAST([Income] / [Quantity] AS float)
        ELSE NULL
      END [Price],
      [Income]
    FROM #return_aux


	
	INSERT INTO #mod_data
    SELECT
      a.n,
      a.[Date],
      a.[Quantity],
      a.[Price],
      a.[Income],
      CAST(NULL AS float) [sma_q_past_30],
      CAST(NULL AS float) [sma_q_future_30],
      CAST(NULL AS float) [sma_p_past_30],
      CAST(NULL AS float) [sma_p_future_30],
      CAST(NULL AS float) [sma_q_past_60],
      CAST(NULL AS float) [sma_q_future_60],
      CAST(NULL AS float) [sma_p_past_60],
      CAST(NULL AS float) [sma_p_future_60],
      CAST(NULL AS float) [sma_q_past_120],
      CAST(NULL AS float) [sma_q_future_120],
      CAST(NULL AS float) [sma_p_past_120],
      CAST(NULL AS float) [sma_p_future_120],
      CAST(b.[Quantity] AS float) [30_day_prev_q],
      CAST(c.[Quantity] AS float) [30_day_forw_q],
      CAST(b.[Price] AS float) [30_day_prev_p],
      CAST(c.[Price] AS float) [30_day_forw_p],
      CAST(d.[Quantity] AS float) [60_day_prev_q],
      CAST(e.[Quantity] AS float) [60_day_forw_q],
      CAST(d.[Price] AS float) [60_day_prev_p],
      CAST(e.[Price] AS float) [60_day_forw_p],
      CAST(f.[Quantity] AS float) [120_day_prev_q],
      CAST(g.[Quantity] AS float) [120_day_forw_q],
      CAST(f.[Price] AS float) [120_day_prev_p],
      CAST(g.[Price] AS float) [120_day_forw_p]
    FROM #t1 a
    LEFT JOIN #t1 b
      ON a.[n-30] = b.n
    LEFT JOIN #t1 c
      ON a.[n+30] = c.n
    LEFT JOIN #t1 d
      ON a.[n-60] = d.n
    LEFT JOIN #t1 e
      ON a.[n+60] = e.n
    LEFT JOIN #t1 f
      ON a.[n-120] = f.n
    LEFT JOIN #t1 g
      ON a.[n+120] = g.n

    DECLARE @intervals_30 int = 30
    DECLARE @intervals_60 int = 60
    DECLARE @intervals_120 int = 120


    DECLARE @initial_sum_q_30 float,
            @initial_sum_q_60 float,
            @initial_sum_q_120 float,
            @initial_sum_p_30 float,
            @initial_sum_p_60 float,
            @initial_sum_p_120 float
    DECLARE @moving_sum_q_future_30 float,
            @moving_sum_q_past_30 float,
            @moving_sum_p_future_30 float,
            @moving_sum_p_past_30 float
    DECLARE @moving_sum_q_future_60 float,
            @moving_sum_q_past_60 float,
            @moving_sum_p_future_60 float,
            @moving_sum_p_past_60 float
    DECLARE @moving_sum_q_future_120 float,
            @moving_sum_q_past_120 float,
            @moving_sum_p_future_120 float,
            @moving_sum_p_past_120 float

    DECLARE @average_price float = (SELECT
      AVG([Price])
    FROM #mod_data
    WHERE Quantity > 0)


    SELECT
      @initial_sum_q_30 = SUM([Quantity])
    FROM #mod_data
    WHERE n <= @intervals_30
    SELECT
      @initial_sum_q_60 = SUM([Quantity])
    FROM #mod_data
    WHERE n <= @intervals_60
    SELECT
      @initial_sum_q_120 = SUM([Quantity])
    FROM #mod_data
    WHERE n <= @intervals_120
    SELECT
      @initial_sum_p_30 = ISNULL(SUM([Price]), @average_price * @intervals_30)
    FROM #mod_data
    WHERE n <= @intervals_30
    SELECT
      @initial_sum_p_60 = ISNULL(SUM([Price]), @average_price * @intervals_60)
    FROM #mod_data
    WHERE n <= @intervals_60
    SELECT
      @initial_sum_p_120 = ISNULL(SUM([Price]), @average_price * @intervals_120)
    FROM #mod_data
    WHERE n <= @intervals_120

    UPDATE t1
    SET @moving_sum_q_past_30 =
                               CASE
                                 WHEN n = 1 THEN @initial_sum_q_30
                                 ELSE @moving_sum_q_past_30 + [Quantity] - ISNULL([30_day_prev_q], [Quantity])
                               END,
        @moving_sum_q_future_30 =
                                 CASE
                                   WHEN n = 1 THEN @initial_sum_q_30
                                   ELSE @moving_sum_q_future_30 - [Quantity] + ISNULL([30_day_forw_q], [Quantity])
                                 END,
        @moving_sum_p_past_30 =
                               CASE
                                 WHEN n = 1 THEN @initial_sum_p_30
                                 ELSE @moving_sum_p_past_30 + ISNULL([Price], @moving_sum_p_past_30 / @intervals_30) - ISNULL([30_day_prev_p], @moving_sum_p_past_30 / @intervals_30)
                               END,
        @moving_sum_p_future_30 =
                                 CASE
                                   WHEN n = 1 THEN @initial_sum_p_30
                                   ELSE @moving_sum_p_future_30 - ISNULL([Price], @moving_sum_p_future_30 / @intervals_30) + ISNULL([30_day_forw_p], @moving_sum_p_future_30 / @intervals_30)
                                 END,
        @moving_sum_q_past_60 =
                               CASE
                                 WHEN n = 1 THEN @initial_sum_q_60
                                 ELSE @moving_sum_q_past_60 + [Quantity] - ISNULL([60_day_prev_q], [Quantity])
                               END,
        @moving_sum_q_future_60 =
                                 CASE
                                   WHEN n = 1 THEN @initial_sum_q_60
                                   ELSE @moving_sum_q_future_60 - [Quantity] + ISNULL([60_day_forw_q], [Quantity])
                                 END,
        @moving_sum_p_past_60 =
                               CASE
                                 WHEN n = 1 THEN @initial_sum_p_60
                                 ELSE @moving_sum_p_past_60 + ISNULL([Price], @moving_sum_p_past_60 / @intervals_60) - ISNULL([60_day_prev_p], @moving_sum_p_past_60 / @intervals_60)
                               END,
        @moving_sum_p_future_60 =
                                 CASE
                                   WHEN n = 1 THEN @initial_sum_p_60
                                   ELSE @moving_sum_p_future_60 - ISNULL([Price], @moving_sum_p_future_60 / @intervals_60) + ISNULL([60_day_forw_p], @moving_sum_p_future_60 / @intervals_60)
                                 END,
        @moving_sum_q_past_120 =
                                CASE
                                  WHEN n = 1 THEN @initial_sum_q_120
                                  ELSE @moving_sum_q_past_120 + [Quantity] - ISNULL([120_day_prev_q], [Quantity])
                                END,
        @moving_sum_q_future_120 =
                                  CASE
                                    WHEN n = 1 THEN @initial_sum_q_120
                                    ELSE @moving_sum_q_future_120 - [Quantity] + ISNULL([120_day_forw_q], [Quantity])
                                  END,
        @moving_sum_p_past_120 =
                                CASE
                                  WHEN n = 1 THEN @initial_sum_p_120
                                  ELSE @moving_sum_p_past_120 + ISNULL([Price], @moving_sum_p_past_120 / @intervals_120) - ISNULL([120_day_prev_p], @moving_sum_p_past_120 / @intervals_120)
                                END,
        @moving_sum_p_future_120 =
                                  CASE
                                    WHEN n = 1 THEN @initial_sum_p_120
                                    ELSE @moving_sum_p_future_120 - ISNULL([Price], @moving_sum_p_future_120 / @intervals_120) + ISNULL([120_day_forw_p], @moving_sum_p_future_120 / @intervals_120)
                                  END,
        [sma_q_past_30] = @moving_sum_q_past_30 / CAST(@intervals_30 AS float),
        [sma_q_future_30] = @moving_sum_q_future_30 / CAST(@intervals_30 AS float),
        [sma_p_past_30] = @moving_sum_p_past_30 / CAST(@intervals_30 AS float),
        [sma_p_future_30] = @moving_sum_p_future_30 / CAST(@intervals_30 AS float),
        [sma_q_past_60] = @moving_sum_q_past_60 / CAST(@intervals_60 AS float),
        [sma_q_future_60] = @moving_sum_q_future_60 / CAST(@intervals_60 AS float),
        [sma_p_past_60] = @moving_sum_p_past_60 / CAST(@intervals_60 AS float),
        [sma_p_future_60] = @moving_sum_p_future_60 / CAST(@intervals_60 AS float),
        [sma_q_past_120] = @moving_sum_q_past_120 / CAST(@intervals_120 AS float),
        [sma_q_future_120] = @moving_sum_q_future_120 / CAST(@intervals_120 AS float),
        [sma_p_past_120] = @moving_sum_p_past_120 / CAST(@intervals_120 AS float),
        [sma_p_future_120] = @moving_sum_p_future_120 / CAST(@intervals_120 AS float)
    FROM #mod_data t1 WITH (TABLOCKX)
    OPTION (MAXDOP 1)

    TRUNCATE TABLE #return_aux

    INSERT INTO #return_aux
      SELECT
        [Date],
        [Quantity],
        [Income],
        NULL [Promotion],
        CASE
          WHEN [PromocionesActivas] > 0 THEN 1
          ELSE 0
        END [InformedPromotion],
        [sma_q_past_30],
        [sma_q_future_30],
        [sma_p_past_30],
        [sma_p_future_30],
        [sma_q_past_60],
        [sma_q_future_60],
        [sma_p_past_60],
        [sma_p_future_60],
        [sma_q_past_120],
        [sma_q_future_120],
        [sma_p_past_120],
        [sma_p_future_120]
      FROM #mod_data m
      LEFT JOIN (SELECT
        Fecha,
        PromocionesActivas
      FROM [view].[PromotionView]
      WHERE SKU = @sku
      AND Fecha BETWEEN @beginning_date AND @finishing_date) prom
        ON m.[Date] = prom.[Fecha]


    DECLARE @exigencia_precio float = 1.0
    DECLARE @exigencia_nivel_3 float = 2
    DECLARE @exigencia_nivel_2 float = 1.5
    DECLARE @exigencia_nivel_1 float = 1.1

    INSERT INTO #return
      SELECT
        [Date],
        [Quantity],
        [Income],
        CASE
          WHEN [Quantity] > 0 THEN CASE
              WHEN [dbo].[ComparacionMultiple_Precio]([Quantity], [Income], [Price_SMA_Past_30], [Price_SMA_Future_30], [Price_SMA_Past_60], [Price_SMA_Future_60], [Price_SMA_Past_120], [Price_SMA_Future_120], @exigencia_precio, [InformedPromotion]) = 1 THEN CASE
                  WHEN [dbo].[ComparacionMultiple_Venta]([Quantity], [Quantity_SMA_Past_30], [Quantity_SMA_Future_30], [Quantity_SMA_Past_60], [Quantity_SMA_Future_60], [Quantity_SMA_Past_120], [Quantity_SMA_Future_120], @exigencia_nivel_3, 0) = 1 THEN 3
                  WHEN [dbo].[ComparacionMultiple_Venta]([Quantity], [Quantity_SMA_Past_30], [Quantity_SMA_Future_30], [Quantity_SMA_Past_60], [Quantity_SMA_Future_60], [Quantity_SMA_Past_120], [Quantity_SMA_Future_120], @exigencia_nivel_2, 0) = 1 THEN 2
                  WHEN [dbo].[ComparacionMultiple_Venta]([Quantity], [Quantity_SMA_Past_30], [Quantity_SMA_Future_30], [Quantity_SMA_Past_60], [Quantity_SMA_Future_60], [Quantity_SMA_Past_120], [Quantity_SMA_Future_120], @exigencia_nivel_1, [InformedPromotion]) = 1 THEN 1
                  ELSE 0
                END
              ELSE 0
            END
          ELSE 0
        END
        [Promotion]
      FROM #return_aux

    DROP TABLE #mod_data
    DROP TABLE #return_aux

  END

  -- Se eliminan los resultados anteriores
  DELETE FROM [test].[DetectedPromotions]
  WHERE [Sku] = @sku

  -- Se guardan los resultados nuevos
  INSERT INTO [test].[DetectedPromotions]
    SELECT
      @sku [Sku],
      [Date],
      [Promotion]
    FROM #return
    WHERE [Promotion] > 0

  DROP TABLE #sales
  DROP TABLE #return

END