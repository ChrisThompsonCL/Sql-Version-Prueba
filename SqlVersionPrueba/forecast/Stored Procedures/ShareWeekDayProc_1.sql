﻿
-- =============================================
-- Author:		Sebastián Manzano
-- Create date: 2012-06-19
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [forecast].[ShareWeekDayProc]
	-- Add the parameters for the stored procedure here
	@weeks int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @lastDate smalldatetime 
	DECLARE @beginDate smalldatetime

	SELECT @lastDate  = MAX(Date) from series.Sales
	SELECT @beginDate = DATEADD(day, (-@weeks*7)+1, @lastDate)
	
	DECLARE @sales table (SKU int, [Day] int, Quantity float)

	INSERT INTO @sales (SKU, [Day], Quantity) 
	SELECT SKU, DATEPART(WEEKDAY,[Date]), sum(Quantity) 
	FROM series.Sales 
	WHERE [Date] between @beginDate and @lastDate
	GROUP BY SKU, DATEPART(WEEKDAY,[Date]) 
	
	TRUNCATE TABLE forecast.ShareWeekDay
	
	INSERT INTO forecast.ShareWeekDay (ProductId, ProductType, StoreId, StoreType, [Week], Mon, Tue, Wed, Thu, Fri, Sat, Sun)
	SELECT T.SKU,0,1,3, @lastDate [Date], ISNULL(L.QL, 0)/T.Total L, ISNULL(M.QM,0)/T.Total M, ISNULL(W.QW,0)/T.Total W, ISNULL(J.QJ,0)/T.Total J, ISNULL(V.QV,0)/T.Total V, ISNULL(S.QS,0)/T.Total S, ISNULL(D.QD,0)/T.Total D 
	FROM (
		(select SKU, CASE WHEN sum(Quantity) = 0 THEN 1.0 ELSE CONVERT(float,SUM(Quantity)) END Total from @sales group by SKU) T
		LEFT JOIN
			--LUNES
			( select SKU, SUM(Quantity) QL from @sales XL where [DAY] = 2 	GROUP BY SKU
		) L ON T.SKU = L.SKU
		LEFT JOIN
			--MARTES
			( select SKU, SUM(Quantity) QM from @sales XM where [DAY] = 3 	GROUP BY SKU
		) M ON T.SKU = M.SKU
		LEFT JOIN
			--MIERCOLES
			( select SKU, SUM(Quantity) QW from @sales XW where [DAY] = 4	GROUP BY SKU
		) W ON T.SKU=W.SKU
		LEFT JOIN	
			--JUEVES
			( select SKU, SUM(Quantity) QJ from @sales XJ where [DAY] = 5 	GROUP BY SKU
		) J ON T.SKU=J.SKU
		LEFT JOIN
			--VIERNES
			( select SKU, SUM(Quantity) QV from @sales XV where [DAY] = 6	GROUP BY SKU
		) V ON T.SKU=V.SKU
		LEFT JOIN	
			--SABADO
			( select SKU, SUM(Quantity) QS from @sales XS where [DAY] = 7	GROUP BY SKU
		) S ON T.SKU = S.SKU
		LEFT JOIN	
			--DOMINGO
			( select SKU, SUM(Quantity) QD from @sales XD where [DAY] = 1	GROUP BY SKU
		) D ON T.SKU=D.SKU
	)
	ORDER BY SKU
END

