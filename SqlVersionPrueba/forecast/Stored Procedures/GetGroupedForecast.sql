﻿-- =============================================
-- Author:		Jorge Ibacache
-- Create date: 2018-07-07
-- Description:	Obtiene los pronósticos filtrando y agrupando lo que solicita el usuario
--
-- Modified By: Gabriel Espinoza
-- Modified On: 2018-08-07
-- Description: Modified to receive several inputs instead of only 1 per filter
-- =============================================
CREATE PROCEDURE [forecast].[GetGroupedForecast]
	@IdDivision1		NVARCHAR(MAX) = null,
	@IdCategoria1		NVARCHAR(MAX) = null,
	@IdSubCategoria1	NVARCHAR(MAX) = null,
	@TotalRanking1		NVARCHAR(MAX) = null,
	@SKU1				NVARCHAR(MAX) = null,
	@inicio1			datetime,
	@termino1			datetime,
	@flags1				NVARCHAR(MAX)	= NULL,
	@positioningId1		NVARCHAR(MAX)	= NULL,
	@manufacturerId1	NVARCHAR(MAX)	= NULL
AS
BEGIN

	DECLARE @IdDivision NVARCHAR(MAX) = @IdDivision1
	DECLARE @IdCategoria NVARCHAR(MAX) = @IdCategoria1
	DECLARE @IdSubCategoria NVARCHAR(MAX) = @IdSubCategoria1
	DECLARE @TotalRanking NVARCHAR(MAX) = @TotalRanking1
	DECLARE @SKU NVARCHAR(MAX) =@SKU1
	DECLARE @inicio datetime = @inicio1
	DECLARE @termino datetime = @termino1
	DECLARE @flags NVARCHAR(MAX) = @flags1
	DECLARE @positioningId	NVARCHAR(MAX)	= @positioningId1;
	DECLARE @manufacturerId	NVARCHAR(MAX)	= @manufacturerId1;
	/*
	SET STATISTICS TIME OFF;
	SET STATISTICS IO OFF;
	*/
	
	SET NOCOUNT ON;

	--DECLARE @IdDivision		NVARCHAR(MAX)	= NULL;
	--DECLARE @IdCategoria	NVARCHAR(MAX)	= NULL;
	--DECLARE @IdSubCategoria NVARCHAR(MAX)	= NULL;
	--DECLARE @TotalRanking	NVARCHAR(MAX)	= NULL;
	--DECLARE @SKU			NVARCHAR(MAX)	= 1991100;
	--DECLARE @flags			NVARCHAR(MAX)	= NULL;
	--DECLARE @positioningId	NVARCHAR(MAX)	= NULL;
	--DECLARE @manufacturerId	NVARCHAR(MAX)	= NULL;
	--DECLARE @inicio			DATE			= '20170807'
	--DECLARE @termino		DATE			= '20181030'


	DECLARE @inicio_local DATE = @inicio
	DECLARE @termino_local DATE = @termino

	CREATE TABLE #idDivision_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #idDivision_tbl SELECT [Name] FROM dbo.SplitString(@IdDivision) WHERE [Name] IS NOT NULL;
	CREATE TABLE #idCategoria_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #idCategoria_tbl SELECT [Name] FROM dbo.SplitString(@IdCategoria) WHERE [Name] IS NOT NULL;
	CREATE TABLE #idSubCategoria_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #idSubCategoria_tbl SELECT [Name] FROM dbo.SplitString(@IdSubCategoria) WHERE [Name] IS NOT NULL;
	CREATE TABLE #rankings_tbl (Id NVARCHAR(1) NOT NULL PRIMARY KEY);
	INSERT INTO #rankings_tbl SELECT [Name] FROM dbo.SplitString(@TotalRanking) WHERE [Name] IS NOT NULL;
	CREATE TABLE #sku_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #sku_tbl SELECT [Name] FROM dbo.SplitString(@SKU) WHERE [Name] IS NOT NULL;
	CREATE TABLE #positioning_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #positioning_tbl SELECT [Name] FROM dbo.SplitString(@positioningId) WHERE [Name] IS NOT NULL;
	CREATE TABLE #manufacturer_tbl (Id INT NOT NULL PRIMARY KEY);
	INSERT INTO #manufacturer_tbl SELECT [Name] FROM dbo.SplitString(@manufacturerId) WHERE [Name] IS NOT NULL;

	DECLARE  @fechaVenta date = (select top 1 [Date] from series.LastSale) 

	--DECLARE @flags NVARCHAR(100) = '1,2,3,4';

	DECLARE @flag NVARCHAR(3);
	DECLARE @filtroFlags NVARCHAR(300) = '';
	DECLARE @segmento NVARCHAR(300) = '';
	DECLARE cur01 CURSOR 
	FOR SELECT * FROM SplitString(@flags)
		OPEN cur01  

		FETCH NEXT FROM cur01   
		INTO @flag

		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			SELECT @segmento = CASE 
								WHEN @flag = '1' THEN 'OR p.IsGeneric = 1 '
								WHEN @flag = '2' THEN 'OR p.Ges = 1 '
								WHEN @flag = '3' THEN 'OR p.Procuidado = 1 '
								WHEN @flag = '4' THEN 'OR p.IsInfaltable = 1 '
								WHEN @flag = '0' THEN 'OR 1 = 1 '
							END
			SET @filtroFlags = @filtroFlags + @segmento
			FETCH NEXT FROM cur01   
			INTO @flag
		END   
		CLOSE cur01;  
	DEALLOCATE cur01; 
	IF(LEN(@filtroFlags)>0)
		SET @filtroFlags = CONCAT(' AND (',STUFF(@filtroFlags,1,3,''),')')

	DECLARE @QuerySKU NVARCHAR(MAX);
	CREATE TABLE #TempSKU (SKU int NOT NULL PRIMARY KEY)
	SET @QuerySKU = CONCAT('INSERT INTO #TempSKU SELECT	p.SKU
					FROM [products].[Products] p
					WHERE 1 = 1',
						(SELECT CASE WHEN @IdDivision	  IS NOT NULL THEN ' AND p.[DivisionUnificadaId] IN (SELECT Id FROM #idDivision_tbl)' ELSE '' END),
						(SELECT CASE WHEN @IdCategoria	  IS NOT NULL THEN ' AND p.[CategoriaUnificadaId] IN (SELECT Id FROM #idCategoria_tbl)' ELSE '' END),
						(SELECT CASE WHEN @IdSubCategoria IS NOT NULL THEN ' AND p.[SubcategoriaUnificadaId] IN (SELECT Id FROM #idSubCategoria_tbl)' ELSE '' END),
						(SELECT CASE WHEN @TotalRanking   IS NOT NULL THEN ' AND p.[TotalRanking] IN (SELECT Id COLLATE Modern_Spanish_CI_AS FROM #rankings_tbl)' ELSE '' END),
						(SELECT CASE WHEN @SKU			  IS NOT NULL THEN ' AND p.[SKU] IN (SELECT Id FROM #sku_tbl)' ELSE '' END),
						(SELECT CASE WHEN @positioningId  IS NOT NULL THEN ' AND p.[PositioningId] IN (SELECT Id FROM #positioning_tbl)' ELSE '' END),
						(SELECT CASE WHEN @manufacturerId IS NOT NULL THEN ' AND p.[ManufacturerId] IN (SELECT Id FROM #manufacturer_tbl)' ELSE '' END),
						@filtroFlags,
					' ORDER BY SKU')
	PRINT @QuerySKU
	EXEC(@QuerySKU)
	
	CREATE TABLE #SKUS (SKU int NOT NULL PRIMARY KEY)
	INSERT INTO #SKUS
	SELECT *
	FROM (
		SELECT SKU 
		FROM #TempSKU
		UNION
		SELECT g.AuxSKU
		FROM products.GenericFamily g
		INNER JOIN products.GenericFamilyDetail gd on g.Id = gd.GenericFamilyId 
		WHERE gd.SKU IN (SELECT a.Id FROM #sku_tbl a)
	) skus


	SELECT	df.Fecha,
			SUM(df.Fit) Fit,
			SUM(df.FitCorregido) FitCorregido,
			SUM(CASE 
				WHEN Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN Venta
				ELSE NULL END) Venta,
			SUM(CASE 
				WHEN Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN Ingreso
				ELSE NULL END) Ingreso,
			AVG(CASE 
				WHEN Venta IS NOT NULL and df.Fecha <= @fechaVenta THEN Cobertura
				ELSE NULL END) Cobertura,
			AVG(df.Promocion) Promocion,
			CASE 
				WHEN SUM(df.Venta) IS NULL THEN NULL
				WHEN SUM(df.Venta) = 0 THEN NULL
				ELSE SUM(df.PrecioLista*df.Venta)/SUM(df.Venta) 
			END PrecioLista,
			CASE 
				WHEN SUM(df.Venta) IS NULL THEN NULL
				WHEN SUM(df.Venta) = 0 THEN NULL
				ELSE SUM(df.Ingreso)/SUM(df.Venta)
			END PrecioEfectivo,
			CAST( case when  MAX(ISNULL (dp.Promotion, 0))> 0 then 1 else 0 end   as float)  PromocionDetectada,
			CAST(case when MAX(ISNULL (df.PromocionesActivas, 0) )>0 then 1 else 0 end  as float)  PromocionInformada
	INTO #DailyForecast
	FROM forecast.DailyForecast df
	LEFT JOIN forecast.DetectedPromotions dp ON df.Sku = dp.SKU AND df.Fecha = dp.Date
	WHERE df.sku IN (SELECT SKU FROM #SKUS)
		AND df.Fecha BETWEEN @inicio_local AND @termino_local
	GROUP BY df.Fecha

	SELECT	s.[Date],
			CAST(SUM(s.Sugerido)as float) Sugerido,
			CAST(SUM(c.Maximo)as float) StockAjustadoSugerido,
			CAST(SUM(s.LocalesQuebrados)as float)/ sum(s.Locales) Quiebre
	INTO #MetricasStocks
	FROM reports.MetricasStocks s
	CROSS APPLY [function].Maximo(StockAjustadoSugerido,0) c
	WHERE s.sku IN (SELECT SKU FROM #SKUS)
		AND s.[Date] BETWEEN @inicio_local AND @termino_local
	GROUP BY s.[Date]

	SELECT	df.Fecha,
			df.Fit,
			df.FitCorregido,
			df.Venta,
			df.Ingreso,
			--df.Precio,
			df.PrecioLista,
			df.PrecioEfectivo,
			ms.Quiebre,
			df.Cobertura,
			df.Promocion as Promoción,
			ms.Sugerido as SugeridoPromedio,
			ms.StockAjustadoSugerido as StockAjustadoPromedio,
			df.PromocionDetectada,
			df.PromocionInformada
	FROM #DailyForecast df
	LEFT JOIN #MetricasStocks ms ON df.Fecha = ms.[Date] 
	ORDER BY df.Fecha

	DROP TABLE #MetricasStocks
	DROP TABLE #DailyForecast
	DROP TABLE #SKUS
	DROP TABLE #TempSKU
	DROP TABLE #idDivision_tbl
	DROP TABLE #idCategoria_tbl
	DROP TABLE #idSubCategoria_tbl
	DROP TABLE #rankings_tbl
	DROP TABLE #sku_tbl
	DROP TABLE #positioning_tbl
	DROP TABLE #manufacturer_tbl
END
