﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-08-11
-- Description:	
-- =============================================
CREATE PROCEDURE [forecast].[UpdateWeeklyData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @msg VARCHAR(100) 

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Inicia actualización de Pronósticos...' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	truncate table [forecast].[LastForecast]

	--insert into [forecast].[LastForecast]
	--SELECT cast( Datekey as date) FechaCalculo, cast( [Date] as date) FechaPronosticada, ProductId SKU, Fit, CorrectedFit
	--FROM forecast.PredictedSales
	--WHERE DATEKEY IN (SELECT distinct top 3  Datekey from forecast.PredictedSales order by datekey desc)
	--order by Datekey, ProductId, Date
    
	--cambio predicted sales
	insert into [forecast].[LastForecast]
	SELECT cast( Datekey as date) FechaCalculo, cast( dbo.getmonday([Date]) as date) FechaPronosticada, ProductId SKU, sum(Fit) Fit, sum(CorrectedFit) CorrectedFit
	FROM forecast.PredictedDailySales
	WHERE DATEKEY IN (SELECT distinct top 3  Datekey from forecast.PredictedDailySales order by datekey desc)
	group by cast( Datekey as date), cast( dbo.getmonday([Date]) as date) , ProductId 	
	order by cast( Datekey as date), ProductId, cast( dbo.getmonday([Date]) as date)

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Pronósticos actualizados' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Inicia actualización de Ventas...' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	

	insert into [forecast].WeeklySales
	select cast(dbo.getmonday(Date) as Date) Fecha, SKU, SUM(Quantity) Unidades, sum(Income) Ventas
	from series.Sales
	WHERE date >= (select dateadd(week, 1 , max(date)) from [forecast].WeeklySales)
		AND Date <= (select dateadd(day, -1, cast(dbo.Getmonday(max(Date)) as date)) from series.Sales)
		AND SKU in (select SKU from operation.ServicesProductsDetail where ServiceId=2)
	group by dbo.getmonday(Date), SKU

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ventas actualizadas' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Inicia actualización de Stock...' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	insert into [forecast].StockHistory
	SELECT cast(dbo.GetMonday([date]) as date) Fecha, SKU
		, AVG(Stock) StockPromedio, MIN(Stock) StockMinimo, Max(Stock) StockMaximo
		, AVG(Locales) LocalesPromedio, AVG(LocalesQuebrados) LocalesQuebradosPromedio
	FROM [Salcobrand].[reports].[MetricasStocks]
	WHERE SKU in (select SKU from operation.ServicesProductsDetail where ServiceId=2)
		AND date > (select dateadd(week, 1 ,max(date) ) from [forecast].StockHistory)
		AND Date <= (select dateadd(day, -1, cast(dbo.Getmonday(max(Date)) as date)) from [reports].[MetricasStocks])
	GROUP BY dbo.GetMonday([date]), SKU
	Order by Fecha, SKU

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Stock actualizado' 
	RAISERROR (@msg, 10, 0 ) WITH NOWAIT 


END
