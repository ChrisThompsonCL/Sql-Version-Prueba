﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-12-14
-- Description:	[ESTANDARIZACIÓN] Obtiene datos para el servicio de datos, necesarios para el servicio de Modelos
-- =============================================
CREATE PROCEDURE [forecast].[GetVariableData] 
	-- Add the parameters for the stored procedure here
	@producto int,
	@inicio date,
	@fin date

AS
BEGIN

	declare @skusAux table(SKU int)

	insert into @skusAux
	select SKU 
	from products.Products
	where SKU=@producto 
		and SKU not in (select SKU from products.GenericFamilyDetail)
	union
	select SKU 
	from products.GenericFamilyDetail 
	where GenericFamilyId=(select GenericFamilyId from products.GenericFamilyDetail where SKU=@producto)

	--variables de fecha auxiliares
	--declare @finReal date = (select max([Date]) from Salcobrand.series.DailySales)
	declare @finReal date = (select top 1 [date] from series.lastsale)
	declare @inicioReal date = (select min(case when FirstSale > @inicio then FirstSale else @inicio end) from products.FullProducts where sku in (select SKU from @skusAux))

	declare @promedio float, @desv float
	
	select @promedio = ISNULL(AVG(cast(Locales as float)), 0)
		, @desv = ISNULL(STDEV(cast(Locales as float)), 0)
	from [Salcobrand].[reports].[MetricasStocks] 
	where Locales>1 AND sku = @producto and [date] between @inicio and (case when @finReal>@fin then @fin else @finReal end)

	declare @ClaseUnificadaId INT = (SELECT ClaseUnificadaId FROM products.Products WHERE SKU = @producto);


	

	SET NOCOUNT ON;

    select 
        varsEstacionales.*
		,case when ds.Quantity is not null then Quantity else 0 end Venta
		,case when ds.Income is not null then Income else 0 end Ingreso
		,case when stock.Quiebre is not null and varsEstacionales.Fecha <= @finReal then stock.Quiebre when varsEstacionales.Fecha > @finReal then stockFuturo.Quiebre when stock.Cobertura > 0 then 1 else 0 end Quiebre
		--,case when QuiebreShare.Quiebre is not null and varsEstacionales.Fecha <= @finReal then QuiebreShare.Quiebre when varsEstacionales.Fecha > @finReal then stockFuturo.Quiebre when stock.Cobertura > 0 then 0 else 1 end Quiebre
		,case when stock.Cobertura is not null and varsEstacionales.Fecha <= @finReal then stock.Cobertura 
			when varsEstacionales.Fecha > @finReal then isnull(stockFuturo.Cobertura, 0) 
			else @promedio end Cobertura -- Si no poseo información de cobertura prefiero asumir valor promedio de cobertura en vez de 0 (se penaliza mucho si no)		
		
		,ISNULL(prom.[Promotion],0) Promocion
		-- Para considerar promociones futuras, hay que eliminar la parte 'and varsEstacionales.Fecha <= @finReal' de la línea de arriba y de la creacion de prom
		,case when (
				isnull(prom.[Promotion],0) > 0  --en promoción 
				OR (
					(case 
						when stock.Cobertura is not null and varsEstacionales.Fecha <= @finReal then stock.Cobertura 
						when varsEstacionales.Fecha > @finReal then stockFuturo.Cobertura 
						else @promedio 
					end)  <= 0   -- fuera de rango de cobertura
				OR (case 
						when stock.Quiebre is not null and varsEstacionales.Fecha <= @finReal then stock.Quiebre 
						when varsEstacionales.Fecha > @finReal then stockFuturo.Quiebre 
						when stock.Cobertura > 0 then 1 
						else 0 
					end) > 0.3
				OR ISNULL(varsEstacionales.FeriadoIrrenunciable, 0) = 1 -- feriados irrenunciables
				OR ISNULL(varsEstacionales.Navidad, 0) = 1 OR ISNULL(varsEstacionales.FiestasPatrias, 0) = 1 OR ISNULL(varsEstacionales.DiaMadre, 0) = 1 OR ISNULL(varsEstacionales.DiaNino, 0) = 1--eventos
				OR ISNULL(varsEstacionales.DiaPadre, 0) = 1 OR ISNULL(varsEstacionales.SanValentin, 0) = 1 OR ISNULL(varsEstacionales.InicioClases, 0) = 1 OR ISNULL(varsEstacionales.AnoNuevo, 0) = 1 )
			)
			then 1
			else 0
		end Filtro
		--Recetas
		--, ISNULL(Recetas.Recetas, 0) Recetas
	from

		(SELECT [Fecha]
      ,[Domingo]
      ,[Lunes]
      ,[Martes]
      ,[Miercoles]
      ,[Jueves]
      ,[Viernes]
      ,[Sabado]
      ,[Enero]
      ,[Febrero]
      ,[Marzo]
      ,[Abril]
      ,[Mayo]
      ,[Junio]
      ,[Julio]
      ,[Agosto]
      ,[Septiembre]
      ,[Octubre]
      ,[Noviembre]
      ,[Diciembre]
	  ,[FeriadoIrrenunciable] 
	  ,[AntesFeriadosI]		
	  ,[DespuesFeriadosI]		
	  ,[Feriado]				
	  ,[AntesFeriados]		
	  ,[DespuesFeriados]		
      ,[Navidad]
      ,[FiestasPatrias]
      ,[DiaMadre]
      ,[DiaNino]
      ,[DiaPadre]
      ,[SanValentin]
      ,[InicioClases]
      ,[Halloween]
      ,[SemanaSanta]
      ,[AnoNuevo]
      ,[FinMes]
  FROM [Salcobrand].[forecast].[GenericVariables] where fecha between @inicioReal and @fin) varsEstacionales
		------------------------
	-- información de venta
	left outer join ( 
		select @producto SKU, [date], SUM(Quantity) Quantity, SUM(Income)Income
		from [Salcobrand].[series].[DailySales] 
		where SKU in (select SKU from @skusAux) 
			and [Date] between @inicioReal and @finReal
		group by [date]
	) ds on ds.[Date] = varsEstacionales.Fecha
	-- info stock y stock futuro
	left outer join (
		select [Date]
			, Min(ROUND(cast(LocalesQuebrados*1.0/Locales as float),2)) Quiebre
			, Max(Locales) Cobertura
			, Max(ROUND(cast([StockAjustadoSugerido]*1.0/[Sugerido] as float),2)) [PStock] 
		from [Salcobrand].[reports].[MetricasStocks] 
		where sku  in (select SKU from @skusAux) 
		group by [Date]
	) stock on stock.[Date] = varsEstacionales.Fecha
	--left outer join (select SKU,DATE,Quiebre from [Salcobrand].[forecast].[QuiebreShare] where sku = @producto) QuiebreShare on QuiebreShare.[DATE] = varsEstacionales.Fecha
	cross join (
		select 0 Quiebre, AVG(Locales) Cobertura 
		from [Salcobrand].[reports].[MetricasStocks] 
		where sku in (select SKU from @skusAux) 
			and [Date] between dateadd(day,-30, @finReal) and @finReal
	) stockFuturo
	-- info promociones
	LEFT OUTER JOIN (SELECT  [Date], [Promotion] FROM forecast.[DetectedPromotions] WHERE [Sku] = @producto  AND [Date] >= @inicioReal) prom ON varsEstacionales.Fecha = prom.[Date]
	--informacion recetas
	LEFT OUTER JOIN (SELECT a.Semana, SUM(ISNULL([RecetasDosSemanasAtras],0)) Recetas
						FROM Salcobrand.test.Recetas_AUX_Semanal a
						--WHERE SKU in (select sku from products.Products p where p.ClaseUnificadaId =  @ClaseUnificadaId)
						WHERE sku = @producto
						GROUP BY Semana
	) Recetas ON Recetas.Semana = dbo.GetMonday(varsEstacionales.Fecha)
	
	ORDER BY Fecha


END