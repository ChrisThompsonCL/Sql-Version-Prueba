﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-09-30
-- Description:	Corrige en caso que la detección de outliers esté dejando fuera tendencia de últimas semanas
-- =============================================
CREATE PROCEDURE [forecast].[CorrectForecast]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete o
	from forecast.Outliers o
	where DateKey = (select max(Datekey) from forecast.Outliers)
		and OutlierDate >= DATEADD(week, -3, EnddateAnalysis)
		and ProductId in (
			select ProductId
			from forecast.Outliers
			where DateKey = (select max(Datekey) from forecast.Outliers)
				and OutlierDate >= DATEADD(week, -3, EnddateAnalysis)
			group by ProductId
			having count(*) = 3
		)



	delete o
	from forecast.Outliers o
	where DateKey = (select max(Datekey) from forecast.Outliers)
		and OutlierDate >= DATEADD(week, -6, EnddateAnalysis)
		and ProductId in (
			select ProductId
			from forecast.Outliers
			where DateKey = (select max(Datekey) from forecast.Outliers)
				and OutlierDate >= DATEADD(week, -6, EnddateAnalysis)
			group by ProductId
			having count(*) >= 4
		)


END
