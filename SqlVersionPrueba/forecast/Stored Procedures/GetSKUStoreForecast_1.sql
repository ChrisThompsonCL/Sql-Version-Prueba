﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-04-23
-- Description:	Obtiene los pronósticos por SKU/Local usados en SIIRA
-- =============================================
CREATE PROCEDURE [forecast].[GetSKUStoreForecast]
	@sku INT,
	@store INT,
	@beginDate DATE,
	@endDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE  @idfamilia  INT = (SELECT Id FROM products.GenericFamily WHERE AuxSKU = @sku) 
	DECLARE @inicioFuturo DATE = (SELECT [function].InlineMinDate(MIN([DATE]), CAST(GETDATE() AS date)) FROM minmax.FitsAllNew where sku = @sku)

	SELECT  dt.Fecha
		, CASE WHEN s.Quantity IS NOT NULL THEN s.Quantity 
		  WHEN dt.Fecha  < @inicioFuturo THEN 0
		  WHEN dt.Fecha  > @inicioFuturo THEN NULL
		  END Venta
		, fan.Fit  
		, isnull (fan.SKU, @sku) SKU    
		, isnull( fan.Store,@store) Store
		, df.Promocion 
	FROM (
	  SELECT Fecha 
	  FROM forecast.GenericVariables
	  WHERE Fecha  BETWEEN @beginDate AND @endDate
	) dt 
	LEFT JOIN (SELECT * FROM series.Sales WHERE sku = @sku AND store = @store) s ON s.[Date] = dt.Fecha 
	LEFT JOIN (SELECT * FROM minmax.FitsAllCurrent WHERE sku = @sku AND store = @store) fan ON fan.[Date] = dt.Fecha
	left join (
	SELECT * 
	  FROM forecast.DailyForecast 
	  WHERE sku IN ( 
		SELECT SKU 
		FROM products.GenericFamilyDetail 
		WHERE GenericFamilyId = @idfamilia
		union 
		SELECT @sku
	  )
	)df on df.Fecha = dt.Fecha
	ORDER BY dt.Fecha

END
