﻿-- ==========================================================================================
-- Author:		Carlos Quappe
-- Create date: 2018-03-14
-- Description: Correr para todos los SKU's el procedimiento [DetectPromotions] 
-- ==========================================================================================
create PROCEDURE forecast.[DetectPromotions_Multiple]
	-- Add the parameters for the stored procedure here
	@beginning_date datetime,
	@finishing_date datetime,
	@logger bit
AS
BEGIN
	SET NOCOUNT ON;

	
	if @logger = 1
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
		VALUES (getdate(), 'Info', 'Salcobrand.forecast.DeteccionPromociones', 'Comienza Deteccion Promociones', host_name())

	declare @auxSku int

	declare cursor_Sku cursor fast_forward 
	for
		select distinct IdCodigoCliente from [view].[RelacionIDCombinacion]
	open cursor_Sku
	fetch next from cursor_Sku into @auxSku

	while @@fetch_status = 0
	begin
		if @logger = 1
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
			VALUES (getdate(), 'Info', 'Salcobrand.forecast.DeteccionPromociones', 'SKU: '+cast(@auxSku as nvarchar), host_name())
		exec forecast.[DetectPromotions]  @auxSku, @beginning_date, @finishing_date
		fetch next from cursor_Sku into @auxSku

	end

	close cursor_Sku
	deallocate cursor_Sku	

	if @logger = 1
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
		VALUES (getdate(), 'Info', 'Salcobrand.forecast.DeteccionPromociones', 'Termina Deteccion Promociones', host_name())

END
