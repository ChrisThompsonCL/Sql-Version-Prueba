﻿CREATE TABLE [forecast].[WeeklyRevision] (
    [Datekey]                    DATE NULL,
    [SKU]                        INT  NULL,
    [Revision por venta semanal] INT  NULL,
    [RevisionTodo]               INT  NULL,
    [Generico]                   INT  NULL,
    [Cortes]                     INT  NULL,
    [Semana Pasada]              INT  NULL,
    [GenericoConServicio]        INT  NULL,
    [Diferencia Cobertura]       INT  NULL
);

