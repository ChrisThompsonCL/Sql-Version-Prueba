﻿CREATE TABLE [forecast].[DetectedPromotions] (
    [Sku]       INT  NOT NULL,
    [Date]      DATE NOT NULL,
    [Promotion] INT  NULL,
    CONSTRAINT [PK_DetectedPromotions] PRIMARY KEY CLUSTERED ([Sku] ASC, [Date] ASC)
);

