﻿CREATE TABLE [forecast].[OFPredictedVariables] (
    [ProductId]   INT           NOT NULL,
    [ProductType] INT           NOT NULL,
    [StoreId]     INT           NOT NULL,
    [StoreType]   INT           NOT NULL,
    [Aggregation] INT           NOT NULL,
    [Date]        SMALLDATETIME NOT NULL,
    [VariableId]  INT           NOT NULL,
    [Coefficient] FLOAT (53)    NULL,
    [Value]       FLOAT (53)    NULL,
    CONSTRAINT [PK_OFPredictedVariables] PRIMARY KEY CLUSTERED ([ProductId] ASC, [ProductType] ASC, [StoreId] ASC, [StoreType] ASC, [Aggregation] ASC, [Date] ASC, [VariableId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_OFPredictedVariables]
    ON [forecast].[OFPredictedVariables]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OFPredictedVariables_1]
    ON [forecast].[OFPredictedVariables]([StoreId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OFPredictedVariables_2]
    ON [forecast].[OFPredictedVariables]([Date] ASC);

