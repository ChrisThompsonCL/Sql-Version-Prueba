﻿CREATE TABLE [forecast].[PredictedDailySales] (
    [DateKey]      SMALLDATETIME NOT NULL,
    [ProductId]    INT           NOT NULL,
    [Date]         SMALLDATETIME NOT NULL,
    [Fit]          FLOAT (53)    NULL,
    [MaxValue]     FLOAT (53)    NULL,
    [MaxMean]      FLOAT (53)    NULL,
    [Corrected]    BIT           NULL,
    [CorrectedFit] FLOAT (53)    NULL,
    CONSTRAINT [PK_PredictedDailySales] PRIMARY KEY CLUSTERED ([DateKey] ASC, [ProductId] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PredictedDailySales_SKU]
    ON [forecast].[PredictedDailySales]([ProductId] ASC);

