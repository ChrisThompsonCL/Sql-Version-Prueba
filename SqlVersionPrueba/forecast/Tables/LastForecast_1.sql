﻿CREATE TABLE [forecast].[LastForecast] (
    [FechaCalculo]      DATE       NULL,
    [FechaPronosticada] DATE       NULL,
    [SKU]               INT        NOT NULL,
    [Fit]               FLOAT (53) NULL,
    [CorrectedFit]      FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_LastForecast_SKU]
    ON [forecast].[LastForecast]([SKU] ASC)
    INCLUDE([FechaCalculo], [FechaPronosticada], [Fit]);

