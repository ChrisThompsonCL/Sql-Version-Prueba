﻿CREATE TABLE [forecast].[GenericVariables] (
    [Fecha]                DATE NULL,
    [Domingo]              INT  NOT NULL,
    [Lunes]                INT  NOT NULL,
    [Martes]               INT  NOT NULL,
    [Miercoles]            INT  NOT NULL,
    [Jueves]               INT  NOT NULL,
    [Viernes]              INT  NOT NULL,
    [Sabado]               INT  NOT NULL,
    [Enero]                INT  NOT NULL,
    [Febrero]              INT  NOT NULL,
    [Marzo]                INT  NOT NULL,
    [Abril]                INT  NOT NULL,
    [Mayo]                 INT  NOT NULL,
    [Junio]                INT  NOT NULL,
    [Julio]                INT  NOT NULL,
    [Agosto]               INT  NOT NULL,
    [Septiembre]           INT  NOT NULL,
    [Octubre]              INT  NOT NULL,
    [Noviembre]            INT  NOT NULL,
    [Diciembre]            INT  NOT NULL,
    [FeriadoIrrenunciable] INT  NOT NULL,
    [AntesFeriadosI]       INT  NOT NULL,
    [DespuesFeriadosI]     INT  NOT NULL,
    [Feriado]              INT  NOT NULL,
    [AntesFeriados]        INT  NOT NULL,
    [DespuesFeriados]      INT  NOT NULL,
    [Navidad]              INT  NOT NULL,
    [FiestasPatrias]       INT  NOT NULL,
    [DiaMadre]             INT  NOT NULL,
    [DiaNino]              INT  NOT NULL,
    [DiaPadre]             INT  NOT NULL,
    [SanValentin]          INT  NOT NULL,
    [InicioClases]         INT  NOT NULL,
    [Halloween]            INT  NOT NULL,
    [SemanaSanta]          INT  NOT NULL,
    [AnoNuevo]             INT  NOT NULL,
    [FinMes]               INT  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_GenericVariables_Fecha]
    ON [forecast].[GenericVariables]([Fecha] ASC);

