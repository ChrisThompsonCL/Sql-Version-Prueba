﻿CREATE TABLE [forecast].[SpecialRevision] (
    [SKU]                 INT            NOT NULL,
    [Descripción]         NVARCHAR (255) NULL,
    [Division]            NVARCHAR (255) NULL,
    [Laboratorio]         NVARCHAR (255) NULL,
    [Motivo]              NVARCHAR (255) NULL,
    [Ranking]             NVARCHAR (255) NULL,
    [EsGenerico]          FLOAT (53)     NULL,
    [EstadoServicio]      NVARCHAR (255) NULL,
    [PromLoc]             FLOAT (53)     NULL,
    [VentasSemProm]       FLOAT (53)     NULL,
    [VentasSemxLocalProm] FLOAT (53)     NULL,
    [CálculoPronóstico]   DATETIME       NULL,
    [UsoPronóstico]       DATETIME       NULL,
    [Corregido?]          FLOAT (53)     NULL,
    [Pronóstico]          FLOAT (53)     NULL,
    [PronósticoAnterior]  NVARCHAR (255) NULL,
    [Corrección]          NVARCHAR (255) NULL,
    [PromLocalesNormales] FLOAT (53)     NULL,
    [LocalesHoy]          FLOAT (53)     NULL,
    [DiferenciaCobertura] FLOAT (53)     NULL,
    CONSTRAINT [PK_SpecialRevision] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

