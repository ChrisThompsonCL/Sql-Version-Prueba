﻿CREATE TABLE [forecast].[DailyForecast] (
    [SKU]                  INT            NOT NULL,
    [Fecha]                DATE           NOT NULL,
    [Fit]                  FLOAT (53)     NULL,
    [FitCorregido]         FLOAT (53)     NULL,
    [FeriadoIrrenunciable] FLOAT (53)     NULL,
    [Feriado]              FLOAT (53)     NULL,
    [Navidad]              FLOAT (53)     NULL,
    [FiestasPatrias]       FLOAT (53)     NULL,
    [DiaMadre]             FLOAT (53)     NULL,
    [DiaNino]              FLOAT (53)     NULL,
    [DiaPadre]             FLOAT (53)     NULL,
    [SanValentin]          FLOAT (53)     NULL,
    [InicioClases]         FLOAT (53)     NULL,
    [Halloween]            FLOAT (53)     NULL,
    [SemanaSanta]          FLOAT (53)     NULL,
    [AnoNuevo]             FLOAT (53)     NULL,
    [Venta]                FLOAT (53)     NULL,
    [Ingreso]              FLOAT (53)     NULL,
    [Quiebre]              FLOAT (53)     NULL,
    [Cobertura]            FLOAT (53)     NULL,
    [Promocion]            FLOAT (53)     NULL,
    [ModifiedBy]           NVARCHAR (MAX) NULL,
    [ModifiedOn]           DATE           NULL,
    [PromocionesActivas]   INT            NULL,
    [PrecioLista]          FLOAT (53)     NULL,
    CONSTRAINT [PK_DailyForecast] PRIMARY KEY CLUSTERED ([SKU] ASC, [Fecha] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DailyForecast_Fecha]
    ON [forecast].[DailyForecast]([Fecha] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DailyForecast_Fecha_Includes_SKU_FitCorregido]
    ON [forecast].[DailyForecast]([Fecha] ASC)
    INCLUDE([SKU], [FitCorregido]);

