﻿CREATE TABLE [forecast].[WeeklySales] (
    [Date]     DATE       NOT NULL,
    [SKU]      INT        NOT NULL,
    [Quantity] INT        NULL,
    [Income]   FLOAT (53) NULL,
    CONSTRAINT [PK_WeeklySales] PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Forecast_WeeklySales]
    ON [forecast].[WeeklySales]([SKU] ASC)
    INCLUDE([Date], [Quantity]);

