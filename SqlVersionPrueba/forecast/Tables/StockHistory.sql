﻿CREATE TABLE [forecast].[StockHistory] (
    [Date]                     DATE NULL,
    [SKU]                      INT  NULL,
    [StockPromedio]            INT  NULL,
    [StockMinimo]              INT  NULL,
    [StockMaximo]              INT  NULL,
    [LocalesPromedio]          INT  NULL,
    [LocalesQuebradosPromedio] INT  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_StockHistory_Date]
    ON [forecast].[StockHistory]([Date] ASC)
    INCLUDE([SKU], [LocalesPromedio], [LocalesQuebradosPromedio]);

