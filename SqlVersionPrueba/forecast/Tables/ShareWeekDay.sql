﻿CREATE TABLE [forecast].[ShareWeekDay] (
    [ProductId]   INT           NOT NULL,
    [ProductType] INT           NULL,
    [StoreId]     INT           NULL,
    [StoreType]   INT           NULL,
    [Week]        SMALLDATETIME NULL,
    [Mon]         FLOAT (53)    NULL,
    [Tue]         FLOAT (53)    NULL,
    [Wed]         FLOAT (53)    NULL,
    [Thu]         FLOAT (53)    NULL,
    [Fri]         FLOAT (53)    NULL,
    [Sat]         FLOAT (53)    NULL,
    [Sun]         FLOAT (53)    NULL
);

