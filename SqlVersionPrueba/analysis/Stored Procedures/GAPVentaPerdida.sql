﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-09-26
-- Description:	Reporte GAP de venta perdida
-- =============================================
CREATE PROCEDURE [analysis].[GAPVentaPerdida] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;
	declare @fecha date= (select dateadd(day, -1, max(fecha)) from reports.DailyReportStores)

	select @fecha as FechaAnalisis

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	/***TABLA 1: COMBINACIONES CON VENTA PERDIDA EN DETALLE***/ 
	select dsm.SKU, dsm.Store, p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, dsm.[Date], drs.Stock, drs.Suggest, drs.Transit
		, drs.StockCD, drs.StockTotal, drs.SugeridoTotal, drs.Faltante, drs.VentaPerdidaVal, drs.VentaPerdida
		, drs.UltimaVenta, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
		, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
		, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
		, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
		, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
	from  aux.DailySuggestMix dsm
	inner join products.Products p on p.SKU=dsm.SKU
	inner join (
		select  *
		from reports.DailyReportStores
		where Fecha=@fecha
	) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  and drs.SKU not in (select sku from products.products where TotalRanking = 'Z')
	left join (
		select *
		--from [view].SuggestRestrictionToday
		from #SuggestRestrictionToday
		where Fijo is not null 
	) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store


	--/***TABLA 2: COMBINACIONES CON VENTA PERDIDA DATOS AGREGADOS***/
	----declare @fecha date= (select dateadd(day, -1, max(fecha)) from reports.DailyReportStores)
	SELECT u.Filtro, u.Valor
	FROM (
		select SUM(v.Income) VentaTotal
			, sum(case when drs.Stock is not null then drs.VentaPerdidaVal else 0 end) VentaPerdida
			, sum(case when drs.stock < 0 then drs.VentaPerdidaVal else 0 end) InfoStockRara
			, sum(case when drs.Stock=0 and Promocion=1 then drs.VentaPerdidaVal else 0 end) EnPromocion
			, sum(case when drs.Stock=0 and Promocion=0 and srt.fijo is not null and srt.fijo=drs.Suggest then drs.VentaPerdidaVal else 0 end) RestrCargaManual
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt<>0 then drs.VentaPerdidaVal else 0 end) FallaProveedorAnt
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual<>0 then drs.VentaPerdidaVal else 0 end) FallaActual
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and Faltante>StockCD and Nquiebre>StockCD then drs.VentaPerdidaVal else 0 end) StockCDBajo /*FALTANTE DEBE SER MENOR A LOCALES QEBRADOS*/
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then drs.VentaPerdidaVal else 0 end) ProblemaReposicion
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then drs.VentaPerdidaVal else 0 end) VentaPerdidaModelo
		from  aux.DailySuggestMix dsm
		left join (
			select SKU, Store, cast(Quantity as float) Quantity, cast(Income as float) Income
			from series.LastSale
		) v on v.SKU=dsm.SKU and v.Store=dsm.Store
		left join (
			select *
			from reports.DailyReportStores
			where Fecha=@fecha
		) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  
		left join (
			select *
			--from [view].SuggestRestrictionToday
			from #SuggestRestrictionToday
			where Fijo is not null OR FijoEstricto is not null
		) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
	) a
	unpivot (Valor for Filtro in 
	  (VentaTotal, VentaPerdida, InfoStockRara, EnPromocion, RestrCargaManual
	  , FallaProveedorAnt, FallaActual, StockCDBajo, ProblemaReposicion
	  , VentaPerdidaModelo
	))  u

/***TABLA 3: COMBINACIONES CON VENTA PERDIDA DATOS AGREGADOS POR DIVISION, CATEGORIA Y SUBCATEGORIA UNIFICADA***/
--declare @fecha date= (select dateadd(day, -1, max(fecha)) from reports.DailyReportStores)
select pro.DivisionUnificadaDesc, pro.CategoriaUnificadaDesc, pro.SubcategoriaUnificadaDesc ,SUM(v.Income) [Venta Real($)]
			, sum(case when drs.Stock is not null then drs.VentaPerdidaVal else 0 end) [Venta Perdida($)]
			, sum(case when drs.stock < 0 then drs.VentaPerdidaVal else 0 end) [Foto stock menor a cero]
			, sum(case when drs.Stock=0 and Promocion=1 then drs.VentaPerdidaVal else 0 end) [En Promocion]
			, sum(case when drs.Stock=0 and Promocion=0 and srt.fijo is not null and srt.fijo=drs.Suggest then drs.VentaPerdidaVal else 0 end) [Restr Carga Manual]
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and (FallaProveedorAnt<>0 or FallaActual<>0)  then drs.VentaPerdidaVal else 0 end) [Falla Proveedor Ant]
			--, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual<>0 then drs.VentaPerdidaVal else 0 end) [Falla Actual]
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and Faltante>StockCD and Nquiebre>StockCD then drs.VentaPerdidaVal else 0 end) [StockCDBajo] /*FALTANTE DEBE SER MENOR A LOCALES QEBRADOS*/
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then drs.VentaPerdidaVal else 0 end) [Problema Reposicion]
			, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then drs.VentaPerdidaVal else 0 end) [Venta Perdida Modelo]
		from  aux.DailySuggestMix dsm
		left join (
			select SKU, Store, cast(Quantity as float) Quantity, cast(Income as float) Income
			from series.LastSale
		) v on v.SKU=dsm.SKU and v.Store=dsm.Store
		left join (
			select *
			from reports.DailyReportStores
			where Fecha=@fecha
		) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  and drs.SKU not in (select sku from products.products where TotalRanking = 'Z')
		left join (
			select *
			--from [view].SuggestRestrictionToday
			from #SuggestRestrictionToday
			where Fijo is not null OR FijoEstricto is not null
		) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
		inner join Salcobrand.products.products pro on pro.sku = dsm.SKU
		group by pro.DivisionUnificadaDesc, pro.CategoriaUnificadaDesc, pro.SubcategoriaUnificadaDesc
			  


	--/***TABLA 5: RESUMEN DEL MIX***/
	-- total
	select 'Total' Categoría, count(*) Combinaciones, sum(case when Stock <= 0 then 1 else 0 end) CombinacionesQuebradas, sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float) [%Quiebre]
	from series.LastStock a
	UNION ALL
	select 'Bloqueadas', count(*), sum(case when Stock <= 0 then 1 else 0 end), sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float)
	from series.LastStock A 
	where IsBloqued = 'S'
	UNION ALL
	select 'Modelo', count(*), sum(case when Stock <= 0 then 1 else 0 end), sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float)
	from aux.DailySuggestMix dsm
	inner join series.LastStock ls on ls.SKU=dsm.SKU and ls.Store=dsm.Store



	/***TABLA 6: RESUMEN VP TOTAL VS VP MODELO***/
	--declare @fecha date= (select dateadd(day, -1, max(fecha)) from reports.DailyReportStores)
	select drs.Fecha Día
		, sum(case when drs.Stock is not null then drs.VentaPerdidaVal else 0 end) VentaPerdida
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then drs.VentaPerdidaVal else 0 end) VentaPerdidaModelo
	from  aux.DailySuggestMix dsm
	inner join (
		select *
		from reports.DailyReportStores
		where Fecha=@fecha
	) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  
	left join (
		select *
		--from [view].SuggestRestrictionToday
		from #SuggestRestrictionToday
		where Fijo is not null or FijoEstricto is not null
	) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
	group by drs.Fecha

	--declare @fecha date= (select dateadd(day, -1, max(fecha)) from reports.DailyReportStores)
	declare @ventaPerdidaTotal float = (
		select sum(VentaPerdidaVal) 
		from (
			select dsm.SKU, dsm.Store, p.Division_Desc, dsm.[Date], drs.Stock, drs.Suggest, drs.Transit
				, drs.StockCD, drs.StockTotal, drs.SugeridoTotal, drs.Faltante, drs.VentaPerdidaVal
				, drs.UltimaVenta, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
				, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
				, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
				, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
				, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
			from  aux.DailySuggestMix dsm
			inner join products.Products p on p.SKU=dsm.SKU
			inner join (
				select *
				from reports.DailyReportStores
				where Fecha=@fecha
			) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  and drs.SKU not in (select sku from products.products where TotalRanking = 'Z')
			left join (
				select *
				--from [view].SuggestRestrictionToday
				from #SuggestRestrictionToday
				where Fijo is not null or FijoEstricto is not null
			) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
		) a
		--WHERE FallaActual=0 and FallaProveedorAnt=0 and Promocion=0 and StockCDBajo=0 and RestrCargaManual=0 and InfoStockRara=0 and ProblemaReposicion=0
	)

	/***TABLA 7: COMBINACIONES CON VP MODELO DETALLE***/
	SELECT p.DivisionUnificadaDesc, p.CategoriaUnificadaDesc, p.SubcategoriaUnificadaDesc, p.name Descripción, p.logisticareaId Bodega, a.sku SKU
		, case when p.totalranking is null then '' else p.totalRanking end Ranking
		, case when Division=1 then p.Positioning_Desc when Division=2 then p.Class_Desc else NULL end [Posicionamiento/Clase]
		, a.ConQuiebre [Locales con Quiebre], a.Cobertura [Locales Totales], a.FaltanteProm [Prom. Faltante], a.StockCD [StockCD]
		, PesoSobreVPTotal [Peso Sobre VP Total]  
		, Transit [En Tránsito]
		, (select max(fecha) from [Salcobrand].reports.DailyReportStores) Date
		
	from (
		select a.SKU, count(*) ConQuiebre, Cobertura
			, avg(Faltante) FaltanteProm, max(stockCD) StockCD
			, sum(VentaPerdidaVal)/(@ventaPerdidaTotal) PesoSobreVPTotal
			, SUM(Transit)Transit
		FROM (
			select dsm.SKU, dsm.Store, p.Division_Desc, dsm.[Date], drs.Transit
				, drs.StockCD, drs.StockTotal, drs.SugeridoTotal, drs.Faltante, drs.VentaPerdidaVal
				, drs.UltimaVenta, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
				, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
				, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
				, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
				, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
			from  aux.DailySuggestMix dsm
			inner join products.Products p on p.SKU=dsm.SKU
			inner join (
				select *
				from reports.DailyReportStores
				where Fecha=@fecha
			) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store and drs.SKU not in (select sku from products.products where TotalRanking = 'Z') 
			left join (
				select *
				--from [view].SuggestRestrictionToday
				from #SuggestRestrictionToday
				where Fijo is not null or FijoEstricto is not null
			) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
		) a
		LEFT JOIN (select sku, count(*) Cobertura from series.LastStock where IsBloqued<>'S' group by sku) b on a.sku=b.sku
		WHERE FallaActual=0 and FallaProveedorAnt=0 and Promocion=0 and StockCDBajo=0 and RestrCargaManual=0 and InfoStockRara=0 and ProblemaReposicion=0
		GROUP BY a.SKU,Cobertura
	) a
	LEFT JOIN products.Products p on a.sku=p.sku
	order by PesoSobreVPTotal desc


END
