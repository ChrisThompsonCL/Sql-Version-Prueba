﻿-- =============================================
-- Author:		Mauricio Sepúlveda Duque
-- Create date: 2014-06-10
-- Description:	Crea una foto del estado actual del mix
-- =============================================
CREATE PROCEDURE [analysis].[UpdateScanner]
	@date smalldatetime = null
AS
BEGIN
	set @date = (select convert(date,getdate()))
	truncate table [reports].ResultadoScanner 

	--drop table #table
	insert into [reports].ResultadoScanner
	select sku,store,0,0,suggest,stock,0,null,0,0,0,null,0,Date Date from series.LastStock

	update t set t.unidades = s.unidades,t.venta = s.valorado from [reports].ResultadoScanner t 
	inner join (
	select sku,store,sum(quantity) unidades,sum(income) valorado from series.Sales
	where date >= dateadd(day,-366,@date)
	group by sku,store) s on t.sku = s.sku and t.store = s.Store

	update t set t.modelo = 1 from [reports].ResultadoScanner t 
	inner join [function].[GetOperativeCombination](3,0,3,0,3) s on t.sku = s.ProductId and t.store = s.StoreId

	update t set t.cost = p.Cost from [reports].ResultadoScanner t 
	inner join pricing.LastPriceAndCost p
	on p.sku = t.sku

	declare @tablaCombinaciones table (sku int, store int, suggest int, stock int, suggestModelo int, cost float, descripcion nvarchar(50))

	-- dun por combinación
	insert into @tablaCombinaciones
	select l.sku,l.Store,l.[Max] suggest, l.stock ,m.suggestModelo,p.Cost,'Dun por combinación' descripcion from (
	select l.sku,l.Store,l.[Max],l.[Min],l.Stock 
	from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID=6 AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where l.[Max]-l.[Min]>p.Detail/2 ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- dun por producto
	insert into @tablaCombinaciones
	select l.sku,l.Store,l.[Max] suggest, l.stock ,m.suggestModelo,p.Cost,'dun por producto' descripcion from (
	select l.sku,l.Store,l.[Max],l.[Min],l.Stock 
	from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID=5 AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku
	where l.[Max]-l.[Min]>p.Detail/2 ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- mínimo por combinación
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'mínimo por combinación' descripcion from (
	select l.sku,l.Store,l.Suggest,l.Stock  
	from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID in (4,16) AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- mínimo por producto
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'mínimo por producto' descripcion  from (
	select l.sku,l.Store,l.Suggest,l.Stock 
	from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID in (3,8,12) AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- carga manual
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'Carga Manual' descripcion  from (
	select l.sku,l.Store,l.Suggest,l.Stock from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID=2 AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- planogramaFarma
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'PlanogramaFARMA' descripcion  from (
	select l.sku,l.Store,l.Suggest,l.Stock from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID in (9) AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

		-- PlanogramaDERMO
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'PlanogramaDERMO' descripcion  from (
	select l.sku,l.Store,l.Suggest,l.Stock from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID in (11) AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- planogramaConsumo
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'PlanogramaCONSUMO' descripcion  from (
	select l.sku,l.Store,l.Suggest,l.Stock from (
		select *
		from [view].SuggestRestrictionDetail
		where SuggestRestrictionTypeID=10 AND DATEADD(DAY,-1, @date) between BeginDate and EndDate
	) p
	inner join series.LastStock l on p.ProductID = l.sku and p.StoreID = l.Store
	where p.Detail = l.Suggest ) l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	-- bloqueadas
	insert into @tablaCombinaciones
	select l.*,m.suggestModelo,p.Cost,'Bloqueadas' descripcion  from (
	SELECT sku,store,suggest,stock FROM [Salcobrand].[series].[LastStock] where IsBloqued='S') l
	inner join (select sku,store,max(suggestMin)+1 suggestModelo from minmax.SuggestAllCurrent
	group by sku,store) m
	on l.sku = m.sku and l.Store = m.Store 
	inner join pricing.LastPriceAndCost p
	on p.sku = m.sku

	update t set t.restriccion = p.descripcion,t.sugeridoModelo=p.suggestModelo from [reports].ResultadoScanner t 
	inner join @tablaCombinaciones p
	on p.sku = t.sku and p.store = t.store

	update t set t.division = f.Division , t.ranking = f.totalranking from [reports].ResultadoScanner t 
	inner join products.FullProducts f on t.sku = f.SKU

	update t set t.nivelSugerido = case when sugerido > 6 then 6 else sugerido end from [reports].ResultadoScanner t 

END
