﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-04-03
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[LostSaleReportByStore]
	-- Add the parameters for the stored procedure here
	@date smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select rs.Date, rs.Store
		, SUM(rs.LostSale) VtaPerdida
		, SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaVal
		, SUM(rs.Quantity) VtaReal
		, SUM(rs.Income) VtaRealVal
		, count(*) Combinaciones
	from reports.ReportStores rs
--	left join series.Sales s on s.SKU=rs.SKU and s.Store=rs.Store and rs.Date=s.Date
	where rs.Date =@date
	group by rs.Date, rs.Store
END
