﻿-- =============================================
-- Author:		Constanza Calderon (writen by Daniela Albornoz)
-- Create date: 2014-09-02
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[GetOutsideMixSuggestResult]
	-- Add the parameters for the stored procedure here
	--@executionDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @executionDate date = (select MAX(ExecutionDate) from minmax.OutsideMixSuggestResult)

    -- revisión propuesta Nueva de Sugeridos
SELECT	r.SKU, r.Store, r.ExecutionDate, r.SaleDate, r.TotalRanking, r.Rate, r.Suggest, r.NewSuggest, r.Cost
		,p.Description,p.Division_Desc,p.Class_Desc,p.Positioning_Desc,p.Substitute_Desc, p.ManufacturerDesc
		-- causas de exclusión debidas a producto
		, case when p.SKU is null then 1 else 0 end 'ProductoNOEnMaestro'
		, case when IsActive=1 then 0 else 1 end 'ProductoNOActivoEnMaestro'
		, case when MotiveId=2 then 1 else 0 end 'ProductoPorDescontinuar'
		, case when Class = 196 then 1 else 0 end 'ProductoAutoliquidable'
		, case when Subdivision=15 then 1 else 0 end 'ProductoEsServicio'
		, case when p.LastSale < dateadd(week,-24,GETDATE()) then 1 else 0 end 'ProductoSinVenta24Semanas'
		--, case when gc.Id is null and (p.ManufacturerId in (18,92) OR p.CategoryId=1) then 1 else 0 end 'CategoríaGenéricoNOInformada'
		, case when m.SKU is null then 1 else 0 end 'ProductoSinServicioModelos'
		, case when mop.ManufacturerId is not null then 1 else 0 end 'ProveedorExcluidoServicioSugeridoLocales'
		, case when r.SKU is null then 1 else 0 end 'BodegaProductoSinRutaInformada'
		, case when esku.SKU is not null then 1 else 0 end 'ProductoExcluidoServicioSugeridoLocales'
		, case when p.MotiveId in (6,10) then 1 else 0 end 'ProductoCompraPorSolicitud'
		, case when p.sku in (select sku from products.genericfamilydetail) then 1 else 0 end 'ProductoGenerico'
		, case when p.TotalRanking = 'E' then 1 else 0 end 'ProductoMarcadoEstacional'
		--causas de exclusión debidas al local
		, case when fs.Store is null then 1 else 0 end 'LocalNOEnMaestro'
		, case when fs.LastSale < dateadd(week,-1,@executionDate) then 1 else 0 end 'LocalSinVenta1Semana'
		, case when fs.FirstSale >= DATEADD(MONTH, -2, @executionDate) and r.store not in (select newstore from stores.newstores2) then 1 else 0 end 'LocalNuevoVentaMenorA2Meses'
		, case when estore.Store is not null then 1 else 0 end 'LocalExcluidoServicioSugeridoLocales'
		, case when j.Store is null then 1 else 0 end 'LocalSinRutaInformada'
		--causas de exclusion debidas a la combinación
		, case when LS.Suggest > 0 then 0 else 1 end 'CombinaciónNOActiva'
		, case when IsBloqued='S' then 1 else 0 end 'CombinaciónBloqueada'
		, case when ecomb.SKU is not null then 1 else 0 end 'CombinaciónExcluidaServicioSugeridoLocales'
	    , case when mp.ManufacturerId=p.ManufacturerId then 1 else 0 end [Marca Propia?]
	   , case when FP.Store is not null then 1 else 0 end [Farma Precio?]
	   , case when FF.Store is not null then 1 else 0 end [Fono Farma?]
	   , case when IsBloqued='S' then 1 else 0 end [Bloqueda?]
	   --restricciones
	   , case when r.Planograma is not null then 1 else 0 end [Planogramado]
	   , isnull(cast(minimo as nvarchar(4)),'') RestrMinimo
	   , isnull(cast(Fijo as nvarchar(4)),'') RestrCargaManual
	   , isnull(cast(planograma as nvarchar(4)),'') RestrPlanograma
	   , isnull(cast(dun as nvarchar(4)),'') RestrDUN
	   , isnull(cast(Maximo as nvarchar(4)),'') RestrMaximo
	   , isnull(cast(CajaTira as nvarchar(4)),'') RestrCajaTira
	   , isnull(cast(FijoEstricto as nvarchar(4)),'') RestrFijoEstricto
	   , r.NewRestrictedSuggest [Sugerido Nuevo con Restr]
	   , case when r.NewRestrictedSuggest>r.Suggest then r.NewRestrictedSuggest-r.Suggest else 0 end Alza
	   , case when r.NewRestrictedSuggest<r.Suggest then r.Suggest-r.NewRestrictedSuggest else 0 end Baja
	   , r.Cost*r.NewRestrictedSuggest [Valorado Costo Actual]
FROM minmax.OutsideMixSuggestResult r
inner join products.FullProducts p on r.sku = p.sku
left join operation.PrivateLabelManufacturers Mp on p.ManufacturerId=mp.ManufacturerId
left join (SELECT ManufacturerId FROM operation.InOperationExcludedManufacturerAllStore) mop on mop.ManufacturerId=p.ManufacturerId
left join temp.LocalesFarmaPrecio FP on r.store=FP.Store
left join temp.LocalesFonoFarma FF on r.store=FF.Store
left join series.LastStock Ls on ls.sku=r.sku and r.store=ls.Store
--left join aux.DailySuggestMix dsm on dsm.SKU=ls.SKU and dsm.Store=ls.Store
--left join products.GenericCategory gc on gc.Id=p.GenericCategory
left join operation.InOperationExcludedCombinations ecomb on ecomb.SKU=ls.SKU and ecomb.Store=ls.Store
left join (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 2) m on m.SKU=ls.SKU
left join (SELECT SKU FROM operation.InOperationExcludedProductsAllStores) esku on esku.SKU=ls.SKU
left join (select store from stores.Stores where store in (select distinct Store from replenishment.[Routes])) j on j.Store=ls.Store
left join (SELECT Store FROM operation.InOperationStoreAllProducts WHERE InOperation = 0) estore on estore.Store = ls.Store
left join stores.FullStores fs on fs.Store = ls.Store
where r.ExecutionDate=@executionDate
END
