﻿
-- =============================================
-- Author:		Juan Pablo Cornejo	
-- Create date: 2015-02-19
-- Description:	Entrega información relevante para el análisis especial de modelos de demanda.
-- =============================================
CREATE PROCEDURE [analysis].[DemandModelSpecialRevision]
	-- Add the parameters for the stored procedure here
	@diaCalculoPronostico date,
	@SemanaUsoPronóstico date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- query con datos
	declare @dateM date = (select max(date) from reports.MetricasStocks)

delete from forecast.SpecialRevision
insert into forecast.SpecialRevision 
 Select 
b.sku, p.Name Descripción, p.Division_Desc Division,p.ManufacturerDesc Laboratorio, p.Motive Motivo, p.TotalRanking Ranking
, case when ManufacturerId in (18,92) or categoryId = 1 then 1 else 0 end EsGenerico
, case when k.sku is null then 'Sin Servicio' else 'Con Servicio' end as EstadoServicio

--, b.PromLoc, b.sugDiarioxLocal, b.VentasSemProm
, PromLocalesNormales PromLoc, Quantity VentasSemProm
, b.Quantity/PromLocalesNormales ventasSemxLocalProm
, c.DateKey CálculoPronóstico, c.date UsoPronóstico, c.[Corregido?], c.Fit Pronóstico, c.CorrectedFit PronósticoAnterior, Corrección
, d.PromLocalesNormales, d.LocalesHoy, d.DiferenciaCobertura

from (
select SKU, AVG(cast(Quantity as float))Quantity--, count(*)
from forecast.WeeklySales
where date >=dateadd(week,-13,@dateM)
group by SKU
	--select sku, avg(promlocales) PromLoc, avg(sugDiarioxLocal) sugDiarioxLocal, avg(ventasSem) VentasSemProm
	--, avg(ventasSemxLocal) ventasSemxLocalProm
	--from (
	--	select dbo.GetMonday(date) semana, sku, avg(locales) promlocales, avg(suglocal) sugDiarioxLocal, sum(ventas) ventasSem, convert(float,sum(ventas))/avg(locales) ventasSemXLocal
	--	from (
	--		select Date,r.sku, count(*) locales, sum(suggest)/count(*) suglocal, sum(Quantity) ventas, convert(float,sum(Quantity))/count(*) ventasxlocal
	--		from reports.Reportstores r
	--		inner join mix.PilotDetails p on p.SKU = r.SKU 
	--		where suggest>0 and date <= getdate()-2 and PilotId = 3
	--		group by date,r.sku
	--	) s
	--	group by dbo.GetMonday(date), sku
	--) a
	--group by a.sku
) b
left JOIN (	 
	select ProductId SKU, datekey, ps.date date
			, fit, CorrectedFit
			, CASE WHEN FIT>CorrectedFit then 'Aumenta' else (CASE WHEN fit<CorrectedFit then 'Disminuye' else null end) end Corrección
			, CASE WHEN CorrectedFit is not null then 1 else 0 end [Corregido?]
	from forecast.PredictedDailySales ps
	inner join (	
		-- laboratorios marcas propias
		SELECT sku 
		from products.products 
		where  ManufacturerId in (select ManufacturerId from [operation].[PrivateLabelManufacturers])
		union 
		-- genéricos
		select sku 
		from products.products 
		where ManufacturerId in(18,92) or CategoryId =1
	) pd on pd.sku = ps.ProductId 
	where datekey=@diaCalculoPronostico and ps.date=@SemanaUsoPronóstico
	group by ProductId, datekey, ps.Date, fit, CorrectedFit
) c on b.SKU=c.SKU
left join (
	select sku, PromLocales PromLocalesNormales
			, isnull(Locales,0) LocalesHoy
			, case when Locales is null then -1 else (Locales-PromLocales)/cast(PromLocales as float) end DiferenciaCobertura 
			, L
	from (
		select ms1.sku, avg(ms1.locales) PromLocales, count(*) L, ms2.locales
		from (
			select * from reports.MetricasStocks 
			where date between dateadd(week,-12,dbo.GetMonday(@dateM))
				and dateadd(day,-1,dbo.GetMonday(@dateM))) ms1 
		LEFT JOIN (select * from reports.MetricasStocks WHERE date=@dateM) ms2 on ms1.sku=ms2.sku
		group by ms1.sku, ms2.locales
	) a
) d on d.SKU=b.sku
inner JOIN products.Products p on b.SKU=p.sku
left join (select  * from operation.ServicesProductsDetail where ServiceId=3) k on b.SKU=k.sku
where c.Fit is not null

END

