﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-05-09
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[DailyReportStores] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select cast([Date] as date) Fecha, count(distinct SKU) Productos, COUNT(distinct Store) Locales, COUNT(*) TotalCombinaciones
		, SUM(Suggest) SumaSugerido, SUM(Stock) SumaStock, SUM(VE) VentaEsperada, SUM(Quantity) VentaReal, SUM(LostSale) VentaPerdida
		, SUM(case when MixActive=1 then 1 else 0 end) CombinacionesPricing
		, SUM(case when Suggest=0 then 1 else 0 end) Desactivados
		, SUM(case when Stock=0 then 1 else 0 end) CombQuebradas
		, SUM(case when Stock=0 and MixActive=1 then 1 else 0 end) CombQuebradasPricing
	from reports.ReportStores rs 
	where [Date] >= dateadd(day, -21,getdate())
	group by [Date]
	order by [Date] desc


-- otros indicadores/revisiones Coni (NO BORRAR !!!!)

---- Share Histórico para visualizar comportamiento normal
--select s.Week, sum(share) Share 
--from forecast.ShareStoreHistory s
--group by Week
--order by week

---- Share que se va a utilizar la próxima semana o ya se está usando dependiendo si lo mira antes o después del día Jueves
--select s.Week, sum(share) ÚltimoCálculoShare 
--from forecast.ShareStoreCurrent s
--group by Week


-- Alerta de locales que llegan en foto de stock pero no están en el maestro, generan problemas de cobertura en el modelo, 
select cast(date as date) ÚltimaFotoStock, count(distinct rs.store) LocalesFoto, count(distinct s.store) LocalesMaestro
from series.LastStock rs
FULL OUTER JOIN stores.FullStores S ON rs.Store=s.Store
WHERE s.store is null
group by date
order by date desc

--Detalle de locales que faltan en el maestro para solicitar a SB
--select distinct cast(date as date) Date, rs.store LocalFoto, s.store LocalMaestro
--from series.LastStock rs
--FULL OUTER JOIN stores.FullStores S ON rs.Store=s.Store
--where s.store is null
--order by date desc

-- Alerta de alguna restricción con FechaFin o FechaInicio que no corresponda y se note que sea por error al usar Excel.
SELECT MAX(BeginDate) MaxFechaInicio, MAX(EndDate) MaxFechaFin
from br.SuggestRestrictionDetail

END
