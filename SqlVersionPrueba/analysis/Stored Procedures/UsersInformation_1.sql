﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-07-23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[UsersInformation]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select pu.ID, Username, pu.Name Nombre
		, isnull(cr.Name, pr.Name) RolDSS
		, isnull(cr.[Description], pr.[Description]) Descripción
		--, case end
	from users.PricingUser pu
	left join users.CustomUser cu on cu.PricingUserId=pu.Id
	left join users.CustomRole cr on cr.Id=cu.CustomRoleId
	left join users.PricingRole pr on pr.Id=pu.PricingRoleId
	order by Username


	select r.Id, r.Name Nombre, r.[Description], r.CustomSB, sum(case when cu.Id is not null OR pu.id is not null then 1 else 0 end) R
	from (
	select *, 1 CustomSB
	from  users.CustomRole cr 
	union all 
	select * , 0
	from users.PricingRole pr 
	where id <> 7
	) r
	left join users.CustomUser cu on cu.CustomRoleId=r.Id and r.CustomSB=1
	left join users.PricingUser pu on pu.PricingRoleId=r.Id and r.CustomSB=0
	group by r.Name, r.[Description], r.CustomSB, r.Id
	order by CustomSB, Id 

END
