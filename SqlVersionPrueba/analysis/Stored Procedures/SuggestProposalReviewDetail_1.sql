﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-04-21
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[SuggestProposalReviewDetail]
	-- Add the parameters for the stored procedure here

	@date date  = null

AS
BEGIN
	SET NOCOUNT ON;

	if(@date is null)
	begin
		set @date = GETDATE()
	end

	--drop table #datos
	create table #datos (SKU int,Store int
		, NSMax int, NSMin int, NSRate float, [NSDays] int --NextReplenishment
		, stock int, LSSuggest int, [LSMax] int,[LSMin] int --LastStock
		, estado nvarchar(10), deltaUnidades int
		, Menora5deltaUn int
		
		--restricciones
		, minActivo nvarchar(20), valorMin int
		, fijoActivo nvarchar(20), valorFijo int
		, DUNActivo nvarchar(20), valorDUN int
		, PlanogramaActivo nvarchar(20), valorPlanograma int
		, maxActivo nvarchar(20), valorMax int
		--, cajaTiraActivo nvarchar(20), valorCajaTira int
		, FijoEstrictoActivo nvarchar(20), valorFijoEstricto int
		

		, deltaDemanda float, estadoReposición nvarchar(15), estadoDemanda nvarchar(15)
		, ultimaSugerencia int, estadoUltimaSugerencia nvarchar(15), fechaUltimaSugerencia smalldatetime

		, TeníaSugerenciaAnterior bit
		, ultimaPromoción varchar(50), fechaUltimaPromocion smalldatetime, SaliendoDePromoción bit
		, tipoProducto nvarchar(50)
		, Genérico bit
		, enRestriccion bit
		--, CajaTira bit
		, SaliendoDeFeriado bit, fechaUltimoFeriado smalldatetime
		, CombFeriadoFindeAño bit
		, CombAlzaEstival bit)

	insert into #datos
	SELECT NS.[SKU],NS.[Store],NS.[Max],NS.[Min],NS.[Rate],NS.[Days],LS.Stock,LS.Suggest,LS.[Max],LS.[Min]
		,case when NS.[Max]>LS.[Max] then 'Alza' when NS.[Max]<LS.[Max] then 'Baja' end
		,case when NS.[Max]>LS.[Max] then NS.[Max]-LS.[Max] when NS.[Max]<LS.[Max] then LS.[Max]-NS.[Max] end
		,case when (case when NS.[Max]>LS.[Max] then NS.[Max]-LS.[Max] when NS.[Max]<LS.[Max] then LS.[Max]-NS.[Max] end<=5) then 1 else 0 END
		,'S/R',0,'S/R',0,'S/R',0,'S/R',0,'S/R',0,'S/R',0
		,null,'SinInformación',null,null,null,null
		,0
		,'SinPromocion',null,0,'Normal',0,0,0,null,0,0
	from replenishment.NextSuggest NS
	inner join series.LastStock LS on NS.SKU = LS.SKU and NS.Store = LS.Store
	WHERE NS.[Date] = @date 
	--comentar
	and ns.sku not in ((select SKU from products.GenericFamilyDetail))

	--restricciones
	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	update d 
		set minActivo = case when srt.Minimo=d.NSMin then 'Mínimo Activo' when srt.Minimo is not null then '' else 'S/R' end, valorMin=isnull(srt.Minimo,0)
		, DUNActivo = case when srt.MedioDUN=d.NSMin then 'DUN Activo' when srt.MedioDUN is not null then ''  else 'S/R'  end, valorDUN=isnull((srt.MedioDUN-1)*2,0)
		, fijoActivo = case when srt.Fijo=d.NSMin then 'Fijo Activo' when srt.Fijo is not null then ''  else 'S/R'  end, valorFijo=isnull(srt.Fijo,0)
		, planogramaActivo = case when srt.Planograma=d.NSMin then 'Planograma' when srt.Planograma is not null then ''  else 'S/R'  end, valorPlanograma=isnull(srt.Planograma,0)
		, maxActivo = case when srt.Maximo=d.NSMin then 'Máximo Activo' when srt.Maximo is not null then ''  else 'S/R'  end, valorMax=isnull(srt.Maximo,0)
		--, cajaTiraActivo = case when d.NSMin % srt.CajaTira = 0 then 'Caja Tira Activo' when srt.CajaTira is not null then ''  else 'S/R'  end, valorCajaTira=isnull(srt.CajaTira,0)
		, FijoEstrictoActivo = case when srt.FijoEstricto=d.NSMin then 'Mínimo Estricto Activo' when srt.FijoEstricto is not null then ''  else 'S/R'  end, valorFijoEstricto=isnull(srt.FijoEstricto,0)
	from #datos d 
	--inner join [view].SuggestRestrictionToday srt on srt.SKU=d.SKU and srt.Store=d.Store
	inner join #SuggestRestrictionToday srt on srt.SKU=d.SKU and srt.Store=d.Store

	--días de reposicion, variación demanda
	update d set d.estadoReposición = case 
			when d.NSDays>s.[Days] then 'AumentoDías' 
			when d.NSDays=s.[Days] then 'IgualDías' 
			when d.NSDays<s.[Days] then 'DisminuciónDías' 
			else 'SinInformación' 
		end
		, d.deltaDemanda= case 
			when s.[Days] >0 then case 
				when (s.Rate/s.[Days]) >0 then ((d.NSRate/d.NSDays)-(s.Rate/s.[Days]))/(s.Rate/s.Days) 
			end 
		end
		, d.ultimaSugerencia=s.[Max]
		, d.fechaUltimaSugerencia=s.[Date]
		, d.TeníaSugerenciaAnterior=case when s.[Date] is not null then 1 else 0 end
	from #datos d 
	inner join (
		
		--último envío de sugerido x cada combinacion
		select * from (
		select h.SKU, h.Store, h.Days, h.Date, h.Rate, h.[Max], ROW_NUMBER() over (partition by h.SKU, h.Store order by h.[Date] desc) R
		from suggest.SuggestDownloadHistory h
		) a where R=1
	) s on d.sku  = s.SKU and d.store = s.store

	--estado demanda
	update d set d.estadoDemanda = case 
			when d.deltaDemanda>0.1 then 'AumentoDDA' 
			when d.deltaDemanda<-0.1 then 'DisminuciónDDA' 
			when d.deltaDemanda between -0.1 and 0.1 then 'IgualDDA' 
			else 'SinInformacion' 
		end
		--, d.estadoReposición= case when d.estadoReposición in ('AumentoDías','DisminuciónDías') and d.deltaDemanda not between -0.01 and 0.01 then 'SinInformación'  end
		, d.estadoUltimaSugerencia= case when ultimaSugerencia=LSMax then 'Aprobada' when ultimaSugerencia<>LSMax then 'Rechazada' else 'SinInformación' end 
	from #datos d

	--info promociones
	update d set ultimaPromoción=p.Nombre, fechaUltimaPromocion=p.EndDate
	, d.SaliendoDePromoción= case when p.EndDate between DATEADD(day,-6,getdate()) and DATEADD(day,+6,getdate()) then 1 else 0 END
	from #datos d inner join (
		select * from (select * , ROW_NUMBER() over (partition by ProductId order by EndDate desc) R
		from (
			select ProductId, [FileName] Nombre, BeginDate, EndDate from promotion.PromotionFiles p inner join promotion.PromotionStock d on d.PromotionFileId=p.PromotionFileID where EndDate <= dateadd(day,6,@date)
		) a ) b where R=1
	) p on p.ProductId=d.sku

	--marcas de producto
	--update d set d.tipoProducto = 'MP'
	--from #datos d where sku in (select SKU from products.Products where ManufacturerId in (select ManufacturerId from operation.PrivateLabelManufacturers))
	--update d set d.tipoProducto = 'PilotoDistintoNS'
	--from #datos d where sku in (select SKU from temp.PruebaMinMaxDistintoNS)
	--update d set d.tipoProducto = 'FF'
	--from #datos d where store in (select store from stores.stores where store in (select Store from temp.LocalesFonoFarma))
	
	update d set d.tipoProducto = 'MinMax'
	from minmax.MinMaxCombinations h
	inner join #datos d on d.SKU =h.SKU and d.Store=h.Store

	--productos caja tira
	/*update s set CajaTira=1
	from (--Restricciones con condicion
			select productID SKU
			, t.name
			, Detail Sugerido
			from br.SuggestRestrictionDetail d
			left join br.SuggestRestrictionType t on t.SuggestRestrictionTypeID=d.SuggestRestrictionTypeID
			where t.SuggestRestrictionTypeID = 7 and EndDate is NULL
			) CT
	inner join #datos s on s.SKU = CT.SKU*/

	--productos gENÉRICOS
	update s set Genérico=1
	from (select * from [products].[GenericFamilyDetail]) gen --select * from products.Products where ManufacturerId in (18,92) OR CategoryId=1) gen
	inner join #datos s on s.SKU = gen.SKU

		--productos FP
	--update s set Genérico=1
	--from (select * from TEMP.LocalesFarmaPrecio) gen
	--inner join #datos s on s.Store = gen.Store

	--productos en alza por restricción
	update s set enRestriccion=1
	from  (
		SELECT *, productid sku, StoreID store
		FROM [Salcobrand].[br].[SuggestRestrictionDetail]
		where EndDate between DATEADD(day, -6, GETDATE()) and GETDATE()
	) n 
	inner join #datos s on s.SKU = n.SKU and s.Store = n.Store and n.Detail = s.LSSuggest
	
	--productos Saliendo de Feriado
	update s set s.SaliendoDeFeriado= case when p.FechaFeriado between DATEADD(day,-10,getdate()) and DATEADD(day,+10,getdate()) then 1 else 0 END
	from #datos s 
	inner join (select sku, store, max(HolidayDate) FechaFeriado from minmax.HolidaySuggestNew where HolidayDate <= @date
	GROUP BY SKU, STORE)p on s.sku=p.sku and s.store=p.store

	-- Fecha último Feriado
	update s set s.fechaUltimoFeriado= FechaFeriado
	from #datos s 
	inner join (select sku, store, max(HolidayDate) FechaFeriado from minmax.HolidaySuggestNew where HolidayDate <= @date
	GROUP BY SKU, STORE)p on s.sku=p.sku and s.store=p.store

	--	-- Combinaciones Alza Estival
	--	update s set CombAlzaEstival=1
	--from  (
	--	SELECT distinct sku, store from temp.CombsEstivalesDefinitivasMinMax
	--) n 
	--inner join #datos s on s.SKU = n.SKU and s.Store = n.Store
	
	
	-- Combinaciones Alza por Feriado Fin de Año
		update s set CombFeriadoFindeAño=1
	from  (
		SELECT distinct sku, store from minmax.HolidayAuxData 
	) n 
	inner join #datos s on s.SKU = n.SKU and s.Store = n.Store


	select a.*
	, case when a.minActivo<>'S/R' or a.dunactivo<>'S/R' or a.fijoactivo<>'S/R' or a.PlanogramaActivo<>'S/R' or a.maxActivo<>'S/R' --or a.cajaTiraActivo<>'S/R' --or a.minEstrictoActivo<>'S/R'
			then 1 else 0 end Restricción 
	from #datos a
	WHERE a.deltaunidades is not null and a.estado is not null

END

	/*ALZAS Y BAJAS POR SKU*/
select NS.SKU, FP.Description, FP.TotalRanking,
		SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
		SUM(case when NS.Max>LS.Max then 1 else 0 end) [Combs Alzas],
		SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
		SUM(case when NS.Max<LS.Max then 1 else 0 end) [Combs Bajas]
		, Positioning_Desc, Class_Desc, ManufacturerDesc, LogisticAreaDesc, Division_Desc
		, cast(p.EndDate as date) FinPromoción
		, case when p.EndDate between DATEADD(day,-6,getdate()) and DATEADD(day,+6,getdate()) then 1 else 0 END SalidaPromCercana
	from replenishment.NextSuggest NS
	left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	left outer join products.FullProducts FP on NS.SKU=FP.SKU
	left join operation.PrivateLabelManufacturers M on m.ManufacturerId=fp.ManufacturerId
	LEFT JOIN (select productId, Nombre, BeginDate, EndDate, R from (select * , ROW_NUMBER() over (partition by ProductId order by EndDate desc) R
			from (
			select ProductId, Name Nombre, p.BeginDate, p.EndDate from promotion.Promotion p inner join promotion.PromotionDetail d on d.PromotionId=p.Id where p.EndDate<= dateadd(day,6,getdate())
			union
			select ProductId, [FileName] Nombre, BeginDate, EndDate from promotion.PromotionFiles p inner join promotion.PromotionStock d on d.PromotionFileId=p.PromotionFileID where EndDate<= dateadd(day,6,getdate())
		) a ) b where R=1) p on p.ProductId=NS.sku
	where LogisticAreaId not like 'Bespe.Pro' and NS.[Date] = cast(getdate() as date)
	--comentar
	and ns.sku not in ((select SKU from products.GenericFamilyDetail))
	group by NS.[Date], NS.SKU, FP.Description, FP.TotalRanking, Positioning_Desc, Class_Desc, ManufacturerDesc
		, LogisticAreaDesc, Division_Desc, p.endDate
