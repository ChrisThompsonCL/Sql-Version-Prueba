﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-03-25
-- Description:	Extrae información de Msh para 4, 8, 12 semanas y la venta perdida en 4 semanas
-- =============================================
CREATE PROCEDURE [analysis].[MarketshareAlert1]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @weeks1 int = 4, @weeks2 int = 8, @weeks3 int = 12
	declare @maxDateF smalldatetime
	declare @MinDate1wF smalldatetime
	declare @MaxDate1wF smalldatetime
	declare @MinDate2wF smalldatetime
	declare @MaxDate2wF smalldatetime
	declare @MinDate3wF smalldatetime
	declare @MaxDate3wF smalldatetime
	declare @maxDateC smalldatetime
	declare @MinDate1wC smalldatetime
	declare @MaxDate1wC smalldatetime
	declare @MinDate2wC smalldatetime
	declare @MaxDate2wC smalldatetime
	declare @MinDate3wC smalldatetime
	declare @MaxDate3wC smalldatetime

	/*** FARMA ***/
	select @maxDateF = max(date) from series.IMS
	select @MinDate1wF = DATEADD(week, -@weeks1+1, @maxDateF)
	select @MinDate2wF = DATEADD(week, -@weeks2+1, @maxDateF)
	select @MinDate3wF = DATEADD(week, -@weeks3+1, @maxDateF)
	select @MaxDate1wF = DATEADD(day, 6, @maxDateF)
	select @MaxDate2wF = DATEADD(day, 6, @maxDateF)
	select @MaxDate3wF = DATEADD(day, 6, @maxDateF)

	select @maxDateC = max(date) from series.Nielsen
	select @MinDate1wC = DATEADD(week, -@weeks1+1, @maxDateC)
	select @MinDate2wC = DATEADD(week, -@weeks2+1, @maxDateC)
	select @MinDate3wC = DATEADD(week, -@weeks3+1, @maxDateC)
	select @MaxDate1wC = DATEADD(day, 6, @maxDateC)
	select @MaxDate2wC = DATEADD(day, 6, @maxDateC)
	select @MaxDate3wC = DATEADD(day, 6, @maxDateC)

	SELECT p.division_desc División,p.Positioning_Desc [Posicionamiento/Clase], m1.SKU, p.Name Descripción
		, MshVentas4Sem, UnidadesSB4Sem, UnidadesPerdidas4Sem, VentasSB4Sem, VentaPerdida4Sem, PrecioSB4Sem, PrecioComp4Sem,Stock4Sem
		, MshVentas8Sem, UnidadesSB8Sem, UnidadesPerdidas8Sem, VentasSB8Sem, VentaPerdida8Sem, PrecioSB8Sem, PrecioComp8Sem,Stock8Sem
		, MshVentas12Sem, UnidadesSB12Sem, UnidadesPerdidas12Sem, VentasSB12Sem, VentaPerdida12Sem, PrecioSB12Sem, PrecioComp12Sem,Stock12Sem
	FROM
	(
		select SKU, SUM(UnidadesSB)UnidadesSB4Sem, SUM(VentasSB)VentasSB4Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades4Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas4Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB4Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp4Sem
		from series.IMS
		where date >= @MinDate1wF
		group by SKU
		HAVING SUM(UnidadesCadenas) >0
	) m1
	INNER JOIN (
		select SKU, SUM(UnidadesSB)UnidadesSB8Sem, SUM(VentasSB)VentasSB8Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades8Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas8Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB8Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp8Sem
		from series.IMS
		where date >= @MinDate2wF
		group by SKU
	) m2 ON m2.SKU=m1.SKU
	INNER JOIN(
		select SKU, SUM(UnidadesSB)UnidadesSB12Sem, SUM(VentasSB)VentasSB12Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades12Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas12Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB12Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp12Sem
		from series.IMS
		where date >= @MinDate3wF
		group by SKU
	) m3 ON m3.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas4Sem, SUM(LostSale*SPrice) VentaPerdida4Sem, sum(Stock) Stock4Sem
		from reports.ReportStores
		where date between @MinDate1wF and @MaxDate1wF
		group by SKU
	) v1 on v1.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas8Sem, SUM(LostSale*SPrice) VentaPerdida8Sem, sum(Stock) Stock8Sem
		from reports.ReportStores
		where date between @MinDate2wF and  @MaxDate2wF
		group by SKU
	) v2 on v2.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas12Sem, SUM(LostSale*SPrice) VentaPerdida12Sem, sum(Stock) Stock12Sem
		from reports.ReportStores
		where date between @MinDate3wF and  @MaxDate3wF
		group by SKU
	) v3 on v3.SKU=m1.SKU
	INNER JOIN products.Products p on p.SKU=m1.SKU
	WHERE p.Division=1

	/*** CONSUMO MASIVO ***/
	UNION ALL

	SELECT p. Division_Desc División, p.Class_Desc, m1.SKU, p.Name Descripción
		, MshVentas4Sem, UnidadesSB4Sem, UnidadesPerdidas4Sem, VentasSB4Sem, VentaPerdida4Sem, PrecioSB4Sem, PrecioComp4Sem,Stock4Sem
		, MshVentas8Sem, UnidadesSB8Sem, UnidadesPerdidas8Sem, VentasSB8Sem, VentaPerdida8Sem, PrecioSB8Sem, PrecioComp8Sem,Stock8Sem
		, MshVentas12Sem, UnidadesSB12Sem, UnidadesPerdidas12Sem, VentasSB12Sem, VentaPerdida12Sem, PrecioSB12Sem, PrecioComp12Sem,Stock12Sem
	FROM
	(
		select SKU, SUM(UnidadesSB)UnidadesSB4Sem, SUM(VentasSB)VentasSB4Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades4Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas4Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB4Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp4Sem
		from series.Nielsen
		where date >= @MinDate1wC
		group by SKU
		HAVING SUM(UnidadesCadenas) >0
	) m1
	INNER JOIN (
		select SKU, SUM(UnidadesSB)UnidadesSB8Sem, SUM(VentasSB)VentasSB8Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades8Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas8Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB8Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp8Sem
		from series.Nielsen
		where date >= @MinDate2wC
		group by SKU
	) m2 ON m2.SKU=m1.SKU
	INNER JOIN(
		select SKU, SUM(UnidadesSB)UnidadesSB12Sem, SUM(VentasSB)VentasSB12Sem
			, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidades12Sem
			, SUM(VentasSB)/SUM(VentasCadenas) MshVentas12Sem
			, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB12Sem
			, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp12Sem
		from series.Nielsen
		where date >= @MinDate3wC
		group by SKU
	) m3 ON m3.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas4Sem, SUM(LostSale*SPrice) VentaPerdida4Sem, sum(Stock) Stock4Sem
		from reports.ReportStores
		where date between @MinDate1wC and @MaxDate1wC
		group by SKU
	) v1 on v1.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas8Sem, SUM(LostSale*SPrice) VentaPerdida8Sem, sum(Stock) Stock8Sem
		from reports.ReportStores
		where date between @MinDate2wC and @MaxDate2wC
		group by SKU
	) v2 on v2.SKU=m1.SKU
	INNER JOIN (
		select SKU, SUM(LostSale) UnidadesPerdidas12Sem, SUM(LostSale*SPrice) VentaPerdida12Sem, sum(Stock) Stock12Sem
		from reports.ReportStores
		where date between @MinDate3wC and @MaxDate3wC
		group by SKU
	) v3 on v3.SKU=m1.SKU
	INNER JOIN products.Products p on p.SKU=m1.SKU
	WHERE p.Division=2




END
