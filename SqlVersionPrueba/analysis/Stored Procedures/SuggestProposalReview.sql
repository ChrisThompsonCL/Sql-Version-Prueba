﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2013-01-16
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[SuggestProposalReview]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    /*ALZAS Y BAJAS TOTALES*/ --PARA COMPROBAR VALORES DE REPORTE Y VER ALERTAS
	select NS.[Date],
		SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) ALZAS,
		SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) BAJAS,
		(select convert(int,count(*)*0.11) from salcobrand.aux.dailysuggestmix) ALERTA
	from replenishment.NextSuggest NS
	inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	inner join products.FullProducts FP on NS.SKU=FP.SKU
	where LogisticAreaDesc not like 'Bespe.Pro'
	--and ns.SKU in (select SKU from products.GenericFamilyDetail)	
	group by NS.[Date]

/*ALZAS Y BAJAS TOTALES SIN GENÉRICOS*/--PARA COMPROBAR VALORES DE REPORTE Y VER ALERTAS
	select NS.[Date],
		SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) ALZAS,
		SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) BAJAS,
		(select convert(int,count(*)*0.11) from salcobrand.aux.dailysuggestmix) ALERTA
	from replenishment.NextSuggest NS
	inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	inner join products.FullProducts FP on NS.SKU=FP.SKU
	where LogisticAreaDesc not like 'Bespe.Pro'
	and ns.SKU not in (select SKU from products.GenericFamilyDetail)
	group by NS.[Date]

	/*ALZAS Y BAJAS TOTALES Familia y Combinación GENERICOS */ --PARA COMPROBAR VALORES DE REPORTE 
SELECT  SUM(ALZASF)[Alzas Familia/Store], SUM(BAJASF) [Bajas Familia/Store]
  , SUM(ALZASC)[Alzas SKU/Store], SUM(BAJASC) [Bajas SKU/Store]
FROM (select NS.[Date],gf.id, gf.[Name],ns.Store,
    MAX(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) ALZASF,
    MAX(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) BAJASF,
    sum(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) ALZASC,
    sum(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) BAJASC
  from replenishment.NextSuggest NS
  inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
  inner join products.FullProducts FP on NS.SKU=FP.SKU
  inner join products.genericfamilydetail gfd on gfd.SKU=ns.SKU
  inner join products.GenericFamily gf on gf.Id=gfd.GenericFamilyId
  where LogisticAreaDesc not like 'Bespe.Pro'
  group by NS.[Date],gf.id, gf.[Name],ns.Store
) a
group by [Date]

--------------------TABLAS REPORTE------------------------
		 
	/*ALZAS Y BAJAS POR BODEGA SIN GENÉRICOS*/
	SELECT B.LogisticAreaId, B.TotalRanking Ranking
	, case when A.[Total Alzas] is null then 0 else A.[Total Alzas] END [Total Alzas]
	, case when A.[Num Alzas] is null then 0 else A.[Num Alzas] end [Num Alzas]
	, case when A.[Total Bajas] is null then 0 else A.[Total Bajas] end [Total Bajas]
	, case when A.[Num Bajas] is null then 0 else A.[Num Bajas] end [Num Bajas]
	FROM (
			select LogisticAreaId, CASE WHEN TOTALRANKING IS NULL THEN 'NULL' ELSE TOTALRANKING END TotalRanking
			from
			(select distinct LogisticAreaId from products.Products where LogisticAreaId is not null and logisticareaid not in ('Bespe.Pro')) b
			cross join
			(select distinct TotalRanking from products.Products) a
	)B
	LEFT JOIN (
		select FP.LogisticAreaId, CASE WHEN FP.TOTALRANKING IS NULL THEN 'NULL' ELSE FP.TOTALRANKING END [Totalranking],
			SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
			SUM(case when NS.Max>LS.Max then 1 else 0 end) [Num Alzas],
			SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
			SUM(case when NS.Max<LS.Max then 1 else 0 end) [Num Bajas] 
		from replenishment.NextSuggest NS
		left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
		left outer join products.FullProducts FP on NS.SKU=FP.SKU
		where NS.[Date] = cast(GETDATE() as date) and NS.SKU not in (select SKU from products.GenericFamilyDetail)  --filtro de genericos despues del AND
		group by NS.[Date], FP.LogisticAreaId, FP.Totalranking
	) A ON B.TotalRanking=A.TotalRanking and a.LogisticAreaId=b.LogisticAreaId
	order by B.LogisticAreaId, B.TotalRanking


	  /*ALZAS Y BAJAS POR BODEGA DE GENÉRICOS*/
	SELECT B.LogisticAreaId, B.TotalRanking Ranking
	, case when A.[Total Alzas] is null then 0 else A.[Total Alzas] END [Total Alzas]
	, case when A.[Num Alzas] is null then 0 else A.[Num Alzas] end [Num Alzas]
	, case when A.[Total Bajas] is null then 0 else A.[Total Bajas] end [Total Bajas]
	, case when A.[Num Bajas] is null then 0 else A.[Num Bajas] end [Num Bajas]
	FROM (
			select LogisticAreaId, CASE WHEN TOTALRANKING IS NULL THEN 'NULL' ELSE TOTALRANKING END TotalRanking
			from
			(select distinct LogisticAreaId from products.Products where LogisticAreaId is not null and logisticareaid not in ('Bespe.Pro')) b
			cross join
			(select distinct TotalRanking from products.Products) a
	)B
	LEFT JOIN (
		select FP.LogisticAreaId, CASE WHEN FP.TOTALRANKING IS NULL THEN 'NULL' ELSE FP.TOTALRANKING END [Totalranking],
			SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
			SUM(case when NS.Max>LS.Max then 1 else 0 end) [Num Alzas],
			SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
			SUM(case when NS.Max<LS.Max then 1 else 0 end) [Num Bajas] 
		from replenishment.NextSuggest NS
		left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
		left outer join products.FullProducts FP on NS.SKU=FP.SKU
		where NS.[Date] = cast(GETDATE() as date) and NS.SKU in (select SKU from products.GenericFamilyDetail)  --filtro de genericos despues del AND
		group by NS.[Date], FP.LogisticAreaId, FP.Totalranking
	) A ON B.TotalRanking=A.TotalRanking and a.LogisticAreaId=b.LogisticAreaId
	order by B.LogisticAreaId, B.TotalRanking

	/*ALZAS Y BAJAS POR SKU*/--(VAN TODOS Y SE FILTRAN EN HOJA "RESUMEN")
	select NS.SKU, FP.Description, FP.TotalRanking,
		SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
		SUM(case when NS.Max>LS.Max then 1 else 0 end) [Num Alzas],
		SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
		SUM(case when NS.Max<LS.Max then 1 else 0 end) [Num Bajas],
		fp.Positioning_Desc Posicionamiento, fp.Class_Desc Clase, 
		fp.ManufacturerDesc Proveedor, fp.LogisticAreaDesc Bodega, fp.Division_Desc División,
		CASE WHEN  gfd.genericfamilyId IS NOT NULL THEN 1 ELSE 0 END IsGenericFinal
	from replenishment.NextSuggest NS
	inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	inner join products.FullProducts FP on NS.SKU=FP.SKU
	left join products.genericfamilydetail gfd on gfd.SKU=ns.SKU
	where LogisticAreaId not like 'Bespe.Pro' and NS.[Date] = cast(GETDATE() as date)
	group by NS.[Date], NS.SKU, FP.Description, FP.TotalRanking, 
	fp.Positioning_Desc, fp.Class_Desc, fp.ManufacturerDesc, fp.LogisticAreaDesc, 
	fp.Division_Desc,(CASE WHEN  gfd.genericfamilyId IS NOT NULL THEN 1 ELSE 0 END)
	order by NS.SKU

    /*ALZAS Y BAJAS FAMILIA STORE*/ -- en hoja Generico_Familia 

    select gf.id, gf.[Name],ns.Store,
    sum(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) Alza_SKUs,
    sum(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) Baja_SKUs,
    MAX(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) Alza,
    MAX(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) Baja,
	count(ns.sku) #SKU
    
    from replenishment.NextSuggest NS
    inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
    inner join products.FullProducts FP on NS.SKU=FP.SKU
    inner join products.genericfamilydetail gfd on gfd.SKU=ns.SKU
    inner join products.GenericFamily gf on gf.Id=gfd.GenericFamilyId
    where LogisticAreaDesc not like 'Bespe.Pro'
    group by NS.[Date],gf.id, gf.[Name],ns.Store


	
	
	
------------- procedimiento anterior (backup)---------------------	
	
	
	-- Insert statements for procedure here
	/*ALZAS Y BAJAS TOTALES*/
	--select NS.[Date],
	--	SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) ALZAS,
	--	SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) BAJAS,
	--	(select convert(int,count(*)*0.11) from salcobrand.aux.dailysuggestmix) ALERTA
	--from replenishment.NextSuggest NS
	--left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	--left outer join products.FullProducts FP on NS.SKU=FP.SKU
	--where LogisticAreaDesc not like 'Bespe.Pro'
	--group by NS.[Date]

		 
	--/*ALZAS Y BAJAS POR BODEGA*/
	--select FP.LogisticAreaId, FP.[Totalranking],
	--	SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
	--	SUM(case when NS.Max>LS.Max then 1 else 0 end) [Num Alzas],
	--	SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
	--	SUM(case when NS.Max<LS.Max then 1 else 0 end) [Num Bajas] 
	--from replenishment.NextSuggest NS
	--left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	--left outer join products.FullProducts FP on NS.SKU=FP.SKU
	--where LogisticAreaDesc not like 'Bespe.Pro' and NS.[Date] = cast(GETDATE() as date)
	--group by NS.[Date], FP.LogisticAreaId, FP.Totalranking
	--order by LogisticAreaId, TotalRanking


	--/*ALZAS Y BAJAS POR SKU*/
	--select NS.SKU, FP.Description, FP.TotalRanking,
	--	SUM(case when NS.Max>LS.Max then NS.Max-LS.Max else 0 end) [Total Alzas], 
	--	SUM(case when NS.Max>LS.Max then 1 else 0 end) [Num Alzas],
	--	SUM(case when NS.Max<LS.Max then LS.Max-NS.Max else 0 end) [Total Bajas], 
	--	SUM(case when NS.Max<LS.Max then 1 else 0 end) [Num Bajas],
	--	fp.Positioning_Desc Posicionamiento, fp.Class_Desc Clase, 
	--	fp.ManufacturerDesc Proveedor, fp.LogisticAreaDesc Bodega, fp.Division_Desc División 
	--from replenishment.NextSuggest NS
	--left outer join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	--left outer join products.FullProducts FP on NS.SKU=FP.SKU
	--where LogisticAreaId not like 'Bespe.Pro' and NS.[Date] = cast(GETDATE() as date)
	--group by NS.[Date], NS.SKU, FP.Description, FP.TotalRanking, fp.Positioning_Desc, fp.Class_Desc, fp.ManufacturerDesc, fp.LogisticAreaDesc, fp.Division_Desc
	--order by NS.SKU

END

