﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-09-26
-- Description:	Reporte GAP de quiebre
-- =============================================
CREATE PROCEDURE [analysis].[GAPQuiebre] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	declare @fecha date= (select max(fecha) from reports.DailyReportStores)
	
	select @fecha as FechaAnalisis

    /***TABLA 1: COMBINACIONES QUEBRADAS EN DETALLE***/ 
	select dsm.SKU, dsm.Store, p.Division_Desc, dsm.[Date], drs.Stock, drs.Suggest, drs.Transit
		, drs.StockCD, drs.StockTotal, drs.SugeridoTotal, drs.Faltante
		, drs.Nquiebre, drs.Locales
		, drs.UltimaVenta, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
		, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
		, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
		, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
		, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
	from  aux.DailySuggestMix dsm
	inner join products.Products p on p.SKU = dsm.SKU
	inner join (
		select *
		from reports.DailyReportStores
		where Fecha=@fecha
	) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store	
	left join (
		select *
		--from [view].SuggestRestrictionToday
		from #SuggestRestrictionToday
		where Fijo is not null OR FijoEstricto is not null
	) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
	where dsm.SKU not in (select SKU from [products].[GenericFamilyDetail]) --filtro genericos
	and isnull(totalranking,'NULL') not in ('Z','E')
	and isnull(motive,'')<>'2-Descontinuado'
	and isnull(ManufacturerId,'')<>990

	/***TABLA 2: COMBINACIONES QUEBRADAS DATOS AGREGADOS***/
	--declare @fecha date= (select max(fecha) from reports.DailyReportStores)
		SELECT u.Filtro, u.Combs
	FROM (
	  select count(*) Modelo
		, sum(case when drs.Stock is not null then 1 else 0 end) Quiebre
		, sum(case when drs.stock < 0 then 1 else 0 end) InfoStockMenorACero
		, sum(case when drs.Stock=0 and Promocion=1 then 1 else 0 end) EnPromocion
		, sum(case when drs.Stock=0 and Promocion=0 and srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt<>0 and sas.fillrate<=0.50 then 1 else 0 end) FallaProveedorAnt
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and FallaActual<>0 and sas.fillrate<=0.50 then 1 else 0 end) FallaActual
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and ((FallaActual<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaActual=0) and Faltante>StockCD and Nquiebre>StockCD then 1 else 0 end) StockCDBajo /*FALTANTE DEBE SER MENOR A LOCALES QEBRADOS*/
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and ((FallaActual<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaActual=0) and not (Faltante>StockCD and Nquiebre>StockCD) and ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and ((FallaActual<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaActual=0) and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) QuiebreModeloTOTAL
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and ((FallaActual<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaActual=0) and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) and Dias>30 and cast(DiasSinQuiebre as float) > 0.8*Dias and cast(venta as float)/DiasSinQuiebre < 0.5 and drs.Suggest=1 then 1 else 0 end) QuiebreModeloSugeridoUno
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and ((FallaProveedorAnt<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaProveedorAnt=0) and ((FallaActual<>0 and (sas.fillrate>0.50 or sas.fillrate is null)) or FallaActual=0) and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) and not (Dias>30 and cast(DiasSinQuiebre as float) > 0.8*Dias and cast(venta as float)/DiasSinQuiebre < 0.5 and drs.Suggest=1) then 1 else 0 end) QuiebreModeloResto
	  from  aux.DailySuggestMix dsm
	  left join (
		select *
		from reports.DailyReportStores
		where Fecha=@fecha and SKU not in (select SKU from [products].[GenericFamilyDetail]) --filtro genericos
	  ) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store  
	  left join (
		select *
		--from [view].SuggestRestrictionToday
		from #SuggestRestrictionToday
		where (Fijo is not null or FijoEstricto is not null)  -- filtro genericos
	  ) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
	  left join (select * from SalcobrandAbastecimiento.aux.FillRate)sas on sas.sku=drs.sku
	 where dsm.SKU not in (select SKU from [products].[GenericFamilyDetail]) -- filtro genericos
				and dsm.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
	) a
	unpivot (Combs for Filtro in 
	  (Modelo, Quiebre, InfoStockMenorACero, EnPromocion, RestrCargaManual
	  , FallaproveedorAnt, FallaActual, StockCDBajo, ProblemaReposicion
	  , QuiebreModeloTOTAL, QuiebreModeloSugeridoUno, QuiebreModeloRESTO
	))  u
	
	/***TABLA 3: RESUMEN ESTADO DE MIX***/
	-- total
	select 'Total' Categoría, count(*) Combinaciones, sum(case when Stock <= 0 then 1 else 0 end) CombinacionesQuebradas, sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float) [%Quiebre]
	from series.LastStock a
	where a.SKU not in (select SKU from [products].[GenericFamilyDetail]) -- filtro genericos
	and a.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
	UNION ALL
	select 'Bloqueadas', count(*), sum(case when Stock <= 0 then 1 else 0 end), sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float)
	from series.LastStock A 
	where IsBloqued = 'S' and A.SKU not in (select SKU from [products].[GenericFamilyDetail]) -- filtro genericos despues del and
	and a.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
	UNION ALL
	select 'Modelo', count(*), sum(case when Stock <= 0 then 1 else 0 end), sum(case when Stock <= 0 then 1 else 0 end)/cast(count(*) as float)
	from aux.DailySuggestMix dsm
	inner join series.LastStock ls on ls.SKU=dsm.SKU and ls.Store=dsm.Store
	where dsm.SKU not in (select SKU from [products].[GenericFamilyDetail]) -- filtro genericos
	and dsm.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)



	/***TABLA 5: RESUMEN QUIEBRE TOTAL VS QUIEBRE MODELO***/
	--declare @fecha date= (select max(fecha) from reports.DailyReportStores)
	select drs.Fecha Día
		, sum(case when drs.Stock is not null then 1 else 0 end) Quiebre
		, sum(case when drs.Stock=0 and Promocion=0 and not (srt.fijo is not null and srt.fijo=drs.Suggest) and FallaProveedorAnt=0 and FallaActual=0 and not (Faltante>StockCD and Nquiebre>StockCD) and not ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) QuiebreModeloTOTAL
	from  aux.DailySuggestMix dsm
	inner join (
		select *
		from reports.DailyReportStores
		where Fecha=@fecha
	) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store	
	left join (
		select *
		--from [view].SuggestRestrictionToday
		from #SuggestRestrictionToday
		where Fijo is not null OR FijoEstricto is not null
	) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
	where dsm.SKU not in (select SKU from [products].[GenericFamilyDetail]) --filtro genericos
	and dsm.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
	group by drs.Fecha


	--declare @fecha date= (select max(fecha) from reports.DailyReportStores)
	declare @quiebreTotal float = (
		select count(*) 
		from (
			select dsm.SKU, dsm.Store, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
				, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
				, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
				, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
				, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
			from  aux.DailySuggestMix dsm
			inner join products.Products p on p.SKU = dsm.SKU
			inner join (
				select *
				from reports.DailyReportStores
				where Fecha=@fecha 
			) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store	
			left join (
				select *
				--from [view].SuggestRestrictionToday
				from #SuggestRestrictionToday
				where Fijo is not null or FijoEstricto is not null
			) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
			where dsm.SKU not in (select SKU from [products].[GenericFamilyDetail]) --filtro genericos
				and dsm.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
		) a
		--WHERE FallaActual=0 and FallaProveedorAnt=0 and Promocion=0 and StockCDBajo=0 and RestrCargaManual=0 and InfoStockRara=0 and ProblemaReposicion=0
	) 

	/***TABLA 7: COMBINACIONES QUEBRADAS MODELO DETALLE***/
		
	SELECT p.Division_Desc División, p.name Descripción, p.logisticareaId Bodega, a.sku SKU
		, case when p.totalranking is null then '' else p.totalRanking end Ranking
		, case when Division=1 then p.Positioning_Desc when Division=2 then p.Class_Desc else NULL end [Posicionamiento/Clase]
		, a.ConQuiebre [Locales con Quiebre], a.Cobertura [Locales Totales], a.FaltanteProm [Prom. Faltante], a.StockCD [StockCD]
		,PesoSobreQuiebreTotal [Peso Sobre Quiebre Total]  
		,Transit [En Tránsito]
		,(select max(fecha) from [Salcobrand].reports.DailyReportStores) Date
		
	from (
		select a.SKU, count(*) ConQuiebre, Cobertura
			, avg(Faltante) FaltanteProm, max(stockCD) StockCD
			, CAST(count(*) as float)/(@quiebreTotal) PesoSobreQuiebreTotal
			, SUM(Transit)Transit
		FROM (
			select dsm.SKU, dsm.Store, p.Division_Desc, dsm.[Date], drs.transit
				, drs.StockCD, drs.StockTotal, drs.SugeridoTotal, drs.Faltante
				, drs.UltimaVenta, drs.FallaProveedorAnt, drs.FallaActual, drs.Promocion
				, (case when drs.stock < 0 then 1 else 0 end) InfoStockRara
				, (case when srt.fijo is not null and srt.fijo=drs.Suggest then 1 else 0 end) RestrCargaManual
				, (case when StockCD=0 OR (StockCD>0 and Faltante>StockCD and Nquiebre>StockCD) then 1 else 0 end) StockCDBajo
				, (case when ((StockTotalAjustado<0.6*SugeridoTotal and SugeridoTotal > Cobertura and drs.UltimaVenta <= dateadd(day,-5,@fecha)) OR (Dias>0 and cast(DiasSinQuiebre as float)<0.5*Dias and drs.UltimaVenta <= dateadd(day,-5,@fecha)) ) then 1 else 0 end) ProblemaReposicion
			from  aux.DailySuggestMix dsm
			inner join products.Products p on p.SKU = dsm.SKU
			inner join (
				select *
				from reports.DailyReportStores
				where Fecha=@fecha
			) drs on drs.SKU=dsm.SKU and drs.Store=dsm.Store	
			left join (
				select *
				--from [view].SuggestRestrictionToday
				from #SuggestRestrictionToday
				where Fijo is not null OR FijoEstricto is not null
			) srt on srt.SKU=dsm.SKU and srt.Store=dsm.Store
		) a
		LEFT JOIN (select sku, count(*) Cobertura from series.LastStock where IsBloqued<>'S' group by sku) b on a.sku=b.sku
		WHERE FallaActual=0 and FallaProveedorAnt=0 and Promocion=0 and StockCDBajo=0 and RestrCargaManual=0 and InfoStockRara=0 and ProblemaReposicion=0
		GROUP BY a.SKU,Cobertura
	) a
	LEFT JOIN products.Products p on a.sku=p.sku
	where  a.sku not in (select SKU from [products].[GenericFamilyDetail]) -- filtro genericos
		and a.sku not in (select sku from products.Products where isnull(totalranking,'NULL') in ('Z','E')
	and isnull(motive,'')='2-Descontinuado'
	and isnull(ManufacturerId,'')=990)
	order by PesoSobreQuiebreTotal desc

END