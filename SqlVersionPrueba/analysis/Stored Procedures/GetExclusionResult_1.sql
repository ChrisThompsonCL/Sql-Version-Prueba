﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[GetExclusionResult] 
	-- Add the parameters for the stored procedure here
	@date smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [Division], [Ranking], [IsGeneric], [SKU], ER.[Store], FS.Region0,
		   [Date], [Suggest], [Stock], [Cost], [StockVal], 1-[WithStock] [Quiebre], [Motive],  
		   CASE WHEN (ISNULL([Planogram],0)>0 OR ISNULL([DUN],0)>0 OR ISNULL([Child],0)>0 OR ISNULL([Minimum],0)>0) THEN 'Con Restricción' ELSE 'Sin Restricción' END [TieneRestriccion?], 
		   ISNULL([Planogram],0) [Planogram]
		   , ISNULL([DUN],0) [DUN]
		   , ISNULL([Child],0) [Child]
		   , ISNULL([Minimum],0) [Minimum]
		   , ISNULL(Maximum,0) Maximum
		   , ISNULL(Fixed,0) Fixed
		   , ISNULL(StrictFix,0) StrictFix
		   , [LastPromotion], Modelo
		   ,[3M],[4M],[5M],[6M],[12M]
	  FROM [Salcobrand].[reports].[ExclusionResults] ER
	  LEFT JOIN [Salcobrand].[stores].[FullStores] FS ON ER.Store=FS.Store
	  where Date = @date
END
