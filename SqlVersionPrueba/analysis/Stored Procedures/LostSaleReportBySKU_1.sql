﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-03-17
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[LostSaleReportBySKU]
	-- Add the parameters for the stored procedure here
	@date smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select fp.Division_Desc,fp.Motive, fp.CategoryDesc, fp.ManufacturerDesc
	, CASE WHEN fp.Division=1 THEN fp.Positioning_Desc ELSE NULL END Positioning_Desc
	, CASE WHEN fp.Division=2 THEN fp.Class_Desc ELSE NULL END Class_Desc
	, fp.SKU, fp.Name, rs.Date
		--, CASE WHEN IsBloqued='S' THEN 1 ELSE 0 END [Bloqueada?]
		--, case when rs.date between Max(BeginDate) and Max(EndDate) then 1 else 0 end [Promoción?]
		--, case when mm.sku is not null and mm.Store is not null then 1 else 0 end [MinMax?]
		, case when fp.ManufacturerId IN (18,92) OR fp.CategoryId=1 then 1 else 0 end [Genérico?]
		, cast(sum(case when Stock<= 0 then 1 else 0 end) as float) [NQuiebreLocales]
		, cast(sum(case when rs.ZeroStockCD=1 then 1 else 0 end) as float) [NQuiebreCD]
		, SUM(rs.LostSale) VtaPerdida
		, SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) VtaPerdidaVal
		, SUM(isnull(rs.Quantity,0)) VtaReal
		, SUM(isnull(rs.Income,0)) VtaRealVal
		, count(*) Combinaciones
		, case when SUM(isnull(rs.Income,0))>SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end) then 1 else 0 end MenorVtaPerdida
		, case when SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end)=0 then 1 else 0 end SinVentaPerdida
	from reports.ReportStores rs
	inner join products.Products fp on fp.sku=rs.SKU
	--LEFT JOIN promotion.PromotionDetail pm on rs.sku=pm.ProductId
	--LEFT JOIN temp.ComPilotoMINMAXdefinitivas MM ON rs.sku=mm.sku and rs.store=mm.Store
	where rs.Date =@date and MixActive=1
	group by fp.Division,fp.Division_Desc, fp.CategoryDesc, fp.ManufacturerDesc, fp.Positioning_Desc, fp.Class_Desc, fp.SKU, fp.Name, rs.Date,fp.Motive
	, ManufacturerId, CategoryId --, IsBloqued, mm.SKU, mm.Store, ManufacturerId, CategoryId
END