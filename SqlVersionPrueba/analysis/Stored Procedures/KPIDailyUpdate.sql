﻿-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2015-04-24
-- Description:	Entrega el estado actualizado de los kpis definidos para salcobrand.
-- =============================================
CREATE PROCEDURE [analysis].[KPIDailyUpdate] 
AS
BEGIN

declare @sku int=null,
@fecha_creacion datetime=null,
@fecha_inicio datetime = cast(getdate() as date), 
@fecha_fin datetime = cast(getdate() as date),
@ajuste_new bit= 0
	
create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
insert into #SuggestRestrictionToday
exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new
		 
-- Query de alertas KPI's
declare @date smalldatetime =cast(getdate() as date)
declare @Lunes smalldatetime =cast(dbo.getmonday(cast(getdate() as date)) as date)
DECLARE @beginDate	smalldatetime 
DECLARE @endDate	smalldatetime
SELECT @endDate = MAX([Date]) FROM pricing.LastPriceAndCost
SELECT @beginDate = DATEADD(day,-7,@endDate)
declare @yesterday varchar(10) = convert(date,dateadd(d,-1,getdate()),112)

exec analysis.UpdateScanner @yesterday

--STOCK VALORADO
select 'StockValorado' Stat
, SUM(Stock*pc.Cost)--/cast((select SUM(Stock*pc.Cost) StockVal from reports.reportstores rs 
	--LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku where rs.date=@Date-1)as float) 
	Modelo
, SUM(case when r.SKU is null then (Stock*pc.Cost) ELSE 0 END)--/cast((select SUM(Stock*pc.Cost) StockVal from reports.reportstores rs 
	--LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku where rs.date=@Date-1)as float) 
	[Modelo S/R]
from reports.reportstores rs 
LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku 
--LEFT JOIN [view].SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
LEFT JOIN #SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
where rs.date=@Date-1 and mixactive=1

UNION ALL
--STOCK VALORADO%
select '%StockValorado' Stat
, SUM(Stock*pc.Cost)/cast((select SUM(Stock*pc.Cost) StockVal from reports.reportstores rs 
	LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku where rs.date=@Date-1)as float) Modelo
, SUM(case when r.SKU is null then (Stock*pc.Cost) ELSE 0 END)/cast((select SUM(Stock*pc.Cost) StockVal from reports.reportstores rs 
	LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku where rs.date=@Date-1)as float) [Modelo S/R]
from reports.reportstores rs 
LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku 
--LEFT JOIN [view].SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
LEFT JOIN #SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
where rs.date=@Date-1 and mixactive=1
UNION ALL
--DÍAS DE INVENTARIO
select 'Días de Inventario' Stat
, SUM(Stock*Costo)/SUM(unidades*Costo)*344 Modelo
, (SUM(case when r.SKU is null then (Stock*Costo) else 0 end)/
	SUM(case when r.SKU is null then (unidades*Costo) else 0 end)*344) [Modelo S/R]
from (select rs.*, pc.Cost Costo from reports.reportstores rs LEFT JOIN pricing.lastpriceandcost pc on rs.sku=pc.sku
		where rs.date=@Date-1 and mixactive=1) a
--LEFT JOIN [view].SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store
LEFT JOIN #SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store
LEFT JOIN (select sku,store,sum(quantity) unidades,sum(income) valorado from series.Sales
where date >= dateadd(day,-366,@date) group by sku, store) b on a.sku=b.sku and a.store=b.store
UNION ALL
--QUIEBRES
select 'Quiebres' Stat, a.a, b.b from (SELECT '1' id, 'Modelo' KPI
, cast((select count(*) from (select * from series.LastStock where stock <=0 and IsBloqued <> 'S' ) a 
		inner join (select * from  aux.DailySuggestMix) b on a.sku = b.sku and a.Store = b.store)as float)/cast((select count(*) from
		(select * from series.LastStock where IsBloqued <> 'S') a
		inner join (select * from  aux.DailySuggestMix ) b on a.sku = b.sku and a.Store = b.Store)as float)a)a
left join (select '1' id, 'ModeloSinRestricciones' KPI
			, CAST(count(*) as float)/(select count(*) from (select * from series.LastStock where IsBloqued <> 'S') a
						inner join (select * from  aux.DailySuggestMix) b on a.sku = b.sku and a.Store = b.Store) b 
			FROM (
				select Fecha, a.sku,a.store,FallaProveedorAnt,FallaActual,Promocion
				, case when Fecha between MAX(r.FechaInicio) and MAX(r.FechaFin) then 1 else 0 END RestrCMact
				, case when faltante>stockCd then 1 else 0 end NoCubre
				, DATEDIFF(day,UltimaVenta,Fecha) Tiempo
				, (DATEDIFF(day,UltimaVenta,Fecha)-(DATEDIFF(day,UltimaVenta,Fecha)%7))/7 Agrupación
				, case when (DATEDIFF(day,UltimaVenta,Fecha)-(DATEDIFF(day,UltimaVenta,Fecha)%7))/7>12 then 12 
					else (DATEDIFF(day,UltimaVenta,Fecha)-(DATEDIFF(day,UltimaVenta,Fecha)%7))/7 end Agrupación2
				, case when StockCD<6 then 1 else 0 end [StockCD>6Un]
				from (select * from [Salcobrand].reports.DailyReportStores where Fecha=@date-1) a
				LEFT JOIN (select d.ProductID,d.StoreID, max(BeginDate) [FechaInicio], max(EndDate) [FechaFin]
							from br.SuggestRestrictionDetail d
							where d.SuggestRestrictionTypeID =2
							group by  d.SuggestRestrictionTypeID,d.ProductID,d.StoreID
							having max(endDate)>=getdate() 
							) r on r.ProductID=a.SKU and r.StoreID=a.Store
				group by a.sku,a.store,FallaProveedorAnt,FallaActual,Promocion,Fecha, stockCD, Faltante, UltimaVenta)a
				WHERE Agrupación2<=3 AND FallaActual=0 and FallaProveedorAnt=0 and Promocion=0 and [StockCD>6Un]=0 and NoCubre=0 and RestrCMact=0
				GROUP BY FECHA)b on a.id=b.id
UNION ALL
	--ingresos totales farma
select 'IngresosFARMA' Stat
, SUM(Income) Modelo
, SUM(case when r.SKU is null then Income else 0 end) [Modelo S/R]
from reports.reportstores rs
LEFT JOIN products.products p on rs.sku=p.sku
--LEFT JOIN [view].SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
LEFT JOIN #SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
where date=@Date-2 and mixactive=1 and Division=1

UNION ALL
	--ingresos totales consumo 
select 'IngresosCONSUMO' Stat
, SUM(Income) Modelo
, SUM(case when r.SKU is null then Income else 0 end) [Modelo S/R]
from reports.reportstores rs
LEFT JOIN products.products p on rs.sku=p.sku
--LEFT JOIN [view].SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
LEFT JOIN #SuggestRestrictionToday r on RS.sku=r.sku and RS.store=r.Store
where date=@Date-2 and mixactive=1 and Division=2

UNION ALL
	--COMBINACIONES
		-- Mix Activo
	select 'Combinaciones' Stat
	, cast(count(*) as float)--/cast((select count(*) from series.LastStock)as float) 
	Modelo
	, cast(SUM(case when r.SKU is null then 1 else 0 end) as float)--/cast((select count(*) from series.laststock)as float) 
	[Modelo S/R]
	from (select * FROM reports.reportstores where date=@Date-1 and mixactive=1) a
	--LEFT JOIN [view].SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store
	LEFT JOIN #SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store

UNION ALL	
	--MixActivo%
	select 'Combinaciones' Stat
	, cast(count(*) as float)/cast((select count(*) from series.LastStock)as float) Modelo
	, cast(SUM(case when r.SKU is null then 1 else 0 end) as float)/cast((select count(*) from series.laststock)as float) [Modelo S/R]
	from (select * FROM reports.reportstores where date=@Date-1 and mixactive=1) a
	--LEFT JOIN [view].SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store
	LEFT JOIN #SuggestRestrictionToday r on a.sku=r.sku and a.store=r.Store
UNION ALL
	-- Bloqueos
	select 'CombinacionesBloqueadas' Stat
	, ROUND(cast(sum(case when isbloqued='S' then 1 else 0 end) as float)/cast(count(*) as float),4) Modelo 
	, NULL [Modelo S/R]
	from (select * from series.laststock) a
UNION ALL
	-- Rechazos
	SELECT 'CombinacionesRechazadas',((cast((SELECT COUNT(*) IndicadoresRechazo FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A) as float)/cast
		((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where IsBloqued='N')b)as float))) RECHAZO
		, NULL [Modelo S/R]

UNION ALL
--VENTA PERDIDA
--Farma
select 'VtaPerdidaFarma'
,(SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end))/((SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end))+(SUM(isnull(rs.Income,0)))) Modelo
, NULL [Modelo S/R]
from reports.ReportStores rs
inner join products.Products fp on fp.sku=rs.SKU
where rs.Date between DATEADD(DAY,-2,@date) and @date-2 and MixActive=1 and division=1
group by rs.Date
UNION ALL
--Consumo
select 'VtaPerdidaConsumo'
, (SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end))/((SUM(case when rs.LostSale*rs.SPrice >0 then rs.LostSale*rs.SPrice else 0 end))+(SUM(isnull(rs.Income,0)))) Modelo
, NULL [Modelo S/R]
from reports.ReportStores rs
inner join products.Products fp on fp.sku=rs.SKU
where rs.Date between DATEADD(DAY,-2,@date) and @date-2 and MixActive=1 and division=2
group by rs.Date

UNION ALL
SELECT 'MSHventasFarma' ,
		CAST(sum(VENTASSB) AS FLOAT)/CAST(sum(VentasCadenas) AS FLOAT) Modelo 
		, NULL [Modelo S/R]
		FROM (SELECT p.sku
		,(case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end)*SUM(UnidadesSB) VentasSB
		, (case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end)*SUM(UnidadesCadenas) VentasCompetidores
		, (case when SUM(UnidadesCadenas) > 0 then SUM(VentasCadenas)/SUM(UnidadesCadenas) else 0 end)*SUM(UnidadesCadenas) VentasCadenas
		, (case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end) PrecioEfectivoSB
		, (case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end) PrecioEfectivoComp
		FROM (SELECT [Date], i.SKU, UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas
		FROM series.IMS i WHERE Date = (select max(date) from series.ims)) m
	--INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	--INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN products.Products p on p.SKU=m.SKU
	WHERE p.Division=1 group by p.sku)A
	WHERE VentasSB>0 and VentasCompetidores>0 
	--and (ABS(cast(PrecioEfectivoComp as float)-cast(PrecioEfectivoSB as float)))/(cast(PrecioEfectivoSB as float))<cast(0.2 as float)
UNION ALL
SELECT 'MSHventasConsumo' ,
		CAST(sum(VENTASSB) AS FLOAT)/CAST(sum(VentasCadenas) AS FLOAT) Modelo 
		, NULL [Modelo S/R]
		FROM (SELECT p.sku
		,(case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end)*SUM(UnidadesSB) VentasSB
		, (case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end)*SUM(UnidadesCadenas) VentasCompetidores
		, (case when SUM(UnidadesCadenas) > 0 then SUM(VentasCadenas)/SUM(UnidadesCadenas) else 0 end)*SUM(UnidadesCadenas) VentasCadenas
		, (case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end) PrecioEfectivoSB
		, (case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end) PrecioEfectivoComp
		FROM (SELECT Semana [Date], i.SKU, UnidadesSB, UnidadesCadena UnidadesCadenas, ValoresSB VentasSB, ValoresCadena VentasCadenas
		FROM series.NIELSEN i WHERE Semana = (select max(semana) from series.Nielsen)) m
	--INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	--INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN products.Products p on p.SKU=m.SKU
	WHERE p.Division=2 group by p.sku)A
	WHERE VentasSB>0 and VentasCompetidores>0 
END