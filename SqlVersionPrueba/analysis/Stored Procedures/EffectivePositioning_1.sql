﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-03-25
-- Description:	Obtiene datos de posicionamiento basado en mediciones de competencia para las últimas 4 semanas
-- =============================================
CREATE PROCEDURE [analysis].[EffectivePositioning]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @analisysWeeks	int
	DECLARE @minDate		smalldatetime
	DECLARE @maxDate		smalldatetime

	--PARAMETROS
	SET @analisysWeeks = 4

	/*** FARMA ***/
	SELECT @maxDate= max(Date) from series.IMS
	SELECT @minDate = DATEADD(WK, -(@analisysWeeks-1), @maxDate) 
	SET @maxdate = DATEADD(day,6,@maxDate)

	SELECT 
		cast(m.Date as date) [Date], p.Division_Desc DivisionName, dgn.Id CategoryId, dgn.Name CategoryName, ct.Name ClassName,p.SKU SKU , p.Name Descripcion
		, SUM(UnidadesCadenas) UnidadesCadenas
		, SUM(UnidadesSB) UnidadesSB
		, SUM(VentasCadenas) VentasCadenas
		, SUM(VentasSB) VentasSB
		, CostSB CostoSB
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioEfectivoSB
		, case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end PrecioEfectivoResto
		FROM (
		SELECT [Date], i.SKU, UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas
		FROM series.IMS i
		WHERE Date BETWEEN @minDate and @maxDate
	) m
	INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN products.Products p on p.SKU=m.SKU
	LEFT JOIN (
		SELECT dbo.GetMonday([Date]) [Date], SKU, AVG(Cost) CostSB
		FROM series.PriceAndCost
		WHERE [Date] BETWEEN @minDate and @maxDate
		GROUP BY dbo.GetMonday([Date]), SKU
	) c on c.SKU=m.SKU and c.Date=m.Date
	LEFT JOIN class.ProductClassificationFinal pcf on pcf.SKU = m.SKU 
	LEFT JOIN [class].ClassificationType ct ON ct.Id = pcf.ClassificationTypeId
	WHERE p.Division=1
	GROUP BY m.date, p.Division_Desc, dgn.Id, dgn.Name, ct.Name, p.SKU, p.Name, CostSB, UnidadesCadenas-UnidadesSB
	ORDER BY [Date], DivisionName, CategoryName, ClassName


	/*** CM ***/
	SELECT @maxDate= max(Date) from series.Nielsen
	SELECT @minDate = DATEADD(WK, -(@analisysWeeks-1), @maxDate) 
	SET @maxdate = DATEADD(day,6,@maxDate)

	SELECT 
		cast(m.Date as date) [Date], p.Division_Desc DivisionName, dgn.Id CategoryId, dgn.Name CategoryName, ct.Name ClassName,p.SKU SKU , p.Name Descripcion
		, SUM(UnidadesCadenas) UnidadesCadenas, SUM(UnidadesSB) UnidadesSB
		, SUM(VentasCadenas) VentasCadenas, SUM(VentasSB) VentasSB
		, CostSB CostoSB
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioEfectivoSB
		, case when SUM(UnidadesCadenas - UnidadesSB) > 0 then SUM(VentasCadenas - VentasSB)/SUM(UnidadesCadenas - UnidadesSB) else 0 end PrecioEfectivoResto
	FROM (
		SELECT [Date], i.SKU, UnidadesSB, UnidadesCadenas, VentasSB, VentasCadenas
		FROM series.Nielsen i
		WHERE Date BETWEEN @minDate and @maxDate
	) m
	INNER JOIN products.DemandGroupProduct dgp on dgp.SKU = m.SKU
	INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
	INNER JOIN products.Products p on p.SKU=m.SKU
	LEFT JOIN (
		SELECT dbo.GetMonday([Date]) [Date], SKU, AVG(Cost) CostSB
		FROM series.PriceAndCost
		WHERE [Date] BETWEEN @minDate and @maxDate
		GROUP BY dbo.GetMonday([Date]), SKU
	) c on c.SKU=m.SKU and c.Date=m.Date
	LEFT JOIN class.ProductClassificationFinal pcf on pcf.SKU = m.SKU 
	LEFT JOIN [class].ClassificationType ct ON ct.Id = pcf.ClassificationTypeId
	WHERE p.Division=2
	GROUP BY m.date, p.Division_Desc, dgn.Id, dgn.Name, ct.Name, p.SKU, p.Name, CostSB
	ORDER BY [Date], DivisionName, CategoryName, ClassName

END
