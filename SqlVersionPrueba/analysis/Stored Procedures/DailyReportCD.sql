﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-05-09
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[DailyReportCD]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 select top 10 cast([Date] as date) Fecha, count(distinct SKU) TotalProductos
		, SUM(StockCD) SumaStock
		, SUM(case when MixActive=1 then 1 else 0 end) ProductosPricing
		, SUM(case when StockCD=0 then 1 else 0 end) ProductosQuebrados
		, SUM(case when StockCD=0 and MixActive=1 then 1 else 0 end) ProdsQuebradosPricing
	from reports.ReportCD rs 
	group by [Date]
	order by [Date] desc


END
