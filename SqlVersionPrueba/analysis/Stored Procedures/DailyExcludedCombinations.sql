﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[DailyExcludedCombinations]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @date date = (select top 1 [date] from series.LastSale)

	/*DETALLE*/
    -- Insert statements for procedure here
	select *
	from (
	select ls.sku,
		 ls.Store
		-- causas de exclusión debidas a producto
		, case when fp.SKU is null then 1 else 0 end 'ProductoNOEnMaestro'
		, case when IsActive=1 then 0 else 1 end 'ProductoNOActivoEnMaestro'
		, case when MotiveId=2 then 1 else 0 end 'ProductoPorDescontinuar'
		, case when Class = 196 then 1 else 0 end 'ProductoAutoliquidable'
		, case when Subdivision=15 then 1 else 0 end 'ProductoEsServicio'
		, case when fp.LastSale >= dateadd(week,-24,getdate()) then 0 else 1 end 'ProductoSinVenta24Semanas'
		, case when fp.FirstSale>=@date OR fp.FirstSale is null then 1 else 0 end 'ProductoMuyNuevo'
		, case when a.SKU is null then 1 else 0 end 'ProductoNOEnElÁrbol(?)'
		--, case when m.SKU is null then 1 else 0 end 'ProductoSinServicioModelos'
		, case when pron.ProductId IS NULL then 1 else 0 end 'ProductoSinModelo'
		, case when mop.ManufacturerId is not null then 1 else 0 end 'ProveedorExcluidoServicioSugeridoLocales'
		, case when r.SKU is null then 1 else 0 end 'BodegaProductoSinRutaInformada'
		, case when esku.SKU is not null then 1 else 0 end 'ProductoExcluidoServicioSugeridoLocales'
		, case when fp.MotiveId in (6,10) then 1 else 0 end 'ProductoCompraPorSolicitud'
		, case when fp.TotalRanking = 'E' then 1 else 0 end'ProductoMarcadoEstacional'
		--, case when (fp.CategoryId=1 OR fp.ManufacturerId in (18,92)) AND pg.SKU is null then 1 else 0 end 'ProductoGenericoNoEnPiloto'
		--causas de exclusión debidas al local
		, case when fs.Store is null then 1 else 0 end 'LocalNOEnMaestro'
		, case when fs.LastSale < dateadd(week,-1,@date) then 1 else 0 end 'LocalSinVenta1Semana'
		, case when fs.FirstSale >= DATEADD(MONTH, -2, @date) OR fs.FirstSale is null then 1 else 0 end 'LocalNuevoVentaMenorA2Meses'
		, case when cd.StoreId is null then 1 else 0 end 'LocalNOEnElÁrbol(?)'
		, case when estore.Store is not null then 1 else 0 end 'LocalExcluidoServicioSugeridoLocales'
		, case when j.Store is null then 1 else 0 end 'LocalSinRutaInformada'
		--causas de exclusion debidas a la combinación
		, case when Suggest > 0 then 0 else 1 end 'CombinaciónNOActiva'
		, case when IsBloqued='S' then 1 else 0 end'CombinaciónBloqueada'
		, case when ecomb.SKU is not null then 1 else 0 end 'CombinaciónExcluidaServicioSugeridoLocales'
		--excluido o no
		, case when dsm.SKU is null then 1 else 0 end 'NOMixPricing'
	from series.LastStock ls
	left join products.FullProducts fp on fp.SKU = ls.SKU
	left join products.DemandGroupProduct a on a.SKU=ls.SKU
	left join (SELECT SKU FROM operation.ServicesProductsDetail WHERE ServiceId = 2) m on m.SKU=ls.SKU
	left join (SELECT ManufacturerId FROM operation.InOperationExcludedManufacturerAllStore) mop on mop.ManufacturerId=fp.ManufacturerId
	left join (select sku from products.FullProducts where LogisticAreaId in (select distinct LogisticAreaId from minmax.replenishment where [days]>0)) r on r.SKU=fp.SKU
	left join (SELECT SKU FROM operation.InOperationExcludedProductsAllStores) esku on esku.SKU=ls.SKU
	left join stores.FullStores fs on fs.Store = ls.Store
	left join (SELECT Store FROM operation.InOperationStoreAllProducts WHERE InOperation = 0) estore on estore.Store = ls.Store
	left join stores.ClusterDetail cd on cd.StoreId = ls.Store
	left join aux.DailySuggestMix dsm on dsm.SKU=ls.SKU and dsm.Store=ls.Store
	left join operation.InOperationExcludedCombinations ecomb on ecomb.SKU=ls.SKU and ecomb.Store=ls.Store
	left join (select store from stores.FullStores where store in (select distinct Store from minmax.replenishment where [days]>0)) j on j.Store=ls.Store
	--left join (select SKU from mix.PilotDetails where PilotId=1) pg on pg.SKU=ls.SKU
	--left join (select distinct ProductId from forecast.PredictedSales where DateKey=(select MAX(datekey) from forecast.PredictedSales where dbo.GetMonday(GETDATE())=[Date] and [DateKey] < [date])) pron on pron.ProductId=ls.SKU
	left join (select distinct ProductId from forecast.PredictedDailySales where DateKey=(select MAX(datekey) from forecast.PredictedDailySales where [Date] BETWEEN dbo.GetMonday(GETDATE()) AND DATEADD(DAY,6,dbo.GetMonday(GETDATE())) and [DateKey] < [date])) pron on pron.ProductId=ls.SKU	
	) a 
	where NOMixPricing=1


END
