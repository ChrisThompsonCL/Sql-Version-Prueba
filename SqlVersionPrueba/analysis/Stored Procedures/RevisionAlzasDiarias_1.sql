﻿
-- =============================================
-- Author:		Juan Pablo Cornejo
-- Create date: 2015-09-09
-- Description:	Consulta las alzas cargadas del día anterior para el último feriado
-- =============================================
CREATE PROCEDURE [analysis].[RevisionAlzasDiarias]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	
	select count(*) CombEnviadasAlza
		, SUM(case when (a.SMin <> ls.[Min] or  a.SMax <> ls.[Max]) then 1 else 0 end) CombEnviadasNoTomadasAlza
	from minmax.HolidayApprovedSuggests a
	inner join minmax.HolidaySuggestNew sg on a.sku=sg.sku and a.Store=sg.store and sg.HolidayDate=a.HolidayDate
	left join series.LastStock ls on ls.sku=a.sku and ls.Store=a.Store
	left join temp.StoreClassRank s on a.Store=s.Store
	inner join products.Products p on a.sku=p.sku
	LEFT JOIN (
		select * from series.StockCD where date = (select max(date) from series.stockCD)
	) cd on a.sku=cd.SKU
	where a.HolidayDate>=(select min(HolidayDate) from minmax.HolidayApprovedSuggests) 
	and sg.CreateDate=cast(getdate()-1 as date)


	select a.sku, p.Name, p.Division_Desc, p.TotalRanking, p.logisticAreaID, a.store, s.StoreClass ClaseLocal, a.HolidayDate FechaFeriado
	, sg.createDate FechadeAlzaAplicada, a.SMax SugeridoPropuestoMax, a.SMin SugeridoPropuestoMin, sg.increase AlzaPropuesta
	, IsBloqued, ls.Stock, cd.StockCD, ls.Max, ls.Min
	from minmax.HolidayApprovedSuggests a
	inner join minmax.HolidaySuggestNew sg on a.sku=sg.sku and a.Store=sg.store and sg.HolidayDate=a.HolidayDate
	left join series.LastStock ls on ls.sku=a.sku and ls.Store=a.Store
	left join temp.StoreClassRank s on a.Store=s.Store
	inner join products.Products p on a.sku=p.sku
	LEFT JOIN (
		select * from series.StockCD where date = (select max(date) from series.stockCD)
	) cd on a.sku=cd.SKU
	where a.HolidayDate>=(select min(HolidayDate) from minmax.HolidayApprovedSuggests) 
	and sg.CreateDate=cast(getdate()-1 as date)
	and (a.SMin <> ls.[Min] or  a.SMax <> ls.[Max])
	order by a.SMax desc

END

