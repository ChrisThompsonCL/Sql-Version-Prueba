﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-01-17
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[SuggestProposalCompliance] 
	-- Add the parameters for the stored procedure here
	@fecha smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	--set @fecha = DATEADD(day,1,@fecha)
	
select date, count(*) CombsTotales, SUM(case when MixActive=1 then 1 else 0 end) CombsPricing
, sum(case when isbloqued='S' then 1 else 0 end) combsBloqueadas
, cast(sum(case when isbloqued='S' then 1 else 0 end) as float)/cast(count(*) as float) [CombsBloq%sobreTotal]
 from (select * from reports.ReportStores where date>=GETDATE()-3) a
 group by date
 order by date desc

IF @fecha < (select max(Date) from suggest.SuggestDownloadHistory)
BEGIN

SELECT COUNT(*) IndicadoresRechazo FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A
UNION ALL
Select ((cast((SELECT COUNT(*) IndicadoresRechazo FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A) as float)/cast

		((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where IsBloqued='N')b)as float))) c


SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where not (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N')b
UNION ALL
Select ((cast((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where not(ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A) as float)/cast

		((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where IsBloqued='N')b)as float))) c
end
ELSE
begin

SELECT COUNT(*) IndicadoresRechazo FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A
UNION ALL
Select ((cast((SELECT COUNT(*) IndicadoresRechazo FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A) as float)/cast

		((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where IsBloqued='N')b)as float))) c


SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where not (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N')b
UNION ALL
Select ((cast((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where not(ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N') A) as float)/cast

		((SELECT COUNT(*) IndicadoresAprobación FROM(SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where IsBloqued='N')b)as float))) c

end


	IF @fecha < (select max(Date) from suggest.SuggestDownloadHistory)
	BEGIN
		--/****** [Salcobrand].[suggest].[SuggestDownloadHistory] H DATE+1 - REPORTSTORE DATE   ******/
		SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, rs.Stock , rs.Suggest [Suggest Sist], rs.Max [Max Sist], rs.Min [Min Sist], rs.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)rs on rs.SKU=a.SKU and rs.Store=a.Store
		left join Salcobrand.products.FullProducts p on a.SKU=p.SKU
		where (rs.[Max]<>a.[Max] or rs.[Min]<>a.[Min]) and IsBloqued='N'

		SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, rs.Stock , rs.Suggest [Suggest Sist], rs.Max [Max Sist], rs.Min [Min Sist], rs.[isBloqued]
		FROM (select * from [Salcobrand].[suggest].[SuggestDownloadHistory] where Date=@fecha) a
		inner join (select * from Salcobrand.reports.ReportStores where Date=@fecha)rs on rs.SKU=a.SKU and rs.Store=a.Store
		left join Salcobrand.products.FullProducts p on a.SKU=p.SKU
		where not (rs.[Max]<>a.[Max] or rs.[Min]<>a.[Min]) and IsBloqued='N'



	END

	ELSE
	BEGIN

		--/****** [Salcobrand].[suggest].[SuggestDownload] DATE - LASTSTOCK DATE  ******/
		SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N'
		
		SELECT a.SKU, p.Description, p.Division_Desc, p.LogisticAreaDesc, a.Store, a.Max [Smax Sug], a.Min [Smin Sug]
			, ls.Stock , ls.Suggest [Suggest Sist], ls.[Max] [Smax Sist], ls.[Min] [Smin Sist], ls.[isBloqued]
		FROM [Salcobrand].[suggest].[SuggestDownload] a 
		inner join Salcobrand.series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
		left join Salcobrand.products.FullProducts p on ls.SKU=p.SKU
		where not (ls.[Max]<>a.[Max] or ls.[Min]<>a.[Min]) and IsBloqued='N'
	END

END