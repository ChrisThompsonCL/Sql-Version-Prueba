﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-03-25
-- Description:	Detecta casos en los que el precio ha sido menor al de la competencia y el Msh es peor al del periodo analizado
-- =============================================
CREATE PROCEDURE [analysis].[MarketshareAlert2] 
	-- Add the parameters for the stored procedure here
	@weeks int, @totalweeks int, @compareweeks int, @x float, @sens float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @maxDateF smalldatetime
	declare @maxDateC smalldatetime

	/*** FARMA ***/
	select @maxDateF = max(date) from series.IMS
	select @maxDateC = max(date) from series.Nielsen

	-- busca evolución del MSh para los productos con alerta
	select p.Division_Desc, p.Positioning_Desc, [Date], p.SKU, p.Name, SUM(UnidadesSB)UnidadesSB, SUM(VentasSB)VentasSB
		, case when SUM(UnidadesCadenas)>0 then SUM(UnidadesSB)/SUM(UnidadesCadenas) else 0 end MshUnidades
		, case when SUM(VentasCadenas)>0 then SUM(VentasSB)/SUM(VentasCadenas) else 0 end MshVentas
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
		, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
		, case when SUM(UnidadesSB) >0 and SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasSB)/SUM(UnidadesSB)/(SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB)) end Ratio
		, sum(i)/sum(q) PrecioSBEfectivoData
	from series.IMS i
	inner join products.Products p on p.SKU=i.SKU
	LEFT JOIN (select DATENAME(WEEK,DATE)Semana, sku, sum(INcome)i, Sum(Quantity)q 
					from SERIES.SALES where DATE >= DATEADD(week, -@totalweeks+1, @maxDateF)
					GROUP BY sku, DATENAME(WEEK,DATE)
					) v on i.sku=v.sku and DATENAME(WEEK,i.date)=v.Semana
	where [Date] >= DATEADD(week, -@totalweeks+1, @maxDateF)
	AND i.SKU IN (
		--Casos con MshUnidades mas bajo
		select n.SKU
		from
		(
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesViejo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasViejo
			from series.IMS
			where date BETWEEN DATEADD(week, -@totalweeks+1, @maxDateF) AND DATEADD(week, -@totalweeks+@compareweeks, @maxDateF)
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) v
		inner join (
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesNuevo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasNuevo
			from series.IMS
			where date >= DATEADD(week, -@compareweeks+1, @maxDateF) 
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) n 
		on v.SKU=n.SKU
		where v.MshUnidadesViejo > n.MshUnidadesNuevo + @sens and n.MshUnidadesNuevo > 0
		and v.SKU in (
			--reconocer productos en los que el @x% de las últimas @weeks semanas ha sido el más barato
			SELECT SKU FROM (
				select SKU, [Date] 
					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
					, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
				from series.IMS
				where [Date] >= DATEADD(week, -@weeks+1, @maxDateF)
				GROUP BY SKU, [Date]
				HAVING SUM(UnidadesCadenas) >0
			) A
			GROUP BY SKU
			HAVING SUM(CASE WHEN PrecioSB<PrecioComp and PrecioSB>0 and PrecioComp>0 THEN 1 ELSE 0 END) > floor(@weeks*@x)
		)
	)
		and division=1
	GROUP BY p.SKU, p.Name, p.Division_Desc, p.Positioning_Desc, [Date]
	HAVING SUM(UnidadesCadenas) >0

	UNION ALL
	/*** CM ***/
	

	-- busca evolución del MSh para los productos con alerta
	select p.Division_Desc, p.Class_Desc, [Date],  p.SKU, p.Name, SUM(UnidadesSB)UnidadesSB, SUM(VentasSB)VentasSB
		, case when SUM(UnidadesCadenas)>0 then SUM(UnidadesSB)/SUM(UnidadesCadenas) else 0 end MshUnidades
		, case when SUM(VentasCadenas)>0 then SUM(VentasSB)/SUM(VentasCadenas) else 0 end MshVentas
		, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
		, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
		, case when SUM(UnidadesSB) >0 and SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasSB)/SUM(UnidadesSB)/(SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB)) end Ratio
		, sum(i)/sum(q) PrecioSBEfectivoData
	from series.Nielsen n
	LEFT JOIN (select DATENAME(WEEK,DATE)Semana, sku, sum(INcome)i, Sum(Quantity)q 
					from SERIES.SALES where DATE >= DATEADD(week, -@totalweeks+1, @maxDateC)
					GROUP BY sku, DATENAME(WEEK,DATE)
					) v on n.sku=v.sku and DATENAME(WEEK,n.date)=v.Semana
	inner join products.Products p on p.SKU=n.SKU
	where [Date] >= DATEADD(week, -@totalweeks+1, @maxDateC)
	AND n.SKU IN (
		--Casos con MshUnidades mas bajo
		select n.SKU
		from
		(
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesViejo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasViejo
			from series.Nielsen
			where date BETWEEN DATEADD(week, -@totalweeks+1, @maxDateC) AND DATEADD(week, -@totalweeks+@compareweeks, @maxDateC)
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) v
		inner join (
			select SKU 
				, SUM(UnidadesSB)/SUM(UnidadesCadenas) MshUnidadesNuevo
				, SUM(VentasSB)/SUM(VentasCadenas) MshVentasNuevo
			from series.Nielsen
			where date >= DATEADD(week, -@compareweeks+1, @maxDateC) 
				and	UnidadesCadenas>0 and VentasCadenas>0
			group by SKU
		) n 
		on v.SKU=n.SKU
		where v.MshUnidadesViejo > n.MshUnidadesNuevo + @sens and n.MshUnidadesNuevo > 0
		and v.SKU in (
			--reconocer productos en los que el @x% de las últimas @weeks semanas ha sido el más barato
			SELECT SKU FROM (
				select SKU, [Date] 
					, case when SUM(UnidadesSB) >0 then SUM(VentasSB)/SUM(UnidadesSB) else 0 end PrecioSB
					, case when SUM(UnidadesCadenas-UnidadesSB) > 0 then SUM(VentasCadenas-VentasSB)/SUM(UnidadesCadenas-UnidadesSB) else 0 end PrecioComp
				from series.Nielsen
				where [Date] >= DATEADD(week, -@weeks+1, @maxDateC)
				GROUP BY SKU, [Date]
				HAVING SUM(UnidadesCadenas) >0
			) A
			GROUP BY SKU
			HAVING SUM(CASE WHEN PrecioSB<PrecioComp and PrecioSB>0 and PrecioComp>0 THEN 1 ELSE 0 END) > floor(@weeks*@x)
		)
	)
		and division=2
	GROUP BY p.SKU, p.Name, p.Division_Desc, p.Class_Desc, [Date]
	HAVING SUM(UnidadesCadenas) >0


END