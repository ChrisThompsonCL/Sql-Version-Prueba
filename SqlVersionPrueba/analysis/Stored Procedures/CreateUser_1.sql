﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-07-23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [analysis].[CreateUser] 
	-- Add the parameters for the stored procedure here
	@username nvarchar(50) ,
	@roleId int ,
	@isCustom bit 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @roleAux int = 0
	declare @idUser table (id int) 

	if(@isCustom=1)
	begin
		set @roleAux=@roleId
		set @roleId=@roleId
	end

	insert into users.PricingUser (UserName, [Password], Name, PricingRoleId)
	output INSERTED.Id into @idUser
	select Username, Clave, Nombre, @roleId
	from IntraUsers.dbo.Usuario
	where Username = @username

	if(@isCustom=1)
	begin
		insert into users.CustomUser (PricingUserId, CustomRoleId)
		select Id , @roleAux
		from @idUser
	end 
END
