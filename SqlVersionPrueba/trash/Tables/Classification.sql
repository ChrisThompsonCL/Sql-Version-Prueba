﻿CREATE TABLE [trash].[Classification] (
    [Id]                   INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CategoryId]           INT        NOT NULL,
    [ClusterId]            INT        NOT NULL,
    [ClassificationTypeId] INT        NOT NULL,
    [Threshold]            FLOAT (53) NULL,
    CONSTRAINT [PK_Classification] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Classification_ClassificationType] FOREIGN KEY ([ClassificationTypeId]) REFERENCES [class].[ClassificationType] ([Id])
);

