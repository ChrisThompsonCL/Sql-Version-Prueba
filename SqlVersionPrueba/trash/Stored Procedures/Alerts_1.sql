﻿CREATE PROCEDURE [trash].[Alerts] 
	@param varchar(10) = null
AS
BEGIN
------------------------------------
	-- Alerta Cozacionesaaaaaa
------------------------------------
select 'Reporte de Cotizaciones'
declare @tolerance float = 0.3
declare @oldDate smalldatetime = dateadd(day,-120, getdate())

	select cast((select getdate()) as date) Date,
	Estado, [Filtro de los casos2] Alerta, [Filtro de los casos3] [Sub-Alerta],
	Division, Categoría, LastSale, SKU, Description, MotivoPrecios, MotivoInventario, MotivoOperativo, [Proveedor no se cotiza], 
	CompetidoresCotizados, [Fasa Cotizado+R1] FasaCotizado, [CV Cotizado] [CVCotizado], [Simi Cotizado] [SimiCotizado]
	,Fasa,CV, Simi
	, UpdateFasa, UpdateCV, UpdateSimi
	, FasaAntes, CVAntes, SimiAntes
	, UpdateFasaAntes, UpdateCVAntes, UpdateSimiAntes
	, TieneVentaUltimos2Meses, Price
	--, Alert1, Alert2, Alert3, Alert4
	from 
	
	(Select Division, Categoría, LastSale, sku, Name Description, Motive MotivoPrecios, MotivoMaestro MotivoInventario
	, Motive2 MotivoPrecios2, MotivoMaestro2 MotivoInventario2,MotivoOperativo, Proveedor, [ProveedorNoSeCotiza] [Proveedor no se cotiza]
	, Fasa, CV, Simi
	, CompetidoresCotizados, [Fasa Cotizado] [Fasa Cotizado+R1], [CV Cotizado], [Simi Cotizado], CotizacionesIguales2 Igual2
	, CotizacionesIguales3 Igual3, UpdateFasa, UpdateCV, UpdateSimi, TieneVentaUltimos2Meses, Price, Alert1, Alert1F
	, Alert2F, Alert2, Alert3, Alert3F, Alert4, Alert4F, AlertF5, FasaAntes, CVAntes, SimiAntes, UpdateFasaAntes
	, UpdateCVAntes,UpdateSimiAntes
	, CASE WHEN AlertF5=1 then 'SinCotización'
		WHEN Alert3F=1 then 'CotizaciónAntigua' 
		WHEN Alert4F=1 then 'CotizaciónAnteriorConCambioBrusco'
		WHEN Alert1F=1 then 'CotizaciónAlejadaDeSB'
		WHEN Alert2F=1 then 'CotizacionesAlejadasEntreSí' else 'Ok' END 'Filtro de los casos2'
	, CASE WHEN ProveedorNoSeCotiza=1 then 'ProveedorNoSeCotiza' else case when (CASE WHEN AlertF5=1 then 'SinCotización'
		WHEN Alert3F=1 then 'CotizaciónAntigua' 
		WHEN Alert4F=1 then 'CotizaciónAnteriorConCambioBrusco'
		WHEN Alert1F=1 then 'CotizaciónAlejadaDeSB'
		WHEN Alert2F=1 then 'CotizacionesAlejadasEntreSí' else 'Ok' END)<>'Ok' then 'Cotizar' else 'CotizaciónOk' end end Estado
	, CASE WHEN AlertF5=1 then 'SinCotización'
		WHEN Alert3F=1 then Alert3 
		WHEN Alert4F=1 then Alert4
		WHEN Alert1F=1 then Alert1
		WHEN Alert2F=1 then Alert2 else 'Ok' END 'Filtro de los casos3'
	, CASE WHEN AlertF5=1 then 'SinCotización'
		WHEN Alert3F=1 then Alert3 
		WHEN Alert4F=1 then Alert4
		WHEN Alert1F=1 then Alert1
		WHEN Alert2F=1 then Alert2 else 'Ok' END 'Filtro de los casos'
	from (SELECT fp.Division_Desc Division, fp.CategoryDesc Categoría, fp.LastSale, d.sku, fp.Name
	, s.Motive,fp.motive MotivoMaestro
	, s.Motive Motive2,fp.motive MotivoMaestro2
	, case when s.motive='CGE' then 1 else OperativeMotive.isOperative end MotivoOperativo
	, fp.ManufacturerDesc Proveedor
	, case when manufacturerID in (10,67,94,147,208,210,235,274,330,410,411,435,462,4855,601,605,707,715,721,809,810,900,901,903) then 1 else 0 end ProveedorNoSeCotiza
	,d.FASA, d.CV, d.SIMI
	, SUM(CASE WHEN d.Fasa is null then 0 else 1 end+ CASE WHEN d.cv is null then 0 else 1 end+CASE WHEN d.Simi is null then 0 else 1 end) CompetidoresCotizados
	, case when d.fasa is not null then 1 else 0 end 'Fasa Cotizado'
	, case when d.CV is not null then 1 else 0 end 'CV Cotizado'
	, case when d.Simi is not null then 1 else 0 end 'Simi Cotizado'
	, case when ((d.fasa=d.CV and (d.simi is null)) or (d.Fasa=d.simi and d.CV is null) or (d.CV=d.Simi and d.Fasa is null) ) then 1 else 0 end CotizacionesIguales2
	, case when d.fasa=d.CV and d.fasa=d.simi and (d.simi is not null) then 1 else 0 end CotizacionesIguales3
	, cast(UpdateFasa as Date)UpdateFasa, cast(UpdateCV as date)UpdateCV, cast(UpdateSimi as date) UpdateSimi
	, case when LastSale >= DATEADD(MONTH,-2, getdate()) then 1 else 0 end TieneVentaUltimos2Meses
	, s.Price
	,CASE WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance) 
				AND (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
				AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
					THEN 'CV-Fasa-Simi'
		WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance) 
				AND (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
					THEN 'Fasa-CV'
		WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance) 
				AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
					THEN 'Fasa-Simi'
		WHEN (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance) 
				AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
					THEN 'CV-Simi'
		WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-d.Fasa))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
            THEN 'Fasa'
        WHEN (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
            THEN 'CV'
		WHEN (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
            THEN 'Simi'
        ELSE 'Competidores OK' END Alert1
	, case when s.Price > 0 AND (
    (d.Fasa > 0 AND convert(float,ABS(s.Price-d.Fasa))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)
    OR (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance) 
	OR (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/(case when convert(float,s.Price)<=0 then NULL ELSE convert(float,s.Price) end) >= @tolerance)) then 1 else 0 end Alert1F
	, case when (CASE WHEN d.CV > 0 AND d.Fasa > 0 THEN convert(float,ABS(d.CV-d.Fasa))/(case when convert(float,d.CV)<=0 then NULL ELSE convert(float,d.CV) end) ELSE 0 END) >= @tolerance
    OR (CASE WHEN d.CV > 0 AND d.Fasa > 0 THEN convert(float,ABS(d.Fasa-d.CV))/(case when convert(float,d.Fasa)<=0 then NULL ELSE convert(float,d.Fasa) end) ELSE 0 END) >= @tolerance 
	OR (CASE WHEN d.CV > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Simi-d.CV))/(case when convert(float,d.Simi)<=0 then NULL ELSE convert(float,d.Simi) end) ELSE 0 END) >= @tolerance 
	OR (CASE WHEN d.Fasa > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Simi-d.Fasa))/(case when convert(float,d.Simi)<=0 then NULL ELSE convert(float,d.Simi) end) ELSE 0 END) >= @tolerance
	OR (CASE WHEN d.CV > 0 AND d.Simi > 0 THEN convert(float,ABS(d.CV-d.Simi))/(case when convert(float,d.CV)<=0 then NULL ELSE convert(float,d.CV) end) ELSE 0 END) >= @tolerance 
	OR (CASE WHEN d.Fasa > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Fasa-d.Simi))/(case when convert(float,d.Fasa)<=0 then NULL ELSE convert(float,d.Fasa) end) ELSE 0 END) >= @tolerance then 1 else 0 end Alert2F
	, CASE WHEN (d.CV > 0 AND d.Fasa > 0 and convert(float,ABS(d.CV-d.Fasa))/(case when convert(float,d.CV)<=0 then NULL ELSE convert(float,d.CV) end) >= @tolerance)
				THEN 'Fasa de CV'
			WHEN (d.CV > 0 AND d.Fasa > 0 AND convert(float,ABS(d.Fasa-d.CV))/(case when convert(float,d.Fasa)<=0 then NULL ELSE convert(float,d.Fasa) end) >= @tolerance) 
				THEN 'CV de Fasa'
			WHEN (d.CV > 0 AND d.Simi > 0 AND convert(float,ABS(d.Simi-d.CV))/(case when convert(float,d.Simi)<=0 then NULL ELSE convert(float,d.Simi) end) >= @tolerance) 
				THEN 'CV de Simi'
			WHEN (d.Fasa > 0 AND d.Simi > 0 AND convert(float,ABS(d.Simi-d.Fasa))/(case when convert(float,d.Simi)<=0 then NULL ELSE convert(float,d.Simi) end) >= @tolerance) 
				THEN 'Fasa de Simi'
			WHEN (d.CV > 0 AND d.Simi > 0 AND convert(float,ABS(d.CV-d.Simi))/(case when convert(float,d.CV)<=0 then NULL ELSE convert(float,d.CV) end)>= @tolerance)
				THEN 'Simi de CV'
			WHEN (d.Fasa > 0 AND d.Simi > 0 AND convert(float,ABS(d.Fasa-d.Simi))/(case when convert(float,d.Fasa)<=0 then NULL ELSE convert(float,d.Fasa) end) >= @tolerance)
				THEN 'Simi de Fasa' else 'Competidores Ok' end Alert2
	, CASE
        WHEN UpdateCV <= @oldDate AND UpdateFasa <= @oldDate AND UpdateSimi <= @oldDate
            THEN 'CV-Fasa-Simi'
		WHEN UpdateCV <= @oldDate AND UpdateFasa <= @oldDate
            THEN 'CV-Fasa'
		WHEN UpdateCV <= @oldDate AND UpdateFasa <= @oldDate
            THEN 'CV-Simi'
		WHEN UpdateCV <= @oldDate AND UpdateFasa <= @oldDate
            THEN 'Fasa-Simi'
		WHEN UpdateSimi <= @oldDate
			THEN 'Simi'
        WHEN UpdateCV <= @oldDate
            THEN 'CV'
        WHEN UpdateFasa <= @oldDate
            THEN 'Fasa'
        ELSE 'CotizaciónVigente' End Alert3
	, case when UpdateCV <= @oldDate or UpdateFasa <= @oldDate or UpdateSimi <= @oldDate then 1 else 0 end Alert3F
	, CASE
        WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa))/(case when convert(float,afasa.Fasa)<=0 then NULL ELSE convert(float,afasa.Fasa) end) >= @tolerance
			AND CONVERT(float,ABS(d.CV-Acv.CV))/(case when convert(float,acv.CV)<=0 then NULL ELSE convert(float,acv.CV) end) >= @tolerance
			AND CONVERT(float,ABS(d.Simi-Asimi.Simi))/(case when convert(float,asimi.simi)<=0 then NULL ELSE convert(float,asimi.simi) end) >= @tolerance
            THEN 'CV-Fasa-Simi'
		WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa))/(case when convert(float,afasa.fasa)<=0 then NULL ELSE convert(float,afasa.fasa) end) >= @tolerance
			AND CONVERT(float,ABS(d.CV-Acv.CV))/(case when convert(float,acv.CV)<=0 then NULL ELSE convert(float,acv.CV) end) >= @tolerance
            THEN 'Fasa-CV'
		WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa))/(case when convert(float,afasa.fasa)<=0 then NULL ELSE convert(float,afasa.fasa) end) >= @tolerance
			AND CONVERT(float,ABS(d.Simi-Asimi.Simi))/(case when convert(float,asimi.simi)<=0 then NULL ELSE convert(float,asimi.simi) end) >= @tolerance
            THEN 'Fasa-Simi'
		WHEN CONVERT(float,ABS(d.CV-Acv.CV))/(case when convert(float,acv.cv)<=0 then NULL ELSE convert(float,acv.cv) end) >= @tolerance
			AND CONVERT(float,ABS(d.Simi-Asimi.Simi))/(case when convert(float,asimi.simi)<=0 then NULL ELSE convert(float,asimi.simi) end) >= @tolerance
			THEN 'CV-Simi'
		WHEN CONVERT(float,ABS(d.CV-acv.CV))/(case when convert(float,acv.cv)<=0 then NULL ELSE convert(float,acv.cv) end) >= @tolerance
            THEN 'CV'
		WHEN CONVERT(float,ABS(d.Fasa-Afasa.Fasa))/(case when convert(float,afasa.fasa)<=0 then NULL ELSE convert(float,afasa.fasa) end) >= @tolerance
            THEN 'Fasa'
		WHEN CONVERT(float,ABS(d.Simi-ASimi.Simi))/(case when convert(float,asimi.simi)<=0 then NULL ELSE convert(float,asimi.simi) end) >= @tolerance
            THEN 'Simi'
        ELSE 'Sin Cambio Brusco' end Alert4
	, case when CONVERT(float,ABS(d.Fasa-afasa.Fasa))/(case when convert(float,afasa.fasa)<=0 then NULL ELSE convert(float,afasa.fasa) end) >= @tolerance
    or CONVERT(float,ABS(d.CV-acv.CV))/(case when convert(float,acv.cv)<=0 then NULL ELSE convert(float,acv.cv) end) >= @tolerance
	or CONVERT(float,ABS(d.Simi-aSimi.Simi))/(case when convert(float,asimi.simi)<=0 then NULL ELSE convert(float,asimi.simi) end) >= @tolerance then 1 else 0 end Alert4F
	, case when SUM(CASE WHEN d.Fasa is null then 0 else 1 end+ CASE WHEN d.cv is null then 0 else 1 end+CASE WHEN d.Simi is null then 0 else 1 end)<=0 then 1 else 0 end AlertF5
	, afasa.fasa FasaAntes, acv.cv CVAntes, asimi.simi SimiAntes
	, afasa.Date UpdateFasaAntes, acv.Date UpdateCVAntes, asimi.Date UpdateSimiAntes
	FROM (select distinct dgp.sku,Comp.Simi, comp.Date UpdateSimi, Comp2.Fasa, Comp2.Date UpdateFasa, Comp3.CV, comp3.Date UpdateCV
				FROM products.DemandGroupProduct dgp
				INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
				INNER JOIN [class].ProductClassificationFinal pcf on pcf.SKU=DGP.SKU
				INNER JOIN [class].ClassificationType ct on ct.Id=pcf.ClassificationTypeId
				INNER JOIN pricing.LastPriceAndCost pc ON pc.SKU = dgp.SKU
				INNER JOIN products.products p on dgp.sku=p.sku
				LEFT JOIN (select * from 
					(select sku, date, Simi,'Simi' Comp, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
							from (select * from series.CompetitorsDailyPrice where (UpdatedSimi=1) --and date>=dateadd(day,-90, getdate())
									)a
					)b where row=1
				) comp ON comp.SKU = dgp.SKU

				LEFT JOIN (select * from 
					(select sku, date,Fasa,'Fasa' Comp, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
							from (select * from series.CompetitorsDailyPrice where (UpdatedFasa=1) --and date>=dateadd(day,-90, getdate())
									)a
					)b where row=1
				) comp2 ON comp2.SKU = dgp.SKU

				LEFT JOIN (select * from 
					(select sku, date,CV,'CV' Comp, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
							from (select * from series.CompetitorsDailyPrice where (UpdatedCV=1) --and date>=dateadd(day,-90, getdate())
									)a
					)b where row=1
				) comp3 ON comp3.SKU = dgp.SKU
	
	WHERE dgn.ParentId in (2,3) 			
	) d
	INNER JOIN (select sku, Price, Motive from pricing.lastpriceandcost) s on d.sku=s.sku
	INNER JOIN products.Products fp on d.sku=fp.sku
	LEFT JOIN (select SKU, CV, date from (select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
				from (select * from series.CompetitorsDailyPrice where (UpdatedCV=1)
				)a)b where row=1) Acv on acv.sku=d.SKU
	LEFT JOIN (select SKU, Fasa, Date from (select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
				from (select * from series.CompetitorsDailyPrice where (UpdatedFasa=1)
				)a)b where row=2) Afasa on afasa.sku=d.sku
	LEFT JOIN (select SKU, Simi, date from (select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
				from (select * from series.CompetitorsDailyPrice where (UpdatedSimi=1)
				)a)b where row=2) Asimi on asimi.sku=d.SKU
	LEFT JOIN (select * from pricing.ProductMotive) OperativeMotive on cast(OperativeMotive.MotiveId as varchar(50))=cast(s.Motive as varchar(50))
	
	GROUP BY  fp.Division_Desc, fp.CategoryDesc, fp.LastSale, d.sku, fp.Name ,d.Fasa, d.cv, UpdateFasa, UpdateCV, d.simi
	, UpdateSimi, Price, afasa.Fasa, acv.CV, asimi.Simi, afasa.Date, acv.Date, asimi.Date, OperativeMotive.IsOperative, s.Motive
	, fp.Motive,ManufacturerDesc, fp.name,manufacturerID)a)a



------------------------------------
	-- Alerta Cotas de Posicionamiento
------------------------------------
select 'Reporte de Cotas de Posicionamiento'
select cast((select getdate()) as date) Date,
 case when Alert1=0 and Alert2=0 and Alert3=0 and Alert4=0 then 'Posicionamientos Ok' else 'Corregir Posicionamiento' end Estado
,case when Alert1=1 then 'Categoria-ClaseSinCota' else 
	case when Alert1=0 and Alert2=1 then 'Minimo=Maximo' else
		case when Alert1=0 and Alert2=0 and Alert3=1 then 'Categorías con Cotas Infactibles por SKU' else
			case when Alert1=0 and Alert2=0 and Alert3=0 and Alert4=1 then 'Categorías con espacio entre cotas menor a 1%' else
				'Posicionamientos Ok' end end end end Alerta
--, motive, isoperative
, DivisionName División, CategoryName Categoría, ClassName Clasificación, Minimo, Maximo, TotalSKUs, EspacioCotaProm
, [espacioCotaProm%],EspacioCota, PrecioSBProm,PVPSB,PVPFASA,PVPCV,PVPSimi, MarcaPropiaFueraLab, Infactibles NSKUsInfactibles 
from (select a.divisionName, a.CategoryName, a.ClassName, a.Minimo, a.Maximo, a.TotalSKUs, a.EspacioCotaProm, a.[EspacioCotaProm%]
,MaxBound-MinBound [EspacioCota]
, a.PrecioSBProm, a.PVPSB, a.PVPFASA, a.PVPCV, a.PVPSimi, a.MarcaPropiaFueraLab
, Infactibles
--ALERTA 1: No Tiene Cota La Categoría
, case when Minimo IS NULL THEN 1 ELSE 0 END Alert1
--ALERTA 2: Mínimo=Máximo
, case when Minimo=Maximo THEN 1 ELSE 0 END Alert2
--ALERTA 3: SKUsconEspaciosInfactibles>1
, case when Infactibles>=1 then 1 else 0 end  Alert3
--ALERTA 4: EspacioEntreCotasPromInlcuyendoCompetidoresBAJO
, case when [EspacioCotaProm%]<=cast(0.01 as float) then 1 else 0 end Alert4
--, motive, isoperative
from (
SELECT DivisionName--, CategoryId
, CategoryName--, ClassId
, ClassName
, case when MinRef is null then NULL else concat(MinRef,'-', MinBound*100,'%') end Minimo
, case when MaxRef is null then NULL else concat(MaxRef,'-',MaxBound*100,'%') end Maximo
, MinBound, MaxBound
 , COUNT(*) TotalSKUs
 --Es un promedio porque depende el SKU de sus cotizadores, CotaMax y CotaMin es independiente por SKU
 , ROUND(cast (SUM(CotaMax-CotaMin)*1.0/(case when cast(COUNT(*) as float)=0 then NULL else cast(COUNT(*) as float) end) as float), 0) EspacioCotaProm
 , ROUND(cast (SUM(CotaMax-CotaMin)*1.0/(case when cast(COUNT(*) as float)=0 then NULL else cast(COUNT(*) as float) end) as float)/(case when (SUM(PriceSB)/(case when cast(COUNT(*) as float)=0 then NULL else cast(COUNT(*) as float) end))=0 then NULL else (SUM(PriceSB)/(case when cast(COUNT(*) as float)=0 then NULL else cast(COUNT(*) as float) end)) end), 2) [EspacioCotaProm%]
 , ROUND(AVG(PriceSB), 2)[PrecioSBProm]

 , ROUND(SUM(PriceSB*Quantity)/SUM(CASE WHEN PriceSB > 0 AND QUANTITY>0 THEN Quantity ELSE NULL END),2) PVPSB
 , ROUND(SUM(PriceCV*Quantity)/SUM(CASE WHEN PriceCV > 0 AND QUANTITY>0 THEN Quantity ELSE NULL END),2) PVPCV
  , ROUND(SUM(PriceFASA*Quantity)/SUM(CASE WHEN PriceFASA > 0 AND QUANTITY>0 THEN Quantity ELSE NULL END),2) PVPFASA
 , ROUND(SUM(PriceSimi*Quantity)/SUM(CASE WHEN PriceSimi > 0 AND QUANTITY>0 THEN Quantity ELSE NULL END),2) PVPSimi

 , AVG(MarcaPropiaFueraLab) MarcaPropiaFueraLab
 , SUM(case when CotaMin>CotaMax then 1 else 0 end) Infactibles
 --, motive, isoperative
FROM (
SELECT Division_Desc DivisionName, a.CategoryId, a.CategoryName, a.ClassId, ct.Name ClassName
, prmin.Name MinRef, MinBound, prmax.Name MaxRef, MaxBound, MarcaPropiaFueraLab
, a.motive
--, operativemotive.isoperative
 , a.SKU, p.Name
 , ROUND(a.Cost,2) Cost
 , ROUND(PriceSB,2) PriceSB
 , ROUND(PriceSB,2)*Quantity [PriceSB*Unidades]
 , quantity quantity
 , ROUND(PriceFasa,2) PriceFasa
 , ROUND(PriceCV,2) PriceCV
 , ROUND(PriceSimi,2) PriceSimi
 , ROUND(PriceFasa,2)*Quantity [PriceFasa*Unidades]
 , ROUND(PriceCV,2)*Quantity [PriceCV*Unidades] 
 , ROUND(PriceSimi,2) *Quantity [PriceSimi*Unidades]
 , [function].PriceEnding(a.sku, case prmin.Id when 1 then [function].[InlineMin]([function].[InlineMin](PriceFasa,PriceSimi), PriceCV)*1.0 when 2 then [function].[InlineMax]([function].[InlineMax](PriceFasa,PriceSimi), PriceCV)*1.0 end * MinBound) CotaMin
 , [function].PriceEnding(a.sku, case prmax.Id when 1 then [function].[InlineMin]([function].[InlineMin](PriceFasa,PriceSimi), PriceCV)*1.0 when 2 then [function].[InlineMax]([function].[InlineMax](PriceFasa,PriceSimi), PriceCV)*1.0 end * MaxBound) CotaMax
FROM 
(
--INSERT INTO @data( CategoryId, CategoryName, SKU, ClassId, PriceSB, PriceFasa, PriceCV, PriceSimi, Cost, MarcaPropiaFueraLab, Quantity)
SELECT dgn.Id CategoryId, dgn.Name CategoryName, dgp.SKU, pcf.ClassificationTypeId ClassId
 , pc.Price PriceSB, comp.Fasa PriceFasa, comp.CV PriceCV, comp.Simi PriceSimi, pc.Cost, pc.motive
 , CASE WHEN DGN.ParentId=800 THEN 1 ELSE 0 END MarcaPropiaFueraLab, quantity
FROM products.DemandGroupProduct dgp
INNER JOIN (SELECT * FROM products.DemandGroupNew WHERE ParentId in (2,3,800) and id not in (2,3))dgn on dgn.id = dgp.Id 
LEFT JOIN class.ProductClassificationFinal pcf on pcf.SKU = dgp.SKU 
LEFT JOIN pricing.LastPriceAndCost pc ON pc.SKU = dgp.SKU 
LEFT JOIN pricing.LastCompetitorsPrice comp ON comp.SKU = dgp.SKU
LEFT JOIN (select sku, sum(quantity) quantity from series.sales where date>= DATEADD(MONTH,-1,getdate()-2) group by sku)s on dgp.sku=s.sku

) a
INNER JOIN products.Products p on p.SKU = a.SKU
LEFT JOIN [class].ClassificationType ct on ct.Id = a.ClassId
LEFT JOIN [class].Classification c ON c.CategoryId = a.CategoryId AND c.ClassificationTypeId = a.ClassId 
LEFT JOIN [class].Positioning pos ON pos.ClassificationId = c.Id
LEFT JOIN [class].PositioningReferredDetail prdmin ON prdmin.PosReferredId=pos.MinReferredId 
LEFT JOIN [class].PositioningReference prmin ON prdmin.PosReferenceId=prmin.Id 
LEFT JOIN [class].PositioningReferredDetail prdmax ON prdmax.PosReferredId=pos.MaxReferredId 
LEFT JOIN [class].PositioningReference prmax ON prdmax.PosReferenceId=prmax.Id
--LEFT JOIN (SELECT [SKU], [Motive], [Cost], Precio, [Date] 
	--		FROM (SELECT [SKU], [Motive], [Cost], [Price] Precio, [Date], ROW_NUMBER() over (partition by sku order by [Date] desc) rn
	--		FROM [Salcobrand].aux.priceandcost p) a Where rn = 1) MOTIVE ON MOTIVE.SKU=d.SKU
	LEFT JOIN (select * from pricing.ProductMotive) OperativeMotive on cast(OperativeMotive.MotiveId as varchar(50))=cast(a.Motive as varchar(50))
--ORDER BY Division_Desc, a.CategoryId, a.CategoryName, a.ClassId, ct.Name, a.SKU, p.Name, prmin.Name, MinBound, prmax.Name, MaxBound
) a
--WHERE CotaMin is not null and CotaMax is not null
GROUP BY DivisionName, CategoryId, CategoryName, ClassId, ClassName, MinRef, MinBound, MaxRef, MaxBound--, motive, Isoperative
--, CotaMin, CotaMax
)a)a
--)A
--where case when Infactibles>=1 then 1 else 0 end=1
order by DivisionName--, CategoryId

------------------------------------------
	-- Alerta Validacion Posicionamiento
------------------------------------------
select 'Reporte de Validacion de Posicionamiento'

declare @minDate as date=dateadd(month, -1, dateadd(day,-2,getdate()))
declare @maxDate as date=dateadd(day,-2,getdate())
declare @date as date =(select Max(Date) from aux.PriceAndCost)
set @oldDate = dateadd(day,-90, getdate())

/*tabla 1*/
SELECT * 
	, case when MargenEffCotaMax < 0.095 then 'CotaMax no cumple margen mínimo' else '' end [MargenCotaMax?]
	, case [Posicionamiento?] 
		when 'Cumple' then (case [MargenEffMínimo10%?]
			when 'Cumple' then 'OK'
			when 'No cumple' then (case
				when PSB < CotaMax then 'Subir a CotaMax'
				when PSB = CotaMax then '' --CONSIDERA NEGOCIAR POSICIONAMIENTO o COSTO
			end)
		end)
		when 'No cumple' then (case
			when PSB < CotaMin then 'Subir a CotaMin'
			when PSB > CotaMax then (case 
				when MargenEffCotaMax >= 0.095 then 'Bajar a CotaMax'
				when MargenEffCotaMax < 0.095 then 'Bajar a CotaMax' --CONSIDERA NEGOCIAR POSICIONAMIENTO o COSTO
			end)
		end) 
		when 'Sin datos' then (case [MargenEffMínimo10%?]
			when 'Cumple' then  (case
				when MinBound is null and MaxBound is null then 'Sin regla de posicionamiento'
				else 'Sin información de cotizaciones'
			end)
			when 'No cumple' then 'Subir a PMínimoMargen'			
		end)
		when 'Sin cotización' then (case [MargenEffMínimo10%?]
			when 'Cumple' then  (case
				when MinBound is null and MaxBound is null then 'Sin regla de posicionamiento'
				else 'Sin información de cotizaciones'
			end)
			when 'No cumple' then 'Subir a PMínimoMargen'		
		end)
	end Acción
	, case when MargenEffCotaMax < 0.095 then 'Considerar negociación de Posicionamiento o Costo' else '' end Recomendación
	, isnull(cast(case [Posicionamiento?] 
		when 'No cumple' then (case
			when PSB > CotaMax then round(cast(QMes*(PListaPond*(1-DctoProm)-CostoPond) as float)/(CotaMax*(1-DctoProm)-CostoPond),0) --cálculo de Q
		end) 
	end as varchar(10)),'') QRecuperación
	into #tabla1
FROM (
	SELECT * 
		, case when cast([function].PriceEnding(a.SKU, CotaMax*(1-DctoProm)) as float)>0 then 
		ROUND((cast([function].PriceEnding(a.SKU, CotaMax*(1-DctoProm)) as float)-CostoPond)/cast([function].PriceEnding(a.SKU, CotaMax*(1-DctoProm)) as float),3) end MargenEffCotaMax
		, case when CotaMin is not null and CotaMax is not null then (case when PSB between CotaMin and CotaMax then 'Cumple' else 'No cumple' end) when MinBound is not null  and MinBound is not null then 'Sin cotización' else 'Sin regla definida' end [Posicionamiento?]
		, case when Millar is not null then (case when PSB <= Millar then 'Cumple' else 'No cumple' end) else 'Sin datos' end [Millar?]
		, case when PEffSB >= PMínimoMargen then 'Cumple' else 'No cumple' end [MargenEffMínimo10%?]
		, case when PEffSB > PListaPond then 'P-ef > P-lista' else '' end [PEfecivoMayor?]
		, case when PListaPond < PMargen0 then 'PLista con margen negativo' else '' end [PListaOK?]
	FROM (
		SELECT Division_Desc Division, p.CategoryDesc Categoría, a.Category  [Posicionamiento/Clase], p.ManufacturerDesc Proveedor
			, ct.Name Clasificación, MinBound, prmin.Name MinRef, MaxBound, prmax.Name MaxRef
			, a.SKU, p.Name
			, p.ProCuidado, (Case when Manufacturerid in (18,92) or p.CategoryId=1 then 1 else 0 end) Genérico
			, pp.Motive MotivoPRECIOS
			, p.Motive MotivoINVENTARIO
			, QMes
			, ROUND(PriceSB,0) PSB
			, ROUND(PListaPond,0) PListaPond
			, ROUND(CostoPond,0) CostoPond
			, ROUND(PEffSB,0) PEffSB
			, ROUND(PriceFasa,0) PFasa
			, ROUND(PriceCV,0) PCV
			, ROUND(PriceSimi,0) PSimi
			, [function].PriceEnding(a.sku, case prmin.Id when 1 then [function].[MasBarato](PriceFasa,PriceSimi,PriceCV)*1.0 when 2 then [function].[MasCostoso](PriceFasa,PriceSimi,PriceCV)*1.0 end * MinBound) CotaMin
			, [function].PriceEnding(a.sku, case prmax.Id when 1 then [function].[MasBarato](PriceFasa,PriceSimi,PriceCV)*1.0 when 2 then [function].[MasCostoso](PriceFasa,PriceSimi,PriceCV)*1.0 end * MaxBound) CotaMax
			, CEILING([function].[MasCostoso](PriceFasa,PriceSimi,PriceCV)/1000)*1000-10 Millar
			, [function].PriceEnding(a.SKU, CostoPond/0.906) PMínimoMargen
			, [function].PriceEnding(a.SKU, PListaPond*0.894) CMínimoMargen
			, [function].PriceEnding(a.SKU, CostoPond-50) PMargen0
			, [function].InlineMax(ROUND((PListaPond-PEffSB)*1.0/PListaPond,3), 0)  DctoProm
		FROM (
			SELECT dgn.Id CategoryId, dgn.Name Category, dgp.SKU, pcf.ClassificationTypeId ClassId
				, pc.Price PriceSB, comp.Fasa PriceFasa, comp.CV PriceCV, comp.Simi PriceSimi
				, case when SUM(Q) > 0 then SUM(spc.Price*Q)/SUM(Q) END PListaPond
				, case when SUM(Q) > 0 then SUM(spc.Cost*Q)/SUM(Q) END CostoPond
				, case when SUM(Q) > 0 then SUM(I)/SUM(Q) END PEffSB
				, SUM(Q) QMes
			FROM products.DemandGroupProduct dgp
			INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
			INNER JOIN class.ProductClassificationFinal pcf on pcf.SKU = dgp.SKU 
			INNER JOIN pricing.LastPriceAndCost pc ON pc.SKU = dgp.SKU 
			INNER JOIN (select * from series.PriceAndCost where [Date] between @minDate and @maxDate) spc on spc.SKU=pc.SKU
			LEFT JOIN pricing.LastCompetitorsPrice comp ON comp.SKU = dgp.SKU
			LEFT JOIN (
				select SKU, [Date], SUM(Quantity) Q, SUM(Income) I  
				from Salcobrand.series.DailySales
				where [Date] between @minDate and @maxDate
				group by SKU, [Date]
			) s on s.SKU=dgp.SKU and spc.[Date]=s.[Date]
			WHERE dgn.ParentId in (2,3)
			GROUP BY dgn.Id, dgn.Name, dgp.SKU, pcf.ClassificationTypeId, pc.Price, comp.Fasa, comp.CV, comp.Simi, pc.Cost
		) a
		INNER JOIN products.Products p on p.SKU = a.SKU
		LEFT JOIN (select * from aux.PriceAndCost where date = @date) pp on pp.SKU=a.SKU
		LEFT JOIN [class].ClassificationType ct on ct.Id = a.ClassId
		LEFT JOIN [class].Classification c ON c.CategoryId = a.CategoryId AND c.ClassificationTypeId = a.ClassId 
		LEFT JOIN [class].Positioning pos ON pos.ClassificationId = c.Id
		LEFT JOIN [class].PositioningReferredDetail prdmin ON prdmin.PosReferredId=pos.MinReferredId 
		LEFT JOIN [class].PositioningReference prmin ON prdmin.PosReferenceId=prmin.Id 
		LEFT JOIN [class].PositioningReferredDetail prdmax ON prdmax.PosReferredId=pos.MaxReferredId 
		LEFT JOIN [class].PositioningReference prmax ON prdmax.PosReferenceId=prmax.Id 
		WHERE PListaPond > 0 --and cost>0
	) a
) a

/*tabla 2*/
select 
	Estado, [Filtro de los casos2] Alerta, [Filtro de los casos3] [Sub-Alerta],
	Division, Categoría, LastSale, SKU, Description, MotivoPrecios, MotivoInventario, MotivoOperativo, [Proveedor no se cotiza], 
	CompetidoresCotizados, [Fasa Cotizado+R1] FasaCotizado, [CV Cotizado] [CVCotizado], [Simi Cotizado] [SimiCotizado]
	,Fasa,CV, Simi
	, UpdateFasa, UpdateCV, UpdateSimi
	, FasaAntes, CVAntes, SimiAntes
	, UpdateFasaAntes, UpdateCVAntes, UpdateSimiAntes
	, TieneVentaUltimos2Meses, Price
	into #tabla2
	from (
	Select Division, Categoría, LastSale, sku, [Name] [Description], Motive MotivoPrecios, MotivoMaestro MotivoInventario
		, MotivoOperativo, Proveedor, [ProveedorNoSeCotiza] [Proveedor no se cotiza]
		, Fasa, CV, Simi
		, CompetidoresCotizados, [Fasa Cotizado] [Fasa Cotizado+R1], [CV Cotizado], [Simi Cotizado], CotizacionesIguales2 Igual2
		, CotizacionesIguales3 Igual3, UpdateFasa, UpdateCV, UpdateSimi, TieneVentaUltimos2Meses, Price, Alert1F
		, Alert2F, Alert3F, Alert4F, AlertF5, FasaAntes, CVAntes, SimiAntes, UpdateFasaAntes
		, UpdateCVAntes,UpdateSimiAntes
		, CASE WHEN AlertF5=1 then 'SinCotización'
			WHEN Alert3F=1 then 'CotizaciónAntigua' 
			WHEN Alert4F=1 then 'CotizaciónAnteriorConCambioBrusco'
			WHEN Alert1F=1 then 'CotizaciónAlejadaDeSB'
			WHEN Alert2F=1 then 'CotizacionesAlejadasEntreSí' else 'Ok' END 'Filtro de los casos2'
		, CASE WHEN ProveedorNoSeCotiza=1 then 'ProveedorNoSeCotiza' else case when (CASE WHEN AlertF5=1 then 'SinCotización'
			WHEN Alert3F=1 then 'CotizaciónAntigua' 
			WHEN Alert4F=1 then 'CotizaciónAnteriorConCambioBrusco'
			WHEN Alert1F=1 then 'CotizaciónAlejadaDeSB'
			WHEN Alert2F=1 then 'CotizacionesAlejadasEntreSí' else 'Ok' END)<>'Ok' then 'Cotizar' else 'CotizaciónOk' end end Estado
		, CASE WHEN AlertF5=1 then 'SinCotización'
			WHEN Alert3F=1 then Alert3 
			WHEN Alert4F=1 then Alert4
			WHEN Alert1F=1 then Alert1
			WHEN Alert2F=1 then Alert2 else 'Ok' END 'Filtro de los casos3'
		, CASE WHEN AlertF5=1 then 'SinCotización'
			WHEN Alert3F=1 then Alert3 
			WHEN Alert4F=1 then Alert4
			WHEN Alert1F=1 then Alert1
			WHEN Alert2F=1 then Alert2 else 'Ok' END 'Filtro de los casos'
		from (
			SELECT fp.Division_Desc Division, fp.CategoryDesc Categoría, fp.LastSale, d.sku, fp.Name
			, s.Motive,fp.motive MotivoMaestro
			, case when s.motive='CGE' then 1 else OperativeMotive.isOperative end MotivoOperativo
			, fp.ManufacturerDesc Proveedor
			, case when manufacturerID in (10,67,94,147,208,210,235,274,330,410,411,435,462,4855,601,605,707,715,721,809,810,900,901,903) then 1 else 0 end ProveedorNoSeCotiza
			,d.FASA, d.CV, d.SIMI
			, SUM(CASE WHEN d.Fasa is null then 0 else 1 end+ CASE WHEN d.cv is null then 0 else 1 end+CASE WHEN d.Simi is null then 0 else 1 end) CompetidoresCotizados
			, case when d.fasa is not null then 1 else 0 end 'Fasa Cotizado'
			, case when d.CV is not null then 1 else 0 end 'CV Cotizado'
			, case when d.Simi is not null then 1 else 0 end 'Simi Cotizado'
			, case when ((d.fasa=d.CV and (d.simi is null)) or (d.Fasa=d.simi and d.CV is null) or (d.CV=d.Simi and d.Fasa is null) ) then 1 else 0 end CotizacionesIguales2
			, case when d.fasa=d.CV and d.fasa=d.simi and (d.simi is not null) then 1 else 0 end CotizacionesIguales3
			, cast(LastUpdateFasa as Date)UpdateFasa, cast(LastUpdateCV as date)UpdateCV, cast(LastUpdateSimi as date) UpdateSimi
			, case when LastSale >= DATEADD(MONTH,-2, getdate()) then 1 else 0 end TieneVentaUltimos2Meses
			, s.Price
			,CASE WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/convert(float,s.Price) >= @tolerance) 
						AND (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/convert(float,s.Price) >= @tolerance)
						AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/convert(float,s.Price) >= @tolerance)
							THEN 'CV-Fasa-Simi'
				WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/convert(float,s.Price) >= @tolerance) 
						AND (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/convert(float,s.Price) >= @tolerance)
							THEN 'Fasa-CV'
				WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-D.Fasa))/convert(float,s.Price) >= @tolerance) 
						AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/convert(float,s.Price) >= @tolerance)
							THEN 'Fasa-Simi'
				WHEN (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/convert(float,s.Price) >= @tolerance) 
						AND (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/convert(float,s.Price) >= @tolerance)
							THEN 'CV-Simi'
				WHEN (d.Fasa > 0 AND convert(float,ABS(s.Price-d.Fasa))/convert(float,s.Price) >= @tolerance)
					THEN 'Fasa'
				WHEN (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/convert(float,s.Price) >= @tolerance)
					THEN 'CV'
				WHEN (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/convert(float,s.Price) >= @tolerance)
					THEN 'Simi'
				ELSE 'Competidores OK' END Alert1
			, case when s.Price > 0 AND (
			(d.Fasa > 0 AND convert(float,ABS(s.Price-d.Fasa))/convert(float,s.Price) >= @tolerance)
			OR (d.CV > 0 AND convert(float,ABS(s.Price-d.CV))/convert(float,s.Price) >= @tolerance) 
			OR (d.Simi > 0 AND convert(float,ABS(s.Price-d.Simi))/convert(float,s.Price) >= @tolerance)) then 1 else 0 end Alert1F
			, case when (CASE WHEN d.CV > 0 AND d.Fasa > 0 THEN convert(float,ABS(d.CV-d.Fasa))/convert(float,d.CV) ELSE 0 END) >= @tolerance
			OR (CASE WHEN d.CV > 0 AND d.Fasa > 0 THEN convert(float,ABS(d.Fasa-d.CV))/convert(float,d.Fasa) ELSE 0 END) >= @tolerance 
			OR (CASE WHEN d.CV > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Simi-d.CV))/convert(float,d.Simi) ELSE 0 END) >= @tolerance 
			OR (CASE WHEN d.Fasa > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Simi-d.Fasa))/convert(float,d.Simi) ELSE 0 END) >= @tolerance
			OR (CASE WHEN d.CV > 0 AND d.Simi > 0 THEN convert(float,ABS(d.CV-d.Simi))/convert(float,d.CV) ELSE 0 END) >= @tolerance 
			OR (CASE WHEN d.Fasa > 0 AND d.Simi > 0 THEN convert(float,ABS(d.Fasa-d.Simi))/convert(float,d.Fasa) ELSE 0 END) >= @tolerance then 1 else 0 end Alert2F
			, CASE WHEN (d.CV > 0 AND d.Fasa > 0 and convert(float,ABS(d.CV-d.Fasa))/convert(float,d.CV) >= @tolerance)
						THEN 'Fasa de CV'
					WHEN (d.CV > 0 AND d.Fasa > 0 AND convert(float,ABS(d.Fasa-d.CV))/convert(float,d.Fasa) >= @tolerance) 
						THEN 'CV de Fasa'
					WHEN (d.CV > 0 AND d.Simi > 0 AND convert(float,ABS(d.Simi-d.CV))/convert(float,d.Simi) >= @tolerance) 
						THEN 'CV de Simi'
					WHEN (d.Fasa > 0 AND d.Simi > 0 AND convert(float,ABS(d.Simi-d.Fasa))/convert(float,d.Simi) >= @tolerance) 
						THEN 'Fasa de Simi'
					WHEN (d.CV > 0 AND d.Simi > 0 AND convert(float,ABS(d.CV-d.Simi))/convert(float,d.CV)>= @tolerance)
						THEN 'Simi de CV'
					WHEN (d.Fasa > 0 AND d.Simi > 0 AND convert(float,ABS(d.Fasa-d.Simi))/convert(float,d.Fasa) >= @tolerance)
						THEN 'Simi de Fasa' else 'Competidores Ok' end Alert2
			, CASE
				WHEN LastUpdateCV <= @oldDate AND LastUpdateFasa <= @oldDate AND LastUpdateSimi <= @oldDate
					THEN 'CV-Fasa-Simi'
				WHEN LastUpdateCV <= @oldDate AND LastUpdateFasa <= @oldDate
					THEN 'CV-Fasa'
				WHEN LastUpdateCV <= @oldDate AND LastUpdateFasa <= @oldDate
					THEN 'CV-Simi'
				WHEN LastUpdateCV <= @oldDate AND LastUpdateFasa <= @oldDate
					THEN 'Fasa-Simi'
				WHEN LastUpdateSimi <= @oldDate
					THEN 'Simi'
				WHEN LastUpdateCV <= @oldDate
					THEN 'CV'
				WHEN LastUpdateSimi <= @oldDate
					THEN 'Fasa'
				ELSE 'CotizaciónVigente' End Alert3
			, case when LastUpdateCV <= @oldDate or LastUpdateFasa <= @oldDate or LastUpdateSimi <= @oldDate then 1 else 0 end Alert3F
			, CASE
				WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa)) >= @tolerance*CONVERT(float,afasa.Fasa)
					AND CONVERT(float,ABS(d.CV-Acv.CV)) >= @tolerance*CONVERT(float,Acv.CV)
					AND CONVERT(float,ABS(d.Simi-Asimi.Simi)) >= @tolerance*CONVERT(float,Asimi.Simi)
					THEN 'CV-Fasa-Simi'
				WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa)) >= @tolerance*CONVERT(float,afasa.Fasa)
					AND CONVERT(float,ABS(d.CV-Acv.CV)) >= @tolerance*CONVERT(float,Acv.CV)
					THEN 'Fasa-CV'
				WHEN CONVERT(float,ABS(d.Fasa-afasa.Fasa)) >= @tolerance*CONVERT(float,afasa.Fasa)
					AND CONVERT(float,ABS(d.Simi-Asimi.Simi)) >= @tolerance*CONVERT(float,Asimi.Simi)
					THEN 'Fasa-Simi'
				WHEN CONVERT(float,ABS(d.CV-Acv.CV)) >= @tolerance*CONVERT(float,Acv.CV) 
					AND CONVERT(float,ABS(d.Simi-Asimi.Simi)) >= @tolerance*CONVERT(float,Asimi.Simi)
					THEN 'CV-Simi'
				WHEN CONVERT(float,ABS(d.CV-acv.CV)) >= @tolerance*CONVERT(float,acv.CV)
					THEN 'CV'
				WHEN CONVERT(float,ABS(d.Fasa-Afasa.Fasa)) >= @tolerance*CONVERT(float,Afasa.Fasa)
					THEN 'Fasa'
				WHEN CONVERT(float,ABS(d.Simi-ASimi.Simi)) >= @tolerance*CONVERT(float,Asimi.Simi)
					THEN 'Simi'
				ELSE 'Sin Cambio Brusco' end Alert4
			, case when (afasa.Fasa > 0 and CONVERT(float,ABS(d.Fasa-afasa.Fasa))>= @tolerance*CONVERT(float,afasa.Fasa) )
				or (acv.CV > 0 and CONVERT(float,ABS(d.CV-acv.CV)) >= @tolerance*CONVERT(float,acv.CV))
				or (aSimi.Simi > 0 and CONVERT(float,ABS(d.Simi-aSimi.Simi)) >= @tolerance*CONVERT(float,aSimi.Simi) )
				then 1 else 0 end Alert4F
			, case when COALESCE(d.Fasa, d.cv, d.Simi) is null then 1 else 0 end AlertF5
			, afasa.fasa FasaAntes, acv.cv CVAntes, asimi.simi SimiAntes
			, afasa.[Date] UpdateFasaAntes, acv.[Date] UpdateCVAntes, asimi.[Date] UpdateSimiAntes
	FROM products.products fp
	LEFT JOIN pricing.LastCompetitorsPrice d on d.SKU=fp.SKU
	LEFT JOIN pricing.lastpriceandcost s on d.sku=s.sku
	LEFT JOIN pricing.ProductMotive OperativeMotive on OperativeMotive.MotiveId=s.Motive
	LEFT JOIN (
		select SKU, CV, date 
		from (
			select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
			from series.CompetitorsDailyPrice where UpdatedCV=1
		)b where row=2
	) Acv on acv.sku=d.SKU
	LEFT JOIN (
		select SKU, Fasa, Date 
		from (
			select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
			from series.CompetitorsDailyPrice where UpdatedFasa=1
		)b where row=2
	) Afasa on afasa.sku=d.sku
	LEFT JOIN (
		select SKU, Simi, date 
		from (
			select *, ROW_NUMBER() OVER(PARTITION BY sku ORDER BY date desc) as row 
			from series.CompetitorsDailyPrice where UpdatedSimi=1
		)b where row=2
	) Asimi on asimi.sku=d.SKU
	GROUP BY  fp.Division_Desc, fp.CategoryDesc, fp.LastSale, d.sku, fp.Name ,d.Fasa, d.cv, LastUpdateFasa, LastUpdateCV, d.simi
	, LastUpdateSimi, Price, afasa.Fasa, acv.CV, asimi.Simi, afasa.Date, acv.Date, asimi.Date, OperativeMotive.IsOperative, s.Motive
	, fp.Motive,ManufacturerDesc, fp.name,manufacturerID)a)a


SELECT cast(GETDATE() as date) Date, A.*, B.Estado,b.Alerta AlertaCotización, B.[Sub-Alerta] SubAlerta 
FROM #tabla1 A 
LEFT JOIN #tabla2 B ON A.SKU=B.SKU

drop table #tabla1
drop table #tabla2


END