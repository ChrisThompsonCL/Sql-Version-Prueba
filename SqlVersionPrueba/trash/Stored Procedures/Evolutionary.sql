﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE trash.[Evolutionary]

AS
BEGIN

--FARMA------

--Seleccionamos el periodo a mirar evolución
declare @Hasta as date=DATEADD(DAY,+0-1,dbo.GetMonday(getdate()))
declare @Desde as date=dbo.getmonday(DATEADD(MONTH,-13,@Hasta))

--3MESES 2 MINUTOS 78MIL filas
--6MESES 3 MINUTOS 170MIL FILAS
--1AÑO 7 MINUTOS 325MIL FILAS

select --top 1 
Descripción,SKU,Descripción,CAST(Date AS DATE) Date,División, Clasificación, Motive, MotivoOperativo, CategoryDesc Categoría, Posicionamiento
, CategoryManager_Desc Category
, ManufacturerDesc Fabricante
, Cost, Ingresos, Unidades
, case when VentasIMS_SB is null then 0 else VentasIMS_SB end VentasIMS_SB
, case when VentasIMS_CADENA is null then 0 else VentasIMS_CADENA end VentasIMS_CADENA
, case when PriceSB*Unidades is null then 0 else PriceSB*Unidades end VentasSB
, case when PriceFasa*Unidades is null then 0 else PriceFasa*Unidades end VentasFasa
, case when PriceCV*Unidades is null then 0 else PriceCV*Unidades end VentasCV
, case when PriceSimi*Unidades is null then 0 else PriceSimi*Unidades end VentasSimi
, case when sPrice*Unidades is null then 0 else sPrice*Unidades end VentasEfectivasSB
, Cost*Unidades CostoUnidades
, PriceSB, sPrice, PriceFasa, ÚltCotizaciónFasa, PriceSimi, ÚltCotizaciónSimi, PriceCV, ÚltCotizaciónCV
, QuiebreLocales, Locales
from (select dgp.SKU, dgn.Id Categoría, dgn.Name NombreCategoría, ct.Name Clasificación, IC.CategoryManager_Desc
	, pc1.Motive, pc.date, OperativeMotive.IsOperative MotivoOperativo
	, p.Division_Desc División, P.CategoryDesc, P.Subdivision_Desc, P.Positioning_Desc Posicionamiento
	, p.name Descripción, pc.cost, Income Ingresos, Unidades, MSHfarma.VentasSB VentasIMS_SB
	, p.manufacturerdesc
	, MSHfarma.VentasCadena VentasIMS_CADENA, VentasCadena, UnidadesCadena
	, pc.PriceSB, ROUND(sPrice.sPrice,0) sPrice
	, comp.Fasa PriceFasa, LastUpdateFasa ÚltCotizaciónFasa
	, comp.CV PriceCV, LastUpdateCV ÚltCotizaciónCV
	, comp.Simi PriceSimi, LastUpdateSimi ÚltCotizaciónSimi
	--NS 
	, ns.QuiebreLocales, ns.Locales
FROM products.DemandGroupProduct dgp
INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
INNER JOIN [class].ProductClassificationFinal pcf on pcf.SKU=DGP.SKU
INNER JOIN [class].ClassificationType ct on ct.Id=pcf.ClassificationTypeId
INNER JOIN (select dbo.getmonday(date) Date,pc.SKU, AVG(PC.Price) PriceSB, AVG(pc.Cost) Cost from series.priceandcost PC 
		where date between @Desde and @Hasta group by dbo.getmonday(date), pc.SKU) PC ON pc.SKU=DGP.SKU
LEFT JOIN PRICING.LastPriceAndCost PC1 ON PC1.SKU=DGP.SKU

--DIVISION
INNER JOIN (SELECT * FROM products.products WHERE DIVISION=1) p on p.SKU=pc.SKU

--CATEGORYS
LEFT JOIN pricing.CategoryInfo ic on ic.SKU=pc.SKU

--MShareFARMA	
LEFT JOIN (
	select p.Division_Desc, p.Positioning_Desc, [Date], p.SKU, p.Description
		, SUM(VentasSB) VentasSB
		, SUM(VentasCadenas-VentasSB) VentasCompetidores
		, SUM(VentasCadenas) VentasCadena
		, SUM(UnidadesCadenas) UnidadesCadena
	from series.IMS i
	inner join products.FullProducts p on p.SKU=i.SKU
	where [Date] between @Desde and @Hasta
	GROUP BY p.SKU, p.Description, p.Division_Desc, p.Positioning_Desc, [Date]
	HAVING SUM(UnidadesCadenas) >0) MSHfarma on MSHfarma.sku=pc.sku and MSHfarma.Date=pc.Date

--ÚLTIMA COTIZACIÓN
LEFT JOIN (select * from pricing.LastCompetitorsPrice) COMP ON comp.sku=pc.sku

--NIVEL DE SERVICIO
LEFT JOIN (select cast(dbo.getmonday(date) as date) Semana, SKU--, 1-SUM(cast(QuiebreLocales as float))/SUM(cast(Combinaciones as float)) NS
			, SUM(cast(QuiebreLocales as float)) QuiebreLocales
			, SUM(cast(Combinaciones as float)) Locales
			from reports.lostsalebySKU 
		--	where sku = 2832611
			group by sku, cast(dbo.getmonday(date) as date)
		--	order by cast(dbo.getmonday(date) as date) desc
			) ns on ns.sku=pc.sku and ns.Semana=pc.Date

-- VENTA Y CONTRIBUCIÓN
LEFT JOIN (select sku,dbo.getmonday(date) Date, sum(income) Income, sum(cast(case when quantity=0 then null else Quantity end as float)) Unidades
			, sum(income)/sum(cast(case when quantity=0 then null else Quantity end as float)) sPrice
			from series.sales 
			where date between @Desde and @Hasta
			group by sku,dbo.getmonday(date)
			HAVING sum(quantity)>0
			) sPrice on sPrice.sku=pc.sku and sPrice.date=pc.date

--MOTIVOS POR SKU OPERATIVOS			
--LEFT JOIN (SELECT [SKU], [Motive], [Cost], [Price], [Date] 
--			FROM (SELECT [SKU], [Motive], [Cost], [Price], [Date], ROW_NUMBER() over (partition by sku order by [Date] desc) rn
--			FROM [Salcobrand].[aux].[PriceAndCost] p) a Where rn = 1) MOTIVE ON MOTIVE.SKU=pc.SKU
LEFT JOIN (select * from pricing.ProductMotive) OperativeMotive on cast(OperativeMotive.MotiveId as varchar(50))=cast(pc1.Motive as varchar(50))
WHERE dgn.ParentId in (2,3)
--where 
--MOTIVE.SKU is not null AND
--OperativeMotive.IsOperative IS NOT NULL
--AND PC.SKU IN (SELECT TOP 10 SKU FROM PRODUCTS.Products)
--AND PC.SKU =3262895
) a


---CONSUMO-----
--Seleccionamos el periodo a mirar evolución

--3MESES 
--6MESES 
--1AÑO 7 MINUTOSYMEDIO 360MIL FILAS

select-- top 1 
Descripción,SKU,Descripción,CAST(Date AS DATE) Date,División, Clasificación, Motive, MotivoOperativo, CategoryDesc Categoría, Clase
, CategoryManager_Desc Category
-- Campo manufacturer agregado el jueves 11-02-2016
, ManufacturerDesc Fabricante
, Cost, Ingresos, Unidades
, case when VentasNIELSEN_SB is null then 0 else VentasNIELSEN_SB end VentasNIELSEN_SB
, case when VentasNIELSEN_CADENA is null then 0 else VentasNIELSEN_CADENA end VentasNIELSEN_CADENA
, case when PriceSB*Unidades is null then 0 else PriceSB*Unidades end VentasSB
, case when PriceFasa*Unidades is null then 0 else PriceFasa*Unidades end VentasFasa
, case when PriceCV*Unidades is null then 0 else PriceCV*Unidades end VentasCV
, case when PriceSimi*Unidades is null then 0 else PriceSimi*Unidades end VentasSimi
, case when sPrice*Unidades is null then 0 else sPrice*Unidades end VentasEfectivasSB
, Cost*Unidades CostoUnidades
, PriceSB, sPrice, PriceFasa, ÚltCotizaciónFasa, PriceSimi, ÚltCotizaciónSimi, PriceCV, ÚltCotizaciónCV
, QuiebreLocales, Locales
from (select DGP.SKU, dgn.Id Categoría, dgn.Name NombreCategoría, ct.Name Clasificación, IC.CategoryManager_Desc
	, PC1.Motive, pc.date, OperativeMotive.IsOperative MotivoOperativo
	, p.Division_Desc División, P.CategoryDesc, P.Subdivision_Desc, P.Class_Desc Clase
	, p.name Descripción, pc.cost, Income Ingresos, Unidades, MSHconsumo.VentasSB VentasNIELSEN_SB
	-- Campo manufacturer agregado el jueves 11-02-2016
	, p.manufacturerdesc
	, MSHconsumo.VentasCadena VentasNIELSEN_CADENA, VentasCadena, UnidadesCadena
	, pc.PriceSB, ROUND(sPrice.sPrice,0) sPrice
	, comp.Fasa PriceFasa, LastUpdateFasa ÚltCotizaciónFasa
	, comp.CV PriceCV, LastUpdateCV ÚltCotizaciónCV
	, comp.Simi PriceSimi, LastUpdateSimi ÚltCotizaciónSimi
		--NS 
	, ns.QuiebreLocales, ns.Locales
FROM products.DemandGroupProduct dgp
INNER JOIN products.DemandGroupNew dgn on dgn.id = dgp.Id 
INNER JOIN [class].ProductClassificationFinal pcf on pcf.SKU=DGP.SKU
INNER JOIN [class].ClassificationType ct on ct.Id=pcf.ClassificationTypeId
INNER JOIN (select dbo.getmonday(date) Date,pc.SKU, AVG(PC.Price) PriceSB, AVG(pc.Cost) Cost from series.priceandcost PC 
		where date between @Desde and @Hasta group by dbo.getmonday(date), pc.SKU) PC ON pc.SKU=DGP.SKU
LEFT JOIN PRICING.LastPriceAndCost PC1 ON PC1.SKU=DGP.SKU
--DIVISION
INNER JOIN (SELECT * FROM products.products WHERE DIVISION=2) p on p.SKU=pc.SKU

--CATEGORYS
LEFT JOIN pricing.CategoryInfo ic on ic.SKU=pc.SKU

--MShareCONSUMO	
LEFT JOIN (
	select p.Division_Desc, p.Positioning_Desc, Semana, p.SKU, p.Description
	, SUM(ValoresSB) VentasSB
	, SUM(ValoresCadena-ValoresSB) VentasCompetidores
	, SUM(ValoresCadena) VentasCadena
	, SUM(UnidadesCadena) UnidadesCadena
	from series.NIELSEN i
	inner join products.FullProducts p on p.SKU=i.SKU
	where Semana between @Desde and @Hasta
	GROUP BY p.SKU, p.Description, p.Division_Desc, p.Positioning_Desc, Semana
	HAVING SUM(UnidadesCadena) >0) MSHconsumo on MSHconsumo.sku=pc.sku and MSHconsumo.Semana=dbo.getmonday(pc.Date)

--ÚLTIMA COTIZACIÓN
LEFT JOIN (select * from pricing.LastCompetitorsPrice) COMP ON comp.sku=pc.sku

--NIVEL DE SERVICIO
LEFT JOIN (select cast(dbo.getmonday(date) as date) Semana, SKU--, 1-SUM(cast(QuiebreLocales as float))/SUM(cast(Combinaciones as float)) NS
			, SUM(cast(QuiebreLocales as float)) QuiebreLocales
			, SUM(cast(Combinaciones as float)) Locales
			from reports.lostsalebySKU 
		--	where sku = 2832611
			group by sku, cast(dbo.getmonday(date) as date)
		--	order by cast(dbo.getmonday(date) as date) desc
			) ns on ns.sku=pc.sku and ns.Semana=pc.Date

-- VENTA Y CONTRIBUCIÓN
LEFT JOIN (select sku,dbo.getmonday(date) Date, sum(income) Income, sum(cast(case when quantity=0 then null else Quantity end as float)) Unidades
			, sum(income)/sum(cast(case when quantity=0 then null else Quantity end as float)) sPrice
			from series.sales 
			where date between @Desde and @Hasta
			group by sku,dbo.getmonday(date)
			HAVING sum(quantity)>0
			) sPrice on sPrice.sku=pc.sku and sPrice.date=pc.date

--MOTIVOS POR SKU OPERATIVOS			
--LEFT JOIN (SELECT [SKU], [Motive], [Cost], [Price], [Date] 
--			FROM (SELECT [SKU], [Motive], [Cost], [Price], [Date], ROW_NUMBER() over (partition by sku order by [Date] desc) rn
--			FROM [Salcobrand].[aux].[PriceAndCost] p) a Where rn = 1) MOTIVE ON MOTIVE.SKU=pc.SKU
LEFT JOIN (select * from pricing.ProductMotive) OperativeMotive on cast(OperativeMotive.MotiveId as varchar(50))=cast(pc1.Motive as varchar(50))
WHERE dgn.ParentId in (2,3)
--where 
--MOTIVE.SKU is not null AND
--OperativeMotive.IsOperative IS NOT NULL
--AND PC.SKU IN (SELECT TOP 10 SKU FROM PRODUCTS.Products)
--AND PC.SKU =3262895
) a
END
