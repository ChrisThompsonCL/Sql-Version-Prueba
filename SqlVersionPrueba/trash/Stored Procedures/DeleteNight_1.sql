﻿-- =============================================
-- Author:		Mauricio Sepulveda
-- Create date: 2009-09-14
-- Description:	vuelva las tablas al dia anterior antes de la carga nocturna
-- =============================================
create PROCEDURE trash.[DeleteNight] 
	-- Add the parameters for the stored procedure here
	@day smalldatetime = NULL
AS
BEGIN
	delete from [series].[Stock] where date >= @day
	delete from [temp].[TempStock] where date >= @day
	--delete from [series].[Suggests] where date >= @day
	delete from [temp].[TempSuggests] where date >= @day
	delete from [series].[TransitStock] where date >= @day
	delete from [temp].[TempTransitStock] where date >= @day
	delete from [series].[Sales] where date >= dateadd(day,-1,@day)
	delete from [series].[StockCD] where date >= @day
	delete from [robot].[Log] where Date >= @day
END
