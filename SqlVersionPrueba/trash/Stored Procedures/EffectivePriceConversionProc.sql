﻿-- =============================================
-- Author:		Sebastián Manzano
-- Create date: 2012-06-12
-- Description:	<Description,,>
-- =============================================
create PROCEDURE trash.[EffectivePriceConversionProc] 
	-- Add the parameters for the stored procedure here
	@weeks int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @lastDate smalldatetime 
	DECLARE @beginDate smalldatetime

	SELECT @lastDate  = MAX([Date]) from series.Sales
	SELECT @beginDate = DATEADD(day, (-@weeks*7)+1, @lastDate)

	TRUNCATE TABLE [pricing].[EffectivePriceConversion]

	INSERT INTO [pricing].[EffectivePriceConversion] ([SKU],[Date],[ListPrice],[Quantity],[EffectivePrice],[Conversion])
	SELECT SKU, @lastDate Date,AVG(ListPrice) ListPrice
		, CASE WHEN SUM(Quantity) <= 0 OR SUM(Quantity) IS NULL THEN 0 ELSE SUM(Quantity) END Quantity
		, CASE 
			WHEN SUM(Quantity) <= 0 OR SUM(Quantity) IS NULL THEN AVG(ListPrice)
			ELSE SUM(Peff*Quantity)/SUM(Quantity) 
		END EffectivePrice
		, CASE 
			WHEN SUM(Quantity) <= 0 OR SUM(Quantity) IS NULL THEN 1.0 
		ELSE SUM(mu*Quantity)/SUM(Quantity) END Conversion
	FROM (
		SELECT *, CASE WHEN a.ListPrice <= 0 THEN NULL ELSE [function].inLineMin(a.Peff,a.ListPrice)/a.ListPrice END mu 
		FROM (
			SELECT p.SKU, Quantity, p.Date
			, CASE WHEN p.Price IS NULL OR p.Price <= 0 THEN avrg.AvgPrice ELSE p.Price END ListPrice
			, CASE WHEN Quantity <= 0 THEN 0 ELSE Income/Quantity END Peff
			FROM
				(SELECT * FROM series.PriceAndCost 
				WHERE Date between @beginDate and @lastDate and SKU in (select SKU from products.Products)
			) p
			LEFT JOIN
				(SELECT SKU, AVG(Price) AvgPrice, AVG(Cost) AvgCost FROM series.PriceAndCost 
				WHERE Date between @beginDate and @lastDate and SKU in (select SKU from products.Products)
				GROUP BY SKU
			) avrg ON avrg.SKU = p.SKU 
			LEFT JOIN
				(SELECT SKU, [Date] , SUM(Quantity) Quantity, SUM(Income) Income 
				FROM series.Sales
				WHERE [Date] between @beginDate and @lastDate and SKU in (select SKU from products.Products)
					AND Store not in (SELECT Store FROM operation.Pricingexcludedstores union SELECT Store FROM temp.LocalesFarmaPrecio) --VENTA DE LOCALES EXTREMOS --	VENTA DE FARMA PRECIO
				GROUP BY SKU, [Date]
			) s ON p.SKU = s.SKU AND p.Date = s.Date
		) a
	) b
	GROUP BY SKU
	ORDER BY Conversion


END
