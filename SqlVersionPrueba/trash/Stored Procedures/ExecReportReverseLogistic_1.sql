﻿
-- =============================================
CREATE PROCEDURE trash.[ExecReportReverseLogistic] 
	@date smalldatetime = NULL --puede ser cualquier fecha y la lleva a lunes de la semana
AS
BEGIN
DECLARE @fromDays int,@fromDaysSales int,@toDays int, @divisorMensual int, @divisorAnual int

Set @date = dbo.GetMonday(@date)
SET @fromDays = -27 -- para el sugerido historico
SET @toDays = 41 -- para el maximo sugerido 
SET @fromDaysSales = -7*12 --semanas de venta, para la venta de 3 meses
set @divisorMensual = datediff(week,dbo.GetMonday(dateadd(day,@fromDaysSales,@date)),@date)
set @divisorAnual = datediff(week,dbo.GetMonday(dateadd(day,-364,@date)),@date)

DECLARE @skus table (sku int)

insert into @skus select sku from [products].[FullProducts]

truncate table [reports].[ReverseLogisticResult]

-- inserto el stock actual para cada local
insert into [reports].[ReverseLogisticResult] (sku,store,stock)
select sku,store,stock+transit from [series].[LastStock] where sku in (select sku from @skus)
and suggest>0 and SKU in (select sku from products.Products where FirstSale <=DATEADD(day,-28*18,@date) and LogisticAreaId not in ('Bref.Ref','Bcon.Con') and MotiveId  not in (9))

update [reports].[ReverseLogisticResult]
set SugMaxHis = T.suggest from [reports].[ReverseLogisticResult] r 
inner join (
select sku,store,max(suggest) suggest from (
select r.sku,r.store,suggest from reports.ReportStores r
inner join reports.ReverseLogisticResult l
on l.SKU = r.SKU and r.Store = l.Store
where r.Date >= DATEADD(day,-90,@date)
) h
group by sku,store

) T
on T.sku = r.sku and T.store = r.store

-- coloco el sugerido maximo futuro
update [reports].[ReverseLogisticResult]
set SugMaxFut = T.sugMax from [reports].[ReverseLogisticResult] r inner join (
select sku,store, MAX(sug) sugMax from (
SELECT [SKU],[Store],[W1] sug FROM [minmax].[SuggestForecastCorrected]
union all 
SELECT [SKU],[Store],[W2] sug FROM [minmax].[SuggestForecastCorrected]
union all 
SELECT [SKU],[Store],[W3] sug FROM [minmax].[SuggestForecastCorrected]
union all 
SELECT [SKU],[Store],[W4] sug FROM [minmax].[SuggestForecastCorrected]) s
group by SKU,store
) T
on T.sku = r.sku and T.store = r.store

-- coloco la venta promedio semanal 3 ultimos meses
update [reports].[ReverseLogisticResult]
set Sale3Month = T.salew from [reports].[ReverseLogisticResult] r inner join (
select sku,store, convert(float,sum(quantity))/@divisorMensual salew from 
[series].[Sales] where date between dateadd(day,@fromDaysSales,@date) and @date and sku in (select sku from @skus)
group by sku,store
) T
on T.sku = r.sku and T.store = r.store


-- coloco la venta promedio semanal anual
update [reports].[ReverseLogisticResult]
set SaleYear = T.salew from [reports].[ReverseLogisticResult] r inner join (
select sku,store, convert(float,sum(quantity))/@divisorAnual salew from 
[series].[Sales] where date between dateadd(day,-364,@date) and @date and sku in (select sku from @skus)
group by sku,store
) T
on T.sku = r.sku and T.store = r.store


--update [reports].[ReverseLogisticResult]
--set SaleFut = T.saleFut from [reports].[ReverseLogisticResult] r inner join (
--select fo.sku,sh.Store,fo.Fit*sh.Share*6 saleFut from 
--(select ProductId sku,fit from forecast.PredictedSales
--where DateKey in (select MAX(DateKey) from forecast.PredictedSales)
--and Date in (select MIN(date) from forecast.PredictedSales where DateKey in (select MAX(DateKey) from forecast.PredictedSales))) fo
--inner join forecast.ShareStoreCurrent sh
--on sh.SKU = fo.sku 
--) T on T.sku = r.sku and T.store = r.store 

-- cambio de predicted sales
update [reports].[ReverseLogisticResult]
set SaleFut = T.saleFut from [reports].[ReverseLogisticResult] r 
inner join (
select sku,store,sum(fit)*6  saleFut from minmax.FitsAllCurrent
where dbo.getmonday(date) in (select MIN(dbo.getmonday(date)) from minmax.FitsAllCurrent)
group by sku,store
) T on T.sku = r.sku and T.store = r.store 


--SaleFut para los sin pronosticos -mira 4 semanas hacia adelante de la venta del año pasado.
update [reports].[ReverseLogisticResult]
set SaleFut = T.salew from [reports].[ReverseLogisticResult] r inner join (
select sku,store, convert(float,sum(quantity))/4 salew from 
[series].[Sales] where date between dateadd(week,-52,@date) and dateadd(day,27,dateadd(week,-52,@date)) and sku in (select sku from @skus)
group by sku,store
) T
on T.sku = r.sku and T.store = r.store
where SaleFut is null

-- coloco el sugerido maximo futuro para los que no tienen sugerido futuro
update [reports].[ReverseLogisticResult]
set SugMaxFut = T.suggest from [reports].[ReverseLogisticResult] r inner join (
select sku,store,ceiling((case when [Sale3Month] > [SaleFut] then [Sale3Month] else [SaleFut] end) *2) as suggest from [reports].[ReverseLogisticResult]
) T
on T.sku = r.sku and T.store = r.store
where SugMaxFut is null

--restriccion dura
delete from [reports].[ReverseLogisticResult]
where sku in (select sku from [products].[Products] where logisticareaid in ('Bcon.Con','Bref.ref'))


update [reports].[ReverseLogisticResult] 
set SugMax = 0 where SugMax is null

update [reports].[ReverseLogisticResult] 
set Sale3Month = 0 where Sale3Month is null

update [reports].[ReverseLogisticResult] 
set SaleYear = 0 where SaleYear is null

update [reports].[ReverseLogisticResult] 
set SaleFut = 0 where SaleFut is null

update [reports].[ReverseLogisticResult] 
set SaleWeekMax = 0 where SaleWeekMax is null


-- actualizo el max
update [reports].[ReverseLogisticResult] 
set SugMax = case when [SugMaxHis] > [SugMaxFut] then [SugMaxHis] else [SugMaxFut] end 
, SaleWeekMax = case when [Sale3Month] > [SaleFut] then [Sale3Month] else [SaleFut] end

update [reports].ReverseLogisticResult
set SaleWeekMax = case when SaleWeekMax> SaleYear then SaleWeekMax else SaleYear end

delete from [reports].[ReverseLogisticResult] 
where SugMax is null

--dejar este cambio en otra columna 
--update [reports].[ReverseLogisticResult] 
--set SugMaxPond = SugMax*4 
--where sku in (select sku from [products].[FullProducts] where totalranking = 'A')

--update [reports].[ReverseLogisticResult] 
--set SugMaxPond = SugMax*3 
--where sku in (select sku from [products].[FullProducts] where totalranking = 'B')

--update [reports].[ReverseLogisticResult] 
--set SugMaxPond = SugMax*2 
--where sku in (select sku from [products].[FullProducts] where totalranking in ('C','D'))

--update [reports].[ReverseLogisticResult] 
--set SugMaxPond = SugMax*2 
--where SugMaxPond is null

update [reports].[ReverseLogisticResult] 
set SugMaxPond = SugMax


-- borra las filas que no tienen sobre stock, hay que cambiar sugMax por la nueva fila que se creara
delete from [reports].[ReverseLogisticResult]
where stock<=sugMaxPond 

-- calcula los dias de sobre stock
update [reports].[ReverseLogisticResult] 
set OverStockDays = case when SaleWeekMax>0 then convert(float,stock-sugMaxPond)/(convert(float,SaleWeekMax)/7) else -1 end

update rlr set Planogram=srt.Planograma, Dun = (srt.MedioDUN-1)*2, minimum=srt.Minimo
from [reports].[ReverseLogisticResult]  rlr
inner join [view].SuggestRestrictionToday srt on srt.SKU=rlr.SKU and srt.Store=rlr.Store


---- info planograma
--update r set planogram = p.Detail from [reports].[ReverseLogisticResult] r
--inner join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 1
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store

---- info DUN 
--update r set Dun = case when p.Detail > isnull(q.Detail,0) and p.Detail is not null then p.Detail else q.Detail end from [reports].[ReverseLogisticResult] r
--left outer join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 6
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store
--left outer join (
--	select ProductID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 5
--	group by ProductID,StoreID
--) q on q.ProductID = r.sku
--where q.Detail is not null or p.Detail is not null

---- info caja tira
--update r set Child = p.Detail from [reports].[ReverseLogisticResult] r
--inner join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 7
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU

---- info minimo
--update r set [Minimum] = case when p.Detail > isnull(q.Detail,0) and p.Detail is not null then p.Detail else q.Detail end from [reports].[ReverseLogisticResult] r
--left outer join (
--	select ProductID,StoreID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 4
--	group by ProductID,StoreID
--) p on p.ProductID = r.SKU and p.StoreID = r.Store
--left outer join (
--	select ProductID,max(Detail) Detail from [function].GetActiveSuggestRestriction(@date)
--	where SuggestRestrictionTypeID = 3
--	group by ProductID,StoreID
--) q on q.ProductID = r.sku
--where q.Detail is not null or p.Detail is not null

-- fecha última promocion
update r set LastPromotion = p.date from [reports].[ReverseLogisticResult] r inner join (
	select SKU, MAX(EndDate) Date from (
		
		select ProductId SKU,MAX(EndDate) EndDate from promotion.PromotionStock group by ProductId 
	) a group by SKU
) p on p.SKU = r.SKU 

-- cargo ranking, generic y motive
update r set Ranking = p.TotalRanking,IsGeneric=p.IsGeneric,motive = p.Motive,Division=p.Division from [reports].ReverseLogisticResult r
inner join products.FullProducts p on p.SKU = r.SKU 

-- sobrestock
update r set OverStock = stock-sugMaxPond from [reports].ReverseLogisticResult r

-- cargo costo
update r set Cost = p.cost from [reports].ReverseLogisticResult r
inner join (
select sku,cost from series.PriceAndCost 
where Date in (select MAX(date) from series.PriceAndCost)
) p on p.SKU = r.SKU 

update r set cost =0 from [reports].ReverseLogisticResult r
where r.cost is null

-- cargo sobrestock valorado
update r set OverStockVal =OverStock*cost from [reports].ReverseLogisticResult r


END
