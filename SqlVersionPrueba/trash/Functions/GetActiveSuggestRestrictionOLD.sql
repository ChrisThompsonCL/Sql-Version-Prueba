﻿

-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2012-06-04
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [trash].[GetActiveSuggestRestrictionOLD]
(
	@date smalldatetime
)
RETURNS  TABLE 
AS
RETURN (
	-- Fill the table variable with the rows for your result set
	SELECT ProductID, ProductType, StoreID, StoreType, BeginDate, EndDate, Detail, SuggestRestrictionTypeID, [Description], ActionID 
	FROM (
		SELECT * , ROW_NUMBER() over (partition by ProductID, ProductType, StoreID, StoreType, ActionID order by Detail desc, SuggestRestrictionTypeID desc) R
		FROM (
			SELECT distinct ISNULL(fixAll.ProductID, fixOne.ProductID) ProductID 
				, ISNULL(fixAll.ProductType, fixOne.ProductType) ProductType
				, ISNULL(fixAll.StoreID, fixOne.StoreID) StoreID
				, ISNULL(fixAll.StoreType, fixOne.StoreType) StoreType
				, ISNULL(fixAll.BeginDate, fixOne.BeginDate) BeginDate
				, CASE WHEN ISNULL(fixAll.EndDate, fixOne.EndDate) = '2078-12-31' THEN NULL ELSE ISNULL(fixAll.EndDate, fixOne.EndDate) END EndDate
				, ISNULL(fixAll.Detail, fixOne.Detail) Detail
				, ISNULL(fixAll.SuggestRestrictionTypeID, fixOne.SuggestRestrictionTypeID) SuggestRestrictionTypeID
				, ISNULL(fixAll.Name, fixOne.Name) Name
				, ISNULL(fixAll.[Description], fixOne.[Description]) [Description]
				, ISNULL(fixAll.ActionID, fixOne.ActionID) ActionID
			FROM ( 
				SELECT * FROM (
					SELECT srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.BeginDate, ISNULL(srd.EndDate, '2078-12-31') EndDate, srd.Detail, srd.SuggestRestrictionTypeId, Name, [Description], ActionID
						, cast(InsertionDate as smalldatetime) InsertionDate
						, ROW_NUMBER() over (partition by  srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.SuggestRestrictionTypeId order by cast(InsertionDate as smalldatetime) desc, Detail desc) RBySuggestRestrictionType
					FROM [view].[SuggestRestrictionDetail] srd 
					INNER JOIN [br].[SuggestRestrictionType] srt on srt.SuggestRestrictionTypeID = srd.SuggestRestrictionTypeID
					WHERE @date BETWEEN BeginDate AND ISNULL(EndDate, '2078-12-31') AND ActionID = 3 -- DUN / Caja Tira
				) a WHERE a.RBySuggestRestrictionType = 1
				union all
				SELECT * FROM (
					SELECT srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.BeginDate, ISNULL(srd.EndDate, '2078-12-31') EndDate, srd.Detail, srd.SuggestRestrictionTypeId, Name, [Description], ActionID
						, cast(InsertionDate as smalldatetime) InsertionDate
						, ROW_NUMBER() over (partition by  srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.SuggestRestrictionTypeId order by Detail desc, cast(InsertionDate as smalldatetime) desc) RBySuggestRestrictionType
					FROM [view].[SuggestRestrictionDetail] srd 
					INNER JOIN [br].[SuggestRestrictionType] srt on srt.SuggestRestrictionTypeID = srd.SuggestRestrictionTypeID
					WHERE @date BETWEEN BeginDate AND ISNULL(EndDate, '2078-12-31') AND ActionID = 1 -- Mínimo
				) a WHERE a.RBySuggestRestrictionType = 1
			) fixOne
			full outer join ( 
				SELECT * FROM (
					SELECT srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.BeginDate, ISNULL(srd.EndDate, '2078-12-31') EndDate, srd.Detail, srd.SuggestRestrictionTypeId, Name, [Description], ActionID
						, cast(InsertionDate as smalldatetime) InsertionDate
						, ROW_NUMBER() over (partition by  srd.ProductID, srd.ProductType, srd.StoreID, srd.StoreType, srd.SuggestRestrictionTypeId order by cast(InsertionDate as smalldatetime) desc, Detail desc) RBySuggestRestrictionType
					FROM [view].[SuggestRestrictionDetail] srd 
					INNER JOIN [br].[SuggestRestrictionType] srt on srt.SuggestRestrictionTypeID = srd.SuggestRestrictionTypeID
					WHERE @date BETWEEN BeginDate AND ISNULL(EndDate, '2078-12-31') AND ActionID = 2 -- Fijo
				) a WHERE a.RBySuggestRestrictionType = 1
			) fixAll on fixAll.ProductID = fixOne.ProductID and fixAll.ProductType = fixOne.ProductType and fixAll.StoreID = fixOne.StoreID and fixAll.StoreType = fixOne.StoreType
		) rs
	) f WHERE f.R = 1
)

