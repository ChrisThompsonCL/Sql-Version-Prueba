﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-08-10
-- Description:	Aplica restricciones sobre el sugerido entregado
-- =============================================
CREATE FUNCTION trash.[ApplySuggestRestriction]
(	
	-- Add the parameters for the function here
	@sugerido int,
	@minimo int = null,
	@fijo int = null,
	@planograma int = null,
	@medioDUN int = null--,
	--@maximo int = null,
	--@cajatira int = null,
	--@minimoEstricto int = null
)
RETURNS TABLE 
AS
RETURN 
(
	select case 
		when @mediodun > (case 
			when @planograma > (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) 
			then @planograma 
			else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) 
		end) 
		then @mediodun 
		else (case 
			when @planograma > (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) 
			then @planograma 
			else (case when @fijo is not null then @fijo else (case when @minimo > @sugerido then @minimo else @sugerido end) end) 
		end) 
	end RestrictedSuggest
)
