﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2017-06-06
-- Description:	<Description,,>
-- =============================================
create function trash.[SuggestRestrictionForDay]
(	
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT ls.SKU, ls.Store, max(ls.Suggest) Suggest, max(minimo.Minimo) Minimo, max(fijo.Fijo) Fijo, max(planograma.Planograma) Planograma, Max(dun.MedioDUN) MedioDUN, min(Maximo)Maximo, max(Cajatira)CajaTira, max(FijoEstricto)FijoEstricto
	FROM series.LastStock ls
	LEFT JOIN (
		/*MINIMO*/
		SELECT ls.SKU, ls.Store
			, MAX(case when srd.SuggestRestrictionTypeID in (3,4,8,12) then srd.Detail else null end) Minimo
		FROM [view].[SuggestRestrictionDetail] srd
		INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
		WHERE @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (3,4,8,12)
		GROUP BY ls.SKU, ls.Store
	) minimo on minimo.SKU=ls.SKU and minimo.Store=ls.Store
	LEFT JOIN (
		/*FIJO*/
		SELECt SKU, Store, Fijo
		FROM (
			SELECT srd.ProductID SKU, srd.StoreID Store
				, case when srd.SuggestRestrictionTypeID=2 then srd.Detail else null end Fijo
				, ROW_NUMBER() over (partition by srd.ProductID , srd.StoreID order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
			FROM [view].[SuggestRestrictionDetail] srd
			where @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (2)
		) fijo WHERE R=1
	) fijo ON fijo.SKU=ls.SKU and fijo.Store=ls.Store
	LEFT JOIN (
		/*PLANOGRAMA*/
		SELECT srd.ProductID SKU, srd.StoreID Store
			, MAX(case when srd.SuggestRestrictionTypeID in (9,10,11) then srd.Detail else null end) Planograma
		FROM [view].[SuggestRestrictionDetail] srd
		where @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (9,10,11)
		group by srd.ProductID , srd.StoreID
	) planograma on planograma.SKU=ls.SKU and planograma.Store=ls.Store
	LEFT JOIN (
		/*DUN*/
		SELECT SKU, Store, MedioDUN
		FROM (
			SELECT ls.SKU, ls.Store
				, case when srd.SuggestRestrictionTypeID in (5,6,7) then CEILING(Detail*1.0/2.0)+1 else null end MedioDUN
				, ROW_NUMBER() over (partition by ls.SKU, ls.Store order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
			FROM [view].[SuggestRestrictionDetail] srd
			INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
			WHERE @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (5,6,7)
		) dun WHERE R=1
	) dun on dun.SKU=ls.SKU and dun.Store=ls.Store
	LEFT JOIN (
		/*MAXIMO*/
		SELECT ls.SKU, ls.Store
			, MIN(case when srd.SuggestRestrictionTypeID in (13,14) then srd.Detail else null end) Maximo
		FROM [view].[SuggestRestrictionDetail] srd
		INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
		WHERE @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (13,14)
		GROUP BY ls.SKU, ls.Store
	) maximo on maximo.SKU=ls.SKU and maximo.Store=ls.Store
	LEFT JOIN (
		/*CajaTira*/
			SELECT ls.SKU, ls.Store
				, Detail CajaTira
			FROM [view].[SuggestRestrictionDetail] srd
			INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId
			WHERE @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (15)
	) cajatira on cajatira.SKU=ls.SKU and cajatira.Store=ls.Store
	LEFT JOIN (
		/*FIJO ESTRICTO*/
		SELECt SKU, Store, FijoEstricto
		FROM (
			SELECT srd.ProductID SKU, srd.StoreID Store
				, case when srd.SuggestRestrictionTypeID=16 then srd.Detail else null end FijoEstricto
				, ROW_NUMBER() over (partition by srd.ProductID , srd.StoreID order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
			FROM [view].[SuggestRestrictionDetail] srd
			where @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (16)
		) FijoEstricto WHERE R=1
	) FijoEstricto on FijoEstricto.SKU=ls.SKU and FijoEstricto.Store=ls.Store
	WHERE minimo.Minimo is not null OR fijo.Fijo is not null OR planograma.Planograma is not null or dun.MedioDUN is not null or Maximo is not null or CajaTira is not null or FijoEstricto is not null
	group by ls.SKU, ls.Store

)

