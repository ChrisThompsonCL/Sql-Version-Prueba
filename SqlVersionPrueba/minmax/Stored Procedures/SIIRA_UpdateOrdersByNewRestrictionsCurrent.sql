﻿-- =============================================
-- Author:			Elizabeth Peña
-- Create date:		06-06-2018
-- Description:		Ajuste diario de Pedidos a partir de nuevas restricciones. Aplica las restricciones insertadas despues de la última fecha de ajuste y se identifican 
--					las alzas y bajas que se generan en el sugerido. En función de estos cambios, se calcula el cambio en la proyección de pedidos para cada caso.  
-- =============================================

CREATE PROCEDURE [minmax].[SIIRA_UpdateOrdersByNewRestrictionsCurrent]	 
	-- Add the parameters for the stored procedure here
	 @sku int,
	 @fecha_inicio datetime,
	 @fecha_fin datetime,
	 @fecha_ultimo_ajuste datetime 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
						
	--declare @sku int = 3110057
	--declare @fecha_inicio datetime = DATEADD(day, 1,(SELECT MIN(date) FROM [SalcobrandZ].[test].[DistributionOrdersForecastCurrent] WHERE sku = @sku))
	--declare @fecha_fin datetime = (SELECT MAX(date) FROM [SalcobrandZ].[test].[DistributionOrdersForecastCurrent] WHERE sku = @sku )
	--declare @fecha_ultimo_ajuste datetime = (SELECT fecha_calculo FROM SalcobrandZ.[test].[CalculationDateDistributionOrders] WHERE sku = @sku)
	declare @fecha_calculo datetime = getdate()
	declare @AreaLogistica nvarchar(50) = (SELECT LogisticAreaID FROM Salcobrand.Products.products WHERE SKU =@sku )
	declare @factor_correccion float = (CASE WHEN @AreaLogistica='Baes.Aes' THEN 0.3 ELSE 
										(CASE WHEN @AreaLogistica = 'Bespe.Esp' THEN 0.1 ELSE
										(CASE WHEN (@AreaLogistica= 'Bespe.Liq' OR @AreaLogistica= 'Bref.Ref')THEN 0.15 ELSE 
										(CASE WHEN @AreaLogistica = 'Bespe.Pro' THEN 0.4 ELSE 0 END)END)END)END)

----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
----OBS: YA SE DEBE HABER APLICADO "RestrictedSuggest Post Calculo Pedidos". 
----SE HARIA DESPUES DE QUE SE CALCULA LA PROYECCION DE PEDIDOS (LUEGO DE QUE ESTEN LISTOS, PARA NO DEMORAR MAS ESTE PROCEDIMIENTO)

 UPDATE SRT
		SET RestrictedSuggest =  a1.RestrictedSuggest
 FROM [minmax].[SugeridoProyeccionPedidosCurrent] SRT
	 CROSS APPLY [function].[ApplyRestriction](1, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.cajatira, srt.fijoestricto) a1 --srt.maximo, srt.cajatira, srt.fijoEstricto) a1 
	 WHERE srt.sku= @sku
----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------

--- VENTA PROMEDIO PARA CADA STORE
	SELECT store, AVG(Rate/Days) AVGVenta--, AVG(SuggestMin) AVGSugMin--, AVG(SuggestMax) AVGSugMax
	INTO #datosPromedios 
	FROM [minmax].[SuggestAllCurrent] --[SuggestAllCurrent] 
	WHERE sku = @sku 
	GROUP BY store
	HAVING avg(Rate/Days) > 0  ---> OJO, no se están considerando aquellos que tienen venta promedio == 0

	
--#################################################################################
--- SI #datosPromedios ESTÁ VACÍA ---> SALIR DEL PROCEDIMIENTO
	IF (SELECT COUNT(*) FROM #datosPromedios) = 0
		BEGIN 
			DROP TABLE #datosPromedios

		---- GUARDAR FECHA ÚLTIMO AJUSTE
		--	DELETE 
		--	FROM SalcobrandZ.[test].[CalculationDateDailyRestriction]
		--	WHERE sku=@sku

		--	INSERT INTO Salcobrandz.[test].[CalculationDateDailyRestriction]
		--	SELECT @sku,@fecha_calculo
		RETURN
		END
--#################################################################################

CREATE TABLE #SugPedidos (date date, storeid int, suggest int, PRIMARY KEY (storeid,date))
CREATE TABLE #SugConRestr (date datetime, storeid int, suggest int, RestrictedSuggest int, PRIMARY KEY (storeid,date))


--- BUSCAR	 NUEVAS RESTRICCIONES AL SUGERIDO
EXEC [minmax].[SIIRA_SuggestCorrectedByRestriction] @sku,@fecha_ultimo_ajuste,@fecha_inicio,@fecha_fin,0


--- SUGERIDO DE PROYECIÓN SEMANAL DE PEDIDOS
INSERT INTO #SugPedidos
	SELECT fecha, storeid, RestrictedSuggest AS Suggest
	FROM [minmax].[SugeridoProyeccionPedidosCurrent] s 
	 CROSS JOIN (SELECT Fecha FROM forecast.GenericVariables WHERE Fecha >= @fecha_inicio AND Fecha <= @fecha_fin) Fechas 
	WHERE sku = @sku AND s.date <= Fecha AND Fecha < ISNULL((SELECT MIN(ss.date) FROM [minmax].[SugeridoProyeccionPedidosCurrent] ss WHERE ss.sku = @sku AND ss.storeid=s.storeid AND ss.date > s.date), @fecha_fin)
	
 
----- APLICAR NUEVAS RESTRICCIONES 
INSERT INTO #SugConRestr
	SELECT srt.date, srt.storeid, srp.Suggest, a1.RestrictedSuggest
	FROM [aux].[SugeridoConRestriccionesCurrent] srt
		INNER JOIN #SugPedidos srp on srp.storeid = srt.storeid AND srp.date = srt.date
		CROSS APPLY [function].[ApplyRestriction] (ISNULL(srp.Suggest,1), srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.maximo, srt.cajatira, srt.fijoEstricto) a1	
		WHERE srt.sku = @sku


--- RUTA DE PEDIDOS para las stores que estan en las nuevas restricciones de este sku 
CREATE TABLE #Replenishment (store int, date datetime, days int, LT int, PRIMARY KEY (store, date))
INSERT INTO #Replenishment
	 SELECT  re.store, re.date, re.days, re.LT
	  FROM [minmax].[Replenishment] re
	  INNER JOIN [products].[products] p					ON p.LogisticAreaId = re.LogisticAreaId 
	  INNER JOIN (SELECT DISTINCT (StoreId) FROM #SugConRestr) t		ON t.StoreId = re.Store 
	  WHERE p.sku = @sku 
	  

	  	 
--#########################################################################################################
-----------------	CÁLCULO DE DELTAS EN LA PROYECCIÓN Y CON LAS NUESVAS RESTRICCIONES	-------------------
--#########################################################################################################

CREATE TABLE #CambiosSugerido (storeid int, date datetime, Suggest int, RestrictedSuggest int, DeltaSugProyeccion int, DeltaRestrSug int, DifHorizontal int, days int, AVGVenta float, PRIMARY KEY (storeid,date))

INSERT INTO #CambiosSugerido
 	SELECT sc.storeid
		,sc.date
		,sc.suggest
		,sc.RestrictedSuggest
		,sc.Suggest - scAyer.Suggest AS DeltaSugProyeccion
		,sc.restrictedSuggest - scAyer.restrictedSuggest AS DeltaRestrSug  
		,sc.RestrictedSuggest - sc.Suggest AS DifHorizontal
		,re.days - re.LT AS Days
		,dp.AVGVenta 
	FROM #SugConRestr sc
	INNER JOIN #Replenishment re				ON re.store = sc.storeid AND re.date  = sc.date
	INNER JOIN #datosPromedios dp				ON dp.store = sc.storeId
	LEFT JOIN #SugConRestr scAyer				ON scAyer.storeid = sc.storeid AND scAyer.date = dateadd(day,-1, sc.date) 
	



--#################################################################################################
-----------------	CONSTRUCCION DE SOBRESTOCK, ALZAS POR PEDIR Y PEDIDOS	----------------------
--#################################################################################################

	;WITH RecursivoSS (storeid, date, SobreStockProy, SobreStockRestr, AlzaPorPedirProy, AlzaPorPedirRestr, PedidoProy, PedidoRestr) AS
		(	SELECT cs.storeid, cs.date
					, CAST(0 AS FLOAT)AS SobreStockProy
					, CAST(0 AS FLOAT)AS SobreStockRestr
					, CAST(0 AS FLOAT)AS AlzaPorPedirProy
					, CAST(0 AS FLOAT)AS AlzaPorPedirRestr
					, CAST(0 AS FLOAT)AS PedidoProy
					, CAST(0 AS FLOAT)AS PedidoRestr
			FROM #CambiosSugerido cs
			WHERE cs.date = @fecha_inicio
		UNION ALL
			 SELECT cs.storeid, cs.date
		--SobreStockProy
			 , CASE WHEN (m.SobreStockProy + (CASE WHEN cs.DeltaSugProyeccion < 0 
												THEN(CASE WHEN -DeltaSugProyeccion > (@factor_correccion * cs.suggest) THEN -DeltaSugProyeccion - (@factor_correccion * cs.suggest) ELSE 0 END)
												ELSE (CASE WHEN cs.days > 0 THEN -DeltaSugProyeccion - m.AlzaPorPedirProy ELSE 0 END) END)-  AVGVenta * (cs.days+1)) > 0 
					THEN m.SobreStockProy + (CASE WHEN cs.DeltaSugProyeccion < 0 
												THEN(CASE WHEN -DeltaSugProyeccion > (@factor_correccion * cs.suggest) THEN -DeltaSugProyeccion - (@factor_correccion * cs.suggest) ELSE 0 END)
												ELSE (CASE WHEN cs.days > 0 THEN -DeltaSugProyeccion - m.AlzaPorPedirProy ELSE 0 END) END) - AVGVenta * (cs.days+1)
					ELSE 0 END 
		--SobreStockRestr
			 , CASE WHEN (m.SobreStockRestr + (CASE WHEN cs.DeltaRestrSug <0  
												THEN (CASE WHEN -DeltaRestrSug > (@factor_correccion*cs.RestrictedSuggest) THEN -DeltaRestrSug -(@factor_correccion*cs.RestrictedSuggest) ELSE 0 END) 
												ELSE (CASE WHEN cs.days > 0 THEN -DeltaRestrSug - m.AlzaPorPedirRestr ELSE 0 END) END) - AVGVenta * (cs.days+1)) > 0 
					THEN m.SobreStockRestr + (CASE WHEN cs.DeltaRestrSug <0  
												THEN (CASE WHEN -DeltaRestrSug > (@factor_correccion*cs.RestrictedSuggest) THEN -DeltaRestrSug -(@factor_correccion*cs.RestrictedSuggest) ELSE 0 END) 
												ELSE (CASE WHEN cs.days > 0 THEN -DeltaRestrSug - m.AlzaPorPedirRestr ELSE 0 END) END)- AVGVenta * (cs.days+1)
					ELSE 0 END  
		--AlzaPorPedirProy
			 , CASE WHEN cs.DeltaSugProyeccion > 0 AND cs.days = 0 THEN m.AlzaPorPedirProy + cs.DeltaSugProyeccion ELSE (CASE WHEN cs.Days >0 THEN 0 ELSE m.AlzaPorPedirProy END) END
		--AlzaPorPedirRestr
			 , CASE WHEN cs.DeltaRestrSug > 0 AND cs.days = 0 THEN m.AlzaPorPedirRestr + cs.DeltaRestrSug ELSE (CASE WHEN cs.Days >0 THEN 0 ELSE m.AlzaPorPedirRestr END) END
		--PedidoProyección
			, CASE WHEN cs.days > 0 AND (cs.AVGVenta * cs.days + m.AlzaPorPedirProy 
						+ (CASE WHEN cs.DeltaSugProyeccion < 0 
								THEN (CASE WHEN -DeltaSugProyeccion > (@factor_correccion * cs.suggest) THEN DeltaSugProyeccion + (@factor_correccion * cs.suggest) ELSE 0 END) 
								ELSE cs.DeltaSugProyeccion END)	> m.SobreStockProy)  
					THEN cs.AVGVenta * cs.days + m.AlzaPorPedirProy - m.SobreStockProy 
							+ (CASE WHEN cs.DeltaSugProyeccion < 0 
									THEN (CASE WHEN -DeltaSugProyeccion > (@factor_correccion * cs.suggest) THEN DeltaSugProyeccion + (@factor_correccion * cs.suggest) ELSE 0 END) 
									ELSE cs.DeltaSugProyeccion END)		
					ELSE 0 END 
		--PedidoRestr
			, CASE WHEN cs.days > 0 AND (cs.AVGVenta * cs.days + m.AlzaPorPedirRestr 
							+ (CASE WHEN cs.DeltaRestrSug <0 
									THEN (CASE WHEN -DeltaRestrSug > (@factor_correccion * cs.RestrictedSuggest) THEN DeltaRestrSug +(@factor_correccion * cs.RestrictedSuggest)ELSE 0 END) 
									ELSE cs.DeltaRestrSug END) > m.SobreStockRestr)
					THEN cs.AVGVenta * cs.days  + m.AlzaPorPedirRestr - m.SobreStockRestr 
							+ (CASE WHEN cs.DeltaRestrSug <0 
									THEN (CASE WHEN -DeltaRestrSug > (@factor_correccion * cs.RestrictedSuggest) THEN DeltaRestrSug +(@factor_correccion * cs.RestrictedSuggest)ELSE 0 END) 
									ELSE cs.DeltaRestrSug END) 
					ELSE 0 END
		FROM #CambiosSugerido cs 
			 JOIN RecursivoSS m  ON  cs.date = DATEADD(day, 1, m.date) and cs.storeid = m.storeid
		)


--------------------------------------------------------------------------------------------------------
--------- UPDATE A PEDIDOS
--------------------------------------------------------------------------------------------------------
	UPDATE do
	SET Delta_Restricciones = rss.DeltaRestr
		FROM [minmax].[DistributionOrdersForecastCurrent] do
			INNER JOIN (SELECT date, ROUND(SUM(PedidoRestr - PedidoProy),0) AS DeltaRestr FROM RecursivoSS GROUP BY date) rss on rss.date = do.date
		WHERE do.sku = @sku	
	
	
---- Eliminar SKU de tabla con nuevas restricciones

	DELETE 
	FROM [minmax].[SkuNewRestrictionsCurrent]
	WHERE sku=@sku


---- GUARDAR FECHA ÚLTIMO AJUSTE
--	DELETE 
--	FROM SalcobrandZ.[test].[CalculationDateDailyRestriction]
--	WHERE sku=@sku

--	INSERT INTO Salcobrandz.[test].[CalculationDateDailyRestriction]
--	SELECT @sku,@fecha_calculo


	drop table #datosPromedios
	drop table #SugPedidos
	drop table #SugConRestr
	drop table #Replenishment
	drop table #CambiosSugerido


END
