﻿
-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2012-04-18
-- Description:	Ejecuta todos los procesos para dejar el sugerido con min/max del dia calculado
-- =============================================
CREATE PROCEDURE [minmax].[DailySuggest] 
	@date date
AS
BEGIN
	DECLARE @msg VARCHAR(100) 
	SET NOCOUNT ON;
	
	Declare @monday date--,@sunday date
	select @monday=dbo.GetMonday(@date)
	--select @sunday=dateadd(day,-1,@monday)

	if @date = @monday
	begin
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando Carga Lunes...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
		truncate table [minmax].[SuggestAllCurrent]
		insert into [minmax].[SuggestAllCurrent]
		select * from [minmax].[SuggestAllNew]

		--inserta valores del pioloto Sugerido Distinto Min Max
		update sac   set SuggestMin=r.Smin, SuggestMax=r.Smax
		from [minmax].[SuggestAllCurrent] sac 
		inner join minmax.MinMaxCombinations d on d.SKU=sac.SKU and d.Store=sac.Store
		inner join minmax.MinMaxResult r on r.SKU=sac.SKU and r.Store=sac.Store and r.[Date]=sac.[Date]

		-- pasos para FIT
		truncate table [minmax].[FitsAllWeekEnd]
		insert into [minmax].[FitsAllWeekEnd]
		select * from [minmax].[FitsAllCurrent]
		where date between dateadd(day,-2,@monday) and dateadd(day,-1,@monday)

		truncate table [minmax].[FitsAllCurrent]
		insert into [minmax].[FitsAllCurrent]
		select  * from [minmax].[FitsAllNew]
		where date>=@monday

		INSERT INTO [minmax].[FitsAllCurrent]
		select * from [minmax].[FitsAllWeekEnd]

		-- Pedidos
		truncate table [minmax].[DistributionOrdersForecastCurrent]
		insert into [minmax].[DistributionOrdersForecastCurrent]
		select * from [minmax].[DistributionOrdersForecastNew]

		--fecha pedidos
		truncate table [minmax].[CalculationDateDistributionOrdersCurrent]
		insert into [minmax].[CalculationDateDistributionOrdersCurrent]
		select * from [minmax].[CalculationDateDistributionOrdersNew]

		truncate table [minmax].[SkuNewRestrictionsCurrent]
		insert into [minmax].[SkuNewRestrictionsCurrent]
		select * from [minmax].[SkuNewRestrictionsNew]	
			
		truncate table [minmax].[SugeridoProyeccionPedidosCurrent]
		insert into [minmax].[SugeridoProyeccionPedidosCurrent]
		select * from [minmax].[SugeridoProyeccionPedidosNew]


		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga Lunes ejecutada Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	-- cargar el pricingsuggest
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando Carga en PricingSuggest...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	truncate table minmax.LastPricingSuggest
	
	insert into minmax.LastPricingSuggest
	select * from minmax.PricingSuggest

	truncate table minmax.PricingSuggest


	INSERT INTO [minmax].[PricingSuggest]([SKU],[Store],[Date],[SuggestMin],[SuggestMax], [Days], [Rate], LeadTime, SafetyDays, ServiceLevel)
	select sac.[SKU],sac.[Store],sac.[Date],sac.[SuggestMin],sac.[SuggestMax], sac.[Days], sac.Rate, sac.LeadTime, sac.SafetyDays, sac.ServiceLevel
	from [minmax].[SuggestAllCurrent] sac 
	inner join aux.DailySuggestMix ds on ds.SKU=sac.SKU and ds.Store=sac.Store
	where sac.[Date] = @date
	--where sac.[Date] between @date and dateadd(day, 2, @date)

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga en PricingSuggest Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando replicación para genéricos...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- replicación para genéricos
	select gfd.SKU, sf.Store, sf.[Date], SuggestMin, SuggestMax, [Days], Rate, LeadTime, SafetyDays, ServiceLevel
	into #suggestAux
	from (
		select * 
		from (
			select gfd.GenericFamilyId, Store, [Date]
				, SuggestMin, SuggestMax, [Days], Rate, LeadTime, SafetyDays, ServiceLevel
				, ROW_NUMBER() over (Partition by GenericFamilyId,Store order by SuggestMax desc) R
			from minmax.PricingSuggest ps 
			inner join products.GenericFamilyDetail gfd on gfd.SKU=ps.SKU
		) a where R=1
	) sf
	inner join products.GenericFamilyDetail gfd on gfd.GenericFamilyId=sf.GenericFamilyId
	inner join aux.DailySuggestMix ds on ds.SKU=gfd.SKU and ds.Store=sf.Store

	delete from minmax.PricingSuggest
	where SKU in (select SKU from products.GenericFamilyDetail)

	insert into minmax.PricingSuggest ([SKU],[Store],[Date],[SuggestMin],[SuggestMax], [Days], [Rate], LeadTime, SafetyDays, ServiceLevel)
	select * from #suggestAux

	drop table #suggestAux
	
	-- Aplicar los ajuster por venta
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando minmax.TruncateLowSale...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	declare @auxdate date = dateadd(day,-7,@date)
	Execute minmax.TruncateLowSale @auxdate
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Minmax.TruncateLowSale Listo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	-- ajustar min=max para todos los productos menos los piloto Sugerido Distinto Min Max, farma precio, y marca propia
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes (Min = Max)...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	update p set suggestMin = suggestMin+1,suggestMax = suggestMin+1 
	from minmax.PricingSuggest p
	left join (
	--declare @date date = getdate()
		select mmc.*
		from minmax.MinMaxCombinations mmc
		left join (
			SELECT distinct ls.SKU, ls.Store
			FROM [view].[SuggestRestrictionDetail] srd
			INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
			WHERE @date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (5,6,7,15,13,14)
		) rdun on rdun.SKU= mmc.SKU and rdun.Store=mmc.Store
		where rdun.SKU is null
	) d on d.SKU=p.SKU and d.Store=p.Store
	where d.SKU is null
		and p.Store not in (select store from temp.LocalesFarmaPrecio)
		and p.SKU not in (select SKU from products.Products where TotalRanking in ('A', 'B') and ManufacturerId in (select * from operation.PrivateLabelManufacturers))
		--and p.SKU not in (select SKU from products.GenericFamilyDetail)
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ajuste Listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
	--arreglo para marcas propias
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes para Marcas Propias...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	update p set suggestMin = suggestMax+1,suggestMax = suggestMax+1 
	from minmax.PricingSuggest p 
	where p.SKU in (select SKU from products.Products where TotalRanking in ('A', 'B') and ManufacturerId in (select * from operation.PrivateLabelManufacturers))

	--arreglo para farma precio
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes para Farma Precio...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	update p set suggestMin = suggestMax+1,suggestMax = suggestMax+1 
	from minmax.PricingSuggest p where p.Store in (select store from temp.LocalesFarmaPrecio)

	----arreglo para genéricos
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando ajustes para Genéricos...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--update p set suggestMin = suggestMax+1,suggestMax = suggestMax+1 
	--from minmax.PricingSuggest p where p.SKU in (select SKU from products.GenericFamilyDetail)

	--guardo sugerido antes de aplicar restricciones
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Guardando sugerido antes de aplicar restricciones...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--update
	update pre set [Date]=ps.[Date], SuggestMin=ps.SuggestMin, SuggestMax=ps.SuggestMax, HolidayIncrease=0--select *
	from [minmax].[UnrestrictedPricingSuggest] pre
	inner join minmax.PricingSuggest ps on ps.SKU=pre.SKU and ps.Store=pre.Store
	where ps.[Date]=@date and ps.SuggestMin > 0
	--insert
	insert into [minmax].[UnrestrictedPricingSuggest]
	select ps.[SKU],ps.[Store],ps.[Date],ps.[SuggestMin],ps.[SuggestMax], 0 as HolidayIncrease
	from [minmax].[UnrestrictedPricingSuggest] pre
	right join minmax.PricingSuggest ps on ps.SKU=pre.SKU and ps.Store=pre.Store
	where ps.[Date]=@date and pre.sku is null and ps.SuggestMin > 0

	-- muevo los minmax.suggestForecast
	truncate table minmax.SuggestForecastCorrected
	insert into minmax.SuggestForecastCorrected
	select * from minmax.SuggestForecast
	
	-- Aplicar restricciones
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Ejecutando minmax.SuggestRestriction...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	Execute minmax.SuggestRestriction @date
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'minmax.SuggestRestriction Listo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 


	
	--calcular alzas de feriados cuando ya están aprobadas
	IF (select datepart(WEEKDAY, dateadd(day, 1, @date))) not in (7,1)
	BEGIN
		declare @holidays cursor, @holiday date
		set @holidays = cursor fast_forward for select distinct HolidayDate from minmax.HolidayApprovedSuggests where HolidayDate>=@date order by HolidayDate
		open @holidays 	fetch next from @holidays into @holiday

		while @@FETCH_STATUS = 0
		begin
			exec minmax.HolidayCalculateDailyIncrease @holiday

			fetch next from @holidays into @holiday
		end

		close @holidays deallocate @holidays
	END

	-- Cargar replenishment.nextSuggest
	EXECUTE [minmax].[LoadDailySuggestProposal]  @date

	-- calcular proximas reposiciones -- pendiente

END

