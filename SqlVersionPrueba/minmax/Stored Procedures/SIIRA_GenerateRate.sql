﻿
-- =============================================
-- Author:		Plan Z
-- Create date: 2012-04-19
-- Description:	calcula el rate para un producto


-- =============================================
CREATE PROCEDURE [minmax].[SIIRA_GenerateRate] 
	@beginDate date = NULL,
	@endDate date = NULL, 
	@sku int, 
	@debug bit
AS
BEGIN

	--Si es así hay que mandar a calcula el Fit del sku espejo
	declare @fechacalc date = (select FechaCalculo from aux.UltimoPronostico)

	declare @newSku int=(select case when
		(select mirrorProduct from products.NewProducts where newproduct=@sku) is null
		then @sku else (select mirrorProduct from products.NewProducts where newproduct=@sku)
	end)

	declare @finishdate date = dateadd(day,90,@beginDate)
	declare @hoy date = getdate()
	----------------------------------------
	-- Generate FIT
	----------------------------------------

	truncate table [aux].Fits_Temp

	exec [minmax].[GenerateFits_Base]  @newSku, @hoy, @finishdate
	
	delete from [minmax].[FitsAllNew] where sku=@newSku

	insert into [minmax].[FitsAllNew] (fechacalculo, fechacreacion, SKU, Store ,[date], Fit)
	select @fechacalc,@fechacalc, sku, store, [date], fit
	from [aux].Fits_Temp
	where date between @beginDate and dateadd(day,13,@beginDate)

	----------------------------------------
	-- Generate Rates
	----------------------------------------

	DECLARE @msg VARCHAR(100) 
		SET NOCOUNT ON;


	declare @maxWeek date
	select @maxWeek = (select FechaCalculo from aux.UltimoPronostico)

	declare @skusAux table(SKU int)

	insert into @skusAux
	select SKU 
	from products.Products
	where SKU=@sku 
		and SKU not in (select SKU from products.GenericFamilyDetail)
	union
	select SKU 
	from products.GenericFamilyDetail 
	where GenericFamilyId=(select GenericFamilyId from products.GenericFamilyDetail where SKU=@sku)


	--select @maxWeek = '2012-01-05'
	
	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Inicio '+CONVERT(nvarchar(10),@sku) RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end 
	
	exec [aux].[SIIRA_ClearSuggestAuxData]
	
	--obtengo locales activos para el producto y días a considerar
	insert into aux.RatesForSuggest
	select sku,store,d.[date],0 fitday,0 fitweek,0 shareday,0 dailysale,0 [days],0 rate,0 suggestmin,0 suggestmax, 0 leadtime, 0 servicelevel, 0 safetydays
	from ( select distinct @sku sku,Store from series.LastStock WHERE Suggest>0 and SKU in (select sku from @skusAux)) m,
	(select fecha [date] from forecast.GenericVariables where fecha between @hoy and dateadd(DAY,90,dbo.getmonday(@beginDate))) d

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga de días lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end 
	
	-- paso para productos espejos
	select ISNULL(original.sku, mirror.newsku) SKU, mirror.mirrorsku
		, ISNULL(original.[Date], mirror.[Date]) [date]
		, ISNULL(original.store, mirror.store) store
		, ma.Maximo Fit
	into #vsku
	from (
		select sku,store,[date],fit 
		from [aux].Fits_Temp
		where sku = @sku
	) original
	full join (
	--TODO hacer que bajada a locales baje por laststock
		select ps.store,ps.sku mirrorsku, np.NewProduct newsku,[date],fit*np.Share Fit 
		from products.NewProducts np
		inner join [aux].Fits_Temp ps on ps.sku=np.MirrorProduct
		where np.NewProduct = @sku
	) mirror on original.[Date]=mirror.[Date] and original.sku=mirror.newsku and mirror.store=original.store
	cross apply [function].Maximo(original.Fit, mirror.Fit) ma


	--predicciones bajadas a nivel local
	update v set fitDay = t.fit 
	from aux.RatesForSuggest v
	inner join (
		select ISNULL(original.SKU, mirror.SKU) SKU
			, ISNULL(original.Store, mirror.NewStore) Store
			, ISNULL(original.date, mirror.date) date
			, ma.Maximo Fit
		from (
			select sku,store,[date],fit 
			from #vsku
			where sku = @sku
		) original 
		full join (
			select ssc.SKU, ssc.Store MirrorStore, ns.NewStore, ssc.Fit*ns.Share Fit, ssc.date
			from #vsku   ssc
			right join (
				select NewStore
					, case 
						when SubcategoriaUnificadaId=14 then ns.MirrorStoreDermo
						when DivisionUnificadaId=1 then ns.MirrorStoreFarma
						when DivisionUnificadaId=2 then ns.MirrorStoreConsumo 
						end MirrorStore
					, case 
						when SubcategoriaUnificadaId=14 then ns.ShareDermo
						when DivisionUnificadaId=1 then ns.ShareFarma
						when DivisionUnificadaId=2 then ns.ShareConsumo
					end Share
					, BeginDate
				from products.Products p
				, stores.NewStores2 ns
				where SKU=@sku
			) ns on ns.MirrorStore=ssc.Store
		) mirror on original.SKU=mirror.SKU and original.Store=mirror.NewStore and original.date=mirror.Date
		cross apply [function].Maximo(original.fit, mirror.fit) ma
	) t
	on t.sku = v.sku and t.Store = v.store and t.[date] = v.[date]

	drop table #vsku

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de pronósticos por combinación lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end 
	
	--pega información del share dia, pronósticos a nivel dia-local
	declare @auxSKU int = 
	( --consideracion producto espejo
		select isnull(MirrorProduct, @sku) SKU
		from products.NewProducts
		where NewProduct=@sku
	) 

	update v set shareday= (case when wk.fitweek <=0 then 0 else v.fitday/wk.fitweek end) , fitWeek = wk.fitweek
	from aux.RatesForSuggest v
	inner join(
		select sku,store,dbo.getmonday(date) semana,sum(fitday) fitweek
		from aux.RatesForSuggest
		group by sku,store,dbo.getmonday(date)
	) wk on wk.store=v.store and wk.sku=v.sku and wk.semana=dbo.getmonday(v.date)
	
	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de pronósticos diarios lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	-- venta real diaria
	update v set dailySale = [function].inlineMax(AvgLargo, AvgMes)
	from aux.RatesForSuggest v
	inner join minmax.DailySales ds on ds.SKU=v.sku and ds.Store=v.store

	declare @auxDate date
	select @auxDate = max([date]) from reports.MetricasStocks
	set @auxSKU  = 
			(select SKU
			from reports.metricasStocks
			where date > dateadd(day, -30, @auxDate) and SKU=@sku
			group by SKU
			having ROUND(SUM(cast(LocalesQuebrados*1.0/(Locales*1.0) as float)),0)/30.0 >= 0.5)


	-- solo casos producto no quebrado
	IF(@auxSKU is null)
	BEGIN
		--tasa diaria no puede ser más que 4 veces venta real
		-- guardo casos para análisis futuros.
		insert into [minmax].[TruncateRatesHistory]
		select v.[Date], v.SKU, v.Store, v.dailySale, v.fitDay, v.fitWeek, v.shareday
		from aux.RatesForSuggest v
		where fitDay > dailySale*4.0
			and dailySale*4.0> 0.5

		-- corrigo casos
		update v set fitDay = dailySale*4.0 
		from aux.RatesForSuggest v
		where fitDay > dailySale*4.0
			and dailySale*4.0> 0.5

		if @debug = 1 begin
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Corte por ventas listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		end 

	END

END

