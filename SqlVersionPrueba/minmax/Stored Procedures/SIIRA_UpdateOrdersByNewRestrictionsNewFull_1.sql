﻿
-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-06-18
-- Description:	Ejecuta el ajuste de pedido para todos los SKU's del día Domingo


-- =============================================
create PROCEDURE [minmax].[SIIRA_UpdateOrdersByNewRestrictionsNewFull] 

AS
BEGIN

	declare @sku int


	declare cursor_skus cursor fast_forward 
	for

	select s.sku FROM minmax.[SkuNewRestrictionsNew] s
		inner join (select distinct sku from minmax.[DistributionOrdersForecastNew]) a on a.sku = s.sku	



	open cursor_skus
	fetch next from cursor_skus into @sku

	while @@fetch_status = 0
	begin
	

			declare @fecha_ultimo_ajuste datetime = ISNULL((select Fecha_calculo FROM minmax.[CalculationDateDistributionOrdersNew] WHERE SKU = @sku), '2018-01-01')
			declare @fecha_inicio date = DATEADD(day, 1,(SELECT MIN(date) FROM minmax.[DistributionOrdersForecastNew] WHERE sku = @sku))
			declare @fecha_fin date =  (SELECT MAX(date) FROM minmax.[DistributionOrdersForecastNew] WHERE sku = @sku )


			exec [minmax].[SIIRA_UpdateOrdersByNewRestrictionsNew]   @sku, @fecha_inicio, @fecha_fin, @fecha_ultimo_ajuste 

			fetch next from cursor_skus into @sku

	end

	close cursor_skus
	deallocate cursor_skus

END