﻿
-- =============================================
-- Author:		Daniela Albornoz	
-- Create date: 2015-04-09
-- Description:	Obtiene datos de tasas para cálculo de sugerido MinMax
-- =============================================
CREATE PROCEDURE [minmax].[UpdateMinMaxData]
	-- Add the parameters for the stored procedure here
	@beginDate date
	,@enddate date
AS
BEGIN
	
	truncate table minmax.MinMaxAuxData

	insert into minmax.MinMaxAuxData ([SKU],[Store],[Date],[Rate],[RateNext],[SLMax],[SLMin])
	select n1.SKU, n1.Store, n1.[Date], n1.Rate
		, isnull(LEAD(n1.Rate,01) over (partition by n1.SKU, n1.Store order by [date]),0) RateNext
		, (cast(ServiceLevelDesc as float)-2)/100 SLMax
		, (cast(ServiceLevelDesc as float)-12)/100 SLMin
	from 
	(	--PILOTO Min Max
		select sku, store 
		from minmax.MinMaxCombinations 
	) t
	inner join minmax.SuggestAllNew n1 on t.SKU=n1.SKU and t.Store=n1.Store
	inner join [minmax].[SuggestParameters] s on s.SKU=n1.SKU
	where n1.[Days]>0
		and n1.Rate>0
		and n1.[Date] between @beginDate and @enddate

END

