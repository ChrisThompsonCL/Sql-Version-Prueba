﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2015-10-13
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [minmax].[GenerateDailySales]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    declare @date date = (select top 1 [date] from series.LastSale)

	--truncate table minmax.DailySales

	--insert into minmax.DailySales (SKU, Store, SaleDate, Quantity90, Quantity30)
	--select SKU, Store
	--	, @date SaleDate
	--	, cast(SUM(Quantity)  / 90.0 as float) Quantity90
	--	, cast(SUM(case when [Date] > dateadd(DAY, -30, @date) then Quantity else 0 end)  / 30.0 as float) Quantity30
	--from series.sales
	--where [date] > dateadd(DAY, -90, @date)
	--group by SKU, Store

	truncate table minmax.DailySales
	
	insert into minmax.DailySales
	select SKU, rs.Store
		, @date SaleDate
		, case 
			when count(*)>1 
			then SUM(Quantity)  /cast(count(*)-1 as float) 
			else null end	
		AvgLargo
		, count(*)-1 DiasLargo
		, SUM(case when Stock <= 0 then 1 else 0 end) DiasQuebradoLargo
		, case 
			when SUM(case when [Date] > dateadd(DAY, -30, @date) then 1 else 0 end)>1
			then SUM(case when [Date] > dateadd(DAY, -30, @date) then Quantity else 0 end)  / cast(SUM(case when [Date] > dateadd(DAY, -30, @date) then 1 else 0 end)-1 as float) 
			else null end
		AvgMes
		, SUM(case when [Date] > dateadd(DAY, -30, @date) then 1 else 0 end)-1 DiasMes
		, SUM(case when [Date] > dateadd(DAY, -30, @date) and Stock <= 0 then 1 else 0 end) DiasQuebradoMes
		, case 
			when SUM(case when [Date] > dateadd(WEEK, -1, @date) then 1 else 0 end)  > 1
			then SUM(case when [Date] > dateadd(WEEK, -1, @date) then Quantity else 0 end)  / cast(SUM(case when [Date] > dateadd(WEEK, -1, @date) then 1 else 0 end)-1 as float) 
			else null end
		AvgSemana
		, SUM(case when [Date] > dateadd(WEEK, -1, @date) then 1 else 0 end)-1 DiasSemana
		, SUM(case when [Date] > dateadd(WEEK, -1, @date) and Stock <= 0 then 1 else 0 end) DiasQuebradoSemana
		, STDEV(case when Quantity >0 then Quantity else null end) DesvDia
		, AVG(cast(case when Quantity>0 then Quantity else NULL end as float)) VentaDia
		, SUM(case when Quantity>0 then 1 else 0 end) DiasVenta
		, MAX(Quantity) MaxVenta
	from reports.ReportStores rs
	inner join stores.Stores s on s.Store=rs.Store
	where ( ([Shift] = 'LUNES-VIERNES' and  DATEPART(WEEKDAY, [date]) not in (7,1) ) OR ([Shift] = 'LUNES-SABADO' and  DATEPART(WEEKDAY, [date]) not in (1) ) OR (isnull([Shift],'LUNES-DOMINGO')='LUNES-DOMINGO') )
		and Quantity >= 0 
		and (Stock > 0 OR Quantity>0)
	group by SKU, rs.Store
	--having SUM(case when [Date] > dateadd(WEEK, -1, @date) then 1 else 0 end)-1>0

END
