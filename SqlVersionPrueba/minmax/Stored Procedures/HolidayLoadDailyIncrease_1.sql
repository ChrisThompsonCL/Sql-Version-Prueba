﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-05-07
-- Description:	Carga de alza en propuesta de sugeridos
-- =============================================
CREATE PROCEDURE [minmax].[HolidayLoadDailyIncrease]
	-- Add the parameters for the stored procedure here
	@holidayDate date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @date date = cast(GETDATE() as date)
	DECLARE @msg VARCHAR(100)

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de la reposición...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT

	--actualizar lo que ya esta en el replenishment.NextSuggest
	update s set [Min] = h.MinHolidays,[max]=h.MaxHolidays --select *
		, Rate=null, [Days]=null, LeadTime=null, SafetyDays=null, ServiceLevel=null
	from replenishment.NextSuggest s 
	inner join minmax.HolidaySuggestNew h on s.SKU = h.SKU and s.Store = h.Store 
	where HolidayDate = @holidayDate and s.[Date] = @date and ( s.[Min]<h.MinHolidays or s.[Max]<h.MaxHolidays)
		and @date between BeginDate and EndDate

	--los que no estan hay que insertarlos
	INSERT INTO [replenishment].[NextSuggest] ([SKU],[Store],[Max],[Min],[Date])
	select h.SKU,h.Store,h.MaxHolidays,h.MinHolidays,@date
	from minmax.HolidaySuggestNew h
	left outer join (select * from  replenishment.NextSuggest where [Date] = @date) s on s.SKU = h.SKU and s.Store = h.Store
	inner join (
		select r.Store,p.SKU from minmax.Replenishment r
		inner join products.Products p on r.LogisticAreaId = p.LogisticAreaId
		where [Days]>0 and [Date] = @date
	) r on r.sku = h.sku and r.store = h.store
	inner join series.LastStock ls on ls.SKU=h.SKU and ls.Store=h.Store
	inner join minmax.pricingsuggest ps on ps.SKU=h.SKU and ps.Store=h.Store and ps.[Date] =@date
	where s.SKU is null and HolidayDate = @holidayDate 
		--and (ls.[min]<h.MinHolidays or ls.[max]<h.MaxHolidays) 
		and (ps.SuggestMin<h.MinHolidays or ps.SuggestMax<h.MaxHolidays)
		and not (ls.[min]=h.MinHolidays or ls.[max]=h.MaxHolidays) 
		and @date between BeginDate and EndDate


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Resposición actualizada' RAISERROR (@msg, 10, 0 ) WITH NOWAIT

END
