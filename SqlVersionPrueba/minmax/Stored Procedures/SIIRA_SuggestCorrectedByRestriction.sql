﻿-- =============================================
-- Innovación
-- 2018-04-03
-- Aplica restricciones
-- =============================================
CREATE PROCEDURE [minmax].[SIIRA_SuggestCorrectedByRestriction]
	@sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime, 
	@fecha_fin datetime,
	@ajuste_new bit  
AS
BEGIN
	SET NOCOUNT ON;

	create table #Restricc1(ProductId int, StoreId int, StoreType int, Detail int, BeginDate date, EndDate  date, InsertionDate datetime,SuggestRestrictionTypeId int)
	create index #ix2 on #Restricc1(ProductId, StoreType, StoreId)

	create table #base (SKU int, Store int, Suggest int, Fecha date)
	create index #ix1 on #base(SKU, Store, Fecha)

	SELECT fecha [Date]
	into #fechas
	FROM SALCOBRAND.forecast.GenericVariables 
	WHERE fecha between @fecha_inicio and @fecha_fin



	if(@sku is null)
	begin
		insert into #Restricc1
		select ProductID, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate, SuggestRestrictionTypeID
		from [view].[SuggestRestrictionDetail]
		where BeginDate>=@fecha_creacion or @fecha_creacion is null

		insert into #base
		select a.SKU, a.Store, a.Suggest,Fechas.Date fecha 
		from series.LastStock a
		cross join #fechas Fechas
	end
	else
	begin
		insert into #Restricc1
		select ProductID, StoreID, StoreType, Detail, BeginDate, EndDate, InsertionDate, SuggestRestrictionTypeID
		from [view].[SuggestRestrictionDetail]
		where (productid=@sku) and (BeginDate>=@fecha_creacion or @fecha_creacion is null)

		insert into #base
		select a.SKU, a.Store, a.Suggest,Fechas.Date fecha 
		from series.LastStock a
		cross join #fechas Fechas
		where (sku=@sku)
	end		

	SELECT ls.SKU,ls.Store,ls.fecha, max(ls.Suggest) Suggest, max(minimo.Minimo) Minimo, max(fijo.Fijo) Fijo, max(planograma.Planograma) Planograma, Max(dun.MedioDUN) MedioDUN, min(Maximo)Maximo, max(Cajatira)CajaTira, max(FijoEstricto)FijoEstricto
	into #prueba
	FROM #base ls
	LEFT JOIN (
		/*MINIMO*/
		SELECT a.SKU, a.Store, a.Fecha, Max(Minimo)Minimo
		FROM (
			SELECT ls.sku,ls.Store,ls.fecha, Detail Minimo
			FROM #Restricc1 srd
			INNER JOIN #base ls on ls.SKU = srd.ProductId and (srd.StoreType=3)
			WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (3,8,12)
			union all
			SELECT ls.sku,ls.Store,ls.fecha, Detail Minimo
			FROM #Restricc1 srd
			INNER JOIN #base ls on ls.SKU = srd.ProductId and (srd.StoreType=0 and srd.StoreID=ls.Store) 
			WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (4)
		) a
		group by a.SKU, a.Store, a.Fecha
	) minimo on minimo.SKU=ls.SKU and minimo.Store=ls.Store and minimo.fecha=ls.fecha
	LEFT JOIN (
		/*FIJO*/
		SELECt SKU,Store,date, Fijo
		FROM (
			SELECT srd.ProductID SKU,Fechas.date,srd.StoreID Store
			, CASE WHEN srd.SuggestRestrictionTypeID = 2 THEN srd.Detail ELSE NULL END Fijo
			, ROW_NUMBER() OVER (partition BY srd.ProductID,srd.StoreID,Fechas.Date ORDER BY cast(InsertionDate AS smalldatetime) DESC, Detail DESC) R
			FROM #Restricc1 srd
			cross join #fechas Fechas
			WHERE srd.SuggestRestrictionTypeID IN (2) and Fechas.Date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
		) fijo WHERE R=1
	) fijo ON fijo.SKU=ls.SKU and fijo.Store=ls.Store and fijo.Date=ls.fecha
	LEFT JOIN (
		/*PLANOGRAMA*/
		SELECT srd.ProductID SKU,srd.StoreID Store,Fechas.Date
			, MAX(CASE WHEN srd.SuggestRestrictionTypeID IN (9, 10, 11) THEN srd.Detail ELSE NULL END) Planograma
		FROM #Restricc1 srd
		cross join #fechas Fechas
		where Fechas.Date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
			and srd.SuggestRestrictionTypeID in (9,10,11)
		group by srd.ProductID ,srd.StoreID,Fechas.Date
	) planograma on planograma.SKU=ls.SKU and planograma.Store=ls.Store and planograma.date=ls.fecha
	LEFT JOIN (
		/*DUN*/
		SELECT SKU, Store, Fecha, MedioDUN
		FROM (
			SELECT * , ROW_NUMBER() over (partition by a.SKU,a.Store,a.fecha order by cast(InsertionDate as smalldatetime) desc, MedioDUN desc) R
			FROM (
				SELECT SKU,Store,fecha, MedioDUN, InsertionDate
				FROM (
					SELECT ls.SKU,ls.Store,ls.fecha
						, case when srd.SuggestRestrictionTypeID in (5) then CEILING(Detail*1.0/2.0)+1 else null end MedioDUN
						, InsertionDate
					FROM #Restricc1 srd
					INNER JOIN #base ls on ls.SKU = srd.ProductId and  (srd.StoreType=3)
					WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') and  srd.SuggestRestrictionTypeID in (5)
				) a
				union all 
				SELECT SKU,Store,fecha, MedioDUN, InsertionDate
				FROM (
					SELECT ls.SKU,ls.Store,ls.fecha
						, case when srd.SuggestRestrictionTypeID in (6) then CEILING(Detail*1.0/2.0)+1 else null end MedioDUN
						, InsertionDate
					FROM #Restricc1 srd
					INNER JOIN #base ls on ls.SKU = srd.ProductId and (srd.StoreType=0 and srd.StoreID=ls.Store)
					WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31') and  srd.SuggestRestrictionTypeID in (6)
				) b
			) a 
		) a WHERE R=1
	) dun on dun.SKU=ls.SKU and dun.Store=ls.Store and dun.fecha=ls.fecha
	LEFT JOIN (
		/*MAXIMO*/
		SELECT a.SKU, a.Store, a.Fecha, MIN(a.Maximo) Maximo
		FROM (
			SELECT ls.SKU,ls.Store,ls.fecha ,srd.Detail Maximo
			FROM #Restricc1 srd
			INNER JOIN #base ls on ls.SKU = srd.ProductId and (srd.StoreType=0 and srd.StoreID=ls.Store)
			WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (14)
			union all
			SELECT ls.SKU,ls.Store,ls.fecha ,srd.Detail Maximo
			FROM #Restricc1 srd
			INNER JOIN #base ls on ls.SKU = srd.ProductId and (srd.StoreType=3)
			WHERE ls.fecha BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (13)
		) a GROUP BY a.SKU, a.Store, a.Fecha
	) maximo on maximo.Store=ls.Store and maximo.fecha=ls.fecha
	LEFT JOIN (
		/*CajaTira*/
			SELECT srd.ProductID sku,Fechas.Date
				, max(Detail) CajaTira
			FROM #Restricc1 srd
			cross join #fechas Fechas
			WHERE Fechas.Date  BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (15)
			group by srd.ProductID,Fechas.Date
	) cajatira on cajatira.SKU=ls.SKU and cajatira.Date=ls.fecha
	LEFT JOIN (
		/*FIJO ESTRICTO*/
		SELECt SKU,Store,Date, FijoEstricto
		FROM (
			SELECT srd.ProductID SKU,Fechas.date,srd.StoreID Store
					, case when srd.SuggestRestrictionTypeID=16 then srd.Detail else null end FijoEstricto
					, ROW_NUMBER() over (partition by srd.ProductID,srd.StoreID,Fechas.Date order by cast(InsertionDate as smalldatetime) desc, Detail desc) R
			FROM #Restricc1 srd
			cross join #fechas Fechas
			where Fechas.date BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (16)
		) FijoEstricto WHERE R=1
	) FijoEstricto on FijoEstricto.SKU=ls.SKU and FijoEstricto.Store=ls.Store and FijoEstricto.Date=ls.fecha
	--WHERE (minimo.Minimo is not null OR fijo.Fijo is not null OR planograma.Planograma is not null or dun.MedioDUN is not null or Maximo is not null or CajaTira is not null or FijoEstricto is not null)
	group by ls.SKU,ls.Store,ls.fecha
		
	IF(@sku IS NOT NULL)
		BEGIN

			IF(@ajuste_new = 1)
				BEGIN
					truncate table aux.SugeridoConRestriccionesNew
					--where sku=@sku

					INSERT INTO aux.SugeridoConRestriccionesNew
						SELECT SKU,Store,fecha,[Minimo],[Fijo],[Planograma] ,[MedioDUN],[Maximo],[CajaTira],[FijoEstricto]
						FROM #prueba 
				END
			ELSE
				BEGIN
					truncate table aux.SugeridoConRestriccionescurrent
					--where sku=@sku

					INSERT INTO aux.SugeridoConRestriccionescurrent
						SELECT SKU,Store,fecha,[Minimo],[Fijo],[Planograma] ,[MedioDUN],[Maximo],[CajaTira],[FijoEstricto]
						FROM #prueba 
				END



		END
	ELSE
		BEGIN
				SELECT * 
				FROM #prueba 
				WHERE (Minimo is not null OR Fijo is not null OR Planograma is not null or MedioDUN is not null or Maximo is not null or CajaTira is not null or FijoEstricto is not null)
		END
				
			
			DROP TABLE #prueba
			DROP TABLE #base
			DROP TABLE #fechas
			DROP TABLE #Restricc1
						 					
	


	
END