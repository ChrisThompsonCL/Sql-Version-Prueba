﻿-- =============================================
-- Author:		Gabriel Espinoza, Juam M. Hernández
-- Create date: 2017-10-20
-- Description:	Contiene las reglas del sugerido ágil
-- =============================================
CREATE PROCEDURE [minmax].[CalcularSugeridoAgil]
	
AS
BEGIN
	SET NOCOUNT ON;

	

	/********************************************************************************************/
	/*																							*/
	/*			Paso 1. Detectar oportunidades, n alertas generarán n soluciones				*/
	/*				a. Oportunidades Mega Recientes												*/
	/*																							*/
	/********************************************************************************************/

	IF OBJECT_ID('tempdb.dbo.#anomalias', 'U') IS NOT NULL
	  DROP TABLE #anomalias; 
	
	--declare @noPromoActiva TABLE (
	--	sku int
	--);
	--insert into @noPromoActiva
	--select distinct a.SKU from 
	--test.SugeridoAgil a 
	--where SKU not in (
	--			SELECT SKU 
	--			FROM [view].[PromotionView] b 
	--			where  PromocionesActivas >= 1 and b.Fecha  >  DATEADD (day, -7, a.Fecha) 
	--	)
	--and a.NuevoSugerido is not null


	select 
		ds.SKU, ds.Store
		, DiasQuebradoMes, DiasQuebradoSemana
		, AvgLargo, AvgMes,AvgSemana
		, VentaDia, DesvDia, MaxVenta
		, DiasVenta, Quantity lastQ
		, Suggest,((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1)) [vta-1]
		, DiasSemana, DiasMes, DiasLargo
		, ( case
			when
			/*alerta : última venta no está cubierta por el sugerido y no es venta industrial*/
			(isnull(lsa.Quantity,0) >= Suggest and DiasVenta>4 and Suggest<Quantity*7 and not ( isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) and isnull(lsa.Quantity,0)>10))
				then 'Venta1'
			when
			/*alerta : última venta mayor a venta max y no es venta industrial*/
				(
					isnull(lsa.Quantity,0) >= MaxVenta 
					and DesvDia>0 
					and DiasVenta>4 
					and Suggest<((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))*7 
					and not ( 
						isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) 
						and isnull(lsa.Quantity,0)>10
					)
				)	
				then 'Venta2'
			when 
			/*ALERTA: si última venta está alejada del promedio diario y no es venta industrial*/
				(
					isnull(lsa.Quantity,0) > floor(((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))+2*DesvDia) 
					and DiasVenta>4 
					and Suggest<((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))*7 
					and not ( 
						isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) 
						and isnull(lsa.Quantity,0)>10
					)
				)	
				then 'Venta3'
			when  
			/*alerta : vende y el sugerido queda corto*/
			(DiasVenta >  floor(DiasLargo*0.10) and ceiling(((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))+DesvDia) >= Suggest and DiasQuebradoMes >  0)	
				then 'Modelo4'
			when
			/*alerta : sugerido "bajo" y quiebra tanto como vende */
			(ceiling((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1)+2*DesvDia)>=Suggest and DiasQuebradoLargo >= ceiling(DiasVenta*0.5) and AvgMes>=(DiasMes/7.0) and DiasSemana>0)
				then 'Modelo5'
			when
			/*alerta : sugerido uno con venta potencial por múltiplos*/
			((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1)>1 and Suggest=1 and DiasSemana>2 and DiasVenta>(DiasLargo*0.3) and AvgSemana>0)
				then 'Modelo6'
			--when
			--/*alerta 6: que shusha*/
			--(round((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1),2)=round(VentaDia, 2) and DiasVenta>(DiasLargo*0.3) and avgsemana>0 and Suggest)
			end
		) TipoAlerta
	into #anomalias
	from minmax.DailySales ds 
	inner join aux.DailySuggestMix dsm on dsm.SKU=ds.SKU and dsm.Store=ds.Store
	inner join series.LastStock ls on ls.SKU=ds.SKU and ls.Store=ds.Store
	left join series.LastSale lsa on lsa.SKU=ds.SKU and lsa.Store=ds.Store
	where DiasSemana>=4  --apariciones en el stock de la última semana
		and DiasMes>=10 --apariciones en el stock del último mes
		and DiasVenta > 2 --veces que vendió en el periodo del reportstores
		and ceiling(AvgMes) > 0 -- que venda algo el último mes
		and ds.Store <> 565
		and (
			/*alerta : última venta no está cubierta por el sugerido y no es venta industrial*/
			(isnull(lsa.Quantity,0) >= Suggest and DiasVenta>4 and not ( isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) and isnull(lsa.Quantity,0)>10))
			OR
			/*alerta : última venta mayor a venta max y no es venta industrial*/
			(isnull(lsa.Quantity,0) >= MaxVenta and DesvDia>0 and DiasVenta>4 and Suggest<((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))*7 and not ( isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) and isnull(lsa.Quantity,0)>10))	
			OR
			/*alerta : última venta mayor lo "normal" y no es venta industrial*/
			(isnull(lsa.Quantity,0) > floor(((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))+2*DesvDia) and DiasVenta>4 and Suggest<((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))*7 and not ( isnull(lsa.Quantity,0) > ceiling(VentaDia+(case when DesvDia < VentaDia then VentaDia else DesvDia end)*5) and isnull(lsa.Quantity,0)>10))	
			OR
			/*alerta : vende y el sugerido queda corto*/
			(DiasVenta >  floor(DiasLargo*0.10) and ceiling(((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1))+DesvDia) >= Suggest and DiasQuebradoMes > 0)	
			OR
			/*alerta : sugerido "bajo" y quiebra tanto como vende */
			(ceiling((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1)+2*DesvDia)>=Suggest and DiasQuebradoLargo >= ceiling(DiasVenta*0.5) and AvgMes>=(DiasMes/7.0) and DiasSemana>0)
			OR
			/*alerta : sugerido uno con venta potencial por múltiplos*/
			((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1)>1 and Suggest=1 and DiasSemana>2 and DiasVenta>(DiasLargo*0.3) and AvgSemana>0)
			--OR
			--/*alerta : que shusha*/
			--(round((VentaDia*DiasVenta-MaxVenta)/(DiasVenta-1),2)=round(VentaDia, 2) and DiasVenta>(DiasLargo*0.3) and avgsemana>0 and Suggest)
		)
	
	order by DiasVenta desc

	

	/********************************************************************************************/
	/*																							*/
	/*			Paso 2. Solucionar alertas generadas											*/
	/*																							*/
	/********************************************************************************************/

	select CAST(GETDATE() AS DATE) Fecha, a.*, m.MaxDays, r.ReposicionesSemanales
		, CASE
			WHEN a.TipoAlerta LIKE 'Venta%' THEN
				case 
					when r.ReposicionesSemanales = 5 THEN --repone todos los días
						case
							when a.lastQ > a.Suggest THEN a.lastQ
							when a.lastQ = a.Suggest AND a.DiasQuebradoSemana >= 4 THEN CEILING(a.Suggest * 1.2)
							when a.lastQ = a.Suggest AND a.DiasQuebradoMes >= 10 THEN CEILING(a.Suggest * 1.2)
						end
					when r.ReposicionesSemanales < 5 THEN  --no repone todos los días
						case
							when (a.lastQ+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+1) as float) * m.MaxDays > a.Suggest THEN CEILING((a.lastQ+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+1) as float) * m.MaxDays)
							when (a.lastQ+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+1) as float) * m.MaxDays = a.Suggest THEN CEILING((a.lastQ+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+1) as float) * m.MaxDays * 1.2)
						end
				END 
				WHEN a.TipoAlerta LIKE 'Modelo%' THEN
					CASE 
						when a.DiasQuebradoSemana > Floor(a.DiasSemana*0.4) AND a.Suggest < CEILING((isnull(a.lastQ, 0)+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+(case when a.lastQ is not null then 1 else 0 end)) as float) * m.MaxDays)  then CEILING((isnull(a.lastQ, 0)+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+(case when a.lastQ is not null then 1 else 0 end)) as float) * m.MaxDays)
						when a.Suggest < CEILING((isnull(a.lastQ, 0)+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+(case when a.lastQ is not null then 1 else 0 end)) as float) * m.MaxDays)  then CEILING((isnull(a.lastQ, 0)+a.AvgSemana*a.DiasSemana)/cast((a.DiasSemana+(case when a.lastQ is not null then 1 else 0 end)) as float) * m.MaxDays)
						when a.DiasQuebradoSemana > 0 THEN CEILING(a.Suggest * 1.2)
					END
			
			END NuevoSugerido
	INTO #sugeridoAgil
	from #anomalias a
	inner join aux.DailySuggestMix dsm on dsm.SKU=a.SKU and dsm.Store=a.Store
	inner join series.LastStock ls on ls.SKU=a.SKU and ls.Store=a.Store
	inner join (
		select SKU, Store, MAX([Days])MaxDays
		from minmax.SuggestAllCurrent
		group by SKU, Store
	) m ON a.Store = m.Store AND m.SKU = a.SKU
	inner join products.Products p ON a.SKU = p.SKU
	inner join (
		--select Store, r.LogisticAreaId, COUNT(*) ReposicionesSemanales
		--from replenishment.[Routes] r 
		--where LogisticAreaId not in ('Bref.Ref','Bcon.Ref','Bcon.Con')
		--GROUP BY Store, r.LogisticAreaId

		--este cambio lo hizo la dani, está mal que use la ruta directo
		select Store, r.LogisticAreaId, COUNT(distinct DATEPART(weekday, [date])) ReposicionesSemanales
		from minmax.Replenishment r 
		where LogisticAreaId not in ('Bref.Ref','Bcon.Ref','Bcon.Con')
			and [Days]>0
		GROUP BY Store, r.LogisticAreaId
	) r ON p.LogisticAreaId = r.LogisticAreaId AND r.Store = a.Store
	left join series.LastSale lsa on lsa.SKU=a.SKU and lsa.Store=a.Store
	where a.SKU not in (
			SELECT DISTINCT SKU 
			FROM [view].[PromotionView] b 
			where  PromocionesActivas >= 1 and b.Fecha  >  DATEADD (day, -7, CAST(GETDATE() AS DATE)) 
			--AND DebeCalcularSugerido = 0
		)
	--ORDER BY (a.AvgSemana*m.MaxDays) - a.Suggest desc


	-- De las combinaciones que no se les asignó un nuevo sugerido y corresponden a alertas de venta, protegemos para cumplir la cantidad máxima de días
	UPDATE a 
		SET NuevoSugerido = CEILING((((AvgSemana * DiasSemana) + lastQ) / (DiasSemana + 1)) * MaxDays)
	FROM #sugeridoAgil a
	where 
		TipoAlerta LIKE 'Venta%' 
		and NuevoSugerido is null 
		AND not (ReposicionesSemanales = 5 AND MaxDays = 4)
		and CEILING((((AvgSemana * DiasSemana) + lastQ) / (DiasSemana + 1)) * MaxDays) > Suggest



	/********************************************************************************************/
	/*																							*/
	/*			Paso 4. Evaluar tramos  */
	/*																							*/
	/********************************************************************************************/
	
	/*FlagTramo 
		Combinaciones que quedan con sugerido ágil sin modificar = 0
		Sugerido Actual entre 1 y 10 ademas Sugerido ágil > 20 flag= 1
		Sugerido actual > 10, además si la diferencia entre sugerido ágil  y el actual  > sugerido actual, acoto a que el sugerido crezca en 100%  flag = 2
	*/

	/* creo los el nuevo sugerido por tramo mas los flags */
	SELECT
		   Fecha 
		  ,SKU
		  ,Store
		  ,DiasQuebradoMes
		  ,DiasQuebradoSemana
		  ,AvgLargo
		  ,AvgMes
		  ,AvgSemana
		  ,VentaDia
		  ,DesvDia
		  ,MaxVenta
		  ,DiasVenta
		  ,lastQ
		  ,Suggest
		  ,[vta-1]
		  ,DiasSemana
		  ,DiasMes
		  ,DiasLargo
		  ,TipoAlerta
		  ,MaxDays
		  ,ReposicionesSemanales
		  ,CASE 
			WHEN NuevoSugerido > 20 
				THEN 
				 CASE WHEN Suggest<10 THEN 20
					ELSE
					   CASE WHEN NuevoSugerido - Suggest > Suggest THEN Suggest * 2
							ELSE NuevoSugerido END
					   END
				ELSE
				NuevoSugerido
				END NuevoSugerido
			,CASE 
			WHEN NuevoSugerido > 20 
				THEN 
				 CASE WHEN Suggest<10 THEN 1
					ELSE
					   CASE WHEN NuevoSugerido - Suggest > Suggest THEN 2
							ELSE 0 END
					   END
				ELSE
				0
				END FlagTramo	  
    INTO #SugeridoConCalculoTramo
	FROM #sugeridoAgil

	/* reviso si el nuevo sugerido por tramo tiene restricciones no menores a 15 días, con la finalidad de pueda aplicar una nueva restricción
	 a esta combinación 
	*/
	select Fecha 
		  ,SKU
		  ,Store
		  ,DiasQuebradoMes
		  ,DiasQuebradoSemana
		  ,AvgLargo
		  ,AvgMes
		  ,AvgSemana
		  ,VentaDia
		  ,DesvDia
		  ,MaxVenta
		  ,DiasVenta
		  ,lastQ
		  ,Suggest
		  ,[vta-1]
		  ,DiasSemana
		  ,DiasMes
		  ,DiasLargo
		  ,TipoAlerta
		  ,MaxDays
		  ,ReposicionesSemanales
		  ,NuevoSugerido
	into #tramo 
	from #SugeridoConCalculoTramo ct
	inner join br.SuggestRestrictionDetail sr on sr.StoreID = ct.Store and sr.ProductID = ct.SKU
	where FlagTramo in (1,2)
	and SuggestRestrictionFileID = 11534
	and (select MAX (BeginDate) from  br.SuggestRestrictionDetail s
			where s.StoreID = ct.Store and s.ProductID = ct.SKU
			and SuggestRestrictionFileID= 11534) <= DATEADD(DAY, -15, GETDATE())

	


	/********************************************************************************************/
	/*																							*/
	/*			Paso 5. Guardar en tabla para posterior evaluación								*/
	/*																							*/
	/********************************************************************************************/



	delete from minmax.SugeridoAgil
	where Fecha = CAST(GETDATE() AS date)

	INSERT INTO minmax.SugeridoAgil
	select *, 0 as TieneRestriccion from (
		SELECT a.Fecha 
			  ,a.SKU
			  ,a.Store
			  ,a.DiasQuebradoMes
			  ,a.DiasQuebradoSemana
			  ,a.AvgLargo
			  ,a.AvgMes
			  ,a.AvgSemana
			  ,a.VentaDia
			  ,a.DesvDia
			  ,a.MaxVenta
			  ,a.DiasVenta
			  ,a.lastQ
			  ,a.Suggest
			  ,a.[vta-1]
			  ,a.DiasSemana
			  ,a.DiasMes
			  ,a.DiasLargo
			  ,a.TipoAlerta
			  ,a.MaxDays
			  ,a.ReposicionesSemanales
			  ,a.NuevoSugerido
		FROM #SugeridoConCalculoTramo a
		union
		SELECT * 
		FROM #tramo 
		
		)cc
	
	DROP TABLE #sugeridoAgil
	DROP TABLE #anomalias
	DROP TABLE #SugeridoConCalculoTramo
	DROP TABLE #tramo
END
