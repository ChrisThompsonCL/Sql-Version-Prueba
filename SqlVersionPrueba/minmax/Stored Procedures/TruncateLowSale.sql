﻿

CREATE PROCEDURE [minmax].[TruncateLowSale] 
	@currentdate smalldatetime
AS
BEGIN

delete from minmax.TruncateLowSaleHistory where [DateKey] = @currentdate

--drop table #weekSale
CREATE TABLE #weekSale (sku int,store int, quantity float)
CREATE INDEX AW_Index ON #weekSale (sku) 
CREATE INDEX AW2_Index ON #weekSale (store) 

-- insertar todas las combinaciones que enviaremos 
insert into #weekSale (sku,store)
select distinct sku,store from minmax.PricingSuggest

--borrar genéricos
delete from #weekSale
where SKU in (select SKU from products.GenericFamilyDetail)

update #weekSale
-- máximo entre la venta semanal promedio de los últimas 12 semanas y de las siguientes 12 semanas del año anterior
set quantity = T.Quantity 
from #weekSale w 
inner join (
	select sku, store , [function].inlineMax(AvgLargo, AvgMes)*7.0 Quantity
	from minmax.DailySales
	where SKU not in (select newproduct from products.NewProducts)
	union
	select np.NewProduct, store , [function].inlineMax(AvgLargo, AvgMes)*7.0 Quantity
	from minmax.DailySales ds
	inner join products.NewProducts np on np.MirrorProduct=ds.SKU
) T
on w.sku = t.sku and w.store = t.store

update #weekSale set quantity = 0 where quantity is null




-- reglas C,D y NULL no más de 4 semanas de venta
insert into minmax.TruncateLowSaleHistory(DateKey, SKU, Store, [Date], SuggestMin, SuggestMax, CotaMin, CotaMax)
select @currentDate, p.sku, p.Store, p.Date, p.SuggestMin, p.SuggestMax, w.cotaMin, w.cotaMax
from minmax.PricingSuggest p
inner join (select sku,store,
case when CEILING(5*quantity) <= 0 then 1 else CEILING(5*quantity) end cotaMin,
case when CEILING(6*quantity) <= 0 then 2 else CEILING(6*quantity) end cotaMax
from #weekSale 
where sku in (select sku from products.Products where (TotalRanking in ('C','D','Z') OR TotalRanking is null)) ) w
on w.sku = p.sku and w.store = p.store
WHERE p.SuggestMin>w.cotaMin OR p.SuggestMax>w.cotaMax

update p set suggestMin = case when p.SuggestMin>w.cotaMin then w.cotaMin else p.SuggestMin end,
p.SuggestMax = case when p.SuggestMax>w.cotaMax then w.cotaMax else p.SuggestMax end 
from minmax.PricingSuggest p
inner join (select sku,store,
case when CEILING(5*quantity) <= 0 then 1 else CEILING(5*quantity) end cotaMin,
case when CEILING(6*quantity) <= 0 then 2 else CEILING(6*quantity) end  cotaMax
from #weekSale 
where sku in (select sku from products.Products where (TotalRanking in ('C','D','Z') OR TotalRanking is null)) ) w
on w.sku = p.sku and w.store = p.store

-- reglas A y B no más de 6 semanas de venta
insert into minmax.TruncateLowSaleHistory(DateKey, SKU, Store, [Date], SuggestMin, SuggestMax, CotaMin, CotaMax)
select @currentdate, p.sku, p.Store, p.Date, p.SuggestMin, p.SuggestMax, w.cotaMin, w.cotaMax
from minmax.PricingSuggest p
inner join (select sku,store,
case when CEILING(7*quantity) <= 0 then 1 else CEILING(7*quantity) end cotaMin,
case when CEILING(8*quantity) <= 0 then 2 else CEILING(8*quantity) end  cotaMax
from #weekSale 
where sku in (select sku from products.Products where TotalRanking in ('A','B') ) ) w
on w.sku = p.sku and w.store = p.store
WHERE p.SuggestMin>w.cotaMin OR p.SuggestMax>w.cotaMax

update p set suggestMin = case when p.SuggestMin>w.cotaMin then w.cotaMin else p.SuggestMin end,
p.SuggestMax = case when p.SuggestMax>w.cotaMax then w.cotaMax else p.SuggestMax end 
from minmax.PricingSuggest p
inner join (select sku,store,
case when CEILING(7*quantity) <= 0 then 1 else CEILING(7*quantity) end cotaMin,
case when CEILING(8*quantity) <= 0 then 2 else CEILING(8*quantity) end  cotaMax
from #weekSale 
where sku in (select sku from products.Products where TotalRanking in ('A','B') ) ) w
on w.sku = p.sku and w.store = p.store

-- reglas para A y B
--update p set p.suggestMin = case when p.SuggestMin>w.cotaMin then w.cotaMin else p.SuggestMin end,
--p.SuggestMax = case when p.SuggestMax>w.cotaMax then w.cotaMax else p.SuggestMin end from minmax.PricingSuggest p
--inner join (select sku,store,
--case when CEILING(2*quantity) <= 0 then 3 else CEILING(2*quantity) end  cotaMin,
--case when CEILING(3*quantity) <= 0 then 4 else CEILING(3*quantity) end  cotaMax from #weekSale 
--where sku in (select sku from products.FullProducts where TotalRanking in ('A','B')) ) w
--on w.sku = p.sku and w.store = p.store

END

