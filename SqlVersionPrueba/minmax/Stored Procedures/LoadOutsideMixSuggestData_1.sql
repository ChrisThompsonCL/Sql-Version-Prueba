﻿-- =============================================
-- Author:		Mauricio Sepúlveda (writen by Daniela Albornoz)
-- Create date: 2014-08-27
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [minmax].[LoadOutsideMixSuggestData]
AS
BEGIN
	SET NOCOUNT ON;
	declare @enddate date

	SELECT @enddate = DATEADD(day,-1,dbo.GetMonday(MAX(Date))) from series.Sales

    -- Insert statements for procedure here
	truncate table [minmax].[OutsideMixSuggestData]
	insert into [minmax].[OutsideMixSuggestData] (SKU,Store, ExecutionDate, SaleDate)
	select l.sku,l.store, cast(GETDATE() as date), @enddate 
	from series.LastStock l 
	left outer join aux.DailySuggestMix m on l.sku = m.sku and l.Store = m.store
	where m.sku is null

	update s set s.TotalRanking = p.totalRanking 
	from [minmax].[OutsideMixSuggestData] s
	inner join products.FullProducts p on p.sku = s.sku

	update s set s.Suggest = p.suggest 
	from [minmax].[OutsideMixSuggestData] s
	inner join series.LastStock p on p.sku = s.sku and p.Store = s.store

	--anual
	update s set s.YearQuantity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between DATEADD(year,-1,@enddate) and @enddate
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store

	-- 6 meses
	update s set s.Month6Quantity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between DATEADD(month,-6,@enddate) and @enddate
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store

	-- 3 meses
	update s set s.Month3Quantity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between DATEADD(month,-3,@enddate) and @enddate
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store

	-- 2 meses
	update s set s.Month2Quantity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between DATEADD(month,-2,@enddate) and @enddate
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store


	-- 1 mess
	update s set s.MonthQuantity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between DATEADD(month,-1,@enddate) and @enddate
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store

	-- mismo mes del año pasado
	update s set s.MonthSeasonQuanity = p.unidades 
	from [minmax].[OutsideMixSuggestData] s
	inner join (
		select s.sku,s.store,sum(quantity) unidades 
		from series.Sales s
		inner join [minmax].[OutsideMixSuggestData] p on p.sku = s.sku and p.Store = s.store
		where s.Date between  DATEADD(month,-12,@enddate) and DATEADD(month,-11,@enddate)
		group by s.sku,s.store
	) p on p.sku = s.sku and p.Store = s.store

	--costos
	update s set s.Cost = p.Cost from [minmax].[OutsideMixSuggestData] s inner join pricing.LastPriceAndCost p on p.sku = s.sku 


	update s set s.YearQuantity = 0 from [minmax].[OutsideMixSuggestData] s where s.YearQuantity is null
	update s set s.Month2Quantity = 0 from [minmax].[OutsideMixSuggestData] s where s.Month2Quantity is null
	update s set s.Month3Quantity = 0 from [minmax].[OutsideMixSuggestData] s where s.Month3Quantity is null
	update s set s.Month6Quantity = 0 from [minmax].[OutsideMixSuggestData] s where s.Month6Quantity is null
	update s set s.MonthQuantity = 0 from [minmax].[OutsideMixSuggestData] s where s.MonthQuantity is null
	update s set s.MonthSeasonQuanity = 0 from [minmax].[OutsideMixSuggestData] s where s.MonthSeasonQuanity is null

	update s set TotalRanking='S' from [minmax].[OutsideMixSuggestData] s where s.TotalRanking is null

	--SELECT * FROM [minmax].OutsideMixSuggestData

END
