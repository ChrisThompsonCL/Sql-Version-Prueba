﻿-- =============================================
-- Author:		Dani y Plan Z
-- Create date: 2018-06-18
--- Description:borra tablas del calculo de sugeridos
-- =============================================
CREATE PROCEDURE [minmax].[DeleteAuxSuggestData] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table [minmax].SuggestForecast
	truncate table [minmax].HolidaysRates
	truncate table [minmax].SuggestAllNew
	truncate table [minmax].TruncateRatesHistory
	truncate table [minmax].[FitsAllNew]
	truncate table [minmax].DistributionOrdersForecastNew
	truncate table [minmax].[CalculationDateDistributionOrdersNew]
	truncate table [minmax].[SugeridoProyeccionPedidosNew]
	truncate table [aux].RatesForSuggest
	truncate table [aux].SimulatedSales
	truncate table minmax.[CalculationDateDistributionOrdersNew]

END
