﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-08-25
-- Description:	Obtiene nuevas combinaciones a considerar en politica de inventario minmax. Guarda en tabla auxiliar
-- Modificación: 2017-06-27 Daniela Albornoz - modificación para que no considere combinaciones con DUN dentro del set minmax
-- Modificación: 2018-02-01 Fairenes Zerpa - Modificación para que no considere los genéricos dentro del minmax
-- =============================================
CREATE PROCEDURE [minmax].[AnalizeMinMaxCombinations]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @MAXdate date =  (select top 1 [Date] from series.LastStock)
	DECLARE @MINdate AS date = DATEADD(day,-27, @MAXdate)

	-- Delaramos el corte que queremos en las combinaciones fuera y dentro de MinMax
	DECLARE @MinPickingCombsFuera int = 4
	DECLARE @MinPickingCombsDentro int = 2

	/*DETECTAR NUEVAS COMBINACIONES*/
	truncate table aux.MinMaxCombinations

	insert into aux.MinMaxCombinations
	SELECT rs.SKU, rs.Store, @MAXdate FechaActualización
		,  sum(case when pickedUnits>=1 then 1 else 0 end) Picking
		, case when mmc.EntryDate is null then 1 else 0 end NewComb
	FROM  reports.reportstores rs
	inner join (
	--declare @MAXdate date = '2017-06-26'
		select dsm.SKU, dsm.Store
		from aux.dailysuggestmix dsm
		inner join products.products p on p.SKU=dsm.SKU 
		left join (
			SELECT distinct ls.SKU, ls.Store
			FROM [view].[SuggestRestrictionDetail] srd
			INNER JOIN series.LastStock ls on ls.SKU = srd.ProductId AND ((srd.StoreType=0 and srd.StoreID=ls.Store) OR (srd.StoreType=3))
			WHERE @MAXdate BETWEEN srd.BeginDate AND ISNULL(EndDate, '9999-12-31')
				and srd.SuggestRestrictionTypeID in (5,6,7,15,13,14)
		) rdun on rdun.SKU=dsm.SKU and rdun.Store=dsm.Store
		where p.Class not in (192, 198) -- pañales 
			and logisticareaID not in ('Bref.Ref','Bcon.Con') --controlados y regrigerados
			and not (TotalRanking in ('A','B')  and  ManufacturerID in (select * from operation.PrivateLabelManufacturers)) --and LogisticAreaId <> 'Bespe.Per'-- ranking A/B de proveedores MP menos perfumería
			and dsm.Store not in (select store from temp.LocalesFarmaPrecio) -- Farma Precio
			and rdun.SKU is null --dejar combinaciones con DUN fuera
			and p.sku not in (select sku from products.genericfamilydetail) -- Fuera Genéricos
	) ma on ma.SKU=rs.SKU and ma.Store=rs.Store
	left join [minmax].[MinMaxCombinations] mmc on mmc.SKU=rs.SKU and mmc.Store=rs.Store
	where rs.[date] >=@MINdate
	group by rs.SKU, rs.Store, mmc.EntryDate
	HAVING (mmc.EntryDate is null and  sum(case when pickedUnits>=1 then 1 else 0 end)>=@MinPickingCombsFuera)
		OR (mmc.EntryDate is not null and sum(case when pickedUnits>=1 then 1 else 0 end)>=@MinPickingCombsDentro)

	select d.*
	from (
	/*Evaluación de impacto de combinaciones nuevas*/
	SELECT count(*) CombinacionesTotal
		, sum(Pick) PickingTotal
		, sum(case when ma.SKU is not null then 1 else 0 end) CombinacionesPotencial
		, sum(case when ma.SKU is not null then Pick else 0 end) PickingPotencial
		, sum(case when mmc.SKU is not null then 1 else 0 end) CombinacionesElegidas
		, sum(case when mmc.SKU is not null then Pick else 0 end) PickingElegidas
		, SUM(case when mmc.SKU is not null and mmcr.SKU is null then 1 else 0 end) CombsEntran
		, SUM(case when mmc.SKU is not null and mmcr.SKU is not null then 1 else 0 end) CombsMantienen
		, SUM(case when mmc.SKU is null and mmcr.SKU is not null then 1 else 0 end) CombsSalen
	FROM  (
		select SKU, Store, sum(case when pickedUnits>=1 then 1 else 0 end) Pick
		from reports.reportstores
		where [date] >=@MINdate
		group by SKU, Store
	) rs
	left join (
		select dsm.SKU, dsm.Store
		from aux.dailysuggestmix dsm
		inner join products.products p on p.SKU=dsm.SKU 
		where p.Class not in (192, 198) -- pañales 
			and logisticareaID not in ('Bref.Ref','Bcon.Con') --controlados y regrigerados
			and not (TotalRanking in ('A','B') and ManufacturerID in (select * from operation.PrivateLabelManufacturers)) --and LogisticAreaId <> 'Bespe.Per'-- ranking A/B de proveedores MP menos perfumería
			and Store not in (select store from temp.LocalesFarmaPrecio) -- Farma Precio
		    and dsm.sku not in (select sku from products.genericfamilydetail) -- Fuera Genéricos
	) ma on ma.SKU=rs.SKU and ma.Store=rs.Store
	left join aux.MinMaxCombinations mmc on mmc.SKU=rs.SKU and mmc.Store=rs.Store
	left join minmax.MinMaxCombinations mmcr on mmcr.SKU=rs.SKU and mmcr.Store=rs.Store
	) a
	cross apply (
		SELECT 'Total', CombinacionesTotal, '100%', PickingTotal, '100%'
		UNION ALL
		SELECT 'Potencial', CombinacionesPotencial,CONCAT(ROUND(CombinacionesPotencial/cast(CombinacionesTotal as float)*100,1),'%')
				, PickingPotencial,CONCAT(ROUND(PickingPotencial/cast(PickingTotal as float)*100,1),'%')
		UNION ALL
		SELECT 'Elegidas', CombinacionesElegidas,CONCAT(ROUND(CombinacionesElegidas/cast(CombinacionesTotal as float)*100,1),'%')
				, PickingElegidas,CONCAT(ROUND(PickingElegidas/cast(PickingTotal as float)*100,1),'%')
		UNION ALL
		SELECT 'Entran', CombsEntran, NULL, NULL, NULL
		UNION ALL
		SELECT 'Salen', CombsSalen, NULL, NULL, NULL
		UNION ALL
		SELECT 'Mantienen', CombsMantienen, NULL, NULL, NULL
	) d ([--], Combinaciones, [%Combs], Picking, [%Picking])

END

