﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-09-04
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [minmax].[SuggestRestrictionOutsideMix]
	-- Add the parameters for the stored procedure here
	@date date = NULL
AS
BEGIN
	SET NOCOUNT ON;

	if @date is null
	begin
		set @date = (select max(ExecutionDate) FROM [minmax].[OutsideMixSuggestResult])
	end

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	UPDATE s SET Minimo = srt.Minimo, DUN=(srt.MedioDUN-1)*2, Fijo=srt.Fijo, Planograma=srt.Planograma, Maximo=srt.Maximo, CajaTira=srt.CajaTira, FijoEstricto=srt.FijoEstricto
	--	, NewRestrictedSuggest=[function].GetRestrictedSuggest(s.NewSuggest, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN) 
	FROM [minmax].[OutsideMixSuggestResult] s
	--inner JOIN [view].SuggestRestrictionToday srt on srt.SKU=s.SKU and srt.Store=s.Store
	inner JOIN #SuggestRestrictionToday srt on srt.SKU=s.SKU and srt.Store=s.Store
	where s.ExecutionDate=@date


	UPDATE o SET NewRestrictedSuggest=a.RestrictedSuggest
	FROM [minmax].[OutsideMixSuggestResult]  o
	CROSS APPLY [function].[ApplyRestriction](o.NewSuggest, o.Minimo, o.Fijo, o.Planograma, o.DUN/2+1, o.Maximo, o.CajaTira, o.FijoEstricto) a
    
END
