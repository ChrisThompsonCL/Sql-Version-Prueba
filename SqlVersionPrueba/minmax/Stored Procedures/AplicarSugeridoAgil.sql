﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2017-11-06
-- Description:	Aplica el sugerido ágil a la restricción

-- Modificado:		Juan M Hernandez
-- Create date: 2017-12-18
-- Description: aplicar salida de sugerido
-- =============================================
CREATE PROCEDURE [minmax].[AplicarSugeridoAgil]

AS
BEGIN
	SET NOCOUNT ON;

	--id del archivo al que referenciaremos, dado que esta restricción es originada desde el
	-- sistema, usamos un fileID que representa 'SugeridoAgil' y fue creado a mano.
	DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE();


	--eliminamos las restricciones ágiles que se cargaron hoy.
	DELETE 
	--DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE(); select *
	from  br.SuggestRestrictionDetail 
	where SuggestRestrictionFileID =  @fileId 
		    AND CAST(InsertionDate AS DATE) = cast(@today as DATE)
	
	--DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE();
	
	--inserta como restricción la sugerencia ágil de hoy
    insert into br.SuggestRestrictionDetail (
		ProductID, ProductType, StoreID, StoreType, Detail, 
		BeginDate, EndDate, InsertionDate, 
		SuggestRestrictionFileID, SuggestRestrictionTypeID
	)
	--DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE();
	select   
		sa.SKU ProductID, 0 ProductType, sa.Store StoreID, 0 StoreType, sa.NuevoSugerido Detail, 
		@today BeginDate, '2025-01-01' EndDate, @today InsertionDate,
		@fileId SuggestRestrictionFileID, 4 SuggestRestrictionTypeID
	FROM minmax.SugeridoAgil sa
	INNER JOIN products.Products p ON sa.SKU = p.SKU
	INNER JOIN pricing.LastPriceAndCost pc ON p.SKU = pc.SKU
	WHERE  NuevoSugerido IS NOT NULL
		AND p.TotalRanking IN ('A', 'B')
		AND sa.TieneRestriccion =0
		and pc.Cost < 200000
   
	/*****  revisamos si te las combinaciones no tienen restricción aplicada 
			y actualizamos  la columna TieneRestriccion en sugerido Agil 
			
	*****/


	UPDATE  sa SET TieneRestriccion = 1  --select sa.fecha, sa.nuevosugerido, rd.*
	FROM minmax.SugeridoAgil sa
	inner join products.Products p ON sa.SKU = p.SKU
	INNER JOIN pricing.LastPriceAndCost pc ON p.SKU = pc.SKU
	inner join (
		SELECT * 
		FROM br.SuggestRestrictionDetail
		WHERE SuggestRestrictionFileID = 11534
	) rd ON rd.ProductID = sa.SKU and rd.StoreID = sa.Store and rd.Detail=sa.NuevoSugerido and sa.Fecha <= cast(rd.InsertionDate as date)
	WHERE NuevoSugerido IS NOT NULL
			AND p.TotalRanking IN ('A', 'B')
			AND sa.TieneRestriccion = 0
			and pc.Cost < 200000


	/***************  actualizar las fechas *****/
	/*
		Busco las fechas enddate iguales a 2025-01-01
		luego reviso que el sugerido ágil sea >= al stock + quantity
		evaluo solo las fecha que son >= a la restricción

	*/
	/*******************************************/

	--DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE();
	SELECT DISTINCT (sa.SKU), (sa.Store), BeginDate, EndDate 
	INTO #ActualizarEnddate 
	FROM minmax.SugeridoAgil sa
	INNER JOIN br.SuggestRestrictionDetail d on d.ProductID = sa.SKU and d.StoreID = sa.Store
	WHERE NuevoSugerido IS NOT NULL
	AND sa.TieneRestriccion =1
	AND d.SuggestRestrictionFileID = @fileId 

	CREATE INDEX idx_1 ON #ActualizarEnddate (SKU)
	CREATE INDEX idx_2 ON #ActualizarEnddate (Store)


	--DECLARE @fileId INT = 11534; DECLARE @today DATETIME = GETDATE();
	
	UPDATE br.SuggestRestrictionDetail
	SET EndDate = DATEADD(MONTH, 1, cast(@today as date))
	FROM 
	(
		select p.SKU, p.Store, p.BeginDate ,p.EndDate,  sa.NuevoSugerido 
		FROM #ActualizarEnddate p
		INNER JOIN minmax.SugeridoAgil sa on sa.SKU = p.SKU AND sa.Store = p.Store
		INNER JOIN series.LastStock rs on rs.SKU = p.SKU AND rs.Store = p.Store
		LEFT JOIN series.LastSale ls on ls.SKU=p.SKU and rs.Stock=p.Store
		WHERE rs.Stock + isnull(ls.Quantity,0) >= sa.NuevoSugerido
			AND p.BeginDate <= rs.[Date]
	)sa inner join br.SuggestRestrictionDetail sd on sa.SKU = sd.ProductID and sa.Store = sd.StoreID
	WHERE sd.EndDate = '2025-01-01'            /********** FECHA POR DEFECTO (POSIBLE REDUNDANCIA) ***********/		 
		AND sd.SuggestRestrictionFileID = @fileId  /********** ID SUGERIDO ÁGIL (POSIBLE REDUNDANCIA) ***********/		

	-- Aplica las restricciones de sugerido
	exec br.ApplySuggestRestrictionLite 


	DROP TABLE #ActualizarEnddate
END

