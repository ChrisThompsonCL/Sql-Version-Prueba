﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2014-05-07
-- Description:	Calcula el alza diaria
-- =============================================
CREATE PROCEDURE [minmax].[HolidayCalculateDailyIncrease] 
	-- Add the parameters for the stored procedure here
	@holidayDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    CREATE TABLE #table (SKU int,store int, ranking nvarchar(1) collate Modern_Spanish_CI_AS, SMin int,SMax int,lastMax int,stock int)
	CREATE INDEX #id1 ON #table(SKU)
	CREATE INDEX #id2 ON #table(Store)

	declare @date date = cast(GETDATE() as date)
	DECLARE @msg VARCHAR(100)

	-- PRIMERO: Cálculo del alza
 
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Se crea tabla inicial...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
	insert into #table 
	select a.SKU,a.Store, a.TotalRanking, a.SMin,a.Smax,l.[Max],l.Stock 
	from minmax.HolidayApprovedSuggests a 
	inner join (
		select l.SKU,l.Store,isnull(n.[Max],l.[Max]) [Max],l.Stock 
		from series.LastStock l 
		left outer join (select * from replenishment.NextSuggest where [date] = @date) n 
			on l.SKU = n.SKU and l.Store = n.Store
	) l on a.SKU = l.SKU and a.Store = l.Store
	where a.HolidayDate = @holidayDate
		and a.Smax > l.[Max] --and a.Smax > l.Stock
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla inicial creada' RAISERROR (@msg, 10, 0 ) WITH NOWAIT

	-- borro lo que ya esta en la tabla
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Se borran los datos ya cargados...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
	delete t from #table t 
	inner join minmax.HolidaySuggestNew h on h.sku = t.sku and h.store = t.store 
	where h.HolidayDate = @holidayDate
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Datos borrados' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
 
	-- borro lo que esta bloqueado
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Se borran combinaciones bloqueadas...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
	delete t from #table t 
	inner join series.LastStock h on h.sku = t.sku and h.store = t.store 
	where h.IsBloqued = 'S'
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Combinaciones bloqueadas borradas' RAISERROR (@msg, 10, 0 ) WITH NOWAIT

	-- borro lo que no pickea MAÑANA (replenishmente considera día del movimiento)
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Se borra lo que no repone mañana...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
	DELETE t from #table t 
	left join (
		select r.Store,p.SKU from minmax.Replenishment r
		inner join products.Products p on r.LogisticAreaId = p.LogisticAreaId
		where [Days]>0 and [Date] = @date
	) h on h.sku = t.sku and h.store = t.store
	where h.SKU is null
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Reposición considerada' RAISERROR (@msg, 10, 0 ) WITH NOWAIT
 
	-- guarda alza
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Inicia carga de alza diaria...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT

	DELETE FROM [minmax].[HolidaySuggestNew] WHERE CreateDate=@date and HolidayDate = @holidayDate

		-- OJO!! PARA CALCULAR MÁS RÁPIDO
	INSERT INTO [minmax].[HolidaySuggestNew]([SKU],[Store],[HolidayDate],[CreateDate],[LastSug],[Increase],MinHolidays,MaxHolidays,[BeginDate],[EndDate])
	select t.SKU,t.store,@holidayDate,@date,lastMax,SMax-lastMax,SMin,SMax,@date,h.EndDate 
	from #table t
	inner join (
		select a.sku, p.totalranking, a.increase, p.DailyIncrease, p.EndDate, ac_increase
		from (
			select *, sum(increase) over (partition by ranking order by increase desc rows between unbounded preceding and current row) ac_increase
			from (
				select SKU,ranking,SUM(SMax-lastMax) increase
				from #table 
				group by sku, ranking 
			) a
		) a
		inner join (
			select * from minmax.HolidayIncreaseParams where HolidayDate=@holidayDate and @date between BeginDate and EndDate
		) p on isnull(p.TotalRanking,'0')=isnull(a.ranking,'0')
		where ac_increase <= p.DailyIncrease
	) h on t.SKU=h.SKU 

	-- REPLICAR ALZA PARA GENÉRICOS
	select gfd.SKU, sf.Store, sf.[HolidayDate], CreateDate,LastSug,Increase, MinHolidays,MaxHolidays,BeginDate,EndDate
	into #suggestAux
	from (
		select gfd.GenericFamilyId, Store, [HolidayDate],MIN(CreateDate)CreateDate,MAX(LastSug)LastSug,MAX(Increase)Increase
			, MAX(MinHolidays)MinHolidays, MAX(MaxHolidays)MaxHolidays,MIN([BeginDate])[BeginDate],MAX([EndDate])[EndDate]
		from [minmax].[HolidaySuggestNew] ps 
		inner join products.GenericFamilyDetail gfd on gfd.SKU=ps.SKU
		group by gfd.GenericFamilyId, Store, [HolidayDate]
	) sf
	inner join products.GenericFamilyDetail gfd on gfd.GenericFamilyId=sf.GenericFamilyId
	inner join aux.DailySuggestMix ds on ds.SKU=gfd.SKU and ds.Store=sf.Store

	delete from [minmax].[HolidaySuggestNew]
	where SKU in (select SKU from products.GenericFamilyDetail)

	insert into [minmax].[HolidaySuggestNew]
	select * from #suggestAux

	drop table #suggestAux


	--para guardar sugerido de feriado en tabla de sugeridos sin restriccion
	update s set SuggestMin = h.MinHolidays,SuggestMax=h.MaxHolidays, [Date]=BeginDate, HolidayIncrease=1 --select *
	from [minmax].[UnrestrictedPricingSuggest] s 
	inner join minmax.HolidaySuggestNew h on s.SKU = h.SKU and s.Store = h.Store 
	where HolidayDate = @holidayDate and s.[Date] = @date and ( s.SuggestMin<h.MinHolidays or s.SuggestMax<h.MaxHolidays)
		and @date between BeginDate and EndDate


	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga de alza finalizada' RAISERROR (@msg, 10, 0 ) WITH NOWAIT


END
