﻿
-- =============================================
-- Author:		Innovación, Plan Z, Dani
-- Create date: 2012-04-19
-- Description:	calcula el sugerido para un producto

-- Updated by:		Gabriel Espinoza
-- Updated reason:	Se implementa apertura de locales espejo para permitir definir
--					espejos a nivel "DERMO", "FARMA" o "CONSUMO"

-- Updated by:		Dani y JuanM <3
-- Updated reason:	Nuevas columnas a guardar en el cálculo

-- =============================================
CREATE PROCEDURE [minmax].[SIIRA_GenerateSuggest] 
	@beginDate date = NULL,
	@endDate date = NULL, 
	@sku int, 
	@debug bit
AS
BEGIN

	DECLARE @msg VARCHAR(100) 
		SET NOCOUNT ON;

	declare @maxWeek date
	select @maxWeek = (select FechaCalculo from aux.UltimoPronostico)

	--pega información de días entre reposicion
	update v set [days]= j.[days] + case when j.[Days]>0 then isnull(psd.[SafetyDays],0) else 0 end
		, LeadTime=j.LT , SafetyDays = case when j.[Days]>0 then isnull(psd.[SafetyDays],0) else 0 end
	from aux.RatesForSuggest v
	inner join products.Products p on p.SKU = v.sku
	inner join minmax.Replenishment  j on j.Store = v.Store and j.[date]=v.[date] and j.LogisticAreaId = p.LogisticAreaId
	left join [minmax].[SuggestParameters] psd on psd.SKU=v.SKU

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de días a cubrir en tabla lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end 
	
	--cálculo de tasas de venta entre reposiciones
	update t set t.rate = h.rate from aux.RatesForSuggest t inner join (
		select s1.sku,s1.store,s1.[date],SUM(s2.fitDay) rate from aux.RatesForSuggest s1
		inner join aux.RatesForSuggest s2 on s1.sku = s2.sku and s1.store = s2.store 
		and s2.[date] between dateadd(day,1,s1.[date]) and dateadd(day,s1.[days] + 1 ,s1.[date])
		where s1.[days]>0 
		group by s1.sku,s1.store,s1.[date]
	) h on h.sku = t.sku and  h.store = t.store and h.[date] = t.[date]

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Cálculo de tasas listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	--cálculo de mínimo de acuerdo a la tasa y al intervalo de nivel de servicio que pertenece el producto
	update t set suggestMin=k.Suggest , ServiceLevel=k.ServiceLevel
	from aux.RatesForSuggest t 
	inner join (
		select * from (
			SELECT Suggest, MinRate, MaxRate, ServiceLevel
			FROM [minmax].PoissonSuggest
			WHERE ServiceLevel in (select round(cast(ServiceLevelDesc as float)/100,3) from [minmax].[SuggestParameters] where SKU = @sku)
		) h
	) k on ROUND(t.rate,4) between round(k.MinRate,4) and round(k.MaxRate,4) 
	where t.days>0

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Cálculo de mínimo listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	update t set t.suggestMax=suggestMaxNext 
	from aux.RatesForSuggest t 
	inner join (
		select v1.*,v2.suggestMin+CEILING(v1.rate) suggestMaxNext from aux.RatesForSuggest v1, aux.RatesForSuggest v2
		where v1.sku = v2.sku and v1.store = v2.store and v2.date in (select MIN(date) from aux.RatesForSuggest where sku = v1.sku and store=v1.store and date>v1.date and suggestMin>0)
		and v1.suggestMin>0 
	) v on v.sku = t.sku and v.store = t.store and v.date = t.date 
	where t.days>0
	

	if @debug = 1 begin
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Cálculo de máximo listo' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	end
	
	/*****************************************************/
	/****************TASAS DEL FERIADO********************/
	/*****************************************************/
	
	-- guardo las tasas de los holidays
	DELETE hr FROM [minmax].[HolidaysRates] hr	where hr.SKU = @sku
	
	INSERT INTO [minmax].[HolidaysRates] ([SKU],[Store],[Date],[Rate], RateBefore, RateNext)
	select *
	from (
		select [SKU],[Store],[Date]
			,Rate
			,LAG(Rate) over (partition by Store order by [Date])RateBefore
			,LEAD(Rate) over (partition by Store order by [Date])RateNext
		from aux.RatesForSuggest 
		where [days]>0
			and [date]>=@beginDate 
	) a
	WHERE  dateadd(day,1,[Date]) in (select date from dcpurchase.Holidays) -- me tengo que mover un día para ver el feriado
		and (Rate+isnull(RateBefore,0))>0
	

	/*****************************************************/
	/*********SUGERIDO MAX PROXIMAS SEMANAS***************/
	/*****************************************************/
	
	DELETE sf FROM [minmax].[SuggestForecast] sf where sf.SKU = @sku
	
	INSERT INTO [minmax].[SuggestForecast] ([SKU],[Store],[W1],[W2],[W3],[W4])
	select w1.sku,w1.store,w1.w1,w2.w2,w3.w3,w4.w4 
	from (
		select sku,store,MAX(suggestMin) w1 
		from aux.RatesForSuggest 
		where date between @beginDate and DATEADD(day,6,@beginDate) 
		group by sku,store
	) w1
	inner join (
		select sku,store,MAX(suggestMin) w2 
		from aux.RatesForSuggest 
		where date between DATEADD(day,7,@beginDate) and DATEADD(day,13,@beginDate) 
		group by sku,store
	) w2 on w1.sku = w2.sku and w1.store = w2.store
	inner join (
		select sku,store,MAX(suggestMin) w3 
		from aux.RatesForSuggest 
		where date between DATEADD(day,14,@beginDate) and DATEADD(day,20,@beginDate) 
		group by sku,store
	) w3 on w1.sku = w3.sku and w1.store = w3.store
	inner join (
		select sku,store,MAX(suggestMin) w4 
		from aux.RatesForSuggest 
		where date between DATEADD(day,21,@beginDate) and DATEADD(day,27,@beginDate) 
		group by sku,store
	) w4 on w1.sku = w4.sku and w1.store = w4.store
	

	/*****************************************************/
	/*****************GUARDAR SUGERIDO********************/
	/*****************************************************/

	-- insertar todos los sugeridos
	DELETE san FROM [minmax].[SuggestAllNew] san WHERE san.SKU = @sku
	
	INSERT INTO [minmax].[SuggestAllNew]
           ([SKU],[Store],[Date],[Days],[Rate],[SuggestMin],[SuggestMax],[LeadTime],[SafetyDays],[ServiceLevel])
	select [SKU],[Store],[Date],[days],rate,case when suggestMin > 0 then suggestMin-1 else 0 end,suggestMax, leadtime, safetydays, servicelevel
	from aux.RatesForSuggest 
	where suggestMin>=0 and date between @beginDate and @endDate and [Days]>0

	--si es minmax, guardo una semana más
	INSERT INTO [minmax].[SuggestAllNew]
           ([SKU],[Store],[Date],[Days],[Rate],[SuggestMin],[SuggestMax],[LeadTime],[SafetyDays],[ServiceLevel])
	select rfs.[SKU],rfs.[Store],[Date],[days],rate,case when suggestMin > 0 then suggestMin-1 else 0 end,suggestMax, leadtime, safetydays, servicelevel
	from aux.RatesForSuggest  rfs 
	inner join minmax.MinMaxCombinations mmc on mmc.SKU=rfs.sku and mmc.Store=rfs.store
	where suggestMin>=0 and date between dateadd(day,1, @endDate) and dateadd(day,7, @endDate) and [Days]>0


END

