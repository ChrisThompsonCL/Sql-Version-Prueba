﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2016-08-25
-- Description:	Actualiza combinaciones minmax desde tabla auxiliar. Hace respaldo de combinaciones que desaparecen
-- =============================================
CREATE PROCEDURE [minmax].ConfirmMinMaxCombinations
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   /*combinaciones que salen*/
	INSERT INTO [reports].[MinMaxCombinationsOld]([SKU],[Store],[EntryDate],[ExitDate])
	select mmc.SKU, mmc.Store, mmc.EntryDate, cast(getdate() as date) ExitDate
	from aux.MinMaxCombinations fc
	full join Salcobrand.minmax.MinMaxCombinations mmc on mmc.SKU=fc.sku and mmc.Store=fc.Store
	where fc.sku is null

	delete mmc
	from aux.MinMaxCombinations fc
	full join minmax.MinMaxCombinations mmc on mmc.SKU=fc.sku and mmc.Store=fc.Store
	where fc.sku is null

	/*combinaciones que entran*/
	INSERT INTO minmax.[MinMaxCombinations]([SKU],[Store],[EntryDate])
	select fc.SKU, fc.Store, cast(getdate()+1 as date) EntryDate
	from aux.MinMaxCombinations fc
	full join minmax.MinMaxCombinations mmc on mmc.SKU=fc.sku and mmc.Store=fc.Store
	where mmc.sku is null

END
