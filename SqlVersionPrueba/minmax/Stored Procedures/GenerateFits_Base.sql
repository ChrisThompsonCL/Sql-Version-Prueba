﻿-- ================================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-28
-- ================================================================================================
CREATE PROCEDURE [minmax].[GenerateFits_Base]
	-- Add the parameters for the stored procedure here
	@sku int,
	@beginning_date date,
	@finishing_date date

AS
BEGIN



	create table #sales (store_id int, sale_date date, quantity float)
	create index #ix1 on #sales(store_id)
	create index #ix2 on #sales(sale_date)

	create table #initial_date (store_id int, initial_date date)
	create table #dias_venta (store_id int,Month int, days int)
	create table #monthly_sales_corrected(store_id int, Month int, quantity float)
	create table #first_matrix2 (store_id int, Month int, Day int, quantity float)
	create table #total_sales (store_id int, Quantity float)
	create table #daily_importance2 (store_id int, Day int, quantity float)
	create table #monthly_importance(store_id int, Month int, quantity float)
	create table #second_matrix2 (store_id int, Month int, Day int, quantity float)
	create table #sum_second_matrix2 (Month int, Day int, quantity float)
	create table #last_3months_sales (store_id int, Quantity float)
	create table #last_1month_sales (store_id int, Quantity float)
	create table #last_1week_sales (store_id int, Quantity float)
	create table #first_instance_forecast (date date, store_id int, first_forecast float)
	create table #sum_first_instance_forecast (date date, total_forecast float)
	create table #final_forecast (sku int, date date, store_id int, suggest float)
	create table #original_forecast (date date, fit float)
	create table #stores_to_replace (store_id int)
	create table #sales_by_store(store_id int, sales int)
	create table #days_out_of_stock_historic(Month int, store_id int, days int)
	create table #days_out_of_stock_3months (store_id int, days int)
	create table #days_out_of_stock_1month  (store_id int, days int)
	create table #days_out_of_stock_1week	(store_id int, days int)
	create table #store_importance (store_id int, Quantity float)
	create table #to_delete_2(store_id int, sale_date date)
	create table #to_delete_3(store_id int, sale_date date)
	create table #sales_previo(store_id int, sale_date date, quantity float)
	create table #stores(store_id int)
	create table #fechas(date date, store_id int)
	create table #sales_inter(store_id int, sale_date date, quantity float)
	create table #dias_por_store(storeid int, dias int)



	SET NOCOUNT on;
	
	--Caso sku genérico
	DECLARE @skusAux TABLE(SKU int)

	INSERT INTO @skusAux
	SELECT SKU 
	FROM products.Products
	WHERE SKU=@sku AND SKU not in (select SKU from products.GenericFamilyDetail)
	UNION
	SELECT SKU 
	FROM products.GenericFamilyDetail 
	WHERE GenericFamilyId=(select GenericFamilyId from products.GenericFamilyDetail where SKU=@sku)


	DECLARE @fechaCalc date = (select FechaCalculo from aux.UltimoPronostico)

	DECLARE @categoryID int
	set @categoryID = (select CategoryId from [products].[Products] where sku = @sku)


	-- Guarda venta pronosticada por dia - sku (a nivel nacional)
	insert into #original_forecast
	select [date] Fecha, SUM(Fit) Fit
	from (
		 select datekey,productid,[Date]
		  ,[Fit]
		  ,ROW_NUMBER () OVER (Partition By [ProductId], [Date] ORDER BY [DateKey] DESC) Orden 
		 FROM [Salcobrand].forecast.[PredictedDailySales]
		 where datekey <= @fechaCalc and date >= @beginning_date and date <= @finishing_date and productid in (select sku from @skusAux)
	 ) x
	 where orden=1
	 group by [date]	
	--select [date] Fecha, SUM(Fit) Fit
	--from [forecast].[PredictedDailySales]
	--where datekey = @fechaCalc and date >= @beginning_date and date <= @finishing_date and productid in (select sku from @skusAux)
	--group by [date]



	--Combinaciones local-fecha que presentan quiebre en el pasado (desde hace un año hasta fecha inicio)
	if (select count(*) from @skusAux) =1 
	begin
		insert into #to_delete_2
		select store, date
		from [series].[Stock-outs]
		where sku = @sku  and Date >= DATEADD(yy, -1, @beginning_date) and Date < @beginning_date
	end


	-- venta diaria en el pasado por local-fecha
	insert into #sales_previo
	select store_id, sale_date ,SUM(Quantity) Quantity
	from aux.SalesCalculoFit
	where sku in (select sku from @skusAux)
	group by store_id, sale_date


	-- considerar solo las stores que se consideran para la venta pasada
	insert into #stores
	select distinct store_id
	from #sales_previo

	--insert into #fechas
	--select date, store_id
	--from [generic].[DateAggregationDetail] d 
	--cross join #stores
	--where Date >= DATEADD(yy, -1, @beginning_date) and Date < @beginning_date and id = 0


	-- generar TODAS las combinaciones fecha - local para el pasado 
	insert into #fechas
	select fecha [date], store_id
	from forecast.GenericVariables d 
	cross join #stores
	where fecha >= DATEADD(yy, -1, @beginning_date) and fecha < @beginning_date


	--asignar 0 o quantity para todas las combinaciones 
	insert into #sales_inter
	select d.store_id, d.date as sale_date, isnull(Quantity, 0) as Quantity
	from #sales_previo sp 
	right join #fechas d on (sp.sale_date = d.Date and sp.store_id = d.store_id)


	-- borrar combinaciones que presentan quiebre en el pasado 
	delete s
	from #sales_inter s
	inner join #to_delete_2 d on d.store_Id=s.store_id and d.sale_date=s.sale_date
	where Quantity <= 0 -- se consideran casos en que si hayan ventas a pesar de estar quebrado

	--select store_id, sale_date, quantity, ROW_NUMBER() over(partition by store_id order by sale_date desc) as Row
	--into #new_sales
	--from #sales_inter

	insert into #sales
	select store_id, sale_date, quantity
	from (
		select store_id, sale_date, quantity, ROW_NUMBER() over(partition by store_id order by sale_date desc) as Row
		from #sales_inter
	) a
	where Row <= 90

	-- Datos para generar condiciones de delete_3
	select store_id as sstore_id, avg(quantity*1.0) as AVG_Quantity, STDEV(quantity) as DEV_Quantity
	into #stats
	from #sales
	group by store_id
	
	insert into #dias_por_store
	select store_id as storeid, count(distinct sale_date) as dias 
	from #sales
	group by store_id

	
	insert into #to_delete_3
	select store_id, sale_date
	from #sales sp 
	inner join #stats st on sp.store_id = st.sstore_id 
	inner join #dias_por_store dp on sp.store_id = dp.storeid
	where Quantity > AVG_Quantity*10 and Quantity >= 50 * dias / (90*1.0)
	--Condición 1: Venta > Venta promedio
	--Condicioón 2: Venta >= 50 * ??????


	delete s from 
		#sales s 
		inner join #to_delete_3 td on s.store_id=td.store_id and s.sale_date=td.sale_date
	
	-- importancia de la categoria según mes-dia para cada local
	insert into #second_matrix2
	select store_ID, month, day, Importance 
	from [minmax].[Fits_CategoryImportance]
	where category_ID = @categoryID

	-- importancia de la categoría según mes-dia a nivel nacional 
	insert into #sum_second_matrix2
	select	Month,
			Day,
			sum(quantity) as Quantity
	from #second_matrix2
	group by Month, Day

	
	insert into #store_importance
	select	store_id, avg(Quantity)
	from	#sales
	group by store_id
	
	DECLARE @Dt datetime
	SET @dt = @beginning_date
	WHILE @dt <= @finishing_date
		BEGIN
		


			DECLARE @forecast float
			SET @forecast =	(select fit
								from #original_forecast
								where date = @dt)

			insert into #first_instance_forecast
			select	@dt,
					s.store_id,
					s.quantity * @forecast * l.quantity / m.quantity
			from	#second_matrix2 s inner join
					#store_importance l on s.store_id = l.store_id cross join
					#sum_second_matrix2 m
			where	s.day = datepart(dw, @dt) and s.Month = datepart(m, @dt) and
					m.Day  = datepart(dw, @dt) and m.Month = datepart(m, @dt)


			SET @dt = DATEADD(dd,1,@Dt)

		END


	insert into #sum_first_instance_forecast
	select	date,
			sum(first_forecast)
	from #first_instance_forecast
	group by date


	insert into #final_forecast
	select	@sku,
			s.date,
			store_id,
			case when	s.total_forecast > 0
						then (case when	(f.first_forecast * forecast.fit / s.total_forecast) is null 
										then 0 
										else f.first_forecast * forecast.fit / s.total_forecast end)
						else 0 end
	from	#first_instance_forecast f 
	inner join #sum_first_instance_forecast s on f.date = s.date 
	inner join #original_forecast forecast on s.date = forecast.date
	


	IF	(@fechaCalc is not null)
		BEGIN 
			insert into [aux].[Fits_Temp]  (sku, store, [date], Fit)
			SELECT sku, store_id, [date], suggest--RateFinal
			--from #nuevo_final
			from #final_forecast
		END
	ELSE
		-- Significa que estan llamando este procedimiento almacenado desde una fecha especial (guardamos en otro temp, para que no se guarde nada oficialmente)
		BEGIN 
		insert into [dbo].[Fits_Temp_Demo]
			SELECT * 
			--from #nuevo_final
			from #final_forecast
		END


	drop table #sales
	drop table #initial_date
	drop table #dias_venta 
	drop table #monthly_sales_corrected
	drop table #first_matrix2
	drop table #total_sales
	drop table #daily_importance2
	drop table #monthly_importance
	drop table #second_matrix2
	drop table #sum_second_matrix2
	drop table #last_3months_sales
	drop table #last_1month_sales
	drop table #last_1week_sales
	drop table #first_instance_forecast
	drop table #sum_first_instance_forecast
	drop table #final_forecast
	drop table #original_forecast
	drop table #stores_to_replace
	drop table #sales_by_store
	drop table #days_out_of_stock_historic
	drop table #days_out_of_stock_3months
	drop table #days_out_of_stock_1month
	drop table #days_out_of_stock_1week
	drop table #store_importance
	drop table #to_delete_2
	drop table #to_delete_3
	drop table #sales_previo
	drop table #stores
	drop table #fechas
	drop table #sales_inter
	drop table #dias_por_store


END