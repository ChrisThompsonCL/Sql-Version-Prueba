﻿-- =============================================
-- Author:		Innovación
-- Create date: 2018-03-08
-- Description:	Calcula Pedidos a partir del sugerido de pedidos, a nivel de combinación día/sku/store. Solo considera los sku del Mix Activo ([Salcobrand].(select sku,store from [series].[LastStock] where sku=@sku))
			--	La fecha de inicio DEBE SER AYER (por [Salcobrand].[series].[LastStock]) y la fecha final queda sujeta a la disponibilidad de información de la Venta Simulada ([aux].[SimulatedSales]).
		--		
-- =============================================
CREATE PROCEDURE [minmax].[SIIRA_CalculateOrders]
    @sku int,
	@fecha_fin date,
	@logger bit
	,@porcentajeDUN float = null
AS
BEGIN
	SET NOCOUNT ON;

declare @fecha_inicio date = dateadd(day,-1,getdate())
declare @lai nvarchar(50)= (select LogisticAreaId from [products].[FullProducts] where sku=@sku)
declare @fecha_calculo smalldatetime = getdate()
declare @fecha_hace_tres_años date = dateadd(year,-3,getdate())

--- Aplicar Restricciones al sugerido
exec [minmax].[SIIRA_SuggestCorrectedByRestriction] @sku,null,@fecha_inicio,@fecha_fin,1

update ps set suggestMin=a1.RestrictedSuggest,
			  suggestMax=a1.RestrictedSuggest + suggestMax-suggestMin
FROM [aux].[RatesForSuggest] ps
INNER JOIN aux.SugeridoConRestriccionesNew srt on srt.StoreId=ps.Store and srt.date=ps.date
CROSS APPLY [function].[ApplyRestriction](ps.suggestMin, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a1

--select top 10 * from aux.SugeridoConRestriccionesNew

--GUARDAR SUGERIDO INICIAL Y CAMBIOS EN EL SUGERIDO POR RESTRICCIONES DURANTE EL PERIODO DE CÁLCULO DE PEDIDOS
-----------------------------------------------------------------------------------------------------------------
	
	DELETE 
	FROM [minmax].[SugeridoProyeccionPedidosNew]
	WHERE sku=@sku

	INSERT INTO [minmax].[SugeridoProyeccionPedidosNew]		
	
	SELECT rs.sku
		, rs.store
		, rs.date
		, ss.minimo
		, ss.fijo
		, ss.planograma
		, ss. mediodun 
		,null
		, ss.Maximo, ss.CajaTira, ss.FijoEstricto
	FROM (SELECT sku, store, MIN(date) as date FROM [aux].[RatesForSuggest] GROUP BY sku, store) rs
	LEFT JOIN aux.SugeridoConRestriccionesNew ss ON ss.sku = rs.sku AND ss.storeid = rs.store AND ss.date = rs.date
	WHERE  rs.sku = @sku 
	--order by rs.store, rs.date
	UNION
	SELECT rs.sku, rs.store, ss1.date, ss1.minimo, ss1.fijo, ss1.planograma, ss1. mediodun ,null, ss.Maximo, ss.CajaTira, ss.FijoEstricto
	FROM (SELECT sku, store FROM [aux].[RatesForSuggest] GROUP BY SKU, STORE) rs
	LEFT JOIN aux.SugeridoConRestriccionesNew ss ON ss.sku = rs.sku AND ss.storeid = rs.store
	LEFT JOIN aux.SugeridoConRestriccionesNew ss1 ON ss1.sku =ss.sku AND ss1.storeid = ss.storeid AND ss1.date = DATEADD(day, 1, ss.date)
	WHERE (ss.minimo <> ss1.minimo OR ss.fijo <> ss1.fijo OR ss.planograma <> ss1.planograma OR ss.mediodun <> ss1.mediodun) AND rs.sku = @sku
	--ORDER BY rs.store, ss1.date
	
------------------------------------------------------------------------------------------------------------------
DECLARE @SKU2 int = (select Case
			WHEN @sku IN (SELECT SKUTira FROM products.[CajaTira]) 
			THEN (SELECT SKUCaja FROM products.[CajaTira] WHERE SKUTira = @sku)
			ELSE @sku END)

declare @skusAux table(SKU int)

-- se aplica lógica para considerar genéricos
insert into @skusAux
select SKU 
	from products.Products
	where SKU=@sku and SKU not in (select SKU from products.GenericFamilyDetail)
union all
	select SKU 
		from products.GenericFamilyDetail 
		where GenericFamilyId=(select GenericFamilyId from products.GenericFamilyDetail where SKU=@sku)


--CREAR UNA TABLA TEMPORAL PARA REPLENISHMENT (solo contiene las combinaciones que pueden hacer un pedido ese día)
---------------------------------------------------------------------------------------------
CREATE TABLE #Replenishment (Store INT,	[Date] DATE, [Days] INT,LT INT) 
--CREATE INDEX #idxLT ON #Replenishment(Store,[Date]) 

INSERT INTO  #Replenishment
select re.Store,re.[Date],re.[Days], re.LT
from [minmax].[Replenishment] re
where [Days]>0 and re.LogisticAreaId = @lai

-- TABLA DE STOCK (MIX)
CREATE TABLE #laststock (SKU INT,Store INT,stock float, sugActual float) 
--CREATE INDEX #Idx2 ON #laststock(store)

insert into #laststock
select @sku sku, ls.store, sum(stock), MAX(ls.Suggest)  -- Se está sumando el stock de todos los sku´s de la familia por local y se les asigna a un sku		
from [series].[LastStock] ls
INNER JOIN (select distinct Store from  #Replenishment) ruta on ruta.Store=ls.Store
WHERE Suggest>0 and ls.SKU in (select sku from @skusAux) 
group by ls.Store

--CREAR UNA TABLA TEMPORAL PARA CALCULAR PEDIDOS (solo contiene las combinaciones que pueden hacer un pedido ese día)
---------------------------------------------------------------------------------------------
 CREATE TABLE #CalculoPedidos  (Store INT,  DiasACubrir INT, 	PedidoHoy FLOAT, DisponibilidadPedido DATE) 
 --CREATE INDEX #Idx3 ON #CalculoPedidos(Store)
	

--Creamos tabla con los DUN por combinación,
----------------------------------------------------------------------------
CREATE TABLE #DUNxCOMB (Store int, DUN int)
--CREATE INDEX #Idx4 ON #DUNxCOMB(Store)

insert into #DUNxCOMB
select StoreId Store, (MedioDUN-1)*2 DUN
from aux.SugeridoConRestriccionesNew
where MedioDUN >0

--CREAR UNA TABLA TEMPORAL PARA PEDIDOS EN TRÁNSITO, tiene pedidos DISPONIBLES y EN TRÁNSITO (aún no estas disponibles)
---------------------------------------------------------------------------------------------------
CREATE TABLE #PedidosTransito  ([Date] DATE,  Store INT,	DisponibilidadPedido DATE,	Pedido FLOAT)
--CREATE INDEX #Idx5 ON #PedidosTransito([Date],Store)

INSERT #PedidosTransito
SELECT  rs.[Date], rs.store, 
		--DisponibilidadPedido
		CASE
			WHEN re.LT > 0 THEN DATEADD(DAY, re.LT, rs.[Date]) 
			ELSE DATEADD(DAY, xx.LTprom, rs.[Date]) END, -- CUANDO NO HAY DESPACHO (LT=0), SE TOMA EL LTpromedio
		rs.PickedUnits
FROM (
		SELECT store, AVG(LT) LTprom 
		FROM #Replenishment  
		GROUP BY store
	) xx
inner join (
		SELECT  a.[Date],@sku Sku,a.store,sum(a.picking) PickedUnits
		FROM [series].[DespachosCD]  a
		WHERE Picking > 0  and a.sku in (select sku from @skusAux)--and sku=@sku
		group by a.[Date], a.Store
	) rs on [Date] >= DATEADD(DAY,-(LTprom+1), @fecha_inicio) and rs.Store=xx.Store
LEFT JOIN	#Replenishment re ON re.store = rs.store AND re.[Date] = rs.[Date]  


TRUNCATE TABLE  aux.Pedidos

INSERT aux.Pedidos
SELECT	@fecha_inicio  
		,dsm.SKU	
		,dsm.store  
		,ISNULL(sfc.suggestMin,0)
		,ISNULL(sfc.suggestMax,0) 
		,ISNULL(dpe.venta,0)
		--,ISNULL(sfc.suggestMin,0)   -- Stock igual a (SUG MINIMO) para evitar llenado de cañerías 
		--	+ CASE WHEN dsm.sugActual < dsm.stock THEN dsm.stock - dsm.sugActual ELSE 0 END -- + SOBRESTOCK
		,si.suggestMin   -- Stock igual a (SUG MINIMO) para evitar llenado de cañerías 
			+ CASE WHEN dsm.sugActual < dsm.stock THEN dsm.stock - dsm.sugActual ELSE 0 END -- + SOBRESTOCK
			- ISNULL(ptran.pedido,0) -- TRÁNSITO
		,0 --Pedido
		,0 --PedidoEfectivo
		,0 --PedidoDisponible
		,ptran.Pedido
		,0 --Venta Perdida			
FROM #laststock dsm 
LEFT JOIN (
	select sku,Store, suggestMin
	from (
		select SKU, Store, suggestMin, ROW_NUMBER() over (partition by SKU, Store Order by [Date] ) R
		from [aux].[RatesForSuggest] x
		where x.[days] > 0 AND x.[date] >= @fecha_inicio and x.SKU=@sku
	) a where R=1
) si on si.store=dsm.store
LEFT JOIN  [aux].[RatesForSuggest]  sfc					ON sfc.sku = dsm.SKU AND sfc.Store = dsm.Store and sfc.[Date] = @fecha_inicio
LEFT JOIN [aux].[SimulatedSales]	dpe					ON dpe.sku = dsm.SKU AND dpe.Store = dsm.Store AND dpe.[Date] = @fecha_inicio 
LEFT JOIN (
	SELECT Store, SUM (ptran.Pedido) 	Pedido
	FROM  #PedidosTransito ptran
	where ptran.DisponibilidadPedido > @fecha_inicio
	group by Store
) ptran on ptran.Store=dsm.Store
WHERE dsm.sku= @sku

-----------------------------------


-- GUARDAR FALTANTE
---------------------------------
--delete 
--from [minmax].[Faltante]
--where sku=@sku

--insert into [minmax].[Faltante]
--select getdate(), pez.sku, pez.store, pez.stock, ls.stock, pez.stock - (CASE WHEN (ls.stock < 0) THEN 0 ELSE ls.stock END)
--FROM aux.Pedidos pez
--INNER JOIN #laststock ls ON ls.store = pez.store 

----------------------------------------------------------------------------------------------------
---------------------------------- CALCULO DIA 1 AL DIA 30 -----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @fechaMaxPedidoEli DATE = (SELECT MAX([Date]) FROM aux.Pedidos)
DECLARE @diaSiguienteMax DATE = DATEADD(DAY,1,@fechaMaxPedidoEli)	

WHILE (@fechaMaxPedidoEli < @fecha_fin )
	BEGIN
	
	--INSERTAR DATOS DE LA FECHA NUEVA EN LA TABLA FIJA
	--CALCULAR STOCKHOY PARA TODOS LOS SKU
	----------------------------------------------------------------------------------------------------

	INSERT aux.Pedidos
	SELECT	@diaSiguienteMax
			,dsm.SKU
			,dsm.store
			,ISNULL(sfc.suggestMin,0)
			,ISNULL(sfc.suggestMax,0)
			,ISNULL(dpe.venta,0)
			,CASE --Stock
				WHEN ISNULL(dpe.venta,0) < pe1.Stock +			-- WHEN: Venta < StockAnterior + PedidoDisponible
					ISNULL(ptran.Pedido,0) --[PedidoDisponible]				
				THEN pe1.Stock - ISNULL(dpe.venta,0) +			--THEN: StockAnterior + PedidoDisponible - Venta
					ISNULL(ptran.Pedido,0)  -- [PedidoDisponible]
				ELSE 0 END
			,0 --PedidoHoy
			,0 --PedidoEfectivo
			,ISNULL(ptran.Pedido,0) -- [PedidoDisponible]
			,0 --PedidoEnTransito
			, CASE WHEN ISNULL(dpe.venta,0) > pe1.Stock + ISNULL(ptran.Pedido,0) -- WHEN: Venta > StockAnterior + PedidoDisponible
				 THEN ISNULL(dpe.venta,0) - (pe1.Stock + ISNULL(ptran.Pedido,0))
				 ELSE 0 END --VentaPedida
	FROM #laststock dsm 
	LEFT JOIN (select * from [aux].[RatesForSuggest] where [date] = @diaSiguienteMax) sfc	ON dsm.Store = sfc.Store
	LEFT JOIN (select * from [aux].[SimulatedSales] where [date] = @diaSiguienteMax) dpe	ON dsm.Store = dpe.Store -- Hoy 
	LEFT JOIN (select * from aux.Pedidos where [date] = @fechaMaxPedidoEli)	pe1		ON dsm.store = pe1.store -- Ayer 
	LEFT JOIN (select store,DisponibilidadPedido,sum(Pedido) Pedido from #PedidosTransito  where DisponibilidadPedido = @diaSiguienteMax group by store,DisponibilidadPedido
	) ptran ON ptran.store = dsm.Store 


	--RENOVAR TABLA TEMPORAL CON LOS DATOS DEL DIA DE HOY
	--CALCULAR PEDIDOS PARA LOS SKU QUE TIENEN DESPACHO HOY
	---------------------------------------------------------------------------------------------------
	INSERT #CalculoPedidos
	SELECT	dsm.store, 
			re.[Days],
			CASE-- Pedido Hoy
				WHEN	pe1.Stock + ISNULL(ptran.Pedido,0) --[PedidoEnTránsito]  
				 < ISNULL(sfc.suggestMin,0) 	
					AND re.Days > 0		
				THEN ISNULl(sfc.suggestMin,0) - (pe1.Stock +	ISNULL(ptran.Pedido,0)) 					 
				ELSE 0 
			END,
			DATEADD(day, re.LT, @diaSiguienteMax) --DisponibilidadPedido
    FROM #laststock dsm 
	INNER JOIN (select * from [aux].[RatesForSuggest] where [date] = @diaSiguienteMax) sfc ON dsm.Store = sfc.Store
	INNER JOIN (select * from #Replenishment where [date] = @diaSiguienteMax ) re ON dsm.Store = re.Store
	INNER JOIN (select * from aux.Pedidos where [date] = @diaSiguienteMax)  pe1 ON dsm.store = pe1.STORE  -- Fecha de HOY (STOCKHOY) 
	LEFT JOIN (SELECT store,sum(Pedido) Pedido
				FROM #PedidosTransito
				WHERE DisponibilidadPedido > @diaSiguienteMax
				GROUP BY  store) ptran						ON ptran.store = dsm.Store
	
	--INSERTAR PEDIDOHOY Y VENTAPERDIDA EN LA TABLA FIJA A PARTIR DE LA TABLA TEMPORAL
	--	OBS: FALTA CONSIDERAR AJUSTE POR CAJA-TIRA
	--------------------------------------------------------------------------------------------------------
	UPDATE pe1
	SET pe1.Pedido = ISNULL(pse.PedidoHoy,0)
		,pe1.PedidoEfectivo = 
			CASE 
				WHEN DUN IS NULL					--CUANDO NO EXISTE DUN 
				THEN ISNULL(pse.PedidoHoy,0)		--ENTONCES PEDIDOEFECTIVO=PEDIDO
				
				WHEN pe1.MinSug < (ISNULL(@porcentajeDUN,0.5) * DUN) + 1 -- (0.5 * DUN) + 1 //(0.5*ISNULL(DUN,1)) +1	-- CUANDO EL SUGERIDO ES MENOR A (0.5DUN + 1)
					AND ISNULL(pse.PedidoHoy,0) > 0							-- Y EXISTE UN PEDIDO ( pedido >0)
				THEN DUN--ISNULL(DUN,1)										-- ENTONCES PIDO DUN
				
				WHEN ISNULL(pse.PedidoHoy,0) >=  (ISNULL(@porcentajeDUN,0.5) * DUN) + 1--(0.5 * DUN) + 1 // (0.5*ISNULL(DUN,1)) +1 	-- CUANDO PEDIDO ES MAYOR O IGUAL A (0.5DUN + 1)
					AND ISNULL(pse.PedidoHoy,0) <= (ISNULL(1+@porcentajeDUN,1.5) * DUN) --  (1.5 * DUN)// (1.5*ISNULL(DUN,1))		-- Y PEDIDO ES MENOR O IGUAL A 1,5 DUN
				THEN DUN --ISNULL(DUN,1)										-- ENTONCES PIDO DUN 
				
				WHEN ISNULL(pse.PedidoHoy,0) >=  (ISNULL(1+@porcentajeDUN,1.5) * DUN) + 1--(1.5 * DUN) + 1  // (1.5*ISNULL(DUN,1)) +1 			-- CUANDO ES MAYOR A 1,5 DUN
				THEN CEILING(ISNULL(pse.PedidoHoy,0)/ DUN) * DUN--ISNULL(DUN,1)) * ISNULL(DUN,1)	-- ENTONCES PIDO EL ENTERO MAYOR
				ELSE 0 END
		--,pe1.VentaPerdida = 
		--	CASE WHEN pe2.Stock +  pe1.PedidoDisponible < pe1.Venta 
		--		 THEN pe1.Venta - (pe2.Stock +  pe1.PedidoDisponible)
		--		 ELSE 0 END
	FROM (SELECT * FROM aux.Pedidos WHERE [Date] = @diaSiguienteMax) pe1
	--INNER JOIN (SELECT Store, Stock FROM [dbo].[Pedidos_Z] WHERE [date]=@fechaMaxPedidoEli) pe2 ON pe2.Store=pe1.Store
	--LEFT JOIN #CalculoPedidos pse ON pse.Store = pe1.store
	INNER JOIN #CalculoPedidos pse ON pse.Store = pe1.store
	LEFT JOIN #DUNxCOMB d ON d.Store=pse.Store
	

		-- ELIMINAR PEDIDOS QUE YA ESTUVIERON DISPONIBLES
	------------------------------------------------------------------------------------------------------------------------------------
	DELETE FROM #PedidosTransito 	
	WHERE DisponibilidadPedido = @diaSiguienteMax

	-- INSERTAR PEDIDOS DE HOY (<> 0) EN LA TABLA DE PEDIDOS EN TRÁNSITO 
	--------------------------------------------------------------------------------------------------------
	INSERT #PedidosTransito
	SELECT @diaSiguienteMax, pse.Store, pse.DisponibilidadPedido, pe1.PedidoEfectivo
	FROM #CalculoPedidos pse		
	INNER JOIN (select * from aux.Pedidos where [date]=@diaSiguienteMax) pe1 ON pse.store = pe1.store 
	WHERE pe1.PedidoEfectivo > 0

	TRUNCATE TABLE #CalculoPedidos

	SET @fechaMaxPedidoEli = @diaSiguienteMax
	SET @diaSiguienteMax = DATEADD(DAY,1,@fechaMaxPedidoEli)

END -- Cierra WHILE


	-- guardar fecha de calculo
	delete 
	from minmax.CalculationDateDistributionOrdersNew 
	where sku=@sku

	insert into minmax.CalculationDateDistributionOrdersNew
	select @sku,@fecha_calculo

	-- guardar pedidos
	delete 
	from minmax.DistributionOrdersForecastNew 
	where sku=@sku

	insert into minmax.DistributionOrdersForecastNew
	select [Date],sku,sum(venta) Venta,sum(stock) Stock,sum(pedido) Pedido,sum(pedidoefectivo) PedidoEfectivo
		,sum(pedidodisponible) PedidoDisponible,sum(pedidoentransito) PedidoEnTransito, sum(VentaPerdida), sum(MinSug),null 
	from aux.Pedidos
	group by [Date],sku

	-- borrar sku de la tabla con los sku de las nuevas restricciones
	delete 
	from [minmax].[SkuNewRestrictionsNew]
	where sku=@sku

	delete from minmax.SkuNewRestrictionsNew where sku=@sku

    drop table #Replenishment
	drop table #laststock
	drop table #CalculoPedidos
	drop table #DUNxCOMB
	drop table #PedidosTransito

END
