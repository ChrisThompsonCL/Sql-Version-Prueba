﻿

-- =============================================
-- Author:		Carlos Astroza
-- Create date: 2018-01-23
-- Description:	Ejecuta todos los procesos para dejar el sugerido con min/max del dia calculado
-- =============================================
CREATE PROCEDURE [minmax].[UpdateServiceLevel] 
	@date smalldatetime = NULL
AS
BEGIN
	   
	   /* UPDATE-INSERT Nivel de servicio */

		DECLARE @tabla_serviceLevel TABLE (
		SKU INT,
		ServiceLevel INT,
		ServiceLevelDesc NVARCHAR(10)
		)

		insert into @tabla_serviceLevel
		SELECT SKU
			, case TotalRanking when 'A' then 1 when 'B' then 1 when 'C' then 3 else 5 end SLid
			, case TotalRanking when 'A' then '98' when 'B' then '98' when 'C' then '97' else '95' end SLDesc
		FROM products.Products p
		WHERE SKU in (select sku from operation.ServicesProductsDetail where serviceId=2)
			and SKU not in (select SKU from products.genericfamilydetail)
		union all
		SELECT SKU
			, case TotalRanking when 'A' then 1 when 'B' then 1 when 'C' then 3 else 5 end SLid
			, case TotalRanking when 'A' then '98' when 'B' then '98' when 'C' then '97' else '95' end SLDesc
		FROM products.FullProducts p
		WHERE SKU in (select SKU from products.genericfamilydetail)

		update tabla
		set ServiceLevel=1, ServiceLevelDesc='98'
		from @tabla_serviceLevel tabla
		inner join products.Products p on p.SKU= tabla.SKU
		where p.ProCuidado = 1

		update tabla
		set ServiceLevel=8, ServiceLevelDesc='99'
		from @tabla_serviceLevel tabla
		inner join products.Products p on p.SKU= tabla.SKU
		where p.Ges=1

		update tabla
		set ServiceLevel=7, ServiceLevelDesc='99.9'
		from @tabla_serviceLevel tabla
		inner join products.Products p on p.SKU= tabla.SKU
		where IsInfaltable=1

		MERGE INTO minmax.SuggestParameters as psl
		USING @tabla_serviceLevel AS p
		on p.sku = psl.sku
		WHEN MATCHED AND psl.UserResp = 'SYSTEM' THEN
		UPDATE SET psl.ServiceLevel = p.ServiceLevel, psl.ServiceLevelDesc = p.ServiceLevelDesc, psl.LastUpdate = sysdatetime(), psl.UserResp = 'SYSTEM'
		WHEN NOT MATCHED BY TARGET THEN
		INSERT (sku, ServiceLevel, ServiceLevelDesc, LastUpdate, UserResp)
		VALUES (p.sku,p.ServiceLevel, p.ServiceLevelDesc, sysdatetime(), 'SYSTEM')	  
		WHEN NOT MATCHED BY SOURCE
        THEN DELETE;

		/* END */

		/* UPDATE-INSERT Días de sobreprotección */

		DECLARE @table_safetyDays table (
		SKU INT,
		DAYS INT
		)

		INSERT INTO @table_safetyDays 
		SELECT d.SKU ,0 SP
		FROM operation.ServicesProductsDetail d
		WHERE ServiceId=2
			and SKU not in (select SKU from products.genericfamilydetail)
		union all
		SELECT d.SKU, case when TotalRanking in ('C','D') then 2 else 0 end SP
		FROM products.genericfamilydetail d
		inner join products.fullproducts fp on fp.SKU=d.SKU

	  MERGE INTO minmax.SuggestParameters as psf
	  USING @table_safetyDays AS p
	  on p.sku = psf.sku
	  WHEN MATCHED AND psf.UserResp = 'SYSTEM' THEN
	  UPDATE SET psf.safetydays = p.days
	  WHEN NOT MATCHED BY TARGET THEN
	  INSERT (SKU, SafetyDays)
	  VALUES (p.sku, p.days)
	  WHEN NOT MATCHED BY SOURCE
      THEN DELETE;



	  /* END */

END

