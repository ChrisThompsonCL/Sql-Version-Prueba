﻿-- =============================================
-- Author:		Dani y JuanM <3
-- Create date: 2018-01-17
-- Description:	migra query de código a SP
-- Modificado 2018-04-13  JuanM 
-- =============================================
CREATE PROCEDURE [minmax].[GetSuggestsForDay] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @date date = cast(GETDATE() as date)

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	EXEC [br].[ApplySuggestRestrictionLite]    

	--respaldo de restricciones al momento de cargar sugerido
	truncate table minmax.LastSuggestRestriction

	--Modificado 2018-04-16 JM --
	insert into minmax.LastSuggestRestriction 
			([SKU]
           ,[Store]
           ,[Suggest]
           ,[Minimo]
           ,[Fijo]
           ,[Planograma]
           ,[MedioDUN]
           ,[Maximo]
           ,[CajaTira]
           ,FijoEstricto
           ,[MinimoProd_Restricción]
           ,[MinimoProd_Usuario]
           ,[MinimoProd_Archivo]
           ,[MinimoComb_Restricción]
           ,[MinimoComb_Usuario]
           ,[MinimoComb_Archivo]
           ,[MinimoComb_Término]
           ,[CargaManual_Restricción]
           ,[CargaManual_Usuario]
           ,[CargaManual_Archivo]
           ,[CargaManual_Término]
           ,[Planograma_Restricción]
           ,[Planograma_Usuario]
           ,[Planograma_Archivo]
           ,[DUN_Restricción]
           ,[DUN_Usuario]
           ,[DUN_Archivo]
           ,[MaximoProd_Restricción]
           ,[MaximoProd_Usuario]
           ,[MaximoProd_Archivo]
           ,[MaximoComb_Restricción]
           ,[MaximoComb_Usuario]
           ,[MaximoComb_Archivo]
           ,[MaximoComb_Término]
           ,[CajaTira_Restricción]
           ,[CajaTira_Usuario]
           ,[CajaTira_Archivo]
           ,[FijoEstricto_Restricción]
           ,[FijoEstricto_Usuario]
           ,[FijoEstricto_Archivo]
           ,[FijoEstricto_Término])
	SELECT a.SKU,
			a.Store,
			a.Suggest,
			a.Minimo,
			a.Fijo,
			a.Planograma,
			a.MedioDUN, 
			a.Maximo,
			a.CajaTira,
			a.FijoEstricto,
			b.MinimoProd_Restricción ,
			b.MinimoProd_Usuario ,
			b.MinimoProd_Archivo ,
			b.MinimoComb_Restricción ,
			b.MinimoComb_Usuario ,
			b.MinimoComb_Archivo ,
			b.MinimoComb_Término,
			b.CargaManual_Restricción ,
			b.CargaManual_Usuario,
			b.CargaManual_Archivo ,
			b.CargaManual_Término,
			b.Planograma_Restricción ,
			b.planograma_Usuario ,
			b.planograma_Archivo ,
			cast(b.[DUN_Restricción] as int) DUN_Restricción ,
			b.DUN_Usuario ,
			b.DUN_Archivo,
			b.MaximoProd_Restricción,
			b.MaximoProd_Usuario,
			b.MaximoProd_Archivo,
			b.[MaximoComb_Restricción],
			b.[MaximoComb_Usuario],
			b.[MaximoComb_Archivo],
			b.[MaximoComb_Término],
			b.[CajaTira_Restricción],
			b.[CajaTira_Usuario],
			b.[CajaTira_Archivo],
			b.[FijoEstricto_Restricción],
			b.[FijoEstricto_Usuario],
			b.[FijoEstricto_Archivo],
			b.[FijoEstricto_Término]
	FROM #SuggestRestrictionToday a
	--FROM [view].SuggestRestrictionToday a
	inner join [view].[SuggestRestrictionMatrix] b on b.SKU = a.SKU and b.Store = a.Store


	
	-- eliminar los que no provoquen cambios
	delete s from [replenishment].[NextSuggest] s
	inner join series.LastStock l on s.SKU = l.SKU and s.Store = l.Store
	where s.[Max]=l.[Max] and s.[Min]=l.[Min]


	--respaldo historico de sugerido

     truncate table suggest.suggestdownload

     insert into suggest.suggestdownload ([SKU],[Store],[Max],[Min],[Rate],[Days], LeadTime, SafetyDays, ServiceLevel, Minimo, Fijo, Planograma, MedioDUN, Maximo, CajaTira, FijoEstricto) 
     SELECT [SKU],[Store],[Max],[Min],[Rate],[Days] , LeadTime, SafetyDays, ServiceLevel, Minimo, Fijo, Planograma, MedioDUN, Maximo, CajaTira, FijoEstricto
	 FROM [replenishment].[NextSuggest] 

	 delete from suggest.suggestdownloadhistory where [Date]=@date
     
	 insert into suggest.suggestdownloadhistory ([SKU],[Store],[Max],[Min],[Rate],[Days],[Date], LeadTime, SafetyDays, ServiceLevel, Minimo, Fijo, Planograma, MedioDUN, Maximo, CajaTira, FijoEstricto)
     select [SKU],[Store],[Max],[Min],[Rate],[Days],@date as [Date] , LeadTime, SafetyDays, ServiceLevel, Minimo, Fijo, Planograma, MedioDUN, Maximo, CajaTira, FijoEstricto
	 from suggest.suggestdownload
    
	--genera data del archivo

     select Store AS [Local]
	 , RIGHT(CONCAT('0000000', convert(varchar,SKU)), 7) SKU
	 , 'R' AS  OPR1,[min] Minimo,'R' AS  OPR2,[max] Maximo 
     , 'F' AS FinFila  from suggest.[SuggestDownload]
END
