﻿-- =============================================
-- Author:		Mauricio Sepúlvea
-- Create date: Carga la propuesta diaria de sugeridos
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [minmax].[LoadDailySuggestProposal]
	-- Add the parameters for the stored procedure here
	@date smalldatetime = NULL
AS
BEGIN
	DECLARE @msg VARCHAR(100) 
	SET NOCOUNT ON;

    -- Cargar replenishment.nextSuggest
	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Cargando [replenishment].[NextSuggest]...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	
	truncate table [replenishment].[NextSuggest]

	INSERT INTO [replenishment].[NextSuggest]([SKU],[Store],[Max],[Min],[Rate],[Days],[LS],[OS],[Date], Leadtime, SafetyDays, ServiceLevel, Minimo, Fijo, Planograma, MedioDUN, Maximo, CajaTira, FijoEstricto)
	select p.[SKU],p.[Store],p.SuggestMax,p.SuggestMin,[Rate],[Days], 0, 0 ,p.[Date], p.LeadTime, p.SafetyDays, p.ServiceLevel, p.Minimo, p.Fijo, p.Planograma, p.MedioDUN, Maximo, CajaTira, FijoEstricto
	from minmax.PricingSuggest p 

	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando sugeridos a la baja que suban en el corto plazo...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	--create table #restrBajas (SKU int, Store int, MaximoPropuesto int, MinimoPropuesto int, MaximoActual int, MinimoActual int, StockActual int, Bodega nvarchar(20), Ranking nvarchar(10), SugMin_Maximo int, EfectoMinMax int)
	--insert into #restrBajas
	--select A.* 
	--	, SugMin_Maximo= m3.Maximo -- máximo SugestMin futuro con restricciones aplicadas
	--	, EfectoMinMax=(MaximoPropuesto-MinimoPropuesto)
	--from (
	--	select NS.SKU, NS.Store, NS.[max] MaximoPropuesto, NS.[min] MinimoPropuesto, 
	--		LS.[Max] MaximoActual, LS.[MIN] MinimoActual, LS.Stock StockActual,
	--		FP.LogisticAreaDesc Bodega, FP.TotalRanking Ranking 
	--	from [replenishment].[NextSuggest] NS
	--	inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
	--	inner join products.Products FP on NS.SKU=FP.SKU
	--	where NS.[min]<LS.[Min] --casos en los que haré bajar el sugerido actual
	--) A
	--inner join [minmax].[SuggestForecastCorrected] f on A.SKU=f.SKU and A.Store=f.Store --minimos con restr
	--left join [view].SuggestRestrictionToday srt on srt.SKU=A.SKU and srt.Store=A.Store
	--cross apply [function].Maximo(f.W1, f.W2) m1
	--cross apply [function].Maximo(m1.Maximo, f.W3) m2
	--cross apply [function].Maximo(m2.Maximo, f.W4) m3
	--where m3.Maximo > A.MinimoPropuesto

	--/*2017-09-29 Daniela Albornoz
	--Piloto: dejar combinaciones sin restricción de bajas */
	--delete a --select *
	--from #restrBajas a
	--inner join (
	--	select SKU, Store
	--	from temp.PilotoRestrRusso
	--	where Grupo='Piloto'
	--) b on b.SKU=a.SKU and b.Store=a.Store


	----registro cambios
	--delete from minmax.LogReglaBajas where Fecha in (select distinct [Date] from replenishment.NextSuggest)

	--insert into minmax.LogReglaBajas(Fecha, TotalCombs, CambioMin, CambioMax)
	--select [date], COUNT(*), SUM(CambioMin), SUM(CambioMax)
	--from (
	--	select n.[Date], case when SugMin_Maximo > MinimoActual then MinimoActual-MinimoPropuesto else SugMin_Maximo-MinimoPropuesto end CambioMin
	--		, case when SugMin_Maximo > MinimoActual then MaximoActual-MaximoPropuesto else (SugMin_Maximo+EfectoMinMax)-MaximoPropuesto end CambioMax
	--	from [replenishment].[NextSuggest] n
	--	inner join #restrBajas p on n.SKU = p.SKU and n.Store = p.Store
	--) a
	--group by [date]

	----no borro, corrijo
	--update n set [min] = case when SugMin_Maximo > MinimoActual then MinimoActual else SugMin_Maximo end
	--	, [max] = case when SugMin_Maximo > MinimoActual then MaximoActual else SugMin_Maximo+EfectoMinMax end
	--from [replenishment].[NextSuggest] n
	--inner join #restrBajas p on n.SKU = p.SKU and n.Store = p.Store

	--drop table #restrBajas

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos en alza por feriado...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- borro alza por feriado
	delete n
	from [replenishment].[NextSuggest] n
	inner join (
	select X.SKU, X.Store, X.EndDate
		from (
			select A.*,f.MaxHolidays , f.EndDate
			from(
				Select NS.SKU, NS.Store, NS.Max MaximoPropuesto, NS.Min MinimoPropuesto, 
				LS.Max MaximoActual, LS.Min MinimoActual, LS.Stock StockActual,
				FP.LogisticAreaDesc Bodega, FP.TotalRanking Ranking 
				from replenishment.NextSuggest NS
				inner join series.LastStock LS on NS.SKU=LS.SKU and NS.Store=LS.Store
				inner join products.Products FP on NS.SKU=FP.SKU
			) A
			inner join minmax.HolidaySuggestNew f on A.SKU=f.SKU and A.Store=f.Store
			where dateadd(day,2,@date) >= f.BeginDate and @date <= f.EndDate
		) X where X.MaxHolidays>=X.MaximoPropuesto
	) p on n.SKU = p.SKU and n.Store = p.Store and n.[Date] <= EndDate

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos en promoción...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- eliminar promociones y próximas promociones (que partan hasta en un mes más - hasta que terminen en más de 5 días)
	-- excepto a farma precio
	DELETE ps FROM [replenishment].[NextSuggest] ps
	INNER JOIN promotion.PromotionStock pr ON pr.ProductId=ps.SKU and ps.[Date] between DATEADD(month, -1, BeginDate) and DATEADD(day,-5,EndDate)
	WHERE ps.Store not in  (select store from temp.LocalesFarmaPrecio)
		and pr.DebeCalcularSugerido=0
	

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando productos con sugerencia cero o negativa...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--borro sugerido cero
	delete from replenishment.NextSuggest where ([Max]<=0 OR [Min]<=0)

	--/*SECCION PILOTO SUGERIDO MANUAL*/
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Respaldo sugerencia manual...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	--truncate table [test].[NextSuggest]

	--delete from [replenishment].[NextSuggest] where Store= 565 --local ecommerce
	--delete from minmax.[UnrestrictedPricingSuggest] where Store= 565 --local ecommerce


	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminando los sugeridos que no provoquen cambios...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	/*COMENTADO POR DANI 04-06-2018. SE MUEVE AL MOMENTO QUE SE CARGA EL SUGERIDO*/
	---- eliminar los que no provoquen cambios
	--delete s from [replenishment].[NextSuggest] s
	--inner join series.LastStock l on s.SKU = l.SKU and s.Store = l.Store
	--where s.[Max]=l.[Max] and s.[Min]=l.[Min]
	
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Eliminación Lista...' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Carga [replenishment].[NextSuggest] Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	-- Generar movimiento de sugerido 
	
	/*ESTA PARTE CORRESPONDE AL PROYECTO DE REPOSICIONES, NO SE USA. COMENTADO EL 2017-09-07 POR DANI*/
	---- actualizar venta perdida esperada
	--update n set n.LS = h.ls from replenishment.NextSuggest n inner join (
	--select n.SKU,n.Store,	
	--	case 
	--		when l.[Min]<n.[Min] then [function].GetLostSaleForSuggest(rate,l.[Min])-[function].GetLostSaleForSuggest(rate,n.[Min]) 	
	--		else 0 end 
	--	ls
	--from replenishment.NextSuggest n 
	--inner join series.LastStock l on n.SKU = l.SKU and n.Store = l.Store
	--where l.[Min]<n.[Min] or l.[Max]<n.[Max]) h on h.SKU = n.SKU and h.Store = n.Store WHERE n.[Date]=@date
	
	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de Venta Perdida Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

	---- actualizar costo de inventario
	--update n set n.OS = h.OS from replenishment.NextSuggest n inner join (select n.SKU,n.Store,case when l.[Max]>n.[Max] then l.[Max]-n.[Max] 
	--else case when l.[Min]>n.[Min] then l.[Min]-n.[Min] else 0 end end OS from replenishment.NextSuggest n inner join series.LastStock l on n.SKU = l.SKU and n.Store = l.Store
	--where l.[Min]>n.[Min] or l.[Max]>n.[Max]) h on h.SKU = n.SKU and h.Store = n.Store WHERE n.[Date]=@date

	--SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Actualización de Costos de Inventario Lista' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

END
