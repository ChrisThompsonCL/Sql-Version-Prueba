﻿-- =============================================
-- Author:		Gonzalo Cornejo
-- Create date: 2017-10-24
-- Description:	Script para correr todas las categorías para el Share Local
-- =============================================
CREATE PROCEDURE [minmax].[GenerateFits_FullCategory]
	@FinPasado datetime
AS
BEGIN
	SET NOCOUNT ON;


	declare @auxCat int, @msg nvarchar(max)

	declare cursor_category cursor fast_forward 
	for
		select distinct CategoryId from products.Products order by CategoryId

	open cursor_category
	fetch next from cursor_category into @auxCat

	while @@fetch_status = 0
	begin


		exec [minmax].[GenerateFits_Category]  @auxCat, @FinPasado
		 
		set @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'cat '+cast(@auxCat as nvarchar(10))
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		
		fetch next from cursor_category into @auxCat

	end

	close cursor_category
	deallocate cursor_category

	SELECT fin_futuro, category_ID
		, COUNT(distinct Store_id) Locales
		, COUNT(distinct [month]) Meses
		, COUNT(distinct [Day]) Dias
		, round(SUM(importance)/COUNT(distinct Store_id),4) Imp
	  FROM [Salcobrand].[minmax].[Fits_CategoryImportance]
	  group by fin_futuro, category_ID
	  order by fin_futuro, category_ID
END
