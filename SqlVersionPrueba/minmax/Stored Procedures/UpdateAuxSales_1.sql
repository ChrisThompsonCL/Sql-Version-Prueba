﻿-- =============================================
-- Author:		Plan Z
-- Create date: 2017-12-15
-- Description:	venta auxiliar para cálculo de sugerido
-- =============================================
CREATE PROCEDURE [minmax].UpdateAuxSales
	-- Add the parameters for the stored procedure here
	@beginning_date date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- sales auxiliar
	truncate table Salcobrand.aux.SalesCalculoFit

	insert into Salcobrand.aux.SalesCalculoFit
	select sku,[Store] as store_id, date as sale_date ,Quantity
	from Salcobrand.[series].[Sales]
	where date < @beginning_date and date >= DATEADD(yy, -1, @beginning_date)

END
