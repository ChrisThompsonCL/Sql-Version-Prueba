﻿-- =============================================
-- Author:		Mauricio Sepúlveda
-- Create date: 2012-06-15
-- Description:	calcula las restricciones 
-- =============================================
CREATE PROCEDURE [minmax].[SuggestRestriction] 
	-- Add the parameters for the stored procedure here
	@date date = NULL
AS
BEGIN

	declare @sku int=null,
	@fecha_creacion datetime=null,
	@fecha_inicio datetime = cast(getdate() as date), 
	@fecha_fin datetime = cast(getdate() as date),
	@ajuste_new bit= 0
	
	create table #SuggestRestrictionToday (sku int, store int,Fecha date, suggest int, Minimo int, Fijo int, Planograma int, MedioDUN int, Maximo int, CajaTira int, FijoEstricto int)
	
	insert into #SuggestRestrictionToday
	exec [minmax].[SIIRA_SuggestCorrectedByRestriction]  @sku,@fecha_creacion,@fecha_inicio,@fecha_fin,@ajuste_new

	update ps set SuggestMax=a1.RestrictedSuggest+(SuggestMax-SuggestMin)
		, SuggestMin=a1.RestrictedSuggest
		, Minimo = srt.Minimo, Fijo=srt.Fijo, Planograma=srt.Planograma, MedioDUN=srt.MedioDUN, Maximo=srt.Maximo, CajaTira=srt.CajaTira, FijoEstricto=srt.FijoEstricto
	from minmax.PricingSuggest ps
	--inner join [view].SuggestRestrictionToday srt on srt.SKU=ps.SKU and srt.Store=ps.Store
	inner join #SuggestRestrictionToday srt on srt.SKU=ps.SKU and srt.Store=ps.Store
	cross apply [function].[ApplyRestriction](ps.SuggestMin, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a1

	update ps set SuggestMax = SuggestMin
	from minmax.PricingSuggest ps
	where ps.SuggestMin > ps.SuggestMax

	/*sugerido futuro*/
	update ps set W1=a1.RestrictedSuggest
			, W2=a2.RestrictedSuggest
			, W3=a3.RestrictedSuggest
			, W4=a4.RestrictedSuggest
	from minmax.SuggestForecastCorrected ps
	--inner join [view].SuggestRestrictionToday srt on srt.SKU=ps.SKU and srt.Store=ps.Store
	inner join #SuggestRestrictionToday srt on srt.SKU=ps.SKU and srt.Store=ps.Store
	cross apply [function].[ApplyRestriction](ps.W1, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a1
	cross apply [function].[ApplyRestriction](ps.W2, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a2
	cross apply [function].[ApplyRestriction](ps.W3, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a3
	cross apply [function].[ApplyRestriction](ps.W4, srt.Minimo, srt.Fijo, srt.Planograma, srt.MedioDUN, srt.Maximo, srt.CajaTira, srt.FijoEstricto) a4

		
END

