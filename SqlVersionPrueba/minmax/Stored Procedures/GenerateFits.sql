﻿-- ==========================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-09-28
-- ==========================================================================================
CREATE PROCEDURE [minmax].[GenerateFits] 
	-- Add the parameters for the stored procedure here
	@beginning_date datetime,
	@finishing_date datetime
AS
BEGIN
	SET NOCOUNT ON;

	declare @FinPasado date
	set		@FinPasado = (select dateadd(dd, -1, @beginning_date))
	EXEC [minmax].[GenerateFits_FullCategory] @FinPasado
		
	declare @fecha_creacion smalldatetime = getdate()
	declare @auxSku int
	declare @auxDate date, @auxMirrorDate date
	declare @fechacalc date = (select FechaCalculo from Salcobrand.aux.UltimoPronostico)


	/***********************/
	/*cálculo de tasas base*/
	/***********************/

	declare cursor_skus cursor fast_forward 
	for
		select distinct productid as sku
		from [forecast].[PredictedDailySales]
		where datekey = @fechacalc

	open cursor_skus
	fetch next from cursor_skus into @auxSku

	while @@fetch_status = 0
	begin
		-- Borrar la tabla temporal 
		truncate table [aux].Fits_Temp

		declare @hora smalldatetime = getdate()
		insert into [Temporal].[dbo].[1Pruebas_Performance_GenerateFits]
		select @fecha_creacion, @hora, @auxSku

		exec [minmax].[GenerateFits_Base]  @auxSku, @beginning_date, @finishing_date

		--delete from minmax.Rates where sku=@auxSku and [date]=@auxDate and tagCreacion=@forecast_tag  -- Eliminar tagCreacion=@forecast_tag (deseable), date between fechas (necesario)
		delete from minmax.[FitsAllNew] where sku=@auxSku

		insert into [minmax].[FitsAllNew] (fechacalculo, fechacreacion, SKU, Store ,[date], Fit)
		select @fechacalc,@fecha_creacion, sku, store, [date], fit
		from [aux].Fits_Temp

		fetch next from cursor_skus into @auxSku

	end

	close cursor_skus
	deallocate cursor_skus


END
