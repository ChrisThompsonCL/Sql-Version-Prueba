﻿-- ====================================================================================================================================================================================
-- Author:		Carlos Quappe
-- Create date: 2017-10-06
-- Description:	(descripcion actualizada a julio 2017, con el proceso antiguo)
-- Primera version del nuevo sugerido a partir de pronosticos diarios para una categoría
-- Cuando un local tiene menos de 12 meses de historia para un producto, queremos reemplazar la información de este local-producto por la información 
-- de este local-categoria. Es por esto, que el flujo debiese ser correr este procedimiento para una categoria, luego correr todos los productos de esa 
-- categoria mientras paralelamente se va corriendo otra categoria. Los resultados local-categoria deben estar guardados para que el procedimiento 
-- [GetNewSuggestNaive] consulte estos resultados en caso de ser necesario..
-- 2018-01-25: Cambia Products por FullProducts al crear tabla #skus
-- ====================================================================================================================================================================================
CREATE PROCEDURE [minmax].[GenerateFits_Category]
	-- Add the parameters for the stored procedure here
	@categoryID1 int,
	@fecha_fin_futuro1 date
AS
BEGIN

	declare @categoryID int = @categoryID1
	declare @fecha_fin_futuro date = @fecha_fin_futuro1

	create table #sales (store_id int, sale_date date, quantity float)
	create table #first_matrix2 (store_id int, Month int, Day int, quantity float)
	create table #total_sales (store_id int, Quantity float)
	create table #daily_importance2 (store_id int, Day int, quantity float)
	create table #monthly_importance(store_id int, Month int, Quantity float)
	create table #second_matrix2 (store_id int, Month int, Day int, quantity float)
	create table #skus (sku int)


	SET NOCOUNT ON;
	

	insert into #skus
	select sku
	from [products].[FullProducts]--[products].[Products]  -- Este fue el cambio del 2018-01-25
	where CategoryId = @categoryID
		and SKU not in (select sku from products.genericfamilydetail)

	if @categoryID=1
	begin

		insert into #skus 
		select sku from products.genericfamilydetail

	end


	insert into #sales
	select [Store], date ,sum([Quantity]) as Quantity
	from [series].[Sales]
	where sku in (select sku from #skus) and date <= @fecha_fin_futuro
	group by [Store], date


	insert into #first_matrix2
	select	store_id,
			datepart(m,sale_date) as NombreMes,
			datepart(dw,sale_date) as NombreDia,
			sum(quantity) as Quantity
	from #sales
	group by store_id,datepart(m,sale_date),datepart(dw,sale_date)


	/*********************************************************************************************************************/
	/*Se rellenan con 0's todos los locales que tienen menos de 7 días de la semana (por ejemplo, local no vende domingo)*/
	/*********************************************************************************************************************/
	-- Queda postergado, ya que habria que recorrer cada local-mes, y ver los local-mes con menos de 7 dias de info.
	-- Luego, para estos local-mes, ver si tiene Lunes, Martes, .., Domingo y rellenear los que falten
	/*
	select	store_id, Month, count(distinct Day) as #dias
	into	#stores_to_fill_t  -- drop table #stores_to_fill_t
	from	#first_matrix2		-- select * from #stores_to_fill_t
	group by store_id, Month
	having	count(distinct Day) < 7

	select distinct store_id
	into #stores_to_fill  -- drop table #stores_to_fill
	from #stores_to_fill_t

	select * from #first_matrix2 where store_id in (select * from #stores_to_fill) order by store_id, month, day
	*/
	/*********************************************************************************************************************/
	/*Se rellenan con 0's todos los locales que tienen menos de 7 días de la semana (por ejemplo, local no vende domingo)*/
	/*********************************************************************************************************************/


	insert into #total_sales
	select	store_id, 
			sum(Quantity) as Quantity
	from #sales
	group by store_id


	insert into #daily_importance2
	select	f.store_id,
			day,
			sum(f.Quantity)/t.Quantity as Quantity
	from #first_matrix2 f inner join #total_sales t on f.store_id = t.store_id
	group by f.store_id,day,t.Quantity


	insert into #monthly_importance
	select	s.store_id,
			datepart(m,sale_date) as NombreMes,
			sum(s.Quantity)/t.Quantity as Quantity
	from #sales s inner join #total_sales t on s.store_id = t.store_id
	group by s.store_id, datepart(m,sale_date), t.Quantity
	order by s.store_id, nombremes


	/*****************************************************************************************************/
	/*Reemplaza las tiendas que no tienen informacion de 12 meses por una distribucion sin estacionalidad*/
	/*****************************************************************************************************/
	select store_id--, count(Month) as mes
	into #stores_to_replace
	from #monthly_importance
	group by store_id
	having count(Month) < 12

	delete from #monthly_importance 
	where store_id in (select store_id from #stores_to_replace)

	declare	@store_aux int
	declare cursor_stores cursor fast_forward 
	for
		select store_id from #stores_to_replace
	open cursor_stores
	fetch next from cursor_stores into @store_aux

	while @@fetch_status = 0
	begin
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-01-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-02-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-03-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-04-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-05-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-06-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-07-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-08-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-09-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-10-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-11-01'), 1/(1.0*12)
		insert into #monthly_importance
		select @store_aux, datepart(month, '2017-12-01'), 1/(1.0*12)

		fetch next from cursor_stores into @store_aux

	end

	close cursor_stores
	deallocate cursor_stores

	/*****************************************************************************************************/
	/*Reemplaza las tiendas que no tienen informacion de 12 meses por una distribucion sin estacionalidad*/
	/*****************************************************************************************************/


	insert into #second_matrix2
	select	d.store_id,
			m.Month,
			d.Day,
			d.Quantity * m.Quantity as quantity
	from	#daily_importance2 d inner join #monthly_importance m on d.store_id = m.store_id


	delete r from [minmax].[Fits_CategoryImportance] r where (r.category_ID = @categoryID)
	insert into [minmax].[Fits_CategoryImportance]
	select	@fecha_fin_futuro1,
			@categoryID,
			store_id,
			month,
			day,
			quantity
	from	#second_matrix2

	drop table #sales
	drop table #first_matrix2
	drop table #total_sales
	drop table #daily_importance2
	drop table #monthly_importance
	drop table #second_matrix2
	drop table #skus
	drop table #stores_to_replace


END