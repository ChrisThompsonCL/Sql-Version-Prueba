﻿CREATE TABLE [minmax].[FitsAllCurrent] (
    [FechaCalculo]  SMALLDATETIME NOT NULL,
    [fechacreacion] SMALLDATETIME NOT NULL,
    [SKU]           INT           NOT NULL,
    [Store]         INT           NOT NULL,
    [Date]          DATE          NOT NULL,
    [Fit]           FLOAT (53)    NULL,
    CONSTRAINT [PK_FitsAllCurrent] PRIMARY KEY CLUSTERED ([FechaCalculo] ASC, [SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FistAllCurrent_Date]
    ON [minmax].[FitsAllCurrent]([Date] ASC)
    INCLUDE([SKU], [Store], [Fit]);

