﻿CREATE TABLE [minmax].[MinMaxAuxData] (
    [SKU]      BIGINT     NOT NULL,
    [Store]    BIGINT     NOT NULL,
    [Date]     DATE       NOT NULL,
    [Rate]     FLOAT (53) NULL,
    [RateNext] FLOAT (53) NULL,
    [SLMax]    FLOAT (53) NULL,
    [SLMin]    FLOAT (53) NULL,
    CONSTRAINT [PK_MinMaxAuxData] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

