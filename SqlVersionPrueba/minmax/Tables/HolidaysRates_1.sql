﻿CREATE TABLE [minmax].[HolidaysRates] (
    [SKU]        INT        NULL,
    [Store]      INT        NULL,
    [Date]       DATE       NULL,
    [Rate]       FLOAT (53) NULL,
    [RateBefore] FLOAT (53) NULL,
    [RateNext]   FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidaysRates_SKU]
    ON [minmax].[HolidaysRates]([SKU] ASC);

