﻿CREATE TABLE [minmax].[PricingSuggest] (
    [SKU]          BIGINT     NOT NULL,
    [Store]        INT        NOT NULL,
    [Date]         DATE       NOT NULL,
    [SuggestMin]   INT        NULL,
    [SuggestMax]   INT        NULL,
    [Days]         INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [Minimo]       INT        NULL,
    [Fijo]         INT        NULL,
    [Planograma]   INT        NULL,
    [MedioDUN]     INT        NULL,
    [Maximo]       INT        NULL,
    [CajaTira]     INT        NULL,
    [FijoEstricto] INT        NULL,
    CONSTRAINT [PK_PricingSuggest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PricingSuggest_Store]
    ON [minmax].[PricingSuggest]([Store] ASC)
    INCLUDE([SKU], [Date], [SuggestMax]);


GO
CREATE NONCLUSTERED INDEX [IX_PricingSuggest_DateSugMin]
    ON [minmax].[PricingSuggest]([Date] ASC, [SuggestMin] ASC)
    INCLUDE([SKU], [Store], [SuggestMax]);

