﻿CREATE TABLE [minmax].[OutsideMixSuggestData] (
    [SKU]                INT          NOT NULL,
    [Store]              INT          NOT NULL,
    [Suggest]            INT          NULL,
    [TotalRanking]       NVARCHAR (1) NULL,
    [YearQuantity]       FLOAT (53)   NULL,
    [MonthQuantity]      FLOAT (53)   NULL,
    [Month2Quantity]     FLOAT (53)   NULL,
    [Month3Quantity]     FLOAT (53)   NULL,
    [Month6Quantity]     FLOAT (53)   NULL,
    [MonthSeasonQuanity] FLOAT (53)   NULL,
    [Cost]               FLOAT (53)   NULL,
    [ExecutionDate]      DATE         NULL,
    [SaleDate]           DATE         NULL,
    CONSTRAINT [PK_OutsideMixSuggestData] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

