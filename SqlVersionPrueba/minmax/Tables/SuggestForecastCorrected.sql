﻿CREATE TABLE [minmax].[SuggestForecastCorrected] (
    [SKU]   BIGINT NOT NULL,
    [Store] INT    NOT NULL,
    [W1]    INT    NULL,
    [W2]    INT    NULL,
    [W3]    INT    NULL,
    [W4]    INT    NULL,
    CONSTRAINT [PK_SuggestForecast] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

