﻿CREATE TABLE [minmax].[PoissonSuggest] (
    [ServiceLevel] FLOAT (53) NOT NULL,
    [Suggest]      FLOAT (53) NOT NULL,
    [MinRate]      FLOAT (53) NOT NULL,
    [MaxRate]      FLOAT (53) NOT NULL
);

