﻿CREATE TABLE [minmax].[OutsideMixSuggestResult] (
    [SKU]                  INT          NOT NULL,
    [Store]                INT          NOT NULL,
    [ExecutionDate]        DATE         NOT NULL,
    [SaleDate]             DATE         NULL,
    [TotalRanking]         NVARCHAR (1) NULL,
    [Rate]                 FLOAT (53)   NULL,
    [Suggest]              INT          NULL,
    [NewSuggest]           INT          NULL,
    [Cost]                 FLOAT (53)   NULL,
    [Minimo]               INT          NULL,
    [Fijo]                 INT          NULL,
    [DUN]                  INT          NULL,
    [Planograma]           INT          NULL,
    [NewRestrictedSuggest] INT          NULL,
    [Maximo]               INT          NULL,
    [CajaTira]             INT          NULL,
    [FijoEstricto]         INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_OutsideMixSuggestResult_1]
    ON [minmax].[OutsideMixSuggestResult]([ExecutionDate] ASC)
    INCLUDE([SKU], [Store]);

