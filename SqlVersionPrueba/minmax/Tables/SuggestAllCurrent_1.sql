﻿CREATE TABLE [minmax].[SuggestAllCurrent] (
    [SKU]          BIGINT     NOT NULL,
    [Store]        INT        NOT NULL,
    [Date]         DATE       NOT NULL,
    [Days]         INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [SuggestMin]   INT        NULL,
    [SuggestMax]   INT        NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    CONSTRAINT [PK_SuggestAllCurrent] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllCurrent]
    ON [minmax].[SuggestAllCurrent]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllCurrent_1]
    ON [minmax].[SuggestAllCurrent]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllCurrent_2]
    ON [minmax].[SuggestAllCurrent]([Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllCurrent_Date]
    ON [minmax].[SuggestAllCurrent]([Date] ASC)
    INCLUDE([SKU], [Store], [Days], [Rate], [SuggestMin], [SuggestMax]);

