﻿CREATE TABLE [minmax].[FitsAllNew] (
    [FechaCalculo]  SMALLDATETIME NOT NULL,
    [fechacreacion] SMALLDATETIME NOT NULL,
    [SKU]           INT           NOT NULL,
    [Store]         INT           NOT NULL,
    [Date]          DATE          NOT NULL,
    [Fit]           FLOAT (53)    NULL,
    CONSTRAINT [PK_FitsAllNew] PRIMARY KEY CLUSTERED ([FechaCalculo] ASC, [SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FitsAllNew_SKU]
    ON [minmax].[FitsAllNew]([SKU] ASC)
    INCLUDE([Store], [Date], [Fit]);


GO
CREATE NONCLUSTERED INDEX [IX_FitsAllNew_store]
    ON [minmax].[FitsAllNew]([Store] ASC);

