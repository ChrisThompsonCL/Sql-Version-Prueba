﻿CREATE TABLE [minmax].[HolidayAuxData] (
    [SKU]                 INT            NOT NULL,
    [Store]               INT            NOT NULL,
    [HolidayDate]         DATE           NOT NULL,
    [SL]                  NVARCHAR (100) NOT NULL,
    [LastStockDate]       DATE           NULL,
    [RateBefore]          FLOAT (53)     NULL,
    [RateHoliday]         FLOAT (53)     NULL,
    [SumRate]             FLOAT (53)     NULL,
    [RateNext]            FLOAT (53)     NULL,
    [CurrentSuggest]      INT            NULL,
    [Stock]               INT            NULL,
    [IsMinMax]            BIT            NULL,
    [SuggestSL]           INT            NULL,
    [RealIncreaseSL]      INT            NULL,
    [EffectiveIncreaseSL] INT            NULL,
    [SMinSL]              INT            NULL,
    [SMaxSL]              INT            NULL,
    CONSTRAINT [PK_HolidayAuxData] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [HolidayDate] ASC, [SL] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayAuxData]
    ON [minmax].[HolidayAuxData]([HolidayDate] ASC);

