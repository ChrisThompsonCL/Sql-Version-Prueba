﻿CREATE TABLE [minmax].[DailySales] (
    [SKU]                INT        NOT NULL,
    [Store]              INT        NOT NULL,
    [SaleDate]           DATE       NULL,
    [AvgLargo]           FLOAT (53) NULL,
    [DiasLargo]          INT        NULL,
    [DiasQuebradoLargo]  INT        NULL,
    [AvgMes]             FLOAT (53) NULL,
    [DiasMes]            INT        NULL,
    [DiasQuebradoMes]    INT        NULL,
    [AvgSemana]          FLOAT (53) NULL,
    [DiasSemana]         INT        NULL,
    [DiasQuebradoSemana] INT        NULL,
    [DesvDia]            FLOAT (53) NULL,
    [VentaDia]           FLOAT (53) NULL,
    [DiasVenta]          INT        NULL,
    [MaxVenta]           INT        NULL,
    CONSTRAINT [PK_DailySales_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

