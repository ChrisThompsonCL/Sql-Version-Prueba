﻿CREATE TABLE [minmax].[TruncateLowSaleHistory] (
    [DateKey]    DATE NOT NULL,
    [SKU]        INT  NOT NULL,
    [Store]      INT  NOT NULL,
    [Date]       DATE NOT NULL,
    [SuggestMin] INT  NOT NULL,
    [SuggestMax] INT  NOT NULL,
    [CotaMin]    INT  NOT NULL,
    [CotaMax]    INT  NOT NULL,
    CONSTRAINT [PK_TruncateLowSaleHistory] PRIMARY KEY CLUSTERED ([DateKey] ASC, [SKU] ASC, [Store] ASC, [Date] ASC) ON [FG_200804Q]
);


GO
CREATE NONCLUSTERED INDEX [IX_TruncateLowSaleHistory_Date]
    ON [minmax].[TruncateLowSaleHistory]([Date] ASC)
    INCLUDE([DateKey], [SKU], [Store])
    ON [FG_200804Q];

