﻿CREATE TABLE [minmax].[HolidayIncreaseParams] (
    [HolidayDate]   SMALLDATETIME NULL,
    [BeginDate]     SMALLDATETIME NULL,
    [EndDate]       SMALLDATETIME NULL,
    [TotalRanking]  NVARCHAR (1)  NULL,
    [DailyIncrease] INT           NULL,
    [StockCDFactor] FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayIncreaseParams]
    ON [minmax].[HolidayIncreaseParams]([HolidayDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayIncreaseParams_1]
    ON [minmax].[HolidayIncreaseParams]([TotalRanking] ASC);

