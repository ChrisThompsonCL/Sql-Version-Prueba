﻿CREATE TABLE [minmax].[SuggestParametersAux] (
    [SKU]              INT           NOT NULL,
    [SafetyDays]       INT           NULL,
    [ServiceLevel]     INT           NULL,
    [ServiceLevelDesc] NVARCHAR (10) NULL,
    [LastUpdate]       DATETIME      NULL,
    [UserResp]         NVARCHAR (50) NULL
);

