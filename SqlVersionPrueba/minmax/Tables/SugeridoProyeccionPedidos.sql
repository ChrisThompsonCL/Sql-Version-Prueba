﻿CREATE TABLE [minmax].[SugeridoProyeccionPedidos] (
    [sku]               INT  NOT NULL,
    [StoreId]           INT  NOT NULL,
    [date]              DATE NOT NULL,
    [Minimo]            INT  NULL,
    [Fijo]              INT  NULL,
    [Planograma]        INT  NULL,
    [MedioDUN]          INT  NULL,
    [RestrictedSuggest] INT  NULL,
    [Maximo]            INT  NULL,
    [CajaTira]          INT  NULL,
    [FijoEstricto]      INT  NULL,
    CONSTRAINT [PK_SugeridoProyeccionPedidos] PRIMARY KEY CLUSTERED ([sku] ASC, [StoreId] ASC, [date] ASC)
);

