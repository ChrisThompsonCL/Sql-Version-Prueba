﻿CREATE TABLE [minmax].[DistributionOrderDeltas] (
    [SKU]                      INT        NOT NULL,
    [Date]                     DATE       NOT NULL,
    [SobrecargaFeriado]        FLOAT (53) NULL,
    [SobrecargaPromocion]      FLOAT (53) NULL,
    [SobrecargaEstacionalidad] FLOAT (53) NULL,
    [SobrecargaRestricción]    FLOAT (53) NULL,
    [ExcesoCD]                 FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([Date] ASC, [SKU] ASC)
);

