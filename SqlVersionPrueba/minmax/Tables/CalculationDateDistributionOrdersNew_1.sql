﻿CREATE TABLE [minmax].[CalculationDateDistributionOrdersNew] (
    [SKU]           INT           NOT NULL,
    [Fecha_calculo] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_CalculationDateDistributionOrdersNew] PRIMARY KEY CLUSTERED ([SKU] ASC, [Fecha_calculo] ASC)
);

