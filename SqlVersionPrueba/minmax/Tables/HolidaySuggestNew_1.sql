﻿CREATE TABLE [minmax].[HolidaySuggestNew] (
    [SKU]         INT  NOT NULL,
    [Store]       INT  NOT NULL,
    [HolidayDate] DATE NOT NULL,
    [CreateDate]  DATE NULL,
    [LastSug]     INT  NULL,
    [Increase]    INT  NULL,
    [MaxHolidays] INT  NULL,
    [MinHolidays] INT  NULL,
    [BeginDate]   DATE NULL,
    [EndDate]     DATE NULL,
    CONSTRAINT [PK_HolidaySuggestNew] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [HolidayDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidaySuggestNew_CreateDate]
    ON [minmax].[HolidaySuggestNew]([CreateDate] ASC)
    INCLUDE([SKU], [Store], [HolidayDate]);


GO
CREATE NONCLUSTERED INDEX [IX_HolidaySuggestNew_Dates]
    ON [minmax].[HolidaySuggestNew]([HolidayDate] ASC, [BeginDate] ASC, [EndDate] ASC)
    INCLUDE([SKU], [Store], [MaxHolidays], [MinHolidays]);


GO
CREATE NONCLUSTERED INDEX [IX_HolidaySuggestNew_Dates2]
    ON [minmax].[HolidaySuggestNew]([HolidayDate] ASC, [CreateDate] ASC)
    INCLUDE([SKU], [Store]);

