﻿CREATE TABLE [minmax].[DistributionOrdersForecastCurrent] (
    [Date]                DATE       NOT NULL,
    [sku]                 INT        NOT NULL,
    [Venta]               FLOAT (53) NULL,
    [Stock]               FLOAT (53) NULL,
    [Pedido]              FLOAT (53) NULL,
    [PedidoEfectivo]      FLOAT (53) NULL,
    [PedidoDisponible]    FLOAT (53) NULL,
    [PedidoEnTransito]    FLOAT (53) NULL,
    [VentaPerdida]        INT        NULL,
    [Sugerido]            INT        NULL,
    [Delta_Restricciones] INT        NULL,
    CONSTRAINT [PK_DistributionOrdersForecastCurrent] PRIMARY KEY CLUSTERED ([Date] ASC, [sku] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SKU_Date]
    ON [minmax].[DistributionOrdersForecastCurrent]([sku] ASC, [Date] ASC);

