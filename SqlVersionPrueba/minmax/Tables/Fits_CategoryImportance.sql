﻿CREATE TABLE [minmax].[Fits_CategoryImportance] (
    [fin_futuro]  DATE       NULL,
    [category_ID] INT        NOT NULL,
    [store_ID]    INT        NOT NULL,
    [month]       INT        NOT NULL,
    [day]         INT        NOT NULL,
    [importance]  FLOAT (53) NOT NULL,
    CONSTRAINT [PK_Rates_CategoryImportance] PRIMARY KEY CLUSTERED ([category_ID] ASC, [store_ID] ASC, [month] ASC, [day] ASC)
);

