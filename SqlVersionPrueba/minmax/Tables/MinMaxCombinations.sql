﻿CREATE TABLE [minmax].[MinMaxCombinations] (
    [SKU]       INT  NOT NULL,
    [Store]     INT  NOT NULL,
    [EntryDate] DATE NULL,
    CONSTRAINT [PK_MinMaxCombinations] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

