﻿CREATE TABLE [minmax].[HolidayClientExceptions] (
    [SKU]         INT           NOT NULL,
    [HolidayDate] DATE          NOT NULL,
    [SelectedSL]  NVARCHAR (10) NULL,
    CONSTRAINT [PK_HolidayClientExceptions_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [HolidayDate] ASC)
);

