﻿CREATE TABLE [minmax].[TruncateRatesHistory] (
    [Date]      DATE       NOT NULL,
    [SKU]       INT        NOT NULL,
    [Store]     INT        NOT NULL,
    [DailySale] FLOAT (53) NULL,
    [FitDay]    FLOAT (53) NULL,
    [FitWeek]   FLOAT (53) NULL,
    [ShareDay]  FLOAT (53) NULL
);

