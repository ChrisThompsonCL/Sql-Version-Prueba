﻿CREATE TABLE [minmax].[HolidayApprovedSuggests] (
    [SKU]          INT          NOT NULL,
    [Store]        INT          NOT NULL,
    [HolidayDate]  DATE         NOT NULL,
    [TotalRanking] VARCHAR (50) NULL,
    [SMax]         INT          NULL,
    [SMin]         INT          NULL,
    CONSTRAINT [PK_HolidayApprovedSuggests] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [HolidayDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayApprovedSuggest_HolidayDate]
    ON [minmax].[HolidayApprovedSuggests]([HolidayDate] ASC)
    INCLUDE([SKU], [Store], [TotalRanking], [SMax], [SMin]);

