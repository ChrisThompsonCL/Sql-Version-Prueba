﻿CREATE TABLE [minmax].[SuggestForecast] (
    [SKU]   INT        NULL,
    [Store] INT        NULL,
    [W1]    FLOAT (53) NULL,
    [W2]    FLOAT (53) NULL,
    [W3]    FLOAT (53) NULL,
    [W4]    FLOAT (53) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestForecast_SKUStore]
    ON [minmax].[SuggestForecast]([SKU] ASC, [Store] ASC)
    INCLUDE([W1], [W2], [W3], [W4]);

