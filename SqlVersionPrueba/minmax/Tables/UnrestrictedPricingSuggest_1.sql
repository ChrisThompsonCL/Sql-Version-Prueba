﻿CREATE TABLE [minmax].[UnrestrictedPricingSuggest] (
    [SKU]             INT  NOT NULL,
    [Store]           INT  NOT NULL,
    [Date]            DATE NOT NULL,
    [SuggestMin]      INT  NOT NULL,
    [SuggestMax]      INT  NOT NULL,
    [HolidayIncrease] BIT  NOT NULL,
    CONSTRAINT [PK_PreRestrictionPricingSuggest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_UnrestrictedPricingSuggest_Store]
    ON [minmax].[UnrestrictedPricingSuggest]([Store] ASC)
    INCLUDE([SKU]);

