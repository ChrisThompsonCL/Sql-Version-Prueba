﻿CREATE TABLE [minmax].[SuggestAllNew] (
    [SKU]          BIGINT     NOT NULL,
    [Store]        INT        NOT NULL,
    [Date]         DATE       NOT NULL,
    [Days]         INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [SuggestMin]   INT        NULL,
    [SuggestMax]   INT        NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    CONSTRAINT [PK_SuggestAllNew] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllNew]
    ON [minmax].[SuggestAllNew]([SKU] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllNew_1]
    ON [minmax].[SuggestAllNew]([Store] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SuggestAllNew_2]
    ON [minmax].[SuggestAllNew]([Date] ASC);

