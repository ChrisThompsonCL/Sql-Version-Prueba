﻿CREATE TABLE [minmax].[LogReglaBajas] (
    [Fecha]      DATE NOT NULL,
    [TotalCombs] INT  NULL,
    [CambioMin]  INT  NULL,
    [CambioMax]  INT  NULL,
    CONSTRAINT [PK_LogReglaBajas] PRIMARY KEY CLUSTERED ([Fecha] ASC)
);

