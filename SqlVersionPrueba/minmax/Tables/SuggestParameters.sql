﻿CREATE TABLE [minmax].[SuggestParameters] (
    [SKU]              INT           NOT NULL,
    [SafetyDays]       INT           NULL,
    [ServiceLevel]     INT           NULL,
    [ServiceLevelDesc] NVARCHAR (10) NULL,
    [LastUpdate]       DATETIME      NULL,
    [UserResp]         NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([SKU] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20180123-175747]
    ON [minmax].[SuggestParameters]([SKU] ASC);

