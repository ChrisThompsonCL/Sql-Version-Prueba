﻿CREATE TABLE [minmax].[Faltante] (
    [FechaCalculo] DATE NULL,
    [SKU]          INT  NOT NULL,
    [Store]        INT  NOT NULL,
    [StockPedidos] INT  NULL,
    [StockReal]    INT  NULL,
    [Faltante]     INT  NULL,
    CONSTRAINT [PK_Faltante] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC)
);

