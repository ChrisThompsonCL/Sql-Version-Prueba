﻿CREATE TABLE [minmax].[Replenishment] (
    [Store]          INT           NOT NULL,
    [LogisticAreaId] NVARCHAR (50) NOT NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [Days]           INT           NULL,
    [LT]             INT           NULL,
    CONSTRAINT [PK_Replenishment_2] PRIMARY KEY CLUSTERED ([Store] ASC, [LogisticAreaId] ASC, [Date] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Replenishment_DateDays]
    ON [minmax].[Replenishment]([Date] ASC, [Days] ASC)
    INCLUDE([Store], [LogisticAreaId]);


GO
CREATE NONCLUSTERED INDEX [idx_lt]
    ON [minmax].[Replenishment]([LT] ASC)
    INCLUDE([Store], [LogisticAreaId]);


GO
CREATE NONCLUSTERED INDEX [IDX_store_log_days_lt]
    ON [minmax].[Replenishment]([Date] ASC)
    INCLUDE([Store], [LogisticAreaId], [Days], [LT]);


GO
CREATE NONCLUSTERED INDEX [IDX_StoreDateLt]
    ON [minmax].[Replenishment]([LogisticAreaId] ASC, [Days] ASC)
    INCLUDE([Store], [Date], [LT]);

