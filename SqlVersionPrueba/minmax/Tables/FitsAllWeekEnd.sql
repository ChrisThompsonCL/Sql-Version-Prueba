﻿CREATE TABLE [minmax].[FitsAllWeekEnd] (
    [FechaCalculo]  SMALLDATETIME NOT NULL,
    [fechacreacion] SMALLDATETIME NOT NULL,
    [SKU]           INT           NOT NULL,
    [Store]         INT           NOT NULL,
    [Date]          DATE          NOT NULL,
    [Fit]           FLOAT (53)    NULL,
    CONSTRAINT [PK_FitsAllWeekEnd] PRIMARY KEY CLUSTERED ([FechaCalculo] ASC, [SKU] ASC, [Store] ASC, [Date] ASC)
);

