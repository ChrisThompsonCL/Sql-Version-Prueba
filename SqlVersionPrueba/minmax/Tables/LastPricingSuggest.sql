﻿CREATE TABLE [minmax].[LastPricingSuggest] (
    [SKU]          BIGINT     NOT NULL,
    [Store]        INT        NOT NULL,
    [Date]         DATE       NOT NULL,
    [SuggestMin]   INT        NULL,
    [SuggestMax]   INT        NULL,
    [Days]         INT        NULL,
    [Rate]         FLOAT (53) NULL,
    [LeadTime]     INT        NULL,
    [SafetyDays]   INT        NULL,
    [ServiceLevel] FLOAT (53) NULL,
    [Minimo]       INT        NULL,
    [Fijo]         INT        NULL,
    [Planograma]   INT        NULL,
    [MedioDUN]     INT        NULL,
    [Maximo]       INT        NULL,
    [CajaTira]     INT        NULL,
    [FijoEstricto] INT        NULL,
    CONSTRAINT [PK_LastPricingSuggest] PRIMARY KEY CLUSTERED ([SKU] ASC, [Store] ASC, [Date] ASC)
);

