﻿-- =============================================
-- Author:		Carlos Muñoz
-- Create date: 2018-01-03
-- Description:	FUNCIÓN DUMMY!! para probar y avanzar la pantalla de Visualización de Cálculos de Inventario
-- =============================================
CREATE FUNCTION [minmax].[GetInventorySuggest]
(
	@productosList NVARCHAR(MAX),
	@localesList NVARCHAR(MAX),
	@proveedoresList NVARCHAR(MAX),
	@RankingList NVARCHAR(MAX),
	@BodegaList NVARCHAR(MAX)
)
RETURNS 
@Resultado TABLE 
(
	SKU INT,
	Store INT,
	FechaCalculo DATETIME,
	Desde DATETIME,
	Hasta DATETIME,
	SugeridoActual FLOAT,
	NivelServicio FLOAT,
	Sobrproteccion FLOAT,
	[Min] INT,
	[Max] INT,
	Ruta NVARCHAR(4000)
)
AS
BEGIN
	DECLARE @skus TABLE (SKU INT);
	----
	INSERT INTO @skus
	SELECT DISTINCT SKU
	FROM (
		SELECT SKU
		FROM products.Products
		WHERE ManufacturerId IN (SELECT * FROM dbo.SplitString(@proveedoresList))
		UNION
		SELECT SKU
		FROM products.Products
		WHERE TotalRanking IN (SELECT * FROM dbo.SplitString(@RankingList))
		UNION
		SELECT SKU
		FROM products.Products
		WHERE LogisticAreaId IN (SELECT * FROM dbo.SplitString(@BodegaList))
		UNION
		SELECT * FROM dbo.SplitString(@productosList)
	) a
	--

IF @productosList is null
	BEGIN
	INSERT INTO @Resultado

			SELECT
				SKU SKU,
				Store Store,
				CAST(Date AS DATETIME) FechaCalculo,
				GETDATE() Desde,
				GETDATE() Hasta,
				CAST(10 AS FLOAT) SugeridoActual,
				CAST(10 AS FLOAT) NivelServicio,
				CAST(10 AS FLOAT) Sobreproteccion,
				[Min],
				[Max],
				'-' Ruta
				
				FROM [reports].[LastReportStores] rs
				WHERE (rs.Store IN (SELECT * FROM dbo.SplitString(@localesList)) OR @localesList IS NULL)
		ORDER BY rs.Store
  option (OPTIMIZE FOR (@localesList UNKNOWN))

	END

ELSE
	BEGIN 
		INSERT INTO @Resultado
		SELECT
			SKU SKU,
			Store Store,
			CAST(Date AS DATETIME) FechaCalculo,
			GETDATE() Desde,
			GETDATE() Hasta,
			CAST(10 AS FLOAT) SugeridoActual,
			CAST(10 AS FLOAT) NivelServicio,
			CAST(10 AS FLOAT) Sobreproteccion,
			[Min],
			[Max],
			'-' Ruta
			FROM [reports].[LastReportStores] rs
			WHERE (rs.SKU IN (SELECT * FROM @skus) OR @productosList IS NULL)
			AND (rs.Store IN (SELECT * FROM dbo.SplitString(@localesList)) OR @localesList IS NULL)
		ORDER BY rs.SKU, rs.Store
  option (OPTIMIZE FOR (@localesList UNKNOWN))
		
	END



	RETURN 

END
